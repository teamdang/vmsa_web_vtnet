/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 * @author quytv7
 */

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "STATION_PUT_FILE_OSS")
public class StationFlowRunAction implements Serializable {

    private String groupActionName;
    private StationPlan stationPlan;
    private Long id;
    private Node node;
    private FlowRunAction flowRunAction;
    private String filePath;
    private String fileName;
    private String commandPutFile;
    private Date updateTime;
    private Long type;
    private byte[] fileContent;
    private FlowRunAction flowRunActionRollback;

    @Column(name = "FILE_CONTENT")
    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }


    @Column(name = "TYPE", precision = 22, scale = 0)
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NODE_ID")
    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FLOW_RUN_ID")
    public FlowRunAction getFlowRunAction() {
        return flowRunAction;
    }

    public void setFlowRunAction(FlowRunAction flowRunAction) {
        this.flowRunAction = flowRunAction;
    }

    @Column(name = "GROUP_ACTION_NAME", length = 200)
    public String getGroupActionName() {
        return groupActionName;
    }

    public void setGroupActionName(String groupActionName) {
        this.groupActionName = groupActionName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATION_PLAN_ID")
    public StationPlan getStationPlan() {
        return stationPlan;
    }

    public void setStationPlan(StationPlan stationPlan) {
        this.stationPlan = stationPlan;
    }

    @SequenceGenerator(name = "generator", sequenceName = "STATION_PUT_FILE_OSS_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "FILE_PATH", length = 500)
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Column(name = "FILE_NAME", length = 500)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Column(name = "COMMAND_PUT_FILE", length = 500)
    public String getCommandPutFile() {
        return commandPutFile;
    }

    public void setCommandPutFile(String commandPutFile) {
        this.commandPutFile = commandPutFile;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FLOW_RUN_ROLLBACK_ID")
    public FlowRunAction getFlowRunActionRollback() {
        return flowRunActionRollback;
    }

    public void setFlowRunActionRollback(FlowRunAction flowRunActionRollback) {
        this.flowRunActionRollback = flowRunActionRollback;
    }


    public StationFlowRunAction() {
    }

    public StationFlowRunAction(Long id) {
        this.id = id;
    }
}
