package com.viettel.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "STATION_PARAM_INPUT")
public class StationParamInput implements Serializable {

    private String paramValue;
    private String paramCode;
    private String cellId;
    private Date updateTime;
    private Long paramInputId;

    @Column(name = "PARAM_VALUE", length = 200)
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Column(name = "PARAM_CODE", length = 2000)
    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    @Column(name = "CELL_ID", length = 20)
    public String getCellId() {
        return cellId;
    }

    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @SequenceGenerator(name = "generator", sequenceName = "STATION_PARAM_INPUT_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "PARAM_INPUT_ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getParamInputId() {
        return paramInputId;
    }

    public void setParamInputId(Long paramInputId) {
        this.paramInputId = paramInputId;
    }

    public StationParamInput() {
    }

    public StationParamInput(Long paramInputId) {
        this.paramInputId = paramInputId;
    }
}
