/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 * @author quytv7
 */

import com.viettel.util.Config;
import com.viettel.util.MessageUtil;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;

import static javax.persistence.GenerationType.SEQUENCE;

import java.util.Date;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "STATION_PLAN")
public class StationPlan implements Serializable {

    private String userCreate;
    private Long id;
    private Date updateTime;
    private String fileName;
    private String crNumber;
    private String networkType;
    private Vendor vendor;
    private Long proficientType;
    private String proficientTypeStr;
    private String crNumberGnoc;
    private Long createDtId;

    private byte[] fileContent;
    private String filePath;

    @Column(name = "FILE_PATH")
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Column(name = "FILE_CONTENT")
    public byte[] getFileContent() {
        return fileContent;
    }

    public void setFileContent(byte[] fileContent) {
        this.fileContent = fileContent;
    }

    @Transient
    public Long getCreateDtId() {
        return createDtId;
    }

    public void setCreateDtId(Long createDtId) {
        this.createDtId = createDtId;
    }

    @Transient
    public String getCrNumberGnoc() {
        return crNumberGnoc;
    }

    public void setCrNumberGnoc(String crNumberGnoc) {
        this.crNumberGnoc = crNumberGnoc;
    }

    @Transient
    public String getProficientTypeStr() {
        if (proficientType == null) {
            return "";
        } else if (proficientType.equals(Config.IS_RULE_INTEGRATE)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type0");
        } else if (proficientType.equals(Config.IS_RULE_RELATION)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type1");
        } else if (proficientType.equals(Config.IS_RULE_DELETE)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type2");
        } else if (proficientType.equals(Config.IS_RULE_UPGRADE_DONWGRADE)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type3");
        } else if (proficientType.equals(Config.IS_RULE_CHANGE_FESTIVAL)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type4");
        } else if (proficientType.equals(Config.IS_RULE_CHANGE_FORCE)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type5");
        } else if (proficientType.equals(Config.IS_RULE_LOAD_LICENSE)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type7");
        } else if (proficientType.equals(Config.IS_RULE_CHANGE_PARAM)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type8");
        }else if (proficientType.equals(Config.IS_RULE_ACTIVE_STANDARD)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type9");
        }else if (proficientType.equals(Config.IS_RULE_ACTIVE_TEST)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type10");
        }else if (proficientType.equals(Config.IS_RULE_CHANGE_VLAN)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type11");
        }

        else {
            return "";
        }
    }

    public void setProficientTypeStr(String proficientTypeStr) {
        this.proficientTypeStr = proficientTypeStr;
    }

    @Column(name = "PROFICIENT_TYPE", precision = 22, scale = 0)
    public Long getProficientType() {
        return proficientType;
    }

    public void setProficientType(Long proficientType) {
        this.proficientType = proficientType;
    }

    @Column(name = "NETWORK_TYPE", length = 50)
    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VENDOR_ID")
    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Column(name = "CR_NUMBER", length = 200)
    public String getCrNumber() {
        return crNumber;
    }

    public void setCrNumber(String crNumber) {
        this.crNumber = crNumber;
    }

    @Column(name = "USER_CREATE", length = 200)
    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    @SequenceGenerator(name = "generator", sequenceName = "STATION_IMPORT_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "FILE_NAME", length = 500)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public StationPlan() {
    }

    public StationPlan(Long id) {
        this.id = id;
    }
}
