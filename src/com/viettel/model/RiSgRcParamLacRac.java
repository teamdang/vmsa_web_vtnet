/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_SG_RC_PARAM_LAC_RAC")
public class RiSgRcParamLacRac implements Serializable {

    private String rncCode;
    private String sgsnActive;
    private String rac;
    private String id;
    private Date updateTime;
    private String lac;

    @Column(name = "RNC_CODE", length = 200)
    public String getRncCode() {
        return rncCode;
    }

    public void setRncCode(String rncCode) {
        this.rncCode = rncCode;
    }

    @Column(name = "SGSN_ACTIVE", length = 200)
    public String getSgsnActive() {
        return sgsnActive;
    }

    public void setSgsnActive(String sgsnActive) {
        this.sgsnActive = sgsnActive;
    }

    @Column(name = "RAC", length = 20)
    public String getRac() {
        return rac;
    }

    public void setRac(String rac) {
        this.rac = rac;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_LAC_RAC_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 20, scale = 0)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "LAC", length = 200)
    public String getLac() {
        return lac;
    }

    public void setLac(String lac) {
        this.lac = lac;
    }

    public RiSgRcParamLacRac() {
    }

    public RiSgRcParamLacRac(String id) {
        this.id = id;
    }
}
