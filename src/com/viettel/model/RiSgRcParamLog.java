/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author quytv7
 */
@Entity
@Table(name = "RI_SG_RC_PARAM_LOG")
public class RiSgRcParamLog implements java.io.Serializable {

    private Long id;
    private String nodeCode;
    private String nodeIp;
    private Date insertTime;
    private String userExcute;
    private String vendorName;
    private String versionName;
    private String nodeTypeName;

    @Column(name = "VENDOR_NAME", length = 200)
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Column(name = "VERSION_NAME", length = 200)
    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    @Column(name = "NODE_TYPE_NAME", length = 200)
    public String getNodeTypeName() {
        return nodeTypeName;
    }

    public void setNodeTypeName(String nodeTypeName) {
        this.nodeTypeName = nodeTypeName;
    }

    public RiSgRcParamLog() {
    }

    public RiSgRcParamLog(Long id) {
        this.id = id;
    }

    @Column(name = "NODE_CODE", length = 200)
    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    @Column(name = "NODE_IP", length = 500)
    public String getNodeIp() {
        return nodeIp;
    }

    public void setNodeIp(String nodeIp) {
        this.nodeIp = nodeIp;
    }

    @Column(name = "USER_EXCUTE", length = 500)
    public String getUserExcute() {
        return userExcute;
    }

    public void setUserExcute(String userExcute) {
        this.userExcute = userExcute;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INSERT_TIME", nullable = false, length = 7)
    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_LOG_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 20, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
