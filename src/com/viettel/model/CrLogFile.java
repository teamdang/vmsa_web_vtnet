/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author hienhv4
 */
@Entity
@Table(name = "CR_LOG_FILE")
public class CrLogFile implements java.io.Serializable {

    private String crNumber;
    private byte[] logFile;
    private byte[] dtFile;

    @Id
    @Column(name = "CR_NUMBER")
    public String getCrNumber() {
        return crNumber;
    }

    public void setCrNumber(String crNumber) {
        this.crNumber = crNumber;
    }

    @Column(name = "LOG_FILE")
    public byte[] getLogFile() {
        return logFile;
    }

    public void setLogFile(byte[] logFile) {
        this.logFile = logFile;
    }

    @Column(name = "DT_FILE")
    public byte[] getDtFile() {
        return dtFile;
    }

    public void setDtFile(byte[] dtFile) {
        this.dtFile = dtFile;
    }
}
