package com.viettel.model;

// Generated Sep 20, 2016 10:49:00 AM by Hibernate Tools 4.0.0
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * ParamInOut generated by hbm2java
 */
@Entity
@Table(name = "PARAM_IN_OUT")
public class ParamInOut implements java.io.Serializable {

    private ParamInOutId id;
    private ActionOfFlow actionOfFlowByActionFlowOutId;
    private ActionOfFlow actionOfFlowByActionFlowInId;
    private ActionCommand actionCommandByActionCommandOutputId;
    private ParamInput paramInput;
    private ActionCommand actionCommandByActionCommandInputId;
    private Integer paramInOutOrder;
    //20180112_quytv7_them kieu clone tham so tham chieu
    private Long type;

    public ParamInOut() {
    }

    public ParamInOut(ParamInOutId id, ActionCommand actionCommandByActionCommandOutputId, ParamInput paramInput, ActionCommand actionCommandByActionCommandInputId) {
        this.id = id;
        this.actionCommandByActionCommandOutputId = actionCommandByActionCommandOutputId;
        this.paramInput = paramInput;
        this.actionCommandByActionCommandInputId = actionCommandByActionCommandInputId;
    }

    public ParamInOut(ParamInOutId id, ActionOfFlow actionOfFlowByActionFlowOutId, ActionOfFlow actionOfFlowByActionFlowInId, ActionCommand actionCommandByActionCommandOutputId,
            ParamInput paramInput, ActionCommand actionCommandByActionCommandInputId) {
        this.id = id;
        this.actionOfFlowByActionFlowOutId = actionOfFlowByActionFlowOutId;
        this.actionOfFlowByActionFlowInId = actionOfFlowByActionFlowInId;
        this.actionCommandByActionCommandOutputId = actionCommandByActionCommandOutputId;
        this.paramInput = paramInput;
        this.actionCommandByActionCommandInputId = actionCommandByActionCommandInputId;
    }

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "actionCommandInputId", column = @Column(name = "ACTION_COMMAND_INPUT_ID", nullable = false, precision = 22, scale = 0)),
        @AttributeOverride(name = "paramInputId", column = @Column(name = "PARAM_INPUT_ID", nullable = false, precision = 22, scale = 0)),
        @AttributeOverride(name = "actionCommandOutputId", column = @Column(name = "ACTION_COMMAND_OUTPUT_ID", nullable = false, precision = 22, scale = 0))})
    public ParamInOutId getId() {
        return this.id;
    }

    public void setId(ParamInOutId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "ACTION_FLOW_OUT_ID")
    public ActionOfFlow getActionOfFlowByActionFlowOutId() {
        return this.actionOfFlowByActionFlowOutId;
    }

    public void setActionOfFlowByActionFlowOutId(ActionOfFlow actionOfFlowByActionFlowOutId) {
        this.actionOfFlowByActionFlowOutId = actionOfFlowByActionFlowOutId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "ACTION_FLOW_IN_ID", insertable = false, updatable = false)
    public ActionOfFlow getActionOfFlowByActionFlowInId() {
        return this.actionOfFlowByActionFlowInId;
    }

    public void setActionOfFlowByActionFlowInId(ActionOfFlow actionOfFlowByActionFlowInId) {
        this.actionOfFlowByActionFlowInId = actionOfFlowByActionFlowInId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "ACTION_COMMAND_OUTPUT_ID", nullable = false)
    public ActionCommand getActionCommandByActionCommandOutputId() {
        return this.actionCommandByActionCommandOutputId;
    }

    public void setActionCommandByActionCommandOutputId(ActionCommand actionCommandByActionCommandOutputId) {
        this.actionCommandByActionCommandOutputId = actionCommandByActionCommandOutputId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "PARAM_INPUT_ID", nullable = false, insertable = false, updatable = false)
    public ParamInput getParamInput() {
        return this.paramInput;
    }

    public void setParamInput(ParamInput paramInput) {
        this.paramInput = paramInput;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "ACTION_COMMAND_INPUT_ID", nullable = false, insertable = false, updatable = false)
    public ActionCommand getActionCommandByActionCommandInputId() {
        return this.actionCommandByActionCommandInputId;
    }

    public void setActionCommandByActionCommandInputId(ActionCommand actionCommandByActionCommandInputId) {
        this.actionCommandByActionCommandInputId = actionCommandByActionCommandInputId;
    }

    @Transient
    public Integer getParamInOutOrder() {
        return paramInOutOrder;
    }

    public void setParamInOutOrder(Integer paramInOutOrder) {
        this.paramInOutOrder = paramInOutOrder;
    }

    @Column
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

}
