/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_MS_RC_PARAM_MSC_RNC")
public class RiMsRcParamMscRnc implements Serializable {

    private String mscCode;
    private String rncOfc;
    private String rncRt;
    private String rncPort;
    private String rncLinkName;
    private String rncSsn;
    private String rncIp2;
    private String rncIp1;
    private String rncSccp;
    private Date updateTime;
    private String rncLinkSet;
    private String rncCode;
    private String mscIp1;
    private String mscIp2;
    private String mscPort;
    private Long id;
    private String rncSpc;
    private String rncDsp;

    @Column(name = "MSC_CODE", length = 200)
    public String getMscCode() {
        return mscCode;
    }

    public void setMscCode(String mscCode) {
        this.mscCode = mscCode;
    }

    @Column(name = "RNC_OFC", length = 100)
    public String getRncOfc() {
        return rncOfc;
    }

    public void setRncOfc(String rncOfc) {
        this.rncOfc = rncOfc;
    }

    @Column(name = "RNC_RT", length = 100)
    public String getRncRt() {
        return rncRt;
    }

    public void setRncRt(String rncRt) {
        this.rncRt = rncRt;
    }

    @Column(name = "RNC_PORT", length = 50)
    public String getRncPort() {
        return rncPort;
    }

    public void setRncPort(String rncPort) {
        this.rncPort = rncPort;
    }

    @Column(name = "RNC_LINK_NAME", length = 100)
    public String getRncLinkName() {
        return rncLinkName;
    }

    public void setRncLinkName(String rncLinkName) {
        this.rncLinkName = rncLinkName;
    }

    @Column(name = "RNC_SSN", length = 200)
    public String getRncSsn() {
        return rncSsn;
    }

    public void setRncSsn(String rncSsn) {
        this.rncSsn = rncSsn;
    }

    @Column(name = "RNC_IP2", length = 100)
    public String getRncIp2() {
        return rncIp2;
    }

    public void setRncIp2(String rncIp2) {
        this.rncIp2 = rncIp2;
    }

    @Column(name = "RNC_IP1", length = 100)
    public String getRncIp1() {
        return rncIp1;
    }

    public void setRncIp1(String rncIp1) {
        this.rncIp1 = rncIp1;
    }

    @Column(name = "RNC_SCCP", length = 200)
    public String getRncSccp() {
        return rncSccp;
    }

    public void setRncSccp(String rncSccp) {
        this.rncSccp = rncSccp;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "RNC_LINK_SET", length = 100)
    public String getRncLinkSet() {
        return rncLinkSet;
    }

    public void setRncLinkSet(String rncLinkSet) {
        this.rncLinkSet = rncLinkSet;
    }

    @Column(name = "RNC_CODE", length = 200)
    public String getRncCode() {
        return rncCode;
    }

    public void setRncCode(String rncCode) {
        this.rncCode = rncCode;
    }

    @Column(name = "MSC_IP1", length = 100)
    public String getMscIp1() {
        return mscIp1;
    }

    public void setMscIp1(String mscIp1) {
        this.mscIp1 = mscIp1;
    }

    @Column(name = "MSC_IP2", length = 100)
    public String getMscIp2() {
        return mscIp2;
    }

    public void setMscIp2(String mscIp2) {
        this.mscIp2 = mscIp2;
    }

    @Column(name = "MSC_PORT", length = 50)
    public String getMscPort() {
        return mscPort;
    }

    public void setMscPort(String mscPort) {
        this.mscPort = mscPort;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_MS_RC_PARAM_MSC_RNC_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "RNC_SPC", length = 50)
    public String getRncSpc() {
        return rncSpc;
    }

    public void setRncSpc(String rncSpc) {
        this.rncSpc = rncSpc;
    }

    @Column(name = "RNC_DSP", length = 100)
    public String getRncDsp() {
        return rncDsp;
    }

    public void setRncDsp(String rncDsp) {
        this.rncDsp = rncDsp;
    }

    public RiMsRcParamMscRnc() {
    }

    public RiMsRcParamMscRnc(Long id) {
        this.id = id;
    }
}
