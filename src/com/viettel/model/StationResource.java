package com.viettel.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "STATION_RESOURCE")
public class StationResource implements Serializable {

    private String sgsn;
    private String celllCode;
    private Long id;
    private String msc;
    private String rncBsc;
    private String bscType;

    @Column(name = "BSC_TYPE", length = 800)
    public String getBscType() {
        return bscType;
    }

    public void setBscType(String bscType) {
        this.bscType = bscType;
    }

    @Column(name = "SGSN", length = 800)
    public String getSgsn() {
        return sgsn;
    }

    public void setSgsn(String sgsn) {
        this.sgsn = sgsn;
    }

    @Column(name = "CELLL_CODE", length = 800)
    public String getCelllCode() {
        return celllCode;
    }

    public void setCelllCode(String celllCode) {
        this.celllCode = celllCode;
    }

    @SequenceGenerator(name = "generator", sequenceName = "STATION_RESOURCE_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "MSC", length = 800)
    public String getMsc() {
        return msc;
    }

    public void setMsc(String msc) {
        this.msc = msc;
    }

    @Column(name = "RNC_BSC", length = 800)
    public String getRncBsc() {
        return rncBsc;
    }

    public void setRncBsc(String rncBsc) {
        this.rncBsc = rncBsc;
    }

    public StationResource() {
    }

    public StationResource(Long id) {
        this.id = id;
    }
}
