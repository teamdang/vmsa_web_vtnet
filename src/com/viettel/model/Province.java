package com.viettel.model;
// Generated Oct 20, 2016 5:33:59 PM by Hibernate Tools 4.0.0

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Province generated by hbm2java
 */
@Entity
@Table(name = "PROVINCE")
public class Province implements java.io.Serializable {

    private Long id;
    private String areaCode;
    private String bccsCode;
    private String provinceCode;
    private String provinceName;

    public Province() {
    }

    public Province(Long id) {
        this.id = id;
    }

    public Province(Long id, String areaCode, String bccsCode, String provinceCode, String provinceName) {
        this.id = id;
        this.areaCode = areaCode;
        this.bccsCode = bccsCode;
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
    }

    @Column(name = "ID", unique = true, nullable = false, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "AREA_CODE", length = 20)
    public String getAreaCode() {
        return this.areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    @Id
    @Column(name = "BCCS_CODE")
    public String getBccsCode() {
        return this.bccsCode;
    }

    public void setBccsCode(String bccsCode) {
        this.bccsCode = bccsCode;
    }

    @Column(name = "PROVINCE_CODE", length = 20)
    public String getProvinceCode() {
        return this.provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    @Column(name = "PROVINCE_NAME", length = 50)
    public String getProvinceName() {
        return this.provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.bccsCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Province other = (Province) obj;
        if (!Objects.equals(this.bccsCode, other.bccsCode)) {
            return false;
        }
        return true;
    }

}
