/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author Dunglv
 */

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "AUTO_RESET_CONFIG_PARAM")
public class AutoResetConfigParam implements Serializable {

    private Long id;
    private Long configMopId;
    private String paramCode;
    private String columnName;
    private String regexFomula;
    private Date updateTime;
    private Long paramType;
    private String manualParamCode;

    public AutoResetConfigParam() {
    }

    public AutoResetConfigParam(Long id) {
        this.id = id;
    }
    
    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "CONFIG_MOP_ID", precision = 22, scale = 0)
    public Long getConfigMopId() {
        return configMopId;
    }

    public void setConfigMopId(Long configMopId) {
        this.configMopId = configMopId;
    }

    @Column(name = "PARAM_CODE", length = 200)
    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    @Column(name = "COLUMN_NAME", length = 200)
    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @Column(name = "REGEX_FOMULA", length = 200)
    public String getRegexFomula() {
        return regexFomula;
    }

    public void setRegexFomula(String regexFomula) {
        this.regexFomula = regexFomula;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "PARAM_TYPE", precision = 22, scale = 0)
    public Long getParamType() {
        return paramType;
    }

    public void setParamType(Long paramType) {
        this.paramType = paramType;
    }

    @Column(name = "MANUAL_PARAM_CODE", length = 200)
    public String getManualParamCode() {
        return manualParamCode;
    }

    public void setManualParamCode(String manualParamCode) {
        this.manualParamCode = manualParamCode;
    }
}
