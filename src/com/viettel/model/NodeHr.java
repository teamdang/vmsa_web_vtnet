package com.viettel.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "NODE_HR")
public class NodeHr implements Serializable {
    private Long id;
    private String cellCode;
    private String rncBsc;
    private Double scr;
    private Double tcr;
    private String kvCode;
    private String provCode;
    private Long tuNonHr;
    private Long trx;
    private Date updateTime;
    private Long countScr;
    private Long countTcr;
    private Long type;
    private Long dtStatus;
    
    public NodeHr() {
    }

    public NodeHr(Long id) {
        this.id = id;
    }

    @SequenceGenerator(name = "generator", sequenceName = "NODE_HR_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "SCR")
    public Double getScr() {
        return scr;
    }

    public void setScr(Double scr) {
        this.scr = scr;
    }

    @Column(name = "TCR")
    public Double getTcr() {
        return tcr;
    }

    public void setTcr(Double tcr) {
        this.tcr = tcr;
    }

    @Column(name = "KV_CODE", length = 200)
    public String getKvCode() {
        return kvCode;
    }

    public void setKvCode(String kvCode) {
        this.kvCode = kvCode;
    }

    @Column(name = "PROV_CODE", length = 200)
    public String getProvCode() {
        return provCode;
    }

    public void setProvCode(String provCode) {
        this.provCode = provCode;
    }

    @Column(name = "TU_NON_HR", length = 22)
    public Long getTuNonHr() {
        return tuNonHr;
    }

    public void setTuNonHr(Long tuNonHr) {
        this.tuNonHr = tuNonHr;
    }

    @Column(name = "TRX", length = 22)
    public Long getTrx() {
        return trx;
    }

    public void setTrx(Long trx) {
        this.trx = trx;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "CELL_CODE", length = 200)
    public String getCellCode() {
        return cellCode;
    }

    public void setCellCode(String cellCode) {
        this.cellCode = cellCode;
    }

    @Column(name = "RNC_BSC", length = 200)
    public String getRncBsc() {
        return rncBsc;
    }

    public void setRncBsc(String rncBsc) {
        this.rncBsc = rncBsc;
    }

    @Column(name = "COUNT_SCR", length = 20)
    public Long getCountScr() {
        return countScr;
    }

    public void setCountScr(Long countScr) {
        this.countScr = countScr;
    }

    @Column(name = "COUNT_TCR", length = 20)
    public Long getCountTcr() {
        return countTcr;
    }

    public void setCountTcr(Long countTcr) {
        this.countTcr = countTcr;
    }

    @Column(name = "TYPE", length = 20)
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Column(name = "DT_STATUS", length = 20)
    public Long getDtStatus() {
        return dtStatus;
    }

    public void setDtStatus(Long dtStatus) {
        this.dtStatus = dtStatus;
    }
}
