package com.viettel.model;

// Generated Sep 8, 2016 5:07:30 PM by Hibernate Tools 4.0.0

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ActionOfFlow generated by hbm2java
 */
@Entity
@Table(name = "ACTION_OF_FLOW")
public class ActionOfFlow implements java.io.Serializable, Cloneable {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private Long stepNum;
    private FlowTemplates flowTemplates;
    //private ActionOfFlow actionOfFlow;
    private Long previousStep;
    private Action action;
    private String ifValue;
    //private List<ActionOfFlow> actionOfFlows = new ArrayList<ActionOfFlow>(0);
    private Long stepNumberLabel; // tuong ung voi truong previous step
    private String groupActionName;
    private Long cloneType;
    private Long groupActionOrder;
    private Long isRollback = 0L;
    private Long delayTime;
    private List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<NodeRunGroupAction>(0);

    private Integer indexParamValue = 0;
    private Integer totalIndexParamvalue = 1;
    private Integer actionFlowStatus; // khai bao de hien thi trang thai chay cua action

    /*
     * hanhnv68 20161025 khai bao them cac bien de hien thi tren giao dien action of flow
     */
    private List<Long> actionFlowIds; // danh sach action of flow id
    private List<List<NodeRunGroupAction>> lstNodeRunGroupAction = new ArrayList<>();
    private List<String> preStepsNumber;
    //	private String[] preStepsNumber; // danh sach id action of flow phia truoc ma action se thuc hien tiep theo
    private String preStepsNumberLabel; // hien thi cac dau viec truoc tren giao dien
    private String preStepsCondition; // danh sach dieu kien de thuc hien action cua cac previous step
    private boolean rollbackStatus;
    private List<String> ifValues;
    private boolean actionManual;
    private Long actionType = 0L;
    //20170824_hienhv4_add_start
    private Long removeDuplicate = 1l;
    private Long cloneCmdMap = 0l;
    private boolean removeDuplicateStatus;
    private boolean cloneCmdMapStatus;
    private boolean decisionResultCloneStatus;
    private Long decisionResultClone;
    //20170824_hienhv4_add_end

    //20171026_hienhv4_bo sung group order_start
    private Long groupActionOrder2;
    //20171026_hienhv4_bo sung group order_end

    //2018/01/11 Gan action fail khi co 1 lenh fail(Khong chay action clone nua) Stop luon start
    private Boolean isStopCmdFail = false;
    //2018/01/11 Gan action fail khi co 1 lenh fail(Khong chay action clone nua) Stop luon mail

    public ActionOfFlow() {
    }

//	public ActionOfFlow(Long stepNum, FlowTemplates flowTemplates, Action action, String ifValue) {
//		this.stepNum = stepNum;
//		this.flowTemplates = flowTemplates;
//		this.action = action;
//		this.ifValue = ifValue;
//	}
//
//	public ActionOfFlow(Long stepNum, FlowTemplates flowTemplates, ActionOfFlow actionOfFlow, Action action, String ifValue, List<ActionOfFlow> actionOfFlows) {
//		this.stepNum = stepNum;
//		this.flowTemplates = flowTemplates;
//		this.actionOfFlow = actionOfFlow;
//		this.action = action;
//		this.ifValue = ifValue;
//		this.actionOfFlows = actionOfFlows;
//	}

    @Id
    @Column(name = "STEP_NUM", unique = true, nullable = false, precision = 22, scale = 0)
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @SequenceGenerator(name = "generator", sequenceName = "ACTION_OF_FLOW_SEQ", allocationSize = 1)
    public Long getStepNum() {
        return this.stepNum;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "FLOW_TEMPLATES_ID", nullable = false)
    public FlowTemplates getFlowTemplates() {
        return this.flowTemplates;
    }

    //	@ManyToOne(fetch = FetchType.EAGER)
//	@LazyCollection(LazyCollectionOption.EXTRA)
//	@JoinColumn(name = "PREVIOUS_STEP")
//	public ActionOfFlow getActionOfFlow() {
//		return this.actionOfFlow;
//	}
//
//	public void setActionOfFlow(ActionOfFlow actionOfFlow) {
//		this.actionOfFlow = actionOfFlow;
//	}
    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "ACTION_ID", nullable = false)
    public Action getAction() {
        return this.action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    //    public void setActionOfFlow(ActionOfFlow actionOfFlow) {
//        this.actionOfFlow = actionOfFlow;
//    }
    @Column(name = "IF_VALUE", nullable = false, length = 200)
    public String getIfValue() {
        return this.ifValue;
    }

    //	@OneToMany(fetch = FetchType.EAGER, mappedBy = "actionOfFlow")
//	@LazyCollection(LazyCollectionOption.EXTRA)
//	public List<ActionOfFlow> getActionOfFlows() {
//		return this.actionOfFlows;
//	}
//
//	public void setActionOfFlows(List<ActionOfFlow> actionOfFlows) {
//		this.actionOfFlows = actionOfFlows;
//	}
    @Column(name = "STEP_NUMBER_LABEL", nullable = false, precision = 6, scale = 0)
    public Long getStepNumberLabel() {
        return stepNumberLabel;
    }

    public void setStepNumberLabel(Long stepNumberLabel) {
        this.stepNumberLabel = stepNumberLabel;
    }

    @Column(name = "PREVIOUS_STEP", nullable = false, precision = 6, scale = 0)
    public Long getPreviousStep() {
        return previousStep;
    }

    public void setPreviousStep(Long previousStep) {
        this.previousStep = previousStep;
    }

    @Column(name = "GROUP_ACTION_NAME", length = 200)
    public String getGroupActionName() {
        return groupActionName;
    }

    public void setGroupActionName(String groupActionName) {
        this.groupActionName = groupActionName;
    }

    @Column(name = "CLONE_TYPE", precision = 1, scale = 0)
    public Long getCloneType() {
        return cloneType;
    }

    public void setCloneType(Long cloneType) {
        this.cloneType = cloneType;
    }

    @Column(name = "GROUP_ACTION_ORDER")
    public Long getGroupActionOrder() {
        return groupActionOrder;
    }

    public void setGroupActionOrder(Long groupActionOrder) {
        this.groupActionOrder = groupActionOrder;
    }

    public void setStepNum(Long stepNum) {
        this.stepNum = stepNum;
    }

    public void setFlowTemplates(FlowTemplates flowTemplates) {
        this.flowTemplates = flowTemplates;
    }

    public void setIfValue(String ifValue) {
        this.ifValue = ifValue;
    }

    @Column(name = "IS_ROLLBACK", precision = 1, scale = 0)
    public Long getIsRollback() {
        return isRollback;
    }

    public void setIsRollback(Long isRollback) {
        this.isRollback = isRollback;
    }

    @Column(name = "ACTION_TYPE", precision = 1, scale = 0)
    public Long getActionType() {
        if (actionType != null) {
            return actionType;
        } else {
            return 0L;
        }
    }

    public void setActionType(Long actionType) {
        this.actionType = actionType;
    }

    @Column(name = "DELAY_TIME", precision = 5, scale = 0)
    public Long getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Long delayTime) {
        this.delayTime = delayTime;
    }

    @Column(name = "REMOVE_DUPLICATE", precision = 1, scale = 0)
    public Long getRemoveDuplicate() {
        return removeDuplicate;
    }

    public void setRemoveDuplicate(Long removeDuplicate) {
        this.removeDuplicate = removeDuplicate;
    }

    @Column(name = "CLONE_CMD_MAP", precision = 1, scale = 0)
    public Long getCloneCmdMap() {
        return cloneCmdMap;
    }

    public void setCloneCmdMap(Long cloneCmdMap) {
        this.cloneCmdMap = cloneCmdMap;
    }

    @Transient
    public boolean isDecisionResultCloneStatus() {
        return decisionResultCloneStatus;
    }

    public void setDecisionResultCloneStatus(boolean decisionResultCloneStatus) {
        this.decisionResultCloneStatus = decisionResultCloneStatus;
    }

    @Column(name = "DECISION_RESULT_CLONE", precision = 1, scale = 0)
    public Long getDecisionResultClone() {
        return decisionResultClone;
    }

    public void setDecisionResultClone(Long decisionResultClone) {
        this.decisionResultClone = decisionResultClone;
    }

    @Transient
    public boolean isRemoveDuplicateStatus() {
        return removeDuplicateStatus;
    }

    public void setRemoveDuplicateStatus(boolean removeDuplicateStatus) {
        this.removeDuplicateStatus = removeDuplicateStatus;
    }

    @Transient
    public boolean isCloneCmdMapStatus() {
        return cloneCmdMapStatus;
    }

    public void setCloneCmdMapStatus(boolean cloneCmdMapStatus) {
        this.cloneCmdMapStatus = cloneCmdMapStatus;
    }

    // hanhnv68 add 20160913
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((stepNum == null) ? 0 : stepNum.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ActionOfFlow other = (ActionOfFlow) obj;
        if (stepNum == null) {
            if (other.stepNum != null) {
                return false;
            }
        } else if (!stepNum.equals(other.stepNum)) {
            return false;
        }
        return true;
    }
    // end hanhnv68 add 20160913

    //// huynx6 added Sep 15, 2016
//	@Fetch(FetchMode.JOIN)
//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "actionOfFlow")
//	@LazyCollection(LazyCollectionOption.EXTRA)
    @Transient
    public List<NodeRunGroupAction> getNodeRunGroupActions() {
        return this.nodeRunGroupActions;
    }

    public void setNodeRunGroupActions(List<NodeRunGroupAction> nodeRunGroupActions) {
        this.nodeRunGroupActions = nodeRunGroupActions;
    }

    public boolean isNoCommand(Node node) {
        if (action != null) {
//			if(action.getName().equalsIgnoreCase("T_Khai bao MTU trên interface"))
//				System.err.println();
            for (ActionDetail actionDetail : action.getActionDetails()) {
                if (actionDetail.getNodeType().equals(node.getNodeType())
                        && actionDetail.getVendor().equals(node.getVendor())
                        && actionDetail.getVersion().equals(node.getVersion())) {
                    for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                        if (//actionCommand.getCommandDetail().getNodeType().equals(node.getNodeType()) &&
                                actionCommand.getCommandDetail().getVendor().equals(node.getVendor())
                                        && actionCommand.getCommandDetail().getVersion().equals(node.getVersion())) {
                            if (actionCommand.getCommandDetail().getCommandTelnetParser() != null) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @Transient
    public Integer getIndexParamValue() {
        return indexParamValue;
    }

    public void setIndexParamValue(Integer indexParamValue) {
        this.indexParamValue = indexParamValue;
    }

    @Transient
    public Integer getTotalIndexParamvalue() {
        return totalIndexParamvalue;
    }

    public void setTotalIndexParamvalue(Integer totalIndexParamvalue) {
        this.totalIndexParamvalue = totalIndexParamvalue;
    }

    @Transient
    public Integer getActionFlowStatus() {
        return actionFlowStatus;
    }

    public void setActionFlowStatus(Integer actionFlowStatus) {
        this.actionFlowStatus = actionFlowStatus;
    }

    @Override
    public String toString() {
        return "ActionOfFlow [" + (action != null ? "action=" + action.getName() + ", " : "") + (stepNumberLabel != null ? "stepNumberLabel=" + stepNumberLabel + ", " : "")
                + (groupActionName != null ? "groupActionName=" + groupActionName + ", " : "") + (indexParamValue != null ? "indexParamValue=" + indexParamValue : "") + "]";
    }

    public String buildCommand(Map<String, List<ParamValue>> mapParamValue, String subFlowRun,
                               boolean isStyle, Node node, Long status, Integer groupIndex, boolean isClone) {
        if (isClone) {
            return buildCommandNew(mapParamValue.get(subFlowRun + "#" + node.getNodeCode()), isStyle, node, status, groupIndex);
        } else {
            return buildCommand(mapParamValue.get(subFlowRun + "#" + node.getNodeCode()), isStyle, node, status);
        }
    }

    public String buildCommand(List<ParamValue> paramValues, boolean isStyle, Node node, Long status) {
//		<c:if test="#{actionDetail.vendor==node.vendor and actionDetail.nodeType==node.nodeType and actionDetail.version==node.version }">
        String rs = "";
        for (ActionDetail actionDetail : action.getActionDetails()) {
            if (actionDetail.getVendor().equals(node.getVendor())
                    && actionDetail.getVersion().equals(node.getVersion())
                    && actionDetail.getNodeType().equals(node.getNodeType())) {
                rs += actionDetail.buildCommand(paramValues, isStyle, this, status);
            }
        }
//		System.err.println(node.getNodeCode() + ": " + rs);
        return rs;

    }

    public String buildCommandNew(List<ParamValue> paramValues, boolean isStyle, Node node, Long status, Integer groupIndex) {
        String rs = "";
        for (ActionDetail actionDetail : action.getActionDetails()) {
            if (actionDetail.getVendor().equals(node.getVendor())
                    && actionDetail.getVersion().equals(node.getVersion())
                    && actionDetail.getNodeType().equals(node.getNodeType())) {
                rs += actionDetail.buildCommandNew(paramValues, isStyle, this, status, groupIndex);
            }
        }
        return rs;
    }

    public ActionOfFlow clone() {
        ActionOfFlow actionOfFlow = new ActionOfFlow();
        actionOfFlow.setGroupActionName(groupActionName);
        actionOfFlow.setStepNum(stepNum);
        actionOfFlow.setStepNumberLabel(stepNumberLabel);
        actionOfFlow.setPreviousStep(previousStep);
        actionOfFlow.setAction(action);
        return actionOfFlow;
    }

    @Transient
    public String getPreStepsCondition() {
        return preStepsCondition;
    }

    public void setPreStepsCondition(String preStepsCondition) {
        this.preStepsCondition = preStepsCondition;
    }

    @Transient
    public List<String> getPreStepsNumber() {
        return preStepsNumber;
    }

    public void setPreStepsNumber(List<String> preStepsNumber) {
        this.preStepsNumber = preStepsNumber;
    }

    @Transient
    public boolean isRollbackStatus() {
        return rollbackStatus;
    }

    public void setRollbackStatus(boolean rollbackStatus) {
        this.rollbackStatus = rollbackStatus;
    }

    @Transient
    public boolean isActionManual() {
        return actionManual;
    }

    public void setActionManual(boolean actionManual) {
        this.actionManual = actionManual;
    }

    @Transient
    public String getPreStepsNumberLabel() {
        return preStepsNumberLabel;
    }

    public void setPreStepsNumberLabel(String preStepsNumberLabel) {
        this.preStepsNumberLabel = preStepsNumberLabel;
    }

    @Transient
    public List<Long> getActionFlowIds() {
        return actionFlowIds;
    }

    public void setActionFlowIds(List<Long> actionFlowIds) {
        this.actionFlowIds = actionFlowIds;
    }

    @Transient
    public List<List<NodeRunGroupAction>> getLstNodeRunGroupAction() {
        return lstNodeRunGroupAction;
    }

    public void setLstNodeRunGroupAction(List<List<NodeRunGroupAction>> lstNodeRunGroupAction) {
        this.lstNodeRunGroupAction = lstNodeRunGroupAction;
    }

    @Transient
    public List<String> getIfValues() {
        return ifValues;
    }

    public void setIfValues(List<String> ifValues) {
        this.ifValues = ifValues;
    }

    public ActionOfFlow deepClone() {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeUnshared(this);
            oos.flush();
            oos.reset();
            oos.close();
            System.out.println("baos.toByteArray(): " + baos.toByteArray().length);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            baos.reset();
            bais.close();
            return (ActionOfFlow) ois.readObject();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return null;
        }
    }

    public ActionOfFlow cloneToExport() {
        ActionOfFlow clone = new ActionOfFlow();
        clone.setAction(action.cloneToExport());
        clone.setStepNum(stepNum);
        return clone;
    }

    //20171026_hienhv4_bo sung group order_start
    @Transient
    public Long getGroupActionOrder2() {
        return groupActionOrder2;
    }

    public void setGroupActionOrder2(Long groupActionOrder2) {
        this.groupActionOrder2 = groupActionOrder2;
    }
    //20171026_hienhv4_bo sung group order_end

    @Column(name = "IS_STOP_CMD_FAIL", nullable = false, precision = 1, scale = 0)

    public Boolean getStopCmdFail() {
        return isStopCmdFail;
    }

    public void setStopCmdFail(Boolean stopCmdFail) {
        isStopCmdFail = stopCmdFail;
    }
}
