/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_SG_RC_PARAM_LINK_SGSN")
public class RiSgRcParamLinkSgsn implements Serializable {

    private String ipTwo;
    private Long profilePgi;
    private Long papu;
    private Long port;
    private Long id;
    private String ipOne;
    private Date updateTime;
    private String nodeCode;
    private String vrfname;

    @Column(name = "VRFNAME")
    public String getVrfname() {
        return vrfname;
    }

    public void setVrfname(String vrfname) {
        this.vrfname = vrfname;
    }

    @Column(name = "IP_TWO", length = 20)
    public String getIpTwo() {
        return ipTwo;
    }

    public void setIpTwo(String ipTwo) {
        this.ipTwo = ipTwo;
    }

    @Column(name = "PROFILE_PGI", precision = 22, scale = 0)
    public Long getProfilePgi() {
        return profilePgi;
    }

    public void setProfilePgi(Long profilePgi) {
        this.profilePgi = profilePgi;
    }

    @Column(name = "PAPU", precision = 22, scale = 0)
    public Long getPapu() {
        return papu;
    }

    public void setPapu(Long papu) {
        this.papu = papu;
    }

    @Column(name = "PORT", precision = 22, scale = 0)
    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_LINK_SGSN_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "IP_ONE", length = 200)
    public String getIpOne() {
        return ipOne;
    }

    public void setIpOne(String ipOne) {
        this.ipOne = ipOne;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "NODE_CODE", length = 200)
    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    public RiSgRcParamLinkSgsn() {
    }

    public RiSgRcParamLinkSgsn(Long id) {
        this.id = id;
    }
}
