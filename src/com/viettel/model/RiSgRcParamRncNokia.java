/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_SG_RC_PARAM_RNC_NOKIA")
public class RiSgRcParamRncNokia implements Serializable {

    private String rncPort;
    private String rncIp2;
    private String rncIp1;
    private String remoteAsId;
    private String assocId;
    private String nodeAssoc;
    private String localAsName;
    private String assocciationNameIndentify;
    private String signallingLink;
    private String listSignallingLink;
    private String signallingLinkName;
    private String pointCodeId;
    private String remoteAsName;
    private String concernSsnName;
    private String sgsnPort;
    private Long id;
    private String nodeCode;
    private String icsu;
    private String subSystemId;
    private Date updateTime;
    private String sgsnIp2;
    private String sgsnIp1;
    private String affectSsnName;
    private String subSystemName;
    private String assocciationSetName;
    private String pointCodeName;
    private String localClientPort;
    private String concernPointCodeName;
    private String cpnpgepIp1;
    private String cpnpgepIp2;

    private String listRemoteAsId;
    private String listAssocId;
    private String listPointCodeId;
    private String listSubSystemId;
    private String ipbrId;
    private String sgsnName;

    @Column(name = "SGSN_NAME", length = 200)
    public String getSgsnName() {
        return sgsnName;
    }

    public void setSgsnName(String sgsnName) {
        this.sgsnName = sgsnName;
    }

    @Column(name = "LIST_REMOTE_AS_ID", length = 200)
    public String getListRemoteAsId() {
        return listRemoteAsId;
    }

    public void setListRemoteAsId(String listRemoteAsId) {
        this.listRemoteAsId = listRemoteAsId;
    }

    @Column(name = "LIST_ASSOC_ID", length = 200)
    public String getListAssocId() {
        return listAssocId;
    }

    public void setListAssocId(String listAssocId) {
        this.listAssocId = listAssocId;
    }

    @Column(name = "LIST_POINT_CODE_ID", length = 200)
    public String getListPointCodeId() {
        return listPointCodeId;
    }

    public void setListPointCodeId(String listPointCodeId) {
        this.listPointCodeId = listPointCodeId;
    }

    @Column(name = "LIST_SUB_SYSTEM_ID", length = 200)
    public String getListSubSystemId() {
        return listSubSystemId;
    }

    public void setListSubSystemId(String listSubSystemId) {
        this.listSubSystemId = listSubSystemId;
    }

    @Column(name = "IPBR_ID", length = 200)
    public String getIpbrId() {
        return ipbrId;
    }

    public void setIpbrId(String ipbrId) {
        this.ipbrId = ipbrId;
    }

    @Column(name = "RNC_PORT", length = 20)
    public String getRncPort() {
        return rncPort;
    }

    public void setRncPort(String rncPort) {
        this.rncPort = rncPort;
    }

    @Column(name = "RNC_IP2", length = 200)
    public String getRncIp2() {
        return rncIp2;
    }

    public void setRncIp2(String rncIp2) {
        this.rncIp2 = rncIp2;
    }

    @Column(name = "RNC_IP1", length = 200)
    public String getRncIp1() {
        return rncIp1;
    }

    public void setRncIp1(String rncIp1) {
        this.rncIp1 = rncIp1;
    }

    @Column(name = "REMOTE_AS_ID", length = 200)
    public String getRemoteAsId() {
        return remoteAsId;
    }

    public void setRemoteAsId(String remoteAsId) {
        this.remoteAsId = remoteAsId;
    }

    @Column(name = "ASSOC_ID", length = 200)
    public String getAssocId() {
        return assocId;
    }

    public void setAssocId(String assocId) {
        this.assocId = assocId;
    }

    @Column(name = "NODE_ASSOC", length = 200)
    public String getNodeAssoc() {
        return nodeAssoc;
    }

    public void setNodeAssoc(String nodeAssoc) {
        this.nodeAssoc = nodeAssoc;
    }

    @Column(name = "LOCAL_AS_NAME", length = 200)
    public String getLocalAsName() {
        return localAsName;
    }

    public void setLocalAsName(String localAsName) {
        this.localAsName = localAsName;
    }

    @Column(name = "ASSOCCIATION_NAME_INDENTIFY", length = 500)
    public String getAssocciationNameIndentify() {
        return assocciationNameIndentify;
    }

    public void setAssocciationNameIndentify(String assocciationNameIndentify) {
        this.assocciationNameIndentify = assocciationNameIndentify;
    }

    @Column(name = "SIGNALLING_LINK", length = 500)
    public String getSignallingLink() {
        return signallingLink;
    }

    public void setSignallingLink(String signallingLink) {
        this.signallingLink = signallingLink;
    }

    @Column(name = "LIST_SIGNALLING_LINK", length = 500)

    public String getListSignallingLink() {
        return listSignallingLink;
    }

    public void setListSignallingLink(String listSignallingLink) {
        this.listSignallingLink = listSignallingLink;
    }

    @Column(name = "SIGNALLING_LINK_NAME", length = 500)
    public String getSignallingLinkName() {
        return signallingLinkName;
    }

    public void setSignallingLinkName(String signallingLinkName) {
        this.signallingLinkName = signallingLinkName;
    }

    @Column(name = "POINT_CODE_ID", length = 200)
    public String getPointCodeId() {
        return pointCodeId;
    }

    public void setPointCodeId(String pointCodeId) {
        this.pointCodeId = pointCodeId;
    }

    @Column(name = "REMOTE_AS_NAME", length = 200)
    public String getRemoteAsName() {
        return remoteAsName;
    }

    public void setRemoteAsName(String remoteAsName) {
        this.remoteAsName = remoteAsName;
    }

    @Column(name = "CONCERN_SSN_NAME", length = 200)
    public String getConcernSsnName() {
        return concernSsnName;
    }

    public void setConcernSsnName(String concernSsnName) {
        this.concernSsnName = concernSsnName;
    }

    @Column(name = "SGSN_PORT", length = 20)
    public String getSgsnPort() {
        return sgsnPort;
    }

    public void setSgsnPort(String sgsnPort) {
        this.sgsnPort = sgsnPort;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_RNC_NOKIA_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NODE_CODE", length = 200)
    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    @Column(name = "ICSU", length = 200)
    public String getIcsu() {
        return icsu;
    }

    public void setIcsu(String icsu) {
        this.icsu = icsu;
    }

    @Column(name = "SUB_SYSTEM_ID", length = 200)
    public String getSubSystemId() {
        return subSystemId;
    }

    public void setSubSystemId(String subSystemId) {
        this.subSystemId = subSystemId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "SGSN_IP2", length = 200)
    public String getSgsnIp2() {
        return sgsnIp2;
    }

    public void setSgsnIp2(String sgsnIp2) {
        this.sgsnIp2 = sgsnIp2;
    }

    @Column(name = "SGSN_IP1", length = 200)
    public String getSgsnIp1() {
        return sgsnIp1;
    }

    public void setSgsnIp1(String sgsnIp1) {
        this.sgsnIp1 = sgsnIp1;
    }

    @Column(name = "AFFECT_SSN_NAME", length = 100)
    public String getAffectSsnName() {
        return affectSsnName;
    }

    public void setAffectSsnName(String affectSsnName) {
        this.affectSsnName = affectSsnName;
    }

    @Column(name = "SUB_SYSTEM_NAME", length = 200)
    public String getSubSystemName() {
        return subSystemName;
    }

    public void setSubSystemName(String subSystemName) {
        this.subSystemName = subSystemName;
    }

    @Column(name = "ASSOCCIATION_SET_NAME", length = 200)
    public String getAssocciationSetName() {
        return assocciationSetName;
    }

    public void setAssocciationSetName(String assocciationSetName) {
        this.assocciationSetName = assocciationSetName;
    }

    @Column(name = "POINT_CODE_NAME", length = 200)
    public String getPointCodeName() {
        return pointCodeName;
    }

    public void setPointCodeName(String pointCodeName) {
        this.pointCodeName = pointCodeName;
    }

    @Column(name = "LOCAL_CLIENT_PORT", length = 200)
    public String getLocalClientPort() {
        return localClientPort;
    }

    public void setLocalClientPort(String localClientPort) {
        this.localClientPort = localClientPort;
    }

    @Column(name = "CONCERN_POINT_CODE_NAME", length = 200)
    public String getConcernPointCodeName() {
        return concernPointCodeName;
    }

    public void setConcernPointCodeName(String concernPointCodeName) {
        this.concernPointCodeName = concernPointCodeName;
    }

    @Column(name = "CPNPGEP_IP1", length = 200)
    public String getCpnpgepIp1() {
        return cpnpgepIp1;
    }

    public void setCpnpgepIp1(String cpnpgepIp1) {
        this.cpnpgepIp1 = cpnpgepIp1;
    }

    @Column(name = "CPNPGEP_IP2", length = 200)
    public String getCpnpgepIp2() {
        return cpnpgepIp2;
    }

    public void setCpnpgepIp2(String cpnpgepIp2) {
        this.cpnpgepIp2 = cpnpgepIp2;
    }

    public RiSgRcParamRncNokia() {
    }

    public RiSgRcParamRncNokia(Long id) {
        this.id = id;
    }
}
