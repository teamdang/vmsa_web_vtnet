/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author quytv7
 */
@Entity
@Table(name = "RI_SG_RC_PARAM_UNIQUE")
public class RiSgRcParamUnique implements java.io.Serializable {

    private Long id;
    private String rncCode;
    private String paramCode;
    private String paramValue;
    private Date updateTime;
    private String sgsnCode;
    private Long riSgRcPlanId;
    private Long flowRunActionId;

    public RiSgRcParamUnique() {
    }

    public RiSgRcParamUnique(String rncCode, String paramCode, String paramValue, 
            Date updateTime, String sgsnCode, Long riSgRcPlanId) {
        this.rncCode = rncCode;
        this.paramCode = paramCode;
        this.paramValue = paramValue;
        this.updateTime = updateTime;
        this.sgsnCode = sgsnCode;
        this.riSgRcPlanId = riSgRcPlanId;
    }

    public RiSgRcParamUnique(Long id) {
        this.id = id;
    }

    @Column(name = "RNC_CODE", length = 200)
    public String getRncCode() {
        return rncCode;
    }

    public void setRncCode(String rncCode) {
        this.rncCode = rncCode;
    }

    @Column(name = "PARAM_CODE", length = 200)
    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    @Column(name = "PARAM_VALUE", length = 500)
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_TIME", nullable = false, length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "SGSN_CODE", length = 500)
    public String getSgsnCode() {
        return sgsnCode;
    }

    public void setSgsnCode(String sgsnCode) {
        this.sgsnCode = sgsnCode;
    }

    @Column(name = "RI_SG_RC_PLAN_ID", length = 20)
    public Long getRiSgRcPlanId() {
        return riSgRcPlanId;
    }

    public void setRiSgRcPlanId(Long riSgRcPlanId) {
        this.riSgRcPlanId = riSgRcPlanId;
    }

    @Column(name = "FLOW_RUN_ACTION_ID", length = 20)
    public Long getFlowRunActionId() {
        return flowRunActionId;
    }

    public void setFlowRunActionId(Long flowRunActionId) {
        this.flowRunActionId = flowRunActionId;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_UNIQUE_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 20, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
