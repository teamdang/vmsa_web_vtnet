package com.viettel.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "STATION_DETAIL")
public class StationDetail implements Serializable {

    private String cellCode;
    private StationResource stationResource;
    private Long cellId;
    private StationPlan stationPlan;
    private String stationCode;
    private Date updateTime;
    private String rncBsc;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLAN_ID")
    public StationPlan getStationPlan() {
        return stationPlan;
    }

    public void setStationPlan(StationPlan stationPlan) {
        this.stationPlan = stationPlan;
    }

    @Column(name = "CELL_CODE", length = 200)
    public String getCellCode() {
        return cellCode;
    }

    public void setCellCode(String cellCode) {
        this.cellCode = cellCode;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RESOURCE_ID")
    public StationResource getStationResource() {
        return stationResource;
    }

    public void setStationResource(StationResource stationResource) {
        this.stationResource = stationResource;
    }

    @SequenceGenerator(name = "generator", sequenceName = "STATION_DETAIL_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "CELL_ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getCellId() {
        return cellId;
    }

    public void setCellId(Long cellId) {
        this.cellId = cellId;
    }

    @Column(name = "STATION_CODE", length = 200)
    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "RNC_BSC", length = 200)
    public String getRncBsc() {
        return rncBsc;
    }

    public void setRncBsc(String rncBsc) {
        this.rncBsc = rncBsc;
    }

    public StationDetail() {
    }

    public StationDetail(Long cellId) {
        this.cellId = cellId;
    }
}
