/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

import java.io.Serializable;

/**
 *
 * @author quytv7
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "STATION_CONFIG_IMPORT")
public class StationConfigImport implements Serializable {

    private String vendorName;
    private String networkType;
    private Long isOblige;
    private Long isRule;
    private Long id;
    private String fileName;
    private Long templateId;
    private String sheetName;
    private String bscType;
    private String btsType;
    private String deleteType;
    private Long isValidate;
    private String businessCode;
    private String classController;
    private String functionValidate;
    private String functionCreateDt;
    private String columnValidate;
    private String classNimsWs;
    private String functionNimsWs;
    private Long isValidateNims; //0: Khong validate exist cell NIMS, 1: Co
    private Long isUpdateNims; // 0: Khong update trang thai cell sang NIMS; 1: co
    private Long templateRollbackId;

    @Column(name = "TEMPLATE_ROLLBACK_ID", precision = 22, scale = 0)
    public Long getTemplateRollbackId() {
        return templateRollbackId;
    }

    public void setTemplateRollbackId(Long templateRollbackId) {
        this.templateRollbackId = templateRollbackId;
    }

    @Column(name = "IS_UPDATE_NIMS", precision = 22, scale = 0)
    public Long getIsUpdateNims() {
        return isUpdateNims;
    }

    public void setIsUpdateNims(Long isUpdateNims) {
        this.isUpdateNims = isUpdateNims;
    }

    @Column(name = "IS_VALIDATE_NIMS", precision = 22, scale = 0)
    public Long getIsValidateNims() {
        return isValidateNims;
    }

    public void setIsValidateNims(Long isValidateNims) {
        this.isValidateNims = isValidateNims;
    }

    @Column(name = "IS_VALIDATE", precision = 22, scale = 0)
    public Long getIsValidate() {
        return isValidate;
    }

    public void setIsValidate(Long isValidate) {
        this.isValidate = isValidate;
    }

    @Column(name = "BUSINESS_CODE", length = 200)
    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    @Column(name = "CLASS_CONTROLLER", length = 200)
    public String getClassController() {
        return classController;
    }

    public void setClassController(String classController) {
        this.classController = classController;
    }

    @Column(name = "FUNCTION_VALIDATE", length = 200)
    public String getFunctionValidate() {
        return functionValidate;
    }

    public void setFunctionValidate(String functionValidate) {
        this.functionValidate = functionValidate;
    }

    @Column(name = "FUNCTION_CREATE_DT", length = 200)
    public String getFunctionCreateDt() {
        return functionCreateDt;
    }

    public void setFunctionCreateDt(String functionCreateDt) {
        this.functionCreateDt = functionCreateDt;
    }

    @Column(name = "DELETE_TYPE", length = 200)
    public String getDeleteType() {
        return deleteType;
    }

    public void setDeleteType(String deleteType) {
        this.deleteType = deleteType;
    }

    @Column(name = "BSC_TYPE", length = 200)
    public String getBscType() {
        return bscType;
    }

    public void setBscType(String bscType) {
        this.bscType = bscType;
    }

    @Column(name = "BTS_TYPE", length = 200)
    public String getBtsType() {
        return btsType;
    }

    public void setBtsType(String btsType) {
        this.btsType = btsType;
    }

    @Column(name = "VENDOR_NAME", length = 200)
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Column(name = "NETWORK_TYPE", length = 50)
    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    @Column(name = "IS_OBLIGE", precision = 22, scale = 0)
    public Long getIsOblige() {
        return isOblige;
    }

    public void setIsOblige(Long isOblige) {
        this.isOblige = isOblige;
    }

    @Column(name = "IS_RULE", precision = 22, scale = 0)
    public Long getIsRule() {
        return isRule;
    }

    public void setIsRule(Long isRule) {
        this.isRule = isRule;
    }

    @SequenceGenerator(name = "generator", sequenceName = "STATION_CONFIG_IMPORT_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "FILE_NAME", length = 200)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Column(name = "TEMPLATE_ID", precision = 22, scale = 0)
    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    @Column(name = "SHEET_NAME", length = 200)
    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    @Column(name = "COLUMN_VALIDATE", length = 200)
    public String getColumnValidate() {
        return columnValidate;
    }

    public void setColumnValidate(String columnValidate) {
        this.columnValidate = columnValidate;
    }

    @Column(name = "CLASS_NIMS_WS", length = 200)
    public String getClassNimsWs() {
        return classNimsWs;
    }

    public void setClassNimsWs(String classNimsWs) {
        this.classNimsWs = classNimsWs;
    }

    @Column(name = "FUNCTION_NIMS_WS", length = 200)
    public String getFunctionNimsWs() {
        return functionNimsWs;
    }

    public void setFunctionNimsWs(String functionNimsWs) {
        this.functionNimsWs = functionNimsWs;
    }


    public StationConfigImport() {
    }
}
