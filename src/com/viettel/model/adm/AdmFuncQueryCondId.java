package com.viettel.model.adm;
// Generated Mar 10, 2011 9:50:44 AM by Hibernate Tools 3.2.1.GA

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AdmFuncQueryCondId generated by hbm2java
 */
@Embeddable
public class AdmFuncQueryCondId implements java.io.Serializable {

    private String functionCode;
    private Long queryId;
    private Long controlId;

    public AdmFuncQueryCondId() {
    }

    public AdmFuncQueryCondId(String functionCode, Long queryId, Long controlId) {
        this.functionCode = functionCode;
        this.queryId = queryId;
        this.controlId = controlId;
    }

    @Column(name = "FUNCTION_CODE", nullable = false, length = 64)
    public String getFunctionCode() {
        return this.functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    @Column(name = "QUERY_ID", nullable = false, precision = 10, scale = 0)
    public Long getQueryId() {
        return this.queryId;
    }

    public void setQueryId(Long queryId) {
        this.queryId = queryId;
    }

    @Column(name = "CONTROL_ID", nullable = false, precision = 10, scale = 0)
    public Long getControlId() {
        return this.controlId;
    }

    public void setControlId(Long controlId) {
        this.controlId = controlId;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
}
