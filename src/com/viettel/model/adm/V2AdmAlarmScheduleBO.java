/* 
 * Copyright 2011 Viettel Telecom. All rights reserved. 
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms. 
 */
package com.viettel.model.adm;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "V2_ADM_ALARM_SCHEDULE")
/**
 *
 * @author: ThuanNHT
 * @version: 1.0
 * @since: 1.0
 */
public class V2AdmAlarmScheduleBO implements Serializable {

    private Long repeatNumber;
    private String functionCode;
    private Date lastRunTime;
    private Long id;
    private Date endDate;
    private Date startDate;
    private Long status;
    private Long repeatTime;

    private String runCycle;

    @Transient
    public String getRunCycle() {
        runCycle = repeatNumber 
                + " " 
                + (repeatTime == null 
                    ? "" 
                    : (repeatTime.equals(1L) 
                        ? (repeatNumber > 1 ? "hours" : "hour") 
                        : (repeatNumber> 1 ? "days" : "day")));
        return runCycle;
    }

    public void setRunCycle(String runCycle) {
        this.runCycle = runCycle;
    }
    
    
    @Column(name = "REPEAT_NUMBER", precision = 22, scale = 0)
    public Long getRepeatNumber() {
        return repeatNumber;
    }

    public void setRepeatNumber(Long repeatNumber) {
        this.repeatNumber = repeatNumber;
    }

    @Column(name = "FUNCTION_CODE", length = 200)
    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_RUN_TIME", length = 7)
    public Date getLastRunTime() {
        return lastRunTime;
    }

    public void setLastRunTime(Date lastRunTime) {
        this.lastRunTime = lastRunTime;
    }

    @SequenceGenerator(name = "generator", sequenceName = "V2_ADM_ALARM_SCHEDULE_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_DATE", length = 7)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endTime) {
        this.endDate = endTime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START_DATE", length = 7)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startTime) {
        this.startDate = startTime;
    }

    @Column(name = "STATUS", precision = 22, scale = 0)
    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    @Column(name = "REPEAT_TIME", precision = 22, scale = 0)
    public Long getRepeatTime() {
        return repeatTime;
    }

    public void setRepeatTime(Long repeatTime) {
        this.repeatTime = repeatTime;
    }

    public V2AdmAlarmScheduleBO() {
    }

    public V2AdmAlarmScheduleBO(Long id) {
        this.id = id;
    }

    public V2AdmAlarmScheduleBO(Long repeatNumber, String functionCode, Date lastRunTime, Long id, Date endTime, Date startTime, Long status, Long repeatTime) {
        this.repeatNumber = repeatNumber;
        this.functionCode = functionCode;
        this.lastRunTime = lastRunTime;
        this.id = id;
        this.endDate = endTime;
        this.startDate = startTime;
        this.status = status;
        this.repeatTime = repeatTime;
    }

    public V2AdmAlarmScheduleBO cloneObject() {
        return new V2AdmAlarmScheduleBO(repeatNumber, functionCode, lastRunTime, id, endDate, startDate, status, repeatTime);
    }
    
    
    
}
