package com.viettel.model.adm;
// Generated Apr 19, 2011 1:47:25 PM by Hibernate Tools 3.2.1.GA

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * V2AdmFuncQueryColCalBO generated by hbm2java
 */
@Entity
@Table(name = "V2_ADM_FUNC_QUERY_COL_CAL")
public class V2AdmFuncQueryColCalBO implements java.io.Serializable {

    private AdmFuncQueryColCalId id;
    private String columnName;
    private Long columnIndex;
    private String columnStyle;
    private String chartIndex;
    private String kpiColumnCode;
    private Long compareWithControl;
    private String compareWithValue;
    private String valueColumnCode;
    private String columnNameL1;
    private String columnNameL2;
    private Long isDisplay;

    public V2AdmFuncQueryColCalBO() {
    }

    public V2AdmFuncQueryColCalBO(AdmFuncQueryColCalId id) {
        this.id = id;
    }

    public V2AdmFuncQueryColCalBO(AdmFuncQueryColCalId id, String columnName, Long columnIndex, String columnStyle) {
        this.id = id;
        this.columnName = columnName;
        this.columnIndex = columnIndex;
        this.columnStyle = columnStyle;
    }

    public V2AdmFuncQueryColCalBO(AdmFuncQueryColCalId id, String columnName, Long columnIndex, String columnStyle, String chartIndex, String kpiColumnCode, Long compareWithControl, String compareWithValue, String valueColumnCode, String columnNameL1, String columnNameL2, Long isDisplay) {
        this.id = id;
        this.columnName = columnName;
        this.columnIndex = columnIndex;
        this.columnStyle = columnStyle;
        this.chartIndex = chartIndex;
        this.kpiColumnCode = kpiColumnCode;
        this.compareWithControl = compareWithControl;
        this.compareWithValue = compareWithValue;
        this.valueColumnCode = valueColumnCode;
        this.columnNameL1 = columnNameL1;
        this.columnNameL2 = columnNameL2;
        this.isDisplay = isDisplay;
    }

    @AttributeOverrides({
        @AttributeOverride(name = "functionCode", column =
        @Column(name = "FUNCTION_CODE", nullable = false, length = 64)),
        @AttributeOverride(name = "queryId", column =
        @Column(name = "QUERY_ID", nullable = false, precision = 10, scale = 0)),
        @AttributeOverride(name = "columnCode", column =
        @Column(name = "COLUMN_CODE", nullable = false, length = 256)),
        @AttributeOverride(name = "calType", column =
        @Column(name = "CAL_TYPE", nullable = false, length = 64))})
    @EmbeddedId
    public AdmFuncQueryColCalId getId() {
        return this.id;
    }

    public void setId(AdmFuncQueryColCalId id) {
        this.id = id;
    }

    @Column(name = "COLUMN_NAME", length = 256)
    public String getColumnName() {
        return this.columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    @Column(name = "COLUMN_INDEX", precision = 10, scale = 0)
    public Long getColumnIndex() {
        return this.columnIndex;
    }

    public void setColumnIndex(Long columnIndex) {
        this.columnIndex = columnIndex;
    }

    @Column(name = "COLUMN_STYLE", length = 64)
    public String getColumnStyle() {
        return this.columnStyle;
    }

    public void setColumnStyle(String columnStyle) {
        this.columnStyle = columnStyle;
    }

    @Column(name = "CHART_INDEX", precision = 10, scale = 0)
    public String getChartIndex() {
        return chartIndex;
    }

    public void setChartIndex(String chartIndex) {
        this.chartIndex = chartIndex;
    }

    @Column(name = "COMPARE_WITH_CONTROL", precision = 10, scale = 0)
    public Long getCompareWithControl() {
        return compareWithControl;
    }

    public void setCompareWithControl(Long compareWithControl) {
        this.compareWithControl = compareWithControl;
    }

    @Column(name = "COMPARE_WITH_VALUE", length = 64)
    public String getCompareWithValue() {
        return compareWithValue;
    }

    public void setCompareWithValue(String compareWithValue) {
        this.compareWithValue = compareWithValue;
    }

    @Column(name = "KPI_COLUMN_CODE", length = 256)
    public String getKpiColumnCode() {
        return kpiColumnCode;
    }

    public void setKpiColumnCode(String kpiColumnCode) {
        this.kpiColumnCode = kpiColumnCode;
    }

    @Column(name = "VALUE_COLUMN_CODE", length = 256)
    public String getValueColumnCode() {
        return valueColumnCode;
    }

    public void setValueColumnCode(String valueColumnCode) {
        this.valueColumnCode = valueColumnCode;
    }

    @Column(name = "COLUMN_NAME_L1", length = 256)
    public String getColumnNameL1() {
        return columnNameL1;
    }

    public void setColumnNameL1(String columnNameL1) {
        this.columnNameL1 = columnNameL1;
    }

    @Column(name = "COLUMN_NAME_L2", length = 256)
    public String getColumnNameL2() {
        return columnNameL2;
    }

    public void setColumnNameL2(String columnNameL2) {
        this.columnNameL2 = columnNameL2;
    }

    @Column(name = "IS_DISPLAY", precision = 10, scale = 0)
    public Long getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(Long isDisplay) {
        this.isDisplay = isDisplay;
    }
}
