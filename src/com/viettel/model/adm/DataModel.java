/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model.adm;

import java.util.LinkedList;

/**
 *
 * @author vipc
 */
public class DataModel {

    private Object field1;
    private Object field2;
    private Object field3;
    private Object field4;
    private Object field5;
    private Object field6;
    private Object field7;
    private Object field8;
    private Object field9;
    private Object field10;
    private Object field11;
    private Object field12;
    private Object field13;
    private Object field14;
    private Object field15;
    private Object field16;
    private Object field17;
    private Object field18;
    private Object field19;
    private Object field20;
    private Object field21;
    private Object field22;
    private Object field23;
    private Object field24;
    private Object field25;
    private Object field26;
    private Object field27;
    private Object field28;
    private Object field29;
    private Object field30;
    private Object field31;
    private Object field32;
    private Object field33;
    private Object field34;
    private Object field35;
    private Object field36;
    private Object field37;
    private Object field38;
    private Object field39;
    private Object field40;
    private Object field41;
    private Object field42;
    private Object field43;
    private Object field44;
    private Object field45;
    private Object field46;
    private Object field47;
    private Object field48;
    private Object field49;
    private Object field50;
    private Object field51;
    private Object field52;
    private Object field53;
    private Object field54;
    private Object field55;
    private Object field56;
    private Object field57;
    private Object field58;
    private Object field59;
    private Object field60;
    private Object field61;
    private Object field62;
    private Object field63;
    private Object field64;
    private Object field65;
    private Object field66;
    private Object field67;
    private Object field68;
    private Object field69;
    private Object field70;
    private Object field71;
    private Object field72;
    private Object field73;
    private Object field74;
    private Object field75;
    private Object field76;
    private Object field77;
    private Object field78;
    private Object field79;
    private Object field80;
    private Object field81;
    private Object field82;
    private Object field83;
    private Object field84;
    private Object field85;
    private Object field86;
    private Object field87;
    private Object field88;
    private Object field89;
    private Object field90;
    private Object field91;
    private Object field92;
    private Object field93;
    private Object field94;
    private Object field95;
    private Object field96;
    private Object field97;
    private Object field98;
    private Object field99;
    private Object field100;
    private LinkedList<Object> listData;

    public Object getField1() {
        return field1;
    }

    public void setField1(Object field1) {
        this.field1 = field1;
    }

    public Object getField2() {
        return field2;
    }

    public void setField2(Object field2) {
        this.field2 = field2;
    }

    public Object getField3() {
        return field3;
    }

    public void setField3(Object field3) {
        this.field3 = field3;
    }

    public Object getField4() {
        return field4;
    }

    public void setField4(Object field4) {
        this.field4 = field4;
    }

    public Object getField5() {
        return field5;
    }

    public void setField5(Object field5) {
        this.field5 = field5;
    }

    public Object getField6() {
        return field6;
    }

    public void setField6(Object field6) {
        this.field6 = field6;
    }

    public Object getField7() {
        return field7;
    }

    public void setField7(Object field7) {
        this.field7 = field7;
    }

    public Object getField8() {
        return field8;
    }

    public void setField8(Object field8) {
        this.field8 = field8;
    }

    public Object getField9() {
        return field9;
    }

    public void setField9(Object field9) {
        this.field9 = field9;
    }

    public Object getField10() {
        return field10;
    }

    public void setField10(Object field10) {
        this.field10 = field10;
    }

    public Object getField11() {
        return field11;
    }

    public void setField11(Object field11) {
        this.field11 = field11;
    }

    public Object getField12() {
        return field12;
    }

    public void setField12(Object field12) {
        this.field12 = field12;
    }

    public Object getField13() {
        return field13;
    }

    public void setField13(Object field13) {
        this.field13 = field13;
    }

    public Object getField14() {
        return field14;
    }

    public void setField14(Object field14) {
        this.field14 = field14;
    }

    public Object getField15() {
        return field15;
    }

    public void setField15(Object field15) {
        this.field15 = field15;
    }

    public Object getField16() {
        return field16;
    }

    public void setField16(Object field16) {
        this.field16 = field16;
    }

    public Object getField17() {
        return field17;
    }

    public void setField17(Object field17) {
        this.field17 = field17;
    }

    public Object getField18() {
        return field18;
    }

    public void setField18(Object field18) {
        this.field18 = field18;
    }

    public Object getField19() {
        return field19;
    }

    public void setField19(Object field19) {
        this.field19 = field19;
    }

    public Object getField20() {
        return field20;
    }

    public void setField20(Object field20) {
        this.field20 = field20;
    }

    public Object getField21() {
        return field21;
    }

    public void setField21(Object field21) {
        this.field21 = field21;
    }

    public Object getField22() {
        return field22;
    }

    public void setField22(Object field22) {
        this.field22 = field22;
    }

    public Object getField23() {
        return field23;
    }

    public void setField23(Object field23) {
        this.field23 = field23;
    }

    public Object getField24() {
        return field24;
    }

    public void setField24(Object field24) {
        this.field24 = field24;
    }

    public Object getField25() {
        return field25;
    }

    public void setField25(Object field25) {
        this.field25 = field25;
    }

    public Object getField26() {
        return field26;
    }

    public void setField26(Object field26) {
        this.field26 = field26;
    }

    public Object getField27() {
        return field27;
    }

    public void setField27(Object field27) {
        this.field27 = field27;
    }

    public Object getField28() {
        return field28;
    }

    public void setField28(Object field28) {
        this.field28 = field28;
    }

    public Object getField29() {
        return field29;
    }

    public void setField29(Object field29) {
        this.field29 = field29;
    }

    public Object getField30() {
        return field30;
    }

    public void setField30(Object field30) {
        this.field30 = field30;
    }

    public Object getField31() {
        return field31;
    }

    public void setField31(Object field31) {
        this.field31 = field31;
    }

    public Object getField32() {
        return field32;
    }

    public void setField32(Object field32) {
        this.field32 = field32;
    }

    public Object getField33() {
        return field33;
    }

    public void setField33(Object field33) {
        this.field33 = field33;
    }

    public Object getField34() {
        return field34;
    }

    public void setField34(Object field34) {
        this.field34 = field34;
    }

    public Object getField35() {
        return field35;
    }

    public void setField35(Object field35) {
        this.field35 = field35;
    }

    public Object getField36() {
        return field36;
    }

    public void setField36(Object field36) {
        this.field36 = field36;
    }

    public Object getField37() {
        return field37;
    }

    public void setField37(Object field37) {
        this.field37 = field37;
    }

    public Object getField38() {
        return field38;
    }

    public void setField38(Object field38) {
        this.field38 = field38;
    }

    public Object getField39() {
        return field39;
    }

    public void setField39(Object field39) {
        this.field39 = field39;
    }

    public Object getField40() {
        return field40;
    }

    public void setField40(Object field40) {
        this.field40 = field40;
    }

    public Object getField41() {
        return field41;
    }

    public void setField41(Object field41) {
        this.field41 = field41;
    }

    public Object getField42() {
        return field42;
    }

    public void setField42(Object field42) {
        this.field42 = field42;
    }

    public Object getField43() {
        return field43;
    }

    public void setField43(Object field43) {
        this.field43 = field43;
    }

    public Object getField44() {
        return field44;
    }

    public void setField44(Object field44) {
        this.field44 = field44;
    }

    public Object getField45() {
        return field45;
    }

    public void setField45(Object field45) {
        this.field45 = field45;
    }

    public Object getField46() {
        return field46;
    }

    public void setField46(Object field46) {
        this.field46 = field46;
    }

    public Object getField47() {
        return field47;
    }

    public void setField47(Object field47) {
        this.field47 = field47;
    }

    public Object getField48() {
        return field48;
    }

    public void setField48(Object field48) {
        this.field48 = field48;
    }

    public Object getField49() {
        return field49;
    }

    public void setField49(Object field49) {
        this.field49 = field49;
    }

    public Object getField50() {
        return field50;
    }

    public void setField50(Object field50) {
        this.field50 = field50;
    }

    public Object getField51() {
        return field51;
    }

    public void setField51(Object field51) {
        this.field51 = field51;
    }

    public Object getField52() {
        return field52;
    }

    public void setField52(Object field52) {
        this.field52 = field52;
    }

    public Object getField53() {
        return field53;
    }

    public void setField53(Object field53) {
        this.field53 = field53;
    }

    public Object getField54() {
        return field54;
    }

    public void setField54(Object field54) {
        this.field54 = field54;
    }

    public Object getField55() {
        return field55;
    }

    public void setField55(Object field55) {
        this.field55 = field55;
    }

    public Object getField56() {
        return field56;
    }

    public void setField56(Object field56) {
        this.field56 = field56;
    }

    public Object getField57() {
        return field57;
    }

    public void setField57(Object field57) {
        this.field57 = field57;
    }

    public Object getField58() {
        return field58;
    }

    public void setField58(Object field58) {
        this.field58 = field58;
    }

    public Object getField59() {
        return field59;
    }

    public void setField59(Object field59) {
        this.field59 = field59;
    }

    public Object getField60() {
        return field60;
    }

    public void setField60(Object field60) {
        this.field60 = field60;
    }

    public Object getField61() {
        return field61;
    }

    public void setField61(Object field61) {
        this.field61 = field61;
    }

    public Object getField62() {
        return field62;
    }

    public void setField62(Object field62) {
        this.field62 = field62;
    }

    public Object getField63() {
        return field63;
    }

    public void setField63(Object field63) {
        this.field63 = field63;
    }

    public Object getField64() {
        return field64;
    }

    public void setField64(Object field64) {
        this.field64 = field64;
    }

    public Object getField65() {
        return field65;
    }

    public void setField65(Object field65) {
        this.field65 = field65;
    }

    public Object getField66() {
        return field66;
    }

    public void setField66(Object field66) {
        this.field66 = field66;
    }

    public Object getField67() {
        return field67;
    }

    public void setField67(Object field67) {
        this.field67 = field67;
    }

    public Object getField68() {
        return field68;
    }

    public void setField68(Object field68) {
        this.field68 = field68;
    }

    public Object getField69() {
        return field69;
    }

    public void setField69(Object field69) {
        this.field69 = field69;
    }

    public Object getField70() {
        return field70;
    }

    public void setField70(Object field70) {
        this.field70 = field70;
    }

    public Object getField71() {
        return field71;
    }

    public void setField71(Object field71) {
        this.field71 = field71;
    }

    public Object getField72() {
        return field72;
    }

    public void setField72(Object field72) {
        this.field72 = field72;
    }

    public Object getField73() {
        return field73;
    }

    public void setField73(Object field73) {
        this.field73 = field73;
    }

    public Object getField74() {
        return field74;
    }

    public void setField74(Object field74) {
        this.field74 = field74;
    }

    public Object getField75() {
        return field75;
    }

    public void setField75(Object field75) {
        this.field75 = field75;
    }

    public Object getField76() {
        return field76;
    }

    public void setField76(Object field76) {
        this.field76 = field76;
    }

    public Object getField77() {
        return field77;
    }

    public void setField77(Object field77) {
        this.field77 = field77;
    }

    public Object getField78() {
        return field78;
    }

    public void setField78(Object field78) {
        this.field78 = field78;
    }

    public Object getField79() {
        return field79;
    }

    public void setField79(Object field79) {
        this.field79 = field79;
    }

    public Object getField80() {
        return field80;
    }

    public void setField80(Object field80) {
        this.field80 = field80;
    }

    public Object getField81() {
        return field81;
    }

    public void setField81(Object field81) {
        this.field81 = field81;
    }

    public Object getField82() {
        return field82;
    }

    public void setField82(Object field82) {
        this.field82 = field82;
    }

    public Object getField83() {
        return field83;
    }

    public void setField83(Object field83) {
        this.field83 = field83;
    }

    public Object getField84() {
        return field84;
    }

    public void setField84(Object field84) {
        this.field84 = field84;
    }

    public Object getField85() {
        return field85;
    }

    public void setField85(Object field85) {
        this.field85 = field85;
    }

    public Object getField86() {
        return field86;
    }

    public void setField86(Object field86) {
        this.field86 = field86;
    }

    public Object getField87() {
        return field87;
    }

    public void setField87(Object field87) {
        this.field87 = field87;
    }

    public Object getField88() {
        return field88;
    }

    public void setField88(Object field88) {
        this.field88 = field88;
    }

    public Object getField89() {
        return field89;
    }

    public void setField89(Object field89) {
        this.field89 = field89;
    }

    public Object getField90() {
        return field90;
    }

    public void setField90(Object field90) {
        this.field90 = field90;
    }

    public Object getField91() {
        return field91;
    }

    public void setField91(Object field91) {
        this.field91 = field91;
    }

    public Object getField92() {
        return field92;
    }

    public void setField92(Object field92) {
        this.field92 = field92;
    }

    public Object getField93() {
        return field93;
    }

    public void setField93(Object field93) {
        this.field93 = field93;
    }

    public Object getField94() {
        return field94;
    }

    public void setField94(Object field94) {
        this.field94 = field94;
    }

    public Object getField95() {
        return field95;
    }

    public void setField95(Object field95) {
        this.field95 = field95;
    }

    public Object getField96() {
        return field96;
    }

    public void setField96(Object field96) {
        this.field96 = field96;
    }

    public Object getField97() {
        return field97;
    }

    public void setField97(Object field97) {
        this.field97 = field97;
    }

    public Object getField98() {
        return field98;
    }

    public void setField98(Object field98) {
        this.field98 = field98;
    }

    public Object getField99() {
        return field99;
    }

    public void setField99(Object field99) {
        this.field99 = field99;
    }

    public Object getField100() {
        return field100;
    }

    public void setField100(Object field100) {
        this.field100 = field100;
    }


    public LinkedList<Object> getListData() {
        return listData;
    }

    public void setListData(LinkedList<Object> listData) {
        this.listData = listData;
    }
}
