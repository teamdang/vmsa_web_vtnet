/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model.adm;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author qlmvt_dongnd3
 */
public class AdmFuncQueryUnMapBO {

    private V2AdmFuncQueryBO query;
    private List<V2AdmFuncQueryColumnBO> columns = new ArrayList<>();
    private List<AdmFuncQueryCondUnMap> conditions = new ArrayList<>();
    private List<String> columnType = new ArrayList<>();
    private List<V2AdmFuncQueryColCalBO> colCals = new ArrayList<>();
    private List<V2AdmFuncQueryColGroupBO> colGroups = new ArrayList<>();
    private List<V2AdmFuncQueryButtonBO> buttons = new ArrayList<>();
    private boolean hasCheckBox;
    private int rows = 20;
    private int page = 1;
    private List<DataModel> datas = new ArrayList<>();
    private int totalRows = 0;
    private int totalPages = 0;

    public int getTotalPages() {
        totalPages = (totalRows / rows) + (totalRows % rows != 0 ? 1 : 0);
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<DataModel> getDatas() {
        return datas;
    }

    public void setDatas(List<DataModel> datas) {
        this.datas = datas;
    }

    public boolean isHasCheckBox() {
        return hasCheckBox;
    }

    public void setHasCheckBox(boolean hasCheckBox) {
        this.hasCheckBox = hasCheckBox;
    }

    public AdmFuncQueryUnMapBO() {
    }

    public AdmFuncQueryUnMapBO(V2AdmFuncQueryBO query) {
        this.query = query;
    }

    public AdmFuncQueryUnMapBO(V2AdmFuncQueryBO query, List<V2AdmFuncQueryColumnBO> column, List<AdmFuncQueryCondUnMap> condition) {
        this.query = query;
        this.columns = column;
        this.conditions = condition;
    }

    public List<String> getColumnType() {
        return columnType;
    }

    public void setColumnType(List<String> columnType) {
        this.columnType = columnType;
    }

    public List<V2AdmFuncQueryColumnBO> getColumns() {
        return columns;
    }

    public void setColumns(List<V2AdmFuncQueryColumnBO> columns) {
        this.columns = columns;
    }

    public List<AdmFuncQueryCondUnMap> getConditions() {
        return conditions;
    }

    public void setConditions(List<AdmFuncQueryCondUnMap> conditions) {
        this.conditions = conditions;
    }

    public V2AdmFuncQueryBO getQuery() {
        return query;
    }

    public void setQuery(V2AdmFuncQueryBO query) {
        this.query = query;
    }

    public List<V2AdmFuncQueryColCalBO> getColCals() {
        return colCals;
    }

    public void setColCals(List<V2AdmFuncQueryColCalBO> colCals) {
        this.colCals = colCals;
    }

    public List<V2AdmFuncQueryButtonBO> getButtons() {
        return buttons;
    }

    public void setButtons(List<V2AdmFuncQueryButtonBO> buttons) {
        this.buttons = buttons;
    }

    public List<V2AdmFuncQueryColGroupBO> getColGroups() {
        return colGroups;
    }

    public void setColGroups(List<V2AdmFuncQueryColGroupBO> colGroups) {
        this.colGroups = colGroups;
    }

}
