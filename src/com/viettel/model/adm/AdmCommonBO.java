/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model.adm;

/**
 *
 * @author qlmvt_dongnd3
 */
public class AdmCommonBO {

    public AdmCommonBO(String value, String label) {
        this.value = value;
        this.label = label;
    }
    private String value;
    private String label;

    public String getLabel() {
        return label;
    }

    public AdmCommonBO() {
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
