package com.viettel.model.adm;
// Generated Mar 10, 2011 9:32:20 AM by Hibernate Tools 3.2.1.GA

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * AdmFuncQueryId generated by hbm2java
 */
@Embeddable
public class AdmFuncQueryId implements java.io.Serializable {

    private Long queryId;
    private String functionCode;

    public AdmFuncQueryId() {
    }

    public AdmFuncQueryId(Long queryId, String functionCode) {
        this.queryId = queryId;
        this.functionCode = functionCode;
    }

    @Column(name = "QUERY_ID", nullable = false, precision = 10, scale = 0)
    public Long getQueryId() {
        return this.queryId;
    }

    public void setQueryId(Long queryId) {
        this.queryId = queryId;
    }

    @Column(name = "FUNCTION_CODE", nullable = false, length = 64)
    public String getFunctionCode() {
        return this.functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
}
