package com.viettel.model.adm;
// Generated Apr 15, 2011 2:18:56 PM by Hibernate Tools 3.2.1.GA

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * V2AdmFuncQueryCondDependBO generated by hbm2java
 */
@Entity
@Table(name = "V2_ADM_FUNC_QUERY_COND_DEPEND")
public class V2AdmFuncQueryCondDependBO implements java.io.Serializable {

    private AdmFuncQueryCondDependId id = new AdmFuncQueryCondDependId();
    private String compareType;
    private Long condDependIndex;

    public V2AdmFuncQueryCondDependBO() {
    }

    public V2AdmFuncQueryCondDependBO(AdmFuncQueryCondDependId id) {
        this.id = id;
    }

    public V2AdmFuncQueryCondDependBO(AdmFuncQueryCondDependId id, Long condDependIndex) {
        this.id = id;
        this.condDependIndex = condDependIndex;
    }
    

    @EmbeddedId
    @AttributeOverrides({
        @AttributeOverride(name = "functionCode", column
                = @Column(name = "FUNCTION_CODE", nullable = false, length = 64)),
        @AttributeOverride(name = "queryId", column
                = @Column(name = "QUERY_ID", nullable = false, precision = 10, scale = 0)),
        @AttributeOverride(name = "controlParrent", column
                = @Column(name = "CONTROL_PARRENT", nullable = false, precision = 10, scale = 0)),
        @AttributeOverride(name = "controlChild", column
                = @Column(name = "CONTROL_CHILD", nullable = false, precision = 10, scale = 0))})
    public AdmFuncQueryCondDependId getId() {
        return this.id;
    }

    public void setId(AdmFuncQueryCondDependId id) {
        this.id = id;
    }

    @Column(name = "COMPARE_TYPE")
    public String getCompareType() {
        return compareType;
    }

    public void setCompareType(String compareType) {
        this.compareType = compareType;
    }

    @Column(name = "COND_DEPEND_INDEX")
    public Long getCondDependIndex() {
        return condDependIndex;
    }

    public void setCondDependIndex(Long condDependIndex) {
        this.condDependIndex = condDependIndex;
    }
    
    public V2AdmFuncQueryCondDependBO cloneObject(){
        return new V2AdmFuncQueryCondDependBO(id, condDependIndex);
    }
}
