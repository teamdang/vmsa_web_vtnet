package com.viettel.model.adm;
// Generated Mar 10, 2011 9:32:20 AM by Hibernate Tools 3.2.1.GA

public class AdmFuncQueryColumnObj implements java.io.Serializable {

    private String columnName;
    private Long columnIndex;
    private Long isDisplay;
    private String columnStyle;
    private String chartIndex;
    private String columnNameL1;
    private String columnNameL2;
    private String formula;
    private String functionCode;
    private Long queryId;
    private String columnCode;

    public AdmFuncQueryColumnObj() {
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public Long getQueryId() {
        return queryId;
    }

    public void setQueryId(Long queryId) {
        this.queryId = queryId;
    }

    public String getColumnCode() {
        return columnCode;
    }

    public void setColumnCode(String columnCode) {
        this.columnCode = columnCode;
    }

    public String getColumnName() {
        return this.columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Long getColumnIndex() {
        return this.columnIndex;
    }

    public void setColumnIndex(Long columnIndex) {
        this.columnIndex = columnIndex;
    }

    public Long getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(Long isDisplay) {
        this.isDisplay = isDisplay;
    }

    public String getColumnStyle() {
        return columnStyle;
    }

    public void setColumnStyle(String columnStyle) {
        this.columnStyle = columnStyle;
    }

    public String getChartIndex() {
        return chartIndex;
    }

    public void setChartIndex(String chartIndex) {
        this.chartIndex = chartIndex;
    }

    public String getColumnNameL1() {
        return columnNameL1;
    }

    public void setColumnNameL1(String columnNameL1) {
        this.columnNameL1 = columnNameL1;
    }

    public String getColumnNameL2() {
        return columnNameL2;
    }

    public void setColumnNameL2(String columnNameL2) {
        this.columnNameL2 = columnNameL2;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
