/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model.adm;

/**
 *
 * @author qlmvt_dongnd3
 */
public class AdmFuncQueryOptionUnMap {

    private String optionType;
    private String functionCode;
    private String value;
    private Long queryId;
    private String optionName;
    private String optionCode;
    private Long isUsed;
    private Long optionIndex;

    public Long getOptionIndex() {
        return optionIndex;
    }

    public void setOptionIndex(Long optionIndex) {
        this.optionIndex = optionIndex;
    }

    public Long getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Long isUsed) {
        this.isUsed = isUsed;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getQueryId() {
        return queryId;
    }

    public void setQueryId(Long queryId) {
        this.queryId = queryId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionCode() {
        return optionCode;
    }

    public void setOptionCode(String optionCode) {
        this.optionCode = optionCode;
    }
}
