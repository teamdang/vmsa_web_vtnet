/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model.adm;

/**
 *
 * @author qlmvt_dongnd3
 */
public class AdmFuncQueryButtonUnMap {

    private String buttonCode;
    private String buttonName;
    private String buttonType;
    private String actionStr;
    private String numRowsGrid;
    private String icon;
    private Long btnIndex;

    public Long getBtnIndex() {
        return btnIndex;
    }

    public void setBtnIndex(Long btnIndex) {
        this.btnIndex = btnIndex;
    }

    public String getActionStr() {
        return actionStr;
    }

    public void setActionStr(String actionStr) {
        this.actionStr = actionStr;
    }

    public String getButtonCode() {
        return buttonCode;
    }

    public void setButtonCode(String buttonCode) {
        this.buttonCode = buttonCode;
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNumRowsGrid() {
        return numRowsGrid;
    }

    public void setNumRowsGrid(String numRowsGrid) {
        this.numRowsGrid = numRowsGrid;
    }
}
