/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model.adm;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author qlmvt_dongnd3
 */
public class AdmFuncQueryCondUnMap {

    private V2AdmFuncQueryCondBO condition;
    private List<V2AdmFuncQueryCondDependBO> condDepend;
    private Object valueSearch;
    private List<Object> listValueSearch = new ArrayList<>();

    public List<Object> getListValueSearch() {
        return listValueSearch;
    }

    public void setListValueSearch(List<Object> listValueSearch) {
        this.listValueSearch = listValueSearch;
    }

    public Object getValueSearch() {
        if(valueSearch != null && !"".equals(valueSearch.toString())){
            if (valueSearch instanceof String) {
                valueSearch = valueSearch.toString().trim();
            }
        }
        return valueSearch;
    }

    public void setValueSearch(Object valueSearch) {
        this.valueSearch = valueSearch;
    }

    public AdmFuncQueryCondUnMap() {
    }

    public AdmFuncQueryCondUnMap(V2AdmFuncQueryCondBO condition, List<V2AdmFuncQueryCondDependBO> condDepend) {
        this.condition = condition;
        this.condDepend = condDepend;
    }

    public List<V2AdmFuncQueryCondDependBO> getCondDepend() {
        return condDepend;
    }

    public void setCondDepend(List<V2AdmFuncQueryCondDependBO> condDepend) {
        this.condDepend = condDepend;
    }

    public V2AdmFuncQueryCondBO getCondition() {
        return condition;
    }

    public void setCondition(V2AdmFuncQueryCondBO condition) {
        this.condition = condition;
    }
}
