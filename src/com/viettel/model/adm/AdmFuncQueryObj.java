package com.viettel.model.adm;
// Generated Mar 10, 2011 9:32:20 AM by Hibernate Tools 3.2.1.GA

/**
 * AdmFuncQueryBO generated by hbm2java
 */
public class AdmFuncQueryObj implements java.io.Serializable{

    private Long queryId;
    private String functionCode;
    private String queryName;
    private String queryString;
    private Long queryIndex;
    private String queryType;
    private String chartType;
    private String gridName;
    private String isLarge;
    private String sheetName;
    private String timestenSource;

    public AdmFuncQueryObj() {
    }

    public Long getQueryId() {
        return queryId;
    }

    public void setQueryId(Long queryId) {
        this.queryId = queryId;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public Long getQueryIndex() {
        return queryIndex;
    }

    public void setQueryIndex(Long queryIndex) {
        this.queryIndex = queryIndex;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    public String getGridName() {
        return gridName;
    }

    public void setGridName(String gridName) {
        this.gridName = gridName;
    }

    public String getIsLarge() {
        return isLarge;
    }

    public void setIsLarge(String isLarge) {
        this.isLarge = isLarge;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public String getTimestenSource() {
        return timestenSource;
    }

    public void setTimestenSource(String timestenSource) {
        this.timestenSource = timestenSource;
    }
}
