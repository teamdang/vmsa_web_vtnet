/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model.adm;

import java.util.List;

/**
 *
 * @author qlmvt_dongnd3
 */
public class AdmFuncChartBO {

    private String objectName;
    private List<String> time;
    private List<String> value;
    private Long group;
    private String groupName;
    private Long isEnd;

    public Long getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(Long isEnd) {
        this.isEnd = isEnd;
    }

    public Long getGroup() {
        return group;
    }

    public void setGroup(Long group) {
        this.group = group;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
