package com.viettel.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "STATION_PARAM_DEFAULT")
public class StationParamDefault implements Serializable {

    private Long paramId;
    private String paramCode;
    private String paramDefault;
    private String paramFormula;
    private Vendor vendor;
    private Version version;
    private NodeType nodeType;
    private Long proficientType;
    @Column(name = "PROFICIENT_TYPE", length = 2)
    public Long getProficientType() {
        return proficientType;
    }

    public void setProficientType(Long proficientType) {
        this.proficientType = proficientType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VENDOR_ID")
    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VERSION_ID")
    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NODE_TYPE_ID")
    public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }

    @SequenceGenerator(name = "generator", sequenceName = "STATION_PARAM_DEFAULT_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "PARAM_ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getParamId() {
        return paramId;
    }

    public void setParamId(Long paramId) {
        this.paramId = paramId;
    }

    @Column(name = "PARAM_CODE", length = 800)
    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    @Column(name = "PARAM_DEFAULT", length = 2000)
    public String getParamDefault() {
        return paramDefault;
    }

    public void setParamDefault(String paramDefault) {
        this.paramDefault = paramDefault;
    }

    @Column(name = "PARAM_FORMULA", length = 4000)
    public String getParamFormula() {
        return paramFormula;
    }

    public void setParamFormula(String paramFormula) {
        this.paramFormula = paramFormula;
    }

    public StationParamDefault() {
    }

    public StationParamDefault(Long paramId) {
        this.paramId = paramId;
    }
}
