/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 * @author quytv7
 */

import com.viettel.util.Config;
import com.viettel.util.MessageUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "MANAGE_CREATE_DT_FROM_GNOC")
public class ManageCreateDtFromGnoc implements Serializable {

    private Long id;
    private Date updateTime;
    private Long crId;
    private String crNumber;
    private Long isCreate;
    private String userCreate;


    @Column(name = "CR_NUMBER", length = 200)
    public String getCrNumber() {
        return crNumber;
    }

    public void setCrNumber(String crNumber) {
        this.crNumber = crNumber;
    }

    @Column(name = "USER_CREATE", length = 200)
    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }


    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @SequenceGenerator(name = "generator", sequenceName = "MANAGE_CREATE_DT_FROM_GNOC_SEQ", allocationSize = 1)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "CR_ID", precision = 22, scale = 0)
    public Long getCrId() {
        return crId;
    }

    public void setCrId(Long crId) {
        this.crId = crId;
    }

    @Column(name = "IS_CREATE", precision = 22, scale = 0)
    public Long getIsCreate() {
        return isCreate;
    }

    public void setIsCreate(Long isCreate) {
        this.isCreate = isCreate;
    }

    public ManageCreateDtFromGnoc() {
    }

    public ManageCreateDtFromGnoc(Long id) {
        this.id = id;
    }
}
