package com.viettel.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "MAP_NODE")
public class MapNode implements Serializable {

    private Long id;
    private String cellCode;
    private String rncBsc;
    private String nodeb;
    private String nodebIp;
    
    public MapNode() {
    }

    public MapNode(Long id) {
        this.id = id;
    }
    
    @SequenceGenerator(name = "generator", sequenceName = "STATION_RESOURCE_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name = "CELL_CODE", length = 200)
    public String getCellCode() {
        return cellCode;
    }

    public void setCellCode(String cellCode) {
        this.cellCode = cellCode;
    }
    
    @Column(name = "RNC_BSC", length = 200)
    public String getRncBsc() {
        return rncBsc;
    }

    public void setRncBsc(String rncBsc) {
        this.rncBsc = rncBsc;
    }

    @Column(name = "NODEB", length = 200)
    public String getNodeb() {
        return nodeb;
    }

    public void setNodeb(String nodeb) {
        this.nodeb = nodeb;
    }

    @Column(name = "NODEB_IP", length = 200)
    public String getNodebIp() {
        return nodebIp;
    }

    public void setNodebIp(String nodebIp) {
        this.nodebIp = nodebIp;
    }
}
