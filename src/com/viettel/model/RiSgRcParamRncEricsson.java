/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_SG_RC_PARAM_RNC_ERICSSON")
public class RiSgRcParamRncEricsson implements Serializable {

    private String rncPort;
    private String sgsnIuIp2;
    private String rncIuIp1;
    private String sccpApLocal;
    private String sgsnIuIp1;
    private String ipAccessHostPool;
    private String sctpId;
    private Date lastUpdate;
    private String sgsnPort;
    private String sgsnName;
    private String rncIuIp2;
    private Long id;
    private String sccpApRemoteId;
    private String nodeCode;
    private String indexM3ua;
    private String dscp;

    @Column(name = "INDEX_M3UA", length = 100)
    public String getIndexM3ua() {
        return indexM3ua;
    }

    public void setIndexM3ua(String indexM3ua) {
        this.indexM3ua = indexM3ua;
    }

    @Column(name = "DSCP", length = 100)
    public String getDscp() {
        return dscp;
    }

    public void setDscp(String dscp) {
        this.dscp = dscp;
    }

    @Column(name = "RNC_PORT", length = 10)
    public String getRncPort() {
        return rncPort;
    }

    public void setRncPort(String rncPort) {
        this.rncPort = rncPort;
    }

    @Column(name = "SGSN_IU_IP2", length = 100)
    public String getSgsnIuIp2() {
        return sgsnIuIp2;
    }

    public void setSgsnIuIp2(String sgsnIuIp2) {
        this.sgsnIuIp2 = sgsnIuIp2;
    }

    @Column(name = "RNC_IU_IP1", length = 100)
    public String getRncIuIp1() {
        return rncIuIp1;
    }

    public void setRncIuIp1(String rncIuIp1) {
        this.rncIuIp1 = rncIuIp1;
    }

    @Column(name = "SCCP_AP_LOCAL", length = 200)
    public String getSccpApLocal() {
        return sccpApLocal;
    }

    public void setSccpApLocal(String sccpApLocal) {
        this.sccpApLocal = sccpApLocal;
    }

    @Column(name = "SGSN_IU_IP1", length = 100)
    public String getSgsnIuIp1() {
        return sgsnIuIp1;
    }

    public void setSgsnIuIp1(String sgsnIuIp1) {
        this.sgsnIuIp1 = sgsnIuIp1;
    }

    @Column(name = "IP_ACCESS_HOST_POOL", length = 200)
    public String getIpAccessHostPool() {
        return ipAccessHostPool;
    }

    public void setIpAccessHostPool(String ipAccessHostPool) {
        this.ipAccessHostPool = ipAccessHostPool;
    }

    @Column(name = "SCTP_ID", length = 200)
    public String getSctpId() {
        return sctpId;
    }

    public void setSctpId(String sctpId) {
        this.sctpId = sctpId;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_UPDATE", length = 7)
    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Column(name = "SGSN_PORT", length = 10)
    public String getSgsnPort() {
        return sgsnPort;
    }

    public void setSgsnPort(String sgsnPort) {
        this.sgsnPort = sgsnPort;
    }

    @Column(name = "SGSN_NAME", length = 200)
    public String getSgsnName() {
        return sgsnName;
    }

    public void setSgsnName(String sgsnName) {
        this.sgsnName = sgsnName;
    }

    @Column(name = "RNC_IU_IP2", length = 100)
    public String getRncIuIp2() {
        return rncIuIp2;
    }

    public void setRncIuIp2(String rncIuIp2) {
        this.rncIuIp2 = rncIuIp2;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_RNC_ERIC_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "SCCP_AP_REMOTE_ID", length = 200)
    public String getSccpApRemoteId() {
        return sccpApRemoteId;
    }

    public void setSccpApRemoteId(String sccpApRemoteId) {
        this.sccpApRemoteId = sccpApRemoteId;
    }

    @Column(name = "NODE_CODE", length = 200)
    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    public RiSgRcParamRncEricsson() {
    }

    public RiSgRcParamRncEricsson(Long id) {
        this.id = id;
    }
}
