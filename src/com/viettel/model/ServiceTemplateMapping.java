/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author hienhv4
 */
@Entity
@Table(name = "SERVICE_TEMPLATE_MAPPING")
public class ServiceTemplateMapping implements java.io.Serializable {

    private Long mappingId;
    private String serviceCode;
    private FlowTemplates template;
    private Vendor vendor;
    private Version version;
    private NodeType nodeType;

    @SequenceGenerator(name = "generator", sequenceName = "SERVICE_TEMPLATE_MAPPING_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "MAPPING_ID", nullable = false, precision = 22, scale = 0)
    public Long getMappingId() {
        return mappingId;
    }

    public void setMappingId(Long mappingId) {
        this.mappingId = mappingId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "TEMPLATE_ID")
    public FlowTemplates getTemplate() {
        return template;
    }

    public void setTemplate(FlowTemplates template) {
        this.template = template;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "VENDOR_ID")
    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "VERSION_ID")
    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.EXTRA)
    @JoinColumn(name = "NODE_TYPE_ID")
    public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }

    @Column(name = "SERVICE_CODE", length = 100)
    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    @Override
    public String toString() {
        return "ServiceTemplateMapping{" + "serviceCode=" + serviceCode
                + ", template=" + (template == null ? "" : template.getFlowTemplateName())
                + ", vendor=" + (vendor == null ? "" : vendor.getVendorName())
                + ", version=" + (version == null ? "" : version.getVersionName())
                + ", nodeType=" + (nodeType == null ? "" : nodeType.getTypeName()) + '}';
    }
}
