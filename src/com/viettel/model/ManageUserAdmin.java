package com.viettel.model;

import com.viettel.object.ConfigFile;

import javax.persistence.*;
import java.util.List;

/**
 * Created by xuanhuy on 6/22/2017.
 */
@Entity
@Table(name = "MANAGE_USER_ADMIN")
public class ManageUserAdmin {
    private Long id;
    private String userAdmin;


    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "USER_ADMIN")

    public String getUserAdmin() {
        return userAdmin;
    }

    public void setUserAdmin(String userAdmin) {
        this.userAdmin = userAdmin;
    }
}
