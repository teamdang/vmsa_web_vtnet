/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_MS_RC_PARAM_LAC_SAC")
public class RiMsRcParamLacSac implements Serializable {

    private String rncCode;
    private String mscActive;
    private String sac;
    private String id;
    private Date updateTime;
    private String lac;
    private String provinceLac;
    private String provinceSac;
    private String cellName;
    private String locationName;

    @Column(name = "LOCATION_NAME", length = 200)
    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    @Column(name = "CELL_NAME", length = 200)
    public String getCellName() {
        return cellName;
    }

    public void setCellName(String cellName) {
        this.cellName = cellName;
    }

    @Column(name = "PROVINCE_LAC", length = 200)
    public String getProvinceLac() {
        return provinceLac;
    }

    public void setProvinceLac(String provinceLac) {
        this.provinceLac = provinceLac;
    }

    @Column(name = "PROVINCE_SAC", length = 200)
    public String getProvinceSac() {
        return provinceSac;
    }

    public void setProvinceSac(String provinceSac) {
        this.provinceSac = provinceSac;
    }

    @Column(name = "RNC_CODE", length = 200)
    public String getRncCode() {
        return rncCode;
    }

    public void setRncCode(String rncCode) {
        this.rncCode = rncCode;
    }

    @Column(name = "MSC_ACTIVE", length = 200)
    public String getMscActive() {
        return mscActive;
    }

    public void setMscActive(String mscActive) {
        this.mscActive = mscActive;
    }

    @Column(name = "SAC", length = 20)
    public String getSac() {
        return sac;
    }

    public void setSac(String sac) {
        this.sac = sac;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_MS_RC_PARAM_LAC_RAC_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 20, scale = 0)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "LAC", length = 200)
    public String getLac() {
        return lac;
    }

    public void setLac(String lac) {
        this.lac = lac;
    }

    public RiMsRcParamLacSac() {
    }

    public RiMsRcParamLacSac(String id) {
        this.id = id;
    }
}
