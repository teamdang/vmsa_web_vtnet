/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_MS_RC_PARAM_RNC_HUAWEI")
public class RiMsRcParamRncHuawei implements Serializable {

    private String sctpLink;
    private String rncPort;
    private String cnOperatorId;
    private String subrack;
    private String cnNodeId;
    private String mscPort;
    private String mscIp2;
    private String mscIp1;
    private String dspCode;
    private String mscName;
    private Long id;
    private String nodeCode;
    private String signallingLinkSetId;
    private String signallingLinkId;
    private String rncIp1;
    private String rncIp2;
    private String gouSip;
    private Date updateTime;
    private String dspIndex;
    private String adjacentNodeId;
    private String slotCard;
    private String ipPath;
    private String destinationEntity;
    private String rncSpc;
    private String gouSubrack;
    private String gouSlot;
    private String dspName;
    private String gouSubrackDetail;
    private String gouSlotDetail;

    @Column(name = "SCTP_LINK", length = 200)
    public String getSctpLink() {
        return sctpLink;
    }

    public void setSctpLink(String sctpLink) {
        this.sctpLink = sctpLink;
    }

    @Column(name = "RNC_PORT", length = 50)
    public String getRncPort() {
        return rncPort;
    }

    public void setRncPort(String rncPort) {
        this.rncPort = rncPort;
    }

    @Column(name = "CN_OPERATOR_ID", length = 100)
    public String getCnOperatorId() {
        return cnOperatorId;
    }

    public void setCnOperatorId(String cnOperatorId) {
        this.cnOperatorId = cnOperatorId;
    }

    @Column(name = "SUBRACK", length = 200)
    public String getSubrack() {
        return subrack;
    }

    public void setSubrack(String subrack) {
        this.subrack = subrack;
    }

    @Column(name = "CN_NODE_ID", length = 100)
    public String getCnNodeId() {
        return cnNodeId;
    }

    public void setCnNodeId(String cnNodeId) {
        this.cnNodeId = cnNodeId;
    }

    @Column(name = "MSC_PORT", length = 50)
    public String getMscPort() {
        return mscPort;
    }

    public void setMscPort(String mscPort) {
        this.mscPort = mscPort;
    }

    @Column(name = "MSC_IP_2", length = 100)
    public String getMscIp2() {
        return mscIp2;
    }

    public void setMscIp2(String mscIp2) {
        this.mscIp2 = mscIp2;
    }

    @Column(name = "MSC_IP_1", length = 100)
    public String getMscIp1() {
        return mscIp1;
    }

    public void setMscIp1(String mscIp1) {
        this.mscIp1 = mscIp1;
    }

    @Column(name = "DSP_CODE", length = 200)
    public String getDspCode() {
        return dspCode;
    }

    public void setDspCode(String dspCode) {
        this.dspCode = dspCode;
    }

    @Column(name = "MSC_NAME", length = 200)
    public String getMscName() {
        return mscName;
    }

    public void setMscName(String mscName) {
        this.mscName = mscName;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_MS_RC_PARAM_RNC_HUAWEI_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "NODE_CODE", length = 200)
    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    @Column(name = "SIGNALLING_LINK_SET_ID", length = 50)
    public String getSignallingLinkSetId() {
        return signallingLinkSetId;
    }

    public void setSignallingLinkSetId(String signallingLinkSetId) {
        this.signallingLinkSetId = signallingLinkSetId;
    }

    @Column(name = "SIGNALLING_LINK_ID", length = 50)
    public String getSignallingLinkId() {
        return signallingLinkId;
    }

    public void setSignallingLinkId(String signallingLinkId) {
        this.signallingLinkId = signallingLinkId;
    }

    @Column(name = "RNC_IP_1", length = 100)
    public String getRncIp1() {
        return rncIp1;
    }

    public void setRncIp1(String rncIp1) {
        this.rncIp1 = rncIp1;
    }

    @Column(name = "RNC_IP_2", length = 100)
    public String getRncIp2() {
        return rncIp2;
    }

    public void setRncIp2(String rncIp2) {
        this.rncIp2 = rncIp2;
    }

    @Column(name = "GOU_SIP", length = 300)
    public String getGouSip() {
        return gouSip;
    }

    public void setGouSip(String gouSip) {
        this.gouSip = gouSip;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "DSP_INDEX", length = 100)
    public String getDspIndex() {
        return dspIndex;
    }

    public void setDspIndex(String dspIndex) {
        this.dspIndex = dspIndex;
    }

    @Column(name = "ADJACENT_NODE_ID", length = 50)
    public String getAdjacentNodeId() {
        return adjacentNodeId;
    }

    public void setAdjacentNodeId(String adjacentNodeId) {
        this.adjacentNodeId = adjacentNodeId;
    }

    @Column(name = "SLOT_CARD", length = 200)
    public String getSlotCard() {
        return slotCard;
    }

    public void setSlotCard(String slotCard) {
        this.slotCard = slotCard;
    }

    @Column(name = "IP_PATH", length = 300)
    public String getIpPath() {
        return ipPath;
    }

    public void setIpPath(String ipPath) {
        this.ipPath = ipPath;
    }

    @Column(name = "DESTINATION_ENTITY", length = 200)
    public String getDestinationEntity() {
        return destinationEntity;
    }

    public void setDestinationEntity(String destinationEntity) {
        this.destinationEntity = destinationEntity;
    }

    @Column(name = "RNC_SPC", length = 100)
    public String getRncSpc() {
        return rncSpc;
    }

    public void setRncSpc(String rncSpc) {
        this.rncSpc = rncSpc;
    }

    @Column(name = "GOU_SUBRACK", length = 300)
    public String getGouSubrack() {
        return gouSubrack;
    }

    public void setGouSubrack(String gouSubrack) {
        this.gouSubrack = gouSubrack;
    }

    @Column(name = "GOU_SLOT", length = 300)
    public String getGouSlot() {
        return gouSlot;
    }

    public void setGouSlot(String gouSlot) {
        this.gouSlot = gouSlot;
    }

    @Column(name = "GOU_SUBRACK_DETAIL", length = 300)
    public String getGouSubrackDetail() {
        return gouSubrackDetail;
    }

    public void setGouSubrackDetail(String gouSubrackDetail) {
        this.gouSubrackDetail = gouSubrackDetail;
    }

    @Column(name = "GOU_SLOT_DETAIL", length = 300)
    public String getGouSlotDetail() {
        return gouSlotDetail;
    }

    public void setGouSlotDetail(String gouSlotDetail) {
        this.gouSlotDetail = gouSlotDetail;
    }

    @Column(name = "DSP_NAME", length = 200)
    public String getDspName() {
        return dspName;
    }

    public void setDspName(String dspName) {
        this.dspName = dspName;
    }

    public RiMsRcParamRncHuawei() {
    }

    public RiMsRcParamRncHuawei(Long id) {
        this.id = id;
    }
}
