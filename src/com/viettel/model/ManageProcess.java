package com.viettel.model;

import com.viettel.object.ConfigFile;

import javax.persistence.*;
import java.util.List;

/**
 * Created by xuanhuy on 6/22/2017.
 */
@Entity
@Table(name = "MANAGE_PROCESS")
public class ManageProcess {
    private Long id;
    private String ipServer;
    private String baseDir;
    private String configDir;
    private String logDir;
    private String startCmd;
    private String stopCmd;
    private String restartCmd;
    private String statusCmd;
    private String processName;
    String statusLog;
    private List<ConfigFile> fileConfigs;
    private List<ConfigFile> fileLogs;

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "IP_SERVER")
    public String getIpServer() {
        return ipServer;
    }

    public void setIpServer(String ipServer) {
        this.ipServer = ipServer;
    }

    @Basic
    @Column(name = "BASE_DIR")
    public String getBaseDir() {
        return baseDir;
    }

    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
    }

    @Basic
    @Column(name = "START_CMD")
    public String getStartCmd() {
        return startCmd;
    }

    public void setStartCmd(String startCmd) {
        this.startCmd = startCmd;
    }

    @Basic
    @Column(name = "STOP_CMD")
    public String getStopCmd() {
        return stopCmd;
    }

    public void setStopCmd(String stopCmd) {
        this.stopCmd = stopCmd;
    }

    @Basic
    @Column(name = "RESTART_CMD")
    public String getRestartCmd() {
        return restartCmd;
    }

    public void setRestartCmd(String restartCmd) {
        this.restartCmd = restartCmd;
    }

    @Basic
    @Column(name = "STATUS_CMD")
    public String getStatusCmd() {
        return statusCmd;
    }

    public void setStatusCmd(String statusCmd) {
        this.statusCmd = statusCmd;
    }

    @Basic
    @Column(name = "PROCESS_NAME")
    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ManageProcess that = (ManageProcess) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (ipServer != null ? !ipServer.equals(that.ipServer) : that.ipServer != null) return false;
        if (baseDir != null ? !baseDir.equals(that.baseDir) : that.baseDir != null) return false;
        if (startCmd != null ? !startCmd.equals(that.startCmd) : that.startCmd != null) return false;
        if (stopCmd != null ? !stopCmd.equals(that.stopCmd) : that.stopCmd != null) return false;
        if (restartCmd != null ? !restartCmd.equals(that.restartCmd) : that.restartCmd != null) return false;
        if (statusCmd != null ? !statusCmd.equals(that.statusCmd) : that.statusCmd != null) return false;
        if (processName != null ? !processName.equals(that.processName) : that.processName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ipServer != null ? ipServer.hashCode() : 0);
        result = 31 * result + (baseDir != null ? baseDir.hashCode() : 0);
        result = 31 * result + (startCmd != null ? startCmd.hashCode() : 0);
        result = 31 * result + (stopCmd != null ? stopCmd.hashCode() : 0);
        result = 31 * result + (restartCmd != null ? restartCmd.hashCode() : 0);
        result = 31 * result + (statusCmd != null ? statusCmd.hashCode() : 0);
        result = 31 * result + (processName != null ? processName.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "CONFIG_DIR")
    public String getConfigDir() {
        return configDir;
    }

    public void setConfigDir(String configDir) {
        this.configDir = configDir;
    }

    @Transient
    public String getStatusLog() {
        if (statusLog==null)
            return null;
        return statusLog.substring(0,Math.min(statusLog.length(),200));
    }

    public void setStatusLog(String statusLog) {
        this.statusLog = statusLog;
    }

    @Transient
    public List<ConfigFile> getFileConfigs() {
        return fileConfigs;
    }

    public void setFileConfigs(List<ConfigFile> fileConfigs) {
        this.fileConfigs = fileConfigs;
    }

    @Column(name = "LOG_DIR")
    public String getLogDir() {
        return logDir;
    }

    public void setLogDir(String logDir) {
        this.logDir = logDir;
    }

    @Transient
    public List<ConfigFile> getFileLogs() {
        return fileLogs;
    }

    public void setFileLogs(List<ConfigFile> fileLogs) {
        this.fileLogs = fileLogs;
    }
}
