/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author quytv7
 */
@Entity
@Table(name = "RI_SG_RC_PARAM_LOG_COMMAND")
public class RiSgRcParamLogCommand implements java.io.Serializable {

    private Long id;
    private Long paramLogId;
    private String commandContent;
    private String commandName;
    private Date insertTime;
    private String paramValue;
    private String commandResult;
    private Long commandIndex;

    @Column(name = "COMMAND_INDEX", precision = 20, scale = 0)
    public Long getCommandIndex() {
        return commandIndex;
    }

    public void setCommandIndex(Long commandIndex) {
        this.commandIndex = commandIndex;
    }

    public RiSgRcParamLogCommand() {
    }

    public RiSgRcParamLogCommand(Long id) {
        this.id = id;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INSERT_TIME", nullable = false, length = 7)
    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }

    @Column(name = "PARAM_LOG_ID", precision = 20, scale = 0)
    public Long getParamLogId() {
        return paramLogId;
    }

    public void setParamLogId(Long paramLogId) {
        this.paramLogId = paramLogId;
    }

    @Column(name = "COMMAND_CONTENT")
    public String getCommandContent() {
        return commandContent;
    }

    public void setCommandContent(String commandContent) {
        this.commandContent = commandContent;
    }

    @Column(name = "COMMAND_NAME", length = 200)
    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    @Column(name = "PARAM_VALUE")
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Column(name = "COMMAND_RESULT", length = 200)
    public String getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(String commandResult) {
        this.commandResult = commandResult;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_LOG_COMMAND_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 20, scale = 0)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
