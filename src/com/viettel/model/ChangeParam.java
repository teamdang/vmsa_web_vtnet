package com.viettel.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import static javax.persistence.GenerationType.SEQUENCE;
import java.util.Date;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CHANGE_PARAM")
public class ChangeParam implements Serializable {

    private Long id;
    private String paramCode;
    private String paramValueOld;
    private ChangePlan changePlan;
    private Date updateTime;
    private String paramValueInput;
    private String paramValueNew;
    private Long paramType;
    private String rnc;
    private String nodeb;
    private String cell;
    
    public ChangeParam() {
    }

    public ChangeParam(Long id) {
        this.id = id;
    }
    
    @SequenceGenerator(name = "generator", sequenceName = "CHANGE_PARAM_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name = "PARAM_CODE", length = 2000)
    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    @Column(name = "PARAM_VALUE_OLD", length = 200)
    public String getParamValueOld() {
        return paramValueOld;
    }

    public void setParamValueOld(String paramValueOld) {
        this.paramValueOld = paramValueOld;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLAN_ID")
    public ChangePlan getChangePlan() {
        return changePlan;
    }

    public void setChangePlan(ChangePlan changePlan) {
        this.changePlan = changePlan;
    }
    
    @Temporal(TemporalType.DATE)
    @Column(name = "UPDATE_TIME", length = 7)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "PARAM_VALUE_INPUT", length = 200)
    public String getParamValueInput() {
        return paramValueInput;
    }

    public void setParamValueInput(String paramValueInput) {
        this.paramValueInput = paramValueInput;
    }

    @Column(name = "PARAM_VALUE_NEW", length = 200)
    public String getParamValueNew() {
        return paramValueNew;
    }

    public void setParamValueNew(String paramValueNew) {
        this.paramValueNew = paramValueNew;
    }

    @Column(name = "PARAM_TYPE", length = 20)
    public Long getParamType() {
        return paramType;
    }

    public void setParamType(Long paramType) {
        this.paramType = paramType;
    }

    @Column(name = "RNC", length = 200)
    public String getRnc() {
        return rnc;
    }

    public void setRnc(String rnc) {
        this.rnc = rnc;
    }

    @Column(name = "NODEB", length = 200)
    public String getNodeb() {
        return nodeb;
    }

    public void setNodeb(String nodeb) {
        this.nodeb = nodeb;
    }

    @Column(name = "CELL", length = 200)
    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }
}
