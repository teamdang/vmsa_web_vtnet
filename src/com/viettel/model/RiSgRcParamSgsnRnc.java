/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author quytv7
 */
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "RI_SG_RC_PARAM_SGSN_RNC")
public class RiSgRcParamSgsnRnc implements Serializable {

    private String rspid;
    private String scp;
    private String port;
    private String sctp;
    private String ip2;
    private String ip1;
    private String lspid;
    private Date lastUpdate;
    private String lasid;
    private Long paramId;
    private String rncName;
    private String sgsnName;
    private String rasid;

    @Column(name = "RSPID", length = 10)
    public String getRspid() {
        return rspid;
    }

    public void setRspid(String rspid) {
        this.rspid = rspid;
    }

    @Column(name = "SCP", length = 20)
    public String getScp() {
        return scp;
    }

    public void setScp(String scp) {
        this.scp = scp;
    }

    @Column(name = "PORT", length = 10)
    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @Column(name = "SCTP", length = 20)
    public String getSctp() {
        return sctp;
    }

    public void setSctp(String sctp) {
        this.sctp = sctp;
    }

    @Column(name = "IP2", length = 20)
    public String getIp2() {
        return ip2;
    }

    public void setIp2(String ip2) {
        this.ip2 = ip2;
    }

    @Column(name = "IP1", length = 20)
    public String getIp1() {
        return ip1;
    }

    public void setIp1(String ip1) {
        this.ip1 = ip1;
    }

    @Column(name = "LSPID", length = 10)
    public String getLspid() {
        return lspid;
    }

    public void setLspid(String lspid) {
        this.lspid = lspid;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_UPDATE", length = 7)
    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Column(name = "LASID", length = 10)
    public String getLasid() {
        return lasid;
    }

    public void setLasid(String lasid) {
        this.lasid = lasid;
    }

    @SequenceGenerator(name = "generator", sequenceName = "RI_SG_RC_PARAM_SGSN_RNC_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "PARAM_ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getParamId() {
        return paramId;
    }

    public void setParamId(Long paramId) {
        this.paramId = paramId;
    }

    @Column(name = "RNC_NAME", length = 100)
    public String getRncName() {
        return rncName;
    }

    public void setRncName(String rncName) {
        this.rncName = rncName;
    }

    @Column(name = "SGSN_NAME", length = 100)
    public String getSgsnName() {
        return sgsnName;
    }

    public void setSgsnName(String sgsnName) {
        this.sgsnName = sgsnName;
    }

    @Column(name = "RASID", length = 10)
    public String getRasid() {
        return rasid;
    }

    public void setRasid(String rasid) {
        this.rasid = rasid;
    }

    public RiSgRcParamSgsnRnc() {
    }

    public RiSgRcParamSgsnRnc(Long paramId) {
        this.paramId = paramId;
    }
}
