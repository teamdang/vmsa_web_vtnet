/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

/**
 *
 * @author Dunglv
 */

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "AUTO_MAN_RESET_MOP_CONFIG")
public class AutoManResetMopConfig implements Serializable {

    private Long id;
    private String vendor;
    private String type;
    private Integer resetType;
    private Integer status;

    public AutoManResetMopConfig() {
    }

    public AutoManResetMopConfig(Long id) {
        this.id = id;
    }
    
    @SequenceGenerator(name = "generator", sequenceName = "CHANGE_PLAN_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name = "VENDOR", length = 200)
    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    @Column(name = "STATUS", precision = 5, scale = 0)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setResetType(Integer resetType) {
        this.resetType = resetType;
    }

    @Column(name = "TYPE", length = 200)
    public String getType() {
        return type;
    }

    @Column(name = "RESET_TYPE", precision = 5, scale = 0)
    public Integer getResetType() {
        return resetType;
    }
}
