package com.viettel.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

/**
 * Created by hanh on 4/14/2017.
 */

@Entity
@Table(name = "PARAM_THRESHOLD")
public class ParamThresold implements java.io.Serializable {

    private Long id;
    private Long flowTemplateId;
    private Long type;
    private Long upperThreshold;
    private Long lowerThreshold;

    @SequenceGenerator(name = "generator", sequenceName = "PARAM_THRESHOLD_SEQ", allocationSize = 1)
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "FLOW_TEMPLATE_ID", precision = 22, scale = 0)
    public Long getFlowTemplateId() {
        return flowTemplateId;
    }

    public void setFlowTemplateId(Long flowTemplateId) {
        this.flowTemplateId = flowTemplateId;
    }

    @Column(name = "TYPE", precision = 22, scale = 0)
    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Column(name = "UPPER_THRESHOLD", precision = 22, scale = 0)
    public Long getUpperThreshold() {
        return upperThreshold;
    }

    public void setUpperThreshold(Long upperThreshold) {
        this.upperThreshold = upperThreshold;
    }

    @Column(name = "LOWER_THRESHOLD", precision = 22, scale = 0)
    public Long getLowerThreshold() {
        return lowerThreshold;
    }

    public void setLowerThreshold(Long lowerThreshold) {
        this.lowerThreshold = lowerThreshold;
    }
}
