package com.viettel.model;

// Generated Sep 8, 2016 5:07:30 PM by Hibernate Tools 4.0.0
import static javax.persistence.GenerationType.SEQUENCE;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import com.viettel.util.MyLinkedList;

/**
 * Action generated by hbm2java
 */
@Entity
@Table(name = "RI_INCIDENT_SOLUTION")
public class RiIncidentSolution implements java.io.Serializable {

    private Long id;
    private RiIncidentSolution incidentSolution;
    private String name;
    private String description;
    private Long TemplateId;
    private List<RiIncidentSolution> lstIncidentSolution = new ArrayList<RiIncidentSolution>(0);

    /**
     * For export
     */
    private List<String> commandExecutes = new MyLinkedList<String>();
    private List<String> commandRollbacks = new MyLinkedList<String>();
    private String nameRollback;
    /**
     * For tree
     */
    private boolean expanded = false;

    public RiIncidentSolution() {
    }

    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 12, scale = 0)
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @SequenceGenerator(name = "generator", sequenceName = "RI_INCIDENT_SOLUTION_SEQ", allocationSize = 1)
    public Long getId() {
        return this.id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARENT_ID")
    public RiIncidentSolution getIncidentSolution() {
        return this.incidentSolution;
    }

    public void setIncidentSolution(RiIncidentSolution incidentSolution) {
        this.incidentSolution = incidentSolution;
    }

    @Column(name = "NAME", length = 500)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "DESCRIPTION", length = 500)
    public String getDescription() {
        return this.description;
    }

    @Fetch(FetchMode.SELECT)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "incidentSolution")
    @LazyCollection(LazyCollectionOption.EXTRA)
    public List<RiIncidentSolution> getLstIncidentSolution() {
        return this.lstIncidentSolution;
    }

    @Transient
    public List<String> getCommandExecutes() {
        return commandExecutes;
    }

    public void setCommandExecutes(List<String> commandExecutes) {
        this.commandExecutes = commandExecutes;
    }

    @Transient
    public List<String> getCommandRollbacks() {
        return commandRollbacks;
    }

    public void setCommandRollbacks(List<String> commandRollbacks) {
        this.commandRollbacks = commandRollbacks;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//	public void setActionOfFlows(List<ActionOfFlow> actionOfFlows) {
//		this.actionOfFlows = actionOfFlows;
//	}
//
//	public void setActionDetails(List<ActionDetail> actionDetails) {
//		this.actionDetails = actionDetails;
//	}
    public void setLstIncidentSolution(List<RiIncidentSolution> lstIncidentSolution) {
        this.lstIncidentSolution = lstIncidentSolution;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RiIncidentSolution other = (RiIncidentSolution) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public int compareTo(RiIncidentSolution action) {
        return this.getId().compareTo(action.getId());
    }

//	@Override
//	public String toString() {
//		return "Action [name=" + name + ", actionDetails=" + actionDetails + "]";
//	}
    @Transient
    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    @Transient
    public String getNameRollback() {
        return nameRollback;
    }

    public void setNameRollback(String nameRollback) {
        this.nameRollback = nameRollback;
    }

    @Column(name = "TEMPLATE_ID")
    public Long getTemplateId() {
        return TemplateId;
    }

    public void setTemplateId(Long TemplateId) {
        this.TemplateId = TemplateId;
    }

}
