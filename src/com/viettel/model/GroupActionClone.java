/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.SEQUENCE;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author hienhv4
 */
@Entity
@Table(name = "GROUP_ACTION_CLONE")
public class GroupActionClone implements java.io.Serializable {

    private Long id;
    private Long flowTemplateId;
    private String groupName;
    private Long groupClone;
    private Long groupStop;
    private Boolean clone = true;
    private Boolean stop = true;

    @Id
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    @GeneratedValue(strategy = SEQUENCE, generator = "generator")
    @SequenceGenerator(name = "generator", sequenceName = "GROUP_ACTION_CLONE_SEQ", allocationSize = 1)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "FLOW_TEMPLATE_ID", nullable = false, precision = 22, scale = 0)
    public Long getFlowTemplateId() {
        return flowTemplateId;
    }

    public void setFlowTemplateId(Long flowTemplateId) {
        this.flowTemplateId = flowTemplateId;
    }

    @Column(name = "GROUP_NAME", length = 200)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Column(name = "GROUP_CLONE", nullable = false, precision = 22, scale = 0)
    public Long getGroupClone() {
        return groupClone;
    }

    public void setGroupClone(Long groupClone) {
        this.groupClone = groupClone;
    }

    @Column(name = "GROUP_STOP", nullable = false, precision = 22, scale = 0)
    public Long getGroupStop() {
        return groupStop;
    }

    public void setGroupStop(Long groupStop) {
        this.groupStop = groupStop;
    }

    @Transient
    public Boolean getClone() {
        clone = !(groupClone == null || groupClone == 0);
        return clone;
    }

    public void setClone(Boolean clone) {
        this.clone = clone;
        if (clone) {
            groupClone = 1l;
        } else {
            groupClone = 0l;
        }
    }

    @Transient
    public Boolean getStop() {
        stop = !(groupStop == null || groupStop == 0);
        return stop;
    }

    public void setStop(Boolean stop) {
        this.stop = stop;
        if (stop) {
            groupStop = 1l;
        } else {
            groupStop = 0l;
        }
    }
}
