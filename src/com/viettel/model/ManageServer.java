package com.viettel.model;

import com.viettel.passprotector.PassProtector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

/**
 * Created by xuanhuy on 6/22/2017.
 */
@Entity
@Table(name = "MANAGE_SERVER")
public class ManageServer {
    private Long id;
    private String ipServer;
    private String userServer;
    private String passServer;

    protected static final Logger logger = LoggerFactory.getLogger(ManageServer.class);

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "IP_SERVER")
    public String getIpServer() {
        return ipServer;
    }

    public void setIpServer(String ipServer) {
        this.ipServer = ipServer;
    }

    @Basic
    @Column(name = "USER_SERVER")
    public String getUserServer() {
        return userServer;
    }

    public void setUserServer(String userServer) {
        this.userServer = userServer;
    }

    @Basic
    @Column(name = "PASS_SERVER")
    public String getPassServer() {
        return passServer;
    }

    public void setPassServer(String passServer) {
        this.passServer = passServer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ManageServer that = (ManageServer) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (ipServer != null ? !ipServer.equals(that.ipServer) : that.ipServer != null) return false;
        if (userServer != null ? !userServer.equals(that.userServer) : that.userServer != null) return false;
        if (passServer != null ? !passServer.equals(that.passServer) : that.passServer != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ipServer != null ? ipServer.hashCode() : 0);
        result = 31 * result + (userServer != null ? userServer.hashCode() : 0);
        result = 31 * result + (passServer != null ? passServer.hashCode() : 0);
        return result;
    }

    @Transient
    public String getUserDecode(){
        try {
            return PassProtector.decrypt(userServer, "ipchange");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        return null;
    }
    @Transient
    public String getPassDecode(){
        try {
            return PassProtector.decrypt(passServer, "ipchange");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
        return null;
    }
}
