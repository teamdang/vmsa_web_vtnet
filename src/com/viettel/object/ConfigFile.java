package com.viettel.object;

import java.io.Serializable;

/**
 * Created by xuanhuy on 6/23/2017.
 */
public class ConfigFile implements Serializable {
    String filePath;
    String content;
    String size;
    Integer index;
    String dateChanged;


    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getDateChanged() {
        return dateChanged;
    }

    public void setDateChanged(String dateChanged) {
        this.dateChanged = dateChanged;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConfigFile that = (ConfigFile) o;

        return filePath != null ? filePath.equals(that.filePath) : that.filePath == null;
    }

    @Override
    public int hashCode() {
        return filePath != null ? filePath.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ConfigFile{" +
                "filePath='" + filePath + '\'' +
                ", content='" + content + '\'' +
                ", size='" + size + '\'' +
                ", index=" + index +
                ", dateChanged='" + dateChanged + '\'' +
                '}';
    }
}
