package com.viettel.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.viettel.model.NodeType;
import com.viettel.model.ParamValue;
import com.viettel.model.Vendor;
import com.viettel.model.Version;

public class ObjectImportDt implements Serializable {
	Vendor vendor;
	Version version;
	NodeType nodeType;
	List<String> paramNames = new ArrayList<String>();
	private List<List<Object>> paramValues = new ArrayList<>();
	private String sheetName;
	
	public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	public Version getVersion() {
		return version;
	}
	public void setVersion(Version version) {
		this.version = version;
	}
	public NodeType getNodeType() {
		return nodeType;
	}
	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}
	public List<String> getParamNames() {
		return paramNames;
	}
	public void setParamNames(List<String> paramNames) {
		this.paramNames = paramNames;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public List<List<Object>> getParamValues() {
		return paramValues;
	}
	public void setParamValues(List<List<Object>> paramValues) {
		this.paramValues = paramValues;
	}
	 
	
	
}
