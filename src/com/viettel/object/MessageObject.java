/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

import java.util.Map;

/**
 *
 * @author hienhv4
 */
public class MessageObject {
    private Long flowRunId; //ID cua flow_run_action can chay
    private String username; //username nguoi thuc hien dt
    private String account; //account dang nhap node mang
    private String password; //mat khau dang nhap node mang
    private Long flowRunLogId;
    private Long actionOfFlowIdStart;
    private Long actionOfFlowIdEnd;
    private String flowRunName;
    private String actionName;
    private int runType; //Chay tac dong hoac chay rollback
    private int errorMode; //1:Gap loi thi rollback, 2:Gap loi thi tam dung
    private int runningType; //Chay doc lap (1) hoac chay phu thuoc (2)
    private Map<String, AccountObj> mapAccount;

    public MessageObject(Long flowRunId, String username, String account, String password,
            Long flowRunLogId, String flowRunName, String actionName) {
        this.flowRunId = flowRunId;
        this.username = username;
        this.account = account;
        this.password = password;
        this.flowRunLogId = flowRunLogId;
        this.flowRunName = flowRunName;
        this.actionName = actionName;
    }

    public int getErrorMode() {
        return errorMode;
    }

    public void setErrorMode(int errorMode) {
        this.errorMode = errorMode;
    }

    public Long getActionOfFlowIdStart() {
        return actionOfFlowIdStart;
    }

    public void setActionOfFlowIdStart(Long actionOfFlowIdStart) {
        this.actionOfFlowIdStart = actionOfFlowIdStart;
    }

    public Long getActionOfFlowIdEnd() {
        return actionOfFlowIdEnd;
    }

    public void setActionOfFlowIdEnd(Long actionOfFlowIdEnd) {
        this.actionOfFlowIdEnd = actionOfFlowIdEnd;
    }

    public int getRunType() {
        return runType;
    }

    public void setRunType(int runType) {
        this.runType = runType;
    }
    
    public String getFlowRunName() {
        return flowRunName;
    }

    public void setFlowRunName(String flowRunName) {
        this.flowRunName = flowRunName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public Long getFlowRunLogId() {
        return flowRunLogId;
    }

    public void setFlowRunLogId(Long flowRunLogId) {
        this.flowRunLogId = flowRunLogId;
    }

    public Long getFlowRunId() {
        return flowRunId;
    }

    public void setFlowRunId(Long flowRunId) {
        this.flowRunId = flowRunId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Map<String, AccountObj> getMapAccount() {
        return mapAccount;
    }

    public void setMapAccount(Map<String, AccountObj> mapAccount) {
        this.mapAccount = mapAccount;
    }
    
    public int getRunningType() {
        return runningType;
    }

    public void setRunningType(int runningType) {
        this.runningType = runningType;
    }
}
