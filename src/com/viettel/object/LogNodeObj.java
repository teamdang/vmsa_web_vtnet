package com.viettel.object;

import java.util.ArrayList;
import java.util.List;
import com.viettel.model.FlowRunLogCommand;

//20171031_hienhv4_them mau sac cho node chay loi tren topo_start
public class LogNodeObj {

    private Integer status; // 1 - thanh cong; 0 - loi
    private List<FlowRunLogCommand> lstLogCmd = new ArrayList<>();

    public LogNodeObj(Integer status) {
        super();
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<FlowRunLogCommand> getLstLogCmd() {
        return lstLogCmd;
    }

    public void setLstLogCmd(List<FlowRunLogCommand> lstLogCmd) {
        this.lstLogCmd = lstLogCmd;
    }

    public enum LogNodeStatus {
        SUCESS(1), FAIL(0);

        private Integer status;

        LogNodeStatus(Integer status) {
            this.status = status;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }
    }
}
//20171031_hienhv4_them mau sac cho node chay loi tren topo_end
