/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author quytv7
 */
public class MessageObjectRescue {

    private HashMap<String, String> mapNodeCode; //Map Node can lay tham so
    private Map<String, String> mapUserName;
    private Map<String, String> mapPass;
    private Map<String, String> mapRescueId;
    private Map<String, Long> mapParamLogId;

    public Map<String, Long> getMapParamLogId() {
        return mapParamLogId;
    }

    public void setMapParamLogId(Map<String, Long> mapParamLogId) {
        this.mapParamLogId = mapParamLogId;
    }

    public Map<String, String> getMapRescueId() {
        return mapRescueId;
    }

    public void setMapRescueId(Map<String, String> mapRescueId) {
        this.mapRescueId = mapRescueId;
    }

    public HashMap<String, String> getMapNodeCode() {
        return mapNodeCode;
    }

    public void setMapNodeCode(HashMap<String, String> mapNodeCode) {
        this.mapNodeCode = mapNodeCode;
    }

    public Map<String, String> getMapUserName() {
        return mapUserName;
    }

    public void setMapUserName(Map<String, String> mapUserName) {
        this.mapUserName = mapUserName;
    }

    public Map<String, String> getMapPass() {
        return mapPass;
    }

    public void setMapPass(Map<String, String> mapPass) {
        this.mapPass = mapPass;
    }

}
