/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

/**
 *
 * @author quytv7
 */
public class StationNodeBResult {

    private String NodeB;
    private String RBSID;
    private String Config;
    private String OAM_IP_address;
    private String OAM_VLAN;
    private String Service_VLAN;
    private String cellIdentity_C1_S1;
    private String cellIdentity_C1_S2;
    private String cellIdentity_C1_S3;
    private String cellIdentity_C2_S1;
    private String cellIdentity_C2_S2;
    private String cellIdentity_C2_S3;
    private String cellIdentity_C3_S1;
    private String cellIdentity_C3_S2;
    private String cellIdentity_C3_S3;
    private String cellIdentity_C4_S1;
    private String cellIdentity_C4_S2;
    private String cellIdentity_C4_S3;
    private String RNC;

    public String getNodeB() {
        return NodeB;
    }

    public void setNodeB(String NodeB) {
        this.NodeB = NodeB;
    }

    public String getRBSID() {
        return RBSID;
    }

    public void setRBSID(String RBSID) {
        this.RBSID = RBSID;
    }

    public String getConfig() {
        return Config;
    }

    public void setConfig(String Config) {
        this.Config = Config;
    }

    public String getOAM_IP_address() {
        return OAM_IP_address;
    }

    public void setOAM_IP_address(String OAM_IP_address) {
        this.OAM_IP_address = OAM_IP_address;
    }

    public String getOAM_VLAN() {
        return OAM_VLAN;
    }

    public void setOAM_VLAN(String OAM_VLAN) {
        this.OAM_VLAN = OAM_VLAN;
    }

    public String getService_VLAN() {
        return Service_VLAN;
    }

    public void setService_VLAN(String Service_VLAN) {
        this.Service_VLAN = Service_VLAN;
    }

    public String getCellIdentity_C1_S1() {
        return cellIdentity_C1_S1;
    }

    public void setCellIdentity_C1_S1(String cellIdentity_C1_S1) {
        this.cellIdentity_C1_S1 = cellIdentity_C1_S1;
    }

    public String getCellIdentity_C1_S2() {
        return cellIdentity_C1_S2;
    }

    public void setCellIdentity_C1_S2(String cellIdentity_C1_S2) {
        this.cellIdentity_C1_S2 = cellIdentity_C1_S2;
    }

    public String getCellIdentity_C1_S3() {
        return cellIdentity_C1_S3;
    }

    public void setCellIdentity_C1_S3(String cellIdentity_C1_S3) {
        this.cellIdentity_C1_S3 = cellIdentity_C1_S3;
    }

    public String getCellIdentity_C2_S1() {
        return cellIdentity_C2_S1;
    }

    public void setCellIdentity_C2_S1(String cellIdentity_C2_S1) {
        this.cellIdentity_C2_S1 = cellIdentity_C2_S1;
    }

    public String getCellIdentity_C2_S2() {
        return cellIdentity_C2_S2;
    }

    public void setCellIdentity_C2_S2(String cellIdentity_C2_S2) {
        this.cellIdentity_C2_S2 = cellIdentity_C2_S2;
    }

    public String getCellIdentity_C2_S3() {
        return cellIdentity_C2_S3;
    }

    public void setCellIdentity_C2_S3(String cellIdentity_C2_S3) {
        this.cellIdentity_C2_S3 = cellIdentity_C2_S3;
    }

    public String getCellIdentity_C3_S1() {
        return cellIdentity_C3_S1;
    }

    public void setCellIdentity_C3_S1(String cellIdentity_C3_S1) {
        this.cellIdentity_C3_S1 = cellIdentity_C3_S1;
    }

    public String getCellIdentity_C3_S2() {
        return cellIdentity_C3_S2;
    }

    public void setCellIdentity_C3_S2(String cellIdentity_C3_S2) {
        this.cellIdentity_C3_S2 = cellIdentity_C3_S2;
    }

    public String getCellIdentity_C3_S3() {
        return cellIdentity_C3_S3;
    }

    public void setCellIdentity_C3_S3(String cellIdentity_C3_S3) {
        this.cellIdentity_C3_S3 = cellIdentity_C3_S3;
    }

    public String getCellIdentity_C4_S1() {
        return cellIdentity_C4_S1;
    }

    public void setCellIdentity_C4_S1(String cellIdentity_C4_S1) {
        this.cellIdentity_C4_S1 = cellIdentity_C4_S1;
    }

    public String getCellIdentity_C4_S2() {
        return cellIdentity_C4_S2;
    }

    public void setCellIdentity_C4_S2(String cellIdentity_C4_S2) {
        this.cellIdentity_C4_S2 = cellIdentity_C4_S2;
    }

    public String getCellIdentity_C4_S3() {
        return cellIdentity_C4_S3;
    }

    public void setCellIdentity_C4_S3(String cellIdentity_C4_S3) {
        this.cellIdentity_C4_S3 = cellIdentity_C4_S3;
    }

    public String getRNC() {
        return RNC;
    }

    public void setRNC(String RNC) {
        this.RNC = RNC;
    }

}
