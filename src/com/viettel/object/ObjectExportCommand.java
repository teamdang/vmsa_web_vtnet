/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

/**
 *
 * @author quytv7
 */
public class ObjectExportCommand implements java.io.Serializable {

    String commandName;
    String operator;
    String standarValue;
    String result;
    String actionName;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getStandarValue() {
        return standarValue;
    }

    public void setStandarValue(String standarValue) {
        this.standarValue = standarValue;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }
    
    public ObjectExportCommand clone() {
        ObjectExportCommand clone = new ObjectExportCommand();
        clone.setCommandName(commandName);
        clone.setOperator(operator);
        clone.setResult(result);
        clone.setStandarValue(standarValue);
        clone.setActionName(actionName);
        return clone;
    }

}
