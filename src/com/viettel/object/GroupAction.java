package com.viettel.object;

import java.util.LinkedList;
import java.util.List;

import com.viettel.model.Action;
import com.viettel.model.ActionCommand;
import com.viettel.model.ActionDetail;
import com.viettel.model.ActionOfFlow;
import com.viettel.model.Node;

public class GroupAction {

    String groupActionName;
    private boolean declare = true;
    private List<ActionOfFlow> actionOfFlows = new LinkedList<>();
    //20171018_hienhv4_clone dau viec_start
    private Integer cloneNumber = 0;
    private Integer cloneTotal = 1;
    private boolean clone = false;
    //20171018_hienhv4_clone dau viec_end

    public GroupAction() {
    }

    public GroupAction(String groupActionName, List<ActionOfFlow> actionOfFlows) {
        super();
        this.groupActionName = groupActionName;
        this.actionOfFlows = actionOfFlows;
    }

    public GroupAction(String groupActionName, Boolean declare) {
        super();
        this.groupActionName = groupActionName;
        this.declare = declare;
    }

    public GroupAction(String groupActionName, List<ActionOfFlow> actionOfFlows, boolean declare) {
        this.groupActionName = groupActionName;
        this.declare = declare;
        this.actionOfFlows = actionOfFlows;
    }

    //20171018_hienhv4_clone dau viec_start
    public Integer getCloneNumber() {
        return cloneNumber;
    }

    public void setCloneNumber(Integer cloneNumber) {
        this.cloneNumber = cloneNumber;
    }

    public Integer getCloneTotal() {
        return cloneTotal;
    }

    public void setCloneTotal(Integer cloneTotal) {
        this.cloneTotal = cloneTotal;
    }
    
    public boolean isClone() {
        return clone;
    }

    public void setClone(boolean clone) {
        this.clone = clone;
    }
    //20171018_hienhv4_clone dau viec_end

    public String getGroupActionName() {
        return groupActionName;
    }

    public void setGroupActionName(String groupActionName) {
        this.groupActionName = groupActionName;
    }

    public List<ActionOfFlow> getActionOfFlows() {
        return actionOfFlows;
    }

    public void setActionOfFlows(List<ActionOfFlow> actionOfFlows) {
        this.actionOfFlows = actionOfFlows;
    }

    public boolean isNoCommand(Node node) {
        for (ActionOfFlow actionOfFlow : actionOfFlows) {
            Action action = actionOfFlow.getAction();
            if (action != null) {
                for (ActionDetail actionDetail : action.getActionDetails()) {
                    if (actionDetail.getNodeType().equals(node.getNodeType())
                            && actionDetail.getVendor().equals(node.getVendor())
                            && actionDetail.getVersion().equals(node.getVersion())) {
                        for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                            if (actionCommand.getCommandDetail().getNodeType().equals(node.getNodeType())
                                    && actionCommand.getCommandDetail().getVendor().equals(node.getVendor())
                                    && actionCommand.getCommandDetail().getVersion().equals(node.getVersion())) {
                                if (actionCommand.getCommandDetail().getCommandTelnetParser() != null) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    public boolean isDeclare() {
        return declare;
    }

    public void setDeclare(boolean declare) {
        this.declare = declare;
    }

}
