/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

/**
 *
 * @author quytv7
 */
public class RiMsRcResult {

    private String RNCPort = "";
    private String RNCIuIP1 = "";
    private String RNCIuIP2 = "";

    private String MSCIuIP1 = "";
    private String MSCIuIP2 = "";
    private String MSCPort = "";

    private String icsu = "";
    private String SignallingLink = "";
    private String ListSignallingLink = "";
    private String SignallingLinkName = "";
    private String AssocciationSetName = "";

    private String GatewayCPNPGEP = "";
    private String MscIuIP = "";
    private String CPNPGEP1 = "";
    private String CPNPGEP2 = "";
    private String AssocID = "";

    private String index = "";
    private String number = "";
    //Tham so cho RNC nokia mcRNC
    private String remoteAsId = "";
    private String remoteAsName = "";
    private String localAsName = "";
    private String localClientPort = "";
    private String nodeAssoc = "";
    private String pointCodeId = "";
    private String pointCodeName = "";
    private String subSystemId = "";
    private String subSystemName = "";
    private String concernSsnName = "";
    private String affectSsnName = "";
    private String concernPointCodeName = "";
    private String listRemoteAsId = "";
    private String listAssocId = "";
    private String listPointCodeId = "";
    private String listSubSystemId = "";
    private String ipbrId = "";
    private String ipbrName = "";
    private String gateway1 = "";
    private String gateway2 = "";
    //act mcRNC
    private String actConcernPointCodeName = "";
    private String actConcernSsnName = "";
    private String actSubSystemName = "";
    private String actRemoteAsName = "";

    //Tham so cho RNC ericsson
    private String sctpid = "";
    private String IpAccessHostPool = "";
    private String SccpApLocal = "";
    private String MscSctpProfile = "";
    private String NPGEPIndex = "";
    private String UPNPGEPIndex = "";
    private String actIndexM3ua = "";
    private String dscp = "";
    private String actDscp = "";

    //Tham so cho RNC Huawei
    private String destinationEntity = "";
    private String DSPIndex = "";
    private String CNOperatorID = "";
    private String CNNodeID = "";
    private String AdjacentNodeID = "";
    private String SignallingLinkSetID = "";
    private String signallingLinkId = "";
    private String Subrack = "";
    private String slotCard = "";
    private String Sctplink = "";
    private String ipPathId = "";
    private String RncUserIp = "";
    private String GOUSubrack = "";
    private String GOUSlot = "";
    private String GOUSubrackDetail = "";
    private String GOUSlotDetail = "";
    private String NexthopIP = "";

    private String actSignallingLinkSetID = "";
    private String actSignallingLinkId = "";
    private String actDSPIndex = "";
    private String actCNOperatorID = "";
    private String actCNNodeID = "";
    private String actAdjacentNodeID = "";
    private String actDestinationEntity = "";

    //Tham so act
    private String ActSignallingLinkName = "";
    private String ActAssocciationSetName = "";
    private String ActSignallingLink = "";
    private String actRNCIuIP1 = "";
    private String actRNCIuIP2 = "";
    private String actAssocID = "";
    private String actRNCLac = "";
    private String actRNCRac = "";
    private String actRNCLasid = "";
    private String actRncSPC = "";
    private String actRNCLspid = "";
    private String actRNCRspid = "";
    private String actRNCRasid = "";

    //Tham so MSC
    private String RNCBillOFN = "";
    private String RNCLac = "";
    private String RNCSAI = "";
    private String RNCLAI = "";
    private String RNCProvinceLai = "";
    private String RNCProvinceSai = "";
    private String RNCSAIName = "";
    private String actRNCSCCP = "";
    private String actRNCSSN = "";
    private String actRNCDSP = "";
    private String actRNCOFC = "";
    private String actRNCLinkName = "";
    private String actRNCRT = "";
    private String actRNCLinkSet = "";
    private String actRNCDE = "";
    private String LocationName = "";
    private String actRNCSAI = "";
    private String actRNCLAI = "";
    private String PRIORITY = "";

    public String getPRIORITY() {
        return PRIORITY;
    }

    public void setPRIORITY(String PRIORITY) {
        this.PRIORITY = PRIORITY;
    }

    public String getActRNCSAI() {
        return actRNCSAI;
    }

    public void setActRNCSAI(String actRNCSAI) {
        this.actRNCSAI = actRNCSAI;
    }

    public String getActRNCLAI() {
        return actRNCLAI;
    }

    public void setActRNCLAI(String actRNCLAI) {
        this.actRNCLAI = actRNCLAI;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String LocationName) {
        this.LocationName = LocationName;
    }

    public String getNexthopIP() {
        return NexthopIP;
    }

    public void setNexthopIP(String NexthopIP) {
        this.NexthopIP = NexthopIP;
    }

    public String getGOUSubrackDetail() {
        return GOUSubrackDetail;
    }

    public void setGOUSubrackDetail(String GOUSubrackDetail) {
        this.GOUSubrackDetail = GOUSubrackDetail;
    }

    public String getGOUSlotDetail() {
        return GOUSlotDetail;
    }

    public void setGOUSlotDetail(String GOUSlotDetail) {
        this.GOUSlotDetail = GOUSlotDetail;
    }

    public String getGOUSubrack() {
        return GOUSubrack;
    }

    public void setGOUSubrack(String GOUSubrack) {
        this.GOUSubrack = GOUSubrack;
    }

    public String getGOUSlot() {
        return GOUSlot;
    }

    public void setGOUSlot(String GOUSlot) {
        this.GOUSlot = GOUSlot;
    }

    public String getIpPathId() {
        return ipPathId;
    }

    public void setIpPathId(String ipPathId) {
        this.ipPathId = ipPathId;
    }

    public String getRncUserIp() {
        return RncUserIp;
    }

    public void setRncUserIp(String RncUserIp) {
        this.RncUserIp = RncUserIp;
    }

    public String getActDscp() {
        return actDscp;
    }

    public void setActDscp(String actDscp) {
        this.actDscp = actDscp;
    }

    public String getActSignallingLinkSetID() {
        return actSignallingLinkSetID;
    }

    public void setActSignallingLinkSetID(String actSignallingLinkSetID) {
        this.actSignallingLinkSetID = actSignallingLinkSetID;
    }

    public String getActSignallingLinkId() {
        return actSignallingLinkId;
    }

    public void setActSignallingLinkId(String actSignallingLinkId) {
        this.actSignallingLinkId = actSignallingLinkId;
    }

    public String getActDSPIndex() {
        return actDSPIndex;
    }

    public void setActDSPIndex(String actDSPIndex) {
        this.actDSPIndex = actDSPIndex;
    }

    public String getActCNOperatorID() {
        return actCNOperatorID;
    }

    public void setActCNOperatorID(String actCNOperatorID) {
        this.actCNOperatorID = actCNOperatorID;
    }

    public String getActCNNodeID() {
        return actCNNodeID;
    }

    public void setActCNNodeID(String actCNNodeID) {
        this.actCNNodeID = actCNNodeID;
    }

    public String getActAdjacentNodeID() {
        return actAdjacentNodeID;
    }

    public void setActAdjacentNodeID(String actAdjacentNodeID) {
        this.actAdjacentNodeID = actAdjacentNodeID;
    }

    public String getActDestinationEntity() {
        return actDestinationEntity;
    }

    public void setActDestinationEntity(String actDestinationEntity) {
        this.actDestinationEntity = actDestinationEntity;
    }

    public String getActRNCDE() {
        return actRNCDE;
    }

    public void setActRNCDE(String actRNCDE) {
        this.actRNCDE = actRNCDE;
    }

    public String getActRNCLinkSet() {
        return actRNCLinkSet;
    }

    public void setActRNCLinkSet(String actRNCLinkSet) {
        this.actRNCLinkSet = actRNCLinkSet;
    }

    public String getActRNCRT() {
        return actRNCRT;
    }

    public void setActRNCRT(String actRNCRT) {
        this.actRNCRT = actRNCRT;
    }

    public String getActRNCLinkName() {
        return actRNCLinkName;
    }

    public void setActRNCLinkName(String actRNCLinkName) {
        this.actRNCLinkName = actRNCLinkName;
    }

    public String getActRNCOFC() {
        return actRNCOFC;
    }

    public void setActRNCOFC(String actRNCOFC) {
        this.actRNCOFC = actRNCOFC;
    }

    public String getActRNCDSP() {
        return actRNCDSP;
    }

    public void setActRNCDSP(String actRNCDSP) {
        this.actRNCDSP = actRNCDSP;
    }

    public String getActRNCSSN() {
        return actRNCSSN;
    }

    public void setActRNCSSN(String actRNCSSN) {
        this.actRNCSSN = actRNCSSN;
    }

    public String getActRNCSCCP() {
        return actRNCSCCP;
    }

    public void setActRNCSCCP(String actRNCSCCP) {
        this.actRNCSCCP = actRNCSCCP;
    }

    public String getRNCSAIName() {
        return RNCSAIName;
    }

    public void setRNCSAIName(String RNCSAIName) {
        this.RNCSAIName = RNCSAIName;
    }

    public String getRNCProvinceSai() {
        return RNCProvinceSai;
    }

    public void setRNCProvinceSai(String RNCProvinceSai) {
        this.RNCProvinceSai = RNCProvinceSai;
    }

    public String getRNCProvinceLai() {
        return RNCProvinceLai;
    }

    public void setRNCProvinceLai(String RNCProvinceLai) {
        this.RNCProvinceLai = RNCProvinceLai;
    }

    public String getRNCSAI() {
        return RNCSAI;
    }

    public void setRNCSAI(String RNCSAI) {
        this.RNCSAI = RNCSAI;
    }

    public String getRNCLAI() {
        return RNCLAI;
    }

    public void setRNCLAI(String RNCLAI) {
        this.RNCLAI = RNCLAI;
    }

    public String getRNCBillOFN() {
        return RNCBillOFN;
    }

    public void setRNCBillOFN(String RNCBillOFN) {
        this.RNCBillOFN = RNCBillOFN;
    }

    public String getActConcernPointCodeName() {
        return actConcernPointCodeName;
    }

    public void setActConcernPointCodeName(String actConcernPointCodeName) {
        this.actConcernPointCodeName = actConcernPointCodeName;
    }

    public String getActConcernSsnName() {
        return actConcernSsnName;
    }

    public void setActConcernSsnName(String actConcernSsnName) {
        this.actConcernSsnName = actConcernSsnName;
    }

    public String getActSubSystemName() {
        return actSubSystemName;
    }

    public void setActSubSystemName(String actSubSystemName) {
        this.actSubSystemName = actSubSystemName;
    }

    public String getActRemoteAsName() {
        return actRemoteAsName;
    }

    public void setActRemoteAsName(String actRemoteAsName) {
        this.actRemoteAsName = actRemoteAsName;
    }

    public String getIpbrName() {
        return ipbrName;
    }

    public void setIpbrName(String ipbrName) {
        this.ipbrName = ipbrName;
    }

    public String getGateway1() {
        return gateway1;
    }

    public void setGateway1(String gateway1) {
        this.gateway1 = gateway1;
    }

    public String getGateway2() {
        return gateway2;
    }

    public void setGateway2(String gateway2) {
        this.gateway2 = gateway2;
    }

    public String getListRemoteAsId() {
        return listRemoteAsId;
    }

    public void setListRemoteAsId(String listRemoteAsId) {
        this.listRemoteAsId = listRemoteAsId;
    }

    public String getListAssocId() {
        return listAssocId;
    }

    public void setListAssocId(String listAssocId) {
        this.listAssocId = listAssocId;
    }

    public String getListPointCodeId() {
        return listPointCodeId;
    }

    public void setListPointCodeId(String listPointCodeId) {
        this.listPointCodeId = listPointCodeId;
    }

    public String getListSubSystemId() {
        return listSubSystemId;
    }

    public void setListSubSystemId(String listSubSystemId) {
        this.listSubSystemId = listSubSystemId;
    }

    public String getIpbrId() {
        return ipbrId;
    }

    public void setIpbrId(String ipbrId) {
        this.ipbrId = ipbrId;
    }

    public String getActIndexM3ua() {
        return actIndexM3ua;
    }

    public void setActIndexM3ua(String actIndexM3ua) {
        this.actIndexM3ua = actIndexM3ua;
    }

    public String getDscp() {
        return dscp;
    }

    public void setDscp(String dscp) {
        this.dscp = dscp;
    }

    public String getListSignallingLink() {
        return ListSignallingLink;
    }

    public void setListSignallingLink(String ListSignallingLink) {
        this.ListSignallingLink = ListSignallingLink;
    }

    public String getConcernPointCodeName() {
        return concernPointCodeName;
    }

    public void setConcernPointCodeName(String concernPointCodeName) {
        this.concernPointCodeName = concernPointCodeName;
    }

    public String getConcernSsnName() {
        return concernSsnName;
    }

    public void setConcernSsnName(String concernSsnName) {
        this.concernSsnName = concernSsnName;
    }

    public String getAffectSsnName() {
        return affectSsnName;
    }

    public void setAffectSsnName(String affectSsnName) {
        this.affectSsnName = affectSsnName;
    }

    public String getPointCodeName() {
        return pointCodeName;
    }

    public void setPointCodeName(String pointCodeName) {
        this.pointCodeName = pointCodeName;
    }

    public String getSubSystemId() {
        return subSystemId;
    }

    public void setSubSystemId(String subSystemId) {
        this.subSystemId = subSystemId;
    }

    public String getSubSystemName() {
        return subSystemName;
    }

    public void setSubSystemName(String subSystemName) {
        this.subSystemName = subSystemName;
    }

    public String getPointCodeId() {
        return pointCodeId;
    }

    public void setPointCodeId(String pointCodeId) {
        this.pointCodeId = pointCodeId;
    }

    public String getNodeAssoc() {
        return nodeAssoc;
    }

    public void setNodeAssoc(String nodeAssoc) {
        this.nodeAssoc = nodeAssoc;
    }

    public String getLocalClientPort() {
        return localClientPort;
    }

    public void setLocalClientPort(String localClientPort) {
        this.localClientPort = localClientPort;
    }

    public String getLocalAsName() {
        return localAsName;
    }

    public void setLocalAsName(String localAsName) {
        this.localAsName = localAsName;
    }

    public String getRemoteAsId() {
        return remoteAsId;
    }

    public void setRemoteAsId(String remoteAsId) {
        this.remoteAsId = remoteAsId;
    }

    public String getRemoteAsName() {
        return remoteAsName;
    }

    public void setRemoteAsName(String remoteAsName) {
        this.remoteAsName = remoteAsName;
    }

    public String getDestinationEntity() {
        return destinationEntity;
    }

    public void setDestinationEntity(String destinationEntity) {
        this.destinationEntity = destinationEntity;
    }

    public String getDSPIndex() {
        return DSPIndex;
    }

    public void setDSPIndex(String DSPIndex) {
        this.DSPIndex = DSPIndex;
    }

    public String getCNOperatorID() {
        return CNOperatorID;
    }

    public void setCNOperatorID(String CNOperatorID) {
        this.CNOperatorID = CNOperatorID;
    }

    public String getCNNodeID() {
        return CNNodeID;
    }

    public void setCNNodeID(String CNNodeID) {
        this.CNNodeID = CNNodeID;
    }

    public String getAdjacentNodeID() {
        return AdjacentNodeID;
    }

    public void setAdjacentNodeID(String AdjacentNodeID) {
        this.AdjacentNodeID = AdjacentNodeID;
    }

    public String getSignallingLinkSetID() {
        return SignallingLinkSetID;
    }

    public void setSignallingLinkSetID(String SignallingLinkSetID) {
        this.SignallingLinkSetID = SignallingLinkSetID;
    }

    public String getSignallingLinkId() {
        return signallingLinkId;
    }

    public void setSignallingLinkId(String signallingLinkId) {
        this.signallingLinkId = signallingLinkId;
    }

    public String getSubrack() {
        return Subrack;
    }

    public void setSubrack(String Subrack) {
        this.Subrack = Subrack;
    }

    public String getSlotCard() {
        return slotCard;
    }

    public void setSlotCard(String slotCard) {
        this.slotCard = slotCard;
    }

    public String getSctplink() {
        return Sctplink;
    }

    public void setSctplink(String Sctplink) {
        this.Sctplink = Sctplink;
    }

    public String getUPNPGEPIndex() {
        return UPNPGEPIndex;
    }

    public void setUPNPGEPIndex(String UPNPGEPIndex) {
        this.UPNPGEPIndex = UPNPGEPIndex;
    }

    public String getSignallingLinkName() {
        return SignallingLinkName;
    }

    public void setSignallingLinkName(String SignallingLinkName) {
        this.SignallingLinkName = SignallingLinkName;
    }

    public String getActSignallingLinkName() {
        return ActSignallingLinkName;
    }

    public void setActSignallingLinkName(String ActSignallingLinkName) {
        this.ActSignallingLinkName = ActSignallingLinkName;
    }

    public String getActRNCLac() {
        return actRNCLac;
    }

    public void setActRNCLac(String actRNCLac) {
        this.actRNCLac = actRNCLac;
    }

    public String getActRNCRac() {
        return actRNCRac;
    }

    public void setActRNCRac(String actRNCRac) {
        this.actRNCRac = actRNCRac;
    }

    public String getActRNCLasid() {
        return actRNCLasid;
    }

    public void setActRNCLasid(String actRNCLasid) {
        this.actRNCLasid = actRNCLasid;
    }

    public String getActRncSPC() {
        return actRncSPC;
    }

    public void setActRncSPC(String actRncSPC) {
        this.actRncSPC = actRncSPC;
    }

    public String getActRNCLspid() {
        return actRNCLspid;
    }

    public void setActRNCLspid(String actRNCLspid) {
        this.actRNCLspid = actRNCLspid;
    }

    public String getActRNCRspid() {
        return actRNCRspid;
    }

    public void setActRNCRspid(String actRNCRspid) {
        this.actRNCRspid = actRNCRspid;
    }

    public String getActRNCRasid() {
        return actRNCRasid;
    }

    public void setActRNCRasid(String actRNCRasid) {
        this.actRNCRasid = actRNCRasid;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getActAssocID() {
        return actAssocID;
    }

    public void setActAssocID(String actAssocID) {
        this.actAssocID = actAssocID;
    }

    public String getNPGEPIndex() {
        return NPGEPIndex;
    }

    public void setNPGEPIndex(String NPGEPIndex) {
        this.NPGEPIndex = NPGEPIndex;
    }

    public String getRNCLac() {
        return RNCLac;
    }

    public void setRNCLac(String RNCLac) {
        this.RNCLac = RNCLac;
    }

    public String getMscSctpProfile() {
        return MscSctpProfile;
    }

    public void setMscSctpProfile(String MscSctpProfile) {
        this.MscSctpProfile = MscSctpProfile;
    }

    public String getActRNCIuIP1() {
        return actRNCIuIP1;
    }

    public void setActRNCIuIP1(String actRNCIuIP1) {
        this.actRNCIuIP1 = actRNCIuIP1;
    }

    public String getActRNCIuIP2() {
        return actRNCIuIP2;
    }

    public void setActRNCIuIP2(String actRNCIuIP2) {
        this.actRNCIuIP2 = actRNCIuIP2;
    }

    public String getRNCPort() {
        return RNCPort;
    }

    public void setRNCPort(String RNCPort) {
        this.RNCPort = RNCPort;
    }

    public String getRNCIuIP1() {
        return RNCIuIP1;
    }

    public void setRNCIuIP1(String RNCIuIP1) {
        this.RNCIuIP1 = RNCIuIP1;
    }

    public String getRNCIuIP2() {
        return RNCIuIP2;
    }

    public void setRNCIuIP2(String RNCIuIP2) {
        this.RNCIuIP2 = RNCIuIP2;
    }

    public String getMSCIuIP1() {
        return MSCIuIP1;
    }

    public void setMSCIuIP1(String MSCIuIP1) {
        this.MSCIuIP1 = MSCIuIP1;
    }

    public String getMSCIuIP2() {
        return MSCIuIP2;
    }

    public void setMSCIuIP2(String MSCIuIP2) {
        this.MSCIuIP2 = MSCIuIP2;
    }

    public String getMSCPort() {
        return MSCPort;
    }

    public void setMSCPort(String MSCPort) {
        this.MSCPort = MSCPort;
    }

    public String getIcsu() {
        return icsu;
    }

    public void setIcsu(String icsu) {
        this.icsu = icsu;
    }

    public String getSignallingLink() {
        return SignallingLink;
    }

    public void setSignallingLink(String SignallingLink) {
        this.SignallingLink = SignallingLink;
    }

    public String getAssocciationSetName() {
        return AssocciationSetName;
    }

    public void setAssocciationSetName(String AssocciationSetName) {
        this.AssocciationSetName = AssocciationSetName;
    }

    public String getGatewayCPNPGEP() {
        return GatewayCPNPGEP;
    }

    public void setGatewayCPNPGEP(String GatewayCPNPGEP) {
        this.GatewayCPNPGEP = GatewayCPNPGEP;
    }

    public String getMscIuIP() {
        return MscIuIP;
    }

    public void setMscIuIP(String MscIuIP) {
        this.MscIuIP = MscIuIP;
    }

    public String getCPNPGEP1() {
        return CPNPGEP1;
    }

    public void setCPNPGEP1(String CPNPGEP1) {
        this.CPNPGEP1 = CPNPGEP1;
    }

    public String getCPNPGEP2() {
        return CPNPGEP2;
    }

    public void setCPNPGEP2(String CPNPGEP2) {
        this.CPNPGEP2 = CPNPGEP2;
    }

    public String getAssocID() {
        return AssocID;
    }

    public void setAssocID(String AssocID) {
        this.AssocID = AssocID;
    }

    public String getActAssocciationSetName() {
        return ActAssocciationSetName;
    }

    public void setActAssocciationSetName(String ActAssocciationSetName) {
        this.ActAssocciationSetName = ActAssocciationSetName;
    }

    public String getActSignallingLink() {
        return ActSignallingLink;
    }

    public void setActSignallingLink(String ActSignallingLink) {
        this.ActSignallingLink = ActSignallingLink;
    }

    public String getSctpid() {
        return sctpid;
    }

    public void setSctpid(String sctpid) {
        this.sctpid = sctpid;
    }

    public String getIpAccessHostPool() {
        return IpAccessHostPool;
    }

    public void setIpAccessHostPool(String IpAccessHostPool) {
        this.IpAccessHostPool = IpAccessHostPool;
    }

    public String getSccpApLocal() {
        return SccpApLocal;
    }

    public void setSccpApLocal(String SccpApLocal) {
        this.SccpApLocal = SccpApLocal;
    }

}
