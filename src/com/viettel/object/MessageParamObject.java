/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

import java.util.Map;

/**
 *
 * @author hienhv4
 */
public class MessageParamObject {
    private Long flowRunGetParamId;
    private Map<String, AccountObj> mapAccount;

    public Long getFlowRunGetParamId() {
        return flowRunGetParamId;
    }

    public void setFlowRunGetParamId(Long flowRunGetParamId) {
        this.flowRunGetParamId = flowRunGetParamId;
    }

    public Map<String, AccountObj> getMapAccount() {
        return mapAccount;
    }

    public void setMapAccount(Map<String, AccountObj> mapAccount) {
        this.mapAccount = mapAccount;
    }
}
