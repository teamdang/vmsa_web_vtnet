/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

/**
 *
 * @author quytv7
 */
public class RiSgRcResultImport {

    private String nodeRnc;
    private String nodeSgsnActive;
    private String nodeSgsnBackup;
    private String resultImport;
    private String detailImport;

    public String getNodeRnc() {
        return nodeRnc;
    }

    public void setNodeRnc(String nodeRnc) {
        this.nodeRnc = nodeRnc;
    }

    public String getNodeSgsnActive() {
        return nodeSgsnActive;
    }

    public void setNodeSgsnActive(String nodeSgsnActive) {
        this.nodeSgsnActive = nodeSgsnActive;
    }

    public String getNodeSgsnBackup() {
        return nodeSgsnBackup;
    }

    public void setNodeSgsnBackup(String nodeSgsnBackup) {
        this.nodeSgsnBackup = nodeSgsnBackup;
    }

    public String getResultImport() {
        return resultImport;
    }

    public void setResultImport(String resultImport) {
        this.resultImport = resultImport;
    }

    public String getDetailImport() {
        return detailImport;
    }

    public void setDetailImport(String detailImport) {
        this.detailImport = detailImport;
    }

}
