/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

import java.io.Serializable;

/**
 *
 * @author quytv7
 */
public class StationCellResult implements Serializable {

    private String ZONE;
    private String RNC;
    private String nodeB;
    private String userLabel;
    private String UtrancellID;
    private String localCellId;
    private String uarfcnDl;
    private String uarfcnUl;
    private String lac;
    private String rac;
    private String sac;
    private String URA;
    private String primaryScramblingCode;
    private String tCell;
    private String pwroffset;
    private String pwradm;
    private String dlCodeAdm;
    private String sIntraSearch;
    private String sInterSearch;
    private String qualMeasQuantity;
    private String fachMeasOccaCycLenCoeff;
    private String dchIflsThreshPower;
    private String dchIflsMarginPower;
    private String dchIflsThreshCode;
    private String dchIflsMarginCode;
    private String pathlossThreshold;
    private String hsdpaUsersAdm;
    private String eulServingCellUsersAdm;
    private String hsIflsThreshUsers;
    private String hsIflsMarginUsers;
    private String iflsMode;
    private String hsIflsSpeechMultiRabTrigg;
    private String iflsCpichEcnoThresh;
    private String loadSharingMargin;
    private String hoType;
    private String usedFreqThresh2dEcno;
    private String usedFreqThresh2dRscp;

    public String getZONE() {
        return ZONE;
    }

    public void setZONE(String ZONE) {
        this.ZONE = ZONE;
    }

    public String getRNC() {
        return RNC;
    }

    public void setRNC(String RNC) {
        this.RNC = RNC;
    }

    public String getNodeB() {
        return nodeB;
    }

    public void setNodeB(String nodeB) {
        this.nodeB = nodeB;
    }

    public String getUserLabel() {
        return userLabel;
    }

    public void setUserLabel(String userLabel) {
        this.userLabel = userLabel;
    }

    public String getUtrancellID() {
        return UtrancellID;
    }

    public void setUtrancellID(String UtrancellID) {
        this.UtrancellID = UtrancellID;
    }

    public String getLocalCellId() {
        return localCellId;
    }

    public void setLocalCellId(String localCellId) {
        this.localCellId = localCellId;
    }

    public String getUarfcnDl() {
        return uarfcnDl;
    }

    public void setUarfcnDl(String uarfcnDl) {
        this.uarfcnDl = uarfcnDl;
    }

    public String getUarfcnUl() {
        return uarfcnUl;
    }

    public void setUarfcnUl(String uarfcnUl) {
        this.uarfcnUl = uarfcnUl;
    }

    public String getLac() {
        return lac;
    }

    public void setLac(String lac) {
        this.lac = lac;
    }

    public String getRac() {
        return rac;
    }

    public void setRac(String rac) {
        this.rac = rac;
    }

    public String getSac() {
        return sac;
    }

    public void setSac(String sac) {
        this.sac = sac;
    }

    public String getURA() {
        return URA;
    }

    public void setURA(String URA) {
        this.URA = URA;
    }

    public String getPrimaryScramblingCode() {
        return primaryScramblingCode;
    }

    public void setPrimaryScramblingCode(String primaryScramblingCode) {
        this.primaryScramblingCode = primaryScramblingCode;
    }

    public String gettCell() {
        return tCell;
    }

    public void settCell(String tCell) {
        this.tCell = tCell;
    }

    public String getPwroffset() {
        return pwroffset;
    }

    public void setPwroffset(String pwroffset) {
        this.pwroffset = pwroffset;
    }

    public String getPwradm() {
        return pwradm;
    }

    public void setPwradm(String pwradm) {
        this.pwradm = pwradm;
    }

    public String getDlCodeAdm() {
        return dlCodeAdm;
    }

    public void setDlCodeAdm(String dlCodeAdm) {
        this.dlCodeAdm = dlCodeAdm;
    }

    public String getsIntraSearch() {
        return sIntraSearch;
    }

    public void setsIntraSearch(String sIntraSearch) {
        this.sIntraSearch = sIntraSearch;
    }

    public String getsInterSearch() {
        return sInterSearch;
    }

    public void setsInterSearch(String sInterSearch) {
        this.sInterSearch = sInterSearch;
    }

    public String getQualMeasQuantity() {
        return qualMeasQuantity;
    }

    public void setQualMeasQuantity(String qualMeasQuantity) {
        this.qualMeasQuantity = qualMeasQuantity;
    }

    public String getFachMeasOccaCycLenCoeff() {
        return fachMeasOccaCycLenCoeff;
    }

    public void setFachMeasOccaCycLenCoeff(String fachMeasOccaCycLenCoeff) {
        this.fachMeasOccaCycLenCoeff = fachMeasOccaCycLenCoeff;
    }

    public String getDchIflsThreshPower() {
        return dchIflsThreshPower;
    }

    public void setDchIflsThreshPower(String dchIflsThreshPower) {
        this.dchIflsThreshPower = dchIflsThreshPower;
    }

    public String getDchIflsMarginPower() {
        return dchIflsMarginPower;
    }

    public void setDchIflsMarginPower(String dchIflsMarginPower) {
        this.dchIflsMarginPower = dchIflsMarginPower;
    }

    public String getDchIflsThreshCode() {
        return dchIflsThreshCode;
    }

    public void setDchIflsThreshCode(String dchIflsThreshCode) {
        this.dchIflsThreshCode = dchIflsThreshCode;
    }

    public String getDchIflsMarginCode() {
        return dchIflsMarginCode;
    }

    public void setDchIflsMarginCode(String dchIflsMarginCode) {
        this.dchIflsMarginCode = dchIflsMarginCode;
    }

    public String getPathlossThreshold() {
        return pathlossThreshold;
    }

    public void setPathlossThreshold(String pathlossThreshold) {
        this.pathlossThreshold = pathlossThreshold;
    }

    public String getHsdpaUsersAdm() {
        return hsdpaUsersAdm;
    }

    public void setHsdpaUsersAdm(String hsdpaUsersAdm) {
        this.hsdpaUsersAdm = hsdpaUsersAdm;
    }

    public String getEulServingCellUsersAdm() {
        return eulServingCellUsersAdm;
    }

    public void setEulServingCellUsersAdm(String eulServingCellUsersAdm) {
        this.eulServingCellUsersAdm = eulServingCellUsersAdm;
    }

    public String getHsIflsThreshUsers() {
        return hsIflsThreshUsers;
    }

    public void setHsIflsThreshUsers(String hsIflsThreshUsers) {
        this.hsIflsThreshUsers = hsIflsThreshUsers;
    }

    public String getHsIflsMarginUsers() {
        return hsIflsMarginUsers;
    }

    public void setHsIflsMarginUsers(String hsIflsMarginUsers) {
        this.hsIflsMarginUsers = hsIflsMarginUsers;
    }

    public String getIflsMode() {
        return iflsMode;
    }

    public void setIflsMode(String iflsMode) {
        this.iflsMode = iflsMode;
    }

    public String getHsIflsSpeechMultiRabTrigg() {
        return hsIflsSpeechMultiRabTrigg;
    }

    public void setHsIflsSpeechMultiRabTrigg(String hsIflsSpeechMultiRabTrigg) {
        this.hsIflsSpeechMultiRabTrigg = hsIflsSpeechMultiRabTrigg;
    }

    public String getIflsCpichEcnoThresh() {
        return iflsCpichEcnoThresh;
    }

    public void setIflsCpichEcnoThresh(String iflsCpichEcnoThresh) {
        this.iflsCpichEcnoThresh = iflsCpichEcnoThresh;
    }

    public String getLoadSharingMargin() {
        return loadSharingMargin;
    }

    public void setLoadSharingMargin(String loadSharingMargin) {
        this.loadSharingMargin = loadSharingMargin;
    }

    public String getHoType() {
        return hoType;
    }

    public void setHoType(String hoType) {
        this.hoType = hoType;
    }

    public String getUsedFreqThresh2dEcno() {
        return usedFreqThresh2dEcno;
    }

    public void setUsedFreqThresh2dEcno(String usedFreqThresh2dEcno) {
        this.usedFreqThresh2dEcno = usedFreqThresh2dEcno;
    }

    public String getUsedFreqThresh2dRscp() {
        return usedFreqThresh2dRscp;
    }

    public void setUsedFreqThresh2dRscp(String usedFreqThresh2dRscp) {
        this.usedFreqThresh2dRscp = usedFreqThresh2dRscp;
    }

}
