/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

/**
 *
 * @author quytv7
 */
public class RiMsRcResultImport {

    private String node;
    private String nodeActive;
    private String nodeBackup;
    private String resultImport;
    private String detailImport;

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public String getNodeActive() {
        return nodeActive;
    }

    public void setNodeActive(String nodeActive) {
        this.nodeActive = nodeActive;
    }

    public String getNodeBackup() {
        return nodeBackup;
    }

    public void setNodeBackup(String nodeBackup) {
        this.nodeBackup = nodeBackup;
    }

    public String getResultImport() {
        return resultImport;
    }

    public void setResultImport(String resultImport) {
        this.resultImport = resultImport;
    }

    public String getDetailImport() {
        return detailImport;
    }

    public void setDetailImport(String detailImport) {
        this.detailImport = detailImport;
    }

}
