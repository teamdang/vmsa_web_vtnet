/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

import com.viettel.model.CommandDetail;
import com.viettel.model.Node;

/**
 *
 * @author hienhv4
 */
public class CommandObject {
    private String cmd;
    private String cmdSend;
    private String result;
    private String resultDetail;
    private String resultFinal;
    private Node nodeRun;
    private CommandDetail commandDetail;
    private String account;
    private String password;
    
    //Cac truong filter
    private Long filterVersionId;
    private Long filterVendorId;
    private Long filterNodeTypeId;
    private String meid;

    public String getMeid() {
        return meid;
    }

    public void setMeid(String meid) {
        this.meid = meid;
    }

    public Long getFilterVersionId() {
        return filterVersionId;
    }

    public void setFilterVersionId(Long filterVersionId) {
        this.filterVersionId = filterVersionId;
    }

    public Long getFilterVendorId() {
        return filterVendorId;
    }

    public void setFilterVendorId(Long filterVendorId) {
        this.filterVendorId = filterVendorId;
    }

    public Long getFilterNodeTypeId() {
        return filterNodeTypeId;
    }

    public void setFilterNodeTypeId(Long filterNodeTypeId) {
        this.filterNodeTypeId = filterNodeTypeId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CommandDetail getCommandDetail() {
        return commandDetail;
    }

    public void setCommandDetail(CommandDetail commandDetail) {
        this.commandDetail = commandDetail;
    }

    public Node getNodeRun() {
        return nodeRun;
    }

    public void setNodeRun(Node nodeRun) {
        this.nodeRun = nodeRun;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getCmdSend() {
        return cmdSend;
    }

    public void setCmdSend(String cmdSend) {
        this.cmdSend = cmdSend;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResultDetail() {
        return resultDetail;
    }

    public void setResultDetail(String resultDetail) {
        this.resultDetail = resultDetail;
    }

    public String getResultFinal() {
        return resultFinal;
    }

    public void setResultFinal(String resultFinal) {
        this.resultFinal = resultFinal;
    }

    @Override
    public String toString() {
        return "CommandObject{" +
                "cmd='" + cmd + '\'' +
                ", cmdSend='" + cmdSend + '\'' +
                ", result='" + result + '\'' +
                ", resultDetail='" + resultDetail + '\'' +
                ", resultFinal='" + resultFinal + '\'' +
                ", nodeRun=" + nodeRun +
                ", filterVersionId=" + filterVersionId +
                ", filterVendorId=" + filterVendorId +
                ", filterNodeTypeId=" + filterNodeTypeId +
                ", meid='" + meid + '\'' +
                '}';
    }
}
