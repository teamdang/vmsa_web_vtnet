/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

import java.io.Serializable;

/**
 *
 * @author quytv7
 */
public class RiSgRcModelExcel implements Serializable {

    String nodeCodeRnc;
    String nodeCodeSgsnCurrent;
    String nodeCodeSgsnBackup;
    String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getNodeCodeRnc() {
        return nodeCodeRnc;
    }

    public void setNodeCodeRnc(String nodeCodeRnc) {
        this.nodeCodeRnc = nodeCodeRnc;
    }

    public String getNodeCodeSgsnCurrent() {
        return nodeCodeSgsnCurrent;
    }

    public void setNodeCodeSgsnCurrent(String nodeCodeSgsnCurrent) {
        this.nodeCodeSgsnCurrent = nodeCodeSgsnCurrent;
    }

    public String getNodeCodeSgsnBackup() {
        return nodeCodeSgsnBackup;
    }

    public void setNodeCodeSgsnBackup(String nodeCodeSgsnBackup) {
        this.nodeCodeSgsnBackup = nodeCodeSgsnBackup;
    }

}
