/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

import java.io.Serializable;

/**
 *
 * @author quytv7
 */
public class RiMsRcModelExcel implements Serializable {

    String nodeCode;
    String nodeCodeCurrent;
    String nodeCodeBackup;
    String result;

    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    public String getNodeCodeCurrent() {
        return nodeCodeCurrent;
    }

    public void setNodeCodeCurrent(String nodeCodeCurrent) {
        this.nodeCodeCurrent = nodeCodeCurrent;
    }

    public String getNodeCodeBackup() {
        return nodeCodeBackup;
    }

    public void setNodeCodeBackup(String nodeCodeBackup) {
        this.nodeCodeBackup = nodeCodeBackup;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
