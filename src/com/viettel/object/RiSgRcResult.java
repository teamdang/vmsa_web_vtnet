/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.object;

/**
 *
 * @author quytv7
 */
public class RiSgRcResult {

    private String RNCPort = "";
    private String RNCIuIP1 = "";
    private String RNCIuIP2 = "";

    private String SGSNIuIP1 = "";
    private String SGSNIuIP2 = "";
    private String SGSNPort = "";

    private String icsu = "";
    private String SignallingLink = "";
    private String ListSignallingLink = "";
    private String SignallingLinkName = "";
    private String AssocciationSetName = "";

    private String GatewayCPNPGEP = "";
    private String SgsnIuIP = "";
    private String CPNPGEP1 = "";
    private String CPNPGEP2 = "";
    private String AssocID = "";
    private String VRFName = "";

    private String RNCLac = "";
    private String RNCRac = "";
    private String index = "";
    //Tham so cho RNC nokia mcRNC
    private String remoteAsId = "";
    private String remoteAsName = "";
    private String localAsName = "";
    private String localClientPort = "";
    private String nodeAssoc = "";
    private String pointCodeId = "";
    private String pointCodeName = "";
    private String subSystemId = "";
    private String subSystemName = "";
    private String concernSsnName = "";
    private String affectSsnName = "";
    private String concernPointCodeName = "";
    private String listRemoteAsId = "";
    private String listAssocId = "";
    private String listPointCodeId = "";
    private String listSubSystemId = "";
    private String ipbrId = "";
    private String ipbrName = "";
    private String gateway1 = "";
    private String gateway2 = "";
    private String localServerPort = "";
    //act mcRNC
    private String actConcernPointCodeName = "";
    private String actConcernSsnName = "";
    private String actSubSystemName = "";
    private String actRemoteAsName = "";

    //Tham so cho RNC ericsson
    private String sctpid = "";
    private String IpAccessHostPool = "";
    private String SccpApLocal = "";
    private String SgsnSctpProfile = "";
    private String NPGEPIndex = "";
    private String UPNPGEPIndex = "";
    private String actIndexM3ua = "";
    private String dscp = "";
    private String actDscp = "";

    //Tham so cho RNC Huawei
    private String destinationEntity = "";
    private String DSPIndex = "";
    private String CNOperatorID = "";
    private String CNNodeID = "";
    private String AdjacentNodeID = "";
    private String SignallingLinkSetID = "";
    private String signallingLinkId = "";
    private String Subrack = "";
    private String slotCard = "";
    private String Sctplink = "";

    private String actSignallingLinkSetID = "";
    private String actSignallingLinkId = "";
    private String actDSPIndex = "";
    private String actCNOperatorID = "";
    private String actCNNodeID = "";
    private String actAdjacentNodeID = "";
    private String actDestinationEntity = "";
    private String actSctplink = "";

    //Tham so act
    private String ActSignallingLinkName = "";
    private String ActAssocciationSetName = "";
    private String ActSignallingLink = "";
    private String actRNCIuIP1 = "";
    private String actRNCIuIP2 = "";
    private String actAssocID = "";
    private String actRNCLac = "";
    private String actRNCRac = "";
    private String actRNCLasid = "";
    private String actRncSPC = "";
    private String actRNCLspid = "";
    private String actRNCRspid = "";
    private String actRNCRasid = "";
    private String papu = "";

    public String getActSctplink() {
        return actSctplink;
    }

    public void setActSctplink(String actSctplink) {
        this.actSctplink = actSctplink;
    }

    public String getVRFName() {
        return VRFName;
    }

    public void setVRFName(String VRFName) {
        this.VRFName = VRFName;
    }

    public String getPapu() {
        return papu;
    }

    public void setPapu(String papu) {
        this.papu = papu;
    }

    public String getLocalServerPort() {
        return localServerPort;
    }

    public void setLocalServerPort(String localServerPort) {
        this.localServerPort = localServerPort;
    }

    public String getActSignallingLinkSetID() {
        return actSignallingLinkSetID;
    }

    public void setActSignallingLinkSetID(String actSignallingLinkSetID) {
        this.actSignallingLinkSetID = actSignallingLinkSetID;
    }

    public String getActSignallingLinkId() {
        return actSignallingLinkId;
    }

    public void setActSignallingLinkId(String actSignallingLinkId) {
        this.actSignallingLinkId = actSignallingLinkId;
    }

    public String getActDSPIndex() {
        return actDSPIndex;
    }

    public void setActDSPIndex(String actDSPIndex) {
        this.actDSPIndex = actDSPIndex;
    }

    public String getActCNOperatorID() {
        return actCNOperatorID;
    }

    public void setActCNOperatorID(String actCNOperatorID) {
        this.actCNOperatorID = actCNOperatorID;
    }

    public String getActCNNodeID() {
        return actCNNodeID;
    }

    public void setActCNNodeID(String actCNNodeID) {
        this.actCNNodeID = actCNNodeID;
    }

    public String getActAdjacentNodeID() {
        return actAdjacentNodeID;
    }

    public void setActAdjacentNodeID(String actAdjacentNodeID) {
        this.actAdjacentNodeID = actAdjacentNodeID;
    }

    public String getActDestinationEntity() {
        return actDestinationEntity;
    }

    public void setActDestinationEntity(String actDestinationEntity) {
        this.actDestinationEntity = actDestinationEntity;
    }

    public String getActDscp() {
        return actDscp;
    }

    public void setActDscp(String actDscp) {
        this.actDscp = actDscp;
    }

    public String getActConcernPointCodeName() {
        return actConcernPointCodeName;
    }

    public void setActConcernPointCodeName(String actConcernPointCodeName) {
        this.actConcernPointCodeName = actConcernPointCodeName;
    }

    public String getActConcernSsnName() {
        return actConcernSsnName;
    }

    public void setActConcernSsnName(String actConcernSsnName) {
        this.actConcernSsnName = actConcernSsnName;
    }

    public String getActSubSystemName() {
        return actSubSystemName;
    }

    public void setActSubSystemName(String actSubSystemName) {
        this.actSubSystemName = actSubSystemName;
    }

    public String getActRemoteAsName() {
        return actRemoteAsName;
    }

    public void setActRemoteAsName(String actRemoteAsName) {
        this.actRemoteAsName = actRemoteAsName;
    }

    public String getIpbrName() {
        return ipbrName;
    }

    public void setIpbrName(String ipbrName) {
        this.ipbrName = ipbrName;
    }

    public String getGateway1() {
        return gateway1;
    }

    public void setGateway1(String gateway1) {
        this.gateway1 = gateway1;
    }

    public String getGateway2() {
        return gateway2;
    }

    public void setGateway2(String gateway2) {
        this.gateway2 = gateway2;
    }

    public String getListRemoteAsId() {
        return listRemoteAsId;
    }

    public void setListRemoteAsId(String listRemoteAsId) {
        this.listRemoteAsId = listRemoteAsId;
    }

    public String getListAssocId() {
        return listAssocId;
    }

    public void setListAssocId(String listAssocId) {
        this.listAssocId = listAssocId;
    }

    public String getListPointCodeId() {
        return listPointCodeId;
    }

    public void setListPointCodeId(String listPointCodeId) {
        this.listPointCodeId = listPointCodeId;
    }

    public String getListSubSystemId() {
        return listSubSystemId;
    }

    public void setListSubSystemId(String listSubSystemId) {
        this.listSubSystemId = listSubSystemId;
    }

    public String getIpbrId() {
        return ipbrId;
    }

    public void setIpbrId(String ipbrId) {
        this.ipbrId = ipbrId;
    }

    public String getActIndexM3ua() {
        return actIndexM3ua;
    }

    public void setActIndexM3ua(String actIndexM3ua) {
        this.actIndexM3ua = actIndexM3ua;
    }

    public String getDscp() {
        return dscp;
    }

    public void setDscp(String dscp) {
        this.dscp = dscp;
    }

    public String getListSignallingLink() {
        return ListSignallingLink;
    }

    public void setListSignallingLink(String ListSignallingLink) {
        this.ListSignallingLink = ListSignallingLink;
    }

    public String getConcernPointCodeName() {
        return concernPointCodeName;
    }

    public void setConcernPointCodeName(String concernPointCodeName) {
        this.concernPointCodeName = concernPointCodeName;
    }

    public String getConcernSsnName() {
        return concernSsnName;
    }

    public void setConcernSsnName(String concernSsnName) {
        this.concernSsnName = concernSsnName;
    }

    public String getAffectSsnName() {
        return affectSsnName;
    }

    public void setAffectSsnName(String affectSsnName) {
        this.affectSsnName = affectSsnName;
    }

    public String getPointCodeName() {
        return pointCodeName;
    }

    public void setPointCodeName(String pointCodeName) {
        this.pointCodeName = pointCodeName;
    }

    public String getSubSystemId() {
        return subSystemId;
    }

    public void setSubSystemId(String subSystemId) {
        this.subSystemId = subSystemId;
    }

    public String getSubSystemName() {
        return subSystemName;
    }

    public void setSubSystemName(String subSystemName) {
        this.subSystemName = subSystemName;
    }

    public String getPointCodeId() {
        return pointCodeId;
    }

    public void setPointCodeId(String pointCodeId) {
        this.pointCodeId = pointCodeId;
    }

    public String getNodeAssoc() {
        return nodeAssoc;
    }

    public void setNodeAssoc(String nodeAssoc) {
        this.nodeAssoc = nodeAssoc;
    }

    public String getLocalClientPort() {
        return localClientPort;
    }

    public void setLocalClientPort(String localClientPort) {
        this.localClientPort = localClientPort;
    }

    public String getLocalAsName() {
        return localAsName;
    }

    public void setLocalAsName(String localAsName) {
        this.localAsName = localAsName;
    }

    public String getRemoteAsId() {
        return remoteAsId;
    }

    public void setRemoteAsId(String remoteAsId) {
        this.remoteAsId = remoteAsId;
    }

    public String getRemoteAsName() {
        return remoteAsName;
    }

    public void setRemoteAsName(String remoteAsName) {
        this.remoteAsName = remoteAsName;
    }

    public String getDestinationEntity() {
        return destinationEntity;
    }

    public void setDestinationEntity(String destinationEntity) {
        this.destinationEntity = destinationEntity;
    }

    public String getDSPIndex() {
        return DSPIndex;
    }

    public void setDSPIndex(String DSPIndex) {
        this.DSPIndex = DSPIndex;
    }

    public String getCNOperatorID() {
        return CNOperatorID;
    }

    public void setCNOperatorID(String CNOperatorID) {
        this.CNOperatorID = CNOperatorID;
    }

    public String getCNNodeID() {
        return CNNodeID;
    }

    public void setCNNodeID(String CNNodeID) {
        this.CNNodeID = CNNodeID;
    }

    public String getAdjacentNodeID() {
        return AdjacentNodeID;
    }

    public void setAdjacentNodeID(String AdjacentNodeID) {
        this.AdjacentNodeID = AdjacentNodeID;
    }

    public String getSignallingLinkSetID() {
        return SignallingLinkSetID;
    }

    public void setSignallingLinkSetID(String SignallingLinkSetID) {
        this.SignallingLinkSetID = SignallingLinkSetID;
    }

    public String getSignallingLinkId() {
        return signallingLinkId;
    }

    public void setSignallingLinkId(String signallingLinkId) {
        this.signallingLinkId = signallingLinkId;
    }

    public String getSubrack() {
        return Subrack;
    }

    public void setSubrack(String Subrack) {
        this.Subrack = Subrack;
    }

    public String getSlotCard() {
        return slotCard;
    }

    public void setSlotCard(String slotCard) {
        this.slotCard = slotCard;
    }

    public String getSctplink() {
        return Sctplink;
    }

    public void setSctplink(String Sctplink) {
        this.Sctplink = Sctplink;
    }

    public String getUPNPGEPIndex() {
        return UPNPGEPIndex;
    }

    public void setUPNPGEPIndex(String UPNPGEPIndex) {
        this.UPNPGEPIndex = UPNPGEPIndex;
    }

    public String getSignallingLinkName() {
        return SignallingLinkName;
    }

    public void setSignallingLinkName(String SignallingLinkName) {
        this.SignallingLinkName = SignallingLinkName;
    }

    public String getActSignallingLinkName() {
        return ActSignallingLinkName;
    }

    public void setActSignallingLinkName(String ActSignallingLinkName) {
        this.ActSignallingLinkName = ActSignallingLinkName;
    }

    public String getActRNCLac() {
        return actRNCLac;
    }

    public void setActRNCLac(String actRNCLac) {
        this.actRNCLac = actRNCLac;
    }

    public String getActRNCRac() {
        return actRNCRac;
    }

    public void setActRNCRac(String actRNCRac) {
        this.actRNCRac = actRNCRac;
    }

    public String getActRNCLasid() {
        return actRNCLasid;
    }

    public void setActRNCLasid(String actRNCLasid) {
        this.actRNCLasid = actRNCLasid;
    }

    public String getActRncSPC() {
        return actRncSPC;
    }

    public void setActRncSPC(String actRncSPC) {
        this.actRncSPC = actRncSPC;
    }

    public String getActRNCLspid() {
        return actRNCLspid;
    }

    public void setActRNCLspid(String actRNCLspid) {
        this.actRNCLspid = actRNCLspid;
    }

    public String getActRNCRspid() {
        return actRNCRspid;
    }

    public void setActRNCRspid(String actRNCRspid) {
        this.actRNCRspid = actRNCRspid;
    }

    public String getActRNCRasid() {
        return actRNCRasid;
    }

    public void setActRNCRasid(String actRNCRasid) {
        this.actRNCRasid = actRNCRasid;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getActAssocID() {
        return actAssocID;
    }

    public void setActAssocID(String actAssocID) {
        this.actAssocID = actAssocID;
    }

    public String getNPGEPIndex() {
        return NPGEPIndex;
    }

    public void setNPGEPIndex(String NPGEPIndex) {
        this.NPGEPIndex = NPGEPIndex;
    }

    public String getRNCLac() {
        return RNCLac;
    }

    public void setRNCLac(String RNCLac) {
        this.RNCLac = RNCLac;
    }

    public String getRNCRac() {
        return RNCRac;
    }

    public void setRNCRac(String RNCRac) {
        this.RNCRac = RNCRac;
    }

    public String getSgsnSctpProfile() {
        return SgsnSctpProfile;
    }

    public void setSgsnSctpProfile(String SgsnSctpProfile) {
        this.SgsnSctpProfile = SgsnSctpProfile;
    }

    public String getActRNCIuIP1() {
        return actRNCIuIP1;
    }

    public void setActRNCIuIP1(String actRNCIuIP1) {
        this.actRNCIuIP1 = actRNCIuIP1;
    }

    public String getActRNCIuIP2() {
        return actRNCIuIP2;
    }

    public void setActRNCIuIP2(String actRNCIuIP2) {
        this.actRNCIuIP2 = actRNCIuIP2;
    }

    public String getRNCPort() {
        return RNCPort;
    }

    public void setRNCPort(String RNCPort) {
        this.RNCPort = RNCPort;
    }

    public String getRNCIuIP1() {
        return RNCIuIP1;
    }

    public void setRNCIuIP1(String RNCIuIP1) {
        this.RNCIuIP1 = RNCIuIP1;
    }

    public String getRNCIuIP2() {
        return RNCIuIP2;
    }

    public void setRNCIuIP2(String RNCIuIP2) {
        this.RNCIuIP2 = RNCIuIP2;
    }

    public String getSGSNIuIP1() {
        return SGSNIuIP1;
    }

    public void setSGSNIuIP1(String SGSNIuIP1) {
        this.SGSNIuIP1 = SGSNIuIP1;
    }

    public String getSGSNIuIP2() {
        return SGSNIuIP2;
    }

    public void setSGSNIuIP2(String SGSNIuIP2) {
        this.SGSNIuIP2 = SGSNIuIP2;
    }

    public String getSGSNPort() {
        return SGSNPort;
    }

    public void setSGSNPort(String SGSNPort) {
        this.SGSNPort = SGSNPort;
    }

    public String getIcsu() {
        return icsu;
    }

    public void setIcsu(String icsu) {
        this.icsu = icsu;
    }

    public String getSignallingLink() {
        return SignallingLink;
    }

    public void setSignallingLink(String SignallingLink) {
        this.SignallingLink = SignallingLink;
    }

    public String getAssocciationSetName() {
        return AssocciationSetName;
    }

    public void setAssocciationSetName(String AssocciationSetName) {
        this.AssocciationSetName = AssocciationSetName;
    }

    public String getGatewayCPNPGEP() {
        return GatewayCPNPGEP;
    }

    public void setGatewayCPNPGEP(String GatewayCPNPGEP) {
        this.GatewayCPNPGEP = GatewayCPNPGEP;
    }

    public String getSgsnIuIP() {
        return SgsnIuIP;
    }

    public void setSgsnIuIP(String SgsnIuIP) {
        this.SgsnIuIP = SgsnIuIP;
    }

    public String getCPNPGEP1() {
        return CPNPGEP1;
    }

    public void setCPNPGEP1(String CPNPGEP1) {
        this.CPNPGEP1 = CPNPGEP1;
    }

    public String getCPNPGEP2() {
        return CPNPGEP2;
    }

    public void setCPNPGEP2(String CPNPGEP2) {
        this.CPNPGEP2 = CPNPGEP2;
    }

    public String getAssocID() {
        return AssocID;
    }

    public void setAssocID(String AssocID) {
        this.AssocID = AssocID;
    }

    public String getActAssocciationSetName() {
        return ActAssocciationSetName;
    }

    public void setActAssocciationSetName(String ActAssocciationSetName) {
        this.ActAssocciationSetName = ActAssocciationSetName;
    }

    public String getActSignallingLink() {
        return ActSignallingLink;
    }

    public void setActSignallingLink(String ActSignallingLink) {
        this.ActSignallingLink = ActSignallingLink;
    }

    public String getSctpid() {
        return sctpid;
    }

    public void setSctpid(String sctpid) {
        this.sctpid = sctpid;
    }

    public String getIpAccessHostPool() {
        return IpAccessHostPool;
    }

    public void setIpAccessHostPool(String IpAccessHostPool) {
        this.IpAccessHostPool = IpAccessHostPool;
    }

    public String getSccpApLocal() {
        return SccpApLocal;
    }

    public void setSccpApLocal(String SccpApLocal) {
        this.SccpApLocal = SccpApLocal;
    }

}
