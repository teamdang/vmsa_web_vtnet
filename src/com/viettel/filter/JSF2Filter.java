package com.viettel.filter;

import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.ConfigTable;
import com.viettel.model.MapUserCountryBO;
import com.viettel.persistence.ConfigTableServiceImpl;
import com.viettel.persistence.DaoSimpleService;
import com.viettel.persistence.MapUserCountryServiceImpl;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.faces.application.ResourceHandler;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.viettel.vsa.token.ObjectToken;
import com.viettel.vsa.token.UserToken;
import com.viettel.vsa.util.Connector;
import com.viettel.vsa.util.SessionVTListener;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.viettel.rest.Constants;

/**
 * Filter chung cua ca he thong doi voi duong dan tuyet doi. Tat ca cac module
 * deu phai khai bao phan quyen o day.
 *
 * @author hanh45
 *
 */
@WebFilter(dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.ERROR}, urlPatterns = {"/faces/*"})
public class JSF2Filter implements Filter {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private String _REQUEST_PATH = "";
    private static final String _HOME_PATH = "/home";
    private static final String _TIMEOUT_PATH = "/timeout";
    private static final String _NO_PERMISSION_PATH = "/permission";
    private static final String _XHTML = ".xhtml";
    private static final String _FACES = "/faces/";
    private static final String _VSA_USER_TOKEN = "vsaUserToken";
    private static final String AUTHEN_PATH = "/authentication";
    private static final String _MAX_USER = "/max-user";

    public JSF2Filter() {
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        HttpServletResponse res = (HttpServletResponse) response;

        Connector localConnector = new Connector(req, res);
        if (localConnector.isAuthenticate() && localConnector.hadTicket()) {
            LOGGER.info("Session count: " + SessionVTListener.getCountSess());

            session.setAttribute("count_user", SessionVTListener.getCountSess());
            if (SessionVTListener.getCountSess() > 100) {
                res.sendRedirect(req.getContextPath() + _MAX_USER);
                return;
            }
        }

        boolean checkAuth = false;

        // Skip JSF resources (CSS/JS/Images/etc)
        if (!req.getRequestURI().startsWith(req.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER)) {
            res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
            // 1.1.
            res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            res.setDateHeader("Expires", 0); // Proxies.
        }

        _REQUEST_PATH = req.getServletPath();
        int pos = _REQUEST_PATH.indexOf("/", _FACES.length());
        String SUB_REQUEST_PATH = _REQUEST_PATH.substring(0, pos);
        _REQUEST_PATH = _REQUEST_PATH.substring(0, _REQUEST_PATH.indexOf(_XHTML) + _XHTML.length());

        // Kiem tra session timeout.
        UserToken userToken = (UserToken) session.getAttribute(_VSA_USER_TOKEN);
        if (userToken != null) {
            // Current session on.
            switch (SUB_REQUEST_PATH) {
                case _FACES + "home":
                case _FACES + "topo-net":
                case _FACES + "dashboard":
                case _FACES + "database":
                case _FACES + "rescue":
                case _FACES + "station":
                case _FACES + "change":
                case _FACES + "adm":
                    checkAuth = true;
                    break;
                default:
                    checkAuth = getPermission(session, _REQUEST_PATH);
                    break;
            }
        } else {
            // Session timeout.
            // Xu ly cho ajax khi session timeout.
            session.setAttribute(_VSA_USER_TOKEN, null);
            handleSessionTimeout(req, res);
        }

        localConnector = new Connector(req, res);
        if (localConnector.isAuthenticate() && localConnector.hadTicket()) {
            String sqlInsMapUserCountry = "MERGE INTO MAP_USER_COUNTRY D\n"
                    + "USING (SELECT ? username, ? country from dual) S ON (D.USER_NAME = S.username and D.COUNTRY_CODE=S.country)\n"
                    + "WHEN NOT MATCHED THEN INSERT (ID,USER_NAME,COUNTRY_CODE,STATUS,LAST_LOGIN) VALUES (MAP_USER_COUNTRY_SEQ.nextval, S.username,NULL ,1,sysdate)\n"
                    + "WHEN MATCHED THEN UPDATE SET LAST_LOGIN = sysdate";

            try {
                if (userToken != null) {
                    Map<String, Object> filter = new HashMap<>();
                    LinkedHashMap<String, String> order = new LinkedHashMap<>();
                    filter.put("userName-EXAC", userToken.getUserName().toLowerCase());
                    order.put("lastLogin", "DESC");
                    List<MapUserCountryBO> loginHistoris = new MapUserCountryServiceImpl().findList(filter, order);
                    if (loginHistoris.isEmpty()) {
                        new DaoSimpleService().execteNativeBulk(sqlInsMapUserCountry, userToken.getUserName().toLowerCase(), "NO_COUNTRY");
                        session.setAttribute(Constants.COUNTRY_CODE_CURRENT, Constants.VNM);
                    } else {
                        for (MapUserCountryBO history : loginHistoris) {
                            if (history.getLastLogin() != null) {
                                session.setAttribute(Constants.COUNTRY_CODE_CURRENT, history.getCountryCode());
                                break;
                            }
                        }
                        String countryCode = (String) session.getAttribute(Constants.COUNTRY_CODE_CURRENT);
                        if (countryCode == null) {
                            session.setAttribute(Constants.COUNTRY_CODE_CURRENT, Constants.VNM);
                        } else {
                            new DaoSimpleService().execteNativeBulk(sqlInsMapUserCountry, userToken.getUserName().toLowerCase(), countryCode);
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        if (checkAuth) {

            Boolean isAuthen = (Boolean) session.getAttribute("isAuthOtp");
            if (isAuthen == null || !isAuthen) {
                ConfigTable otpConfig = null;
                try {
                    otpConfig = new ConfigTableServiceImpl().findById("SHOW_OTP");
                } catch (AppException | SysException e) {
                    LOGGER.error(e.getMessage(), e);
                }

                if (otpConfig != null && otpConfig.getConfigValue() != null && "true".equalsIgnoreCase(otpConfig.getConfigValue().trim())) {
                    res.sendRedirect(req.getContextPath() + AUTHEN_PATH);
                    return;
                }
            }
        }

        if (checkAuth) {
            chain.doFilter(request, response);
        } else // Dieu huong den trang bao loi.
        if (!res.isCommitted()) {
            res.sendRedirect(req.getContextPath() + _NO_PERMISSION_PATH);
        }
    }

    /**
     * Xu ly session timeout.
     *
     * @throws IOException
     */
    private void handleSessionTimeout(HttpServletRequest req, HttpServletResponse res) throws IOException {
        if ("partial/ajax".equals(req.getHeader("Faces-Request"))) {
            //res.setContentType("text/xml");
//			res.getWriter().append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
//					.printf("<partial-response><redirect url=\"%s\"></redirect></partial-response>", req.getContextPath() + _HOME_PATH);
            String loginUrl;
            loginUrl = req.getContextPath() + _TIMEOUT_PATH;
            res.getWriter().print(xmlPartialRedirectToPage(req, loginUrl));
            res.flushBuffer();
        } else {
            if (!res.isCommitted()) {
                res.sendRedirect(req.getContextPath() + _HOME_PATH);
            }
        }
    }

    /**
     * Ham kiem tra quyen cua user tren session
     *
     * @param urlCode
     * @return
     */
    private boolean getUrlPermission(HttpSession session, String urlCode) {
        boolean result = false;

        UserToken userToken = (UserToken) session.getAttribute(_VSA_USER_TOKEN);
        if (userToken != null) {
            for (ObjectToken ot : userToken.getObjectTokens()) {
                String objToken = ot.getObjectUrl();
                if (objToken.equalsIgnoreCase(urlCode)) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    private boolean getPermission(HttpSession session, String requestPath) {
        // //System.out.println("vao getManagerPermisstion !!!!!!!!!!!!!!!!!!!!!!!!! ");
        // Logger.getLogger("huynx6_3").info(requestPath);
        boolean checkAuth = false;
        try {
            if (Pattern.compile("^" + _FACES + ".+?" + "/index.xhtml$").matcher(requestPath).find()) {
                checkAuth = getUrlPermission(session, requestPath.substring(_FACES.length() - 1, requestPath.indexOf("/index.xhtml")));
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return checkAuth;
    }

    private String xmlPartialRedirectToPage(HttpServletRequest req, String page) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version='1.0' encoding='UTF-8'?>");
        sb.append("<partial-response><redirect url=\"").append(page).append("\"/></partial-response>");
        return sb.toString();
    }
}
