package com.viettel.thread;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;

import com.viettel.model.ENodeBPlan;
import com.viettel.util.ObjectConcurrentMap;
import com.viettel.util.TelnetClientUtil;


/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Jul 6, 2016
 * @version 1.0 
 */
@SuppressWarnings("serial")
public class TraceRouteThread implements Runnable  {
	ENodeBPlan eNodeBPlan;
	private volatile State state = new State();
	int type;
	SimpleDateFormat sdFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private String keyClientLog;
	TelnetClientUtil telnetClient;
	protected Logger logger = Logger.getLogger(getClass());
	private int index;
	private static final  int RUNNING = 0;
	private static final  int PAUSE = 1;
	private static final int STOP = 2;
	private static final  int RETRY = 3;
	private static final  int NOT_RUN = 4;
	
	
	public TraceRouteThread(ENodeBPlan eNodeBPlan, int type, String keyClientLog, int index) {
		super();
		this.eNodeBPlan = eNodeBPlan;
		this.type = type;
		this.keyClientLog = keyClientLog;
		this.index = index;
	}


	@Override
	public void run() {
		state.setStatus(RUNNING);
		state.setError(false);
		try {
			if(type==1)
				pingSrtToEmm();
					
		} catch (Exception e) {
			logger.debug(e.getMessage(), e);
			pullAppendLog(e.getMessage());
			getState().setError(true);
			getState().setDetail(e.getMessage());
		} finally {
			getState().status = (STOP);
		}
	}

	 
	private void pingSrtToEmm() {
		// TODO Auto-generated method stub
		
	}


	private void execute(String tmpCmd) throws Exception{	
		if (telnetClient == null || !telnetClient.isConnected())
			//if (!connect())
				return;
		
		{
			
//			String cmd = tmpCmd;
//			if (cmd.length() > 0) {
//				//String curLog;
//				new Thread(new PullAppendLog(this)).start();
//				Result rs = sshClient.sendLineWithTimeOutNew(cmd, 30000);
//				//curLog = rs.getResult();
//				//pullAppendLog(curLog);
//				if (rs.getResultOfCommand().isEmpty())
//					singleCmd.setStatus(SingleCommand.OK);
//				else {
//					singleCmd.setStatus(SingleCommand.NOK);
//				}
//				checkPauseThread();
//				if (isStop())
//					return;
//			}
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void pullAppendLog(String log) {
		Date now = new Date();
		ConcurrentMap<Integer, String> map = (ConcurrentMap<Integer, String>) ObjectConcurrentMap.getInstance().get(keyClientLog);

		if (map == null) {
			map = new ConcurrentHashMap<Integer, String>();
		}
		if (log != null) {
			String oldLog = map.get(index);
			map.put(type, (oldLog == null ? sdFormat.format(now) : oldLog) + "\n\r" + log);
		}
		ObjectConcurrentMap.getInstance().put(keyClientLog, map);
	}


	public State getState() {
		return state;
	}


	public void setState(State state) {
		this.state = state;
	}
	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
	}
	public String getKeyClientLog() {
		return keyClientLog;
	}


	public void setKeyClientLog(String keyClientLog) {
		this.keyClientLog = keyClientLog;
	}
	@SuppressWarnings("serial")
	public class State implements Serializable{
		public boolean isError;
		public int status =-1;
		String detail;
		
		public boolean isError() {
			return isError;
		}
		public void setError(boolean isError) {
			this.isError = isError;
		}
		public String getDetail() {
			return detail;
		}
		public void setDetail(String detail) {
			this.detail = detail;
		}
		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}
		
		
		
	}
}
