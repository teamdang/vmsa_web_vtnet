package com.viettel.thread;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.viettel.util.ObjectConcurrentMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PullAppendLog implements Runnable {
	
	private TraceRouteThread traceRouteThread;
	SimpleDateFormat sdFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	public PullAppendLog(TraceRouteThread traceRouteThread) {
		this.traceRouteThread = traceRouteThread;
	}
	@Override
	public void run() {
		autoPullAppendLog();

	}
	@SuppressWarnings("unchecked")
	protected void autoPullAppendLog(){
		Date now = new Date();
		ConcurrentMap<Integer, String> map = (ConcurrentMap<Integer, String>) ObjectConcurrentMap.getInstance().get(traceRouteThread.getKeyClientLog());

		if (map == null) {
			map = new ConcurrentHashMap<Integer, String>();
		}
		String log = null;
		String oldLog = null;
		while(!traceRouteThread.telnetClient.isStopPullBuff()){
			log = traceRouteThread.telnetClient.getSbuff();
			if (log != null) {
				oldLog = map.get(traceRouteThread.getIndex());
				map.put(traceRouteThread.getIndex(), log);
			}
			ObjectConcurrentMap.getInstance().put(traceRouteThread.getKeyClientLog(), map);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			}
		}
		if(oldLog==null || oldLog.equalsIgnoreCase(sdFormat.format(now))){
			map.put(traceRouteThread.getIndex(), log);
			ObjectConcurrentMap.getInstance().put(traceRouteThread.getKeyClientLog(), map);
		}
	}

	 
}
