/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.viettel.webservice.object.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hienhv4
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface WSVMSA {
    //quytv7 create mop
    @WebMethod(operationName = "createDt")
    ResultDTOFlowRun createDt(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                              @WebParam(name = "workflow") String workflow, @WebParam(name = "paramValsDto") ResultParamValsDTO paramValsDto,
                              @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "username") String username,
                              @WebParam(name = "dtName") String dtName);
}
