/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.sun.net.httpserver.HttpExchange;
import com.viettel.controller.GenerateFlowRunController;
import com.viettel.model.ChangeFlowRunAction;
import com.viettel.model.ChangeParam;
import com.viettel.model.ChangePlan;
import com.viettel.model.FlowRunAction;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.model.ParamValue;
import com.viettel.model.StationConfigImport;
import com.viettel.object.GroupAction;
import com.viettel.persistence.ChangeFlowRunActionServiceImpl;
import com.viettel.persistence.ChangeParamServiceImpl;
import com.viettel.persistence.ChangePlanServiceImpl;
import com.viettel.persistence.DaoSimpleService;
import com.viettel.persistence.FlowRunActionServiceImpl;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.MapNodeServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.StationConfigImportlServiceImpl;
import com.viettel.util.Config;
import com.viettel.util.Constants;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionUtil;
import com.viettel.webservice.object.ChangeParamDTO;
import com.viettel.webservice.object.ChangeParamInputDTO;
import com.viettel.webservice.object.ResultDTO;
import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dunglv
 */
@WebService(endpointInterface = "com.viettel.webservice.WSForNCMS")
public class WSForNCMSImpl implements WSForNCMS {

    protected final Logger logger = LoggerFactory.getLogger(WSForNCMSImpl.class);
    @Resource
    private WebServiceContext context;

    public WebServiceContext getContext() {
        return context;
    }

    public void setContext(WebServiceContext context) {
        this.context = context;
    }

    @Override
    public ResultDTO updateChangeParam(ChangeParamInputDTO changeParam) {
//        if (!checkWhiteListIp()) {
//            return null;
//        }
        ResultDTO result = new ResultDTO();
        logger.error("**************** updateChangeParam START ********************");
        if (null != changeParam && changeParam.getChangeParam().size() > 0) {
            int resultCode = 0;
            try {
                switch (changeParam.getChangeParam().get(0).getType()) {
                    case Constants.ProcessPhase.CHANGE_ROLLBACK:
                        resultCode = processRollbackParam(changeParam.getChangeParam());
                        break;
                    case Constants.ProcessPhase.CHANGE_CHECK_RESULT:
                        resultCode = processCheckResultParam(changeParam.getChangeParam());
                        break;
                }

                switch (resultCode) {
                    case Constants.ResultType.PLAN_NOT_FOUND:
                        result.setResultCode(Constants.ResultType.PLAN_NOT_FOUND);
                        result.setResultMessage("Dữ liệu sự kiện không tồn tại");
                        break;
                    case Constants.ResultType.CREATE_DT_ERROR:
                        result.setResultCode(Constants.ResultType.CREATE_DT_ERROR);
                        result.setResultMessage("Dữ liệu sự kiện không tồn tại");
                        break;
                    case Constants.ResultType.SUCCESS:
                        result.setResultCode(Constants.ResultType.SUCCESS);
                        result.setResultMessage("SUCCESS");
                        break;
                }
                logger.error("**************** updateChangeParam END ********************");
                return result;
            } catch (Exception ex) {
                result.setResultCode(1);
                result.setResultMessage("Có lỗi xảy ra khi cập nhật tham số");
                logger.error(ex.getMessage(), ex);
                return result;
            }
        } else {
            result.setResultCode(2);
            result.setResultMessage("Dữ liệu sai định dạng");
            logger.error("Dữ liệu sai định dạng");
            return result;
        }
    }

    private int processRollbackParam(List<ChangeParamDTO> inputForm) throws Exception {
        logger.error("**************** processRollbackParam START ********************");
        return updateParam(Constants.ProcessPhase.CHANGE_ROLLBACK, inputForm);
    }

    private int processCheckResultParam(List<ChangeParamDTO> inputForm) throws Exception {
        logger.error("**************** processCheckResultParam START ********************");
        return updateParam(Constants.ProcessPhase.CHANGE_CHECK_RESULT, inputForm);
    }

    private int updateParam(int processPhase, List<ChangeParamDTO> inputForm) throws Exception {
        Map<String, String> rncMopParam = new HashMap<>();
        ChangeParamServiceImpl service = new ChangeParamServiceImpl();
        MapNodeServiceImpl mapNodeServiceImpl = new MapNodeServiceImpl();
        String nodeBList = "";
        String nodeBIpList = "";
        String cellList = "";
        String time = "";
        int nodeBParamCount = 0;
        int rncOrCellParamCount = 0;
        long now = System.currentTimeMillis();

        Map<String, Object> filters = new HashMap<>();
        filters.put("changePlan.id", inputForm.get(0).getPlanId());
        List<ChangeParam> changeParams = service.findList(filters);
        int updateResult;

        if (null == changeParams || changeParams.isEmpty()) {
            return Constants.ResultType.PLAN_NOT_FOUND;
        }

        for (ChangeParamDTO changeParamDTO : inputForm) {
            logger.info("ParamCode : " + changeParamDTO.getParamCode());
            logger.info("ParamValue : " + changeParamDTO.getParamValue());
            logger.info("ParamType : " + changeParamDTO.getParamType());
            logger.info("Cell : " + changeParamDTO.getCell());
            logger.info("NodeB : " + changeParamDTO.getNodeB());
            logger.info("Rnc : " + changeParamDTO.getRnc());
            logger.info("PlanId : " + changeParamDTO.getPlanId());
            logger.info("Type : " + changeParamDTO.getType());
            logger.info("NodeType : " + changeParamDTO.getNodeType());

            // Save all param to Database
            StringBuilder builder = new StringBuilder();
            builder.append("update ChangeParam set ");
            if (processPhase == Constants.ProcessPhase.CHANGE_ROLLBACK) {
                builder.append("paramValueOld = ? ");
            } else {
                builder.append("paramValueNew = ? ");
            }
            builder.append(", updateTime = ? ");
            builder.append("where paramCode = ? ");
            builder.append("and paramType = ? ");
            builder.append("and changePlan.id = ? ");

            if (changeParamDTO.getParamType() == Constants.ParamType.RNC) {
                logger.info("changeParamDTO.getParamType() == Constants.ParamType.RNC");
                builder.append("and rnc = ? ");
                updateResult = service.execteBulk(builder.toString(), changeParamDTO.getParamValue(), new Date(), changeParamDTO.getParamCode(), changeParamDTO.getParamType(), changeParamDTO.getPlanId(), changeParamDTO.getRnc());

                if (updateResult == 0) {
                    logger.error("Không tìm thấy param : " + changeParamDTO.getParamCode());
                    logger.error("Plan Id : " + changeParamDTO.getPlanId());
                    logger.error("Rnc : " + changeParamDTO.getRnc());
                    logger.error("Type : " + changeParamDTO.getParamType());
                }

                rncOrCellParamCount++;
            }

            if (changeParamDTO.getParamType() == Constants.ParamType.NODEB || changeParamDTO.getParamType() == Constants.ParamType.NODEBInRnc) {
                logger.info("changeParamDTO.getParamType() == Constants.ParamType.NODEB");
                builder.append("and rnc = ? ");
                builder.append("and nodeb = ? ");
                updateResult = service.execteBulk(builder.toString(), changeParamDTO.getParamValue(), new Date(), changeParamDTO.getParamCode(), changeParamDTO.getParamType(), changeParamDTO.getPlanId(), changeParamDTO.getRnc(), changeParamDTO.getNodeB());

                if (updateResult == 0) {
                    logger.error("Không tìm thấy param : " + changeParamDTO.getParamCode());
                    logger.error("Plan Id : " + changeParamDTO.getPlanId());
                    logger.error("Rnc : " + changeParamDTO.getRnc());
                    logger.error("Type : " + changeParamDTO.getParamType());
                }

                if (!nodeBList.contains(changeParamDTO.getNodeB())) {
                    nodeBList += changeParamDTO.getNodeB() + ";";

                    // Get nodeb ip
                    String sql = "select distinct NODEB_IP from Map_Node where RNC_BSC = ? and NODEB = ?";
                    List<String> nodeBIps = (List<String>) mapNodeServiceImpl.findListSQLAll(sql, changeParamDTO.getRnc(), changeParamDTO.getNodeB());
                    if (null != nodeBIps && nodeBIps.size() > 0) {
                        String nodebIp = nodeBIps.get(0);

                        String[] nodebIpArr = nodebIp.split("\\.");
                        String octet = String.valueOf(Integer.valueOf(nodebIpArr[1]) + 2);
                        nodebIp = nodebIpArr[0] + "." + octet + "." + nodebIpArr[2] + "." + nodebIpArr[3];

                        nodeBIpList += nodebIp + ";";
                    }

                    time += now++ + ";";
                }

                nodeBParamCount++;
            }

            if (changeParamDTO.getParamType() == Constants.ParamType.CELL) {
                logger.info("changeParamDTO.getParamType() == Constants.ParamType.CELL");
                builder.append("and rnc = ? ");
                builder.append("and cell = ? ");
                updateResult = service.execteBulk(builder.toString(), changeParamDTO.getParamValue(), new Date(), changeParamDTO.getParamCode(), changeParamDTO.getParamType(), changeParamDTO.getPlanId(), changeParamDTO.getRnc(), changeParamDTO.getCell());

                if (updateResult == 0) {
                    logger.error("Không tìm thấy param : " + changeParamDTO.getParamCode());
                    logger.error("Plan Id : " + changeParamDTO.getPlanId());
                    logger.error("Rnc : " + changeParamDTO.getRnc());
                    logger.error("Type : " + changeParamDTO.getParamType());
                }

                if (!cellList.contains(changeParamDTO.getCell())) {
                    cellList += changeParamDTO.getCell() + ";";
                }

                rncOrCellParamCount++;
            }

            // Create map param for generate rollback DT
            if (null == rncMopParam.get(changeParamDTO.getParamCode())) {
                rncMopParam.put(changeParamDTO.getParamCode(), changeParamDTO.getParamValue());
            } else {
                String value = rncMopParam.get(changeParamDTO.getParamCode()) + ";" + changeParamDTO.getParamValue();
                rncMopParam.put(changeParamDTO.getParamCode(), value);
            }
        }

        if (!"".equals(nodeBList)) {
            nodeBList = nodeBList.substring(0, nodeBList.length() - 1);
            rncMopParam.put("nodeb", nodeBList);
        }

        if (!"".equals(time)) {
            time = time.substring(0, time.length() - 1);
            rncMopParam.put("time", time);
        }
        now++;
        rncMopParam.put("timernc", String.valueOf(now));

        if (!"".equals(nodeBIpList)) {
            nodeBIpList = nodeBIpList.substring(0, nodeBIpList.length() - 1);
            rncMopParam.put("nodebip", nodeBIpList);
        }

        if (!"".equals(cellList)) {
            cellList = cellList.substring(0, cellList.length() - 1);
            rncMopParam.put("cell", cellList);
        }

        logger.info("rncMopParam start");
        for (Map.Entry<String, String> entrySet : rncMopParam.entrySet()) {
            logger.info("key : " + entrySet.getKey());
            logger.info("value : " + entrySet.getValue());
        }
        logger.info("rncMopParam end");

        if (processPhase == Constants.ProcessPhase.CHANGE_ROLLBACK) {
            if (!createRollbackDt(inputForm.get(0).getPlanId(), inputForm.get(0).getRnc(), rncMopParam, nodeBParamCount, rncOrCellParamCount)) {
                return Constants.ResultType.CREATE_DT_ERROR;
            }
        }

        return Constants.ResultType.SUCCESS;
    }

    private boolean createRollbackDt(Long planId, String rnc, Map<String, String> rncMopParam, int nodeBParamCount, int rncOrCellParamCount) throws Exception {
        // Get Plan
        ChangePlan changePlan = new ChangePlanServiceImpl().findById(planId);

        // Get config
        Map<String, Object> filters = new HashMap<>();
        filters.put("vendorName", changePlan.getVendor().getVendorName());
        filters.put("networkType", changePlan.getNetworkType());
        filters.put("isRule", Config.IS_RULE_CHANGE_FESTIVAL);
        List<StationConfigImport> stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
        if (null == stationConfigImports || stationConfigImports.size() == 0) {
            logger.error("Không tìm thấy cấu hình template.");
            return false;
        } else {
            FlowTemplates flowTemplates;
            GenerateFlowRunController generateFlowRunController;
            List<ChangeFlowRunAction> changeFlowRunActions = new ArrayList<>();
            if ("3G".equals(changePlan.getNetworkType())) {
                flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
            } else {
                return false;
            }

            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                logger.error(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                return false;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    logger.error(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    return false;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(changePlan.getNetworkType())) {
                    filePutOSS = changePlan.getCrNumber().toUpperCase()
                            + "-ROLLBACK"
                            + "-Change_Param_Festival-" + changePlan.getVendor().getVendorName().toUpperCase()
                            + "-" + changePlan.getNetworkType().toUpperCase()
                            + "-RNC_" + rnc.toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    return false;
                }
                generateFlowRunController = new GenerateFlowRunController();
                FlowRunAction flowRunAction = new FlowRunAction();
                flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
                flowRunAction.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);

                //Lay danh sach param tu bang 
                generateFlowRunController.setNodes(new ArrayList<Node>());
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                filters = new HashMap<>();
                filters.put("nodeCode", rnc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new Exception("Khong tim thay node RNC hoac tim thay lon hon 1 ban ghi");
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }

                if ("3G".equals(changePlan.getNetworkType())) {
                    rncMopParam.put("file_name", filePutOSS);
                    rncMopParam.put("rnc", rnc);
                }

                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        logger.info("Show ParamCode: " + paramValue.getParamCode());

                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }

                        Object value = null;
                        try {
                            value = rncMopParam.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }

                        ResourceBundle bundle = ResourceBundle.getBundle("cas");
                        if (bundle.getString("service").contains("10.61.127.190")) {
                            if (value == null || value.toString().isEmpty()) {
                                value = "TEST_NOT_FOUND";
                            }
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
                    }
                }
                rncMopParam.clear();
//                for (GroupAction groupAction : nodeRncBsc.getGroupActions()) {

                for (GroupAction groupAction : generateFlowRunController.getMapGroupAction().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                    if (groupAction.getGroupActionName().equals(MessageUtil.getResourceBundleMessage("label.changeParam.run.dt.nodeB")) && nodeBParamCount == 0) {
                        groupAction.setDeclare(false);
                    }
                    if (groupAction.getGroupActionName().equals(MessageUtil.getResourceBundleMessage("label.changeParam.run.dt.rnc")) && rncOrCellParamCount == 0) {
                        groupAction.setDeclare(false);
                    }
                }

                boolean saveDT = generateFlowRunController.saveDTFromService();
                if (saveDT) {
                    try {
                        // Save flow to database                           
                        ChangeFlowRunAction changeFlowRunAction = new ChangeFlowRunAction();
                        changeFlowRunAction.setFlowRunAction(flowRunAction);
                        changeFlowRunAction.setNode(nodeRncBsc);
                        changeFlowRunAction.setChangePlan(changePlan);
                        changeFlowRunAction.setType(1L);
                        changeFlowRunAction.setUpdateTime(new Date());
                        changeFlowRunActions.add(changeFlowRunAction);
                        logger.info("Sinh Mop Rollback thành công node RNC:" + rnc);
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new Exception(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
            } finally {
                //Tam biet
            }

            ChangeFlowRunActionServiceImpl changeFlowRunActionServiceImpl = new ChangeFlowRunActionServiceImpl();

            // Get rnc node id
            filters.clear();
            filters.put("nodeCode", rnc);
            List<Node> rncNode = new NodeServiceImpl().findList(filters);

            if (rncNode.size() > 0) {
                // Get flow run id of current plan
                filters.clear();
                filters.put("changePlan.id", planId);
                filters.put("node.nodeId", rncNode.get(0).getNodeId());
                List<ChangeFlowRunAction> currentFlowRunActions = changeFlowRunActionServiceImpl.findList(filters);

                if (currentFlowRunActions.size() > 0) {
                    // Save rollback flow
                    changeFlowRunActionServiceImpl.saveOrUpdate(changeFlowRunActions);

                    FlowRunActionServiceImpl flowRunActionServiceImpl = new FlowRunActionServiceImpl();
                    for (ChangeFlowRunAction changeFlowRunAction : changeFlowRunActions) {
                        FlowRunAction flowRunAction = flowRunActionServiceImpl.findById(currentFlowRunActions.get(0).getFlowRunAction().getFlowRunId());
                        flowRunAction.setFlowRunRollbackId(changeFlowRunAction.getFlowRunAction().getFlowRunId());
                        flowRunActionServiceImpl.saveOrUpdate(flowRunAction);
                    }
                }
            }

            return true;
        }
    }

    private boolean checkWhiteListIp() {
        try {
            HttpExchange exchange = (HttpExchange) context.getMessageContext().get("com.sun.xml.internal.ws.http.exchange");
            InetSocketAddress remoteAddress = exchange.getRemoteAddress();
            String remoteHost = remoteAddress.getAddress().getHostAddress();

            List<?> ips = new DaoSimpleService().findListSQLAll("SELECT IP from WHITELIST_WS WHERE SYSTEM_NAME='WS_FOR_NCMS'");
            logger.info("IP remote: " + remoteHost);
            if (ips == null || ips.isEmpty()) {
                return true;
            }
            for (Object ip : ips) {
                if (ip != null && ip.equals(remoteHost)) {
                    return true;
                }
            }
            logger.info("Forbidden access: " + remoteHost);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return false;
    }
}
