/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice.ncms.client;

import com.viettel.model.ChangeParam;
import com.viettel.ncms.webservice.ChangeParamForm;
import com.viettel.ncms.webservice.InputChangeParamForm;
import com.viettel.ncms.webservice.NCMSWS;
import com.viettel.ncms.webservice.NCMSWS_Service;
import com.viettel.util.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author taitd
 */
public class NCMSWebServiceClient {
    
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    
    public void requestOldParams(Long nodeType,List<ChangeParam> changeParams, Map<String, String> nodeBIpMap) {
        List<ChangeParamForm> requestParams;
        Map<String, List<ChangeParamForm>> rncParamMap = new HashMap<>();
        
        // Build request param
        for (ChangeParam changeParam : changeParams) {
            ChangeParamForm changeParamDTO = new ChangeParamForm();
            changeParamDTO.setCell(changeParam.getCell());
            changeParamDTO.setNodeB(changeParam.getNodeb());
            changeParamDTO.setNodeBIp(nodeBIpMap.get(changeParam.getNodeb()));
            changeParamDTO.setParamCode(changeParam.getParamCode());
//            changeParamDTO.setParamValue(changeParam.getParamValueInput());
            changeParamDTO.setPlanId(String.valueOf(changeParam.getChangePlan().getId()));
            changeParamDTO.setBscRnc(changeParam.getRnc());
            changeParamDTO.setParamType(changeParam.getParamType());
            changeParamDTO.setType(Long.parseLong(Constants.ProcessPhase.CHANGE_ROLLBACK + ""));
            
            if(rncParamMap.get(changeParam.getRnc()) == null) {
                requestParams = new ArrayList<>();
                requestParams.add(changeParamDTO);
                rncParamMap.put(changeParam.getRnc(), requestParams);
            } else {
                requestParams = rncParamMap.get(changeParam.getRnc());
                requestParams.add(changeParamDTO);
            }
        }
        
        for (Map.Entry<String, List<ChangeParamForm>> entrySet : rncParamMap.entrySet()) {
            List<ChangeParamForm> params = entrySet.getValue();
            InputChangeParamForm inputChangeParamForm;
            List<ChangeParamForm> changeParamList;
            
            //Set timeout until the response is received
            try {
                NCMSWS_Service service = new NCMSWS_Service();
                NCMSWS ncmsws = service.getNCMSWSPort();

//                Map<String, Object> context = ((BindingProvider) ncmsws).getRequestContext();
                //Set timeout params
//                context.put("com.sun.xml.internal.ws.connect.timeout", Constants.NCMS_REQUEST_TIMEOUT);
//                context.put("com.sun.xml.internal.ws.request.timeout", Constants.NCMS_REQUEST_TIMEOUT);
//                context.put("com.sun.xml.ws.request.timeout", Constants.NCMS_REQUEST_TIMEOUT);
//                context.put("com.sun.xml.ws.connect.timeout", Constants.NCMS_REQUEST_TIMEOUT);

                // Don't need to wait the response
                inputChangeParamForm = new InputChangeParamForm();
                changeParamList = inputChangeParamForm.getChangeParamList();
                changeParamList.addAll(params);
                inputChangeParamForm.setNodeType(nodeType);
                ncmsws.getChangeParam(inputChangeParamForm);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }
}
