/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.gnoc.cr.service.*;
import com.viettel.model.*;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.nims.infra.webservice.CheckCellForCRForm;
import com.viettel.nims.infra.webservice.UpdateInfraWS;
import com.viettel.nims.infra.webservice.UpdateInfraWSService;
import com.viettel.object.AccountObj;
import com.viettel.object.MessageException;
import com.viettel.object.MessageObject;
import com.viettel.passprotector.PassProtector;
import com.viettel.persistence.*;
import com.viettel.util.*;
import com.viettel.webservice.object.*;
import com.viettel.webservice.object.MopDetailDTO;
import com.viettel.webservice.object.ResultDTO;
import com.viettel.webservice.utils.ConstantsWS;
import com.viettel.webservice.utils.MopUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.hibernate.HibernateException;

/**
 * @author hienhv4
 */
@WebService(endpointInterface = "com.viettel.webservice.WSForGNOC")
public class WSForGNOCImpl implements WSForGNOC {

    protected static final Logger logger = LoggerFactory.getLogger(WSForGNOCImpl.class);
    public static final int RESPONSE_SUCCESS = 1;
    public static final int RESPONSE_FAIL = 0;

    @Resource
    private static WebServiceContext context;

    @Override
    public ResultDTO updateCrCode(String userService, String passService, String mopId, String crCode) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultDTO result = new ResultDTO();
        Long mopIdLocal;
        logger.info("updateCrCode mopId: " + mopId + ", crCode: " + crCode);
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        }

        try {
            mopIdLocal = Long.parseLong(mopId);
        } catch (Exception ex) {
            result.setResultCode(3);
            result.setResultMessage("mopId phải là số nguyên");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        try {
            FlowRunActionServiceImpl service = new FlowRunActionServiceImpl();
            FlowRunAction flowRun = service.findById(mopIdLocal);

            if (flowRun == null) {
                result.setResultCode(4);
                result.setResultMessage("MOP không tồn tại");
                return result;
            }

            CrLogFile crLogFile = new CrLogFile();
            crLogFile.setCrNumber(crCode);
            new CrLogFileServiceImpl().saveOrUpdate(crLogFile);

            flowRun.setCrNumber(crCode);
            service.saveOrUpdate(flowRun);

            result.setResultCode(0);
            result.setResultMessage("SUCCESS");
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ResultDTO updateCrStatus(String userService, String passService, String crCode, String status) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultDTO result = new ResultDTO();
        Long statusLocal;
        logger.info("updateCrStatus status: " + status + ", crCode: " + crCode);
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        }

        try {
            statusLocal = Long.parseLong(status);
        } catch (Exception ex) {
            result.setResultCode(3);
            result.setResultMessage("status phải là số nguyên");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        try {
            FlowRunActionServiceImpl service = new FlowRunActionServiceImpl();

            Map<String, Object> filters = new HashMap<>();
            filters.put("crNumber-" + FlowRunActionServiceImpl.EXAC, crCode);

            List<FlowRunAction> flowRuns = service.findList(filters);

            if (flowRuns != null && !flowRuns.isEmpty()) {
                for (FlowRunAction flow : flowRuns) {
                    if (statusLocal == 6) {
                        flow.setStatus(1l);
                    }
                    flow.setCrStatus(statusLocal);
                }
            }

            service.saveOrUpdate(flowRuns);

            result.setResultCode(0);
            result.setResultMessage("SUCCESS");
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public MopOutputDTO getMopByUser(String userService, String passService, String username) {
        if (!checkWhiteListIp()) {
            return null;
        }
        MopOutputDTO output = new MopOutputDTO();
        logger.info("getMopByUser username: " + username);
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                output.setResultCode(2);
                output.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return output;
            }
        } catch (Exception ex) {
            output.setResultCode(1);
            output.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return output;
        }

        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("createBy-" + FlowRunActionServiceImpl.EXAC, username);
            filters.put("status", 0l);

            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("createDate", "DESC");

            List<FlowRunAction> flowRunActions = (new FlowRunActionServiceImpl()).findList(filters, orders);

            List<MopDTO> mopDTOs = new ArrayList<>();
            if (flowRunActions != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HHmmss");
                for (FlowRunAction flow : flowRunActions) {
                    if (flow.getCrNumber() != null && (flow.getCrNumber().equals(Config.CR_DEFAULT)
                            || flow.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER))) {
                        MopDTO mopDTO = new MopDTO();
                        mopDTO.setCreateTime(sdf.format(flow.getCreateDate()));
                        mopDTO.setMopId(flow.getFlowRunId().toString());
                        mopDTO.setMopName(flow.getFlowRunName());
                        mopDTO.setTemplateName(flow.getFlowTemplates().getFlowTemplateName());

                        mopDTOs.add(mopDTO);
                    }
                }
            }
            output.setResultCode(0);
            output.setResultMessage("SUCCESS");
            output.setMops(mopDTOs);
        } catch (Exception ex) {
            output.setResultCode(1);
            output.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return output;
    }

    @Override
    public MopDetailOutputDTO getMopInfo(String userService, String passService, String mopId) {
        if (!checkWhiteListIp()) {
            return null;
        }
        MopDetailOutputDTO output = new MopDetailOutputDTO();
        logger.info("getMopInfo mopId: " + mopId);
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                output.setResultCode(2);
                output.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return output;
            }
        } catch (Exception ex) {
            output.setResultCode(1);
            output.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return output;
        }

        //FileOutputStream os = null;
        //File fileOut = null;
        try {
            FlowRunAction flowRun = (new FlowRunActionServiceImpl()).findById(Long.parseLong(mopId));
            List<Node> lstNode = new ArrayList<>();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HHmmss");
            MopDetailDTO mopDetail = new MopDetailDTO();
            mopDetail.setCreateTime(sdf.format(flowRun.getCreateDate()));
            mopDetail.setMopId(flowRun.getFlowRunId().toString());
            mopDetail.setMopName(flowRun.getFlowRunName());
            mopDetail.setTemplateName(flowRun.getFlowTemplates().getFlowTemplateName());

            //set danh sach node mang tac dong
            if (flowRun.getNodeRuns() != null) {
                mopDetail.setNodes(new ArrayList<NodeDTO>());
                for (NodeRun nodeRun : flowRun.getNodeRuns()) {
                    mopDetail.getNodes().add(new NodeDTO(nodeRun.getNode().getNodeCode(), nodeRun.getNode().getNodeIp()));
                    if (!lstNode.contains(nodeRun.getNode())) {
                        lstNode.add(nodeRun.getNode());
                    }
                }
            }

            //ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            mopDetail.setMopFileContent(Base64.encodeBase64String(flowRun.getFileContent()));
            mopDetail.setMopFileName(ZipUtils.getSafeFileName(flowRun.getFlowRunId()
                    + "_" + ZipUtils.clearHornUnicode(flowRun.getFlowRunName()) + ".xlsx"));
            mopDetail.setMopFileType("xlsx");

//            File fileKpi = new File(servletContext.getRealPath("/")
//                    + File.separator + "templates" + File.separator
//                    + "Form_KPI.xlsx");
//            mopDetail.setKpiFileContent(Base64.encodeBase64String(FileUtils.readFileToByteArray(fileKpi)));
//            mopDetail.setKpiFileType("xlsx");
//            mopDetail.setKpiFileName("Form_KPI.xlsx");
            output.setResultCode(0);
            output.setResultMessage("SUCCESS");
            output.setMopDetailDTO(mopDetail);
            return output;
        } catch (Exception ex) {
            output.setResultCode(1);
            output.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return output;
        }
    }

    @Override
    public ResultDTO updateCrCodeMops(String userService, String passService, String mopIds, String crCode) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultDTO result = new ResultDTO();
        logger.info("updateCrCode crCode: " + crCode + ", mopIds: " + mopIds);
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        }

        List<Long> mopIdLocals = new ArrayList<>();
        try {
            if (mopIds != null) {
                String[] arrs = mopIds.split(",");
                for (String mopId : arrs) {
                    mopIdLocals.add(Long.parseLong(mopId.trim()));
                }
            }
        } catch (Exception ex) {
            result.setResultCode(3);
            result.setResultMessage("mopId phải là số nguyên");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        try {
            if (mopIdLocals.size() > 0) {
                FlowRunActionServiceImpl flowService = new FlowRunActionServiceImpl();

                Map<String, Object> mapFilter = new HashMap<>();
                mapFilter.put("flowRunId", mopIdLocals);
                List<FlowRunAction> lstFlow = flowService.findList(mapFilter);

                if (lstFlow != null && !lstFlow.isEmpty()) {
                    for (FlowRunAction flow : lstFlow) {
                        flow.setCrNumber(crCode);
                    }
                    flowService.saveOrUpdate(lstFlow);
                }

                CrLogFile crLogFile = new CrLogFile();
                crLogFile.setCrNumber(crCode);
                new CrLogFileServiceImpl().saveOrUpdate(crLogFile);
            }

            result.setResultCode(0);
            result.setResultMessage("SUCCESS");
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ResultDTO updateCrStatusWithMops(String userService, String passService, String crCode, String status, String mopIds) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultDTO result = new ResultDTO();
        Long statusLocal;
        logger.info("updateCrStatus status: " + status + ", crCode: " + crCode + ", mopIds: " + mopIds);

        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        }
        try {
            statusLocal = Long.parseLong(status);
        } catch (Exception ex) {
            result.setResultCode(3);
            result.setResultMessage("status phải là số nguyên");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        try {
            FlowRunActionServiceImpl service = new FlowRunActionServiceImpl();

            Map<String, Object> filters = new HashMap<>();
            filters.put("crNumber-" + FlowRunActionServiceImpl.EXAC, crCode);

            List<FlowRunAction> flowRunsExist = service.findList(filters);

//            if (flowRuns != null && !flowRuns.isEmpty()) {
//                logger.info("Ton tai " + flowRuns.size() + " mops cho CR: " + crCode);
//                for (FlowRunAction flow : flowRuns) {
//                    if (statusLocal == 6) {
//                        flow.setStatus(1l);
//                    }
//                    flow.setCrStatus(statusLocal);
//
//                }
//                service.saveOrUpdate(flowRuns);
//            } else {
//                logger.info("Khong ton tai mop cho CR: " + crCode);
//
//                List<Long> mopIdLocals = new ArrayList<>();
//                try {
//                    if (mopIds != null) {
//                        String[] arrs = mopIds.split(",");
//                        for (String mopId : arrs) {
//                            mopIdLocals.add(Long.parseLong(mopId.trim()));
//                        }
//                    }
//                } catch (Exception ex) {
//                    result.setResultCode(4);
//                    result.setResultMessage("mopId phải là số nguyên");
//                    logger.error(ex.getMessage(), ex);
//                    return result;
//                }
//
//                Map<String, Object> filters1 = new HashMap<>();
//                filters1.put("flowRunId", mopIdLocals);
//
//                flowRuns = service.findList(filters1);
//
//                if (flowRuns != null && !flowRuns.isEmpty()) {
//                    logger.info("Ton tai " + flowRuns.size() + " mops");
//                    for (FlowRunAction flow : flowRuns) {
//                        if (statusLocal == 6) {
//                            flow.setStatus(1l);
//                        }
//                        flow.setCrStatus(statusLocal);
//                        flow.setCrNumber(crCode);
//                    }
//                    service.saveOrUpdate(flowRuns);
//                }
//            }

            //20171211_Quytv7_update_crNumber_va status_start
            List<FlowRunAction> flowRuns;
            logger.info("So Mop CR truyen sang: " + mopIds);

            List<Long> mopIdLocals = new ArrayList<>();
            try {
                if (mopIds != null) {
                    String[] arrs = mopIds.split(",");
                    for (String mopId : arrs) {
                        mopIdLocals.add(Long.parseLong(mopId.trim()));
                    }
                }
            } catch (Exception ex) {
                result.setResultCode(4);
                result.setResultMessage("mopId phải là số nguyên");
                logger.error(ex.getMessage(), ex);
                return result;
            }

            Map<String, Object> filters1 = new HashMap<>();
            filters1.put("flowRunId", mopIdLocals);

            flowRuns = service.findList(filters1);
            if (flowRunsExist != null && !flowRunsExist.isEmpty()) {
                logger.info("Ton tai " + flowRunsExist.size() + " mops cho CR: " + crCode);
                for (FlowRunAction flow : flowRunsExist) {
                    logger.info("Gan lai status va cr number cho cac dt da map voi CR nay: " + flow.getFlowRunId());
                    if (flowRuns != null && flowRuns.size() > 0) {
                        logger.info("Gan lai status =0, cr_number = '',crStatus = null cho cac dt da map voi CR nay: " + flow.getFlowRunId());
                        flow.setStatus(0l);
                        flow.setCrStatus(null);
                        flow.setCrNumber("");
                    } else {
                        logger.info("Cap nhat status dt da map voi CR nay: " + flow.getFlowRunId());
                        if (statusLocal == 6) {
                            flow.setStatus(1l);
                        }
                        flow.setCrStatus(statusLocal);
                    }
                }
                service.saveOrUpdate(flowRunsExist);
            }
            if (flowRuns != null && !flowRuns.isEmpty()) {
                logger.info("Ton tai " + flowRuns.size() + " mops");

                for (FlowRunAction flow : flowRuns) {
                    logger.info("Gan lai status va cr number cho cac dt moi voi CR nay: " + flow.getFlowRunId());
                    if (statusLocal == 6) {
                        flow.setStatus(1l);
                    }
                    flow.setCrStatus(statusLocal);
                    flow.setCrNumber(crCode);
                }
                service.saveOrUpdate(flowRuns);
            }

            //20171211_Quytv7_update_set_crNumber_da co mop_end

            result.setResultCode(0);
            result.setResultMessage("SUCCESS");
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        return result;
    }

    @Override
    public ResultTemplateGroupsDTO getListTemplateGroup(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultTemplateGroupsDTO result = new ResultTemplateGroupsDTO();
        logger.info("start get list template group mop");
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        }

        ArrayList<TemplateGroupsDTO> lstTemplateGroup = new ArrayList<>();
        try {
            List<TemplateGroup> lstGroup = new TemplateGroupServiceImpl().findList();
            if (lstGroup != null) {
                for (TemplateGroup group : lstGroup) {
                    lstTemplateGroup.add(new TemplateGroupsDTO(group.getId(), group.getGroupName()));
                }
            }
            result.setMessage("SUCCESS");
            result.setLstTemplateGroup(lstTemplateGroup);
            logger.info("List template group size: " + lstTemplateGroup.size());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public ResultFlowTemplatesDTO getListMopByGroup(@WebParam(name = "userService") String userService,
                                                    @WebParam(name = "passService") String passService,
                                                    @WebParam(name = "templateGroupId") Long templateGroupId) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultFlowTemplatesDTO result = new ResultFlowTemplatesDTO();
        result.setMessage("");
        logger.info("start get list template mop");
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        }

        ArrayList<FlowTemplatesDTO> lsTemplate = new ArrayList<>();
        try {
            Map<String, Object> filters = new HashedMap();
            filters.put("templateGroup.id", templateGroupId);
            List<FlowTemplates> lstTemplate = new FlowTemplatesServiceImpl().findList(filters);
            if (lstTemplate != null) {
                for (FlowTemplates template : lstTemplate) {
                    lsTemplate.add(new FlowTemplatesDTO(template.getTemplateGroup().getId(),
                            template.getFlowTemplatesId(), template.getFlowTemplateName(), template.getFlowTemplateName()));
                }
            }
            result.setLstFlowTemplate(lsTemplate);
            logger.info("List template size: " + lsTemplate.size());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public ResultParamValsDTO getListParamInputByCell(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                                      @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "cellCodes") String cellCodes) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultParamValsDTO resultGetParamInputDTO = new ResultParamValsDTO();
        logger.info("start get param input mop");
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                resultGetParamInputDTO.setMessages("Username or password is not correct");
                return resultGetParamInputDTO;
            }
        } catch (Exception ex) {
            resultGetParamInputDTO.setMessages("Error when authenticate user/pass");
            logger.error(ex.getMessage(), ex);
            return resultGetParamInputDTO;
        }

        if (cellCodes == null || cellCodes.trim().isEmpty()) {
            resultGetParamInputDTO.setMessages("Cell name was not filled");
            return resultGetParamInputDTO;
        }

        if (flowTemplateId == null || flowTemplateId == 0) {
            resultGetParamInputDTO.setMessages("Mop run was not selected");
            return resultGetParamInputDTO;
        }

        try {
            String content = "";
            List<String> lstCellCode = Arrays.asList(cellCodes.trim().split(";"));
            for (String cell : lstCellCode) {
                logger.info(">>>>>>>>>>> cell: " + cell);
            }

            // get bsc/rnc node from list cell code
            Map<String, Object> filters = new HashedMap();
            Map<Long, Node> mapNode = new HashMap<>();
            for (String cell : lstCellCode) {
                filters.clear();
                filters.put("cellCode", cell);
                List<MapNode> lstMapNode = new ArrayList<>();
                try {
                    lstMapNode = new MapNodeServiceImpl().findListExac(filters, null);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    lstMapNode = null;
                }

                if (lstMapNode != null && !lstMapNode.isEmpty()) {
                    filters.clear();
                    filters.put("nodeCode", lstMapNode.get(0).getRncBsc());
                    List<Node> lstNode = new NodeServiceImpl().findListExac(filters, null);
                    if (lstNode != null && !lstNode.isEmpty()) {
                        mapNode.put(lstNode.get(0).getNodeId(), lstNode.get(0));
                    } else {
                        resultGetParamInputDTO.setMessages("Can not getted node information " + lstMapNode.get(0).getRncBsc());
                        return resultGetParamInputDTO;
                    }
                } else {
                    resultGetParamInputDTO.setMessages("Can not getted information bettwen cell: " + cell + " and bsc/rnc node ");
                    return resultGetParamInputDTO;
                }
            }

            if (mapNode != null && !mapNode.isEmpty()) {

                /*
                 Kiem tra cac cell co nam trong danh sach duoc phep chay test hay khong
                 */
//                for (Map.Entry<Long, Node> entry : mapNode.entrySet()) {
//                    if (WhilelistNodeIns.getIns().getBscRncWhilelist().get(entry.getValue().getNodeCode().toUpperCase()) == null) {
//                        resultGetParamInputDTO.setMessages("Node is not ollow impact");
//                        return resultGetParamInputDTO;
//                    }
//                }
                for (Map.Entry<Long, Node> entry : mapNode.entrySet()) {

                    List<ParamValue> lstParamValue = new MopUtils().getLstParamInput(entry.getValue(), flowTemplateId);
                    if (lstParamValue == null || lstParamValue.isEmpty()) {
                        resultGetParamInputDTO.setMessages("Can not get parameter from node: " + entry.getValue().getNodeCode());
                        return resultGetParamInputDTO;
                    } else {

                        // Xoa cac tham so la tham so tham chieu
                        List<ParamValue> lstParamValueTmp = new ArrayList<>();
                        for (ParamValue p : lstParamValue) {
                            if (!p.isDisableByInOut()) {
                                lstParamValueTmp.add(p);
                            }
                        }
                        // Xoa bo cac tham so trung nhau
                        lstParamValueTmp = new MopUtils().distinctParamValueSameParamCode(lstParamValueTmp);

                        if (lstParamValueTmp != null) {
                            ParamValuesDTO paramDto;
                            ArrayList<ParamValuesDTO> lstParamValueDto = new ArrayList<>();

                            for (ParamValue p : lstParamValueTmp) {
                                paramDto = new ParamValuesDTO();
                                paramDto.setDescription(p.getDescription() == null ? "" : p.getDescription());
                                paramDto.setDisableByInOut(p.isDisableByInOut());
                                paramDto.setFormula(p.getFormula() == null ? "" : p.getFormula());
                                paramDto.setGroupCode(p.getGroupCode());
                                paramDto.setParamInputId(p.getParamInput().getParamInputId());
                                paramDto.setParamLabel(p.getParamCode());
                                paramDto.setParamCode(p.getParamCode());
                                // set gia tri cell doi voi nghiep vu halted, nguoi dung khong phai nhap lai nua
                                if ("noc_ha_cell_name".equalsIgnoreCase(p.getParamCode().trim())) {
                                    paramDto.setParamValue(cellCodes);
                                } else {
                                    paramDto.setParamValue("");
                                }

                                lstParamValueDto.add(paramDto);
                            }

                            ArrayList<ParamValuesDTO> lstAllParamValueMop = new ArrayList<>();
                            for (ParamValue p : lstParamValue) {
                                paramDto = new ParamValuesDTO();
                                paramDto.setDescription(p.getDescription() == null ? "" : p.getDescription());
                                paramDto.setDisableByInOut(p.isDisableByInOut());
                                paramDto.setFormula(p.getFormula() == null ? "" : p.getFormula());
                                paramDto.setGroupCode(p.getGroupCode());
                                paramDto.setParamInputId(p.getParamInput().getParamInputId());
                                paramDto.setParamLabel(p.getParamCode());
                                paramDto.setParamCode(p.getParamCode());
                                paramDto.setParamValue("");
                                lstAllParamValueMop.add(paramDto);
                            }

                            ParamValsOfNodeDTO paramsNode = new ParamValsOfNodeDTO();
                            paramsNode.setLstAllParamOfMop(lstAllParamValueMop);
                            paramsNode.setLstParamValues(lstParamValueDto);
                            paramsNode.setNodeId(entry.getValue().getNodeId());
                            paramsNode.setMessage("");
                            paramsNode.setNodeCode(entry.getValue().getNodeCode());

                            resultGetParamInputDTO.getLstParamValOfNode().add(paramsNode);
                        } else {
                            resultGetParamInputDTO.setMessages("Can not get distinct parameter from node: " + entry.getValue().getNodeCode());
                            return resultGetParamInputDTO;
                        }
                    }
                }
            } else {
                resultGetParamInputDTO.setMessages("Can not get node cell");
                return resultGetParamInputDTO;
            }

            resultGetParamInputDTO.setMessages(content.trim().isEmpty() ? "SUCCESS" : content);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return resultGetParamInputDTO;
    }

    /*public static void main(String args[]) {
     try {

     //            WSForGNOCImpl ws = new WSForGNOCImpl();
     //            ws.getParamResetMop("", "", 16726l, "WO_TT_20170809_14763940");
     //            JsonParser parser = new JsonParser();
     //            JsonObject o = parser.parse("{\"a\": \"A\"}").getAsJsonObject();
     //            ConnectionPoolRedis.getRedis().del("10.60.49.100");
     //            System.out.println(PassProtector.encrypt("annv162", "ipchange"));
     //            System.out.println(PassProtector.encrypt("Matkhau123!", "ipchange"));
     //            System.out.print(PassProtector.decrypt("25K36qoB0P+k0KJi47v2Oj07ygRWtaBHtAcqAcCC3aFytqNI4t4Z6yV3Xg4nl2Rp", "ipchange"));
     System.out.println(PassProtector.decrypt("25K36qoB0P+k0KJi47v2Oj07ygRWtaBHtAcqAcCC3aFytqNI4t4Z6yV3Xg4nl2Rp", "ipchange"));
     System.out.println(PassProtector.decrypt("o/vrPA+mB5cvwxK4qYkLEqC+cHNWEwPqsXrSOqoO8ikz8ljFL5xt/Nx3oOqg8ep5RWmXE9aQwNeB1Er/mlo6/A==", "ipchange"));
     //            System.err.print(PassProtector.encrypt("Redis#2017", "ipchange"));

     //            ResultFlowTemplatesDTO result = new ResultFlowTemplatesDTO();
     //            System.out.println(sdf1.format(new Date()));
     // Lay thong tin vendor, type dua theo workorder code
     //            try {
     //                AuthorityBO authorityBO = new AuthorityBO();
     //                authorityBO.setUserName(ConstantsWS.WEBSERVICE_NOCPRO_USER);
     //                authorityBO.setPassword(ConstantsWS.WEBSERVICE_NOCPRO_PASS);
     //                authorityBO.setRequestId(ConstantsWS.WEBSERVICE_NOCPRO_RESQUEST_ID);
     //
     //                ParameterBO parameterBO = new ParameterBO();
     //                parameterBO.setName("woTroubleCode");
     //                parameterBO.setValue("WO_TT_20170809_14763940");
     //                parameterBO.setType("String");
     //
     //                RequestInputBO inputBO = new RequestInputBO();
     //                inputBO.setCode("VMSA_ACCESS_MANUAL");
     //                inputBO.getParams().add(parameterBO);
     //
     //                JsonResponseBO resultData = NocProWebserviceUtils.service.getDataJson(authorityBO, inputBO);
     //                JsonElement jsonElement = new JsonParser().parse(resultData.getDataJson());
     //                System.out.println(jsonElement.getAsJsonObject().getAsJsonArray("data").get(0).toString());
     //
     //                Gson gson = (new GsonBuilder()).create();
     //                JsonNocproData nocproData = gson.fromJson(jsonElement.getAsJsonObject().getAsJsonArray("data").get(0).toString(), JsonNocproData.class);
     //
     //                // Lay danh sach cac mop co the
     //                Map<String, Object> filters = new HashedMap();
     //                if (nocproData.getVendor() != null) {
     //                    filters.put("vendor", nocproData.getVendor().trim().toUpperCase());
     //                }
     //                if (nocproData.getType_station() != null) {
     //                    filters.put("type", nocproData.getType_station());
     //                }
     //                List<AutoManResetMopConfig> manResetMops = new AutoManResetMopConfigServiceImpl().findList(filters);
     //                if (manResetMops != null) {
     //                    filters.clear();
     //                    filters.put("templateGroup.id", ConstantsWS.MANUAL_RESET_TEMPLATE_GROUP);
     //                    for (AutoManResetMopConfig mop : manResetMops) {
     //                        filters.put("resetType", Long.valueOf(mop.getResetType()));
     //                        List<FlowTemplates> templates = new FlowTemplatesServiceImpl().findList(filters);
     //                        if (templates != null) {
     //                            for (FlowTemplates template : templates) {
     //                                FlowTemplatesDTO templatesDTO = new FlowTemplatesDTO();
     //                                templatesDTO.setDesc(template.getFlowTemplateName());
     //                                templatesDTO.setTemplateGroupId(template.getResetType());
     //                                templatesDTO.setTemplateId(template.getFlowTemplatesId());
     //                                templatesDTO.setTemplateName(template.getFlowTemplateName());
     //
     //                                result.getLstFlowTemplate().add(templatesDTO);
     //                            }
     //                        }
     //                    }
     //                }
     //            } catch (Exception e) {
     //                logger.error(e.getMessage(), e);
     //            }
     //            System.out.println(PassProtector.decrypt("OGQz2RMz+woYW36dBTkd6ciJhhbvNNN7GZbFT7SdUMFytqNI4t4Z6yV3Xg4nl2Rp", "ipchange"));
     //            System.out.println(PassProtector.decrypt("nIrui5TseRBbWAXxJ9C0GKr/iWY7Z7UbXIX07dNByyA4wBKdJSIW5GpoUcfNCMl4jO+ArCoa3xhM6PiiN9btlQ==", "ipchange"));
     ConnectionPoolRedis.getRedis().get("WO_TT_20171110_17433533");
     ConnectionPoolRedis.getRedis().get("192.168.136.21");
     ConnectionPoolRedis.getRedis().del("WO_TT_20171110_17433533");
     //            ConnectionPoolRedis.getRedis().del("WO_TT_20170816_14955758");
     //            ConnectionPoolRedis.getRedis().del("WO_TT_20170817_171640");
     ConnectionPoolRedis.getRedis().del("192.168.136.21");
     //            ConnectionPoolRedis.getRedis().del("10.73.25.241");
     //            ConnectionPoolRedis.getRedis().del("10.59.12.6");

     } catch (Exception ex) {
     logger.error(ex.getMessage(), ex);
     }
     }*/
    @Override
    public ResultDTO startRunMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                 @WebParam(name = "workflow") String workflow, @WebParam(name = "paramValsDto") ResultParamValsDTO paramValsDto,
                                 @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "username") String username,
                                 @WebParam(name = "delayTime") Long delayTime) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultDTO result = new ResultDTO();
        result.setResultCode(RESPONSE_FAIL);
        logger.info("start start run mop workflow: " + workflow);
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setResultMessage("Xảy ra lỗi khi xác thực user/pass");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        // Kiem tra xem workoder co dang thuc hien hay khong
        Jedis redis = null;
        try {
            redis = ConnectionPoolRedis.getRedis();
            if (redis.get(workflow) != null) {
                Integer numOfNodeRunning = 0;
                try {
                    numOfNodeRunning = Integer.valueOf(ConnectionPoolRedis.getRedis().get(workflow));
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                if (numOfNodeRunning > 0) {
                    result.setResultMessage("Workflow " + workflow + " đang chạy, vui lòng chạy lại sau");
                    return result;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Xảy ra lỗi khi kiểm tra xem workorder có đang thực hiện hay không");
            return result;
        } finally {
            if (redis != null) {
                try {
                    ConnectionPoolRedis.closeJedis(redis);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }

        // Kiem tra xem cac tham so cua mop da dc dien day du hay chua
        try {
            if (paramValsDto.getLstParamValOfNode() == null
                    || paramValsDto.getLstParamValOfNode().isEmpty()
                    || paramValsDto.getLstParamValOfNode().get(0).getLstAllParamOfMop().isEmpty()
                    || paramValsDto.getLstParamValOfNode().get(0).getLstParamValues().isEmpty()) {
                result.setResultMessage("Không có tham số đầu vào cho mop");
                return result;
            }
            List<ParamValuesDTO> lstParamDTO = paramValsDto.getLstParamValOfNode().get(0).getLstParamValues();
            for (ParamValuesDTO p : lstParamDTO) {
                if (p.getParamValue() == null || p.getParamValue().trim().isEmpty()) {
                    result.setResultMessage("Tham số: " + p.getParamCode() + " chưa được đi�?n");
                    return result;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Xảy ra lỗi khi kiểm tra các tham số đầu vào đã được nhập đầy đủ hay chưa");
            return result;
        }

        try {
            FlowTemplates selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(flowTemplateId);
            FlowRunAction flowRunAction = new FlowRunAction();
            List<NodeRun> nodeRuns = new ArrayList<>();
            flowRunAction.setCreateDate(new Date());
            if (flowRunAction.getFlowRunName() != null) {
                flowRunAction.setFlowRunName(flowRunAction.getFlowRunName().trim());
            }
            flowRunAction.setFlowRunName(selectedFlowTemplate.getFlowTemplateName());
            flowRunAction.setStatus(0L);
            flowRunAction.setCrNumber(workflow);
            flowRunAction.setCreateBy(username);
            flowRunAction.setFlowTemplates(selectedFlowTemplate);
            flowRunAction.setTimeRun(new Date());

            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            try {
                Map<Long, List<ActionOfFlow>> mapGroupAction = new HashMap<>();
                for (ActionOfFlow actionOfFlow : selectedFlowTemplate.getActionOfFlows()) {
                    if (mapGroupAction.get(actionOfFlow.getGroupActionOrder()) == null) {
                        mapGroupAction.put(actionOfFlow.getGroupActionOrder(), new ArrayList<ActionOfFlow>());
                    }
                    mapGroupAction.get(actionOfFlow.getGroupActionOrder()).add(actionOfFlow);
                }

                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
                List<ParamValue> paramValues = new ArrayList<>();

                List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<>();
                if (paramValsDto.getLstParamValOfNode() != null && paramValsDto.getLstParamValOfNode().size() > 0) {
                    Node node;
                    for (ParamValsOfNodeDTO paramsNode : paramValsDto.getLstParamValOfNode()) {
                        node = new NodeServiceImpl().findById(paramsNode.getNodeId());
                        if (node == null) {
                            logger.error("ERROR CANNOT GET NODE WITH ID: " + paramsNode.getNodeId());
                            continue;
                        }
                        logger.info("chay vao node :" + paramsNode.getNodeId());
                        NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM);
                        nodeRuns.add(nodeRun);
                        List<ParamValue> _paramValueOfNode = new MopUtils().updateParamValues(paramsNode.getLstAllParamOfMop(), paramsNode.getLstParamValues(), nodeRun);
                        if (_paramValueOfNode != null) {
                            paramValues.addAll(_paramValueOfNode);
                        }

//                        if (mapGroupAction.get(node) != null) {
                        logger.info(" co vao mapGroupAction size = " + mapGroupAction.get(node));
                        for (Map.Entry<Long, List<ActionOfFlow>> entry : mapGroupAction.entrySet()) {
                            NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                                    new NodeRunGroupActionId(node.getNodeId(),
                                            flowRunAction.getFlowRunId(),
                                            entry.getValue().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), entry.getValue().get(0), nodeRun);
                            nodeRunGroupActions.add(nodeRunGroupAction);
                        } // end loop for group action
                        logger.info(" thoai khoi mapGroupAction");
//                        }
                    }
                    logger.info(" xoa session");
//                    session.clear();
                    logger.info(" insert NodeRunServiceImpl ");
                    new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
                    logger.info(" insert ParamValueServiceImpl ");
                    new ParamValueServiceImpl().saveOrUpdate(paramValues, session, tx, false);

                    session.flush();
                    session.clear();
                    new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
//				session.flush();
//		 		tx.commit();

                } else {

                }
                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);

                // start run mop
                result.setResultCode(RESPONSE_SUCCESS);
                result.setResultMessage("bắt đầu chạy mop " + selectedFlowTemplate.getFlowTemplateName());

            } catch (AppException | SysException | HibernateException e) {
                if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                    tx.rollback();
                }
                logger.error(e.getMessage(), e);
                result.setResultMessage("Xảy ra lỗi khi chạy mop " + selectedFlowTemplate.getFlowTemplateName());
                return result;
            } finally {
                if (session.isOpen()) {
                    session.close();
                }
            }
        } catch (AppException | SysException | HibernateException e) {
            result.setResultMessage("Xảy ra lỗi khi chạy mop");
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public ResultDTO startRunMopSingleNode(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                           @WebParam(name = "workflow") String workflow, @WebParam(name = "paramValues") String paramValues,
                                           @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "username") String username, @WebParam(name = "nodeId") Long nodeId,
                                           @WebParam(name = "delayTime") Long delayTime) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultDTO result = new ResultDTO();
        result.setResultCode(RESPONSE_FAIL);
        logger.info("start start run mop");
        Date startTime = new Date();
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultMessage("Username or password is not correct");
                return result;
            }
        } catch (Exception ex) {
            result.setResultMessage("Error when check user/pass");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        // Kiem tra xem workoder co dang thuc hien hay khong
        Jedis jedis = null;
        Node node = null;
        try {
            jedis = ConnectionPoolRedis.getRedis();
            if (jedis.get(workflow) != null) {
                Integer numOfNodeRunning = 0;
                try {
                    numOfNodeRunning = Integer.valueOf(ConnectionPoolRedis.getRedis().get(workflow));
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                if (numOfNodeRunning != null && numOfNodeRunning > 0) {
                    result.setResultMessage("Workflow " + workflow + " is running");
                    return result;
                }
            }

            // Kiem tra xem node mang co thuc hien DT nao hay khong
            node = new NodeServiceImpl().findById(nodeId);
            if (node == null) {
                logger.error("ERROR CANNOT GET NODE WITH ID: " + nodeId);
                result.setResultMessage("Can not get node information");
                return result;
            }

            if (jedis.get(node.getEffectIp()) != null && !jedis.get(node.getEffectIp()).isEmpty()) {
                result.setResultMessage("Node " + node.getNodeCode() + "(" + node.getEffectIp() + ") is running");
                return result;
            }

            if (node.getAccount() == null || node.getAccount().trim().isEmpty()) {
                result.setResultMessage("No account for node");
                return result;
            } else if (node.getPassword() == null || node.getPassword().trim().isEmpty()) {
                result.setResultMessage("No password for node");
                return result;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Error when check WO");
            return result;
        } finally {
            if (jedis != null) {
                try {
                    jedis.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }

        // Kiem tra xem cac tham so cua mop da dc dien day du hay chua
        List<ParamValuesDTO> lstParamValDTO;
        try {
            if (paramValues == null
                    || paramValues.isEmpty()) {
                result.setResultMessage("Do not param value input");
                return result;
            }
            Type listType = new TypeToken<List<ParamValuesDTO>>() {
            }.getType();
            lstParamValDTO = new Gson().fromJson(paramValues, listType);

            for (ParamValuesDTO p : lstParamValDTO) {
                if (p.getParamValue() == null || p.getParamValue().trim().isEmpty()) {
                    result.setResultMessage("Param: " + p.getParamCode() + " was not filled");
                    return result;
                } else {
                    logger.info("param value: " + p.getParamCode() + " = " + p.getParamValue());
                }
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Error when check param input");
            return result;
        }

        try {
            FlowTemplates selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(flowTemplateId);
            FlowRunAction flowRunAction = new FlowRunAction();
            List<NodeRun> nodeRuns = new ArrayList<>();
            flowRunAction.setCreateDate(new Date());
            if (flowRunAction.getFlowRunName() != null) {
                flowRunAction.setFlowRunName(flowRunAction.getFlowRunName().trim());
            }
            flowRunAction.setFlowRunName(selectedFlowTemplate.getFlowTemplateName());
            flowRunAction.setStatus(0L);
            flowRunAction.setCrNumber(workflow);
            flowRunAction.setCreateBy(username);
            flowRunAction.setFlowTemplates(selectedFlowTemplate);
            flowRunAction.setTimeRun(new Date());

            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            try {
                Map<Long, List<ActionOfFlow>> mapGroupAction = new HashMap<>();
                for (ActionOfFlow actionOfFlow : selectedFlowTemplate.getActionOfFlows()) {
                    if (mapGroupAction.get(actionOfFlow.getGroupActionOrder()) == null) {
                        mapGroupAction.put(actionOfFlow.getGroupActionOrder(), new ArrayList<ActionOfFlow>());
                    }
                    mapGroupAction.get(actionOfFlow.getGroupActionOrder()).add(actionOfFlow);
                }

                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);

                List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<>();
                if (paramValues != null && !paramValues.trim().isEmpty()) {

//                    for (ParamValsOfNodeDTO paramsNode : paramValsDto.getLstParamValOfNode()) {
                    logger.info("chay vao node :" + node.getNodeCode());
                    NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM);
                    nodeRuns.add(nodeRun);
                    List<ParamValue> lstParamValsOfNode = new MopUtils().getLstParamInput(node, flowTemplateId);
                    List<ParamValue> _paramValueFilledOfNode = new MopUtils().updateParamInputValues(lstParamValsOfNode, lstParamValDTO, nodeRun);
                    if (_paramValueFilledOfNode == null || _paramValueFilledOfNode.isEmpty()) {
                        logger.error("ERROR CANNOT GET PARAM VALUE FROM PARAM USER SET: " + nodeId);
                        result.setResultMessage("Can not get param from user input");
                        return result;
                    }
//                        if (mapGroupAction.get(node) != null) {
                    logger.info(" co vao mapGroupAction size = " + mapGroupAction.size());
                    for (Map.Entry<Long, List<ActionOfFlow>> entry : mapGroupAction.entrySet()) {
                        NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                                new NodeRunGroupActionId(node.getNodeId(),
                                        flowRunAction.getFlowRunId(),
                                        entry.getValue().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), entry.getValue().get(0), nodeRun);
                        nodeRunGroupActions.add(nodeRunGroupAction);
                    } // end loop for group action
                    logger.info(" thoai khoi mapGroupAction");
//                        }
//                    }
                    logger.info(" xoa session");
//                    session.clear();
                    logger.info(" insert NodeRunServiceImpl ");
                    new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
                    logger.info(" insert ParamValueServiceImpl ");
                    new ParamValueServiceImpl().saveOrUpdate(_paramValueFilledOfNode, session, tx, false);

                    session.flush();
                    session.clear();
                    new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
//				session.flush();
//		 		tx.commit();

                } else {

                }
                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);

                // start run mop
                startProcess(flowRunAction, nodeRuns, workflow, username, node.getAccount(), node.getPassword());

                /*
                 Ghi log tac dong nguoi dung
                 */
                try {
                    LogUtils.writelog(startTime, WSForGNOCImpl.class.getName(),
                            Thread.currentThread().getStackTrace()[1].getMethodName(),
                            LogUtils.ActionType.IMPACT.name(), flowRunAction.toString());
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                result.setResultCode(RESPONSE_SUCCESS);
                result.setResultMessage("bắt đầu chạy mop " + selectedFlowTemplate.getFlowTemplateName());

            } catch (Exception e) {
                if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                    tx.rollback();
                }
                logger.error(e.getMessage(), e);
                result.setResultMessage("Xảy ra lỗi khi chạy mop " + selectedFlowTemplate.getFlowTemplateName());
                return result;
            } finally {
                if (session.isOpen()) {
                    session.close();
                }
            }
        } catch (AppException | SysException | HibernateException e) {
            result.setResultMessage("Xảy ra lỗi khi chạy mop");
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    private void startProcess(FlowRunAction flowRunAction, List<NodeRun> lstNodeRun, String workorder,
                              String username, String userRun, String passRun) throws Exception {
        try {
            //passRun = PassProtector.decrypt(passRun, "ipchange");

            Map<String, AccountObj> mapNodeAccount = new HashMap<>();
            for (NodeRun nodeRun : lstNodeRun) {
                mapNodeAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj(userRun, passRun));
            }
            MessageObject mesObj;

            mesObj = new MessageObject(flowRunAction.getFlowRunId(),
                    username, userRun, passRun, null, flowRunAction.getFlowRunName(), "");
            mesObj.setRunType(1);
            mesObj.setErrorMode(1);
            mesObj.setRunningType(1);

            String encrytedMess = new String(Base64.encodeBase64((new Gson()).toJson(mesObj).getBytes("UTF-8")), "UTF-8");
            logger.info("vao send message den tien trinh");
            sendMsg2ThreadExecute(encrytedMess, "VNM");

            // Cap nhat so node dang chay cho workorder
            Jedis jedis = null;
            try {
                jedis = ConnectionPoolRedis.getRedis();
                jedis.set(workorder, lstNodeRun.size() + "");
                logger.info(">>>>>>>>>>>>  work order : " + workorder + " / so node dang chay: " + jedis.get(workorder));

                for (NodeRun node : lstNodeRun) {
                    jedis.set(node.getNode().getEffectIp(), "1");
                    logger.info(">>>>>>>>>>>>  Node : " + node.getNode().getEffectIp() + node.getNode().getNodeCode() + ") / dang chay: ");
                }

            } catch (Exception e) {
                logger.error("Loi cap nhat so node dang chay trong 1 workorder " + workorder);
                throw e;
            } finally {
                if (jedis != null) {
                    try {
                        jedis.close();
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }

                }
            }

            logger.info(">>>>>>>>>>>>> START RUN PROCESS WORKORDER: " + workorder);
            com.viettel.gnoc.wfm.service.ResultDTO result;
            try {
                result = WOService.updateMopInfo(workorder, "Bat dau thuc hien mop: " + workorder, flowRunAction.getFlowRunId() + "", 1l);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                result = null;
            }

            if (result != null && "success".equalsIgnoreCase(result.getMessage())) {
                logger.info("UPDATE WO: " + workorder + "SUCCESS");
            } else {
                logger.error("UPDATE WO: " + workorder + "FAIL");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void startProcessWithAcc(FlowRunAction flowRunAction, List<NodeRun> lstNodeRun,
                                     String workorder, String username, String userImpact, String passImpact) throws Exception {
        try {
            MessageObject mesObj;
            mesObj = new MessageObject(flowRunAction.getFlowRunId(),
                    username, userImpact, passImpact, null, flowRunAction.getFlowRunName(), "");
            mesObj.setRunType(1);
            mesObj.setErrorMode(1);
            mesObj.setRunningType(1);

            String encrytedMess = new String(Base64.encodeBase64((new Gson()).toJson(mesObj).getBytes("UTF-8")), "UTF-8");
            logger.info("vao send message den tien trinh");
            sendMsg2ThreadExecute(encrytedMess, "VNM");

            // Cap nhat so node dang chay cho workorder
            Jedis jedis = null;
            try {
                jedis = ConnectionPoolRedis.getRedis();
                jedis.set(workorder, lstNodeRun.size() + "");
                logger.info(">>>>>>>>>>>>  work order : " + workorder + " / so node dang chay: " + jedis.get(workorder));

                for (NodeRun node : lstNodeRun) {
                    jedis.set(node.getNode().getEffectIp(), "1");
                    logger.info(">>>>>>>>>>>>  Node : " + node.getNode().getEffectIp() + node.getNode().getNodeCode() + ") / dang chay: ");
                }

            } catch (Exception e) {
                logger.error("Loi cap nhat so node dang chay trong 1 workorder " + workorder);
                throw e;
            } finally {
                if (jedis != null) {
                    try {
                        jedis.close();
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }

                }
            }

            logger.info(">>>>>>>>>>>>> START RUN PROCESS WORKORDER: " + workorder);
            com.viettel.gnoc.wfm.service.ResultDTO result = null;
            try {
                result = WOService.updateMopInfo(workorder, "Bat dau thuc hien mop: " + workorder, flowRunAction.getFlowRunId() + "", 1l);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                result = null;
            }

            if (result != null && "success".equalsIgnoreCase(result.getMessage())) {
                logger.info("UPDATE WO: " + workorder + "SUCCESS");
            } else {
                logger.error("UPDATE WO: " + workorder + "FAIL");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void sendMsg2ThreadExecute(String encrytedMess, String countryCode) throws IOException, Exception, MessageException {
        Map<String, Object> filters = new HashMap<>();
        filters.put("countryCode.countryCode", countryCode);
        filters.put("status", 1l);

        List<MapProcessCountry> maps = new MapProcessCountryServiceImpl().findListExac(filters, null);

        if (maps != null && !maps.isEmpty()) {
            //Sap xep lai maps theo thu tu random
            Collections.shuffle(maps);

            int i = 0;
            for (MapProcessCountry process : maps) {
                int serverPort = process.getProcessPort();
                String serverIp = process.getProcessIp();

                logger.info("server: " + serverIp + "/ port: " + serverPort);

                SocketClient client = new SocketClient(serverIp, serverPort);
                client.sendMsg(encrytedMess);

                String socketResult = client.receiveResult();
                if (socketResult != null && socketResult.contains("NOK")) {
                    if (i == maps.size() - 1) {
                        throw new MessageException(socketResult);
                    }
                } else {
                    return;
                }
                i++;
            }
        } else {
            logger.error("Loi khong lay duoc thong tin country code: " + countryCode);
        }
    }

    @Override
    public ResultDTO checkWorkOrderRunning(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                           @WebParam(name = "workOrderCode") String workOrderCode) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultDTO result = new ResultDTO();
        logger.info("start start run mop workOrderCode: " + workOrderCode);
        Jedis jedis = null;
        try {
            result.setResultCode(RESPONSE_SUCCESS);
            result.setResultMessage("workorder đang không chạy");
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            jedis = ConnectionPoolRedis.getRedis();
            if (jedis.get(workOrderCode) != null && !jedis.get(workOrderCode).isEmpty()) {
                int numOfNodeRunning = Integer.valueOf(jedis.get(workOrderCode));
                if (numOfNodeRunning > 0) {
                    result.setResultCode(RESPONSE_FAIL);
                    result.setResultMessage("workorder " + workOrderCode + ": đang chạy");
                }
            }
        } catch (Exception ex) {
            result.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        } finally {
            if (jedis != null) {
                try {
                    ConnectionPoolRedis.closeJedis(jedis);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }

//        try {
//            Map<String, Object> filters = new HashedMap();
////            filters.put("");
//        } catch (Exception ex) {
//            result.setResultMessage(ex.getMessage());
//            logger.error(ex.getMessage(), ex);
//            return result;
//        }
        return result;
    }

    private boolean checkWhiteListIp() {
//        try {
//            HttpExchange exchange = (HttpExchange) context.getMessageContext().get(JAXWSProperties.HTTP_EXCHANGE);
//            InetSocketAddress remoteAddress = exchange.getRemoteAddress();
//            String remoteHost = remoteAddress.getAddress().getHostAddress();
//
//            List<?> ips = new DaoSimpleService().findListSQLAll("SELECT IP from WHITELIST_WS WHERE SYSTEM_NAME='WS_FOR_GNOC'");
//            logger.info("IP remote: " + remoteHost);
//            if (ips == null || ips.isEmpty()) {
//                return true;
//            }
//            for (Object ip : ips) {
//                if (ip != null && ip.equals(remoteHost)) {
//                    return true;
//                }
//            }
//            logger.info("Forbidden access: " + remoteHost);
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//        }

        return true;
    }

    @Override
    public ResetFlowTemplatesDTO getResetMopGroup(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                                  @WebParam(name = "workOrderCode") String workOrderCode) {
        ResetFlowTemplatesDTO result = new ResetFlowTemplatesDTO();
        if (workOrderCode == null) {
            result.setMessage("error workorder code empty");
            return result;
        }
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setMessage("Username or password not correct");
                return result;
            }
        } catch (Exception ex) {
            result.setMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return result;
        }

        try {
            // Lay thong tin noi dung canh bao
            JsonNocproData nocproData = MopUtils.getNocproAlarm(workOrderCode);
            if (nocproData == null) {
                result.setMessage("Cannot get alarm infomations from nocPro");
                return result;
            } else if (nocproData.getNode_code() == null || nocproData.getNode_code().isEmpty()) {
                result.setMessage("Cannot get node code from nocPro");
                return result;
            } else if (nocproData.getType_station() == null || nocproData.getType_station().isEmpty()) {
                result.setMessage("Cannot get network type from nocPro");
                return result;
            }

            // Luu thong tin network va vendor
            result.setNetworkType(nocproData.getType_station().toUpperCase().trim());
            result.setVendor(nocproData.getVendor().toUpperCase().trim());

            // Lay danh sach cac mop co the
            Map<String, Object> filters = new HashedMap();
            if (nocproData.getVendor() != null) {
                filters.put("vendor", nocproData.getVendor().trim().toUpperCase());
            }
            if (nocproData.getType_station() != null) {
                filters.put("type", nocproData.getType_station());
            }
            List<AutoManResetMopConfig> manResetMops = new AutoManResetMopConfigServiceImpl().findList(filters);
            if (manResetMops != null) {
                filters.clear();
                filters.put("templateGroup.id", ConstantsWS.MANUAL_RESET_TEMPLATE_GROUP);

                Map<Integer, AutoManResetMopConfig> mapManResetMopConfig = new HashedMap();
                for (AutoManResetMopConfig mop : manResetMops) {
                    mapManResetMopConfig.put(mop.getResetType(), mop);
                }

                for (Map.Entry<Integer, AutoManResetMopConfig> entry : mapManResetMopConfig.entrySet()) {
                    filters.put("resetType", Long.valueOf(entry.getKey()));

                    List<FlowTemplates> templates = new FlowTemplatesServiceImpl().findList(filters);
                    if (templates != null) {
                        ResetMopGroupDTO resetGroup = new ResetMopGroupDTO();
                        resetGroup.setResetType(entry.getKey());

                        for (FlowTemplates template : templates) {
                            if (MopUtils.checkTemplate(template, nocproData)) {
                                ResetMopDTO resetMopDTO = new ResetMopDTO();
                                resetMopDTO.setDesc(template.getFlowTemplateName());
                                resetMopDTO.setTemplateId(template.getFlowTemplatesId());
                                resetMopDTO.setTemplateName(template.getFlowTemplateName());
                                resetGroup.getResetMops().add(resetMopDTO);
                            }
                        }

                        if (!resetGroup.getResetMops().isEmpty()) {
                            result.getMopGroupsDTO().add(resetGroup);
                        }
                    }
                }
                if (result.getMopGroupsDTO() == null || result.getMopGroupsDTO().isEmpty()) {
                    result.setMessage("No mop run was founded with vendor and network type of nocpro alarm info");
                    return result;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return result;
    }

    @Override
    public ResetParamsDTO getParamResetMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                           @WebParam(name = "mopId") Long mopId, @WebParam(name = "workOrderCode") String workOrderCode) {
        ResetParamsDTO resultGetParamInputDTO = new ResetParamsDTO();
        logger.info("Start get param input reset mop");
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                resultGetParamInputDTO.setMessages("Incorrect user/pass");
                return resultGetParamInputDTO;
            }
        } catch (Exception ex) {
            resultGetParamInputDTO.setMessages("Incorrect user/pass");
            logger.error(ex.getMessage(), ex);
            return resultGetParamInputDTO;
        }

        if (mopId == null || mopId == 0) {
            resultGetParamInputDTO.setMessages("No mop run selected");
            return resultGetParamInputDTO;
        }

        try {
            // Lay thong tin noi dung canh bao
            JsonNocproData nocproData = MopUtils.getNocproAlarm(workOrderCode);
            if (nocproData == null) {
                resultGetParamInputDTO.setMessages("Cannot get alarm infomations from nocPro");
                return resultGetParamInputDTO;
            } else if (nocproData.getNode_code() == null) {
                resultGetParamInputDTO.setMessages("Node code infomation is empty");
                return resultGetParamInputDTO;
            }

            // get bsc/rnc node from node code
            Map<String, Object> filters = new HashedMap();
            filters.put("nodeCode", nocproData.getNode_code());
            List<Node> nodes = new NodeServiceImpl().findListExac(filters, null);
            if (nodes == null || nodes.isEmpty()) {
                resultGetParamInputDTO.setMessages("Node node declared with node code: " + nocproData.getNode_code());
                return resultGetParamInputDTO;
            }

            List<ParamValue> lstParamValue = new MopUtils().getLstParamInput(nodes.get(0), mopId);
            if (lstParamValue == null || lstParamValue.isEmpty()) {
                resultGetParamInputDTO.setMessages("Can not get parameters with node: " + nodes.get(0).getNodeCode());
                return resultGetParamInputDTO;
            } else {

                // Xoa cac tham so la tham so tham chieu
                List<ParamValue> lstParamValueTmp = new ArrayList<>();
                for (ParamValue p : lstParamValue) {
                    if (!p.isDisableByInOut()) {
                        lstParamValueTmp.add(p);
                    }
                }
                // Xoa bo cac tham so trung nhau
                lstParamValueTmp = new MopUtils().distinctParamValueSameParamCode(lstParamValueTmp);

                if (lstParamValueTmp != null) {
                    ParamValuesDTO paramDto;
                    ArrayList<ParamValuesDTO> lstParamValueDto = new ArrayList<>();

                    for (ParamValue p : lstParamValueTmp) {
                        paramDto = new ParamValuesDTO();
                        paramDto.setDescription(p.getDescription() == null ? "" : p.getDescription());
                        paramDto.setDisableByInOut(p.isDisableByInOut());
                        paramDto.setFormula(p.getFormula() == null ? "" : p.getFormula());
                        paramDto.setGroupCode(p.getGroupCode());
                        paramDto.setParamInputId(p.getParamInput().getParamInputId());
                        paramDto.setParamLabel(p.getParamCode());
                        paramDto.setParamCode(p.getParamCode());

                        // Lay gia tri phan tich tu content alarm nocpro
                        String paramVal = null;
                        try {
                            paramVal = MopUtils.getResetParamVal(p.getParamCode(), nocproData.getContent_event(), mopId);
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                            if ("ERROR_GET_PARAM_VALUE".equals(e.getMessage())) {
                                resultGetParamInputDTO.setMessages("Can not get param from config: " + p.getParamCode());
                                return resultGetParamInputDTO;
                            }
                        }

                        if (paramVal != null) {
                            paramDto.setParamValue(paramVal);

                            // Lay gia tri tham so neu tham so la ma tram
                        } else if (MopUtils.getSTATION_PARAMS_CODE().contains(p.getParamCode())) {
                            paramDto.setParamValue(nocproData.getCabinet_name());
                            // Lay gia tri ip nodeb
                        } else if (ConstantsWS.NOC_RS_IP_NODEB.equals(p.getParamCode())) {
                            String nodebIp = MopUtils.getIpNodeB(nocproData.getCabinet_name());
                            if (nodebIp != null) {
                                try {
                                    String[] nodebIpArr = nodebIp.split("\\.");
                                    String octet = String.valueOf(Integer.valueOf(nodebIpArr[1]) + 2);
                                    nodebIp = nodebIpArr[0] + "." + octet + "." + nodebIpArr[2] + "." + nodebIpArr[3];

                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                    resultGetParamInputDTO.setMessages("Can not get param from config: " + p.getParamCode());
                                    return resultGetParamInputDTO;
                                }
                                paramDto.setParamValue(nodebIp);
                            } else {
                                resultGetParamInputDTO.setMessages("Can not get param from config: " + p.getParamCode());
                                return resultGetParamInputDTO;
                            }
                        } else if (ConstantsWS.NOC_RS_MANUAL_TG_NUMBER.equals(p.getParamCode())) {
                            String tgNumber = MopUtils.getTgNumber(nocproData.getCell_name());
                            if (tgNumber != null) {
                                paramDto.setParamValue(tgNumber);
                            } else {
                                resultGetParamInputDTO.setMessages("Can not get param from config: " + p.getParamCode());
                                return resultGetParamInputDTO;
                            }
                        } else {
                            paramDto.setParamValue("");
                        }
                        lstParamValueDto.add(paramDto);
                    }

                    ParamValsDTO paramsNode = new ParamValsDTO();
                    paramsNode.setLstParamValues(lstParamValueDto);
                    paramsNode.setNodeId(nodes.get(0).getNodeId());
                    paramsNode.setMessage("");
                    paramsNode.setNodeCode(nodes.get(0).getNodeCode());

                    Gson gson = new Gson();
                    logger.info(gson.toJson(lstParamValueDto));

                    resultGetParamInputDTO.getLstParamValOfNode().add(paramsNode);
                    resultGetParamInputDTO.setMessages("SUCCESS");
                    return resultGetParamInputDTO;
                } else {
                    resultGetParamInputDTO.setMessages("Can not get distinct parameters with node: " + nodes.get(0).getNodeCode());
                    return resultGetParamInputDTO;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public ResultDTO runResetMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                 @WebParam(name = "workOrderCode") String workOrderCode, @WebParam(name = "paramValues") String paramValues,
                                 @WebParam(name = "mopId") Long mopId, @WebParam(name = "username") String username) {
        ResultDTO result = new ResultDTO();
        result.setResultCode(RESPONSE_FAIL);
        logger.info("start start run mop workOrderCode: " + workOrderCode + ", mopId: " + mopId);
        Date startTime = new Date();
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultMessage("Username or password is not correct");
                return result;
            }
        } catch (Exception ex) {
            result.setResultMessage("Error when authentycate user/pass");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        // Kiem tra xem workoder co dang thuc hien hay khong
        Node node;
        try {
            if (MopUtils.checkWoRunning(workOrderCode)) {
                result.setResultMessage("Workorder is running, please wait a minus");
                return result;
            }

            // Lay thong tin noi dung canh bao
            logger.info("workOrderCode: " + workOrderCode);
            JsonNocproData nocproData = MopUtils.getNocproAlarm(workOrderCode);
            if (nocproData == null) {
                result.setResultMessage("Cannot get alarm infomations from nocPro");
                return result;
            } else if (nocproData.getNode_code() == null) {
                result.setResultMessage("Node code infomation is empty");
                return result;
            }

            // get bsc/rnc node from node code
            Map<String, Object> filters = new HashedMap();
            filters.put("nodeCode", nocproData.getNode_code());
            List<Node> nodes = new NodeServiceImpl().findList(filters);
            if (nodes == null || nodes.isEmpty()) {
                result.setResultMessage("Node node declared with node code: " + nocproData.getNode_code());
                return result;
            } else {
                node = nodes.get(0);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Error: " + e.getMessage());
            return result;
        }

        // Kiem tra xem cac tham so cua mop da dc dien day du hay chua
        List<ParamValuesDTO> lstParamValDTO;
        try {

            if (node.getAccount() == null || node.getAccount().trim().isEmpty()) {
                result.setResultMessage("No account for node");
                return result;
            } else if (node.getPassword() == null || node.getPassword().trim().isEmpty()) {
                result.setResultMessage("No password for node");
                return result;
            }

            if (paramValues == null) {
                result.setResultMessage("Don have param input");
                return result;
            }
            Type listType = new TypeToken<List<ParamValuesDTO>>() {
            }.getType();
            lstParamValDTO = new Gson().fromJson(paramValues, listType);

            for (ParamValuesDTO p : lstParamValDTO) {
                if (p.getParamValue() == null || p.getParamValue().trim().isEmpty()) {
                    result.setResultMessage("Param: " + p.getParamCode() + " was not filled");
                    return result;
                }
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Error when validate input data");
            return result;
        }

        try {
            FlowTemplates selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(mopId);
            FlowRunAction flowRunAction = new FlowRunAction();
            List<NodeRun> nodeRuns = new ArrayList<>();
            flowRunAction.setCreateDate(new Date());
            if (flowRunAction.getFlowRunName() != null) {
                flowRunAction.setFlowRunName(flowRunAction.getFlowRunName().trim());
            }
            flowRunAction.setFlowRunName(selectedFlowTemplate.getFlowTemplateName());
            flowRunAction.setStatus(0L);
            flowRunAction.setCrNumber(workOrderCode);
            flowRunAction.setCreateBy(username);
            flowRunAction.setFlowTemplates(selectedFlowTemplate);
            flowRunAction.setTimeRun(new Date());

            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            try {
                Map<Long, List<ActionOfFlow>> mapGroupAction = new HashMap<>();
                for (ActionOfFlow actionOfFlow : selectedFlowTemplate.getActionOfFlows()) {
                    if (mapGroupAction.get(actionOfFlow.getGroupActionOrder()) == null) {
                        mapGroupAction.put(actionOfFlow.getGroupActionOrder(), new ArrayList<ActionOfFlow>());
                    }
                    mapGroupAction.get(actionOfFlow.getGroupActionOrder()).add(actionOfFlow);
                }

                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);

                List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<>();
                if (paramValues != null && !paramValues.trim().isEmpty()) {

                    logger.info("chay vao node :" + node.getNodeCode());
                    NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM);
                    nodeRuns.add(nodeRun);
                    List<ParamValue> lstParamValsOfNode = new MopUtils().getLstParamInput(node, mopId);
                    List<ParamValue> _paramValueFilledOfNode = new MopUtils().updateParamInputValues(lstParamValsOfNode, lstParamValDTO, nodeRun);
                    if (_paramValueFilledOfNode == null || _paramValueFilledOfNode.isEmpty()) {
                        logger.error("ERROR CANNOT GET PARAM VALUE FROM PARAM USER SET: " + node.getNodeId());
                        result.setResultMessage("Error get param value from user");
                        return result;
                    }
                    logger.info(" co vao mapGroupAction size = " + mapGroupAction.size());
                    for (Map.Entry<Long, List<ActionOfFlow>> entry : mapGroupAction.entrySet()) {
                        NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                                new NodeRunGroupActionId(node.getNodeId(),
                                        flowRunAction.getFlowRunId(),
                                        entry.getValue().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), entry.getValue().get(0), nodeRun);
                        nodeRunGroupActions.add(nodeRunGroupAction);
                    } // end loop for group action
                    logger.info(" thoai khoi mapGroupAction");
                    logger.info(" xoa session");
                    logger.info(" insert NodeRunServiceImpl ");
                    new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
                    logger.info(" insert ParamValueServiceImpl ");
                    new ParamValueServiceImpl().saveOrUpdate(_paramValueFilledOfNode, session, tx, false);

                    session.flush();
                    session.clear();
                    new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
                }
                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);

                // start run mop
                startProcess(flowRunAction, nodeRuns, workOrderCode, username, node.getAccount(), node.getPassword());

                /*
                 Ghi log tac dong nguoi dung
                 */
                try {
                    LogUtils.writelog(startTime, WSForGNOCImpl.class.getName(),
                            Thread.currentThread().getStackTrace()[1].getMethodName(),
                            LogUtils.ActionType.IMPACT.name(), flowRunAction.toString());
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                result.setResultCode(RESPONSE_SUCCESS);
                result.setResultMessage("Start run mop " + selectedFlowTemplate.getFlowTemplateName());

            } catch (Exception e) {
                if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                    tx.rollback();
                }
                logger.error(e.getMessage(), e);
                result.setResultMessage("Error message: " + e.getMessage());
                return result;
            } finally {
                if (session.isOpen()) {
                    session.close();
                }
            }
        } catch (AppException | SysException | HibernateException e) {
            result.setResultMessage("Xảy ra lỗi khi chạy mop");
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public FlowRunsLogDTO getFlowRunLogs(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                         @WebParam(name = "workOrderCode") String workOrderCode, @WebParam(name = "username") String username) {
        FlowRunsLogDTO flowRunsLogDTO = new FlowRunsLogDTO();
        if (!checkWhiteListIp()) {
            return null;
        }
        ResultFlowTemplatesDTO result = new ResultFlowTemplatesDTO();
        result.setMessage("");
        logger.info("start get list flow run log, workOrderCode: " + workOrderCode + ", username: " + username);
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setMessage("Username or password is not correct");
                return flowRunsLogDTO;
            }
        } catch (Exception ex) {
            flowRunsLogDTO.setMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return flowRunsLogDTO;
        }

        ArrayList<FlowRunLogDTO> flowRunLogsDTO = new ArrayList<>();

        try {
            Map<String, Object> filters = new HashedMap();
            filters.put("crNumber", workOrderCode);
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("timeRun", "ASC");
            if (username != null && !username.trim().isEmpty()) {
                filters.put("createBy", username);
            }
            List<FlowRunAction> flowRunActions = new FlowRunActionServiceImpl().findListExac(filters, orders);
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
            if (flowRunActions != null) {
                for (FlowRunAction flowRunAction : flowRunActions) {

                    FlowRunLogDTO flowRunLogDTO = new FlowRunLogDTO();
                    flowRunLogDTO.setCreateTime(sdf1.format(flowRunAction.getCreateDate()));
                    if (flowRunAction.getEndRun() != null) {
                        flowRunLogDTO.setFinishTime(sdf1.format(flowRunAction.getEndRun()));
                    }
                    flowRunLogDTO.setFlowRunActionId(flowRunAction.getFlowRunId());
                    flowRunLogDTO.setTemplateName(flowRunAction.getFlowRunName());
                    flowRunLogDTO.setUsername(flowRunAction.getCreateBy());
                    flowRunLogDTO.setWorkorder(workOrderCode);
                    String mopResult = null;
                    switch (flowRunAction.getStatus().intValue()) {
                        case 2:
                            mopResult = "RUNNING";
                            break;
                        case 3:
                            mopResult = "SUCCESSED";
                            break;
                        case 4:
                            mopResult = "FAILED";
                            break;
                        case 5:
                            mopResult = "STOPPED";
                            break;
                        default:
                            break;
                    }
                    flowRunLogDTO.setResult(mopResult);

                    flowRunLogsDTO.add(flowRunLogDTO);
                }

                flowRunsLogDTO.setFlowRunsLogDTO(flowRunLogsDTO);
                flowRunsLogDTO.setMessage("SUCESS");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return flowRunsLogDTO;
    }

    @Override
    public FlowRunLogDetailDTO getLogRunDetail(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                               @WebParam(name = "flowRunLogId") Long flowRunLogId) {
        FlowRunLogDetailDTO logDetailDTO = new FlowRunLogDetailDTO();
        if (flowRunLogId != null) {
            try {
                FlowRunAction flowRunAction = new FlowRunActionServiceImpl().findById(flowRunLogId);
                if (flowRunAction != null) {
                    Map<String, Object> filtersAction = new HashedMap();
                    filtersAction.put("flowRunLogId", flowRunAction.getFlowRunId());

                    LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                    orders.put("startTime", "ASC");

                    List<FlowRunLogAction> flowRunLogActions = new FlowRunLogActionServiceImpl().findList(filtersAction, orders);
                    if (flowRunLogActions != null && !flowRunLogActions.isEmpty()) {
                        Map<String, Object> filtersCmdLog = new HashedMap();
                        LinkedHashMap<String, String> ordersCmd = new LinkedHashMap<>();
                        ordersCmd.put("orderRun", "ASC");

                        long index = 0;
                        for (FlowRunLogAction flowRunLogAction : flowRunLogActions) {
                            filtersCmdLog.put("runLogActionId", flowRunLogAction.getRunLogActionId());
                            List<FlowRunLogCommand> flowRunLogCommands = new FlowRunLogCommandServiceImpl().findListExac(filtersCmdLog, ordersCmd);
                            if (flowRunLogCommands != null && !flowRunLogCommands.isEmpty()) {

                                for (FlowRunLogCommand cmdLog : flowRunLogCommands) {
                                    CommandLogDTO commandLogDTO = new CommandLogDTO();
                                    commandLogDTO.setActionName(getActionName(flowRunLogAction.getActionOfFlowId()));
                                    commandLogDTO.setCmd(cmdLog.getCmdRun());
                                    commandLogDTO.setLogsCmd(cmdLog.getResultDetail().getBytes());
                                    commandLogDTO.setResult(cmdLog.getResult());
                                    commandLogDTO.setOrderRun(index);

                                    logDetailDTO.getCommandLogsDTO().add(commandLogDTO);
                                    index++;

                                }
                            }
                        }
                        String mopResult = null;
                        switch (flowRunAction.getStatus().intValue()) {
                            case 2:
                                mopResult = "RUNNING";
                                break;
                            case 3:
                                mopResult = "SUCCESSED";
                                break;
                            case 4:
                                mopResult = "FAILED";
                                break;
                            case 5:
                                mopResult = "STOPPED";
                                break;
                            default:
                                break;
                        }
                        logDetailDTO.setResult(mopResult);
                        logDetailDTO.setMessage("SUCCESS");
                        return logDetailDTO;
                    }

                } else {
                    logDetailDTO.setMessage("Can not get log run info");
                    return logDetailDTO;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return logDetailDTO;
    }

    private String getActionName(Long actionOfFlowId) {
        if (actionOfFlowId != null) {
            try {
                ActionOfFlow actionOfFlow = new ActionOfFlowServiceImpl().findById(actionOfFlowId);
                if (actionOfFlow != null && actionOfFlow.getAction() != null) {
                    return actionOfFlow.getAction().getName();
                }
            } catch (AppException | SysException e) {
                logger.error(e.getMessage(), e);
            }
            return "";
        } else {
            return "";
        }
    }

    //20171109_Quytv7_WS cho Gnoc call_luong toan trinh_start
    @Override
    public ResultFileValidateDTO validateFileInput(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                                   @WebParam(name = "crId") final Long crId, @WebParam(name = "crNumber") final String crCode,
                                                   @WebParam(name = "userCreate") String userCreate,
                                                   @WebParam(name = "businessCode") String business,
                                                   @WebParam(name = "resultFileValidateDTO") ResultFileValidateDTO resultFileValidateDTO) {
        if (resultFileValidateDTO == null) {
            resultFileValidateDTO = new ResultFileValidateDTO();
            resultFileValidateDTO.setResultCode(1);
            resultFileValidateDTO.setResultMessage(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.dataInput.isNull"));
            return resultFileValidateDTO;
        }
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                resultFileValidateDTO.setResultCode(2);
                resultFileValidateDTO.setResultMessage("Username or password invalid");
                return resultFileValidateDTO;
            }
        } catch (Exception ex) {
            resultFileValidateDTO.setResultCode(1);
            resultFileValidateDTO.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return resultFileValidateDTO;
        }
        try {

            if (crId == null || isNullOrEmpty(crCode) || isNullOrEmpty(userCreate) || isNullOrEmpty(business) || resultFileValidateDTO.getFileValidateDTOs() == null || resultFileValidateDTO.getFileValidateDTOs().isEmpty()) {
                resultFileValidateDTO.setResultCode(1);
                resultFileValidateDTO.setResultMessage(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.dataInput.isNull"));
                return resultFileValidateDTO;
            }

            resultFileValidateDTO.setCrNumber(crCode);
            resultFileValidateDTO.setCrId(crId);
            //Get station config with business code
            Map<String, Object> filter = new HashMap<>();
            filter.put("businessCode", business);
//            filter.put("isValidate", 1L);
            List<StationConfigImport> stationConfigImports = new StationConfigImportlServiceImpl().findList(filter);
            if (stationConfigImports == null || stationConfigImports.isEmpty()) {
                resultFileValidateDTO.setResultCode(1);
                resultFileValidateDTO.setResultMessage(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.businessCode.not.exist"));
                return resultFileValidateDTO;
            }
            HashMap<String, HashMap<String, List<StationConfigImport>>> mapConfigFile = new HashMap<>();
            getMapImportFile(mapConfigFile, stationConfigImports);
            final List<FileValidateObject> fileValidateObjects = new ArrayList<>();
            for (FileValidateDTO fileValidateDTO : resultFileValidateDTO.getFileValidateDTOs()) {
                File fileOut;
                if (isNullOrEmpty(fileValidateDTO.getFileName()) || isNullOrEmpty(fileValidateDTO.getFileContent()) || isNullOrEmpty(fileValidateDTO.getFileType())) {
                    fileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultMessage("You must fill full input for file data input");
                    return resultFileValidateDTO;
                }
                HashMap<String, List<?>> mapCellObjectImports = new HashMap<>();
                LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData = new LinkedHashMap<>();
                boolean isExistFile = false;
                String fileNameStandar = "";
                for (String fileName : mapConfigFile.keySet()) {
                    if (fileValidateDTO.getFileName().toLowerCase().contains(fileName.toLowerCase().replace(".xlsx", ""))) {
                        isExistFile = true;
                        fileNameStandar = fileName;
                        break;
                    }
                }
                if (!isExistFile) {
                    fileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.fileName.isFalse"), fileValidateDTO.getFileName(), business));
                    return resultFileValidateDTO;
                }
                byte[] fileContent = Base64.decodeBase64(fileValidateDTO.getFileContent());
                InputStream inputstream = new ByteArrayInputStream(fileContent);
                ResultDTO resultDTO = new ResultDTO();
                resultDTO.setResultCode(0);

                //Call sang Controller validate
//                com.viettel.controller.DrawTopoStatusExecController
                for (String key : mapConfigFile.get(fileNameStandar).keySet()) {
                    Class cls = Class.forName(mapConfigFile.get(fileNameStandar).get(key).get(0).getClassController());
                    Method method = cls.getMethod(mapConfigFile.get(fileNameStandar).get(key).get(0).getFunctionValidate(), HashMap.class, InputStream.class, String.class, HashMap.class, ResultDTO.class, LinkedHashMap.class);
                    method.invoke(cls.newInstance(), mapConfigFile, inputstream, fileNameStandar, mapCellObjectImports, resultDTO, mapData);
                    break;
                }
                //Validate VMSA fail thi dung luon gui tra GNOC
                if (resultDTO.getResultCode() == 1) {
                    try {
                        inputstream = new ByteArrayInputStream(fileContent);
                        fileOut = exportFileResult(inputstream, mapData, 0, fileNameStandar, crCode);
                        fileValidateDTO.setFileContent(new String(Base64.encodeBase64(readBytesFromFile(fileOut.getPath()))));
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw ex;
                    }
                    resultFileValidateDTO.setResultCode(resultDTO.getResultCode());
                    fileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultMessage(resultDTO.getResultMessage());
                    return resultFileValidateDTO;
                }
                //Validate neu VMSA khong loi thi goi sang NIMS de validate
                //Truong hop nghiep vụ co can check ton tai cell ben NIMS(IS_VALIDATE_NIMS = 1)
                try {
                    ResourceBundle bundle = ResourceBundle.getBundle("config");
                    URL url = new URL(bundle.getString("ws_nims"));
                    for (String sheetName : mapData.keySet()) {
                        if (!"cell".equalsIgnoreCase(sheetName)) {
                            continue;
                        }
                        if (mapConfigFile.get(fileNameStandar).get(sheetName).get(0) == null ||
                                mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getIsValidateNims() == null ||
                                mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getIsValidateNims().equals(0L)) {
                            logger.info("---Truong hop khong can validate ben nims---");
                            continue;
                        }
                        List<CheckCellForCRForm> listCellCheck = new ArrayList<>();
                        CheckCellForCRForm checkCellForCRForm;
                        for (int i = 0; i < mapData.get(sheetName).size(); i++) {
                            checkCellForCRForm = new CheckCellForCRForm();
                            checkCellForCRForm.setCellCode(mapData.get(sheetName).get(i).get(mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getColumnValidate().toLowerCase()).toString());
                            checkCellForCRForm.setId(String.valueOf(i));
                            checkCellForCRForm.setType(mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getNetworkType());
                            listCellCheck.add(checkCellForCRForm);
                        }
                        if (listCellCheck.size() > 0) {
                            UpdateInfraWSService locator = new UpdateInfraWSService(url);
                            UpdateInfraWS service = locator.getUpdateInfraWSPort();
                            listCellCheck = service.checkCellForCR(listCellCheck);
                        }

                        for (int i = 0; i < mapData.get(sheetName).size(); i++) {
                            if (!isNullOrEmpty(listCellCheck.get(i).getResult())) {
                                resultDTO.setResultCode(1);
                                fileValidateDTO.setResultCode(1);
                            }

                            mapData.get(sheetName).get(i).put("vmsa_result_code", (listCellCheck.get(i).getResult() == null || listCellCheck.get(i).getResult().isEmpty()) ? "OK" : "NOK");
                            mapData.get(sheetName).get(i).put("vmsa_result_detail", listCellCheck.get(i).getResult());
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    resultFileValidateDTO.setResultCode(1);
                    fileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultMessage("Nims validate NOK: Can not connect to NIMS service");
                    return resultFileValidateDTO;
                }
                if (resultDTO.getResultCode() == 1) {
                    try {
                        inputstream = new ByteArrayInputStream(fileContent);
                        fileOut = exportFileResult(inputstream, mapData, 0, fileNameStandar, crCode);
                        fileValidateDTO.setFileContent(new String(Base64.encodeBase64(readBytesFromFile(fileOut.getPath()))));
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw ex;
                    }
                    resultFileValidateDTO.setResultCode(resultDTO.getResultCode());
                    fileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultMessage("Nims validate NOK");
                    return resultFileValidateDTO;
                } else {
                    try {
                        inputstream = new ByteArrayInputStream(fileContent);
                        fileOut = exportFileResult(inputstream, mapData, 0, fileNameStandar, crCode);
                        fileValidateDTO.setFileContent(new String(Base64.encodeBase64(readBytesFromFile(fileOut.getPath()))));
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw ex;
                    }
                    fileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultMessage("Validate OK");
                }
                FileValidateObject fileValidateObject = new FileValidateObject();
                fileValidateObject.setFileNameConfig(fileNameStandar);
                fileValidateObject.setMapData(mapData);
                fileValidateObject.setMapCellObjectImports(mapCellObjectImports);
                fileValidateObject.setFileOutPut(fileOut);
                fileValidateObjects.add(fileValidateObject);
            }
            //Quytv7_Create tien trinh create mop
            final ManageCreateDtFromGnoc manageCreateDtFromGnoc = new ManageCreateDtFromGnoc();
            manageCreateDtFromGnoc.setCrId(crId);
            manageCreateDtFromGnoc.setCrNumber(crCode);
            manageCreateDtFromGnoc.setIsCreate(0L);
            manageCreateDtFromGnoc.setUpdateTime(new Date());
            manageCreateDtFromGnoc.setUserCreate(userCreate);
            new ManageCreateDtFromGnocServiceImpl().saveOrUpdate(manageCreateDtFromGnoc);
            resultFileValidateDTO.setDtCreateId(manageCreateDtFromGnoc.getId());

            final HashMap<String, HashMap<String, List<StationConfigImport>>> mapConfigFileFinal = new HashMap<>(mapConfigFile);
            final AutoStopThreadPool autoStopThreadPool = new AutoStopThreadPool("callWSNCMS", 50);
            final String userCreateFinal = userCreate;
            final Long crIdFinal = crId;
            final String crCodeFinal = crCode;
            final String pathOut = getFolderSave();
            final String pathTemplate = getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
            autoStopThreadPool.addTask(new Runnable() {
                @Override
                public void run() {
                    CreateMopThread(fileValidateObjects, mapConfigFileFinal, crIdFinal, crCodeFinal, userCreateFinal, pathOut, pathTemplate, manageCreateDtFromGnoc);
                }
            });

            new Thread(new Runnable() {

                @Override
                public void run() {
                    autoStopThreadPool.execute();
                    logger.info("Call process create mop: Done");
                }
            }).start();

        } catch (InvocationTargetException ex) {
            logger.debug(ex.getCause().getMessage(), ex);
            resultFileValidateDTO.setResultCode(1);
            resultFileValidateDTO.setResultMessage(ex.getCause().getMessage());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            resultFileValidateDTO.setResultCode(1);
            resultFileValidateDTO.setResultMessage(ex.getMessage());
        }
        return resultFileValidateDTO;

    }

    @Override
    public ListMopDetailOutputDTO getMopInfoFromCrNumber(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService, @WebParam(name = "crNumber") String crNumber, @WebParam(name = "dtCreateId") Long dtCreateId) {
        if (!checkWhiteListIp()) {
            return null;
        }
        ListMopDetailOutputDTO output = new ListMopDetailOutputDTO();

        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                output.setResultCode(2);
                output.setResultMessage("Username or password invalid");
                return output;
            }
        } catch (Exception ex) {
            output.setResultCode(1);
            output.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return output;
        }
        logger.info("getMopInfoFromCrNumber crNumber: " + crNumber);
        if (dtCreateId == null || dtCreateId <= 0) {
            output.setResultCode(1);
            output.setResultMessage("dtCreateId must not null and > 0");
            return output;
        }
        logger.info("getMopInfoFromCrNumber crNumber: " + crNumber);
        if (isNullOrEmpty(crNumber)) {
            output.setResultCode(1);
            output.setResultMessage("CrNumber must not null");
            return output;
        }

        //FileOutputStream os = null;
        //File fileOut = null;
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("createDtFromGnocId", dtCreateId);
            ManageCreateDtFromGnoc manageCreateDtFromGnoc = new ManageCreateDtFromGnocServiceImpl().findById(dtCreateId);
            if (manageCreateDtFromGnoc == null) {
                output.setResultCode(1);
                output.setResultMessage("Do not find dtCreateId");
                return output;
            }
            if (manageCreateDtFromGnoc.getIsCreate() == null || manageCreateDtFromGnoc.getIsCreate().equals(1L)) {
                output.setResultCode(1);
                output.setResultMessage("System is creating DT for this CR");
                return output;
            }

            List<FlowRunAction> flowRuns = (new FlowRunActionServiceImpl()).findList(filters);
            List<Node> lstNode = new ArrayList<>();
            if (flowRuns == null || flowRuns.size() == 0) {
                output.setResultCode(1);
                output.setResultMessage("Do not find DT with this Cr number");
                return output;
            }
            output.setMopDetailDTO(new ArrayList<MopDetailDTO>());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HHmmss");
            for (FlowRunAction flowRun : flowRuns) {
                MopDetailDTO mopDetail = new MopDetailDTO();
                mopDetail.setCreateTime(sdf.format(flowRun.getCreateDate()));
                mopDetail.setMopId(flowRun.getFlowRunId().toString());
                mopDetail.setMopName(flowRun.getFlowRunName());
                mopDetail.setTemplateName(flowRun.getFlowTemplates().getFlowTemplateName());

                //set danh sach node mang tac dong
                if (flowRun.getNodeRuns() != null) {
                    mopDetail.setNodes(new ArrayList<NodeDTO>());
                    for (NodeRun nodeRun : flowRun.getNodeRuns()) {
                        mopDetail.getNodes().add(new NodeDTO(nodeRun.getNode().getNodeCode(), nodeRun.getNode().getNodeIp()));
                        if (!lstNode.contains(nodeRun.getNode())) {
                            lstNode.add(nodeRun.getNode());
                        }
                    }
                }

                //ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
                mopDetail.setMopFileContent(Base64.encodeBase64String(flowRun.getFileContent()));
                mopDetail.setMopFileName(ZipUtils.getSafeFileName(flowRun.getFlowRunId()
                        + "_" + ZipUtils.clearHornUnicode(flowRun.getFlowRunName()) + ".xlsx"));
                mopDetail.setMopFileType("xlsx");
                output.getMopDetailDTO().add(mopDetail);
            }
//            File fileKpi = new File(servletContext.getRealPath("/")
//                    + File.separator + "templates" + File.separator
//                    + "Form_KPI.xlsx");
//            mopDetail.setKpiFileContent(Base64.encodeBase64String(FileUtils.readFileToByteArray(fileKpi)));
//            mopDetail.setKpiFileType("xlsx");
//            mopDetail.setKpiFileName("Form_KPI.xlsx");
            output.setResultCode(0);
            output.setResultMessage("SUCCESS");
            return output;
        } catch (Exception ex) {
            output.setResultCode(1);
            output.setResultMessage(ex.getMessage());
            logger.error(ex.getMessage(), ex);
            return output;
        }
    }

    public static void CreateMopThread(List<FileValidateObject> fileValidateObjects, HashMap<String, HashMap<String, List<StationConfigImport>>> mapConfigFile, Long crId, String crNumber, String userCreate, String pathOut, String pathTemplate, ManageCreateDtFromGnoc manageCreateDtFromGnoc_) {

        Map<String, Object> filters = new HashMap<>();
        filters.put("crId", crId);
        filters.put("isCreate", 1L);
        logger.info("Bat dau tao mop cho CrId: " + crId);
        int createMopSuccess;
        String createMopDetail;
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        CrDTService service = null;
        try {
            URL url = new URL(ResourceBundle.getBundle("config").getString("ws_gnoc_new_cr"));
            service = new CrDTServiceImplService(url).getCrDTServiceImplPort();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        if (service == null) {
            logger.info("service NIMS null");
            return;
        }
        String user = "";
        String pass = "";
        try {
            user = PassProtector.decrypt(bundle.getString("ws_gnoc_user"), "ipchange");
            pass = PassProtector.decrypt(bundle.getString("ws_gnoc_pass"), "ipchange");
            for (int i = 0; i < 10; i++) {
                List<ManageCreateDtFromGnoc> manageCreateDtFromGnocs = new ManageCreateDtFromGnocServiceImpl().findList(filters);
                if (manageCreateDtFromGnocs.size() > 0) {
                    logger.info("Dang tao mop cho CrId: " + crId);
                    Thread.sleep(60000l);
                    if (i == 9) {
                        throw new AppException("Create mop is timeout for CR " + crNumber);
                    }
                } else {
                    try {
                        filters.clear();
                        filters.put("crId", crId);
                        filters.put("isCreate", 0L);
                        manageCreateDtFromGnocs = new ManageCreateDtFromGnocServiceImpl().findList(filters);
                        filters.clear();

                        List<FlowRunAction> flowRunActionsDelete;
                        List<String> crNumbers = new ArrayList<>();
                        if (manageCreateDtFromGnocs.size() > 0) {
                            for (ManageCreateDtFromGnoc manageCreateDtFromGnoc : manageCreateDtFromGnocs) {

                                crNumbers.add(manageCreateDtFromGnoc.getCrNumber());
                            }
                            filters.put("crNumber-EXAC", crNumbers);
                            flowRunActionsDelete = new FlowRunActionServiceImpl().findList(filters);

                            logger.info("get flowRunActionsDelete: " + flowRunActionsDelete.size());
                            if (flowRunActionsDelete != null && flowRunActionsDelete.size() > 0) {
                                new FlowRunActionServiceImpl().delete(flowRunActionsDelete);
                            }

                            logger.info("da xoa flowRunActionsDelete: " + flowRunActionsDelete.size());
                        }
                        try {
                            manageCreateDtFromGnoc_.setIsCreate(1L);
                            new ManageCreateDtFromGnocServiceImpl().saveOrUpdate(manageCreateDtFromGnoc_);
                            for (FileValidateObject fileValidateObject : fileValidateObjects) {
                                for (String key : mapConfigFile.get(fileValidateObject.getFileNameConfig()).keySet()) {
                                    Class cls = Class.forName(mapConfigFile.get(fileValidateObject.getFileNameConfig()).get(key).get(0).getClassController());
                                    Method method = cls.getMethod(mapConfigFile.get(fileValidateObject.getFileNameConfig()).get(key).get(0).getFunctionCreateDt(), HashMap.class, HashMap.class, String.class, String.class, String.class, String.class, LinkedHashMap.class, Long.class, File.class);
                                    logger.info("Call luong create mop method: " + method.getName());
                                    method.invoke(cls.newInstance(), mapConfigFile.get(fileValidateObject.getFileNameConfig()), fileValidateObject.getMapCellObjectImports(), crNumber, userCreate, pathOut, pathTemplate, fileValidateObject.getMapData(), manageCreateDtFromGnoc_.getId(), fileValidateObject.getFileOutPut());
                                    break;
                                }
                            }
                            logger.info("create mop success");
                        } catch (InvocationTargetException ex) {
                            logger.debug(ex.getCause().getMessage(), ex);
                            logger.info("create mop fail, nguyen nhan: " + ex.getCause().getMessage());
                            try {
                                logger.info("Chuan bi Xoa cac mop da tao");
                                filters.clear();
                                filters.put("createDtFromGnocId", manageCreateDtFromGnoc_.getId());
                                flowRunActionsDelete = new FlowRunActionServiceImpl().findList(filters);
                                logger.info("Cac mop da tao: " + flowRunActionsDelete.size());
                                if (flowRunActionsDelete != null && flowRunActionsDelete.size() > 0) {
                                    new FlowRunActionServiceImpl().delete(flowRunActionsDelete);
                                    logger.info("Da xoa cac mop da tao: " + flowRunActionsDelete.size());
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }

                            throw new AppException("Create mop fail :" + ex.getCause().getMessage());
                        } catch (Exception ex) {
                            logger.info("create mop fail, nguyen nhan: " + ex.getMessage());
                            logger.error(ex.getMessage(), ex);

                            try {
                                logger.info("Chuan bi Xoa cac mop da tao");
                                filters.clear();
                                filters.put("createDtFromGnocId", manageCreateDtFromGnoc_.getId());
                                flowRunActionsDelete = new FlowRunActionServiceImpl().findList(filters);
                                logger.info("Cac mop da tao: " + flowRunActionsDelete.size());
                                if (flowRunActionsDelete != null && flowRunActionsDelete.size() > 0) {
                                    new FlowRunActionServiceImpl().delete(flowRunActionsDelete);
                                    logger.info("Da xoa cac mop da tao: " + flowRunActionsDelete.size());
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                            throw new AppException("Create mop fail :" + ex.getMessage());
                        }
                        logger.info("Bat dau cap nhat lai trang thai manageCreateDtFromGnoc id");
                        manageCreateDtFromGnoc_.setIsCreate(0L);
                        new ManageCreateDtFromGnocServiceImpl().saveOrUpdate(manageCreateDtFromGnoc_);
                        logger.info("Cap nhat lai trang thai manageCreateDtFromGnoc id success");
                        logger.info("call ws gnoc update mop start");

                        try {
                            //get list DT vua duoc sinh ra
                            filters.clear();

                            filters.put("createDtFromGnocId", manageCreateDtFromGnoc_.getId());
                            List<FlowRunAction> flowRuns = (new FlowRunActionServiceImpl()).findList(filters);
//                            List<Node> lstNode = new ArrayList<>();

                            if (flowRuns == null || flowRuns.size() == 0) {
                                createMopSuccess = 1;
                                createMopDetail = "List DT for this CR is empty";
                                service.insertVMSADT(user, pass, Config.vmsa_code, crId, manageCreateDtFromGnoc_.getId(), createMopSuccess, createMopDetail, null, null);
                                return;
                            }
                            List<com.viettel.gnoc.cr.service.VmsaMopDetailDTO> mopDetails = new ArrayList<>();
                            com.viettel.gnoc.cr.service.VmsaMopDetailDTO mopDetail;
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HHmmss");
                            for (FlowRunAction flowRun : flowRuns) {
                                mopDetail = new com.viettel.gnoc.cr.service.VmsaMopDetailDTO();
                                mopDetail.setCreateTime(sdf.format(flowRun.getCreateDate()));
                                mopDetail.setMopId(flowRun.getFlowRunId().toString());
                                mopDetail.setMopName(flowRun.getFlowRunName());

                                //set danh sach node mang tac dong
                                if (flowRun.getNodeRuns() != null) {
                                    for (NodeRun nodeRun : flowRun.getNodeRuns()) {
                                        mopDetail.getIpList().add(nodeRun.getNode().getNodeIp());
                                    }
                                }

                                //ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
                                mopDetail.setMopFileContent(Base64.encodeBase64String(flowRun.getFileContent()));
                                mopDetail.setMopFileName(ZipUtils.getSafeFileName(flowRun.getFlowRunId()
                                        + "_" + ZipUtils.clearHornUnicode(flowRun.getFlowRunName()) + ".xlsx"));
                                mopDetail.setMopFileType("xlsx");
                                mopDetails.add(mopDetail);
                            }
                            createMopSuccess = 0;
                            createMopDetail = "Update DT for this CR success";
                            logger.info("CrId: " + crId);
                            logger.info("ValidateKey: " + manageCreateDtFromGnoc_.getId());
                            com.viettel.gnoc.cr.service.ResultDTO resultDTO = service.insertVMSADT(user, pass, Config.vmsa_code, crId, manageCreateDtFromGnoc_.getId(), createMopSuccess, createMopDetail, mopDetails, null);
                            logger.info("Result update DT: " + resultDTO.getMessage());
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                            throw new AppException("Update DT for this CR fail: " + ex.getMessage());
                        }

                        logger.info("call ws gnoc update mop end");
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw ex;
                    }
                }
                break;
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            try {
                logger.info("Bat dau cap nhat lai trang thai manageCreateDtFromGnoc id");
                manageCreateDtFromGnoc_.setIsCreate(0L);
                new ManageCreateDtFromGnocServiceImpl().saveOrUpdate(manageCreateDtFromGnoc_);
                logger.info("Cap nhat lai trang thai manageCreateDtFromGnoc id success");
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            createMopSuccess = 1;
            createMopDetail = ex.getMessage();
            logger.info("Fail roi bat dau goi sang gnoc cap nhat mop");
            logger.info("CrId: " + crId);
            logger.info("ValidateKey: " + manageCreateDtFromGnoc_.getId());

//            com.viettel.gnoc.cr.service.ResultDTO resultDTO = service.insertVMSADT(user, pass, Config.vmsa_code, 29479L, 116L, createMopSuccess, createMopDetail, null, null);
            com.viettel.gnoc.cr.service.ResultDTO resultDTO = service.insertVMSADT(user, pass, Config.vmsa_code, crId, manageCreateDtFromGnoc_.getId(), createMopSuccess, createMopDetail, null, null);
            logger.info("Ket thuc goi sang gnoc cap nhat mop: " + resultDTO.getMessage());
        }
    }

    public static void main(String[] args) {
        try {

//            ResourceBundle bundle = ResourceBundle.getBundle("config");
//            CrDTService service;
//            try {
//                URL url = new URL(ResourceBundle.getBundle("config").getString("ws_gnoc_new_cr"));
//                service = new CrDTServiceImplService(url).getCrDTServiceImplPort();
//            } catch (Exception ex) {
//                logger.error(ex.getMessage(), ex);
//            }
            // convert file to byte[]
            byte[] bFile = readBytesFromFile("C:\\Users\\quytv7.VIETTELGROUP\\Desktop\\Auto_VMSA\\Test_GNOC\\Ericsson_2G_Integrate.xlsx");
//            byte[] bFile = readBytesFromFile("E:\\PROJECT\\VTN_QT06_17001_GSTD_FINAL\\VMSA\\VMSA_Web\\WebContent\\templates\\import\\Auto_VMSA\\Ericsson_3G_Active_Test.xlsx");

            //java nio
            //byte[] bFile = Files.readAllBytes(new File("C:\\temp\\testing1.txt").toPath());
            //byte[] bFile = Files.readAllBytes(Paths.get("C:\\temp\\testing1.txt"));
            // save byte[] into a file
//            File fileOut = new File("E:\\PROJECT\\VTN_QT06_17001_GSTD_FINAL\\VMSA\\VMSA_Web\\out\\artifacts\\vmsa\\templates\\import\\Auto_VMSA" + File.separator + "tmp" + new Date().getTime() + ".xlsx");
            Path path = Paths.get("E:\\PROJECT\\VTN_QT06_17001_GSTD_FINAL\\VMSA\\VMSA_Web\\out\\artifacts\\vmsa\\templates\\import\\Auto_VMSA" + File.separator + "tmp" + new Date().getTime() + ".xlsx");
            Files.write(path, bFile);

            System.out.println("Done");

            //Print bytes[]
//            for (int i = 0; i < bFile.length; i++) {
//                System.out.print((char) bFile[i]);
//            }
            ResultFileValidateDTO resultFileValidateDTO = new ResultFileValidateDTO();
            FileValidateDTO fileValidateDTO = new FileValidateDTO();
//            StringBuilder stringBuilder = new StringBuilder();
//
//            fileValidateDTO.setFileContent(stringBuilder.toString());
            fileValidateDTO.setFileContent(new String(Base64.encodeBase64(bFile)));
            fileValidateDTO.setFileName("Ericsson_2G_Integrate.xlsx");
//            fileValidateDTO.setFileName("Ericsson_3G_Active_Test.xlsx");
            fileValidateDTO.setFileType("xlsx");
            resultFileValidateDTO.setFileValidateDTOs(new ArrayList<FileValidateDTO>());
            resultFileValidateDTO.getFileValidateDTOs().add(fileValidateDTO);
            validateFileInputTemp(new Date().getTime(), "Quytv7_Integrate" + new Date().getTime(), "quytv7", "Intergrate_Station", resultFileValidateDTO);
//            validateFileInputTemp(new Date().getTime(), "Quytv7_Active" + new Date().getTime(), "quytv7", "Active_Test_Station", resultFileValidateDTO);

        } catch (Throwable e) {
            System.err.println(e);
        }
    }

    public static void getMapImportFile(HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile, List<StationConfigImport> stationConfigImports) {
        HashMap<String, List<StationConfigImport>> mapImportSheet;
        List<StationConfigImport> lstStationConfigImports;
        for (StationConfigImport stationConfigImport : stationConfigImports) {
            if (mapImportFile.containsKey(stationConfigImport.getFileName())) {
                mapImportSheet = mapImportFile.get(stationConfigImport.getFileName());
                if (mapImportSheet.containsKey(stationConfigImport.getSheetName())) {
                    mapImportSheet.get(stationConfigImport.getSheetName()).add(stationConfigImport);
                } else {
                    lstStationConfigImports = new ArrayList<>();
                    lstStationConfigImports.add(stationConfigImport);
                    mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                }
                mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
            } else {
                mapImportSheet = new HashMap<>();
                lstStationConfigImports = new ArrayList<>();
                lstStationConfigImports.add(stationConfigImport);
                mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
            }
        }
    }

    private static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            File file = new File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }

        }

        return bytesArray;

    }

    public static ResultFileValidateDTO validateFileInputTemp(Long crId, String crCode,
                                                              String userCreate,
                                                              String business,
                                                              ResultFileValidateDTO resultFileValidateDTO) {
        try {
            if (resultFileValidateDTO == null) {
                resultFileValidateDTO = new ResultFileValidateDTO();
                resultFileValidateDTO.setResultCode(1);
                resultFileValidateDTO.setResultMessage(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.dataInput.isNull"));
                return resultFileValidateDTO;
            }
            if (crId == null || isNullOrEmpty(crCode) || isNullOrEmpty(userCreate) || isNullOrEmpty(business) || resultFileValidateDTO.getFileValidateDTOs() == null || resultFileValidateDTO.getFileValidateDTOs().isEmpty()) {
                resultFileValidateDTO.setResultCode(1);
                resultFileValidateDTO.setResultMessage(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.dataInput.isNull"));
                return resultFileValidateDTO;
            }
            //Get station config with business code
            Map<String, Object> filter = new HashMap<>();
            filter.put("businessCode", business);
//            filter.put("isValidate", 1L);
            List<StationConfigImport> stationConfigImports = new StationConfigImportlServiceImpl().findList(filter);
            if (stationConfigImports == null || stationConfigImports.isEmpty()) {
                resultFileValidateDTO.setResultCode(1);
                resultFileValidateDTO.setResultMessage(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.businessCode.not.exist"));
                return resultFileValidateDTO;
            }
            HashMap<String, HashMap<String, List<StationConfigImport>>> mapConfigFile = new HashMap<>();
            getMapImportFile(mapConfigFile, stationConfigImports);
            final List<FileValidateObject> fileValidateObjects = new ArrayList<>();
            for (FileValidateDTO fileValidateDTO : resultFileValidateDTO.getFileValidateDTOs()) {
                if (isNullOrEmpty(fileValidateDTO.getFileName()) || isNullOrEmpty(fileValidateDTO.getFileContent()) || isNullOrEmpty(fileValidateDTO.getFileType())) {
                    fileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultMessage("You must fill full input for file data input");
                    return resultFileValidateDTO;
                }
                HashMap<String, List<?>> mapCellObjectImports = new HashMap<>();
                LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData = new LinkedHashMap<>();
                boolean isExistFile = false;
                String fileNameStandar = "";
                for (String fileName : mapConfigFile.keySet()) {
                    if (fileValidateDTO.getFileName().toLowerCase().contains(fileName.toLowerCase().replace(".xlsx", ""))) {
                        isExistFile = true;
                        fileNameStandar = fileName;
                        break;
                    }
                }
                if (!isExistFile) {
                    fileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.fileName.isFalse"), fileValidateDTO.getFileName(), business));
                    return resultFileValidateDTO;
                }
                byte[] fileContent = Base64.decodeBase64(fileValidateDTO.getFileContent());
                InputStream inputstream = new ByteArrayInputStream(fileContent);
                ResultDTO resultDTO = new ResultDTO();
                resultDTO.setResultCode(0);

                //Call sang Controller validate
//                com.viettel.controller.DrawTopoStatusExecController
                for (String key : mapConfigFile.get(fileNameStandar).keySet()) {
                    Class cls = Class.forName(mapConfigFile.get(fileNameStandar).get(key).get(0).getClassController());
                    Method method = cls.getMethod(mapConfigFile.get(fileNameStandar).get(key).get(0).getFunctionValidate(), HashMap.class, InputStream.class, String.class, HashMap.class, ResultDTO.class, LinkedHashMap.class);
                    method.invoke(cls.newInstance(), mapConfigFile, inputstream, fileNameStandar, mapCellObjectImports, resultDTO, mapData);
                    break;
                }
                if (inputstream != null) {
                    inputstream.close();
                }
                //Validate VMSA fail thi dung luon gui tra GNOC
                if (resultDTO.getResultCode() == 1) {
                    try {
                        inputstream = new ByteArrayInputStream(fileContent);
                        File fileOut = exportFileResult(inputstream, mapData, 0, fileNameStandar, crCode);
                        fileValidateDTO.setFileContent(new String(Base64.encodeBase64(readBytesFromFile(fileOut.getPath()))));
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw ex;
                    } finally {
                        if (inputstream != null) {
                            inputstream.close();
                        }
                    }
                    resultFileValidateDTO.setResultCode(resultDTO.getResultCode());
                    fileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultMessage(resultDTO.getResultMessage());
                    return resultFileValidateDTO;
                }
                //Validate neu VMSA khong loi thi goi sang NIMS de validate
                //Truong hop nghiep vụ co can check ton tai cell ben NIMS(IS_VALIDATE_NIMS = 1)

                try {
                    ResourceBundle bundle = ResourceBundle.getBundle("config");
                    URL url = new URL(bundle.getString("ws_nims"));
                    for (String sheetName : mapData.keySet()) {
                        if (mapConfigFile.get(fileNameStandar).get(sheetName).get(0) == null ||
                                mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getIsValidateNims() == null ||
                                mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getIsValidateNims().equals(0L)) {
                            logger.info("---Truong hop khong can validate ben nims---");
                            continue;
                        }
                        if (!"cell".equalsIgnoreCase(sheetName)) {
                            continue;
                        }
                        List<CheckCellForCRForm> listCellCheck = new ArrayList<>();
                        CheckCellForCRForm checkCellForCRForm;
                        for (int i = 0; i < mapData.get(sheetName).size(); i++) {
                            checkCellForCRForm = new CheckCellForCRForm();
                            checkCellForCRForm.setCellCode(mapData.get(sheetName).get(i).get(mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getColumnValidate().toLowerCase()).toString());
                            checkCellForCRForm.setId(String.valueOf(i));
                            checkCellForCRForm.setType(mapConfigFile.get(fileNameStandar).get(sheetName).get(0).getNetworkType());
                            listCellCheck.add(checkCellForCRForm);
                        }
                        if (listCellCheck.size() > 0) {
                            UpdateInfraWSService locator = new UpdateInfraWSService(url);
                            UpdateInfraWS service = locator.getUpdateInfraWSPort();
                            listCellCheck = service.checkCellForCR(listCellCheck);
                        }

                        for (int i = 0; i < mapData.get(sheetName).size(); i++) {
                            if (listCellCheck != null && listCellCheck.get(i) != null && !isNullOrEmpty(listCellCheck.get(i).getResult())) {
                                resultDTO.setResultCode(1);
                                fileValidateDTO.setResultCode(1);
                            }

                            mapData.get(sheetName).get(i).put("vmsa_result_code", (listCellCheck != null && listCellCheck.get(i) != null && (listCellCheck.get(i).getResult() == null || listCellCheck.get(i).getResult().isEmpty())) ? "OK" : "NOK");
                            mapData.get(sheetName).get(i).put("vmsa_result_detail", (listCellCheck != null && listCellCheck.get(i) != null && (listCellCheck.get(i).getResult() == null || listCellCheck.get(i).getResult().isEmpty())) ? "Validate Nims OK" : listCellCheck.get(i).getResult());
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    resultFileValidateDTO.setResultCode(1);
                    fileValidateDTO.setResultCode(1);
                    resultFileValidateDTO.setResultMessage("Nims validate NOK: " + ex.getMessage());
                    return resultFileValidateDTO;
                }
                if (resultDTO.getResultCode() == 1) {
                    try {
                        inputstream = new ByteArrayInputStream(fileContent);
                        File fileOut = exportFileResult(inputstream, mapData, 0, fileNameStandar, crCode);
                        fileValidateDTO.setFileContent(new String(Base64.encodeBase64(readBytesFromFile(fileOut.getPath()))));
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw ex;
                    } finally {
                        if (inputstream != null) {
                            inputstream.close();
                        }
                    }
                    resultFileValidateDTO.setResultCode(resultDTO.getResultCode());
                    fileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultMessage("Nims validate NOK");
                    return resultFileValidateDTO;
                } else {
                    try {
                        inputstream = new ByteArrayInputStream(fileContent);
                        File fileOut = exportFileResult(inputstream, mapData, 0, fileNameStandar, crCode);
                        fileValidateDTO.setFileContent(new String(Base64.encodeBase64(readBytesFromFile(fileOut.getPath()))));
                    } catch (Exception ex) {
                        logger.debug(ex.getMessage(), ex);
//                        throw ex;
                    } finally {
                        if (inputstream != null) {
                            inputstream.close();
                        }
                    }
                    fileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultCode(resultDTO.getResultCode());
                    resultFileValidateDTO.setResultMessage("Validate OK");
                }
                FileValidateObject fileValidateObject = new FileValidateObject();
                fileValidateObject.setFileNameConfig(fileNameStandar);
                fileValidateObject.setMapData(mapData);
                fileValidateObject.setMapCellObjectImports(mapCellObjectImports);
                fileValidateObjects.add(fileValidateObject);
            }
            //Quytv7_Create tien trinh create mop
            final ManageCreateDtFromGnoc manageCreateDtFromGnoc = new ManageCreateDtFromGnoc();
            manageCreateDtFromGnoc.setCrId(crId);
            manageCreateDtFromGnoc.setCrNumber(crCode);
            manageCreateDtFromGnoc.setIsCreate(0L);
            manageCreateDtFromGnoc.setUpdateTime(new Date());
            manageCreateDtFromGnoc.setUserCreate(userCreate);
            new ManageCreateDtFromGnocServiceImpl().saveOrUpdate(manageCreateDtFromGnoc);
            resultFileValidateDTO.setDtCreateId(manageCreateDtFromGnoc.getId());
            final HashMap<String, HashMap<String, List<StationConfigImport>>> mapConfigFileFinal = new HashMap<>(mapConfigFile);
            final AutoStopThreadPool autoStopThreadPool = new AutoStopThreadPool("callWSNCMS", 50);
            final String userCreateFinal = userCreate;
            final Long crIdFinal = crId;
            final String crCodeFinal = crCode;
//            final String pathOut = getFolderSave();
            final String pathOut = "E:\\PROJECT\\VTN_QT06_17001_GSTD_FINAL\\VMSA\\VMSA_Web\\out\\artifacts\\vmsa\\templates\\import\\Auto_VMSA\\quytv7";
//            final String pathTemplate = getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
            final String pathTemplate = "E:\\PROJECT\\VTN_QT06_17001_GSTD_FINAL\\VMSA\\VMSA_Web\\out\\artifacts\\vmsa\\templates";
            autoStopThreadPool.addTask(new Runnable() {
                @Override
                public void run() {
                    CreateMopThread(fileValidateObjects, mapConfigFileFinal, crIdFinal, crCodeFinal, userCreateFinal, pathOut, pathTemplate, manageCreateDtFromGnoc);
                }
            });

            new Thread(new Runnable() {

                @Override
                public void run() {
                    autoStopThreadPool.execute();
                    logger.info("Call process create mop: Done");
                }
            }).start();

        } catch (InvocationTargetException ex) {
            logger.debug(ex.getCause().getMessage(), ex);
            resultFileValidateDTO.setResultCode(0);
            resultFileValidateDTO.setResultMessage(ex.getCause().getMessage());
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
            resultFileValidateDTO.setResultCode(0);
            resultFileValidateDTO.setResultMessage(ex.getMessage());
        }
        return resultFileValidateDTO;

    }

    public static String getFolderSave() {
        String pathOut;
        ServletContext servletContext = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        pathOut = servletContext.getRealPath("/") + Config.PATH_OUT;
        File folderOut = new File(pathOut);
        if (!folderOut.exists()) {
            folderOut.mkdirs();
        }
        return pathOut;
    }

    public static String getTemplateMultiExport(String fileName) {
        try {
            ServletContext ctx = (ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
            return ctx.getRealPath("/") + File.separator + "templates" + File.separator + fileName;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return "";
        }

    }

    private static File exportFileResult(InputStream inputstream, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData, int rowStart, String fileName, String crNumber) throws IOException, AppException {

        Workbook workbook = null;

//        String fileOut = "E:\\PROJECT\\VTN_QT06_17001_GSTD_FINAL\\VMSA\\VMSA_Web\\out\\artifacts\\vmsa\\templates\\import\\Auto_VMSA" + File.separator + "tmpResult" + new Date().getTime() + ".xlsx";
        fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_" + crNumber + "_" + fileName;
        String fileOut = getFolderSave() + "IntegrateStation" + File.separator + "ResultFileImport" + File.separator + fileName;
        File fileOutResult = new File(fileOut);
        if (!fileOutResult.getParentFile().exists()) {
            fileOutResult.getParentFile().mkdirs();
        }
        fileOut = fileOutResult.getPath();
        try {
            try {

                if (inputstream == null) {
                    throw new NullPointerException("inputstream is null");
                }
                //Get the workbook instance for XLS/xlsx file
                try {
                    workbook = WorkbookFactory.create(inputstream);
                    if (workbook == null) {
                        throw new NullPointerException("workbook is null");
                    }
                } catch (InvalidFormatException e2) {
                    logger.debug(e2.getMessage(), e2);
                    throw new AppException(MessageUtil.getResourceBundleMessage("label.file.config.type"));
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                MessageUtil.setErrorMessageFromRes("meassage.import.fail2");
                throw e;
            }
            if (workbook == null) {
                throw new NullPointerException();
            }
            CellStyle cellStyle = workbook.createCellStyle();
            Font createFont = workbook.createFont();
            createFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

            cellStyle.setFont(createFont);
            cellStyle.setWrapText(true);
            cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            cellStyle.setFillBackgroundColor(HSSFColor.YELLOW.index);
            Sheet sheet = null;

            for (String sheetName : mapData.keySet()) {
                System.out.println("sheetName: " + sheetName);
                sheet = workbook.getSheet(sheetName);
                int lastColumn = mapData.get(sheetName).get(0).size();
                Cell cellTitleCode = sheet.getRow(rowStart).createCell(lastColumn - 2);
                if (cellTitleCode != null) {
                    cellTitleCode.setCellStyle(cellStyle);
                    cellTitleCode.setCellValue(Config.vmsa_result_code);
                }


                Cell cellTitleDetail = sheet.getRow(rowStart).createCell(lastColumn - 1);
                if (cellTitleDetail != null) {
                    cellTitleDetail.setCellStyle(cellStyle);
                    cellTitleDetail.setCellValue(Config.vmsa_result_detail);
                }

                for (int i = rowStart; i < mapData.get(sheetName).size(); i++) {
                    logger.info("CellCode: " + mapData.get(sheetName).get(i).get(Config.vmsa_result_code));
                }

                logger.info("mapData.get(sheetName).size() " + mapData.get(sheetName).size());
                logger.info("sheet.getLastRowNum(): " + sheet.getLastRowNum());
                for (int i = rowStart; i < mapData.get(sheetName).size(); i++) {
                    Cell cellCode = sheet.getRow(i + 1).createCell(lastColumn - 2);
                    if (cellCode != null) {
                        cellCode.setCellStyle(cellStyle);
                        cellCode.setCellValue(mapData.get(sheetName).get(i).get(Config.vmsa_result_code) == null ? "" : mapData.get(sheetName).get(i).get(Config.vmsa_result_code));
                    }
                    Cell cellDetail = sheet.getRow(i + 1).createCell(lastColumn - 1);
                    if (cellDetail != null && cellStyle != null) {
                        cellDetail.setCellStyle(cellStyle);
                        cellDetail.setCellValue(mapData.get(sheetName).get(i).get(Config.vmsa_result_detail) == null ? "" : mapData.get(sheetName).get(i).get(Config.vmsa_result_detail));
                    }
                }
            }
            try {
                FileOutputStream outputStream = new FileOutputStream(fileOut);
                workbook.write(outputStream);
                workbook.close();
            } catch (FileNotFoundException e) {
                logger.error(e.getMessage(), e);
                throw e;
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
                throw e;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
//                    throw e;
                }
            }
            if (inputstream != null) {
                try {
                    inputstream.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
//                    throw e;
                }
            }
        }
        return fileOutResult;
    }

    public static boolean isNullOrEmpty(String obj1) {
        return (obj1 == null || "".equals(obj1.trim()));
    }

    //20171109_Quytv7_WS cho Gnoc call_luong toan trinh_end
//    @Override
//    public ResultDTO startRunSecurityMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
//                                       @WebParam(name = "paramValues") String paramValues, @WebParam(name = "flowTemplateId") Long mopId,
//                                       @WebParam(name = "username") String username, @WebParam(name = "password") String password,
//                                       @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "userImpact") String userImpact,
//                                       @WebParam(name = "passImpact") String passImpact) {
//        ResultDTO result = new ResultDTO();
//        result.setResultCode(RESPONSE_FAIL);
//        logger.info("start start run mop");
//        Date startTime = new Date();
//        try {
//            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
//            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_user_service"), "ipchange");
//
//            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
//                result.setResultMessage("Username hoáº·c máº­t kháº©u webservice khÃ´ng chÃ­nh xÃ¡c");
//                return result;
//            }
//        } catch (Exception ex) {
//            result.setResultMessage("Xáº£y ra lá»—i khi xÃ¡c thá»±c user/pass");
//            logger.error(ex.getMessage(), ex);
//            return result;
//        }
//
//
//        // Kiem tra xem cac tham so cua mop da dc dien day du hay chua
//        List<ParamValuesDTO> lstParamValDTO = new ArrayList<>();
//        try {
//            if (paramValues == null
//                    || paramValues.isEmpty()) {
//                result.setResultMessage("KhÃ´ng cÃ³ tham sá»‘ Ä‘áº§u vÃ o cho mop");
//                return result;
//            }
//            Gson g = new Gson();
//            Type listType = new TypeToken<List<ParamValuesDTO>>() {
//            }.getType();
//            lstParamValDTO = new Gson().fromJson(paramValues, listType);
//
//            for (ParamValuesDTO p : lstParamValDTO) {
//                if (p.getParamValue() == null || p.getParamValue().trim().isEmpty()) {
//                    result.setResultMessage("Param: " + p.getParamCode() + " was not filled");
//                    return result;
//                }
//            }
//        } catch (Exception e) {
//            logger.error(e.getMessage(), e);
//            result.setResultMessage("Error when check input param values");
//            return result;
//        }
//
//        /**
//         * Lay thong tin node mang tac dong dua vao nodecode
//         */
//        Node node = null;
//        if (nodeCode != null && !nodeCode.trim().isEmpty()) {
//            try {
//                Map<String, Object> filtes = new HashedMap();
//                filtes.put("nodeCode", nodeCode);
//
//                List<Node> nodes = new NodeServiceImpl().findListExac(filtes, null);
//                if (nodes != null && !nodes.isEmpty()) {
//                    node = nodes.get(0);
//                }
//            } catch (Exception e) {
//                logger.error(e.getMessage(), e);
//            }
//        }
//        if (node == null) {
//            result.setResultMessage("Cannot get node information from node code: " + nodeCode);
//            return result;
//        }
//
//        try {
//            FlowTemplates selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(mopId);
//            FlowRunAction flowRunAction = new FlowRunAction();
//            List<NodeRun> nodeRuns = new ArrayList<NodeRun>();
//            flowRunAction.setCreateDate(new Date());
//            if (flowRunAction.getFlowRunName() != null) {
//                flowRunAction.setFlowRunName(flowRunAction.getFlowRunName().trim());
//            }
//            flowRunAction.setFlowRunName(selectedFlowTemplate.getFlowTemplateName());
//            flowRunAction.setStatus(1L);
//            flowRunAction.setCrNumber("");
//            flowRunAction.setCreateBy(username);
//            flowRunAction.setFlowTemplates(selectedFlowTemplate);
//            flowRunAction.setTimeRun(new Date());
//
//            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
//            Session session = (Session) objs[0];
//            Transaction tx = (Transaction) objs[1];
//            try {
//                Map<Long, List<ActionOfFlow>> mapGroupAction = new HashMap<>();
//                for (ActionOfFlow actionOfFlow : selectedFlowTemplate.getActionOfFlows()) {
//                    if (mapGroupAction.get(actionOfFlow.getGroupActionOrder()) == null) {
//                        mapGroupAction.put(actionOfFlow.getGroupActionOrder(), new ArrayList<ActionOfFlow>());
//                    }
//                    mapGroupAction.get(actionOfFlow.getGroupActionOrder()).add(actionOfFlow);
//                }
//
//                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
//                List<ParamValue> paramValuesObj = new ArrayList<ParamValue>();
//
//                List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<NodeRunGroupAction>();
//                if (paramValues != null && !paramValues.trim().isEmpty()) {
//
////                    for (ParamValsOfNodeDTO paramsNode : paramValsDto.getLstParamValOfNode()) {
//                    logger.info("chay vao node :" + node.getNodeCode());
//                    NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM);
//                    nodeRuns.add(nodeRun);
//                    List<ParamValue> lstParamValsOfNode = new MopUtils().getLstParamInput(node, mopId);
//                    List<ParamValue> _paramValueFilledOfNode = new MopUtils().updateParamInputValues(lstParamValsOfNode, lstParamValDTO, nodeRun);
//                    if (_paramValueFilledOfNode == null || _paramValueFilledOfNode.isEmpty()) {
//                        logger.error("ERROR CANNOT GET PARAM VALUE FROM PARAM USER SET");
//                        result.setResultMessage("ERROR CANNOT GET PARAM VALUE FROM PARAM USER SET");
//                        return result;
//                    }
////                        if (mapGroupAction.get(node) != null) {
//                    logger.info(" co vao mapGroupAction size = " + mapGroupAction.get(node));
//                    for (Map.Entry<Long, List<ActionOfFlow>> entry : mapGroupAction.entrySet()) {
//                        NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
//                                new NodeRunGroupActionId(node.getNodeId(),
//                                        flowRunAction.getFlowRunId(),
//                                        entry.getValue().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), entry.getValue().get(0), nodeRun);
//                        nodeRunGroupActions.add(nodeRunGroupAction);
//                    } // end loop for group action
//                    logger.info(" thoai khoi mapGroupAction");
////                        }
////                    }
//                    logger.info(" xoa session");
////                    session.clear();
//                    logger.info(" insert NodeRunServiceImpl ");
//                    new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
//                    logger.info(" insert ParamValueServiceImpl ");
//                    new ParamValueServiceImpl().saveOrUpdate(_paramValueFilledOfNode, session, tx, false);
//
//                    session.flush();
//                    session.clear();
//                    new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
////				session.flush();
////		 		tx.commit();
//
//                } else {
//
//                }
//                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
//
//                // start run mop
//                startProcessWithAcc(flowRunAction, nodeRuns, "", username, userImpact, passImpact);
//
//                /*
//                Ghi log tac dong nguoi dung
//                */
//                try {
//                    LogUtils.writelog(startTime, WSForGNOCImpl.class.getName(),
//                            Thread.currentThread().getStackTrace()[1].getMethodName(),
//                            LogUtils.ActionType.IMPACT.name(), flowRunAction.toString());
//                } catch (Exception e) {
//                    logger.error(e.getMessage(), e);
//                }
//
//                result.setResultCode(RESPONSE_SUCCESS);
//                result.setResultMessage("báº¯t Ä‘áº§u cháº¡y mop " + selectedFlowTemplate.getFlowTemplateName());
//
//            } catch (Exception e) {
//                if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
//                    tx.rollback();
//                }
//                logger.error(e.getMessage(), e);
//                result.setResultMessage("Xáº£y ra lá»—i khi cháº¡y mop " + selectedFlowTemplate.getFlowTemplateName());
//                return result;
//            } finally {
//                if (session.isOpen()) {
//                    session.close();
//                }
//            }
//        } catch (Exception e) {
//            result.setResultMessage("Xáº£y ra lá»—i khi cháº¡y mop");
//            logger.error(e.getMessage(), e);
//        }
//        return result;
//    }
}
