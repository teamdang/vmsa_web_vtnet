package com.viettel.webservice.utils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.viettel.controller.GenerateFlowRunController;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.*;
import com.viettel.nms.nocpro.service.AuthorityBO;
import com.viettel.nms.nocpro.service.JsonResponseBO;
import com.viettel.nms.nocpro.service.ParameterBO;
import com.viettel.nms.nocpro.service.RequestInputBO;
import com.viettel.persistence.*;
import com.viettel.util.Config;
import com.viettel.util.NocProWebserviceUtils;
import com.viettel.util.ParamUtil;
import com.viettel.webservice.object.JsonNocproData;
import com.viettel.webservice.object.ParamValuesDTO;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hanh on 4/18/2017.
 */
public class MopUtils {

    private static final List<String> STATION_PARAMS_CODE = new ArrayList<String>(
            Arrays.asList("CABINET_CODE", "STATION_CODE", "BTSNAME", "NODEBNAME")
    );

    public static List<String> getSTATION_PARAMS_CODE() {
        return STATION_PARAMS_CODE;
    }

    protected static final org.slf4j.Logger logger = LoggerFactory.getLogger(MopUtils.class);

    public List<ParamValue> getLstParamInput(Node node, Long flowTemplateId) {

        logger.info("getLstParamInput node: " + (node != null ? node.toString() : "null") + ", flowTemplateId: " + flowTemplateId);

        FlowRunAction flowRunAction = new FlowRunAction();
        Map<Node, List<ParamValue>> mapParamValue = new HashMap<>();
        if (node != null && flowTemplateId != null) {
            try {
                FlowTemplates selectedFlowTemplates = new FlowTemplatesServiceImpl().findById(flowTemplateId);
                Set<ParamInput> inputs = new LinkedHashSet<>();
                Map<Long, Long> mapParamInputGroupCode = new HashMap<>();
                Multimap<Long, ParamValue> _mapParamValueGroup = ArrayListMultimap.create();
                if (selectedFlowTemplates != null) {
                    for (ActionOfFlow actionOfFlow : selectedFlowTemplates.getActionOfFlows()) {
                        List<ActionDetail> actionDetails = new ActionDetailServiceImpl().findList("from ActionDetail where action.actionId =? and vendor.vendorId = ? "
                                + "and version.versionId =? and nodeType.typeId = ?", -1, -1, actionOfFlow.getAction().getActionId(), node.getVendor().getVendorId(),
                                node.getVersion().getVersionId(), node.getNodeType().getTypeId());
                        for (ActionDetail actionDetail : actionDetails) {
                            {
                                List<ActionCommand> actionCommands = new ActionCommandServiceImpl().findList("from ActionCommand where actionDetail.detailId = ? and "
                                        + "commandDetail.vendor.vendorId =? and commandDetail.version.versionId =? ", -1, -1, actionDetail.getDetailId(), node.getVendor().getVendorId(),
                                        node.getVersion().getVersionId());
                                List<Long> commandDetailIds = new ArrayList<>();
                                for (ActionCommand actionCommand : actionCommands) {
                                    CommandDetail commandDetail = actionCommand.getCommandDetail();
                                    commandDetailIds.add(commandDetail.getCommandDetailId());
                                }
                                Map<String, Collection<?>> map = new HashMap<>();
                                map.put("commandDetailIds", commandDetailIds);
                                
                                List<ParamInput> paramInputs = new ArrayList<>();
                                if (!commandDetailIds.isEmpty()) {
                                    paramInputs = new ParamInputServiceImpl().findListWithIn("from ParamInput where commandDetail.commandDetailId in (:commandDetailIds)", -1, -1, map);
                                }
                                
                                Multimap<CommandDetail, ParamInput> mapParamInputs = ArrayListMultimap.create();
                                for (ParamInput paramInput2 : paramInputs) {
                                    mapParamInputs.put(paramInput2.getCommandDetail(), paramInput2);
                                }
                                if (mapParamInputs.size() > 0) {
                                    for (ActionCommand actionCommand : actionCommands) {
                                        CommandDetail commandDetail = actionCommand.getCommandDetail();
                                        Collection<ParamInput> collection = mapParamInputs.get(commandDetail);
                                        if (collection.size() > 0) {
                                            for (ParamInput paramInput : collection) {
                                                if (paramInput.getParamGroups().size() > 0) {
                                                    for (ParamGroup paramGroup : paramInput.getParamGroups()) {
                                                        if (paramGroup.getFlowTemplates().equals(selectedFlowTemplates)) {
                                                            paramInput.setParamDefault(paramGroup.getParamDefault());
                                                            if (paramGroup.getGroupCode() != null) {
                                                                paramInput.setColor(Config.getCOLORS()[Math.min(paramGroup.getGroupCode().intValue(), Config.getCOLORS().length - 1)]);
                                                            }

                                                            if (paramGroup.getGroupCode() != null) {
                                                                mapParamInputGroupCode.put(paramInput.getParamInputId(), paramGroup.getGroupCode());
                                                            }
                                                        }
                                                    }
                                                }

                                                paramInput.setIsDeclare(false);
                                                paramInput.setInOut(false);
                                                paramInput.setActionOfFolowId(actionOfFlow.getStepNum().toString());

                                                // Kiem tra xem tham so co phai la tham so tham chieu
                                                if (checkParamInOut(paramInput, actionOfFlow, commandDetail)) {
                                                    paramInput.setInOut(true);
                                                }
                                                inputs.add(paramInput);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                List<ParamValue> paramValues = new LinkedList<>();
                for (ParamInput paramInput : inputs) {
                    ParamValue paramValue = new ParamValue();
                    paramValue.setParamInput(paramInput);
                    paramValue.setParamCode(paramInput.getParamCode());
                    paramValue.setNodeRun(new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM));
                    paramValue.setDisableByInOut((paramInput.getInOut() == true));

                    paramValue.setParamValue(paramInput.getParamDefault());
                    Long groupCode = mapParamInputGroupCode.get(paramInput.getParamInputId());
                    paramValue.setGroupCode(groupCode);
                    if (groupCode != null) {
                        _mapParamValueGroup.put(groupCode, paramValue);
                    }
                    paramValues.add(paramValue);
                }
                mapParamValue.put(node, paramValues);
            } catch (AppException | SysException e) {
                logger.error(e.getMessage(), e);
            }

        }
        return mapParamValue.get(node);
    }

    private boolean checkParamInOut(ParamInput paramInput, ActionOfFlow actionOfFlow, CommandDetail commandDetail) {
        boolean check = false;
        if (paramInput != null) {
            try {
                if (paramInput.getParamInOuts() != null && !paramInput.getParamInOuts().isEmpty()) {
                    List<ParamInOut> paramInOuts = paramInput.getParamInOuts();
                    for (ParamInOut paramInOut : paramInOuts) {
                        if (paramInOut.getActionOfFlowByActionFlowInId().getStepNum().intValue() == actionOfFlow.getStepNum().intValue()
                                && paramInOut.getActionCommandByActionCommandInputId().getCommandDetail().getCommandDetailId().intValue() == commandDetail.getCommandDetailId().intValue()) {
                            check = true;
                        }
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return check;
    }

    public ArrayList<ParamValue> distinctParamValueSameParamCode(List<ParamValue> paramValues) {
        ArrayList<ParamValue> _paramValues = new ArrayList<>();
        logger.info("distinctParamValueSameParamCode");
        try {
            Set<String> _tmp = new HashSet<>();
            Map<String, String> _tmpFormula = new HashMap<>();
            Map<String, String> _tmpDescription = new HashMap<>();

            Map<CommandDetail, Boolean> mapCommand = new HashMap<>();
            for (ParamValue paramValue : paramValues) {
                if (!mapCommand.containsKey(paramValue.getParamInput().getCommandDetail())) {
                    mapCommand.put(paramValue.getParamInput().getCommandDetail(), Boolean.TRUE);
                }
            }

            for (CommandDetail command : mapCommand.keySet()) {
                command.calculateParam(paramValues, logger);
            }

            for (ParamValue paramValue : paramValues) {
                String paramCode = paramValue.getParamInput().getParamCode();
                if (!_tmp.contains(paramCode)) {
                    paramValue.setIsDeclare(paramValue.getParamInput().getIsDeclare());
                    paramValue.setDisableByInOut(!paramValue.getParamInput().getParamInOuts().isEmpty());
                    _paramValues.add(paramValue);
                    _tmp.add(paramCode);
                } else {
                    if (paramValue.getParamInput().getInOut()) {
                        for (ParamValue paramValue2 : _paramValues) {
                            if (paramValue.getParamInput().equals(paramValue2.getParamInput())) {
                                paramValue2.getParamInput().setInOut(true);
                            }
                        }
                    }

                    if (paramValue.getParamInput().getParamInOuts().isEmpty()) {
                        for (ParamValue paramValue2 : _paramValues) {
                            if (paramValue.getParamInput().getParamCode().equals(paramValue2.getParamInput().getParamCode())) {
                                paramValue2.setDisableByInOut(false);
                            }
                        }
                    }

                    if (paramValue.getParamInput().getIsDeclare()) {
                        for (ParamValue paramValue2 : _paramValues) {
                            if (paramValue.getParamInput().getParamCode().equals(paramValue2.getParamInput().getParamCode())) {
                                paramValue2.setIsDeclare(true);
                            }
                        }
                    }
                }
                if (paramValue.getParamInput().getIsFormula()) {
                    _tmpFormula.put(paramCode, paramValue.getParamInput().getParamFormula());
                }
                if (paramValue.getParamInput().getDescription() != null
                        && !paramValue.getParamInput().getDescription().trim().isEmpty()) {
                    _tmpDescription.put(paramCode, paramValue.getParamInput().getDescription());
                }
            }

            for (ParamValue paramValue : _paramValues) {
                if (_tmpFormula.containsKey(paramValue.getParamInput().getParamCode())) {
                    paramValue.setFormula(_tmpFormula.get(paramValue.getParamInput().getParamCode()));
                }
                if (_tmpDescription.containsKey(paramValue.getParamInput().getParamCode())) {
                    paramValue.setDescription(_tmpDescription.get(paramValue.getParamInput().getParamCode()));
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return _paramValues;
    }

    public ArrayList<ParamValue> updateParamValues(ArrayList<ParamValuesDTO> allParamsOfNode,
            ArrayList<ParamValuesDTO> paramsDistinct,
            NodeRun nodeRun) {
        ArrayList<ParamValue> lstParamVals = new ArrayList<>();
        if (paramsDistinct != null && allParamsOfNode != null) {
            try {
                Map<Long, String> mapParamIdVal = new HashMap<>();
                Map<Long, String> mapGroupCodeVal = new HashMap<>();
                Map<String, String> mapParamCodeVal = new HashMap<>();
                for (ParamValuesDTO p : paramsDistinct) {
                    mapParamIdVal.put(p.getParamInputId(), p.getParamValue());
                    mapParamCodeVal.put(p.getParamCode(), p.getParamValue());
                    if (p.getGroupCode() != null) {
                        mapGroupCodeVal.put(p.getGroupCode(), p.getParamValue());
                    }
                }

                ParamValue pVal;
                for (ParamValuesDTO p : allParamsOfNode) {
                    pVal = new ParamValue();
                    pVal.setCreateTime(new Date());
                    pVal.setDescription(p.getDescription());
                    pVal.setFormula(p.getFormula());
                    pVal.setParamCode(p.getParamCode());
                    if (mapParamIdVal.get(p.getParamInputId()) != null) {
                        pVal.setParamValue(mapParamIdVal.get(p.getParamInputId()));
                    } else if (mapParamCodeVal.get(p.getParamCode()) != null) {
                        pVal.setParamValue(mapParamCodeVal.get(p.getParamCode()));
                    } else if (p.getGroupCode() != null && mapGroupCodeVal.get(p.getGroupCode()) != null) {
                        pVal.setParamValue(mapParamIdVal.get(mapGroupCodeVal.get(p.getGroupCode())));
                    }
                    pVal.setParamValueId(null);
                    pVal.setParamInput(new ParamInputServiceImpl().findById(p.getParamInputId()));
                    pVal.setNodeRun(nodeRun);
                    lstParamVals.add(pVal);
                }
            } catch (AppException | SysException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return lstParamVals;
    }

    public ArrayList<ParamValue> updateParamInputValues(List<ParamValue> allParamsOfNode,
            List<ParamValuesDTO> paramsDistinct,
            NodeRun nodeRun) throws Exception {
        ArrayList<ParamValue> lstParamVals = new ArrayList<>();
        if (paramsDistinct != null && allParamsOfNode != null) {
            try {
                Map<Long, String> mapParamIdVal = new HashMap<>();
                Map<Long, String> mapGroupCodeVal = new HashMap<>();
                Map<String, String> mapParamCodeVal = new HashMap<>();
                for (ParamValuesDTO p : paramsDistinct) {
                    mapParamIdVal.put(p.getParamInputId(), p.getParamValue());
                    if (p.getGroupCode() != null) {
                        mapGroupCodeVal.put(p.getGroupCode(), p.getParamValue());
                    }
                    mapParamCodeVal.put(p.getParamCode(), p.getParamValue());
                }

                ParamValue pVal;
                for (ParamValue p : allParamsOfNode) {
                    if (!p.isDisableByInOut()) {
                        pVal = new ParamValue();
                        pVal.setCreateTime(new Date());
                        pVal.setDescription(p.getDescription());
                        pVal.setFormula(p.getFormula());
                        pVal.setParamCode(p.getParamCode());
                        if (mapParamIdVal.get(p.getParamInput().getParamInputId()) != null) {
                            pVal.setParamValue(mapParamIdVal.get(p.getParamInput().getParamInputId()));
                        } else if (mapParamCodeVal.get(p.getParamCode()) != null) {
                            pVal.setParamValue(mapParamCodeVal.get(p.getParamCode()));
                        } else if (p.getGroupCode() != null && mapGroupCodeVal.get(p.getGroupCode()) != null) {
                            pVal.setParamValue(mapParamIdVal.get(mapGroupCodeVal.get(p.getGroupCode())));
                        } else {
                            throw new Exception("Can not get param value: " + p.getParamCode());
                        }
                        pVal.setParamValueId(null);
                        pVal.setParamInput(p.getParamInput());
                        pVal.setNodeRun(nodeRun);
                        lstParamVals.add(pVal);
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new Exception(e.getMessage());
            }
        }
        return lstParamVals;
    }

    /**
     * Ham lay thong tin noi dung canh bao tu nocpro dua vao work order code
     *
     * @param workOrderCode
     * @return
     */
    public static JsonNocproData getNocproAlarm(String workOrderCode) {
        try {
            AuthorityBO authorityBO = new AuthorityBO();
            authorityBO.setUserName(ConstantsWS.WEBSERVICE_NOCPRO_USER);
            authorityBO.setPassword(ConstantsWS.WEBSERVICE_NOCPRO_PASS);
            authorityBO.setRequestId(ConstantsWS.WEBSERVICE_NOCPRO_RESQUEST_ID);

            ParameterBO parameterBO = new ParameterBO();
            parameterBO.setName("woTroubleCode");
            parameterBO.setValue(workOrderCode);
            parameterBO.setType("String");

            RequestInputBO inputBO = new RequestInputBO();
            inputBO.setCode("VMSA_ACCESS_MANUAL");
            inputBO.getParams().add(parameterBO);

            JsonResponseBO resultData = NocProWebserviceUtils.getService().getDataJson(authorityBO, inputBO);
            JsonElement jsonElement = new JsonParser().parse(resultData.getDataJson());
//            System.out.println(jsonElement.getAsJsonObject().getAsJsonArray("data").get(0).toString());

            Gson gson = (new GsonBuilder()).create();
            JsonArray jsonArray = jsonElement.getAsJsonObject().getAsJsonArray("data");
            JsonNocproData nocproData = null;
            if (jsonArray != null && jsonArray.size() > 0) {
                nocproData = gson.fromJson(jsonArray.get(0).toString(), JsonNocproData.class);
            }
            if (nocproData != null) {
                if ("ERICSSON".equalsIgnoreCase(nocproData.getVendor().trim()) && "3G".equalsIgnoreCase(nocproData.getType_station().trim())) {
                    nocproData.setNode_code("NODEB_RUN");
                }
                return nocproData;
            }
        } catch (JsonSyntaxException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static void main(String args[]) {
        try {
            Node node = new NodeServiceImpl().findById(25610l);
            new MopUtils().getLstParamInput(node, 16716l);
        } catch (AppException | SysException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Ham lay gia tri param tu thong tin canh bao nocpro
     *
     * @param paramCode
     * @param log
     * @return
     */
    public static String getResetParamVal(String paramCode, String log, Long mopId) throws Exception {
        if (log != null && paramCode != null) {
            try {
                Map<String, Object> fillters = new HashMap<>();
                fillters.put("manualParamCode", paramCode);
//                fillters.put("configMopId", mopId);
                List<AutoResetConfigParam> paramsConfig = new AutoRestConfigParamServiceImpl().findList(fillters);
                if (paramsConfig != null && !paramsConfig.isEmpty()) {
                    String regex = paramsConfig.get(0).getRegexFomula();
                    if (regex != null) {
                        Pattern p = Pattern.compile(regex);   // the pattern to search for
                        Matcher m = p.matcher(log);

                        // if we find a match, get the group
                        if (m.find()) {
                            // we're only looking for one group, so get it
                            return m.group(1);
                        }
                    }
                    throw new Exception("ERROR_GET_PARAM_VALUE");
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new Exception(e.getMessage());
            }
        }
        return null;
    }

    /**
     * Ham lay thong tin ip nodeb
     *
     * @param nodebCode
     * @return
     */
    public static String getIpNodeB(String nodebCode) {
        if (nodebCode != null && !nodebCode.trim().isEmpty()) {
            try {
                Map<String, Object> filters = new HashedMap();
                filters.put("nodeb", nodebCode);
                List<MapNode> mapNodes = new MapNodeServiceImpl().findList(filters);
                if (mapNodes != null && !mapNodes.isEmpty()) {
                    if (mapNodes.get(0).getNodebIp() != null) {
                        return mapNodes.get(0).getNodebIp();
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return null;
    }

    /**
     * Ham lay thong tin tg-number tu ma cell
     *
     * @param cellCode
     * @return
     */
    public static String getTgNumber(String cellCode) {
        if (cellCode != null && !cellCode.trim().isEmpty()) {
            List<String> vals = Arrays.asList(cellCode.trim().split("-"));
            if (vals != null && vals.size() >= 2) {
                return vals.get(1);
            }
        }
        return null;
    }

    public static boolean checkWoRunning(String woCode) {
        boolean check = false;
        if (woCode != null) {
            Map<String, Object> filters = new HashedMap();
            filters.put("crNumber", woCode);
            Map<String, String> orders = new HashedMap();
            orders.put("timeRunActual", "DESC");
            try {
                List<FlowRunAction> flowRunActions = new FlowRunActionServiceImpl().findListExac(filters, orders);
                if (flowRunActions != null && !flowRunActions.isEmpty()) {
                    // Neu flow template dang thuc hien
                    if (flowRunActions.get(0).getStatus().intValue() == 2) {
                        return true;
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return check;
    }

    /**
     * Kiem tra xem template co khai bao cho vendor va network type tren nocpro
     *
     * @param flowTemplates
     * @param nocproData
     * @return
     */
    public static boolean checkTemplate(FlowTemplates flowTemplates, JsonNocproData nocproData) {
        try {
            if (flowTemplates != null && nocproData != null && flowTemplates.getActionOfFlows() != null) {
                List<ActionOfFlow> actionOfFlows = flowTemplates.getActionOfFlows();
                List<ActionDetail> actionDetails;

                for (ActionOfFlow actionOfFlow : actionOfFlows) {
                    actionDetails = actionOfFlow.getAction().getActionDetails();
                    if (actionDetails != null) {
                        for (ActionDetail actionDetail : actionDetails) {
                            if (actionDetail.getVendor().getVendorName().trim().equalsIgnoreCase(nocproData.getVendor().trim())
                                    && getNormalNetworkType(actionDetail.getNodeType().getTypeName()).equalsIgnoreCase(nocproData.getType_station().trim().toUpperCase())) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    private static String getNormalNetworkType(String networkType) {
        if (networkType != null) {
            String normalNetTypeName = "";
            switch (networkType.toUpperCase()) {
                case "BSC":
                case "BTS":
                    normalNetTypeName = "2G";
                    break;
                case "RNC":
                case "NODEB":
                    normalNetTypeName = "3G";
                    break;
                default:
                    break;
            }
            return normalNetTypeName;
        } else {
            return "";
        }
    }

}
