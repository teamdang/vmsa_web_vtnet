package com.viettel.webservice.utils;

import com.viettel.util.MessageUtil;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by hanhnv68 on 6/17/2017.
 */
public class WhilelistNodeIns {

    private static final Logger logger = Logger.getLogger(WhilelistNodeIns.class);
    private Map<String, String> bscRncWhilelist = new HashedMap();
    private static WhilelistNodeIns ins;

    public static WhilelistNodeIns getIns() {
        synchronized (WhilelistNodeIns.class) {
            if (ins == null) {
                synchronized (WhilelistNodeIns.class) {
                    ins = new WhilelistNodeIns();
                }
            }
        }
        return ins;
    }

    private WhilelistNodeIns() {
        try {
            logger.info("START INITIAL WHILE LIST BSC RNC NODE CODE");
            String strNodeCodesWhilelist = MessageUtil.getResourceBundleConfig("bsc_rnc_halted_cell_whilelist");
            if (strNodeCodesWhilelist != null) {
                List<String> lstNodeCode = Arrays.asList(strNodeCodesWhilelist.trim().split(";"));
                for (String node : lstNodeCode) {
                    bscRncWhilelist.put(node, "ok");
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public Map<String, String> getBscRncWhilelist() {
        return bscRncWhilelist;
    }

    public void setBscRncWhilelist(Map<String, String> bscRncWhilelist) {
        this.bscRncWhilelist = bscRncWhilelist;
    }

    public static void main(String args[]) {
        try {
            System.out.println(WhilelistNodeIns.getIns().getBscRncWhilelist().size());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
