/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.viettel.webservice.object.FlowRunLogCmdObject;
import com.viettel.webservice.object.FlowTemplatesResult;
import com.viettel.webservice.object.ParamInputDTO;
import com.viettel.webservice.object.ResultCreateMop;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 *
 * @author hohien
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface WSForSecurity {
    /*
    accountEffectiveDate: ddMMyyy HH:mm:ss
    */
    @WebMethod(operationName = "changeEffectiveDateAccount")
    ResultCreateMop changeEffectiveDateAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "accountEffectiveDate") String accountEffectiveDate,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    /*
    accountExpireDate: ddMMyyy HH:mm:ss
    */
    @WebMethod(operationName = "changeExpireDateAccount")
    ResultCreateMop changeExpireDateAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "accountExpireDate") String accountExpireDate,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "extendDateAccount")
    ResultCreateMop extendDateAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "accountEffectiveDate") String accountEffectiveDate, @WebParam(name = "accountExpireDate") String accountExpireDate,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "changePasswordAccount")
    ResultCreateMop changePasswordAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "passwordNew") String passwordNew,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "changePasswordAccountNormal")
    ResultCreateMop changePasswordAccountNormal(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "passwordOld") String passwordOld,
                           @WebParam(name = "passwordNew") String passwordNew);
    
    @WebMethod(operationName = "changeRoleAccount")
    ResultCreateMop changeRoleAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "normalizeRole") String normalizeRole,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "deleteAccount")
    ResultCreateMop deleteAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "createAccount")
    ResultCreateMop createAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "password") String password, @WebParam(name = "nodeCode") String nodeCode,
                           @WebParam(name = "normalizeRole") String normalizeRole, @WebParam(name = "accountExpireDate") String accountExpireDate,
                           @WebParam(name = "accountEffectiveDate") String accountEffectiveDate,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "executeService")
    ResultCreateMop executeService(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "serviceCode") String serviceCode,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "params") ParamInputDTO params,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "grantRoleAccount")
    ResultCreateMop grantRoleAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "normalizeRole") String normalizeRole,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "revokeRoleAccount")
    ResultCreateMop revokeRoleAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "normalizeRole") String normalizeRole,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "viewAccount")
    FlowRunLogCmdObject viewAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                    @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                                    @WebParam(name = "nodeCode") String nodeCode,
                                    @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "resetAccount")
    ResultCreateMop resetAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName, @WebParam(name = "password") String password,
                           @WebParam(name = "nodeCode") String nodeCode,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "lockAccount")
    ResultCreateMop lockAccount(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "crNumber") String crNumber, @WebParam(name = "accountName") String accountName,
                           @WebParam(name = "nodeCode") String nodeCode,
                           @WebParam(name = "accountAdmin") String accountAdmin, @WebParam(name = "passwordAdmin") String passwordAdmin);
    
    @WebMethod(operationName = "getServiceTemplates")
    FlowTemplatesResult getServiceTemplates(@WebParam(name = "requestId") String requestId, @WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService);
}
