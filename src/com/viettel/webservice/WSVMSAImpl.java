/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.viettel.controller.GenerateFlowRunController;
import com.viettel.model.FlowRunAction;
import com.viettel.model.Node;
import com.viettel.model.NodeRun;
import com.viettel.object.MessageException;
import com.viettel.persistence.FlowRunActionServiceImpl;
import com.viettel.util.Config;
import com.viettel.util.MessageUtil;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.viettel.model.*;
import com.viettel.model.FlowTemplates;
import com.viettel.passprotector.PassProtector;
import com.viettel.persistence.*;
import com.viettel.util.*;
import com.viettel.webservice.object.*;

import java.util.*;
import javax.annotation.Resource;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;

import com.viettel.webservice.utils.MopUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.tags.Param;
import redis.clients.jedis.Jedis;

/**
 * @author hienhv4
 */
@WebService(endpointInterface = "com.viettel.webservice.WSVMSA")
public class WSVMSAImpl implements WSVMSA {

    protected static final Logger logger = LoggerFactory.getLogger(WSVMSAImpl.class);
    public static final int RESPONSE_SUCCESS = 1;
    public static final int RESPONSE_FAIL = 0;

    @Resource
    private WebServiceContext context;

    //Quytv7 Ham sinh mop
    @Override
    public ResultDTOFlowRun createDt(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                              @WebParam(name = "workflow") String workflow, @WebParam(name = "paramValsDto") ResultParamValsDTO paramValsDto,
                              @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "username") String username,
                              @WebParam(name = "dtName") String dtName) {
        ResultDTOFlowRun result = new ResultDTOFlowRun();
        result.setResultCode(RESPONSE_FAIL);
        logger.info("start start run mop");
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }
        } catch (Exception ex) {
            result.setResultMessage("Xảy ra lỗi khi xác thực user/pass");
            logger.error(ex.getMessage(), ex);
            return result;
        }

        // Kiem tra xem workoder co dang thuc hien hay khong
        /*Jedis redis = null;
        try {
            redis = ConnectionPoolRedis.getRedis();
            if (redis.get(workflow) != null) {
                Integer numOfNodeRunning = 0;
                try {
                    numOfNodeRunning = Integer.valueOf(ConnectionPoolRedis.getRedis().get(workflow));
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                if (numOfNodeRunning > 0) {
                    result.setResultMessage("Workflow " + workflow + " đang chạy, vui lòng chạy lại sau");
                    return result;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Xảy ra lỗi khi kiểm tra xem workorder có đang thực hiện hay không");
            return result;
        } finally {
            if (redis != null) {
                try {
                    ConnectionPoolRedis.closeJedis(redis);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }*/

        // Kiem tra xem cac tham so cua mop da dc dien day du hay chua
        logger.info("Kiem tra tham so dau vao");
        try {
            if (paramValsDto.getLstParamValOfNode() == null
                    || paramValsDto.getLstParamValOfNode().isEmpty()
                    || paramValsDto.getLstParamValOfNode().get(0).getLstAllParamOfMop().isEmpty()
                    || paramValsDto.getLstParamValOfNode().get(0).getLstParamValues().isEmpty()) {
                result.setResultMessage("Không có tham số đầu vào cho mop");
                return result;
            }
            List<ParamValuesDTO> lstParamDTO = paramValsDto.getLstParamValOfNode().get(0).getLstParamValues();
            for (ParamValuesDTO p : lstParamDTO) {
                if (p.getParamValue() == null || p.getParamValue().trim().isEmpty()) {
                    result.setResultMessage("Tham số: " + p.getParamCode() + " chưa được điền");
                    return result;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.setResultMessage("Xảy ra lỗi khi kiểm tra các tham số đầu vào đã được nhập đầy đủ hay chưa");
            return result;
        }
        logger.info("Bat dau sinh mop");
        try {
            GenerateFlowRunController generateFlowRunController = new GenerateFlowRunController();
            logger.info("Bat dau lay template");
            FlowTemplates selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(flowTemplateId);
            logger.info("Ket thuc lay template");
            FlowRunAction flowRunAction = new FlowRunAction();
            List<NodeRun> nodeRuns = new ArrayList<NodeRun>();
            flowRunAction.setCreateDate(new Date());
            if (flowRunAction.getFlowRunName() != null) {
                flowRunAction.setFlowRunName(flowRunAction.getFlowRunName().trim());
            }
            flowRunAction.setFlowRunName(dtName.replace("#","_"));
           // flowRunAction.setStatus(0L);
            flowRunAction.setCrNumber(workflow);
            flowRunAction.setCreateBy(username);
            flowRunAction.setFlowTemplates(selectedFlowTemplate);
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setPortRun(8860L);
//            flowRunAction.setStatus(7L);
            flowRunAction.setStatus(0L);
            logger.info("---Chay vao generateFlowRunController---");
            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(selectedFlowTemplate);
            generateFlowRunController.setNodes(new ArrayList<Node>());
            generateFlowRunController.loadGroupAction(0l);
            logger.info("---thoat khoi generateFlowRunController---");
            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            try {
                Map<Long, List<ActionOfFlow>> mapGroupAction = new HashMap<>();
                logger.info("---get mapGroupAction---");
                for (ActionOfFlow actionOfFlow : selectedFlowTemplate.getActionOfFlows()) {
                    if (mapGroupAction.get(actionOfFlow.getGroupActionOrder()) == null) {
                        mapGroupAction.put(actionOfFlow.getGroupActionOrder(), new ArrayList<ActionOfFlow>());
                    }
                    mapGroupAction.get(actionOfFlow.getGroupActionOrder()).add(actionOfFlow);
                }
                logger.info("---get mapGroupAction thanh cong "+mapGroupAction.size()+"---");
                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
                logger.info("---save  flowRunAction"+flowRunAction+"---");
                List<ParamValue> paramValues = new ArrayList<ParamValue>();

                List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<NodeRunGroupAction>();
                if (paramValsDto.getLstParamValOfNode() != null && paramValsDto.getLstParamValOfNode().size() > 0) {
                    Node node = null;
                    for (ParamValsOfNodeDTO paramsNode : paramValsDto.getLstParamValOfNode()) {
                        HashMap<String, Object> filters = new HashMap<>();
                        filters.clear();
                        filters.put("nodeCode-EXAC", paramsNode.getNodeCode());
                        logger.info("---bat dau lay danh sach node chay"+paramsNode.getNodeCode()+"---");
                        List<Node> nodesTemp = new NodeServiceImpl().findList(filters);
                        node = nodesTemp.get(0);
                        logger.info("---ket thuc lay danh sach node chay"+node.getNodeCode()+"---");
                        if (node == null) {
                            logger.error("ERROR CANNOT GET NODE WITH ID: " + paramsNode.getNodeId());
                            continue;
                        }
                        logger.info("chay vao node :" + paramsNode.getNodeCode());
                        NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM);
//                        nodeRun.setAccount(MessageUtil.getResourceBundleConfig("account_kv1"));
//                        nodeRun.setPassword(MessageUtil.getResourceBundleConfig("password_kv1"));
                        //Quytv7_02102017_thay doi cach lay account/pass tu bang node
                        nodeRun.setAccount(node.getAccount());
                        nodeRun.setPassword(node.getPassword());
                        nodeRuns.add(nodeRun);
                        HashMap<String, String> mapParamValues = new HashMap<>();
                        List<ParamValuesDTO> lstParamDTO = paramValsDto.getLstParamValOfNode().get(0).getLstParamValues();
                        for(ParamValuesDTO paramValuesDTO : lstParamDTO ){
                            mapParamValues.put(paramValuesDTO.getParamCode(),paramValuesDTO.getParamValue());
                        }

                        paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                        logger.info("---ket thuc lay paramValues"+paramValues.size()+"---");
                        for (ParamValue paramValue : paramValues) {
                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }
                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                            ResourceBundle bundle = ResourceBundle.getBundle("cas");
                            if (bundle.getString("service").contains("10.61.127.190")) {
                                if (value == null || value.toString().isEmpty()) {
                                    value = "TEST_NOT_FOUND";
                                }
                            }
                            if (value != null) {
                                paramValue.setParamValue(value.toString());
                            }
                            paramValue.setNodeRun(nodeRun);
                            paramValue.setCreateTime(new Date());
                            paramValue.setParamValueId(null);

                           // if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
                           //     result.setResultMessage("Tham so: "+ paramValue.getParamCode()+"null");
                               // return result;
                           // }
                        }


//                        if (mapGroupAction.get(node) != null) {
                        logger.info(" co vao mapGroupAction size = " + mapGroupAction.size());
                        for (Map.Entry<Long, List<ActionOfFlow>> entry : mapGroupAction.entrySet()) {
                            NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                                    new NodeRunGroupActionId(node.getNodeId(),
                                            flowRunAction.getFlowRunId(),
                                            entry.getValue().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), entry.getValue().get(0), nodeRun);
                            nodeRunGroupActions.add(nodeRunGroupAction);
                        } // end loop for group action
                        logger.info(" thoai khoi mapGroupAction");
//                        }
                    }
                    logger.info(" xoa session");
//                    session.clear();
                    logger.info(" insert NodeRunServiceImpl ");
                    new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
                    logger.info(" insert ParamValueServiceImpl ");
                    new ParamValueServiceImpl().saveOrUpdate(paramValues, session, tx, false);

                    session.flush();
                    session.clear();
                    new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
//				session.flush();
//		 		tx.commit();

                } else {

                }
                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
                result.setFlowRunId(flowRunAction.getFlowRunId());
//                // start run mop
//                startProcess(flowRunAction, nodeRuns, workflow, username);
//
//                result.setResultCode(RESPONSE_SUCCESS);
//                result.setResultMessage("bắt đầu chạy mop " + selectedFlowTemplate.getFlowTemplateName());

            } catch (Exception e) {
                if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                    tx.rollback();
                }
                logger.error(e.getMessage(), e);
                result.setResultMessage("Xảy ra lỗi khi chạy mop " + selectedFlowTemplate.getFlowTemplateName());
                return result;
            } finally {
                if (session.isOpen()) {
                    session.close();
                }
            }
        } catch (Exception e) {
            result.setResultMessage("Xảy ra lỗi khi chạy mop");
            logger.error(e.getMessage(), e);
        }
        logger.info("Tao mop thanh cong, tra ve ket qua");
        return result;
    }
}
