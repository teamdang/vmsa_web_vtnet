/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.viettel.webservice.object.*;

import javax.faces.flow.Flow;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * @author hienhv4
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT)
public interface WSForGNOC {

    @WebMethod(operationName = "updateCrCode")
    ResultDTO updateCrCode(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                           @WebParam(name = "mopId") String mopId, @WebParam(name = "crCode") String crCode);

    @WebMethod(operationName = "updateCrCodeForMops")
    ResultDTO updateCrCodeMops(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                               @WebParam(name = "mopIds") String mopIds, @WebParam(name = "crCode") String crCode);

    @WebMethod(operationName = "updateCrStatus")
    ResultDTO updateCrStatus(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                             @WebParam(name = "crCode") String crCode, @WebParam(name = "status") String status);

    @WebMethod(operationName = "getMopByUser")
    MopOutputDTO getMopByUser(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                              @WebParam(name = "username") String username);

    @WebMethod(operationName = "getMopInfo")
    MopDetailOutputDTO getMopInfo(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                  @WebParam(name = "mopId") String mopId);

    @WebMethod(operationName = "updateCrStatusWithMops")
    ResultDTO updateCrStatusWithMops(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                     @WebParam(name = "crCode") String crCode, @WebParam(name = "status") String status,
                                     @WebParam(name = "mopIds") String mopIds);

    @WebMethod(operationName = "getListTemplateGroup")
    ResultTemplateGroupsDTO getListTemplateGroup(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService);

    @WebMethod(operationName = "getListMopByGroup")
    ResultFlowTemplatesDTO getListMopByGroup(@WebParam(name = "userService") String userService,
                                             @WebParam(name = "passService") String passService,
                                             @WebParam(name = "templateGroupId") Long templateGroupId);

    @WebMethod(operationName = "getListParamInputByCell")
    ResultParamValsDTO getListParamInputByCell(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                               @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "cellCodes") String cellCodes);

    @WebMethod(operationName = "startRunMop")
    ResultDTO startRunMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                          @WebParam(name = "workflow") String workflow, @WebParam(name = "paramValsDto") ResultParamValsDTO paramValsDto,
                          @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "username") String username,
                          @WebParam(name = "delayTime") Long delayTime);

    @WebMethod(operationName = "startRunMopSingleNode")
    ResultDTO startRunMopSingleNode(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                    @WebParam(name = "workflow") String workflow, @WebParam(name = "paramValues") String paramValues,
                                    @WebParam(name = "flowTemplateId") Long flowTemplateId, @WebParam(name = "username") String username, @WebParam(name = "nodeId") Long nodeId,
                                    @WebParam(name = "delayTime") Long delayTime);

    @WebMethod(operationName = "checkWorkOrderRunning")
    ResultDTO checkWorkOrderRunning(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                    @WebParam(name = "workOrderCode") String workOrderCode);

    @WebMethod(operationName = "getResetMopGroup")
    ResetFlowTemplatesDTO getResetMopGroup(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                           @WebParam(name = "workOrderCode") String workOrderCode);

    @WebMethod(operationName = "getParamResetMop")
    ResetParamsDTO getParamResetMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                    @WebParam(name = "mopId") Long mopId, @WebParam(name = "workOrderCode") String workOrderCode);

    @WebMethod(operationName = "runResetMop")
    ResultDTO runResetMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                          @WebParam(name = "workOrderCode") String workOrderCode, @WebParam(name = "paramValues") String paramValues,
                          @WebParam(name = "mopId") Long mopId, @WebParam(name = "username") String username);

    @WebMethod(operationName = "getFlowRunLogs")
    FlowRunsLogDTO getFlowRunLogs(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                  @WebParam(name = "workOrderCode") String workOrderCode, @WebParam(name = "username") String username);

    @WebMethod(operationName = "getLogRunDetail")
    FlowRunLogDetailDTO getLogRunDetail(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                        @WebParam(name = "flowRunLogId") Long flowRunLogId);

//    @WebMethod(operationName = "startRunSecurityMop")
//    ResultDTO startRunSecurityMop(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
//                                @WebParam(name = "paramValues") String paramValues, @WebParam(name = "flowTemplateId") Long mopId,
//                                @WebParam(name = "username") String username , @WebParam(name = "password") String password,
//                                @WebParam(name = "nodeCode") String nodeCode, @WebParam(name = "userImpact") String userImpact,
//                                @WebParam(name = "passImpact") String passImpact);

    @WebMethod(operationName = "validateFileInput")
    ResultFileValidateDTO validateFileInput(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService,
                                            @WebParam(name = "crId") Long crId, @WebParam(name = "crNumber") String crCode,
                                            @WebParam(name = "userCreate") String userCreate,
                                            @WebParam(name = "businessCode") String business,
                                            @WebParam(name = "resultFileValidateDTO") ResultFileValidateDTO resultFileValidateDTO);

    @WebMethod(operationName = "getMopInfoFromCrNumber")
    ListMopDetailOutputDTO getMopInfoFromCrNumber(@WebParam(name = "userService") String userService, @WebParam(name = "passService") String passService, @WebParam(name = "crNumber") String crNumber, @WebParam(name = "dtCreateId") Long dtCreateId);
}
