package com.viettel.webservice.object;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanhnv68 on 8/25/2017.
 */
public class FlowRunLogDetailDTO {

    private String message;
    private Long flowRunId;
    private String templateName;
    private String result;
    private List<CommandLogDTO> commandLogsDTO = new ArrayList<>();

    public Long getFlowRunId() {
        return flowRunId;
    }

    public void setFlowRunId(Long flowRunId) {
        this.flowRunId = flowRunId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public List<CommandLogDTO> getCommandLogsDTO() {
        return commandLogsDTO;
    }

    public void setCommandLogsDTO(List<CommandLogDTO> commandLogsDTO) {
        this.commandLogsDTO = commandLogsDTO;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
