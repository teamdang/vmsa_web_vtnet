/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice.object;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hienhv4
 */
public class MopDTO {
    private String mopId;
    private String mopName;
    private String templateName;
    private String createTime;
    protected static final Logger logger = LoggerFactory.getLogger(MopDTO.class);

    public String getMopId() {
        return mopId;
    }

    public void setMopId(String mopId) {
        this.mopId = mopId;
    }

    public String getMopName() {
        return mopName;
    }

    public void setMopName(String mopName) {
        this.mopName = mopName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public static void main(String args[]) {
        try {

            Gson gson = new Gson();
            gson.toJson("");

//            String val = "{\n" +
//                    "    \"client\": \"127.0.0.1\",\n" +
//                    "    \"servers\": [\n" +
//                    "        \"8.8.8.8\",\n" +
//                    "        \"8.8.4.4\",\n" +
//                    "        \"156.154.70.1\",\n" +
//                    "        \"156.154.71.1\"\n" +
//                    "    ]\n" +
//                    "}";
//            String myJSONString = "{'test': '100.00'}";
//            JsonObject jobj = new Gson().fromJson(val, JsonObject.class);

//            Type listType = new TypeToken<List<String>>() {}.getType();
//            List<String> yourList = new Gson().fromJson(jobj.get("servers"), listType);

            List<ParamValuesDTO> lstParam = new ArrayList<>();
            ParamValuesDTO p = new ParamValuesDTO();
            p.setDeclare(false);
            p.setDescription("test");
            p.setDisableByInOut(false);
            p.setFormula("");
            p.setGroupCode(null);
            p.setParamCode("ten_cell");
            p.setParamInputId(20317l);
            p.setParamLabel("test");
            p.setParamValue("TEST011");
            lstParam.add(p);
            lstParam.add(p);

            String jsonData = gson.toJson(lstParam);

            System.out.println(jsonData);

            Type listType1 = new TypeToken<List<ParamValuesDTO>>() {}.getType();
            List<ParamValuesDTO> yourList1 = new Gson().fromJson(jsonData, listType1);

            for (ParamValuesDTO s : yourList1) {
                System.out.println(s.getParamLabel());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
