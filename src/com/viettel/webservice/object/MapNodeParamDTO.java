package com.viettel.webservice.object;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by quytv7 on 7/27/2017.
 */
public class MapNodeParamDTO {
    private String nodecode;
    private String key;
    private HashMap<String, String> mapParam;

    public HashMap<String, String> getMapParam() {
        return mapParam;
    }

    public void setMapParam(HashMap<String, String> mapParam) {
        this.mapParam = mapParam;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNodecode() {

        return nodecode;
    }

    public void setNodecode(String nodecode) {
        this.nodecode = nodecode;
    }
}
