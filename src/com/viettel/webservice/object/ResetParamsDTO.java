package com.viettel.webservice.object;

import java.util.ArrayList;

/**
 * Created by hanh on 4/19/2017.
 */
public class ResetParamsDTO {

    private ArrayList<ParamValsDTO> lstParamValOfNode = new ArrayList<>();
    private String messages;

    public ResetParamsDTO() {
    }

    public ResetParamsDTO(ArrayList<ParamValsDTO> lstParamValOfNode) {
        this.lstParamValOfNode = lstParamValOfNode;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public ArrayList<ParamValsDTO> getLstParamValOfNode() {
        return lstParamValOfNode;
    }

    public void setLstParamValOfNode(ArrayList<ParamValsDTO> lstParamValOfNode) {
        this.lstParamValOfNode = lstParamValOfNode;
    }
}
