package com.viettel.webservice.object;

/**
 * Created by quytv7 on 12/29/2017.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;

/**
 * @author quytv7
 */
public class FlowRunLogCmdObject {

    private String flowRunName;
    private String crNumber;
    private String template_group_id;
    private Long flowRunType;
    private Long flowTemplateId;
    private Long flowRunId;
    private String result;
    private int resultCode = 0;
    private String resultMessage;
    private String requestId;
    private List<CommandLogObject> commandLogObjects;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getFlowRunName() {
        return flowRunName;
    }

    public void setFlowRunName(String flowRunName) {
        this.flowRunName = flowRunName;
    }

    public String getCrNumber() {
        return crNumber;
    }

    public void setCrNumber(String crNumber) {
        this.crNumber = crNumber;
    }

    public String getTemplate_group_id() {
        return template_group_id;
    }

    public void setTemplate_group_id(String template_group_id) {
        this.template_group_id = template_group_id;
    }

    public Long getFlowRunType() {
        return flowRunType;
    }

    public void setFlowRunType(Long flowRunType) {
        this.flowRunType = flowRunType;
    }

    public Long getFlowTemplateId() {
        return flowTemplateId;
    }

    public void setFlowTemplateId(Long flowTemplateId) {
        this.flowTemplateId = flowTemplateId;
    }

    public Long getFlowRunId() {
        return flowRunId;
    }

    public void setFlowRunId(Long flowRunId) {
        this.flowRunId = flowRunId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<CommandLogObject> getCommandLogObjects() {
        return commandLogObjects;
    }

    public void setCommandLogObjects(List<CommandLogObject> commandLogObjects) {
        this.commandLogObjects = commandLogObjects;
    }

}

