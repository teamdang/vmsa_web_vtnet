package com.viettel.webservice.object;

import java.util.List;

/**
 * Created by hanhnv68 on 8/25/2017.
 */
public class FlowRunsLogDTO {
    private String message;
    private List<FlowRunLogDTO> flowRunsLogDTO;

    public FlowRunsLogDTO() {
    }

    public FlowRunsLogDTO(String message, List<FlowRunLogDTO> flowRunsLogDTO) {
        this.message = message;
        this.flowRunsLogDTO = flowRunsLogDTO;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FlowRunLogDTO> getFlowRunsLogDTO() {
        return flowRunsLogDTO;
    }

    public void setFlowRunsLogDTO(List<FlowRunLogDTO> flowRunsLogDTO) {
        this.flowRunsLogDTO = flowRunsLogDTO;
    }
}
