/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice.object;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author taitd
 */
@XmlRootElement
public class ChangeParamInputDTO {
    private List<ChangeParamDTO> changeParam;

    @XmlElement
    public List<ChangeParamDTO> getChangeParam() {
        return changeParam;
    }

    public void setChangeParam(List<ChangeParamDTO> changeParam) {
        this.changeParam = changeParam;
    }
}
