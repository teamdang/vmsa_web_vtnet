package com.viettel.webservice.object;

/**
 * Created by hanh on 4/18/2017.
 */
public class TemplateGroupsDTO {
    private Long templateGroupId;
    private String templateName;

    public TemplateGroupsDTO() {
    }

    public TemplateGroupsDTO(Long templateGroupId, String templateName) {
        this.templateGroupId = templateGroupId;
        this.templateName = templateName;
    }

    public Long getTemplateGroupId() {
        return templateGroupId;
    }

    public void setTemplateGroupId(Long templateGroupId) {
        this.templateGroupId = templateGroupId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
}
