package com.viettel.webservice.object;

/**
 * Created by hanhnv68 on 8/10/2017.
 */
public class JsonNocproData {


    private String fault_name;
    private int no_of_cell;
    private String wo_trouble_code;
    private long insert_time;
    private String cabinet_name_on_node;
    private String node_code;
    private String cabinet_name;
    private String content_event;
    private long occured_time;
    private String type_station;
    private long alarm_id;
    private String cell_name;
    private int fault_id;
    private String vendor;

    public String getFault_name() {
        return fault_name;
    }

    public void setFault_name(String fault_name) {
        this.fault_name = fault_name;
    }

    public int getNo_of_cell() {
        return no_of_cell;
    }

    public void setNo_of_cell(int no_of_cell) {
        this.no_of_cell = no_of_cell;
    }

    public String getWo_trouble_code() {
        return wo_trouble_code;
    }

    public void setWo_trouble_code(String wo_trouble_code) {
        this.wo_trouble_code = wo_trouble_code;
    }

    public long getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(long insert_time) {
        this.insert_time = insert_time;
    }

    public String getCabinet_name_on_node() {
        return cabinet_name_on_node;
    }

    public void setCabinet_name_on_node(String cabinet_name_on_node) {
        this.cabinet_name_on_node = cabinet_name_on_node;
    }

    public String getNode_code() {
        return node_code;
    }

    public void setNode_code(String node_code) {
        this.node_code = node_code;
    }

    public String getCabinet_name() {
        return cabinet_name;
    }

    public void setCabinet_name(String cabinet_name) {
        this.cabinet_name = cabinet_name;
    }

    public String getContent_event() {
        return content_event;
    }

    public void setContent_event(String content_event) {
        this.content_event = content_event;
    }

    public long getOccured_time() {
        return occured_time;
    }

    public void setOccured_time(long occured_time) {
        this.occured_time = occured_time;
    }

    public String getType_station() {
        return type_station;
    }

    public void setType_station(String type_station) {
        this.type_station = type_station;
    }

    public long getAlarm_id() {
        return alarm_id;
    }

    public void setAlarm_id(long alarm_id) {
        this.alarm_id = alarm_id;
    }

    public String getCell_name() {
        return cell_name;
    }

    public void setCell_name(String cell_name) {
        this.cell_name = cell_name;
    }

    public int getFault_id() {
        return fault_id;
    }

    public void setFault_id(int fault_id) {
        this.fault_id = fault_id;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
}
