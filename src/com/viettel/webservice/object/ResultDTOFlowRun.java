/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice.object;

import java.util.HashMap;

/**
 *
 * @author hienhv4
 */
public class ResultDTOFlowRun {

    private int resultCode;
    private String resultMessage;
    private Long flowRunId;

    public Long getFlowRunId() {
        return flowRunId;
    }

    public void setFlowRunId(Long flowRunId) {
        this.flowRunId = flowRunId;
    }

    public ResultDTOFlowRun() {
    }

    public ResultDTOFlowRun(int resultCode, String resultMessage) {
        this.resultCode = resultCode;

        this.resultMessage = resultMessage;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
}
