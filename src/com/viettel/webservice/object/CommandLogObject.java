package com.viettel.webservice.object;

import java.util.Date;

/**
 * Created by quytv7 on 12/29/2017.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class CommandLogObject {

    private Long commandLogActionId;
    private Long runLogActionId;
    private Long nodeId;
    private Long commandDetailId;
    private Long actionCommandId;
    private String cmdRun;
    private String result;
    private String resultDetail;
    //    private boolean resultFinal;
    private Long resultFinal;
    private Date startTime;
    private Date finishTime;
    private Long orderRun;

    private int cloneNumber;

    public int getCloneNumber() {
        return cloneNumber;
    }

    public void setCloneNumber(int cloneNumber) {
        this.cloneNumber = cloneNumber;
    }

    public Long getOrderRun() {
        return orderRun;
    }

    public void setOrderRun(Long orderRun) {
        this.orderRun = orderRun;
    }

    public Long getCommandLogActionId() {
        return commandLogActionId;
    }

    public void setCommandLogActionId(Long commandLogActionId) {
        this.commandLogActionId = commandLogActionId;
    }

    public Long getRunLogActionId() {
        return runLogActionId;
    }

    public void setRunLogActionId(Long runLogActionId) {
        this.runLogActionId = runLogActionId;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getCommandDetailId() {
        return commandDetailId;
    }

    public void setCommandDetailId(Long commandDetailId) {
        this.commandDetailId = commandDetailId;
    }

    public Long getActionCommandId() {
        return actionCommandId;
    }

    public void setActionCommandId(Long actionCommandId) {
        this.actionCommandId = actionCommandId;
    }

    public String getCmdRun() {
        return cmdRun;
    }

    public void setCmdRun(String cmdRun) {
        this.cmdRun = cmdRun;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResultDetail() {
        return resultDetail;
    }

    public void setResultDetail(String resultDetail) {
        this.resultDetail = resultDetail;
    }

    //    public boolean isResultFinal() {
//        return resultFinal;
//    }
//
//    public void setResultFinal(boolean resultFinal) {
//        this.resultFinal = resultFinal;
//    }
    public Long getResultFinal() {
        return resultFinal;
    }

    public void setResultFinal(Long resultFinal) {
        this.resultFinal = resultFinal;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }
}

