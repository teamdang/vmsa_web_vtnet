package com.viettel.webservice.object;

/**
 * Created by hanhnv68 on 8/25/2017.
 */
public class CommandLogDTO {

    private String actionName;
    private Long orderRun;
    private String cmd;
    private byte[] logsCmd;
    private String result;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public Long getOrderRun() {
        return orderRun;
    }

    public void setOrderRun(Long orderRun) {
        this.orderRun = orderRun;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public byte[] getLogsCmd() {
        return logsCmd;
    }

    public void setLogsCmd(byte[] logsCmd) {
        this.logsCmd = logsCmd;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
