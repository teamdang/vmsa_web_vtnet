package com.viettel.webservice.object;

import java.util.ArrayList;

/**
 * Created by hanh on 4/19/2017.
 */
public class ResultParamValsDTO {

    private ArrayList<ParamValsOfNodeDTO> lstParamValOfNode = new ArrayList<>();
    private String messages;

    public ResultParamValsDTO() {
    }

    public ResultParamValsDTO(ArrayList<ParamValsOfNodeDTO> lstParamValOfNode) {
        this.lstParamValOfNode = lstParamValOfNode;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public ArrayList<ParamValsOfNodeDTO> getLstParamValOfNode() {
        return lstParamValOfNode;
    }

    public void setLstParamValOfNode(ArrayList<ParamValsOfNodeDTO> lstParamValOfNode) {
        this.lstParamValOfNode = lstParamValOfNode;
    }
}
