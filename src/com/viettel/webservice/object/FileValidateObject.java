package com.viettel.webservice.object;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by quytv7 on 11/9/2017.
 */
public class FileValidateObject {
    private String fileName;
    private String fileContent;
    private String fileType;
    private int resultCode;
    private LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData;
    private String fileNameConfig;
    private HashMap<String, List<?>> mapCellObjectImports;
    private File fileOutPut;

    public File getFileOutPut() {
        return fileOutPut;
    }

    public void setFileOutPut(File fileOutPut) {
        this.fileOutPut = fileOutPut;
    }

    public LinkedHashMap<String, List<LinkedHashMap<String, String>>> getMapData() {
        return mapData;
    }

    public void setMapData(LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) {
        this.mapData = mapData;
    }

    public String getFileNameConfig() {
        return fileNameConfig;
    }

    public void setFileNameConfig(String fileNameConfig) {
        this.fileNameConfig = fileNameConfig;
    }

    public HashMap<String, List<?>> getMapCellObjectImports() {
        return mapCellObjectImports;
    }

    public void setMapCellObjectImports(HashMap<String, List<?>> mapCellObjectImports) {
        this.mapCellObjectImports = mapCellObjectImports;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
