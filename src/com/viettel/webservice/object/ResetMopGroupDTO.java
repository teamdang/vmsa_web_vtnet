package com.viettel.webservice.object;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanhnv68 on 8/16/2017.
 */
public class ResetMopGroupDTO {
    private Integer resetType;
    private List<ResetMopDTO> resetMops = new ArrayList<>();

    public Integer getResetType() {
        return resetType;
    }

    public void setResetType(Integer resetType) {
        this.resetType = resetType;
    }

    public List<ResetMopDTO> getResetMops() {
        return resetMops;
    }

    public void setResetMops(List<ResetMopDTO> resetMops) {
        this.resetMops = resetMops;
    }
}
