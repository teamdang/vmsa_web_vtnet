package com.viettel.webservice.object;

import java.util.HashMap;

/**
 * Created by quytv7 on 11/28/2017.
 */
public class DataCellUpdateNimsForGnoc {
    private String crId;
    private String crNumber;
    private HashMap<String, String> mapDataCell;
    private String classNimsWS;
    private String functionNimsWs;
    private Long isVaildateNims;
    private Long isUpdateNims;

    public Long getIsVaildateNims() {
        return isVaildateNims;
    }

    public void setIsVaildateNims(Long isVaildateNims) {
        this.isVaildateNims = isVaildateNims;
    }

    public Long getIsUpdateNims() {
        return isUpdateNims;
    }

    public void setIsUpdateNims(Long isUpdateNims) {
        this.isUpdateNims = isUpdateNims;
    }

    public String getCrId() {
        return crId;
    }

    public void setCrId(String crId) {
        this.crId = crId;
    }

    public String getCrNumber() {
        return crNumber;
    }

    public void setCrNumber(String crNumber) {
        this.crNumber = crNumber;
    }

    public HashMap<String, String> getMapDataCell() {
        return mapDataCell;
    }

    public void setMapDataCell(HashMap<String, String> mapDataCell) {
        this.mapDataCell = mapDataCell;
    }

    public String getClassNimsWS() {
        return classNimsWS;
    }

    public void setClassNimsWS(String classNimsWS) {
        this.classNimsWS = classNimsWS;
    }

    public String getFunctionNimsWs() {
        return functionNimsWs;
    }

    public void setFunctionNimsWs(String functionNimsWs) {
        this.functionNimsWs = functionNimsWs;
    }
}
