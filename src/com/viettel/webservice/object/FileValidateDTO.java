package com.viettel.webservice.object;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by quytv7 on 11/9/2017.
 */
public class FileValidateDTO {
    private String fileName;
    private String fileContent;
    private String fileType;
    private int resultCode;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
