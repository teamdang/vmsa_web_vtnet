package com.viettel.webservice.object;

import java.util.ArrayList;

/**
 * Created by hanh on 4/20/2017.
 */
public class ResultTemplateGroupsDTO {

    private String message;
    private ArrayList<TemplateGroupsDTO> lstTemplateGroup = new ArrayList<>();

    public ResultTemplateGroupsDTO() {
    }

    public ResultTemplateGroupsDTO(ArrayList<TemplateGroupsDTO> lstTemplateGroup) {
        this.lstTemplateGroup = lstTemplateGroup;
    }

    public ArrayList<TemplateGroupsDTO> getLstTemplateGroup() {
        return lstTemplateGroup;
    }

    public void setLstTemplateGroup(ArrayList<TemplateGroupsDTO> lstTemplateGroup) {
        this.lstTemplateGroup = lstTemplateGroup;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
