package com.viettel.webservice.object;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hanh on 4/18/2017.
 */
public class ParamValsOfNodeDTO {
    private String message;
    private Long nodeId;
    private String nodeCode;
    private ArrayList<ParamValuesDTO> lstParamValues = new ArrayList<>();
    private ArrayList<ParamValuesDTO> lstAllParamOfMop = new ArrayList<>();

    public ParamValsOfNodeDTO() {
    }

    public ParamValsOfNodeDTO(String message, ArrayList<ParamValuesDTO> lstParamValues, ArrayList<ParamValuesDTO> lstAllParamOfMop) {
        this.message = message;
        this.lstParamValues = lstParamValues;
        this.lstAllParamOfMop = lstAllParamOfMop;
    }

    public ParamValsOfNodeDTO(String message, Long nodeId, ArrayList<ParamValuesDTO> lstParamValues,
                              ArrayList<ParamValuesDTO> lstAllParamOfMop) {
        this.message = message;
        this.nodeId = nodeId;
        this.lstParamValues = lstParamValues;
        this.lstAllParamOfMop = lstAllParamOfMop;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ParamValuesDTO> getLstParamValues() {
        return lstParamValues;
    }

    public void setLstParamValues(ArrayList<ParamValuesDTO> lstParamValues) {
        this.lstParamValues = lstParamValues;
    }

    public ArrayList<ParamValuesDTO> getLstAllParamOfMop() {
        return lstAllParamOfMop;
    }

    public void setLstAllParamOfMop(ArrayList<ParamValuesDTO> lstAllParamOfMop) {
        this.lstAllParamOfMop = lstAllParamOfMop;
    }

    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

//    public static void main(String args[]) {
//        try {
//            String stringToSearch = "Location info  =  Cabinet No.=0, Subrack No.=61, Slot No.=0, Port \n" +
//                    "                         No.=Port 0(West), Board Type=MRRU, Specific \n" +
//                    "                         Problem=Reception Power too Low, Site No.=200, Site \n" +
//                    "                         Type=DBS3900 GSM, Site Name=IHC887";
//
//            Pattern p = Pattern.compile("Subrack No.=(\\d+).*");   // the pattern to search for
//            Matcher m = p.matcher(stringToSearch);
//
//            // if we find a match, get the group
//            if (m.find())
//            {
//                // we're only looking for one group, so get it
//                String theGroup = m.group(1);
//
//                // print the group out for verification
//                System.out.format("'%s'\n", theGroup);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
