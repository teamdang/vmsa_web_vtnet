package com.viettel.webservice.object;

import java.util.List;

/**
 * Created by quytv7 on 11/9/2017.
 */
public class ResultFileValidateDTO {
    private int resultCode;
    private String resultMessage;
    private String crNumber;
    private Long crId;
    private List<FileValidateDTO> fileValidateDTOs;
    private Long dtCreateId;


    public Long getDtCreateId() {
        return dtCreateId;
    }

    public void setDtCreateId(Long dtCreateId) {
        this.dtCreateId = dtCreateId;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getCrNumber() {
        return crNumber;
    }

    public void setCrNumber(String crNumber) {
        this.crNumber = crNumber;
    }

    public Long getCrId() {
        return crId;
    }

    public void setCrId(Long crId) {
        this.crId = crId;
    }

    public List<FileValidateDTO> getFileValidateDTOs() {
        return fileValidateDTOs;
    }

    public void setFileValidateDTOs(List<FileValidateDTO> fileValidateDTOs) {
        this.fileValidateDTOs = fileValidateDTOs;
    }
}
