/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.viettel.webservice.object.ChangeParamInputDTO;
import com.viettel.webservice.object.ResultDTO;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 *
 * @author dunglv
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface WSForNCMS {
    @WebMethod(operationName = "updateChangeParam")
    ResultDTO updateChangeParam(@WebParam(name = "changeParams") ChangeParamInputDTO changeParam);
}
