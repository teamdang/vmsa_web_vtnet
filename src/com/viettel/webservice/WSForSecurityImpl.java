/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.viettel.controller.GenerateFlowRunController;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.*;
import com.viettel.model.FlowTemplates;
import com.viettel.object.AccountObj;
import com.viettel.object.MessageException;
import com.viettel.object.MessageObject;
import com.viettel.passprotector.PassProtector;
import com.viettel.persistence.DaoSimpleService;
import com.viettel.persistence.FlowRunActionServiceImpl;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.MapProcessCountryServiceImpl;
import com.viettel.persistence.NodeRunGroupActionServiceImpl;
import com.viettel.persistence.NodeRunServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.ParamInOutServiceImpl;
import com.viettel.persistence.ParamValueServiceImpl;
import com.viettel.persistence.ServiceTemplateMappingServiceImpl;
import com.viettel.util.Config;
import com.viettel.util.MessageUtil;
import com.viettel.util.SocketClient;
import com.viettel.webservice.object.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hohien
 */
@WebService(endpointInterface = "com.viettel.webservice.WSForSecurity")
public class WSForSecurityImpl implements WSForSecurity {

    protected static final Logger LOGGER = LoggerFactory.getLogger(WSForGNOCImpl.class);
    public static final int RESPONSE_SUCCESS = 1;
    public static final int RESPONSE_FAIL = 0;
    private static final String NE_ACCOUNT_CHANGE_ROLE = "NE_ACCOUNT_CHANGE_ROLE";
    private static final String NE_ACCOUNT_CREATE = "NE_ACCOUNT_CREATE";
    private static final String NE_ACCOUNT_DELETE = "NE_ACCOUNT_DELETE";
    private static final String NE_ACCOUNT_EXTEND_DATE = "NE_ACCOUNT_EXTEND_DATE";
    private static final String NE_ACCOUNT_LOCK = "NE_ACCOUNT_LOCK";
    private static final String NE_ACCOUNT_UNLOCK = "NE_ACCOUNT_UNLOCK";
    private static final String NE_ACCOUNT_VIEW = "NE_ACCOUNT_VIEW";
    private static final String NE_ACCOUNT_GRANT_ROLE = "NE_ACCOUNT_GRANT_ROLE";
    private static final String NE_ACCOUNT_REVOKE_ROLE = "NE_ACCOUNT_REVOKE_ROLE";

    @Resource
    private WebServiceContext context;

    @Override
    public ResultCreateMop changeEffectiveDateAccount(String requestId, String userService,
            String passService, String crNumber, String accountName, String nodeCode,
            String accountEffectiveDate, String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE", accountEffectiveDate));

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            if (accountEffectiveDate != null) {
                Calendar effDate = Calendar.getInstance();
                effDate.setTime(sdf.parse(accountEffectiveDate));

                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_date", standardValue(effDate.get(Calendar.DATE))));
                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_month", standardValue(effDate.get(Calendar.MONTH + 1))));
                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_year", standardValue(effDate.get(Calendar.YEAR))));

                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_day_from_now", String.valueOf(countDays(effDate.getTime(), new Date()))));
            }

            return saveDT(requestId, crNumber, "changeEffectiveDateAccount", nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    private static int countDays(Date fromDate, Date toDate) {
        long delta = toDate.getTime() - fromDate.getTime();
        return (int) Math.ceil(delta / (1000.0 * 60 * 60 * 24));
    }

    private String standardValue(int value) {
        return (value >= 0 && value < 10) ? "0" + value : String.valueOf(value);
    }

    @Override
    public ResultCreateMop changeExpireDateAccount(String requestId, String userService, String passService,
            String crNumber, String accountName, String nodeCode, String accountExpireDate,
            String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE", accountExpireDate));

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            if (accountExpireDate != null) {
                Calendar expDate = Calendar.getInstance();
                expDate.setTime(sdf.parse(accountExpireDate));

                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_date", standardValue(expDate.get(Calendar.DATE))));
                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_month", standardValue(expDate.get(Calendar.MONTH + 1))));
                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_year", standardValue(expDate.get(Calendar.YEAR))));

                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_day_from_now", String.valueOf(countDays(expDate.getTime(), new Date()))));
            }

            return saveDT(requestId, crNumber, "changeExpireDateAccount", nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop changePasswordAccount(String requestId, String userService, String passService,
            String crNumber, String accountName, String nodeCode, String passwordNew, String accountAdmin,
            String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("passwordNew", passwordNew));

            return saveDT(requestId, crNumber, "changePasswordAccount", nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop changePasswordAccountNormal(String requestId, String userService,
            String passService, String crNumber, String accountName, String nodeCode, String passwordOld,
            String passwordNew) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("passwordOld", passwordOld));
            prInputDTO.getParams().add(new ParamDTO("passwordNew", passwordNew));

            return saveDT(requestId, crNumber, "changePasswordAccountNormal", nodeCode, null, null, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop changeRoleAccount(String requestId, String userService, String passService,
            String crNumber, String accountName, String nodeCode, String normalizeRole, String accountAdmin,
            String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("NORMALIZE_ROLE", normalizeRole));

            return saveDT(requestId, crNumber, NE_ACCOUNT_CHANGE_ROLE, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop deleteAccount(String requestId, String userService, String passService,
            String crNumber, String accountName, String nodeCode, String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));

            return saveDT(requestId, crNumber, NE_ACCOUNT_DELETE, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop createAccount(String requestId, String userService, String passService,
            String crNumber, String accountName, String password, String nodeCode, String normalizeRole,
            String accountExpireDate, String accountEffectiveDate, String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("PASSWORD", password));
            prInputDTO.getParams().add(new ParamDTO("NORMALIZE_ROLE", normalizeRole));
            prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE", accountExpireDate));
            prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE", accountEffectiveDate));

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            if (accountEffectiveDate != null) {
                Calendar effDate = Calendar.getInstance();
                effDate.setTime(sdf.parse(accountEffectiveDate));

                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_date", standardValue(effDate.get(Calendar.DATE))));
                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_month", standardValue(effDate.get(Calendar.MONTH + 1))));
                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_year", standardValue(effDate.get(Calendar.YEAR))));

                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_day_from_now", String.valueOf(countDays(effDate.getTime(), new Date()))));
            }

            if (accountExpireDate != null) {
                Calendar expDate = Calendar.getInstance();
                expDate.setTime(sdf.parse(accountExpireDate));

                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_date", standardValue(expDate.get(Calendar.DATE))));
                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_month", standardValue(expDate.get(Calendar.MONTH + 1))));
                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_year", standardValue(expDate.get(Calendar.YEAR))));

                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_day_from_now", String.valueOf(countDays(expDate.getTime(), new Date()))));
            }

            return saveDT(requestId, crNumber, NE_ACCOUNT_CREATE, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop grantRoleAccount(String requestId, String userService, String passService,
            String crNumber, String accountName, String nodeCode, String normalizeRole, String accountAdmin,
            String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("NORMALIZE_ROLE", normalizeRole));

            return saveDT(requestId, crNumber, NE_ACCOUNT_GRANT_ROLE, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop revokeRoleAccount(String requestId, String userService, String passService,
            String crNumber, String accountName, String nodeCode, String normalizeRole, String accountAdmin,
            String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("NORMALIZE_ROLE", normalizeRole));

            return saveDT(requestId, crNumber, NE_ACCOUNT_REVOKE_ROLE, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public FlowRunLogCmdObject viewAccount(String requestId, String userService, String passService,
                                         String crNumber, String accountName, String nodeCode, String accountAdmin, String passwordAdmin) {
        FlowRunLogCmdObject result = new FlowRunLogCmdObject();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));

            result = saveDTViewAccount(requestId, crNumber, NE_ACCOUNT_VIEW, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }

        return result;
    }

    @Override
    public FlowTemplatesResult getServiceTemplates(String requestId, String userService, String passService) {
        FlowTemplatesResult result = new FlowTemplatesResult();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ArrayList<FlowTemplateObj> lsTemplate = new ArrayList<>();
            StringBuilder hql = new StringBuilder();
            //hql.append(" select new FlowTemplates(fts, stmp) from FlowTemplates as fts left join ServiceTemplateMapping as stmp on fts.flowTemplatesId = stmp.templateId");
            hql.append(" select fts from FlowTemplates as fts where fts.status = 9 order by fts.flowTemplateName");
            List<FlowTemplates> lstTemplate = new FlowTemplatesServiceImpl().findList(hql.toString(), -1, -1);
            if (lstTemplate != null) {

                List<ServiceTemplateMapping> mapping = new ServiceTemplateMappingServiceImpl().findList();
                Map<Long, String> mapService = new HashMap<>();
                if (mapping != null) {
                    for (ServiceTemplateMapping obj : mapping) {
                        mapService.put(obj.getTemplate().getFlowTemplatesId(), obj.getServiceCode());
                    }
                }

                for (FlowTemplates template : lstTemplate) {
                    lsTemplate.add(new FlowTemplateObj(template.getTemplateGroup() == null ? null : template.getTemplateGroup().getId(),
                            template.getFlowTemplatesId(), template.getFlowTemplateName(),
                            template.getFlowTemplateName(), mapService.get(template.getFlowTemplatesId())));
                }
            }
            result.setLstFlowTemplate(lsTemplate);
            result.setResultCode(0);
            result.setResultMessage("Success");
            LOGGER.info("List template size: " + lsTemplate.size());
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }

        return result;
    }

    @Override
    public ResultCreateMop executeService(String requestId, String userService, String passService,
            String crNumber, String serviceCode, String nodeCode, ParamInputDTO params, String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            if (params != null && params.getParams() != null) {
                params.getParams().add(new ParamDTO("crNumber", crNumber));
                params.getParams().add(new ParamDTO("nodeCode", nodeCode));
                params.getParams().add(new ParamDTO("serviceCode", serviceCode));
                
                String effectiveDate = null, expirDate = null;
                
                for (ParamDTO pr : params.getParams()) {
                    if ("EFFECTIVE_DATE".equalsIgnoreCase(pr.getParamCode().trim())) {
                        effectiveDate = pr.getParamValue();
                    } else if ("EXPIRATION_DATE".equalsIgnoreCase(pr.getParamCode().trim())) {
                        expirDate = pr.getParamValue();
                    }
                }
                
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                if (effectiveDate != null) {
                    Calendar effDate = Calendar.getInstance();
                    effDate.setTime(sdf.parse(effectiveDate));

                    params.getParams().add(new ParamDTO("EFFECTIVE_DATE_date", standardValue(effDate.get(Calendar.DATE))));
                    params.getParams().add(new ParamDTO("EFFECTIVE_DATE_month", standardValue(effDate.get(Calendar.MONTH + 1))));
                    params.getParams().add(new ParamDTO("EFFECTIVE_DATE_year", standardValue(effDate.get(Calendar.YEAR))));

                    params.getParams().add(new ParamDTO("EFFECTIVE_DATE_day_from_now", String.valueOf(countDays(effDate.getTime(), new Date()))));
                }

                if (expirDate != null) {
                    Calendar expDate = Calendar.getInstance();
                    expDate.setTime(sdf.parse(expirDate));

                    params.getParams().add(new ParamDTO("EXPIRATION_DATE_date", standardValue(expDate.get(Calendar.DATE))));
                    params.getParams().add(new ParamDTO("EXPIRATION_DATE_month", standardValue(expDate.get(Calendar.MONTH + 1))));
                    params.getParams().add(new ParamDTO("EXPIRATION_DATE_year", standardValue(expDate.get(Calendar.YEAR))));

                    params.getParams().add(new ParamDTO("EXPIRATION_DATE_day_from_now", String.valueOf(countDays(expDate.getTime(), new Date()))));
                }
            }
            
            return saveDT(requestId, crNumber, serviceCode, nodeCode, accountAdmin, passwordAdmin, params);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop resetAccount(String requestId, String userService, String passService, String crNumber, String accountName, String password, String nodeCode, String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));

            result = saveDT(requestId, crNumber, NE_ACCOUNT_UNLOCK, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }

        return result;
    }

    public ResultCreateMop saveDT(String requestId, String crNumber, String serviceCode, String nodeCode,
            String account, String password, ParamInputDTO paramInputDTO) {

        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {

            if (account == null || account.trim().isEmpty() || password == null || password.trim().isEmpty()) {
                result.setResultCode(10);
                result.setResultMessage("Account hoặc password Admin đang để trống");
                return result;
            }

            if (serviceCode == null || serviceCode.trim().isEmpty()) {
                result.setResultCode(3);
                result.setResultMessage("Service Code không tồn tại");
                return result;
            }

            Map<String, Object> filters = new HashMap<>();
            filters.put("nodeStandardCode", nodeCode);
            List<Node> nodes = new NodeServiceImpl().findList(filters);
            if (nodes == null || nodes.isEmpty()) {
                result.setResultCode(5);
                result.setResultMessage("NodeCode không tồn tại");
                return result;
            }
            Node node = nodes.get(0);

            ServiceTemplateMappingServiceImpl serviceTempService = new ServiceTemplateMappingServiceImpl();
            Map<String, Object> filters1 = new HashMap<>();
            filters1.put("serviceCode", serviceCode);
            List<ServiceTemplateMapping> serviceMappings = serviceTempService.findList(filters1);

            ServiceTemplateMapping serviceMapping = null;
            if (serviceMappings != null && !serviceMappings.isEmpty()) {
                for (ServiceTemplateMapping mapping : serviceMappings) {
                    if (mapping.getVendor() == null || mapping.getVersion() == null || mapping.getNodeType() == null) {
                        continue;
                    }
                    if (node.getVendor().getVendorId().equals(mapping.getVendor().getVendorId())
                            && node.getNodeType().getTypeId().equals(mapping.getNodeType().getTypeId())
                            && node.getVersion().getVersionId().equals(mapping.getVersion().getVersionId())) {
                        serviceMapping = mapping;
                        break;
                    }
                }

                if (serviceMapping == null) {
                    for (ServiceTemplateMapping mapping : serviceMappings) {
                        if (mapping.getVendor() == null || mapping.getNodeType() == null) {
                            continue;
                        }
                        if (node.getVendor().getVendorId().equals(mapping.getVendor().getVendorId())
                                && node.getNodeType().getTypeId().equals(mapping.getNodeType().getTypeId())
                                && mapping.getVersion() == null) {
                            serviceMapping = mapping;
                            break;
                        }
                    }
                }

                if (serviceMapping == null) {
                    for (ServiceTemplateMapping mapping : serviceMappings) {
                        if (mapping.getVendor() == null) {
                            continue;
                        }
                        if (node.getVendor().getVendorId().equals(mapping.getVendor().getVendorId())
                                && mapping.getNodeType() == null && mapping.getVersion() == null) {
                            serviceMapping = mapping;
                            break;
                        }
                    }
                }
            }

            if (serviceMapping == null) {
                result.setResultCode(3);
                result.setResultMessage("Service Code không tồn tại");
                return result;
            }

            FlowTemplates flowTemplate = serviceMapping.getTemplate();
            if (flowTemplate == null) {
                result.setResultCode(3);
                result.setResultMessage("Template không tồn tại");
                return result;
            } else if (flowTemplate.getStatus() != 9) {
                result.setResultCode(4);
                result.setResultMessage("Template chưa được phê duyệt");
                return result;
            }

            if (nodeCode == null || nodeCode.trim().isEmpty()) {
                result.setResultCode(5);
                result.setResultMessage("NodeCode không tồn tại");
                return result;
            }

            GenerateFlowRunController generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();

            if (crNumber == null || crNumber.trim().isEmpty()) {
                flowRunAction.setCrNumber("CR_AUTO");
            } else {
                flowRunAction.setCrNumber(crNumber);
            }
            flowRunAction.setFlowRunName(serviceCode + "_" + nodeCode + "_" + new SimpleDateFormat("yyMMdd_HHmmss").format(new Date()));
            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setCreateDate(new Date());
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplate);
            flowRunAction.setCreateBy("SYSTEM");
            flowRunAction.setMopType(0l);
            flowRunAction.setRequestId(requestId);

            Map<String, Object> _filters = new HashMap<>();
            _filters.put("countryCode.countryCode-" + MapProcessCountryServiceImpl.EXAC, "VNM_SECURITY");
            _filters.put("status", 1l);

            List<MapProcessCountry> maps = new MapProcessCountryServiceImpl().findList(_filters);

            if (maps != null && !maps.isEmpty()) {
                //Sap xep lai maps theo thu tu random
                Collections.shuffle(maps);

                MapProcessCountry obj = maps.get(0);
                flowRunAction.setPortRun(obj.getProcessPort().longValue());
                flowRunAction.setIpRun(obj.getProcessIp());
                flowRunAction.setStatus(7L);
            } else {
                flowRunAction.setStatus(0L);
            }

            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplate);
            generateFlowRunController.setNodes(new ArrayList<Node>());
            generateFlowRunController.getNodes().add(node);
            generateFlowRunController.loadGroupAction(0l);

            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            try {
                Map<Long, List<ActionOfFlow>> mapGroupAction = new HashMap<>();
                LOGGER.info("---get mapGroupAction---");
                for (ActionOfFlow actionOfFlow : flowTemplate.getActionOfFlows()) {
                    if (mapGroupAction.get(actionOfFlow.getGroupActionOrder()) == null) {
                        mapGroupAction.put(actionOfFlow.getGroupActionOrder(), new ArrayList<ActionOfFlow>());
                    }
                    mapGroupAction.get(actionOfFlow.getGroupActionOrder()).add(actionOfFlow);
                }
                LOGGER.info("---get mapGroupAction thanh cong " + mapGroupAction.size() + "---");
                
                Map<String, String> params = new HashMap<>();
                Map<String, String> paramToSave = new HashMap<>();
                if (paramInputDTO.getParams() != null && !paramInputDTO.getParams().isEmpty()) {
                    for (ParamDTO pr : paramInputDTO.getParams()) {
                        params.put(pr.getParamCode(), pr.getParamValue());
                        
                        if (pr.getParamCode() != null && pr.getParamCode().toUpperCase().contains("PASSWORD")) {
                            paramToSave.put(pr.getParamCode(), "******");
                        } else {
                            paramToSave.put(pr.getParamCode(), pr.getParamValue());
                        }
                    }
                }
                flowRunAction.setSecurityParamInput(new Gson().toJson(paramToSave));

                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
                LOGGER.info("---save  flowRunAction" + flowRunAction + "---");

                List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<>();
                Map<String, String> paramLower = new HashMap<>();
                for (String key : params.keySet()) {
                    if (key == null) {
                        continue;
                    }
                    paramLower.put(key.trim().toLowerCase(), params.get(key));
                }

                List<NodeRun> nodeRuns = new ArrayList<>();
                NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM);
                nodeRun.setAccount(PassProtector.encrypt(account, "ipchange"));
                nodeRun.setPassword(PassProtector.encrypt(password, "ipchange"));
                nodeRuns.add(nodeRun);

                List<Long> actionOfFlowIds = new ArrayList<>();
                for (ActionOfFlow action : flowTemplate.getActionOfFlows()) {
                    actionOfFlowIds.add(action.getStepNum());
                }

                Map<String, Collection<?>> map = new HashMap<>();
                map.put("actionFlowIds", actionOfFlowIds);
                List<ParamInOut> paramInOuts = new ArrayList<>();
                if (!actionOfFlowIds.isEmpty()) {
                    paramInOuts = new ParamInOutServiceImpl().findListWithIn("from ParamInOut where actionOfFlowByActionFlowInId.stepNum in (:actionFlowIds)", -1, -1, map);
                }

                List<String> pCodeInOuts = new ArrayList<>();
                for (ParamInOut pInOut : paramInOuts) {
                    pCodeInOuts.add(pInOut.getParamInput().getParamCode());
                }

                List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                LOGGER.info("---ket thuc lay paramValues" + paramValues.size() + "---");

                Iterator<ParamValue> iter = paramValues.iterator();
                while (iter.hasNext()) {
                    ParamValue paramValue = iter.next();
                    if (paramValue.getParamInput().getReadOnly() || pCodeInOuts.contains(paramValue.getParamCode())) {
                        iter.remove();
                        continue;
                    }
                    Object value = null;
                    try {
                        value = paramLower.get(paramValue.getParamCode().toLowerCase().trim());
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    ResourceBundle bundle = ResourceBundle.getBundle("cas");
                    if (bundle.getString("service").contains("10.61.127.190")) {
                        if (value == null || value.toString().isEmpty()) {
                            value = "TEST_NOT_FOUND";
                        }
                    }
                    if (value == null || value.toString().trim().isEmpty()) {
                        LOGGER.info(paramToSave.toString());
                        result.setResultCode(6);
                        result.setResultMessage("Tham số: " + paramValue.getParamCode() + " chưa được điền");
                        return result;
                    } else {
                        paramValue.setParamValue(value.toString());
                    }
                    paramValue.setNodeRun(nodeRun);
                    paramValue.setCreateTime(new Date());
                    paramValue.setParamValueId(null);
                }

                LOGGER.info(" co vao mapGroupAction size = " + mapGroupAction.size());
                for (Map.Entry<Long, List<ActionOfFlow>> entry : mapGroupAction.entrySet()) {
                    NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                            new NodeRunGroupActionId(node.getNodeId(),
                                    flowRunAction.getFlowRunId(),
                                    entry.getValue().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), entry.getValue().get(0), nodeRun);
                    nodeRunGroupActions.add(nodeRunGroupAction);
                }
                LOGGER.info(" insert NodeRunServiceImpl ");
                new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);

                LOGGER.info(" insert ParamValueServiceImpl ");
                new ParamValueServiceImpl().saveOrUpdate(paramValues, session, tx, false);

                new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);

                result.setFlowRunId(flowRunAction.getFlowRunId());
            } catch (AppException | SysException | HibernateException e) {
                if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                    tx.rollback();
                }
                throw e;
            } finally {
                if (session.isOpen()) {
                    session.close();
                }
            }
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }

        return result;
    }
    public FlowRunLogCmdObject saveDTViewAccount(String requestId, String crNumber, String serviceCode, String nodeCode,
                                      String account, String password, ParamInputDTO paramInputDTO) {

        FlowRunLogCmdObject result = new FlowRunLogCmdObject();
        result.setRequestId(requestId);
        try {

            if (account == null || account.trim().isEmpty() || password == null || password.trim().isEmpty()) {
                result.setResultCode(10);
                result.setResultMessage("Account hoặc password Admin đang để trống");
                return result;
            }

            if (serviceCode == null || serviceCode.trim().isEmpty()) {
                result.setResultCode(3);
                result.setResultMessage("Service Code không tồn tại");
                return result;
            }

            Map<String, Object> filters = new HashMap<>();
            filters.put("nodeStandardCode", nodeCode);
            List<Node> nodes = new NodeServiceImpl().findList(filters);
            if (nodes == null || nodes.isEmpty()) {
                result.setResultCode(5);
                result.setResultMessage("NodeCode không tồn tại");
                return result;
            }
            Node node = nodes.get(0);

            ServiceTemplateMappingServiceImpl serviceTempService = new ServiceTemplateMappingServiceImpl();
            Map<String, Object> filters1 = new HashMap<>();
            filters1.put("serviceCode", serviceCode);
            List<ServiceTemplateMapping> serviceMappings = serviceTempService.findList(filters1);

            ServiceTemplateMapping serviceMapping = null;
            if (serviceMappings != null && !serviceMappings.isEmpty()) {
                for (ServiceTemplateMapping mapping : serviceMappings) {
                    if (mapping.getVendor() == null || mapping.getVersion() == null || mapping.getNodeType() == null) {
                        continue;
                    }
                    if (node.getVendor().getVendorId().equals(mapping.getVendor().getVendorId())
                            && node.getNodeType().getTypeId().equals(mapping.getNodeType().getTypeId())
                            && node.getVersion().getVersionId().equals(mapping.getVersion().getVersionId())) {
                        serviceMapping = mapping;
                        break;
                    }
                }

                if (serviceMapping == null) {
                    for (ServiceTemplateMapping mapping : serviceMappings) {
                        if (mapping.getVendor() == null || mapping.getNodeType() == null) {
                            continue;
                        }
                        if (node.getVendor().getVendorId().equals(mapping.getVendor().getVendorId())
                                && node.getNodeType().getTypeId().equals(mapping.getNodeType().getTypeId())
                                && mapping.getVersion() == null) {
                            serviceMapping = mapping;
                            break;
                        }
                    }
                }

                if (serviceMapping == null) {
                    for (ServiceTemplateMapping mapping : serviceMappings) {
                        if (mapping.getVendor() == null) {
                            continue;
                        }
                        if (node.getVendor().getVendorId().equals(mapping.getVendor().getVendorId())
                                && mapping.getNodeType() == null && mapping.getVersion() == null) {
                            serviceMapping = mapping;
                            break;
                        }
                    }
                }
            }

            if (serviceMapping == null) {
                result.setResultCode(3);
                result.setResultMessage("Service Code không tồn tại");
                return result;
            }

            FlowTemplates flowTemplate = serviceMapping.getTemplate();
            if (flowTemplate == null) {
                result.setResultCode(3);
                result.setResultMessage("Template không tồn tại");
                return result;
            } else if (flowTemplate.getStatus() != 9) {
                result.setResultCode(4);
                result.setResultMessage("Template chưa được phê duyệt");
                return result;
            }

            if (nodeCode == null || nodeCode.trim().isEmpty()) {
                result.setResultCode(5);
                result.setResultMessage("NodeCode không tồn tại");
                return result;
            }

            GenerateFlowRunController generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();

            if (crNumber == null || crNumber.trim().isEmpty()) {
                flowRunAction.setCrNumber("CR_AUTO");
            } else {
                flowRunAction.setCrNumber(crNumber);
            }
            flowRunAction.setFlowRunName(serviceCode + "_" + nodeCode + "_" + new SimpleDateFormat("yyMMdd_HHmmss").format(new Date()));
            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setCreateDate(new Date());
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplate);
            flowRunAction.setCreateBy("SYSTEM");
            flowRunAction.setMopType(0l);
            flowRunAction.setRequestId(requestId);

            Map<String, Object> _filters = new HashMap<>();
            _filters.put("countryCode.countryCode-" + MapProcessCountryServiceImpl.EXAC, "VNM_SECURITY");
            _filters.put("status", 1l);

            List<MapProcessCountry> maps = new MapProcessCountryServiceImpl().findList(_filters);

            if (maps != null && !maps.isEmpty()) {
                //Sap xep lai maps theo thu tu random
                Collections.shuffle(maps);

                MapProcessCountry obj = maps.get(0);
                flowRunAction.setPortRun(obj.getProcessPort().longValue());
                flowRunAction.setIpRun(obj.getProcessIp());
                flowRunAction.setStatus(0L);
            } else {
                flowRunAction.setStatus(0L);
            }

            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplate);
            generateFlowRunController.setNodes(new ArrayList<Node>());
            generateFlowRunController.getNodes().add(node);
            generateFlowRunController.loadGroupAction(0l);

            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            try {
                Map<Long, List<ActionOfFlow>> mapGroupAction = new HashMap<>();
                LOGGER.info("---get mapGroupAction---");
                for (ActionOfFlow actionOfFlow : flowTemplate.getActionOfFlows()) {
                    if (mapGroupAction.get(actionOfFlow.getGroupActionOrder()) == null) {
                        mapGroupAction.put(actionOfFlow.getGroupActionOrder(), new ArrayList<ActionOfFlow>());
                    }
                    mapGroupAction.get(actionOfFlow.getGroupActionOrder()).add(actionOfFlow);
                }
                LOGGER.info("---get mapGroupAction thanh cong " + mapGroupAction.size() + "---");

                Map<String, String> params = new HashMap<>();
                Map<String, String> paramToSave = new HashMap<>();
                if (paramInputDTO.getParams() != null && !paramInputDTO.getParams().isEmpty()) {
                    for (ParamDTO pr : paramInputDTO.getParams()) {
                        params.put(pr.getParamCode(), pr.getParamValue());

                        if (pr.getParamCode() != null && pr.getParamCode().toUpperCase().contains("PASSWORD")) {
                            paramToSave.put(pr.getParamCode(), "******");
                        } else {
                            paramToSave.put(pr.getParamCode(), pr.getParamValue());
                        }
                    }
                }
                flowRunAction.setSecurityParamInput(new Gson().toJson(paramToSave));

                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
                LOGGER.info("---save  flowRunAction" + flowRunAction + "---");

                List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<>();
                Map<String, String> paramLower = new HashMap<>();
                for (String key : params.keySet()) {
                    if (key == null) {
                        continue;
                    }
                    paramLower.put(key.trim().toLowerCase(), params.get(key));
                }

                List<NodeRun> nodeRuns = new ArrayList<>();
                NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), Config.SUB_FLOW_RUN_DEFAULT), flowRunAction, node, Config.SUB_FLOW_RUN_DEFAULT_NUM);
                nodeRun.setAccount(PassProtector.encrypt(account, "ipchange"));
                nodeRun.setPassword(PassProtector.encrypt(password, "ipchange"));
                nodeRuns.add(nodeRun);

                List<Long> actionOfFlowIds = new ArrayList<>();
                for (ActionOfFlow action : flowTemplate.getActionOfFlows()) {
                    actionOfFlowIds.add(action.getStepNum());
                }

                Map<String, Collection<?>> map = new HashMap<>();
                map.put("actionFlowIds", actionOfFlowIds);
                List<ParamInOut> paramInOuts = new ArrayList<>();
                if (!actionOfFlowIds.isEmpty()) {
                    paramInOuts = new ParamInOutServiceImpl().findListWithIn("from ParamInOut where actionOfFlowByActionFlowInId.stepNum in (:actionFlowIds)", -1, -1, map);
                }

                List<String> pCodeInOuts = new ArrayList<>();
                for (ParamInOut pInOut : paramInOuts) {
                    pCodeInOuts.add(pInOut.getParamInput().getParamCode());
                }

                List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                LOGGER.info("---ket thuc lay paramValues" + paramValues.size() + "---");

                Iterator<ParamValue> iter = paramValues.iterator();
                while (iter.hasNext()) {
                    ParamValue paramValue = iter.next();
                    if (paramValue.getParamInput().getReadOnly() || pCodeInOuts.contains(paramValue.getParamCode())) {
                        iter.remove();
                        continue;
                    }
                    Object value = null;
                    try {
                        value = paramLower.get(paramValue.getParamCode().toLowerCase().trim());
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    ResourceBundle bundle = ResourceBundle.getBundle("cas");
                    if (bundle.getString("service").contains("10.61.127.190")) {
                        if (value == null || value.toString().isEmpty()) {
                            value = "TEST_NOT_FOUND";
                        }
                    }
                    if (value == null || value.toString().trim().isEmpty()) {
                        LOGGER.info(paramToSave.toString());
                        result.setResultCode(6);
                        result.setResultMessage("Tham số: " + paramValue.getParamCode() + " chưa được điền");
                        return result;
                    } else {
                        paramValue.setParamValue(value.toString());
                    }
                    paramValue.setNodeRun(nodeRun);
                    paramValue.setCreateTime(new Date());
                    paramValue.setParamValueId(null);
                }

                LOGGER.info(" co vao mapGroupAction size = " + mapGroupAction.size());
                for (Map.Entry<Long, List<ActionOfFlow>> entry : mapGroupAction.entrySet()) {
                    NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                            new NodeRunGroupActionId(node.getNodeId(),
                                    flowRunAction.getFlowRunId(),
                                    entry.getValue().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), entry.getValue().get(0), nodeRun);
                    nodeRunGroupActions.add(nodeRunGroupAction);
                }
                LOGGER.info(" insert NodeRunServiceImpl ");
                new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);

                LOGGER.info(" insert ParamValueServiceImpl ");
                new ParamValueServiceImpl().saveOrUpdate(paramValues, session, tx, false);

                new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);

                result.setFlowRunId(flowRunAction.getFlowRunId());
                String accountLocal = PassProtector.encrypt(account == null ? "" : account, "ipchange");
                String passLocal = PassProtector.encrypt(password == null ? "" : password, "ipchange");
               String socketResult =  startProcess(flowRunAction,nodeRuns,"system",accountLocal,passLocal);
               try {
                   if (socketResult == null) {
                       result.setResultCode(1);
                       result.setResult("NOK");
                       result.setCommandLogObjects(new ArrayList<CommandLogObject>());
                       return result;
                   } else {
                       Gson gson = (new GsonBuilder()).create();
                       result = gson.fromJson(socketResult, FlowRunLogCmdObject.class);
                       result.setRequestId(requestId);
                       if(result.getResult().equalsIgnoreCase("OK")){
                           result.setResultCode(0);
                       }else{
                           result.setResultCode(1);
                       }

                   }
               }catch(Exception ex){
                   LOGGER.error(ex.getMessage(),ex);
               }


            } catch (AppException | SysException | HibernateException e) {
                if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                    tx.rollback();
                }
                throw e;
            } finally {
                if (session.isOpen()) {
                    session.close();
                }
            }
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }

        return result;
    }
    private String startProcess(FlowRunAction flowRunAction, List<NodeRun> lstNodeRun,
            String username, String userRun, String passRun) throws Exception {
        String socketResult = null;
        try {
//            passRun = PassProtector.decrypt(passRun, "ipchange");

            Map<String, AccountObj> mapNodeAccount = new HashMap<>();
            for (NodeRun nodeRun : lstNodeRun) {
                mapNodeAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj(userRun, passRun));
            }
            MessageObject mesObj = new MessageObject(flowRunAction.getFlowRunId(),
                    username, userRun, passRun, null, flowRunAction.getFlowRunName(), "");
            mesObj.setRunType(1);
            mesObj.setErrorMode(1);
            mesObj.setRunningType(1);

//            String encrytedMess = new String(Base64.encodeBase64((new Gson()).toJson(mesObj).getBytes("UTF-8")), "UTF-8");
            String encrytedMess = new String(Base64.encodeBase64(("Security:" + (new Gson()).toJson(mesObj)).getBytes("UTF-8")), "UTF-8");
            LOGGER.info("vao send message den tien trinh");
            socketResult= sendMsg2ThreadExecute(encrytedMess, "VNM_SECURITY");

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return socketResult;
    }

    private String sendMsg2ThreadExecute(String encrytedMess, String countryCode) throws Exception {
        Map<String, Object> filters = new HashMap<>();
        filters.put("countryCode.countryCode", countryCode);
        filters.put("status", 1l);
        String socketResult = null;
        List<MapProcessCountry> maps = new MapProcessCountryServiceImpl().findListExac(filters, null);

        if (maps != null && !maps.isEmpty()) {
            //Sap xep lai maps theo thu tu random
            Collections.shuffle(maps);

            int i = 0;
            for (MapProcessCountry process : maps) {
                try {
                    int serverPort = process.getProcessPort();
                    String serverIp = process.getProcessIp();
//                    serverPort = 8866;
//                    serverIp = "localhost";
                    LOGGER.info("server: " + serverIp + "/ port: " + serverPort);

                    SocketClient client = new SocketClient(serverIp, serverPort);
                    client.sendMsg(encrytedMess);

                    socketResult = client.receiveResult();
                    if (socketResult != null && socketResult.contains("NOK_MAX_SESSION")) {
                        if (i == maps.size() - 1) {
                            return null;
                        }
                    } else {
                        return socketResult;
                    }

                    i++;
                    return socketResult;
                }catch(Exception e){
                    if (i == maps.size() - 1) {
                        return null;
                    }
                    LOGGER.error(e.getMessage(), e);
                }
            }
        } else {
            LOGGER.error("Loi khong lay duoc thong tin country code: " + countryCode);

        }
        return socketResult;
    }

    private boolean checkWhiteListIp() {
        try {
            HttpExchange exchange = (HttpExchange) context.getMessageContext().get("com.sun.xml.internal.ws.http.exchange");

            if (exchange == null) {
                exchange = (HttpExchange) context.getMessageContext().get("com.sun.xml.ws.http.exchange");
            }

            String remoteHost;
            if (exchange == null) {
                HttpServletRequest req = (HttpServletRequest) context.getMessageContext().get(MessageContext.SERVLET_REQUEST);
                remoteHost = req.getRemoteAddr();
            } else {
                remoteHost = exchange.getRemoteAddress().getAddress().getHostAddress();
            }

            List<?> ips = new DaoSimpleService().findListSQLAll("SELECT IP from WHITELIST_WS WHERE SYSTEM_NAME='WS_FOR_SECURITY'");
            LOGGER.info("IP remote: " + remoteHost);
            if (ips == null || ips.isEmpty()) {
                return true;
            }
            for (Object ip : ips) {
                if (ip != null && ip.equals(remoteHost)) {
                    return true;
                }
            }
            LOGGER.info("Forbidden access: " + remoteHost);
        } catch (Exception e) {
            throw e;
        }

        return false;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(countDays(new Date(), new Date()));
    }

    @Override
    public ResultCreateMop extendDateAccount(String requestId, String userService, String passService, String crNumber, String accountName, String nodeCode,
            String accountEffectiveDate, String accountExpireDate, String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));
            prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE", accountEffectiveDate));
            prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE", accountExpireDate));

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            if (accountEffectiveDate != null) {
                Calendar effDate = Calendar.getInstance();
                effDate.setTime(sdf.parse(accountEffectiveDate));

                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_date", standardValue(effDate.get(Calendar.DATE))));
                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_month", standardValue(effDate.get(Calendar.MONTH + 1))));
                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_year", standardValue(effDate.get(Calendar.YEAR))));

                prInputDTO.getParams().add(new ParamDTO("EFFECTIVE_DATE_day_from_now", String.valueOf(countDays(effDate.getTime(), new Date()))));
            }

            if (accountExpireDate != null) {
                Calendar expDate = Calendar.getInstance();
                expDate.setTime(sdf.parse(accountExpireDate));

                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_date", standardValue(expDate.get(Calendar.DATE))));
                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_month", standardValue(expDate.get(Calendar.MONTH + 1))));
                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_year", standardValue(expDate.get(Calendar.YEAR))));

                prInputDTO.getParams().add(new ParamDTO("EXPIRATION_DATE_day_from_now", String.valueOf(countDays(expDate.getTime(), new Date()))));
            }

            return saveDT(requestId, crNumber, NE_ACCOUNT_EXTEND_DATE, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }
    }

    @Override
    public ResultCreateMop lockAccount(String requestId, String userService, String passService, String crNumber, String accountName, String nodeCode, String accountAdmin, String passwordAdmin) {
        ResultCreateMop result = new ResultCreateMop();
        result.setRequestId(requestId);
        try {
            if (!checkWhiteListIp()) {
                result.setResultCode(100);
                result.setResultMessage("Bạn không có quyền gọi Webservice");
                return result;
            }

            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                result.setResultCode(2);
                result.setResultMessage("Username hoặc mật khẩu webservice không chính xác");
                return result;
            }

            ParamInputDTO prInputDTO = new ParamInputDTO(new ArrayList<ParamDTO>());
            prInputDTO.getParams().add(new ParamDTO("crNumber", crNumber));
            prInputDTO.getParams().add(new ParamDTO("nodeCode", nodeCode));
            prInputDTO.getParams().add(new ParamDTO("USERNAME", accountName));

            result = saveDT(requestId, crNumber, NE_ACCOUNT_LOCK, nodeCode, accountAdmin, passwordAdmin, prInputDTO);
        } catch (Exception ex) {
            result.setResultCode(1);
            result.setResultMessage("Có lỗi xảy ra khi gọi Webservice");
            LOGGER.error(ex.getMessage(), ex);
            return result;
        }

        return result;
    }
}
