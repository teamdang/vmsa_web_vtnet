/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import com.viettel.model.ChangeParam;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author quytv7
 */
@Scope("session")
@Service(value = "changeParamService")
public class ChangeParamServiceImpl extends GenericDaoImplNewV2<ChangeParam, Long> implements GenericDaoServiceNewV2<ChangeParam, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
