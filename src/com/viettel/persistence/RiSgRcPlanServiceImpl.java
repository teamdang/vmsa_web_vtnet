/**
 * Created on Tue Sep 06 09:17:09 ICT 2016
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.viettel.model.ENodeBPlan;
import com.viettel.model.RiSgRcPlan;

/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Oct 25, 2016
 * @version 1.0
 */
@Scope("session")
@Service(value = "riSgRcPlanService")
public class RiSgRcPlanServiceImpl extends GenericDaoImplNewV2<RiSgRcPlan, Long> implements GenericDaoServiceNewV2<RiSgRcPlan, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
