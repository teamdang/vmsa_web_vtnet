/**
 * Created on Tue Sep 06 09:17:09 ICT 2016
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence;

import com.viettel.model.RiSgRcParamLog;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author quytv7
 * @sin Oct 25, 2016
 * @version 1.0
 */
@Scope("session")
@Service(value = "riSgParamLogService")
public class RiSgRcParamLogServiceImpl extends GenericDaoImplNewV2<RiSgRcParamLog, Long> implements GenericDaoServiceNewV2<RiSgRcParamLog, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
