/**
 * Created on Tue Sep 06 09:17:09 ICT 2016
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.viettel.model.RiSgRcParamLinkSgsn;

@Scope("session")
@Service(value = "riSgRcParamLinkSgsnService")
public class RiSgRcParamLinkSgsnServiceImpl extends GenericDaoImplNewV2<RiSgRcParamLinkSgsn, Long> implements GenericDaoServiceNewV2<RiSgRcParamLinkSgsn, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
