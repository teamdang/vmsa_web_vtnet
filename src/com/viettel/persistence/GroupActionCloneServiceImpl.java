/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import com.viettel.model.GroupActionClone;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author hienhv4
 */
@Scope("session")
@Service(value = "groupActionCloneService")
public class GroupActionCloneServiceImpl extends GenericDaoImplNewV2<GroupActionClone, Long> implements  Serializable{
    
}
