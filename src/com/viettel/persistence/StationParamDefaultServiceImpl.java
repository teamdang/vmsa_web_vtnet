/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import com.viettel.model.StationParamDefault;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author quytv7
 */
@Scope("session")
@Service(value = "stationParamDefaultService")
public class StationParamDefaultServiceImpl extends GenericDaoImplNewV2<StationParamDefault, Long> implements GenericDaoServiceNewV2<StationParamDefault, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
