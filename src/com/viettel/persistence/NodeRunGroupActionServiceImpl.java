/** Created on Tue Sep 06 09:17:09 ICT 2016
*
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
*/
package com.viettel.persistence;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.viettel.model.NodeRunGroupAction;
import com.viettel.model.NodeRunGroupActionId;
/**
* NodeRunServiceImpl.java
*
* @author Huy, Nguyen Xuan<huynx6@viettel.com.vn>
* @since Tue Sep 06 09:17:09 ICT 2016
* @version 1.0.0
*/
@Scope("session")
@Service(value = "nodeRunGroupActionService")
public class NodeRunGroupActionServiceImpl  extends GenericDaoImplNewV2<NodeRunGroupAction, NodeRunGroupActionId> implements  Serializable{
	private static final long serialVersionUID = -41096155610L;

}