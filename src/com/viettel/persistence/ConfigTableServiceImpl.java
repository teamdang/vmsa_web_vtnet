/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import com.viettel.model.ConfigTable;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author hienhv4
 */
@Scope("session")
@Service(value = "configTableServiceImpl")
public class ConfigTableServiceImpl extends GenericDaoImplNewV2<ConfigTable, String> implements  Serializable{
    
}
