/**
 * Created on Tue Sep 06 09:17:09 ICT 2016
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence;

import com.viettel.model.StationPlan;
import com.viettel.model.SyncParamOtherSystem;
import com.viettel.persistence.GenericDaoImplNewV2;
import com.viettel.persistence.GenericDaoServiceNewV2;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * @author quytv7
 * @sin Oct 25, 2016
 * @version 1.0
 */
@Scope("session")
@Service(value = "syncParamOtherSystemService")
public class SyncParamOtherSystemServiceImpl extends GenericDaoImplNewV2<SyncParamOtherSystem, Long> implements GenericDaoServiceNewV2<SyncParamOtherSystem, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
