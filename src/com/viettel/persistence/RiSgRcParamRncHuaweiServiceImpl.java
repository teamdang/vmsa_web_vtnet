/**
 * Created on Tue Sep 06 09:17:09 ICT 2016
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence;

import com.viettel.model.RiSgRcParamRncHuawei;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("session")
@Service(value = "riSgRcParamRncHuaweiService")
public class RiSgRcParamRncHuaweiServiceImpl extends GenericDaoImplNewV2<RiSgRcParamRncHuawei, Long> implements GenericDaoServiceNewV2<RiSgRcParamRncHuawei, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
