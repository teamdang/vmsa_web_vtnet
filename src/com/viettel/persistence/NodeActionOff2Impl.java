/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.viettel.model.NodeActionOff2;

/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Oct 19, 2016
 * @version 1.0 
 */
@SuppressWarnings("serial")
@Scope("session")
@Service(value = "nodeActionOff2Service")
public class NodeActionOff2Impl extends GenericDaoImplNewV2<NodeActionOff2, Long> implements  Serializable{
    
}
