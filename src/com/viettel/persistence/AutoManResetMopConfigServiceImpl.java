/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import com.viettel.model.AutoManResetMopConfig;
import com.viettel.model.CrLogFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 *
 * @author hienhv4
 */
@Scope("session")
@Service(value = "autoManResetMopConfigService")
public class AutoManResetMopConfigServiceImpl extends GenericDaoImplNewV2<AutoManResetMopConfig, Long> implements  Serializable{
    
}
