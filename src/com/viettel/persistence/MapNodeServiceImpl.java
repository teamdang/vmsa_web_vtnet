/**
 * Created on Tue Sep 06 09:17:09 ICT 2016
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence;

import com.viettel.model.MapNode;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @author dunglv
 * @sin Oct 25, 2016
 * @version 1.0
 */
@Scope("session")
@Service(value = "mapNodeService")
public class MapNodeServiceImpl extends GenericDaoImplNewV2<MapNode, Long> implements GenericDaoServiceNewV2<MapNode, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
