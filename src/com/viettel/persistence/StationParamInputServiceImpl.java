/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import com.viettel.model.StationParamInput;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author quytv7
 */
@Scope("session")
@Service(value = "stationParamInputService")
public class StationParamInputServiceImpl extends GenericDaoImplNewV2<StationParamInput, Long> implements GenericDaoServiceNewV2<StationParamInput, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
