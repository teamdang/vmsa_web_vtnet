/**
 * Created on Tue Sep 06 09:17:09 ICT 2016
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence;

import com.viettel.model.RiMsRcParamRncNokia;
import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("session")
@Service(value = "riMsRcParamRncNokiaService")
public class RiMsRcParamRncNokiaServiceImpl extends GenericDaoImplNewV2<RiMsRcParamRncNokia, Long> implements GenericDaoServiceNewV2<RiMsRcParamRncNokia, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
