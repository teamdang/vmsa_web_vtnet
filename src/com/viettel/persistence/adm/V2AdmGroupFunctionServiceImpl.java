/**
 * Created on Thu May 25 08:41:27 ICT 2017
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence.adm;

import com.viettel.model.adm.AdmGroupFunctionId;
import com.viettel.model.adm.V2AdmGroupFunctionBO;
import com.viettel.persistence.*;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("session")
@Service(value = "v2AdmGroupFunctionService")
public class V2AdmGroupFunctionServiceImpl extends GenericDaoImplNewV2<V2AdmGroupFunctionBO, AdmGroupFunctionId> implements GenericDaoServiceNewV2<V2AdmGroupFunctionBO, AdmGroupFunctionId>, Serializable {

}
