/**
 * Created on Thu May 25 08:41:27 ICT 2017
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence.adm;

import com.viettel.model.adm.AdmFuncQueryButtonId;
import com.viettel.model.adm.V2AdmFuncQueryButtonBO;
import com.viettel.persistence.*;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("session")
@Service(value = "v2AdmFuncQueryButtonService")
public class V2AdmFuncQueryButtonServiceImpl extends GenericDaoImplNewV2<V2AdmFuncQueryButtonBO, AdmFuncQueryButtonId> implements GenericDaoServiceNewV2<V2AdmFuncQueryButtonBO, AdmFuncQueryButtonId>, Serializable {

}
