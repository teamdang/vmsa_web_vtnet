/** Created on Wed Jul 26 10:10:59 ICT 2017
*
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
*/
package com.viettel.persistence.adm;

import com.viettel.model.adm.V2AdmDatabase;
import com.viettel.persistence.GenericDaoImplNewV2;
import com.viettel.persistence.GenericDaoServiceNewV2;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
* TnEnergyServiceImpl.java
*
* @author Thuy, Pham Quang<thuypq@viettel.com.vn>
* @since Wed Jul 26 10:10:59 ICT 2017
* @version 1.0.0
*/
@Scope("session")
@Service(value = "v2AdmDatabaseService")
public class V2AdmDatabaseServiceImpl extends GenericDaoImplNewV2<V2AdmDatabase, Long> implements GenericDaoServiceNewV2<V2AdmDatabase, Long> ,  Serializable{
	private static final long serialVersionUID = -41096111488556103L;

}