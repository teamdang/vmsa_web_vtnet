/**
 * Created on Thu May 25 08:41:27 ICT 2017
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence.adm;

import com.viettel.model.adm.AdmFuncQueryCondId;
import com.viettel.persistence.*;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.viettel.model.adm.V2AdmFuncQueryCondBO;

@Scope("session")
@Service(value = "v2AdmFuncQueryCondService")
public class V2AdmFuncQueryCondServiceImpl extends GenericDaoImplNewV2<V2AdmFuncQueryCondBO, AdmFuncQueryCondId> implements GenericDaoServiceNewV2<V2AdmFuncQueryCondBO, AdmFuncQueryCondId>, Serializable {

}
