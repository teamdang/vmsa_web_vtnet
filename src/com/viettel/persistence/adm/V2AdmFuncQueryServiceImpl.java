/**
 * Created on Thu May 25 08:41:27 ICT 2017
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence.adm;

import com.viettel.model.adm.AdmFuncQueryId;
import com.viettel.model.adm.V2AdmFuncQueryBO;
import com.viettel.persistence.*;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("session")
@Service(value = "v2AdmFuncQueryService")
public class V2AdmFuncQueryServiceImpl extends GenericDaoImplNewV2<V2AdmFuncQueryBO, AdmFuncQueryId> implements GenericDaoServiceNewV2<V2AdmFuncQueryBO, AdmFuncQueryId>, Serializable {

}
