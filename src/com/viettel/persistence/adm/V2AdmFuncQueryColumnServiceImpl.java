/**
 * Created on Thu May 25 08:41:27 ICT 2017
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence.adm;

import com.viettel.model.adm.AdmFuncQueryColumnId;
import com.viettel.model.adm.V2AdmFuncQueryColumnBO;
import com.viettel.persistence.*;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("session")
@Service(value = "v2AdmFuncQueryColumnService")
public class V2AdmFuncQueryColumnServiceImpl extends GenericDaoImplNewV2<V2AdmFuncQueryColumnBO, AdmFuncQueryColumnId> implements GenericDaoServiceNewV2<V2AdmFuncQueryColumnBO, AdmFuncQueryColumnId>, Serializable {

}
