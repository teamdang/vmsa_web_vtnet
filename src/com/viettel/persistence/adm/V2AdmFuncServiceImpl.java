/**
 * Created on Thu May 25 08:41:27 ICT 2017
 * 
* Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.persistence.adm;

import com.viettel.model.adm.V2AdmFuncBO;
import com.viettel.persistence.*;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Scope("session")
@Service(value = "v2AdmFuncService")
public class V2AdmFuncServiceImpl extends GenericDaoImplNewV2<V2AdmFuncBO, String> implements GenericDaoServiceNewV2<V2AdmFuncBO, String>, Serializable {

}
