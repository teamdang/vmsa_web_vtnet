package com.viettel.persistence;

import com.viettel.model.ParamThresold;
import com.viettel.model.TemplateGroup;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
 * Created by hanh on 4/14/2017.
 */
@Scope("session")
@Service(value = "paramThresoldService")
public class ParamThresoldServiceImpl extends GenericDaoImplNewV2<ParamThresold, Long>
        implements GenericDaoServiceNewV2<ParamThresold, Long>, Serializable {
}
