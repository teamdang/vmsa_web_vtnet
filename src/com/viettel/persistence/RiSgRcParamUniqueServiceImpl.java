/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.persistence;

import com.viettel.model.RiSgRcParamUnique;
import java.io.Serializable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 *
 * @author hienhv4
 */
@Scope("session")
@Service(value = "riSgRcParamUniqueService")
public class RiSgRcParamUniqueServiceImpl extends GenericDaoImplNewV2<RiSgRcParamUnique, Long> implements GenericDaoServiceNewV2<RiSgRcParamUnique, Long>, Serializable {

    private static final long serialVersionUID = -41096111355610L;

}
