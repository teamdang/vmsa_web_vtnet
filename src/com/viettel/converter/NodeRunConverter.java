package com.viettel.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import com.viettel.model.NodeRun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FacesConverter("nodeRunConverter")
public class NodeRunConverter implements Converter {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
        // TODO Auto-generated method stub
        try {
            //return new NodeRunServiceImpl().findById(Long.valueOf(arg2));
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
        // TODO Auto-generated method stub
        if (arg2 instanceof NodeRun) {
            NodeRun new_name = (NodeRun) arg2;
            return new_name.getId().toString();
        }
        return null;
    }

}
