package com.viettel.converter;

// Created Aug 19, 2016 1:57:49 PM by quanns2
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import com.viettel.model.Vendor;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author quanns2
 *
 */
@FacesConverter(value = "vendorConverter")
public class VendorConverter implements Converter {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component,
            String submittedValue) {
        Vendor o;
        try {
            if (submittedValue != null && !"null".equalsIgnoreCase(submittedValue.trim()) && !MessageUtil.getResourceBundleMessage("common.choose").equalsIgnoreCase(submittedValue)) {
                if ("".equals(submittedValue.trim())) {
                    return null;
                }

                Long id = Long.valueOf(submittedValue.trim());

                o = new VendorServiceImpl().findById(id);
                return o;
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return null;

    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        // hanhnv68 add 20160913
        if (object != null && object instanceof Vendor) {
            // end hanhnv68 add 20160913
            Vendor new_name = (Vendor) object;
            try {
                return (new_name.getVendorId()) != null ? new_name.getVendorId().toString() : "";
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return null;
    }
}
