/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.converter;

import com.viettel.model.Version;
import com.viettel.persistence.VersionServiceImpl;
import com.viettel.util.MessageUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hienhv4
 */
@FacesConverter(value = "versionConverter")
public class VersionConverter implements Converter {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component,
            String submittedValue) {
        Version o;
        try {
            if (submittedValue != null && !"null".equalsIgnoreCase(submittedValue.trim()) && !MessageUtil.getResourceBundleMessage("common.choose").equalsIgnoreCase(submittedValue)) {
                if ("".equals(submittedValue.trim())) {
                    return null;
                }
                LOGGER.info("---------------Version submittedValue.trim() : " + submittedValue.trim());
                Long id = Long.valueOf(submittedValue.trim());
                o = new VersionServiceImpl().findById(id);
                return o;
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return null;

    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null && object instanceof Version) {
            Version new_name = (Version) object;
            try {
                return (new_name.getVersionId()) != null ? new_name.getVersionId().toString() : "";
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return null;
    }
}
