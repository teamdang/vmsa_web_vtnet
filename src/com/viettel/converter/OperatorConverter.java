/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.converter;

import com.viettel.object.ComboBoxObject;
import com.viettel.util.MessageUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hienhv4
 */
@FacesConverter(value = "operatorConverter")
public class OperatorConverter implements Converter {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component,
            String submittedValue) {
        try {
            if (submittedValue != null && !"null".equalsIgnoreCase(submittedValue.trim())
                    && !MessageUtil.getResourceBundleMessage("common.choose").equals(submittedValue.trim())) {
                if ("".equals(submittedValue.trim())) {
                    return null;
                }
                return new ComboBoxObject(submittedValue.trim(), submittedValue.trim());
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return null;

    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        // hanhnv68 add 20160913
        if (object != null && object instanceof ComboBoxObject) {
            // end hanhnv68 add 20160913
            ComboBoxObject new_name = (ComboBoxObject) object;
            try {
                return new_name.getValue();
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return null;
    }
}
