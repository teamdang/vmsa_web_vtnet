package com.viettel.converter;

// Created Aug 19, 2016 1:57:49 PM by quanns2
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.viettel.model.NodeType;
import com.viettel.persistence.NodeTypeServiceImpl;
import com.viettel.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author quanns2
 *
 */
@FacesConverter(value = "nodeTypeConverter")
public class NodeTypeConverter implements Converter {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component,
            String submittedValue) {
        NodeType o;
        try {
            if (submittedValue != null && !"null".equalsIgnoreCase(submittedValue.trim()) && !MessageUtil.getResourceBundleMessage("common.choose").equalsIgnoreCase(submittedValue)) {
                if ("".equals(submittedValue.trim())) {
                    return null;
                }
                Long id = Long.valueOf(submittedValue.trim());
                o = new NodeTypeServiceImpl().findById(id);
                return o;
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return null;

    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        // hanhnv68 add 20160913
        if (object != null && object instanceof NodeType) {
            // end hanhnv68 add 20160913
            NodeType new_name = (NodeType) object;
            try {
                if (new_name.getTypeId() != null) {
                    return new_name.getTypeId().toString();
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return null;
    }
}
