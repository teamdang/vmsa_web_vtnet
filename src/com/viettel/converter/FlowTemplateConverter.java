package com.viettel.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.FlowTemplates;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author huynx6
 */
@FacesConverter("flowTemplateConverter")
public class FlowTemplateConverter implements Converter {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
        // TODO Auto-generated method stub
        try {
            if (arg2 != null && !arg2.trim().isEmpty()
                    && !MessageUtil.getResourceBundleMessage("common.choose").equalsIgnoreCase(arg2)
                    && !MessageUtil.getResourceBundleMessage("label.choose.template").equalsIgnoreCase(arg2)
                    ) {
                return new FlowTemplatesServiceImpl().findById(Long.parseLong(arg2));
            }
        } catch (NumberFormatException | SysException | AppException e) {
            logger.debug(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
        // TODO Auto-generated method stub
        if (arg2 instanceof FlowTemplates) {
            FlowTemplates new_name = (FlowTemplates) arg2;
            if (new_name.getFlowTemplatesId() != null) {
                return new_name.getFlowTemplatesId().toString();
            } else {
                return null;
            }
        }
        return null;
    }

}
