package com.viettel.converter;

import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import com.viettel.model.Node;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FacesConverter("nodeConverter")
public class NodeConverter implements Converter {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
        // TODO Auto-generated method stub
        Node o;
        try {
            if (arg2 != null && !arg2.trim().isEmpty() && !"null".equalsIgnoreCase(arg2.trim())
                    && !MessageUtil.getResourceBundleMessage("common.choose").equalsIgnoreCase(arg2)) {
                if ("".equals(arg2.trim())) {
                    return null;
                }
                Long id = Long.valueOf(arg2.trim());
                o = new NodeServiceImpl().findById(id);
                return o;
            }
        } catch (AppException | SysException e)  {
            logger.error(e.getMessage(), e);
        }catch(NumberFormatException e){
//            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
        // TODO Auto-generated method stub
        if (arg2 instanceof Node) {
            Node new_name = (Node) arg2;
            return new_name.getNodeId() + "";
        }
        return null;
    }

}
