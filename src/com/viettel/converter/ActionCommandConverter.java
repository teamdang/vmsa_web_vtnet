package com.viettel.converter;

// Created Aug 19, 2016 1:57:49 PM by quanns2
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.viettel.model.ActionCommand;
import com.viettel.persistence.ActionCommandServiceImpl;
import com.viettel.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author quanns2
 *
 */
@FacesConverter(value = "actionCommandConverter")
public class ActionCommandConverter implements Converter {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component,
            String submittedValue) {
        ActionCommand o = null;
        try {
            if (submittedValue != null && !"null".equalsIgnoreCase(submittedValue.trim()) && !MessageUtil.getResourceBundleMessage("common.choose").equalsIgnoreCase(submittedValue)) {
                if ("".equals(submittedValue.trim())) {
                    return null;
                }
                Long id = null;
                try {
                    id = Long.valueOf(submittedValue.trim());
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                if (id != null) {
                    o = new ActionCommandServiceImpl().findById(id);
                }
                return o;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if (object != null && object instanceof ActionCommand) {
            return ((ActionCommand) object).getActionCommandId() + "";
        }
        return null;
    }
}
