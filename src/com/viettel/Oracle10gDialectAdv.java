package com.viettel;

import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.type.StringType;

import java.sql.Types;

/**
 * Created by ducnm26 on 5/7/15.
 */
public class Oracle10gDialectAdv extends Oracle10gDialect {

    public Oracle10gDialectAdv() {
        super();

        // Register mappings
        registerHibernateType(Types.NVARCHAR, StringType.INSTANCE.getName());
    }
}