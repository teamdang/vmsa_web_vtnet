package com.viettel.validator;

import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.viettel.persistence.CustomerNodeServiceImpl;
import com.viettel.util.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FacesValidator("customerValidator")
public class CustomerValidator implements Validator {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
        try {
            if (arg2 == null || arg2.toString().isEmpty()) {
                throw new Exception(MessageUtil.getResourceBundleMessage("error.customer.name.empty"));
            }
            Map<String, Object> filter = new HashMap<String, Object>();
            filter.put("customerName", arg2.toString());
            if (new CustomerNodeServiceImpl().findListExac(filter, null).size() > 0) {
                throw new Exception(MessageUtil.getResourceBundleMessage("error.customer.name.exist"));
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            String message = e.getMessage();
            FacesMessage msg = new FacesMessage("", message);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }
    }

}
