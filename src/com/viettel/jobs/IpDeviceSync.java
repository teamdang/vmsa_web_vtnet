package com.viettel.jobs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.viettel.exception.AppException;
import com.viettel.model.Node;
import com.viettel.model.NodeType;
import com.viettel.model.Province;
import com.viettel.model.Vendor;
import com.viettel.model.Version;
import com.viettel.nims.infra.webservice.InfraWS_PortType;
import com.viettel.nims.infra.webservice.InfraWS_ServiceLocator;
import com.viettel.nims.infra.webservice.VCoreMobileDeviceSearchBO;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.NodeTypeServiceImpl;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.persistence.VersionServiceImpl;

public class IpDeviceSync implements Job {
    
    public static final Logger LOGGER = LoggerFactory.getLogger(IpDeviceSync.class);
    
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        JobDataMap mergedJobDataMap = arg0.getMergedJobDataMap();
        Object sync_all = mergedJobDataMap.get("sync_all");
        if (sync_all != null && "true".equals(sync_all.toString())) {
            sync(true);
        } else {
            sync(false);
        }
        
    }
    
    public static void sync(boolean isSyncAll) throws JobExecutionException {
        // TODO Auto-generated method stub
        NodeServiceImpl mlNodeService = new NodeServiceImpl();
        try {
            InfraWS_ServiceLocator locator = new InfraWS_ServiceLocator();
            InfraWS_PortType ws_PortType = locator.getInfraWSPort();
            List<Integer> subnetting = Arrays.asList(224, 192, 160, 128, 96, 64, 32, 0);
            
            List<String> provinces = Arrays.asList("B281", "B241", "V070", "V064", "B240", "C026", "D230", "H019", "H320", "H031", "H018", "L020", "N350", "N038", "N030", "P210", "Q052", "Q033", "S022", "T036", "T280", "T037", "V211", "G059", "K058", "K060", "L063", "N068", "P057", "Q055", "Q510", "Q053", "T054", "B650", "B781", "B651", "B075", "C780", "C710", "D061", "D067", "H711", "K077", "S079", "T073", "T074", "N351", "H004", "H039", "H321", "L231", "L025", "T027", "Y029", "B056", "B062", "D501", "D500", "D511", "A076", "T008", "L072", "T066");
            //List<String> provinces = Arrays.asList("T074","N351","H004","H039","H321","L231","L025","T027","Y029","B056","B062","D501","D500","D511","A076","T008","L072","T066");
            List<Node> allNodes;
            try {
                allNodes = mlNodeService.findList();
            } catch (AppException e1) {
                LOGGER.error(e1.getMessage(), e1);
                return;
            }
            Map<String, Node> mapDevices = new HashMap<String, Node>();
            for (Node node : allNodes) {
                mapDevices.put(node.getNodeCode(), node);
            }
            List<Vendor> vendors = new VendorServiceImpl().findList();
            Map<String, Vendor> mapVendor = new HashMap<>();
            for (Vendor vendor : vendors) {
                mapVendor.put(vendor.getVendorName(), vendor);
                if (vendor.getDescription() != null) {
                    mapVendor.put(vendor.getDescription(), vendor);
                }
            }
            
            List<NodeType> nodeTypes = new NodeTypeServiceImpl().findList();
            Map<String, NodeType> mapNodeType = new HashMap<>();
            for (NodeType nodeType : nodeTypes) {
                mapNodeType.put(nodeType.getTypeName(), nodeType);
                if (nodeType.getDescription() != null) {
                    for (String string : nodeType.getDescription().split(",", -1)) {
                        mapNodeType.put(string.trim(), nodeType);
                    }
                }
            }
            mapNodeType.remove("CORESW_LAYER");
            List<Version> versions = new VersionServiceImpl().findList();
            Map<String, Version> mapVersion = new HashMap<>();
            for (Version version : versions) {
                mapVersion.put(version.getVersionName(), version);
                if (version.getDescription() != null) {
                    mapVersion.put(version.getDescription(), version);
                }
            }
            
            Map<String, String> mapNetworkType = new HashMap<String, String>();
            mapNetworkType.put("DCN", "DCN");
            mapNetworkType.put("IPBN", "TRUYEN TAI");
            mapNetworkType.put("METRO", "TRUYEN TAI");
            mapNetworkType.put("MPBN", "MPBN");
            mapNetworkType.put("INTRANET", "DCN");
            mapNetworkType.put("SERVICE", "CNTT");
            
            for (String provinceCode : provinces) {
                String networkClass = "";
                String vendorCode = "";
                String stationCode = "";
                String deviceCode = "";
                String networkType = "";
                String areaCode = null;
                Long departmentId = null;
                String fromDate = null;
                String toDate = null;
                if (!isSyncAll) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_YEAR, -1);
                    Date date = calendar.getTime();
                    fromDate = new SimpleDateFormat("dd/MM/yyyy 00:00:00").format(date);
                    toDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
                }
                String deviceTypeCode = null;
                Boolean hasIP = true;
                VCoreMobileDeviceSearchBO[] coreDeviceBOs = ws_PortType.getCoreMobileDeviceWithDate(networkClass, vendorCode,
                        stationCode, deviceCode, deviceTypeCode, networkType, areaCode, provinceCode, hasIP,
                        departmentId, fromDate, toDate);
                List<Node> mlNodes = new ArrayList<Node>();
                if (coreDeviceBOs == null) {
                    continue;
                }
                LOGGER.info(provinceCode + "\t" + coreDeviceBOs.length);
                Node mlNode;
                for (VCoreMobileDeviceSearchBO vCoreMobile : coreDeviceBOs) {
                    if (vCoreMobile.getNetworkClass() == null || "CORESW_LAYER".equals(vCoreMobile.getNetworkClass())) {
                        continue;
                    }
                    if (vCoreMobile.getNetworkType() == null || "CTH".equalsIgnoreCase(vCoreMobile.getNetworkType())) {
                        continue;
                    }
                    mlNode = new Node();
                    
                    mlNode.setNodeIp(vCoreMobile.getIpTelnet());
                    try {
                        for (Integer integer : subnetting) {
                            String octar4 = vCoreMobile.getIp().split("\\.", -1)[3];
                            if (Integer.valueOf(octar4) > integer) {
                                mlNode.setSubnetwork(vCoreMobile.getIp().replaceAll("\\." + octar4 + "$", "." + integer.toString()));
                                break;
                            }
                        }
                        
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    mlNode.setNodeCode(vCoreMobile.getDeviceName());
                    
                    Vendor vendor = mapVendor.get(vCoreMobile.getVendorCode());
                    if (vendor == null) {
                        vendor = new Vendor();
                        vendor.setVendorName(vCoreMobile.getVendorCode());
                        vendor.setDescription(vCoreMobile.getVendorCode());
                        new VendorServiceImpl().saveOrUpdate(vendor);
                        mapVendor.put(vCoreMobile.getVendorCode(), vendor);
                    }
                    mlNode.setVendor(vendor);
                    
                    NodeType nodeType = mapNodeType.get(vCoreMobile.getNetworkClass());
                    if (nodeType == null) {
                        nodeType = new NodeType();
                        nodeType.setTypeName(vCoreMobile.getNetworkClass());
                        nodeType.setDescription(vCoreMobile.getNetworkClass());
                        new NodeTypeServiceImpl().saveOrUpdate(nodeType);
                        mapNodeType.put(vCoreMobile.getNetworkClass(), nodeType);
                    }
                    mlNode.setNodeType(nodeType);
                    Version version = mapVersion.get(vCoreMobile.getDeviceTypeCode());
                    if (version == null) {
                        version = new Version();
                        version.setVersionName(vCoreMobile.getDeviceTypeCode());
                        version.setDescription(vCoreMobile.getDeviceTypeCode());
                        new VersionServiceImpl().saveOrUpdate(version);
                        mapVersion.put(vCoreMobile.getDeviceTypeCode(), version);
                    }
                    mlNode.setVersion(version);
                    Province province = new Province();
                    province.setProvinceCode(vCoreMobile.getProvinceCode());
                    mlNode.setProvince(province);
                    String _networkType = mapNetworkType.get(vCoreMobile.getNetworkType());
                    mlNode.setNetworkType(_networkType);
                    if (mapDevices.get(mlNode.getNodeCode()) != null) {
                        mlNode.setNodeId(mapDevices.get(mlNode.getNodeCode()).getNodeId());
                    }
                    mlNodes.add(mlNode);
                    
                }
                
                try {
                    mlNodeService.saveOrUpdate(mlNodes);
                    LOGGER.info(provinceCode + "\t" + coreDeviceBOs.length + "\twas saved!");
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
                
            }
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        
    }
    
    public static void main(String[] args) throws JobExecutionException {
//        String networkClass = "SITE_ROUTER";
//        String vendorCode = "";
//        String stationCode = "";
//        String deviceCode = "";
//        String deviceTypeName = "";
//        String networkType = "";
//        String provinceCode = "";
//        String ip = "";
//        Long departmentId = null;
//        String fromDate = null;
//        String toDate = null;
//        try {
//            InfraWS_ServiceLocator locator = new InfraWS_ServiceLocator();
//            InfraWS_PortType ws_PortType = locator.getInfraWSPort();
//            String areaCode = null;
//            VIpCoreDeviceBO[] coreDeviceBOs;
//            CatDeviceTypeBO[] deviceTypes = ws_PortType.getCatDeviceType();
//            CatVendorBO[] vendors = ws_PortType.getVendor(null, null);
//            coreDeviceBOs = ws_PortType.getIPDeviceWithDate(networkClass, vendorCode, stationCode, deviceCode,
//                    deviceTypeName, networkType, areaCode, provinceCode, ip, departmentId, fromDate, toDate);
//            System.err.println(coreDeviceBOs);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        sync(true);
    }
}
