package com.viettel.controller;

import static com.viettel.controller.RescueInfomationController.logger;
import com.viettel.model.FlowRunAction;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.model.ParamValue;
import com.viettel.model.RiMsRcParam;
import com.viettel.model.RiMsRcParamLacSac;
import com.viettel.model.RiMsRcParamLinkMsc;
import com.viettel.model.RiMsRcParamMscRnc;
import com.viettel.model.RiMsRcParamRncEricsson;
import com.viettel.model.RiMsRcParamRncHuawei;
import com.viettel.model.RiMsRcParamRncNokia;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.viettel.model.RiMsRcPlan;
import com.viettel.model.RiSgRcParam;
import com.viettel.object.MessageException;
import com.viettel.object.RiMsRcModelExcel;
import com.viettel.object.RiMsRcResult;
import com.viettel.object.RiMsRcResultImport;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.RiMsRcParamLacSacServiceImpl;
import com.viettel.persistence.RiMsRcParamLinkMscServiceImpl;
import com.viettel.persistence.RiMsRcParamMscRncServiceImpl;
import com.viettel.persistence.RiMsRcParamRncEricssonServiceImpl;
import com.viettel.persistence.RiMsRcParamRncHuaweiServiceImpl;
import com.viettel.persistence.RiMsRcParamRncNokiaServiceImpl;
import com.viettel.persistence.RiMsRcParamServiceImpl;
import com.viettel.persistence.RiMsRcPlanServiceImpl;
import com.viettel.persistence.RiSgRcParamServiceImpl;
import com.viettel.util.CommonExport;
import com.viettel.util.Config;
import com.viettel.util.Constants;
import com.viettel.util.Importer;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionUtil;
import com.viettel.util.SessionWrapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.event.FileUploadEvent;

public class RiMsRcController {

    //<editor-fold defaultstate="collapsed" desc="CreatMop">
    protected boolean createFlowRunAction(FlowTemplates flowTemplates, RiMsRcPlan plan, HashMap<String, HashMap<String, RiMsRcParam>> mapParamList, RiMsRcResult riResult) throws MessageException {

        GenerateFlowRunController generateFlowRunController;
        try {

            generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();
            flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
            flowRunAction.setFlowRunName(("Rescue_Info - MSC_ACT_" + plan.getNodeMscCurrent().getNodeCode() + " - RNC_" + plan.getNodeRnc().getNodeCode() + " - MSC_BAK_" + plan.getNodeMscBackup().getNodeCode() + " - " + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())));
            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplates);
            flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());
            flowRunAction.setMopType(1l);
            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
            //Lay danh sach param tu bang 
            generateFlowRunController.setNodes(new ArrayList<Node>());
            Map<String, RiMsRcParam> mapTableParam;
            Map<String, String> mapParamValues = new HashMap<>();
            generateFlowRunController.loadGroupAction(1l);
            HashMap<String, Node> mapNodeInPlan = new HashMap<>();
            List<Node> nodeInPlan;

            nodeInPlan = new ArrayList<>();
            if (!mapNodeInPlan.containsKey(plan.getNodeRnc().getNodeCode())) {
                generateFlowRunController.getNodes().add(plan.getNodeRnc());
                mapNodeInPlan.put(plan.getNodeRnc().getNodeCode(), plan.getNodeRnc());
            }
            if (!mapNodeInPlan.containsKey(plan.getNodeMscCurrent().getNodeCode())) {
                plan.getNodeMscCurrent().setFlag(Config.SGSN_ACTIVE);
                generateFlowRunController.getNodes().add(plan.getNodeMscCurrent());
                mapNodeInPlan.put(plan.getNodeMscCurrent().getNodeCode(), plan.getNodeMscCurrent());
            }
            if (!mapNodeInPlan.containsKey(plan.getNodeMscBackup().getNodeCode())) {
                plan.getNodeMscBackup().setFlag(Config.SGSN_BACKUP);
                generateFlowRunController.getNodes().add(plan.getNodeMscBackup());
                mapNodeInPlan.put(plan.getNodeMscBackup().getNodeCode(), plan.getNodeMscBackup());
            }
            nodeInPlan.add(plan.getNodeRnc());
            nodeInPlan.add(plan.getNodeMscCurrent());
            nodeInPlan.add(plan.getNodeMscBackup());

            if (plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON) || plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.HUAWEI)) {
                if (mapParamList.containsKey(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeMscCurrent().getNodeType().getTypeName().trim())) {
                    mapTableParam = mapParamList.get(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeMscCurrent().getNodeType().getTypeName().trim());
                } else {
                    mapTableParam = new HashMap<>();
                }
                if (mapTableParam != null) {
                    for (String map : mapTableParam.keySet()) {
                        mapParamValues.put("act" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                        mapParamValues.put("bak" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                    }
                }

                if (mapParamList.containsKey(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeRnc().getNodeType().getTypeName().trim())) {
                    mapTableParam = mapParamList.get(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeRnc().getNodeType().getTypeName().trim());
                } else {
                    mapTableParam = new HashMap<>();
                }
                if (mapTableParam != null) {
                    for (String map : mapTableParam.keySet()) {
                        mapParamValues.put("bak" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                    }
                }

            } else {
                if (mapParamList.containsKey(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeMscCurrent().getNodeType().getTypeName().trim())) {
                    mapTableParam = mapParamList.get(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeMscCurrent().getNodeType().getTypeName().trim());
                } else {
                    mapTableParam = new HashMap<>();
                }
                if (mapTableParam != null) {
                    for (String map : mapTableParam.keySet()) {
                        mapParamValues.put("act" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                        mapParamValues.put("bak" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                    }
                }
            }
            mapParamValues.put("bak" + Constants.paramCode.RNCName.toLowerCase(), plan.getNodeRnc().getNodeCode());
            mapParamValues.put("act" + Constants.paramCode.RNCName.toLowerCase(), plan.getNodeRnc().getNodeCode());
            mapTableParam = mapParamList.get(plan.getNodeMscCurrent().getNodeCode().trim() + "#" + plan.getNodeMscCurrent().getNodeType().getTypeName().trim());
            if (mapTableParam != null) {
                for (String map : mapTableParam.keySet()) {
                    mapParamValues.put("act" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());

                }
            }
            mapParamValues.put("act" + Constants.paramCode.MSCName.toLowerCase(), plan.getNodeMscCurrent().getNodeCode());

            mapTableParam = mapParamList.get(plan.getNodeMscBackup().getNodeCode().trim() + "#" + plan.getNodeMscBackup().getNodeType().getTypeName().trim());
            if (mapTableParam != null) {
                for (String map : mapTableParam.keySet()) {
                    mapParamValues.put("bak" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                }

            }
            mapParamValues.put("bak" + Constants.paramCode.MSCName.toLowerCase(), plan.getNodeMscBackup().getNodeCode());
            setParamFromTableOther(plan, mapParamValues, riResult);
            for (Node node : nodeInPlan) {
                generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                for (ParamValue paramValue : paramValues) {
                    if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                        continue;
                    }
                    Object value = null;
                    try {
                        value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim()));
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                    ResourceBundle bundle = ResourceBundle.getBundle("cas");
                    if (bundle.getString("service").contains("10.61.127.190")) {
                        if (value == null || value.toString().isEmpty()) {
                            value = "TEST_NOT_FOUND";
                        }
                    }
                    if (value != null) {
                        paramValue.setParamValue(value.toString());
                    }
//                    if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                .replace("{1}", paramValue.getParamCode()));
//                        return false;
//                    }
                }
            }
            mapParamValues.clear();

            boolean saveDT = generateFlowRunController.saveDT();
            if (saveDT) {
                try {
                    MessageUtil.setInfoMessageFromRes("Sinh Mop ứng cứu thành công node RNC:" + plan.getNodeRnc().getNodeCode());
//                    currEnodePlan.setFlowRunId(generateFlowRunController.getFlowRunAction().getFlowRunId());
//                    eNodeBService.saveOrUpdate(currEnodePlan);

                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

            }
            return saveDT;
        } catch (Exception e) {
            if (e instanceof MessageException) {
                MessageUtil.setErrorMessage(e.getMessage());
            } else {
                MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                logger.error(e.getMessage(), e);
            }
        } finally {
            //Tam biet
        }

        return false;
    }

    public void setParamFromTableOther(RiMsRcPlan plan, Map<String, String> mapParamValues, RiMsRcResult riResult) throws MessageException {

        try {

            //Lay tham so rnc
            switch (plan.getNodeRnc().getVendor().getVendorName().toUpperCase()) {

                case Constants.vendorType.NOKIA:
                    setParamRncNokia(plan, mapParamValues, riResult);
                    break;
                case Constants.vendorType.ERICSSON:
                    setParamRncEricsson(plan, mapParamValues, riResult);
                    break;
                case Constants.vendorType.HUAWEI:
                    setParamRncHuawei(plan, mapParamValues, riResult);
                    break;
            }

            //Lay cac tham so cho msc 
            switch (plan.getNodeMscBackup().getVendor().getVendorName().toUpperCase()) {
                case Constants.vendorType.HUAWEI:
                    setParamMscBackupHuawei(plan, mapParamValues, riResult);
                    break;
            }
            switch (plan.getNodeMscCurrent().getVendor().getVendorName().toUpperCase()) {
                case Constants.vendorType.HUAWEI:
                    setParamMscCurrentHuawei(plan, mapParamValues, riResult);
                    break;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new MessageException(MessageUtil.getResourceBundleMessage("label.err.create.mop"));
        }
    }

    public void setParamRncNokia(RiMsRcPlan plan, Map<String, String> mapParamValues, RiMsRcResult riResult) throws MessageException {
        List<RiMsRcParamRncNokia> listPrLinkRncNokia;
        Map<String, Object> filters = new HashMap<>();
        try {
            filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
            String RNCIuIPTemp1 = "";
            String RNCIuIPTemp2 = "";
            String temp;
            int countBreak = 0;
            listPrLinkRncNokia = new RiMsRcParamRncNokiaServiceImpl().findList(filters);
            int countIp = listPrLinkRncNokia.size();
            switch (plan.getNodeRnc().getVersion().getVersionName().toUpperCase()) {
                case Constants.versionType.IPA2600:

                    for (RiMsRcParamRncNokia bo : listPrLinkRncNokia) {
                        riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getRncPort());
                        RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIp1();
                        RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIp2();
                        riResult.setAssocciationSetName(riResult.getAssocciationSetName() + ";" + bo.getAssocciationSetName());
                        riResult.setListSignallingLink(bo.getListSignallingLink());
                        riResult.setIcsu(riResult.getIcsu() + ";" + bo.getIcsu());
                        riResult.setCPNPGEP1(bo.getCpnpgepIp1());
                        riResult.setCPNPGEP2(bo.getCpnpgepIp2());
                        countBreak++;
                        if (countBreak >= 4) {
                            break;
                        }
                    }
                    for (RiMsRcParamRncNokia bo : listPrLinkRncNokia) {
                        riResult.setActAssocID(riResult.getActAssocID() + ";" + bo.getAssocId());
                        riResult.setActAssocciationSetName(bo.getAssocciationSetName());
                        riResult.setActSignallingLink(bo.getSignallingLink());
                        riResult.setActSignallingLinkName(bo.getSignallingLinkName());
                        countBreak++;
                    }
                    if (countIp == 1) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setIcsu(riResult.getIcsu() + riResult.getIcsu() + riResult.getIcsu() + riResult.getIcsu());
                    } else if (countIp == 2) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setIcsu(riResult.getIcsu() + riResult.getIcsu());
                    } else {
                        riResult.setRNCIuIP1(RNCIuIPTemp1);
                        riResult.setRNCIuIP2(RNCIuIPTemp2);
                    }
                    for (int i = 10; i > 0; i--) {
                        temp = "IUCS" + String.valueOf(i);
                        if (!riResult.getAssocciationSetName().contains(temp)) {
                            riResult.setAssocciationSetName(temp);
                            break;
                        }
                    }
                    for (int i = 1023; i > 0; i--) {
                        temp = String.valueOf(i);
                        if (!riResult.getListSignallingLink().contains("##" + temp)) {
                            riResult.setSignallingLink(temp);
                            break;
                        }
                    }
                    countBreak = 0;
                    temp = riResult.getRNCPort();
                    riResult.setRNCPort("");
                    for (int i = 8000; i < 9000; i++) {
                        if (!temp.contains(";" + String.valueOf(i))) {
                            riResult.setRNCPort(riResult.getRNCPort() + ";" + String.valueOf(i));
                            countBreak++;
                        }
                        if (countBreak >= 4) {
                            break;
                        }
                    }
//                            if (!riResult.getRNCPort().contains("8000")) {
//                                riResult.setRNCPort("8000;8001;8002;8003");
//                            } else {
//                                riResult.setRNCPort("8010;8011;8012;8013");
//                            }
                    riResult.setAssocID("0;1;2;3");
                    if (plan.getNodeMscBackup().getNodeCode().length() > 5) {
                        riResult.setSignallingLinkName(plan.getNodeMscBackup().getNodeCode().substring(0, 4) + plan.getNodeMscBackup().getNodeCode().charAt(5));
                    }
                    if (riResult.getCPNPGEP1() != null && riResult.getCPNPGEP2() != null) {
                        riResult.setGatewayCPNPGEP(convertIp(riResult.getCPNPGEP1(), 1, 5) + ";" + convertIp(riResult.getCPNPGEP2(), 1, 5));
                    }
                    riResult.setNPGEPIndex("0;2");
                    riResult.setUPNPGEPIndex("0;0;0;0;2;2;2;2");
                    putParamValue("bak" + Constants.paramCode.SignallingLink.toLowerCase(), riResult.getSignallingLink(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.SignallingLinkName.toLowerCase(), riResult.getSignallingLinkName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.AssocciationSetName.toLowerCase(), riResult.getAssocciationSetName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.icsu.toLowerCase(), riResult.getIcsu(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.AssocID.toLowerCase(), riResult.getAssocID(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.SignallingLink.toLowerCase(), riResult.getActSignallingLink(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.AssocID.toLowerCase(), riResult.getActAssocID(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.SignallingLinkName.toLowerCase(), riResult.getActSignallingLinkName(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.AssocciationSetName.toLowerCase(), riResult.getActAssocciationSetName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.CPNPGEP1.toLowerCase(), riResult.getCPNPGEP1(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.CPNPGEP2.toLowerCase(), riResult.getCPNPGEP2(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.GatewayCPNPGEP.toLowerCase(), riResult.getGatewayCPNPGEP(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.NPGEPIndex.toLowerCase(), riResult.getNPGEPIndex(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.UPNPGEPIndex.toLowerCase(), riResult.getUPNPGEPIndex(), mapParamValues);
                    break;
                case Constants.versionType.mcRNC:
                    for (RiMsRcParamRncNokia bo : listPrLinkRncNokia) {
                        countBreak++;
                        RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIp1();
                        RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIp2();
                        riResult.setGateway1(riResult.getGateway1() + ";" + convertIp(bo.getRncIp1(), 1, 1));
                        if (countBreak == 2 || countBreak == 4) {
                            riResult.setGateway2(riResult.getGateway2() + ";" + convertIp(bo.getRncIp2(), 1, -1));
                        } else {
                            riResult.setGateway2(riResult.getGateway2() + ";" + convertIp(bo.getRncIp2(), 1, 1));
                        }
                        riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getRncPort());
                        riResult.setListRemoteAsId(bo.getListRemoteAsId());
                        riResult.setListAssocId(riResult.getListAssocId() + ";" + bo.getListAssocId());
                        riResult.setListPointCodeId(riResult.getListPointCodeId() + ";" + bo.getListPointCodeId());
                        riResult.setListSubSystemId(riResult.getListSubSystemId() + ";" + bo.getListSubSystemId());
                        riResult.setLocalAsName(riResult.getLocalAsName() + ";" + bo.getLocalAsName());
                        riResult.setLocalClientPort(riResult.getLocalClientPort() + ";" + bo.getLocalClientPort());
                        if (bo.getNodeAssoc().startsWith("/")) {
                            bo.setNodeAssoc(bo.getNodeAssoc().substring(1));
                        }
                        riResult.setNodeAssoc(riResult.getNodeAssoc() + ";" + bo.getNodeAssoc());
                        riResult.setIpbrId(bo.getIpbrId());
                        if (countBreak >= 4) {
                            break;
                        }
                    }
                    for (RiMsRcParamRncNokia bo : listPrLinkRncNokia) {
                        riResult.setActAssocID(riResult.getActAssocID() + ";" + bo.getAssocId());
                        riResult.setActSubSystemName(bo.getSubSystemName());
                        riResult.setActConcernSsnName(bo.getConcernSsnName());
                        riResult.setActRemoteAsName(bo.getRemoteAsName());
                        riResult.setActConcernPointCodeName(bo.getConcernPointCodeName());
                    }
                    if (countIp == 1) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setLocalAsName(riResult.getLocalAsName() + riResult.getLocalAsName() + riResult.getLocalAsName() + riResult.getLocalAsName());
                        riResult.setLocalClientPort(riResult.getLocalClientPort() + riResult.getLocalClientPort() + riResult.getLocalClientPort() + riResult.getLocalClientPort());
                        riResult.setRNCPort(riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort());
                        riResult.setNodeAssoc(riResult.getNodeAssoc() + riResult.getNodeAssoc() + riResult.getNodeAssoc() + riResult.getNodeAssoc());
                        riResult.setGateway1(riResult.getGateway1() + riResult.getGateway1() + riResult.getGateway1() + riResult.getGateway1());
                        riResult.setGateway2(riResult.getGateway2() + riResult.getGateway2() + riResult.getGateway2() + riResult.getGateway2());
                    } else if (countIp == 2) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setLocalAsName(riResult.getLocalAsName() + riResult.getLocalAsName());
                        riResult.setLocalClientPort(riResult.getLocalClientPort() + riResult.getLocalClientPort());
                        riResult.setRNCPort(riResult.getRNCPort() + riResult.getRNCPort());
                        riResult.setNodeAssoc(riResult.getNodeAssoc() + riResult.getNodeAssoc());
                        riResult.setGateway1(riResult.getGateway1() + riResult.getGateway1());
                        riResult.setGateway2(riResult.getGateway2() + riResult.getGateway2());
                    } else {
                        riResult.setRNCIuIP1(RNCIuIPTemp1);
                        riResult.setRNCIuIP2(RNCIuIPTemp2);
                    }
                    countBreak = 0;
                    for (int i = 65535; i > 0; i--) {
                        if (!riResult.getListRemoteAsId().equals("##" + i)) {
                            countBreak++;
                            riResult.setRemoteAsId(String.valueOf(i));
                            break;
                        }
                    }

                    for (int i = 65535; i > 0; i--) {
                        if (!riResult.getListAssocId().equals("" + i)) {
                            countBreak++;
                            riResult.setAssocID(riResult.getAssocID() + ";" + String.valueOf(i));
                        }
                        if (countBreak > 4) {
                            break;
                        }
                    }
                    for (int i = 65535; i > 0; i--) {
                        if (!riResult.getListPointCodeId().equals(";" + i)) {
                            riResult.setPointCodeId(String.valueOf(i));
                            break;
                        }
                    }
                    for (int i = 65535; i > 0; i--) {
                        if (!riResult.getListSubSystemId().equals(";" + i)) {
                            riResult.setSubSystemId(String.valueOf(i));
                            break;
                        }
                    }
                    riResult.setRemoteAsName("RA_" + plan.getNodeMscBackup().getNodeCode());
                    riResult.setSubSystemName("SS_" + plan.getNodeMscBackup().getNodeCode());
                    riResult.setPointCodeName("RA_" + plan.getNodeMscBackup().getNodeCode());
                    riResult.setConcernSsnName("CS_" + plan.getNodeMscBackup().getNodeCode());
                    riResult.setAffectSsnName("SS_" + plan.getNodeMscBackup().getNodeCode());
                    riResult.setConcernPointCodeName("CP_" + plan.getNodeMscBackup().getNodeCode());
                    riResult.setIpbrName("CSUP-" + plan.getNodeMscBackup().getNodeCode());

                    putParamValue("bak" + Constants.paramCode.AssocID.toLowerCase(), riResult.getAssocID(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.RemoteAsID.toLowerCase(), riResult.getRemoteAsId(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.RemoteAsName.toLowerCase(), riResult.getRemoteAsName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.local_as_name.toLowerCase(), riResult.getLocalAsName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.local_client_port.toLowerCase(), riResult.getLocalClientPort(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.local_server_port.toLowerCase(), riResult.getRNCPort(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.NodeAssoc.toLowerCase(), riResult.getNodeAssoc(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.PointCodeId.toLowerCase(), riResult.getPointCodeId(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.PointCodeName.toLowerCase(), riResult.getPointCodeName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.SubSystemID.toLowerCase(), riResult.getSubSystemId(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.SubSystemName.toLowerCase(), riResult.getSubSystemName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.ConcernSSNName.toLowerCase(), riResult.getConcernSsnName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.AffectSSNName.toLowerCase(), riResult.getAffectSsnName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.ConcernPointCodeName.toLowerCase(), riResult.getConcernPointCodeName(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.gateway1.toLowerCase(), riResult.getGateway1(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.gateway2.toLowerCase(), riResult.getGateway2(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.ipbr_id.toLowerCase(), riResult.getIpbrId(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.ipbr_name.toLowerCase(), riResult.getIpbrName(), mapParamValues);

                    putParamValue("act" + Constants.paramCode.AssocID.toLowerCase(), riResult.getActAssocID(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.ConcernPointCodeName.toLowerCase(), riResult.getActConcernPointCodeName(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.ConcernSSNName.toLowerCase(), riResult.getActConcernSsnName(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.SubSystemName.toLowerCase(), riResult.getActSubSystemName(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.RemoteAsName.toLowerCase(), riResult.getActRemoteAsName(), mapParamValues);
                    break;
            }
            countBreak = 0;
            if (mapParamValues.containsKey("bak" + Constants.paramCode.MSCPORTALL.toLowerCase())) {
                for (int i = 8000; i < 9000; i++) {
                    if (!mapParamValues.get("bak" + Constants.paramCode.MSCPORTALL.toLowerCase()).contains(";" + String.valueOf(i))) {
                        riResult.setMSCPort(riResult.getMSCPort() + ";" + String.valueOf(i));
                        countBreak++;
                    }
                    if (countBreak >= 4) {
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new MessageException(MessageUtil.getResourceBundleMessage("label.err.create.mop"));
        }
    }

    public void setParamRncEricsson(RiMsRcPlan plan, Map<String, String> mapParamValues, RiMsRcResult riResult) throws MessageException {
        List<RiMsRcParamRncEricsson> listPrLinkRncEricsson;
        Map<String, Object> filters = new HashMap<>();
        try {
            filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
            String RNCIuIPTemp1 = "";
            String RNCIuIPTemp2 = "";
            int countBreak = 0;
            listPrLinkRncEricsson = new RiMsRcParamRncEricssonServiceImpl().findList(filters);
            int countIp = listPrLinkRncEricsson.size();

            for (RiMsRcParamRncEricsson bo : listPrLinkRncEricsson) {
                riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getRncPort());
                RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIuIp1();
                RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIuIp2();
                if (!riResult.getSctpid().contains(bo.getSctpId())) {
                    riResult.setSctpid(riResult.getSctpid() + ";" + bo.getSctpId());
                }
                if (!riResult.getIpAccessHostPool().contains(bo.getIpAccessHostPool())) {
                    riResult.setIpAccessHostPool(riResult.getIpAccessHostPool() + ";" + bo.getIpAccessHostPool());
                }
                if (!riResult.getSccpApLocal().contains(bo.getSccpApLocal())) {
                    riResult.setSccpApLocal(riResult.getSccpApLocal() + ";" + bo.getSccpApLocal());
                }
                riResult.setDscp(bo.getDscp());
                countBreak++;
                if (countBreak >= 4) {
                    break;
                }
            }
            for (RiMsRcParamRncEricsson bo : listPrLinkRncEricsson) {
                riResult.setActIndexM3ua(bo.getIndexM3ua());
                riResult.setActDscp(bo.getDscp());
            }
            if (countIp == 1) {
                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                riResult.setRNCPort(riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort());

            } else if (countIp == 2) {
                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                riResult.setRNCPort("2905;2906;2906;2905");

            } else {
                riResult.setRNCIuIP1(RNCIuIPTemp1);
                riResult.setRNCIuIP2(RNCIuIPTemp2);
            }
            riResult.setIndex("_1;_2;_3;_4");
            putParamValue("bak" + Constants.paramCode.index.toLowerCase(), riResult.getIndex(), mapParamValues);
            putParamValue("act" + Constants.paramCode.index.toLowerCase(), riResult.getActIndexM3ua(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.sctpid.toLowerCase(), riResult.getSctpid(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.IpAccessHostPool.toLowerCase(), riResult.getIpAccessHostPool(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SccpApLocal.toLowerCase(), riResult.getSccpApLocal(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.dscp.toLowerCase(), riResult.getDscp(), mapParamValues);
            putParamValue("act" + Constants.paramCode.dscp.toLowerCase(), riResult.getDscp(), mapParamValues);
            countBreak = 0;
            if (mapParamValues.containsKey("bak" + Constants.paramCode.MSCPORTALL.toLowerCase())) {
                for (int i = 9000; i < 10000; i++) {
                    if (!mapParamValues.get("bak" + Constants.paramCode.MSCPORTALL.toLowerCase()).contains(";" + String.valueOf(i))) {
                        riResult.setMSCPort(riResult.getMSCPort() + ";" + String.valueOf(i));
                        countBreak++;
                    }
                    if (countBreak >= 4) {
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new MessageException(MessageUtil.getResourceBundleMessage("label.err.create.mop"));
        }
    }

    public void setParamRncHuawei(RiMsRcPlan plan, Map<String, String> mapParamValues, RiMsRcResult riResult) throws MessageException {
        List<RiMsRcParamRncHuawei> listPrLinkRncHuawei;
        Map<String, Object> filters = new HashMap<>();
        Map<String, String> orders;
        orders = new LinkedHashMap<>();
        orders.put("id", "ASC");
        try {
            filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
            String RNCIuIPTemp1 = "";
            String RNCIuIPTemp2 = "";
            int countBreak = 0;
            listPrLinkRncHuawei = new RiMsRcParamRncHuaweiServiceImpl().findList(filters, orders);
            int countIp = listPrLinkRncHuawei.size();
            for (RiMsRcParamRncHuawei bo : listPrLinkRncHuawei) {
                riResult.setSubrack(riResult.getSubrack() + ";" + bo.getSubrack());
                riResult.setSlotCard(riResult.getSlotCard() + ";" + bo.getSlotCard());
                riResult.setSctplink(riResult.getSctplink() + ";" + bo.getSctpLink());
                riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getRncPort());
                RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIp1();
                RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIp2();
                riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + bo.getSignallingLinkId());
                riResult.setSignallingLinkSetID(bo.getSignallingLinkSetId());
                riResult.setDSPIndex(bo.getDspIndex());
                riResult.setCNOperatorID(bo.getCnOperatorId());
                riResult.setCNNodeID(bo.getCnNodeId());
                riResult.setAdjacentNodeID(bo.getAdjacentNodeId());
                riResult.setDestinationEntity(bo.getDestinationEntity());
                riResult.setIpPathId(bo.getIpPath());
                riResult.setRncUserIp(bo.getGouSip());
                riResult.setGOUSubrack(bo.getGouSubrack());
                riResult.setGOUSlot(bo.getGouSlot());

                String[] gouSlotDetail = bo.getGouSlotDetail().split(";");
                String[] subrackDetail = bo.getGouSubrackDetail().split(";");
                if (plan.getNodeRnc().getNodeCode().toUpperCase().contains("RCHT")) {
                    if (!riResult.getNexthopIP().contains(convertIp(bo.getRncIp1(), 1, -1))) {
                        if (gouSlotDetail.length > 1 && subrackDetail.length > 1) {
                            if (Long.valueOf(gouSlotDetail[0]) % 2 == 0) {
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp1(), 1, -1));
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp2(), 1, -1));
                                riResult.setGOUSubrackDetail(riResult.getGOUSubrackDetail() + ";" + subrackDetail[0] + ";" + subrackDetail[1]);
                                riResult.setGOUSlotDetail(riResult.getGOUSlotDetail() + ";" + gouSlotDetail[0] + ";" + String.valueOf(Long.valueOf(gouSlotDetail[1]) - 1));
                            } else {
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp2(), 1, -1));
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp1(), 1, -1));
                                riResult.setGOUSubrackDetail(riResult.getGOUSubrackDetail() + ";" + subrackDetail[1] + ";" + subrackDetail[0]);
                                riResult.setGOUSlotDetail(riResult.getGOUSlotDetail() + ";" + String.valueOf(Long.valueOf(gouSlotDetail[0]) - 1) + ";" + gouSlotDetail[1]);
                            }
                            riResult.setPRIORITY(riResult.getPRIORITY() + ";" + "HIGH;LOW");
                        }
                    }
                } else {
                    if (!riResult.getNexthopIP().contains(convertIp(bo.getRncIp1(), 1, 1))) {
                        if (gouSlotDetail.length > 1) {
                            if (Long.valueOf(gouSlotDetail[0]) % 2 == 0) {
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp1(), 1, 1));
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp2(), 1, 1));
                                riResult.setGOUSubrackDetail(riResult.getGOUSubrackDetail() + ";" + bo.getGouSubrackDetail());
                                riResult.setGOUSlotDetail(riResult.getGOUSlotDetail() + ";" + gouSlotDetail[0] + ";" + String.valueOf(Long.valueOf(gouSlotDetail[1]) - 1));
                            } else {
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp2(), 1, 1));
                                riResult.setNexthopIP(riResult.getNexthopIP() + ";" + convertIp(bo.getRncIp1(), 1, 1));
                                riResult.setGOUSubrackDetail(riResult.getGOUSubrackDetail() + ";" + bo.getGouSubrackDetail());
                                riResult.setGOUSlotDetail(riResult.getGOUSlotDetail() + ";" + String.valueOf(Long.valueOf(gouSlotDetail[0]) - 1) + ";" + gouSlotDetail[1]);
                            }
                            riResult.setPRIORITY(riResult.getPRIORITY() + ";" + "HIGH;LOW");
                        }
                    }
                }

                countBreak++;
                if (countBreak > 4) {
                    break;
                }

            }
            for (RiMsRcParamRncHuawei bo : listPrLinkRncHuawei) {
                riResult.setActSignallingLinkId(riResult.getActSignallingLinkId() + ";" + bo.getSignallingLinkId());
                riResult.setActSignallingLinkSetID(bo.getSignallingLinkSetId());
                riResult.setActDSPIndex(bo.getDspIndex());
                riResult.setActCNOperatorID(bo.getCnOperatorId());
                riResult.setActCNNodeID(bo.getCnNodeId());
                riResult.setActAdjacentNodeID(bo.getAdjacentNodeId());
                riResult.setActDestinationEntity(bo.getDestinationEntity());

            }
            String temp = riResult.getSignallingLinkId();
            riResult.setSignallingLinkId("");
            riResult.setSctplink("");
            countBreak = 0;
            switch (plan.getNodeRnc().getVersion().getVersionName().toUpperCase()) {
                case Constants.versionType.HUAWEI_6900:
                    for (int i = 15; i > 0; i--) {
                        if (!temp.contains(";" + String.valueOf(i))) {
                            riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + String.valueOf(i));
                            riResult.setSctplink(riResult.getSctplink() + ";" + String.valueOf(i + 1000));
                            countBreak++;
                            if (countBreak >= 4) {
                                break;
                            }
                        }
                    }
                    break;
                case Constants.versionType.HUAWEI_6910:
                    for (int i = 15; i > 0; i--) {
                        if (!temp.contains(";" + String.valueOf(i))) {
                            riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + String.valueOf(i));
                            riResult.setSctplink(riResult.getSctplink() + ";" + String.valueOf(i + 10000));
                            countBreak++;
                            if (countBreak >= 4) {
                                break;
                            }
                        }
                    }
                    break;
            }

            if (countIp == 1) {
                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                riResult.setSubrack(riResult.getSubrack() + ";" + riResult.getSubrack() + ";" + riResult.getSubrack() + ";" + riResult.getSubrack());
                riResult.setSlotCard(riResult.getSlotCard() + ";" + riResult.getSlotCard() + ";" + riResult.getSlotCard() + ";" + riResult.getSlotCard());
                riResult.setGOUSubrackDetail(riResult.getGOUSubrackDetail() + ";" + riResult.getGOUSubrackDetail() + ";" + riResult.getGOUSubrackDetail() + ";" + riResult.getGOUSubrackDetail());
                riResult.setGOUSlotDetail(riResult.getGOUSlotDetail() + ";" + riResult.getGOUSlotDetail() + ";" + riResult.getGOUSlotDetail() + ";" + riResult.getGOUSlotDetail());

                riResult.setRNCPort("2905;2906;2906;2905");
            } else if (countIp == 2) {
                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                riResult.setSubrack(riResult.getSubrack() + ";" + riResult.getSubrack());
                riResult.setSlotCard(riResult.getSlotCard() + ";" + riResult.getSlotCard());

                riResult.setGOUSubrackDetail(riResult.getGOUSubrackDetail() + ";" + riResult.getGOUSubrackDetail());
                riResult.setGOUSlotDetail(riResult.getGOUSlotDetail() + ";" + riResult.getGOUSlotDetail());

                riResult.setRNCPort("2905;2906;2906;2905");
            } else {
                riResult.setRNCIuIP1(RNCIuIPTemp1);
                riResult.setRNCIuIP2(RNCIuIPTemp2);
            }
            putParamValue("bak" + Constants.paramCode.Subrack.toLowerCase(), riResult.getSubrack(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.Slot.toLowerCase(), riResult.getSlotCard(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.Sctplink.toLowerCase(), riResult.getSctplink(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SignallingLinkSetID.toLowerCase(), riResult.getSignallingLinkSetID(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SignallingLinkID.toLowerCase(), riResult.getSignallingLinkId(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.DSPIndex.toLowerCase(), riResult.getDSPIndex(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.CNOperatorID.toLowerCase(), riResult.getCNOperatorID(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.CNNodeID.toLowerCase(), riResult.getCNNodeID(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.AdjacentNodeID.toLowerCase(), riResult.getAdjacentNodeID(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.pathid.toLowerCase(), riResult.getIpPathId(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCUserIP.toLowerCase(), riResult.getRncUserIp(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.GOUSubrack.toLowerCase(), riResult.getGOUSubrack(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.GOUSlot.toLowerCase(), riResult.getGOUSlot(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.DestinationEntityNo.toLowerCase(), riResult.getDestinationEntity(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.GOUSubrackDetail.toLowerCase(), riResult.getGOUSubrackDetail(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.GOUSlotDetail.toLowerCase(), riResult.getGOUSlotDetail(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.NexthopIP.toLowerCase(), riResult.getNexthopIP(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.PRIORITY.toLowerCase(), riResult.getPRIORITY(), mapParamValues);

            putParamValue("act" + Constants.paramCode.SignallingLinkSetID.toLowerCase(), riResult.getActSignallingLinkSetID(), mapParamValues);
            putParamValue("act" + Constants.paramCode.SignallingLinkID.toLowerCase(), riResult.getActSignallingLinkId(), mapParamValues);
            putParamValue("act" + Constants.paramCode.DSPIndex.toLowerCase(), riResult.getActDSPIndex(), mapParamValues);
            putParamValue("act" + Constants.paramCode.CNOperatorID.toLowerCase(), riResult.getActCNOperatorID(), mapParamValues);
            putParamValue("act" + Constants.paramCode.CNNodeID.toLowerCase(), riResult.getActCNNodeID(), mapParamValues);
            putParamValue("act" + Constants.paramCode.AdjacentNodeID.toLowerCase(), riResult.getActAdjacentNodeID(), mapParamValues);
            putParamValue("act" + Constants.paramCode.DestinationEntityNo.toLowerCase(), riResult.getActDestinationEntity(), mapParamValues);

            countBreak = 0;
            if (mapParamValues.containsKey("bak" + Constants.paramCode.MSCPORTALL.toLowerCase())) {
                for (int i = 7000; i < 8000; i++) {
                    if (!mapParamValues.get("bak" + Constants.paramCode.MSCPORTALL.toLowerCase()).contains(";" + String.valueOf(i))) {
                        riResult.setMSCPort(riResult.getMSCPort() + ";" + String.valueOf(i));
                        countBreak++;
                    }
                    if (countBreak >= 4) {
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new MessageException(MessageUtil.getResourceBundleMessage("label.err.create.mop"));
        }
    }

    public void setParamMscBackupHuawei(RiMsRcPlan plan, Map<String, String> mapParamValues, RiMsRcResult riResult) throws MessageException {
        List<RiMsRcParamLinkMsc> listPrLinkMsc;
        List<RiMsRcParamLacSac> listPrLacRac;
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("nodeCode", plan.getNodeMscBackup().getNodeCode());
            String MscIuIPTemp1 = "";
            String MscIuIPTemp2 = "";
            int countBreak = 0;

            listPrLinkMsc = new RiMsRcParamLinkMscServiceImpl().findList(filters);

            int countIp = listPrLinkMsc.size();
            for (RiMsRcParamLinkMsc prLinkMsc : listPrLinkMsc) {
                MscIuIPTemp1 = MscIuIPTemp1 + ";" + prLinkMsc.getIpOne();
                MscIuIPTemp2 = MscIuIPTemp2 + ";" + prLinkMsc.getIpTwo();
                countBreak++;
                if (countBreak >= 4) {
                    break;
                }
            }
            for (RiMsRcParamLinkMsc prLinkMsc : listPrLinkMsc) {
                riResult.setMscIuIP(convertIp(prLinkMsc.getIpOne(), 0, 0));
                break;
            }
            if (countIp == 1) {
                riResult.setMSCIuIP1(MscIuIPTemp1 + MscIuIPTemp2 + MscIuIPTemp1 + MscIuIPTemp2);
                riResult.setMSCIuIP2(MscIuIPTemp2 + MscIuIPTemp1 + MscIuIPTemp2 + MscIuIPTemp1);
            } else if (countIp == 2) {
                riResult.setMSCIuIP1(MscIuIPTemp1 + MscIuIPTemp2);
                riResult.setMSCIuIP2(MscIuIPTemp2 + MscIuIPTemp1);
            } else {
                riResult.setMSCIuIP1(MscIuIPTemp1);
                riResult.setMSCIuIP2(MscIuIPTemp2);
            }

            //Put lac sac
            //Voi khu vuc 3 thi lac sac lay tren MSC
            if (plan.getNodeMscBackup().getNodeCode().contains("MSHT") || plan.getNodeMscBackup().getNodeCode().contains("MSPL")) {
                filters.clear();
                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                filters.put("mscActive", plan.getNodeMscCurrent().getNodeCode());
                listPrLacRac = new RiMsRcParamLacSacServiceImpl().findList(filters);
                HashMap<String, RiMsRcParamLacSac> mapLac = new HashMap<>();
                HashMap<String, RiMsRcParamLacSac> mapSac = new HashMap<>();
                for (RiMsRcParamLacSac bo : listPrLacRac) {
                    mapLac.put(bo.getLac(), bo);
                    mapSac.put(bo.getSac(), bo);
                }

                for (String lac : mapLac.keySet()) {
                    riResult.setActRNCLAI(riResult.getActRNCLAI() + ";" + mapLac.get(lac).getLac());
                    riResult.setRNCProvinceLai(riResult.getRNCProvinceLai() + ";" + (mapLac.get(lac).getProvinceLac() != null ? mapLac.get(lac).getProvinceLac() : ""));
                    if (mapLac.get(lac).getLac().length() > 4) {
                        riResult.setRNCLac(riResult.getRNCLac() + ";" + hexToDecimal(mapLac.get(lac).getLac().substring(mapLac.get(lac).getLac().length() - 4)));
                    } else {
                        riResult.setRNCLac(riResult.getRNCLac() + ";" + "");
                    }
                }
                for (String sac : mapSac.keySet()) {
                    riResult.setRNCSAI(riResult.getActRNCSAI() + ";" + sac);
                    riResult.setActRNCSAI(riResult.getRNCSAI() + ";" + sac);
                    riResult.setRNCProvinceSai(riResult.getRNCProvinceSai() + ";" + (mapSac.get(sac).getProvinceSac() != null ? mapSac.get(sac).getProvinceSac() : ""));
                    riResult.setRNCSAIName(riResult.getRNCSAIName() + ";" + mapSac.get(sac).getCellName());
                }
                if (plan.getNodeMscBackup().getNodeCode().contains("MSHT") || plan.getNodeMscBackup().getNodeCode().contains("MSPL")) {
                    for (String sac : mapSac.keySet()) {
                        riResult.setLocationName(riResult.getLocationName() + ";" + mapSac.get(sac).getLocationName());
                    }
                } else {
                    riResult.setLocationName("INVALID");
                }
            } else {
                //Lay tham so act lai sai tren msc
                HashMap<String, RiMsRcParamLacSac> mapLac = new HashMap<>();
                HashMap<String, RiMsRcParamLacSac> mapSac = new HashMap<>();
                filters.clear();
                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                filters.put("mscActive", plan.getNodeMscCurrent().getNodeCode());
                listPrLacRac = new RiMsRcParamLacSacServiceImpl().findList(filters);

                for (RiMsRcParamLacSac bo : listPrLacRac) {
                    mapLac.put(bo.getLac(), bo);
                    mapSac.put(bo.getSac(), bo);
                }

                for (String lac : mapLac.keySet()) {
                    riResult.setActRNCLAI(riResult.getActRNCLAI() + ";" + mapLac.get(lac).getLac());
                }
                for (String sac : mapSac.keySet()) {
                    riResult.setActRNCSAI(riResult.getActRNCSAI() + ";" + sac);
                }

                //Lay tham so lac sac bak tu RNC
                HashMap<String, RiMsRcParamLacSac> mapLacRnc = new HashMap<>();
                HashMap<String, RiMsRcParamLacSac> mapSacRnc = new HashMap<>();
                filters.clear();
                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                filters.put("mscActive", plan.getNodeRnc().getNodeCode());
                listPrLacRac = new RiMsRcParamLacSacServiceImpl().findList(filters);

                for (RiMsRcParamLacSac bo : listPrLacRac) {
                    mapLacRnc.put(bo.getLac(), bo);
                    mapSacRnc.put(bo.getSac(), bo);
                }

                for (String lac : mapLacRnc.keySet()) {
                    if (mapLacRnc.get(lac).getProvinceLac() != null && !"TEST".equalsIgnoreCase(mapLacRnc.get(lac).getProvinceLac())) {
                        riResult.setRNCLAI(riResult.getRNCLAI() + ";" + mapLac.get(lac).getLac());
                        riResult.setRNCProvinceLai(riResult.getRNCProvinceLai() + ";" + mapLac.get(lac).getProvinceLac());
                        if (mapLacRnc.get(lac).getLac().length() > 4) {
                            riResult.setRNCLac(riResult.getRNCLac() + ";" + hexToDecimal(mapLacRnc.get(lac).getLac().substring(mapLacRnc.get(lac).getLac().length() - 4)));
                        } else {
                            riResult.setRNCLac(riResult.getRNCLac() + ";" + "");
                        }
                    }
                }
                for (String sac : mapSacRnc.keySet()) {
                    if (mapSacRnc.get(sac).getProvinceSac() != null && !"TEST".equalsIgnoreCase(mapSacRnc.get(sac).getProvinceSac())) {
                        riResult.setRNCSAI(riResult.getRNCSAI() + ";" + sac);
                        riResult.setRNCProvinceSai(riResult.getRNCProvinceSai() + ";" + (mapSacRnc.get(sac).getProvinceSac() == null ? "" : mapSacRnc.get(sac).getProvinceSac()));
                        riResult.setRNCSAIName(riResult.getRNCSAIName() + ";" + mapSacRnc.get(sac).getCellName());
                    }
                }

                riResult.setLocationName("INVALID");

//                //Lay tham so act lai sai tren msc
//                filters.clear();
//                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
//                filters.put("mscActive", plan.getNodeMscCurrent().getNodeCode());
//                listPrLacRac = new RiMsRcParamLacSacServiceImpl().findList(filters);
//                mapLac = new HashMap<>();
//                mapSac = new HashMap<>();
//                for (RiMsRcParamLacSac bo : listPrLacRac) {
//                    mapLac.put(bo.getLac(), bo);
//                    mapSac.put(bo.getSac(), bo);
//                }
//
//                for (String lac : mapLac.keySet()) {
//                    riResult.setActRNCLAI(riResult.getActRNCLAI() + ";" + mapLac.get(lac).getLac());
//                }
//                for (String sac : mapSac.keySet()) {
//                    riResult.setActRNCSAI(riResult.getActRNCSAI() + ";" + sac);
//                }
            }
            // Lay tham so NMSCGT
            if (mapParamValues.containsKey("bak" + Constants.paramCodeMsc.RNCID.toLowerCase())) {
                String NRNCID = mapParamValues.get("bak" + Constants.paramCodeMsc.RNCID.toLowerCase());
                putParamValue("bak" + Constants.paramCodeMsc.NRNCID.toLowerCase(), NRNCID, mapParamValues);
                if (mapParamValues.containsKey("bak" + Constants.paramCodeMsc.NRNCID_NMSCGT.toLowerCase() + "_" + NRNCID)) {
                    putParamValue("bak" + Constants.paramCodeMsc.NMSCGT.toLowerCase(), mapParamValues.get("bak" + Constants.paramCodeMsc.NRNCID_NMSCGT.toLowerCase() + "_" + NRNCID), mapParamValues);
                }
            }

            riResult.setIndex("_1;_2;_3;_4");
            riResult.setNumber("1;2;3;4");

            putParamValue("bak" + Constants.paramCode.index.toLowerCase(), riResult.getIndex(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.number.toLowerCase(), riResult.getNumber(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCPort.toLowerCase(), riResult.getRNCPort(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCIP1.toLowerCase(), riResult.getRNCIuIP1(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCIP2.toLowerCase(), riResult.getRNCIuIP2(), mapParamValues);

            putParamValue("bak" + Constants.paramCode.MSCIP1.toLowerCase(), riResult.getMSCIuIP1(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.MSCIP2.toLowerCase(), riResult.getMSCIuIP2(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.MSCIP.toLowerCase(), riResult.getMscIuIP(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.MSCPort.toLowerCase(), riResult.getMSCPort(), mapParamValues);
            if (mapParamValues.containsKey("bak" + Constants.paramCode.RncSPC.toLowerCase())) {
                putParamValue("bak" + Constants.paramCode.RNCBillOFN.toLowerCase(), hexToDecimal(getRncSpc(mapParamValues.get("bak" + Constants.paramCode.RncSPC.toLowerCase()))), mapParamValues);
            }
            putParamValue("bak" + Constants.paramCode.RNCLAI.toLowerCase(), riResult.getRNCLAI(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCLAC.toLowerCase(), riResult.getRNCLac(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCSAI.toLowerCase(), riResult.getRNCSAI(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCProvinceLai.toLowerCase(), riResult.getRNCProvinceLai(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCProvinceSai.toLowerCase(), riResult.getRNCProvinceSai(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCSAIName.toLowerCase(), riResult.getRNCSAIName(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.LocationName.toLowerCase(), riResult.getLocationName(), mapParamValues);

            putParamValue("act" + Constants.paramCode.RNCLAI.toLowerCase(), riResult.getActRNCLAI(), mapParamValues);
            putParamValue("act" + Constants.paramCode.RNCSAI.toLowerCase(), riResult.getActRNCSAI(), mapParamValues);

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new MessageException(MessageUtil.getResourceBundleMessage("label.err.create.mop"));
        }
    }

    public void setParamMscCurrentHuawei(RiMsRcPlan plan, Map<String, String> mapParamValues, RiMsRcResult riResult) throws MessageException {
        List<RiMsRcParamMscRnc> listPrMscRnc;
        try {
            Map<String, Object> filters = new HashMap<>();
            //Gan tham so cho RNC ERRICSSON
            // Lay tham so tu bang ri_param_sgsn_rnc cho viec delete and rolback DITY
            filters.put("mscCode", plan.getNodeMscCurrent().getNodeCode());
            filters.put("rncCode", plan.getNodeRnc().getNodeCode());
            listPrMscRnc = new RiMsRcParamMscRncServiceImpl().findList(filters);
            for (RiMsRcParamMscRnc bo : listPrMscRnc) {
                riResult.setActRNCSCCP(bo.getRncSccp());
                riResult.setActRNCSSN(bo.getRncSsn());
                riResult.setActRNCDSP(bo.getRncDsp());
                riResult.setActRNCOFC(bo.getRncOfc());
                riResult.setActRNCLinkName(riResult.getActRNCLinkName() + ";" + bo.getRncLinkName());
                riResult.setActRNCRT(bo.getRncRt());
                riResult.setActRNCLinkSet(bo.getRncLinkSet());
            }

            putParamValue("act" + Constants.paramCode.RNCSCCP.toLowerCase(), riResult.getActRNCSCCP(), mapParamValues);
            putParamValue("act" + Constants.paramCode.RNCSSN.toLowerCase(), riResult.getActRNCSSN(), mapParamValues);
            putParamValue("act" + Constants.paramCode.RNCDSP.toLowerCase(), riResult.getActRNCDSP(), mapParamValues);
            putParamValue("act" + Constants.paramCode.RNCOFC.toLowerCase(), riResult.getActRNCOFC(), mapParamValues);
            putParamValue("act" + Constants.paramCode.RNCLinkName.toLowerCase(), riResult.getActRNCLinkName(), mapParamValues);
            putParamValue("act" + Constants.paramCode.RNCRT.toLowerCase(), riResult.getActRNCRT(), mapParamValues);
            putParamValue("act" + Constants.paramCode.RNCLinkSet.toLowerCase(), riResult.getActRNCLinkSet(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCLinkName.toLowerCase(), riResult.getActRNCLinkName(), mapParamValues);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new MessageException(MessageUtil.getResourceBundleMessage("label.err.create.mop"));
        }
    }

    public void putParamValue(String paramCode, String paramValue, Map<String, String> mapParamValues) {
        try {
            if (paramValue != null) {
                while (paramValue.startsWith(";")) {
                    paramValue = paramValue.substring(1);
                }
                mapParamValues.put(paramCode, paramValue.trim());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public static String getRncSpc(String RNCSPC) {
        RNCSPC = RNCSPC.substring(2);
        for (int i = 0; i < RNCSPC.length(); i++) {

            if (RNCSPC.startsWith("0")) {
                RNCSPC = RNCSPC.substring(1);
            } else {
                break;
            }
        }
        return RNCSPC;
    }

    public String convertIp(String ip, int type, int i) {
        String result;
        try {
            if (ip != null && !"".equals(ip)) {
                String[] str = ip.split("\\.");
                if (type == 0) {
                    result = str[0] + "." + str[1] + "." + str[2] + "." + "0";
                } else if (type == 1) {
                    result = str[0] + "." + str[1] + "." + str[2] + "." + String.valueOf(Long.valueOf(str[3]) + i);
                } else {
                    result = ip;
                }
            } else {
                return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return ip;
        }
        return result;
    }

    public static String hexToDecimal(String s) {
        int val = 0;
        try {
            String digits = "0123456789ABCDEF";
            s = s.toUpperCase();

            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                int d = digits.indexOf(c);
                val = 16 * val + d;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return "";
        }
        return String.valueOf(val);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Common">
    public HashMap<String, Node> mapNode() {
        HashMap<String, Node> mapNode = new HashMap<>();
        try {
            List<Node> lstNode = new NodeServiceImpl().findList();
            for (Node bo : lstNode) {
                mapNode.put(bo.getNodeCode(), bo);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return mapNode;
    }

    public HashMap<String, RiMsRcParam> getMapParam() {
        HashMap<String, RiMsRcParam> mapParam = new HashMap<>();
        List<RiMsRcParam> lstParam;
        RiMsRcParam param;
        Map<String, Object> filters = new HashMap<>();
        List<RiSgRcParam> lstParamCheck;
        List<String> listParamCode = new ArrayList<>();
        try {
            listParamCode.add(Constants.paramCodeRnc.RNCSAU);
            listParamCode.add(Constants.paramCodeRnc.RNCPDP);
            listParamCode.add(Constants.paramCodeMsc.MSCSAU);
            listParamCode.add(Constants.paramCodeMsc.currentPdp);
            listParamCode.add(Constants.paramCodeMsc.MSCSAULicense);
            listParamCode.add(Constants.paramCodeMsc.licensePDP);
            filters.put("paramCode-EXAC", listParamCode);
            lstParam = new RiMsRcParamServiceImpl().findList(filters);
            for (RiMsRcParam bo : lstParam) {
                try {
                    mapParam.put(bo.getNodeCode() + "##" + bo.getParamCode(), bo);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            filters.put("processType-EXAC", "2");
            lstParamCheck = new RiSgRcParamServiceImpl().findList(filters);
            for (RiSgRcParam bo : lstParamCheck) {
                try {
                    param = new RiMsRcParam();
                    param.setNodeCode(bo.getNodeCode());
                    param.setParamCode(bo.getParamCode());
                    param.setParamValue(bo.getParamValue());
                    mapParam.put(bo.getNodeCode() + "##" + bo.getParamCode(), param);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return mapParam;
    }

    public HashMap<String, HashMap<String, RiMsRcParam>> getMapParamList() {
        HashMap<String, HashMap<String, RiMsRcParam>> mapParam = new HashMap<>();
        List<RiMsRcParam> lstParam;
        Map<String, Object> filters = new HashMap<>();

        try {
            filters.put("processType", "1");
            lstParam = new RiMsRcParamServiceImpl().findList(filters);
            for (RiMsRcParam bo : lstParam) {
                try {
                    if (bo.getNodeCode() != null && bo.getNodeType() != null) {
                        if (mapParam.containsKey(bo.getNodeCode().trim() + "#" + bo.getNodeType().trim())) {
                            mapParam.get(bo.getNodeCode() + "#" + bo.getNodeType().trim()).put(bo.getParamCode(), bo);
                        } else {
                            HashMap<String, RiMsRcParam> mapParamTemp = new HashMap<>();
                            mapParamTemp.put(bo.getParamCode(), bo);
                            mapParam.put(bo.getNodeCode().trim() + "#" + bo.getNodeType().trim(), mapParamTemp);
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return mapParam;
    }
//    public HashMap<String, HashMap<String, RiMsRcParam>> getMapParamList() {
//        HashMap<String, HashMap<String, RiMsRcParam>> mapParam = new HashMap<>();
//        List<RiMsRcParam> lstParam;
//
//        try {
//            lstParam = new RiMsRcParamServiceImpl().findList();
//            for (RiMsRcParam bo : lstParam) {
//                try {
//                    if (mapParam.containsKey(bo.getNodeCode())) {
//                        mapParam.get(bo.getNodeCode()).put(bo.getParamCode(), bo);
//                    } else {
//                        HashMap<String, RiMsRcParam> mapParamTemp = new HashMap<>();
//                        mapParamTemp.put(bo.getParamCode(), bo);
//                        mapParam.put(bo.getNodeCode(), mapParamTemp);
//                    }
//                } catch (Exception ex) {
//                    logger.error(ex.getMessage(), ex);
//                }
//            }
//
//        } catch (Exception ex) {
//            logger.error(ex.getMessage(), ex);
//        }
//        return mapParam;
//    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CrossCheck">
    public boolean crossCheck(List<String> listNodeMscActive) {

        List<RiMsRcPlan> listRescuePlan;
        Map<String, Object> filters;
        Map<String, String> orders;
        boolean check = true;
        try {
            HashMap<String, RiMsRcParam> mapParam = getMapParam();
            for (String nodeMscActive : listNodeMscActive) {
                try {
                    filters = new HashMap<>();
                    filters.put("nodeMscCurrent.nodeCode", nodeMscActive);
                    orders = new LinkedHashMap<>();
                    orders.put("nodeMscCurrent.nodeCode", "ASC");
                    orders.put("nodeMscBackup.nodeCode", "ASC");
                    orders.put("nodeMscBackup.province.areaCode", "ASC");
                    orders.put("nodeRnc.province.areaCode", "ASC");
                    orders.put("nodeRnc.nodeCode", "ASC");
                    listRescuePlan = new RiMsRcPlanServiceImpl().findList(filters, orders);
                    Long sauBackupRnc = 0L;
                    Long pdpBackupRnc = 0L;
                    for (RiMsRcPlan boPlan : listRescuePlan) {
                        try {
                            if (mapParam.containsKey(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCSAU)) {
                                try {
                                    boPlan.setSauRnc(Long.valueOf(mapParam.get(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCSAU).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCPDP)) {
                                try {
                                    boPlan.setPdpRnc(Long.valueOf(mapParam.get(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCPDP).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }

                            if (mapParam.containsKey(boPlan.getNodeMscCurrent().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAU)) {
                                try {
                                    boPlan.setSauMscCurrent(Long.valueOf(mapParam.get(boPlan.getNodeMscCurrent().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAU).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeMscCurrent().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAULicense)) {
                                try {
                                    boPlan.setLicenseSauMscCurrent(Long.valueOf(mapParam.get(boPlan.getNodeMscCurrent().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAULicense).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeMscBackup().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAU)) {
                                try {
                                    boPlan.setSauMscBackup(Long.valueOf(mapParam.get(boPlan.getNodeMscBackup().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAU).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeMscBackup().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAULicense)) {
                                try {
                                    boPlan.setLicenseSauMscBackup(Long.valueOf(mapParam.get(boPlan.getNodeMscBackup().getNodeCode() + "##" + Constants.paramCodeMsc.MSCSAULicense).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }

                            sauBackupRnc = sauBackupRnc + boPlan.getSauRnc();
                            pdpBackupRnc = pdpBackupRnc + boPlan.getPdpRnc();
                            boPlan.setSauMscBackupAfter(boPlan.getSauMscBackup() + sauBackupRnc);
                            boPlan.setPdpMscBackupAfter(boPlan.getPdpMscBackup() + pdpBackupRnc);
                            if (!boPlan.getLicenseSauMscCurrent().equals(0L)) {
                                boPlan.setSaurMscCurrent(Double.valueOf(boPlan.getSauMscCurrent()) * 100 / boPlan.getLicenseSauMscCurrent());
                            } else {
                                boPlan.setSaurMscCurrent(0D);
                            }
                            if (!boPlan.getLicensePdpMscCurrent().equals(0L)) {
                                boPlan.setPdprMscCurrent(Double.valueOf(boPlan.getPdpMscCurrent()) * 100 / boPlan.getLicensePdpMscCurrent());
                            } else {
                                boPlan.setPdprMscCurrent(0D);
                            }
                            if (!boPlan.getLicenseSauMscBackup().equals(0L)) {
                                boPlan.setSaurMscBackup(Double.valueOf(boPlan.getSauMscBackup()) * 100 / boPlan.getLicenseSauMscBackup());
                                boPlan.setSaurMscBackupAfter(Double.valueOf(boPlan.getSauMscBackupAfter()) * 100 / boPlan.getLicenseSauMscBackup());
                            } else {
                                boPlan.setSaurMscBackup(0D);
                                boPlan.setSaurMscBackupAfter(0D);
                            }
                            if (!boPlan.getLicensePdpMscBackup().equals(0L)) {
                                boPlan.setPdprMscBackup(Double.valueOf(boPlan.getPdpMscBackup()) * 100 / boPlan.getLicensePdpMscBackup());
                                boPlan.setPdprMscBackupAfter(Double.valueOf(boPlan.getPdpMscBackupAfter()) * 100 / boPlan.getLicensePdpMscBackup());
                            } else {
                                boPlan.setPdprMscBackup(0D);
                                boPlan.setPdprMscBackupAfter(0D);
                            }
                            if (boPlan.getSaurMscBackupAfter() > 0 && boPlan.getSaurMscBackupAfter() < Constants.paramCodeCommon.paramCrossCheck) {
                                boPlan.setAutoCheck(1L);
                            } else {
                                boPlan.setAutoCheck(0L);
                            }
                        } catch (Exception ex) {
                            check = false;
                            logger.error(ex.getMessage(), ex);
                        }
                    }
                    new RiMsRcPlanServiceImpl().saveOrUpdate(listRescuePlan);
                } catch (Exception ex) {
                    check = false;
                    logger.error(ex.getMessage(), ex);
                }
            }
        } catch (Exception ex) {
            check = false;
            logger.error(ex.getMessage(), ex);
        }
        return check;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Export_Rescue">
    public File exportFile() throws Exception {

        String pathOut = CommonExport.getPathSaveFileExport(MessageUtil.getResourceBundleMessage("label.rescue.info.export.filename"));
        Workbook workbook = null;
        try {
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                    .getExternalContext().getContext();

            String pathTemplate = ctx.getRealPath("/")
                    + File.separator + "templates" + File.separator + "TEMPLATE_RESCUE_INFO_EXPORT_MSC_RNC.xlsx";

            InputStream fileTemplate = new FileInputStream(pathTemplate);
            workbook = WorkbookFactory.create(fileTemplate);
            Sheet worksheet = workbook.getSheetAt(0);
            Font font = workbook.createFont();
            font.setFontName("Times New Roman");
            CellStyle cellStyleLeft = workbook.createCellStyle();
            cellStyleLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);
            cellStyleLeft.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleLeft.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setFont(font);
            cellStyleLeft.setWrapText(false);
            //phai
            CellStyle cellStyleRight = workbook.createCellStyle();
            cellStyleRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            cellStyleRight.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setFont(font);
            cellStyleRight.setWrapText(false);
            //giua
            CellStyle cellStyleCenter = workbook.createCellStyle();
            cellStyleCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenter.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenter.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setFont(font);
            cellStyleCenter.setWrapText(false);

            CellStyle cellStyleCenterDate = workbook.createCellStyle();
            cellStyleCenterDate.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenterDate.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenterDate.setBorderLeft(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderBottom(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderRight(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderTop(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setFont(font);
            cellStyleCenterDate.setWrapText(false);
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("nodeMscCurrent.nodeCode", "ASC");
            orders.put("nodeMscBackup.nodeCode", "ASC");
            orders.put("nodeMscBackup.province.areaCode", "ASC");
            orders.put("nodeRnc.province.areaCode", "ASC");
            orders.put("nodeRnc.nodeCode", "ASC");

            Map<String, Object> filters = new HashMap<>();
            List<RiMsRcPlan> lstRescueInfo = new RiMsRcPlanServiceImpl().findList(filters, orders);
            int startRow = 8;
            int i = 0;
            Cell cell;
            Row row;
            row = worksheet.createRow(4);
            cell = row.createCell(6);
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");

            cell.setCellValue(MessageUtil.getResourceBundleMessage("report.dateTime") + dateFormat.format(new Date()));
            cell.setCellStyle(cellStyleCenterDate);
            for (RiMsRcPlan temp : lstRescueInfo) {
                row = worksheet.createRow(i + startRow);

                int currentColunmData = 0;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(i + 1);
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeRnc().getProvince().getProvinceCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeRnc().getNodeCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeRnc().getVendor().getVendorName());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauRnc());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpRnc());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeMscCurrent().getNodeCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeMscCurrent().getVendor().getVendorName());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauMscCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicenseSauMscCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSaurMscCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpMscCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicensePdpMscCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdprMscCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeMscBackup().getNodeCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeMscBackup().getVendor().getVendorName());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicenseSauMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSaurMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicensePdpMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdprMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauMscBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicenseSauMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSaurMscBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpMscBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicensePdpMscBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdprMscBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getAutoCheckName());
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(dateFormat.format(temp.getUpdateTime()));
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getBackOfficeEngineer());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getApproveManager());
                cell.setCellStyle(cellStyleLeft);
//                currentColunmData++;
                i++;
            }
            try {
                FileOutputStream fileOut = new FileOutputStream(pathOut);
                workbook.write(fileOut);
                fileOut.flush();
                fileOut.close();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return new File(pathOut);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Import_Rescue">
    public void onImportPlan(FileUploadEvent event, Node nodeRnc, Node nodeMscBackup, Node nodeMscCurrent, List<RiMsRcResultImport> lstRiMsRcImportResult, List<RiMsRcPlan> lstRescueImportSave, HashMap<String, Node> mapNode) {
        try {
            Importer<RiMsRcModelExcel> importer = new Importer<RiMsRcModelExcel>() {

                protected Class<RiMsRcModelExcel> getDomainClass() {
                    // TODO Auto-generated method stub
                    return RiMsRcModelExcel.class;
                }

                @Override
                protected Map<Integer, String> getIndexMapFieldClass() {
                    // TODO Auto-generated method stub
                    Map<Integer, String> model = new HashMap<Integer, String>();

                    model.put(1, "nodeCode");
                    model.put(2, "nodeCodeCurrent");
                    model.put(3, "nodeCodeBackup");
                    return model;
                }

                @Override
                protected String getDateFormat() {
                    // TODO Auto-generated method stub
                    return null;
                }
            };

            List<RiMsRcModelExcel> lstDataImport = importer.getDatas(event, 0, "2-");

            if (lstDataImport.isEmpty()) {
                MessageUtil.setErrorMessageFromRes("datatable.empty");
                return;
            } else {
                if (valFileImport(lstDataImport)) {

                    // Kiem tra neu cung node code thi thuc hien update du lieu
                    RiMsRcPlan riMsRcPlanImport;

                    Map<String, Object> filters = new HashMap<>();
                    List<RiMsRcPlan> listRescue;
                    for (RiMsRcModelExcel paramExcel : lstDataImport) {
                        try {
                            RiMsRcResultImport rescueInfoResultImport = new RiMsRcResultImport();
                            rescueInfoResultImport.setNode(paramExcel.getNodeCode().trim());
                            rescueInfoResultImport.setNodeActive(paramExcel.getNodeCodeCurrent().trim());
                            rescueInfoResultImport.setNodeBackup(paramExcel.getNodeCodeBackup().trim());
//                            if (mapNode.containsKey(paramExcel.getNodeCodeRnc().trim())
//                                    && mapNode.containsKey(paramExcel.getNodeCodeMscCurrent().trim())
//                                    && mapNode.containsKey(paramExcel.getNodeCodeMscBackup().trim())) {
                            nodeRnc = mapNode.get(paramExcel.getNodeCode().trim());
                            nodeMscBackup = mapNode.get(paramExcel.getNodeCodeBackup().trim());
                            nodeMscCurrent = mapNode.get(paramExcel.getNodeCodeCurrent().trim());

                            if ("".equals(validateDataImport(nodeRnc, nodeMscBackup, nodeMscCurrent))) {
                                if (nodeRnc != null) {
                                    filters.put("nodeRnc.nodeCode", nodeRnc.getNodeCode());
                                }

                                listRescue = new RiMsRcPlanServiceImpl().findList(filters);
                                if (listRescue.isEmpty()) {
                                    riMsRcPlanImport = new RiMsRcPlan();
                                } else {
                                    riMsRcPlanImport = listRescue.get(0);
                                }

                                riMsRcPlanImport.setNodeRnc(nodeRnc);
                                riMsRcPlanImport.setNodeMscBackup(nodeMscBackup);
                                riMsRcPlanImport.setNodeMscCurrent(nodeMscCurrent);
                                riMsRcPlanImport.setBackOfficeEngineer(SessionWrapper.getCurrentUsername());
                                riMsRcPlanImport.setAutoCheck(0L);
                                riMsRcPlanImport.setSauRnc(0L);
                                riMsRcPlanImport.setSauMscBackup(0L);
                                riMsRcPlanImport.setSauMscCurrent(0L);
                                riMsRcPlanImport.setSaurMscBackup(0D);
                                riMsRcPlanImport.setSaurMscCurrent(0D);
                                riMsRcPlanImport.setPdpRnc(0L);
                                riMsRcPlanImport.setPdpMscBackup(0L);
                                riMsRcPlanImport.setPdpMscCurrent(0L);
                                riMsRcPlanImport.setPdprMscBackup(0D);
                                riMsRcPlanImport.setPdprMscCurrent(0D);
                                riMsRcPlanImport.setLicensePdpMscBackup(0L);
                                riMsRcPlanImport.setLicensePdpMscCurrent(0L);
                                riMsRcPlanImport.setLicenseSauMscBackup(0L);
                                riMsRcPlanImport.setLicenseSauMscCurrent(0L);
                                riMsRcPlanImport.setSauMscBackupAfter(0L);
                                riMsRcPlanImport.setPdpMscBackupAfter(0L);
                                riMsRcPlanImport.setSaurMscBackupAfter(0D);
                                riMsRcPlanImport.setPdprMscBackupAfter(0D);
                                riMsRcPlanImport.setUpdateTime(new Date());

                                new RiMsRcPlanServiceImpl().saveOrUpdate(riMsRcPlanImport);
                                lstRescueImportSave.add(riMsRcPlanImport);
                                rescueInfoResultImport.setResultImport("OK");
                                rescueInfoResultImport.setDetailImport("OK");
                                lstRiMsRcImportResult.add(rescueInfoResultImport);

                            } else {
                                rescueInfoResultImport.setResultImport("NOK");
                                rescueInfoResultImport.setDetailImport(validateDataImport(nodeRnc, nodeMscBackup, nodeMscCurrent));
                                lstRiMsRcImportResult.add(rescueInfoResultImport);
                            }
//                            }

                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                }
            }
//            RequestContext.getCurrentInstance().execute("PF('dlgUploadRescueInfo').hide()");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private String validateDataImport(Node nodeRnc, Node nodeMscBackup, Node nodeMscCurrent) {
        String result = "";
        try {
            // Kiem tra xem cac truong da duoc nhap day du du lieu chua
            if (nodeRnc == null) {
                result = result + "Chưa nhập node Rnc hoặc nhập chưa đúng\n";
            }
            if (nodeMscBackup == null) {
                result = result + "Chưa nhập node Msc active hoặc nhập chưa đúng\n";
            }
            if (nodeMscCurrent == null) {
                result = result + "Chưa nhập node Msc backup hoặc nhập chưa đúng\n";
            }

            // Kiem tra tinh logic cua du lieu
            if ("".equals(result)) {
                Map<String, Object> filters = new HashMap<>();

                if (nodeMscCurrent != null && nodeMscBackup != null && nodeMscCurrent.getNodeCode().equals(nodeMscBackup.getNodeCode())) {
                    result = result + MessageUtil.getResourceBundleMessage("label.err.duplicate.mscCurrentBackup");
                }
                filters.clear();
                if (nodeRnc != null) {
                    filters.put("rncName", nodeRnc.getNodeCode());
                }
                if (nodeMscCurrent != null) {
                    filters.put("mscName", nodeMscCurrent.getNodeCode());
                }
//                List<RiMsRcParamMscRnc> lstMscRnc = new RiMsRcParamMscRncServiceImpl().findList(filters);
//                if (lstMscRnc.isEmpty()) {
//                    result = result + MessageUtil.getResourceBundleMessage("label.err.not.existed.rnc.sgsnCurrent");
//                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return result;
    }

    public boolean valFileImport(List<RiMsRcModelExcel> lstParamNodeVal) {
        boolean check = true;
        for (RiMsRcModelExcel param : lstParamNodeVal) {
            try {
                if (param.getNodeCode() == null
                        || param.getNodeCode().trim().isEmpty()
                        || param.getNodeCodeBackup() == null
                        || param.getNodeCodeBackup().trim().isEmpty()
                        || param.getNodeCodeCurrent() == null
                        || param.getNodeCodeCurrent().trim().isEmpty()) {
                    check = false;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return check;
    }

    public File exportFileResultImport(List<RiMsRcResultImport> lstRiMsRcImportResult) throws Exception {

        String pathOut = CommonExport.getPathSaveFileExport(MessageUtil.getResourceBundleMessage("title.rescue.import.result"));
        Workbook workbook = null;
        try {
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                    .getExternalContext().getContext();

            String pathTemplate = ctx.getRealPath("/")
                    + File.separator + "templates" + File.separator + "Template_import_rescue_infomation_result_msc_rnc.xlsx";

            InputStream fileTemplate = new FileInputStream(pathTemplate);
            workbook = WorkbookFactory.create(fileTemplate);
            Sheet worksheet = workbook.getSheetAt(0);
            Font font = workbook.createFont();
            font.setFontName("Times New Roman");
            CellStyle cellStyleLeft = workbook.createCellStyle();
            cellStyleLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);
            cellStyleLeft.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleLeft.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setFont(font);
            cellStyleLeft.setWrapText(false);
            //phai
            CellStyle cellStyleRight = workbook.createCellStyle();
            cellStyleRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            cellStyleRight.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setFont(font);
            cellStyleRight.setWrapText(false);
            //giua
            CellStyle cellStyleCenter = workbook.createCellStyle();
            cellStyleCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenter.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenter.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setFont(font);
            cellStyleCenter.setWrapText(false);

            CellStyle cellStyleCenterDate = workbook.createCellStyle();
            cellStyleCenterDate.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenterDate.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenterDate.setBorderLeft(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderBottom(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderRight(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderTop(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setFont(font);
            cellStyleCenterDate.setWrapText(false);

            List<RiMsRcResultImport> lstRescueInfo = lstRiMsRcImportResult;
            int startRow = 8;
            int i = 0;
            Cell cell;
            Row row;
            row = worksheet.createRow(4);
            cell = row.createCell(1);
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");

            cell.setCellValue(MessageUtil.getResourceBundleMessage("report.dateTime") + dateFormat.format(new Date()));
            cell.setCellStyle(cellStyleCenterDate);
            for (RiMsRcResultImport temp : lstRescueInfo) {
                row = worksheet.createRow(i + startRow);

                int currentColunmData = 0;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(i + 1);
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeActive());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeBackup());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getResultImport());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getDetailImport());
                cell.setCellStyle(cellStyleLeft);
//                currentColunmData++;
                i++;

            }
            try {
                FileOutputStream fileOut = new FileOutputStream(pathOut);
                workbook.write(fileOut);
                fileOut.flush();
                fileOut.close();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return new File(pathOut);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Insert_Update_Rescue">

    public void savePlan(Node nodeRnc, Node nodeMscBackup, Node nodeMscCurrent, boolean isEdit, RiMsRcPlan riMsRcPlan) {
        try {
            if (validateData(nodeRnc, nodeMscBackup, nodeMscCurrent, isEdit, riMsRcPlan)) {

                RiMsRcPlan bo = new RiMsRcPlan();
                if (isEdit) {
                    bo.setId(riMsRcPlan.getId());
                }
                bo.setNodeRnc(nodeRnc);
                bo.setNodeMscBackup(nodeMscBackup);
                bo.setNodeMscCurrent(nodeMscCurrent);
                bo.setBackOfficeEngineer(SessionWrapper.getCurrentUsername());
//                if (!isEdit) {
                bo.setAutoCheck(0L);
                bo.setSauRnc(0L);
                bo.setSauMscBackup(0L);
                bo.setSauMscCurrent(0L);
                bo.setSaurMscBackup(0D);
                bo.setSaurMscCurrent(0D);
                bo.setPdpRnc(0L);
                bo.setPdpMscBackup(0L);
                bo.setPdpMscCurrent(0L);
                bo.setPdprMscBackup(0D);
                bo.setPdprMscCurrent(0D);
                bo.setLicensePdpMscBackup(0L);
                bo.setLicensePdpMscCurrent(0L);
                bo.setLicenseSauMscBackup(0L);
                bo.setLicenseSauMscCurrent(0L);
                bo.setSauMscBackupAfter(0L);
                bo.setPdpMscBackupAfter(0L);
                bo.setSaurMscBackupAfter(0D);
                bo.setPdprMscBackupAfter(0D);
                bo.setIsCreateMop(0L);
                bo.setIsRefreshParam(0L);
//                }
                bo.setUpdateTime(new Date());

                new RiMsRcPlanServiceImpl().saveOrUpdate(bo);
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            }
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }
    }

    private boolean validateData(Node nodeRnc, Node nodeMscBackup, Node nodeMscCurrent, boolean isEdit, RiMsRcPlan riMsRcPlan) {
        try {
            // Kiem tra xem cac truong da duoc nhap day du du lieu chua
            if (nodeRnc == null
                    || nodeMscBackup == null
                    || nodeMscCurrent == null) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                return false;
            }

            // Kiem tra tinh logic cua du lieu
            Map<String, Object> filters = new HashMap<>();
            filters.put("nodeRnc.nodeCode", nodeRnc.getNodeCode());
            if (isEdit) {
                filters.put("id", riMsRcPlan.getId());
            }
            List listRescue = new RiMsRcPlanServiceImpl().findList(filters);
            if ((isEdit && listRescue.size() > 1) || (!isEdit && listRescue.size() > 0)) {
                MessageUtil.setErrorMessageFromRes("label.err.duplicate.nodecode");
                return false;
            }
            if (nodeMscCurrent.getNodeCode().equals(nodeMscBackup.getNodeCode())) {
                MessageUtil.setErrorMessageFromRes("label.err.duplicate.mscCurrentBackup");
                return false;
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return true;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete_Rescue">
    //</editor-fold>
}
