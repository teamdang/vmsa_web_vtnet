package com.viettel.controller;

import com.viettel.vsa.token.UserToken;
import com.viettel.vsa.util.Connector;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean(name = "authService")
@RequestScoped
public class LogoutController implements Serializable {

    private static final long serialVersionUID = 5985977399461345507L;
    protected static final Logger LOGGER = LoggerFactory.getLogger(LogoutController.class);
    private volatile static String serviceURL;
    private volatile static String domainCode;
    private volatile static String logoutUrl;
    private static final String FILE_URL = "cas";
    private volatile static ResourceBundle rb;

    public void doLogout() throws IOException {
        HttpSession localHttpSession = getCurrentSession();
        UserToken localUserToken = (UserToken) localHttpSession.getAttribute("vsaUserToken");
        if (localUserToken != null) {
            setLogoutUrl(getLogoutUrl() + "?service=" + URLEncoder.encode(serviceURL, "UTF-8"));
            setLogoutUrl(getLogoutUrl() + "&userName=" + localUserToken.getUserName());
            setLogoutUrl(getLogoutUrl() + "&appCode=" + Connector.domainCode);
        }
        localHttpSession.setAttribute("vsaUserToken", null);
        localHttpSession.invalidate();
        FacesContext.getCurrentInstance().getExternalContext().redirect(logoutUrl);
    }

    public static HttpSession getCurrentSession() {
        HttpServletRequest localHttpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return localHttpServletRequest.getSession(false);
    }

    public static String getServiceURL() {
        return serviceURL;
    }

    public static void setServiceURL(String paramString) {
        serviceURL = paramString;
    }

    public static String getDomainCode() {
        return domainCode;
    }

    public static void setDomainCode(String paramString) {
        domainCode = paramString;
    }

    public static String getLogoutUrl() {
        return logoutUrl;
    }

    public static void setLogoutUrl(String paramString) {
        logoutUrl = paramString;
    }

    public static ResourceBundle getRb() {
        return rb;
    }

    public static void setRb(ResourceBundle paramResourceBundle) {
        rb = paramResourceBundle;
    }

    public static String getFileUrl() {
        return "cas";
    }

    static {
        try {
            rb = ResourceBundle.getBundle("cas");
            serviceURL = rb.getString("service");
            domainCode = rb.getString("domainCode");
            logoutUrl = rb.getString("logoutUrl");
        } catch (MissingResourceException localMissingResourceException) {
            LOGGER.error(localMissingResourceException.getMessage(), localMissingResourceException);
        }
    }
}

/*
 * Location:
 * C:\Users\xuanhuy\Dropbox\Viettel\ksclm\WebContent\WEB-INF\lib\vtnet-
 * vsa.jar!\com\viettel\vsa\filter\LogoutController.class Java compiler version:
 * 7 (51.0) JD-Core Version: 0.7.1
 */
