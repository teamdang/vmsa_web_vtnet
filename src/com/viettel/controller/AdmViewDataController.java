/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.controller;

import ReportNTD.ChartByJfreeBO;
import ReportNTD.FileUtil;
import ReportNTD.GroupAndSumBO;
import ReportNTD.MergTitleBO;
import ReportNTD.ReportCSV;
import ReportNTD.ReportExcel2007;
import ReportNTD.ReportExcel2007New;
import ReportNTD.ReportExcelInterface;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.adm.AdmCommonBO;
import com.viettel.model.adm.AdmFuncQueryCondUnMap;
import com.viettel.model.adm.AdmFuncQueryUnMapBO;
import com.viettel.model.adm.DataModel;
import com.viettel.model.adm.V2AdmDatabase;
import com.viettel.model.adm.V2AdmFuncBO;
import com.viettel.model.adm.V2AdmFuncQueryBO;
import com.viettel.model.adm.V2AdmFuncQueryButtonBO;
import com.viettel.model.adm.V2AdmFuncQueryColCalBO;
import com.viettel.model.adm.V2AdmFuncQueryColGroupBO;
import com.viettel.model.adm.V2AdmFuncQueryColumnBO;
import com.viettel.model.adm.V2AdmFuncQueryCondBO;
import com.viettel.model.adm.V2AdmFuncQueryCondDependBO;
import com.viettel.persistence.adm.V2AdmDatabaseServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryButtonServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryColGroupServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryColumnServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryCondDependServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryCondServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncServiceImpl;
import com.viettel.util.HibernateUtil;
import com.viettel.util.MessageUtil;
import com.viettel.util.NamedParameterStatement;
import com.viettel.util.ServiceUtil;
import com.viettel.util.StringUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author vipc
 */
@ViewScoped
@ManagedBean
public class AdmViewDataController implements Serializable {

    public interface BUTTON_IN_GRID {

        int FIRST = 1;
        int PRE = 2;
        int NEXT = 3;
        int LAST = 4;
    }
    private V2AdmFuncBO selectedObj = new V2AdmFuncBO();
    private String functionCode;
    private List<String> selectedFunctionCode;
    private List<AdmCommonBO> listFunctionCommon;
    private List<V2AdmFuncBO> lstFunctions;
    private List<V2AdmFuncBO> selectedFunction;
    private List<AdmFuncQueryUnMapBO> listConfigFunc;
    private List<AdmFuncQueryCondUnMap> listCondsUltility;
    @ManagedProperty(value = "#{v2AdmFuncService}")
    private V2AdmFuncServiceImpl v2AdmFuncServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryService}")
    private V2AdmFuncQueryServiceImpl v2AdmFuncQueryServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryCondService}")
    private V2AdmFuncQueryCondServiceImpl v2AdmFuncQueryCondServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryColumnService}")
    private V2AdmFuncQueryColumnServiceImpl v2AdmFuncQueryColumnServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryColGroupService}")
    private V2AdmFuncQueryColGroupServiceImpl v2AdmFuncQueryColGroupServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryCondDependService}")
    private V2AdmFuncQueryCondDependServiceImpl v2AdmFuncQueryCondDependServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryButtonService}")
    private V2AdmFuncQueryButtonServiceImpl v2AdmFuncQueryButtonServiceImpl;

    @PostConstruct
    public void onStart() {
        try {
            functionCode = FacesContext.getCurrentInstance().
                   getExternalContext().getRequestParameterMap().get("functionCode");
            
            Map<String, Object> filters = new HashMap<>();
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("functionTitle", "ASC");
            lstFunctions = v2AdmFuncServiceImpl.findList(filters, orders);
            listFunctionCommon = new ArrayList<>();
            for (V2AdmFuncBO item : lstFunctions) {
                listFunctionCommon.add(new AdmCommonBO(item.getFunctionCode(), item.getFunctionTitle()));
            }
            
            if (functionCode != null && !functionCode.trim().isEmpty()) {
                V2AdmFuncBO function = v2AdmFuncServiceImpl.findById(functionCode);
                
                if (function != null) {
                    onViewFunction(function);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onChangeControl(Long queryId, Long controlId) throws SQLException {
        if (queryId == null || controlId == null) {
            return;
        }
        for (AdmFuncQueryUnMapBO query : listConfigFunc) {
            if (queryId.equals(query.getQuery().getId().getQueryId())) {
                List<AdmFuncQueryCondUnMap> listCond = query.getConditions();
                for (AdmFuncQueryCondUnMap cond : listCond) {
                    //load list data for childrent contron by controlId
                    if (controlId.equals(cond.getCondition().getId().getControlId())) {
                        List<V2AdmFuncQueryCondDependBO> listCondDepend = cond.getCondDepend();
                        Collections.sort(listCondDepend, new Comparator<V2AdmFuncQueryCondDependBO>() {

                            @Override
                            public int compare(V2AdmFuncQueryCondDependBO o1, V2AdmFuncQueryCondDependBO o2) {
                                if (o1.getCondDependIndex() == null) {
                                    return -1;
                                }
                                if (o2.getCondDependIndex() == null) {
                                    return 1;
                                }
                                return o1.getCondDependIndex().compareTo(o2.getCondDependIndex());
                            }
                        });
                        if (listCondDepend != null && !listCondDepend.isEmpty()) {
                            for (V2AdmFuncQueryCondDependBO condDepend : listCondDepend) {
                                for (AdmFuncQueryCondUnMap condeTemp : listCond) {
                                    if (condDepend.getId().getControlChild().equals(condeTemp.getCondition().getId().getControlId())) {
                                        List<AdmFuncQueryCondUnMap> listCondParent = getListCondParent(condDepend.getId().getControlChild(), listCond);
                                        List dataBind = getDataBindControlDepend(condeTemp, listCondParent);
                                        condeTemp.getCondition().setLstSelect(dataBind);
                                    }
                                }
                            }
                        }
                        return;
                    }
                }
            }
        }
    }

    public List<AdmFuncQueryCondUnMap> getListCondParent(Long controlId, List<AdmFuncQueryCondUnMap> listAll) {
        List<AdmFuncQueryCondUnMap> listResult = new ArrayList<AdmFuncQueryCondUnMap>();
        for (AdmFuncQueryCondUnMap item : listAll) {
            if (item.getCondDepend() != null && !item.getCondDepend().isEmpty()) {
                for (V2AdmFuncQueryCondDependBO condDepend : item.getCondDepend()) {
                    if (controlId.equals(condDepend.getId().getControlChild())) {
                        listResult.add(item);
                        break;
                    }
                }
            }
        }
        return listResult;
    }

    public List getDataBindControlDepend(AdmFuncQueryCondUnMap condDepend, List<AdmFuncQueryCondUnMap> listValueParent) throws SQLException {

        List<AdmCommonBO> list = new ArrayList<AdmCommonBO>();
        AdmCommonBO commonBO = new AdmCommonBO();
        String sql;
        if (condDepend.getCondition() != null && condDepend.getCondition().getIsRequire() == null && !"MultiSelect".equalsIgnoreCase(condDepend.getCondition().getControlType())) {
            commonBO.setValue("");
            commonBO.setLabel(MessageUtil.getResourceBundleMessage("label.choose"));
            list.add(commonBO);
        }
        boolean isEscapeXml = true;
        String databind = condDepend.getCondition().getDatabind();
        if (databind
                != null) {
            if (databind.toUpperCase().contains("SELECT")) {
                PreparedStatement ps = null;
                ResultSet rs = null;
                Connection conn = null;
                try {
                    sql = "Select * from (" + databind + ") temp where 1=1 ";
                    Object value;//= null;
                    List<Object> listValue;
                    List<Object> params = new ArrayList();
                    if (!listValueParent.isEmpty()) {
                        for (AdmFuncQueryCondUnMap item : listValueParent) {
                            if (item != null) {
                                V2AdmFuncQueryCondBO cond = item.getCondition();
                                value = item.getValueSearch();
                                listValue = item.getListValueSearch();
//                                if (value != null) {
                                String dataType = cond.getDataType();
                                if ("text".equalsIgnoreCase(dataType)) {
                                    if ("MultiSelect".equalsIgnoreCase(cond.getControlType())) {
                                        if (listValue != null && !listValue.isEmpty()) {
                                            String addStr = "";
                                            boolean add = false;
                                            for (int i = 0; i < listValue.size(); i++) {
                                                if (listValue.get(i) != null && !"".equals(listValue.get(i).toString())) {
                                                    addStr += "?,";
                                                    params.add(((Object) listValue.get(i)).toString());
                                                    add = true;
                                                }
                                            }
                                            if (add) {
                                                sql += " and temp." + cond.getColumnCode() + " in ( ";
                                                sql += addStr.substring(0, addStr.length() - 1) + ") ";
                                            }
                                        }
                                    } else {
                                        if (value != null && !"".equals(value.toString().trim())) {
                                            sql += " and (temp." + cond.getColumnCode() + ") = ? ";
                                            params.add((String) value);
                                        }
                                    }
                                } else if ("number".equalsIgnoreCase(dataType)) {
                                    if ("MultiSelect".equalsIgnoreCase(cond.getControlType())) {
                                        if (listValue != null && !listValue.isEmpty()) {
                                            String addStr = "";
                                            boolean add = false;

                                            for (int i = 0; i < listValue.size(); i++) {
                                                if (listValue.get(i) != null && !"".equals(listValue.get(i).toString())) {
                                                    addStr += "?,";
                                                    params.add((Double.valueOf(listValue.get(i).toString())));
                                                    add = true;
                                                }
                                            }
                                            if (add) {
                                                sql += " and temp." + cond.getColumnCode() + " in ( ";
                                                sql += addStr.substring(0, addStr.length() - 1) + ") ";
                                            }
                                        }
                                    } else {
                                        sql += " and temp." + cond.getColumnCode() + " = ? ";
                                        params.add(Double.valueOf(value.toString()));
                                    }
                                }
//                                }
                            }
                        }
                    }
                    conn = HibernateUtil.connectDbOracle();
                    ps = conn.prepareStatement(sql);
                    for (int i = 0; i < params.size(); i++) {
                        value = params.get(i);
                        if (value instanceof Date) {
                            ps.setTimestamp(i + 1, new Timestamp(((Date) value).getTime()));
                        } else if (value instanceof Double) {
                            ps.setDouble(i + 1, (Double) value);
                        } else if (value instanceof String) {
                            ps.setString(i + 1, value.toString());
                        }
                    }
                    rs = ps.executeQuery();
                    ResultSetMetaData md = rs.getMetaData();
                    int col = md.getColumnCount();
                    List<String> listId = new ArrayList();
                    listId.add("");
                    while (rs.next()) {
                        commonBO = new AdmCommonBO();
                        if (col > 1) {
                            commonBO.setValue(rs.getString(1));
                            if (isEscapeXml) {
                                commonBO.setLabel(StringUtils.escapeHTMLString(rs.getString(2)));
                            } else {
                                commonBO.setLabel(rs.getString(2));
                            }
                        } else if (col == 1) {
                            commonBO.setValue(rs.getString(1));
                            if (isEscapeXml) {
                                commonBO.setLabel(StringUtils.escapeHTMLString(rs.getString(1)));
                            } else {
                                commonBO.setLabel(rs.getString(1));
                            }
                        }

//                        if (condDepend.getCondition().getLstSelect() != null && !condDepend.getCondition().getLstSelect().isEmpty()) {
//                            if (condDepend.getCondition().getLstSelect().contains(commonBO.getValue())) {
//                                if (!listId.contains(commonBO.getValue())) {
//                                    list.add(commonBO);
//                                    listId.add(commonBO.getValue());
//                                }
//                            }
//                        } else {
//                            if (!listId.contains(commonBO.getValue())) {
//                                list.add(commonBO);
//                                listId.add(commonBO.getValue());
//                            }
//                        }
                        list.add(commonBO);
                        listId.add(commonBO.getValue());
                    }
//                    rs.close();
//                    ps.close();
                } catch (SQLException e) {
                    Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, e);
                } finally {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                }
            } else {
                String[] array = databind.trim().split(",");
                for (int i = 0; i < array.length; i += 2) {
                    commonBO = new AdmCommonBO();
                    commonBO.setValue(array[i].trim());
                    commonBO.setLabel(array[i + 1].trim());
                    list.add(commonBO);
                }
            }
        }
        //Remove option Lua chon neu chi co 1 phan tu

        if (list.size()
                == 2) {
            commonBO = list.get(0);
            if ("-1".equalsIgnoreCase(commonBO.getValue())) {
                list.remove(0);
            }
        }
        return list;
    }

    public void onViewFunction() {
//        onViewFunction(selectedFunction.get(0));
        if (selectedFunctionCode != null && !selectedFunctionCode.isEmpty()) {
            try {
                String item = selectedFunctionCode.get(0);
                V2AdmFuncBO func = v2AdmFuncServiceImpl.findById(item);
                if (func != null) {
                    onViewFunction(func);
                }
            } catch (AppException | SysException ex) {
                Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public AdmFuncQueryCondUnMap convertCondToCondUnMap(V2AdmFuncQueryCondBO cond) throws Exception {
        AdmFuncQueryCondUnMap condition = new AdmFuncQueryCondUnMap();
        Map<String, Object> filters = new HashMap<>();
        LinkedHashMap<String, String> orders = new LinkedHashMap<>();
        if (cond.getControlType() != null && "TreePicker".equalsIgnoreCase(cond.getControlType())) {
            String databind = cond.getDatabind();
            if (StringUtils.isNotNull(databind)) {
                String[] actionGetData = databind.split(";");
                if (actionGetData.length == 2) {
                    cond.setDatabind(actionGetData[0]); //Top
                    cond.setDefaultValue(actionGetData[1]); //Children
                }
            }
        } else if (cond.getControlType() != null
                && ("Selectedbox".equalsIgnoreCase(cond.getControlType())
                || ("MultiSelect".equalsIgnoreCase(cond.getControlType())))) {
            String databind = cond.getDatabind();
            if (StringUtils.isNotNull(databind)) {
                if (databind.contains(";")) {
                    String[] actionGetData = databind.split(";");
                    if (actionGetData.length == 2) {
                        cond.setDatabind(actionGetData[0]); //Top
                        cond.setDefaultValue(actionGetData[1]); //Children
                    }
                } else {
                    List<AdmCommonBO> lstSelect = getDatabind(cond);
                    cond.setLstSelect(lstSelect);
                    List<Object> lstSelected = new ArrayList<>();
                    if (cond.getDefaultValue() != null && !"".equals(cond.getDefaultValue().trim())) {
                        for (AdmCommonBO item : lstSelect) {
                            if (cond.getDefaultValue().equalsIgnoreCase(item.getValue())) {
                                if ("Selectedbox".equalsIgnoreCase(cond.getControlType())) {
                                    condition.setValueSearch(item.getValue());
                                    break;
                                } else if ("MultiSelect".equalsIgnoreCase(cond.getControlType())) {
                                    lstSelected.add(item.getValue());
                                }
                            }
                        }
                    }
                    condition.setListValueSearch(lstSelected);
                }
            }
        }
        if (cond.getDefaultValue() != null && !"".equals(cond.getDefaultValue().trim()) && (condition.getValueSearch() == null || "".equals(condition.getValueSearch()))) {
            condition.setValueSearch(cond.getDefaultValue().trim());
        }
        condition.setCondition(cond);
        filters.clear();
        filters.put("id.functionCode-EXAC", cond.getId().getFunctionCode());
        filters.put("id.queryId", cond.getId().getQueryId());
        filters.put("id.controlParent", cond.getId().getControlId());
        orders.clear();
        List<V2AdmFuncQueryCondDependBO> listCondDepend = v2AdmFuncQueryCondDependServiceImpl.findList(filters, orders);
        if (!listCondDepend.isEmpty()) {
            condition.setCondDepend(listCondDepend);
        }
        return condition;
    }

    public void onViewFunction(V2AdmFuncBO selectedFunction) {
        selectedObj = selectedFunction;
        if (selectedFunction == null) {

        } else {
            try {
                for (V2AdmFuncBO bo : lstFunctions) {
                    bo.setChoose(false);
                }
                selectedFunction.setChoose(true);
                listConfigFunc = new ArrayList();
                Map<String, Object> filters = new HashMap<>();
                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                String functionCode = selectedFunction.getFunctionCode();
                //get list query
                filters.put("id.functionCode-EXAC", selectedFunction.getFunctionCode());
                orders.put("queryIndex", "ASC");

                List<V2AdmFuncQueryBO> listQuery = v2AdmFuncQueryServiceImpl.findList(filters, orders);
                filters.put("id.queryId", 0L);
                orders.clear();
                List<V2AdmFuncQueryCondBO> lstCondUtility = v2AdmFuncQueryCondServiceImpl.findList(filters, orders);
                listCondsUltility = new ArrayList<>();
                if (lstCondUtility != null && !lstCondUtility.isEmpty()) {
                    for (V2AdmFuncQueryCondBO item : lstCondUtility) {
                        listCondsUltility.add(convertCondToCondUnMap(item));
                    }
                }
                if (listQuery != null && !listQuery.isEmpty()) {
                    for (V2AdmFuncQueryBO bo : listQuery) {
                        AdmFuncQueryUnMapBO temp = new AdmFuncQueryUnMapBO(bo);
                        List<AdmFuncQueryCondUnMap> listCond = new ArrayList();

                        //Lay ra danh sach cot hien thi
                        if (StringUtils.isNotNull(bo.getQueryType())) {
                            //Truong hop con lai lay o bang Column
                            filters.clear();
                            filters.put("id.functionCode-EXAC", functionCode);
                            filters.put("id.queryId", bo.getId().getQueryId());
                            filters.put("isDisplay", 1L);
                            orders.clear();
                            orders.put("columnIndex", "ASC");

                            List<V2AdmFuncQueryColumnBO> lstColumn = v2AdmFuncQueryColumnServiceImpl.findList(filters, orders);
                            for (int i = 0; i < lstColumn.size(); i++) {
                                lstColumn.get(i).setFieldInDataModel("field" + (i + 1));
                            }
//                    StringUtils.escapeHTMLString(lstColumn);
                            temp.setColumns(lstColumn);
                        }
                        //Lay danh sach dieu kien
                        filters.clear();
                        filters.put("id.functionCode-EXAC", functionCode);
                        filters.put("id.queryId", bo.getId().getQueryId());
                        Map<String, Object> sqlRes = new HashMap<>();
//                        sqlRes.put("(query_id in( 0 , ?)) ", bo.getId().getQueryId());
                        orders.clear();
                        orders.put("controlIndex", "ASC");
                        List<V2AdmFuncQueryCondBO> lstCond = v2AdmFuncQueryCondServiceImpl.findList(filters, sqlRes, orders);
                        if (!lstCond.isEmpty()) {
                            for (V2AdmFuncQueryCondBO cond : lstCond) {
//                                AdmFuncQueryCondUnMap condition = new AdmFuncQueryCondUnMap();
//                                if (cond.getControlType() != null && "TreePicker".equalsIgnoreCase(cond.getControlType())) {
//                                    String databind = cond.getDatabind();
//                                    if (StringUtils.isNotNull(databind)) {
//                                        String[] actionGetData = databind.split(";");
//                                        if (actionGetData.length == 2) {
//                                            cond.setDatabind(actionGetData[0]); //Top
//                                            cond.setDefaultValue(actionGetData[1]); //Children
//                                        }
//                                    }
//                                } else if (cond.getControlType() != null
//                                        && ("Selectedbox".equalsIgnoreCase(cond.getControlType())
//                                        || ("MultiSelect".equalsIgnoreCase(cond.getControlType())))) {
//                                    String databind = cond.getDatabind();
//                                    if (StringUtils.isNotNull(databind)) {
//                                        if (databind.contains(";")) {
//                                            String[] actionGetData = databind.split(";");
//                                            if (actionGetData.length == 2) {
//                                                cond.setDatabind(actionGetData[0]); //Top
//                                                cond.setDefaultValue(actionGetData[1]); //Children
//                                            }
//                                        } else {
//                                            List lstSelect = getDatabind(cond);
//                                            cond.setLstSelect(lstSelect);
//                                        }
//                                    }
//                                }
//                                if (cond.getDefaultValue() != null && !"".equals(cond.getDefaultValue().trim())) {
//                                    condition.setValueSearch(cond.getDefaultValue().trim());
//                                }
//                                condition.setCondition(cond);
//                                filters.clear();
//                                filters.put("id.functionCode-EXAC", functionCode);
//                                filters.put("id.queryId", bo.getId().getQueryId());
//                                filters.put("id.controlParent", cond.getId().getControlId());
//                                orders.clear();
//                                List<V2AdmFuncQueryCondDependBO> listCondDepend = v2AdmFuncQueryCondDependServiceImpl.findList(filters, orders);
//                                if (!listCondDepend.isEmpty()) {
//                                    condition.setCondDepend(listCondDepend);
//                                }
//                                listCond.add(condition);
                                listCond.add(convertCondToCondUnMap(cond));
                            }
                        }
                        temp.setConditions(listCond);

                        //Lay ra danh sach cac button (neu co)
                        filters.clear();
                        filters.put("id.functionCode-EXAC", functionCode);
                        filters.put("id.queryId", bo.getId().getQueryId());
                        orders.clear();
                        orders.put("btnIndex", "ASC");
                        List<V2AdmFuncQueryButtonBO> lstButton = v2AdmFuncQueryButtonServiceImpl.findList(filters, orders);
                        temp.setButtons(lstButton);
                        boolean hasCheckBox = false;
                        for (V2AdmFuncQueryButtonBO button : lstButton) {
                            if (!"None".equals(button.getNumRowsGrid())) {
                                hasCheckBox = true;
                                break;
                            }
                        }
                        temp.setHasCheckBox(hasCheckBox);
                        listConfigFunc.add(temp);
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void onSearch(int button, Long queryId) throws SQLException {
        int movePage;
        switch (button) {
            case BUTTON_IN_GRID.FIRST:
                movePage = Integer.MIN_VALUE;
                break;
            case BUTTON_IN_GRID.PRE:
                movePage = -1;
                break;
            case BUTTON_IN_GRID.NEXT:
                movePage = 1;
                break;
            case BUTTON_IN_GRID.LAST:
                movePage = Integer.MAX_VALUE;
                break;
            default:
                movePage = 0;
                break;
        }
        for (AdmFuncQueryUnMapBO query : listConfigFunc) {
            if (queryId.equals(query.getQuery().getId().getQueryId())) {
                if (movePage == Integer.MIN_VALUE) {
                    query.setPage(1);
                } else if (movePage == Integer.MAX_VALUE) {
                    query.setPage(Integer.MAX_VALUE);
                } else {
                    query.setPage(query.getPage() + movePage);
                }
                search(query);
            }
        }
    }

    public void onSearch() throws SQLException {
        if (listConfigFunc != null && !listConfigFunc.isEmpty()) {
            for (AdmFuncQueryUnMapBO queryBO : listConfigFunc) {
                search(queryBO);
            }
        }
    }

    private String validateSearch(AdmFuncQueryUnMapBO queryBO){
        if (queryBO != null && queryBO.getConditions()!= null && !queryBO.getConditions().isEmpty()) {
            for (AdmFuncQueryCondUnMap con : queryBO.getConditions()) {
                if (con.getCondition() != null 
                        && con.getCondition().getIsRequire() != null 
                        && !con.getCondition().getIsRequire().isEmpty()) {
                    boolean check1 = false;
                    boolean check2 = false;
                    if (con.getValueSearch() == null 
                            || con.getValueSearch().toString().trim().isEmpty()) {
                        check1 = true;
                    }
                    if (con.getListValueSearch()== null 
                            || con.getListValueSearch().isEmpty()) {
                        check2 = true;
                    }
                    
                    if (check1 && check2) {
                        return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                con.getCondition().getControlLabel());
                    }
                }
            }
        }
        return "OK";
    }
    public void search(AdmFuncQueryUnMapBO queryBO) throws SQLException {
        if (listCondsUltility != null && !listCondsUltility.isEmpty()) {
            for (AdmFuncQueryCondUnMap item : listCondsUltility) {
                if (queryBO.getConditions() == null) {
                    queryBO.setConditions(new ArrayList<AdmFuncQueryCondUnMap>());
                }
                if (!queryBO.getConditions().contains(item)) {
                    queryBO.getConditions().add(item);
                }
            }
        }
        String validateStr = validateSearch(queryBO);
        if (!"OK".equalsIgnoreCase(validateStr)) {
            MessageUtil.setWarnMessage(validateStr);
            return;
        }
        List<V2AdmFuncQueryColumnBO> lstColumn = queryBO.getColumns();
        List<DataModel> lstCurrentData = new ArrayList<DataModel>();
        synchronized (this) {
            ResultSet rs = null;
            NamedParameterStatement ps = null;
            try {//generate sql and create a statament to get data start
                Map result = this.getResultSet(queryBO.getQuery(), queryBO, false);
                rs = (ResultSet) result.get("rs");
                ps = (NamedParameterStatement) result.get("ps");
                //generate sql and create a statament to get data end
                while (rs.next()) {
                    DataModel row = new DataModel();
                    for (int i = 0, n = lstColumn.size(); i < n; i++) {
                        String columnCode = lstColumn.get(i).getId().getColumnCode();
                        String columnStyle = lstColumn.get(i).getColumnStyle();
                        Object value;
                        try {

                            if (StringUtils.isNotNull(columnStyle) && ("Float".equalsIgnoreCase(columnStyle) || "Number".equalsIgnoreCase(columnStyle))) {
                                if (rs.getObject(columnCode) == null) {
                                    value = "";
                                } else {
                                    value = getValueFormat(rs.getBigDecimal(columnCode), columnStyle);
                                }
                            } else if (StringUtils.isNotNull(columnStyle) && "Integer".equalsIgnoreCase(columnStyle)) {
                                if (rs.getObject(columnCode) == null) {
                                    value = "";
                                } else {
                                    value = getValueFormat(rs.getInt(columnCode), columnStyle);
                                }
                            } else if (StringUtils.isNotNull(columnStyle) && ("date".equalsIgnoreCase(columnStyle) || "datetime".equalsIgnoreCase(columnStyle))) {
                                if (rs.getObject(columnCode) == null) {
                                    value = "";
                                } else {
                                    value = getValueFormat(new Date(rs.getTimestamp(columnCode).getTime()), columnStyle);
                                }
                            } else {
                                if (rs.getObject(columnCode) == null) {
                                    value = "";
                                } else {
                                    value = getValueFormat(rs.getString(columnCode), columnStyle);
                                }
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
                            System.out.println("Cau hinh sai style cot, qId: " + queryBO.getQuery().getId().getQueryId() + ", column: " + columnCode);
                            if (rs.getObject(columnCode) == null) {
                                value = "";
                            } else {
                                value = rs.getString(columnCode);
                            }
                        }
                        Class aClass = DataModel.class;
                        Field field = aClass.getDeclaredField("field" + (i + 1));
                        field.setAccessible(true);
                        field.set(row, value);
                    }
                    lstCurrentData.add(row);
                }
                queryBO.setDatas(lstCurrentData);
            } catch (Exception ex) {
                MessageUtil.setErrorMessage(ex.getMessage());
                Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            }
        }
    }

    public static Object getValueFormat(Object obj, String format) {
        if (obj == null) {
            return "";
        }
        if (format == null) {
            if (obj instanceof String) {
                if ("null".equalsIgnoreCase((String) obj)) {
                    return "";
                }
                return (String) obj;
            } else {
                if ("null".equalsIgnoreCase(obj.toString())) {
                    return "";
                }
                return obj.toString();
            }
        }

        if ("linkdialog".equalsIgnoreCase(format) || "link".equalsIgnoreCase(format) || "linkdownload".equalsIgnoreCase(format)) {
            if (obj instanceof String) {
                if ("null".equalsIgnoreCase((String) obj)) {
                    return "";
                }
                return (String) obj;
            } else {
                if ("null".equalsIgnoreCase(obj.toString())) {
                    return "";
                }
                return obj.toString();
            }
        }

        if ("date".equalsIgnoreCase(format)) {
            if (obj instanceof Date) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                return sdf.format((Date) obj);
            }
        }
        if ("datetime".equalsIgnoreCase(format)) {
            if (obj instanceof Date) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                return sdf.format((Date) obj);
            }
        }
        if ("integer".equalsIgnoreCase(format)) {
            if (obj instanceof BigDecimal) {
                return ((BigDecimal) obj);
            } else {
                return ((Integer) obj);
            }
        }
        if ("float".equalsIgnoreCase(format)) {
            if (obj instanceof BigDecimal) {
                return ((BigDecimal) obj);
            } else {
                return ((Double) obj);
            }
        }
        if ("number".equalsIgnoreCase(format)) {
            if (obj instanceof BigDecimal) {
                return ((BigDecimal) obj);
            } else {
                return obj;
            }
        }
        return "";
    }

    public Map getResultSet(V2AdmFuncQueryBO queryBO, AdmFuncQueryUnMapBO queryUnMap, boolean all) throws Exception {
        Map kq = new HashMap();
        String sortField = "";
        
        int i = 0;
        for (V2AdmFuncQueryColumnBO column1 : queryUnMap.getColumns()) {
            if ("ASC".equals(column1.getSortType()) || "DESC".equals(column1.getSortType())) {
                if (i == 0) {
                    sortField += " order by " + column1.getId().getColumnCode() + " " + column1.getSortType() + ",";
                } else {
                    sortField += column1.getId().getColumnCode() + " " + column1.getSortType() + ",";
                }
                i ++;
            }
        }
        if (sortField.contains(",")) {
            sortField = sortField.substring(0, sortField.length() - 1);
        }
        if (sortField.trim().isEmpty()) {
            String column = queryUnMap.getColumns().get(0).getId().getColumnCode();
            sortField = " order by " + column.replace(" ", "_");
        }

        Map result = null;
        if (StringUtils.isNotNull(queryBO.getQueryType())) {
            if ("normal".equalsIgnoreCase(queryBO.getQueryType())) {
                result = getQueryStrAndParams(queryBO, queryUnMap);
            } else if ("cal".equalsIgnoreCase(queryBO.getQueryType())) {
                result = getQueryStrAndParamsSpecial(queryBO, queryUnMap);
            } else if ("decode".equalsIgnoreCase(queryBO.getQueryType()) || "full_decode".equalsIgnoreCase(queryBO.getQueryType())) {
                result = getQueryStrAndParamsDecode(queryBO, queryUnMap);
            } else if ("direct".equalsIgnoreCase(queryBO.getQueryType())) {
                result = getQueryStrAndParamsDirect(queryBO, queryUnMap);
            }
        }
        if (result == null) {
            result = new HashMap();
        }
        String sqlFrom = result.get("sql") == null ? "" : (String) result.get("sql");
//        String sqlWhere = result.get("sqlWhere") == null ? "" : (String) result.get("sqlWhere");
//        String sqlOrder = result.get("sqlOrder") == null ? "" : (String) result.get("sqlOrder");
//        String sqlGroup = result.get("sqlGroup") == null ? "" : (String) result.get("sqlGroup");
//        String sqlHaving = result.get("sqlHaving") == null ? "" : (String) result.get("sqlHaving");
        Map<String, Object> params = (Map<String, Object>) result.get("params");

        ResultSet rs = null;
        NamedParameterStatement ps = null;
        try {

            Object value;// = null;
            Connection connection = null;
            String connectType = "ORACLE";
            if (queryBO.getDbId() != null && queryBO.getDbId() != 0) {
                V2AdmDatabase db = new V2AdmDatabaseServiceImpl().findById(queryBO.getDbId());
                if (db != null) {
                    ServiceUtil service = new ServiceUtil();
                    connection = service.connect(db);
                    if ("com.facebook.presto.jdbc.PrestoDriver".equalsIgnoreCase(db.getDriver())) {
                        connectType = "HIVE";
                    }
                }
            } else {
                connection = HibernateUtil.connectDbOracle();
            }
            if ("ORACLE".equalsIgnoreCase(connectType)) {
                String queryCount = "select count(1) number_row from (" + sqlFrom + ") a";
                ps = new NamedParameterStatement(connection, queryCount);
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    value = param.getValue();
                    if (value instanceof Date) {
                        ps.setTimestamp(param.getKey(), new Timestamp(((Date) value).getTime()));
                    } else if (value instanceof Double) {
                        ps.setDouble(param.getKey(), (Double) value);
                    } else if (value instanceof String) {
                        ps.setString(param.getKey(), (String) value);
                    }
                }

                rs = ps.executeQuery();
                Integer rowCount = null;
                while (rs.next()) {
                    rowCount = rs.getInt("number_row");
                }
                kq.put("rowCount", rowCount);
                queryUnMap.setTotalRows(rowCount);
                rs.close();
                ps.close();

                StringBuilder sql = new StringBuilder("select * from (select tbl_temp.*,")
                        .append(" ROW_NUMBER() OVER(")
                        .append(sortField)
                        .append(") row_count")
                        .append(" from (")
                        .append(sqlFrom)
                        .append(") tbl_temp) a");
                if (queryUnMap.getPage() < 0) {
                    queryUnMap.setPage(1);
                }
                if (queryUnMap.getPage() >= Integer.MAX_VALUE) {
                    queryUnMap.setPage(queryUnMap.getTotalPages());
                }
                if (!all) {
                    int start = queryUnMap.getPage() < 2 ? 0 : (queryUnMap.getPage() - 1) * queryUnMap.getRows();
                    int count = queryUnMap.getRows();
                    if (start >= 0 && count > 0) {
                        sql.append(" where row_count between :startRow and :endRow ");
                        params.put("startRow", (double) start + 1);
                        params.put("endRow", (double) start + count);
                    }
                }
                ps = new NamedParameterStatement(connection, sql.toString());
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    value = param.getValue();
                    if (value instanceof Date) {
                        ps.setTimestamp(param.getKey(), new Timestamp(((Date) value).getTime()));
                    } else if (value instanceof Double) {
                        ps.setDouble(param.getKey(), (Double) value);
                    } else if (value instanceof String) {
                        ps.setString(param.getKey(), (String) value);
                    }
                }
                rs = ps.executeQuery();
            } else if ("HIVE".equalsIgnoreCase(connectType)) {
                Statement stmt = connection.createStatement();
                for (Map.Entry<String, Object> param : params.entrySet()) {
                    value = param.getValue();
                    if (value instanceof Date) {
                        sqlFrom = sqlFrom.replace(":" + param.getKey(), " to_date('', 'dd/MM/yyyy ' ) ");
                    } else if (value instanceof Double) {
                        sqlFrom = sqlFrom.replace(":" + param.getKey(), value.toString());
                    } else if (value instanceof String) {
                        sqlFrom = sqlFrom.replace(":" + param.getKey(), "'"+ (String) value + "'");
                    }
                }
                String queryCount = "select count(1) number_row from (" + sqlFrom + ") a";
                rs = stmt.executeQuery(queryCount);
                Integer rowCount = null;
                while (rs.next()) {
                    rowCount = rs.getInt("number_row");
                }
                kq.put("rowCount", rowCount);
                queryUnMap.setTotalRows(rowCount);
                rs.close();
                
                String sql = "with tbl as (select *, row_number() OVER () rowid from ( "+ sqlFrom + " ) ) select * from tbl ";
                if (queryUnMap.getPage() < 0) {
                    queryUnMap.setPage(1);
                }
                if (queryUnMap.getPage() >= Integer.MAX_VALUE) {
                    queryUnMap.setPage(queryUnMap.getTotalPages());
                }
                if (!all) {
                    int start = queryUnMap.getPage() < 2 ? 0 : (queryUnMap.getPage() - 1) * queryUnMap.getRows();
                    int count = queryUnMap.getRows();
                    if (start >= 0 && count > 0) {
                        sql += (" where rowid between :startRow and :endRow ");
                        sql = sql.replace(":startRow", ((double) start + 1) + "");
                        sql = sql.replace(":endRow", ((double) start + count) + "");
                    }
                }
                rs = stmt.executeQuery(sql);
            }
            kq.put("rs", rs);
            ResultSetMetaData md = rs.getMetaData();
            kq.put("md", md);
            kq.put("ps", ps);
        } catch (Exception ex) {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            throw ex;
        }
        return kq;

    }

    public static int countMatcher(String input, String regex) {
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = input.indexOf(regex, lastIndex + 1);
            if (lastIndex != -1) {
                count++;
            }
        }
        return count;
    }

    public static Map getQueryStrAndParams(V2AdmFuncQueryBO queryBO, AdmFuncQueryUnMapBO queryUnMap) throws Exception {

        Map<String, Object> params = new HashMap();
        String sqlFrom = "select ";
        for (V2AdmFuncQueryColumnBO column : queryUnMap.getColumns()) {
            sqlFrom += queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ",";
        }
        String sqlTable = queryBO.getQueryString();

        if (sqlTable.contains(
                "(null)")) {
            int count = countMatcher(sqlTable, "(null)");
            for (AdmFuncQueryCondUnMap temp : queryUnMap.getConditions()) {
                if ("dataType".equalsIgnoreCase(temp.getCondition().getDataType())) {
                    Object value = temp.getValueSearch();
                    if (value instanceof Date) {
                        sqlTable = sqlTable.replace("(null)", " :dateCaseNull ");
                        Date toDate = (Date) value;
                        for (int indexMatcher = 0; indexMatcher < count; indexMatcher++) {
                            params.put("dateCaseNull", toDate);
                        }
                    }
                }
            }
        }

        if (sqlTable.contains(
                "(1=1)")) {
//            int count = countMatcher(sqlTable, "(1=1)");
            for (AdmFuncQueryCondUnMap temp : queryUnMap.getConditions()) {
                if ("date".equalsIgnoreCase(temp.getCondition().getDataType())) {
                    Object value = temp.getValueSearch();
                    if (value instanceof Date) {
                        sqlTable = sqlTable.replace("(1=1)", " " + temp.getCondition().getColumnCode() + " between :fromDate11 and :toDate11 ");
                        Date toDate = (Date) value;
                        Date fromDate = new Date();
                        fromDate.setTime(toDate.getTime() - 8 * 24 * 60 * 60 * 1000);
                        toDate.setTime(toDate.getTime() + 24 * 60 * 60 * 1000 - 1);
                        params.put("fromDate11", fromDate);
                        params.put("toDate11", toDate);
                        break;
                    }
                }
            }
        }
        int countFromTime = countMatcher(sqlTable, ":fromTime");
        int countToTime = countMatcher(sqlTable, ":toTime");
        int countTime = countMatcher(sqlTable, ":time");
        if (countFromTime > 0 || countToTime > 0 || countTime
                > 0) {
            Date time = null;
            Date fromTime = null;
            Date toTime = null;
            for (AdmFuncQueryCondUnMap bo : queryUnMap.getConditions()) {
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    time = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && ">".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    fromTime = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && ">=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    fromTime = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "<".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    toTime = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "<=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    toTime = (Date) value;
                }
            }
            if (countFromTime > 0) {
                if (fromTime == null) {
                    System.out.println("Cau hinh thieu dieu kien :fromTime trong cau query");
                }
                params.put("fromTime", fromTime);
            }
            if (countToTime > 0) {
                if (toTime == null) {
                    System.out.println("Cau hinh thieu dieu kien :toTime trong cau query");
                }
                params.put("toTime", toTime);
            }
            if (countTime > 0) {
                if (time == null) {
                    System.out.println("Cau hinh thieu dieu kien :time trong cau query");
                }
                params.put("time", time);
            }
        }
        sqlFrom = sqlFrom.substring(0, sqlFrom.length() - 1) + " from (" + sqlTable + ") " + queryBO.getId().getFunctionCode();
        String sqlWhere = " where 1=1 ";
        Object value;// = null;
        for (AdmFuncQueryCondUnMap temp : queryUnMap.getConditions()) {
            value = temp.getValueSearch();
            System.out.println("Gia tri value truyen ve:" + value);
//            if (value != null && !"".equals(value.toString())) {
            String dataType = temp.getCondition().getDataType();
            if ("Text".equalsIgnoreCase(dataType)) {
                if ("MultiSelect".equalsIgnoreCase(temp.getCondition().getControlType())) {
                    List<Object> valueArray = temp.getListValueSearch();
                    if (valueArray == null || valueArray.isEmpty()) {
                        continue;
                    }
                    sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";

                    for (int i = 0; i < valueArray.size(); i++) {
                        sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                        params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, valueArray.get(i).toString());
                    }
                    sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                } else if ("TextArea".equalsIgnoreCase(temp.getCondition().getControlType())) {
                    if (value == null || "".equals(value)) {
                        continue;
                    }
                    sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") in ( ";
                    String[] valueArray = value.toString().split(",");
                    for (int i = 0; i < valueArray.length; i++) {
                        sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                        params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, ((String) valueArray[i]).toLowerCase());
                    }
                    sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                } else if ("SelectedBox".equalsIgnoreCase(temp.getCondition().getControlType())) {
                    if (value == null || "".equals(value)) {
                        continue;
                    }
                    sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") = :c" + temp.getCondition().getId().getControlId();
                    params.put("c" + +temp.getCondition().getId().getControlId(), (value.toString()).toLowerCase());

                } else {
                    if (value == null || "".equals(value)) {
                        continue;
                    }
                    sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") like :c" + temp.getCondition().getId().getControlId() + " escape '!' ";
                    params.put("c" + temp.getCondition().getId().getControlId(), "%" + (value.toString()).toLowerCase().replaceAll("!", "!!").replaceAll("%", "!%").replaceAll("_", "!_") + "%");
                }
            } else if (dataType.contains("Date")) {
                if (value == null || "".equals(value)) {
                    continue;
                }
                sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + temp.getCondition().getCompareType() + " :c" + temp.getCondition().getId().getControlId();
                params.put("c" + temp.getCondition().getId().getControlId(), (Date) value);
            } else if ("Number".equalsIgnoreCase(dataType)) {
                if ("MultiSelect".equalsIgnoreCase(temp.getCondition().getControlType())) {
                    List<Object> valueArray = temp.getListValueSearch();
                    if (valueArray == null || valueArray.isEmpty()) {
                        continue;
                    }
                    sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";

                    for (int i = 0; i < valueArray.size(); i++) {
                        sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                        params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, Double.valueOf(valueArray.get(i).toString()));
                    }
                    sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                } else if ("TextArea".equalsIgnoreCase(temp.getCondition().getControlType())) {
                    if (value == null || "".equals(value)) {
                        continue;
                    }
                    sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                    String[] valueArray = value.toString().split(",");

                    for (int i = 0; i < valueArray.length; i++) {
                        sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                        params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, Double.valueOf(valueArray[i]));
                    }
                    sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                } else if ("TextBox".equalsIgnoreCase(temp.getCondition().getControlType()) && "in".equals(temp.getCondition().getCompareType())) {
                    if (value == null || "".equals(value)) {
                        continue;
                    }
                    sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                    String[] valueArray = value.toString().split(",");

                    for (int i = 0; i < valueArray.length; i++) {
                        sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                        params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, Double.valueOf(valueArray[i]));
                    }
                    sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                } else {
                    if (value == null || "".equals(value)) {
                        continue;
                    }
                    sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + temp.getCondition().getCompareType() + " :c" + temp.getCondition().getId().getControlId();
                    params.put("c" + temp.getCondition().getId().getControlId(), Double.valueOf(value.toString()));
                }
            }
//            }
        }
        Map result = new HashMap();

        result.put(
                "sql", sqlFrom + sqlWhere);
        result.put(
                "params", params);
        return result;
    }

    public static Map getQueryStrAndParamsSpecial(V2AdmFuncQueryBO queryBO, AdmFuncQueryUnMapBO queryUnMap) throws Exception {

        Map<String, Object> params = new HashMap();
        List<V2AdmFuncQueryColCalBO> listColumn = queryUnMap.getColCals();
        String sqlFrom = "select ";
        for (V2AdmFuncQueryColCalBO column : listColumn) {
            if ("GROUP".equalsIgnoreCase(column.getId().getCalType())) {
                sqlFrom += queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ",";
            } else if (!"SQL".equalsIgnoreCase(column.getId().getCalType())) {
                String calType = column.getId().getCalType();
                if ("CONCAT".equals(calType)) {
                    calType = "wmsys.wm_concat";
                }
                sqlFrom += calType + "(" + queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ")"
                        + " AS " + column.getId().getCalType() + "_" + column.getId().getColumnCode() + ",";
            }
        }
        sqlFrom = sqlFrom.substring(0, sqlFrom.length() - 1) + " from (" + queryBO.getQueryString() + ") " + queryBO.getId().getFunctionCode();
        //Danh sach cac dieu kien binh thuong - WHERE
        String sqlWhere = " where 1=1 ";
        List<AdmFuncQueryCondUnMap> listCond = queryUnMap.getConditions();
        int countFromTime = countMatcher(sqlFrom, ":fromTime");
        int countToTime = countMatcher(sqlFrom, ":toTime");
        int countTime = countMatcher(sqlFrom, ":time");
        if (countFromTime > 0 || countToTime > 0 || countTime
                > 0) {
            Date time = null;
            Date fromTime = null;
            Date toTime = null;
            for (AdmFuncQueryCondUnMap bo : listCond) {
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    time = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && ">".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    fromTime = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && ">=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    fromTime = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "<".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    toTime = (Date) value;
                }
                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "<=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                    Object value = bo.getValueSearch();
                    toTime = (Date) value;
                }
            }
            if (countFromTime > 0) {
                params.put("fromTime", fromTime);
            }
            if (countToTime > 0) {
                params.put("toTime", toTime);
            }
            if (countTime > 0) {
                params.put("time", time);
            }
        }
        Object value;//= null;
        for (AdmFuncQueryCondUnMap temp : listCond) {
            value = temp.getValueSearch();
            if (value != null && !"".equals(value.toString())) {
                String dataType = temp.getCondition().getDataType();
                if ("Text".equalsIgnoreCase(dataType)) {
                    if ("MultiSelect".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                        Object[] valueArray = (Object[]) value;

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, ((String) valueArray[i]));
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } /*
                     * 18/08/2012-Dungnt44 Thêm control TextArea
                     */ else if ("TextArea".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") in ( ";
                        String[] valueArray = value.toString().split(",");

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, ((String) valueArray[i]).toLowerCase());
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } else if ("SelectedBox".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") = :c" + temp.getCondition().getId().getControlId();
                        params.put("c" + temp.getCondition().getId().getControlId(), ((String) value).toLowerCase());

                    } else {
                        sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") like :c" + temp.getCondition().getId().getControlId() + " escape '!' ";
                        params.put("c" + temp.getCondition().getId().getControlId(), "%" + ((String) value).toLowerCase().replaceAll("!", "!!").replaceAll("%", "!%").replaceAll("_", "!_") + "%");
                    }
                } else if ("Date".contains(dataType)) {
                    sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + temp.getCondition().getCompareType() + " :c" + temp.getCondition().getId().getControlId();
                    params.put("c" + temp.getCondition().getId().getControlId(), (Date) value);

                } else if ("Number".equalsIgnoreCase(dataType)) {
                    if ("MultiSelect".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                        String[] valueArray = (String[]) value;

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, (Double.valueOf(valueArray[i])));
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } else if ("TextArea".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                        String[] valueArray = value.toString().split(",");

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, (Double.valueOf(valueArray[i])));
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } else if ("TextBox".equalsIgnoreCase(temp.getCondition().getControlType()) && "in".equals(temp.getCondition().getCompareType())) {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                        String[] valueArray = value.toString().split(",");

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, (Double.valueOf(valueArray[i])));
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } else {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + temp.getCondition().getCompareType() + " :c" + temp.getCondition().getId().getControlId();
                        params.put("c" + temp.getCondition().getId().getControlId(), Double.valueOf(value.toString()));
                    }
                }
            }
        }
        String sqlGroup = " GROUP BY ";
        String sqlOrder = " ORDER BY ";
        for (V2AdmFuncQueryColCalBO column : listColumn) {
            if ("GROUP".equalsIgnoreCase(column.getId().getCalType())) {
                sqlGroup += queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ",";
                sqlOrder += queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ",";
            }
        }
        sqlGroup = sqlGroup.substring(0, sqlGroup.length() - 1);
        sqlOrder = sqlOrder.substring(0, sqlOrder.length() - 1);
        //Danh sach cac dieu kien dac biet - HAVING
        String sqlHaving = " HAVING 1=1 ";
        for (AdmFuncQueryCondUnMap temp : listCond) {
            value = temp.getValueSearch();
            if (value != null && !"".equals(value.toString())) {
                String dataType = temp.getCondition().getDataType();
                if ("Number".equalsIgnoreCase(dataType)) {
                    sqlHaving += " and " + temp.getCondition().getCalType() + "("
                            + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ")"
                            + temp.getCondition().getCompareType() + " :c" + temp.getCondition().getId().getControlId();
                    params.put("c" + temp.getCondition().getId().getControlId(), Double.valueOf(value.toString()));
                }
            }
        }
        Map result = new HashMap();
        String sql = sqlFrom + sqlWhere + sqlGroup + sqlHaving + sqlOrder;
        String sqlReturn = "Select ";
        for (V2AdmFuncQueryColCalBO column : listColumn) {
            if ("GROUP".equalsIgnoreCase(column.getId().getCalType())) {
                sqlReturn += "temp." + column.getId().getColumnCode() + ",";
            } else if ("SQL".equalsIgnoreCase(column.getId().getCalType())) {
                sqlReturn += column.getKpiColumnCode() + " AS " + column.getId().getColumnCode() + ",";

            } else {
                sqlReturn += "temp." + column.getId().getCalType() + "_" + column.getId().getColumnCode() + ",";
            }
        }
        sqlReturn = sqlReturn.substring(0, sqlReturn.length() - 1) + " from (" + sql + ") temp";

        result.put(
                "sql", sqlReturn);
        result.put(
                "params", params);
        return result;
    }

    public static Map getQueryStrAndParamsDirect(V2AdmFuncQueryBO queryBO, AdmFuncQueryUnMapBO queryUnMap) throws Exception {
        Map<String, Object> params = new HashMap();
        String[] sqlText = queryBO.getQueryString().split("\n");
        List<AdmFuncQueryCondUnMap> listCond = queryUnMap.getConditions();
        Object value;//= null;
        for (AdmFuncQueryCondUnMap temp : listCond) {
            value = temp.getValueSearch();
            if ("MultiSelect".equalsIgnoreCase(temp.getCondition().getControlType())) {
                List<Object> listSelected = temp.getListValueSearch();
                if (listSelected != null && !listSelected.isEmpty()) {
                    for (int j = 0; j < sqlText.length; j++) {
                        if (sqlText[j].contains(":" + temp.getCondition().getColumnCode())) {
                            String sqlParam = "";
                            for (int i = 0; i < listSelected.size(); i++) {
                                sqlParam += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                                if ("number".equalsIgnoreCase(temp.getCondition().getDataType())) {
                                    params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, Double.valueOf(listSelected.get(i).toString()));
                                } else if ("text".equalsIgnoreCase(temp.getCondition().getDataType())) {
                                    params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, listSelected.get(i).toString());
                                }
                            }
                            sqlParam = sqlParam.substring(0, sqlParam.length() - 1);
                            sqlText[j] = sqlText[j].replace(":" + temp.getCondition().getColumnCode(), sqlParam);

                        }
                    }
                } else {
                    for (int i = 0; i < sqlText.length; i++) {
                        if (sqlText[i].contains(":" + temp.getCondition().getColumnCode())) {
                            sqlText[i] = "";
                        }
                    }
                }
            } else if (value != null && !"".equals(value.toString())) {
                if ("TextArea".equalsIgnoreCase(temp.getCondition().getControlType())) {
                    String[] valueArray = value.toString().split(",");
                    for (int j = 0; j < sqlText.length; j++) {
                        if (sqlText[j].contains(":" + temp.getCondition().getColumnCode())) {
                            String sqlParam = "";
                            for (int i = 0; i < valueArray.length; i++) {
                                sqlParam += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                                if ("number".equalsIgnoreCase(temp.getCondition().getDataType())) {
                                    params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, Double.valueOf(valueArray[i]));
                                } else if ("text".equalsIgnoreCase(temp.getCondition().getDataType())) {
                                    params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, valueArray[i]);
                                }
                            }
                            sqlParam = sqlParam.substring(0, sqlParam.length() - 1);
                            sqlText[j] = sqlText[j].replace(":" + temp.getCondition().getColumnCode(), sqlParam);

                        }
                    }
                } else if ("TextBox".equalsIgnoreCase(temp.getCondition().getControlType()) && "in".equals(temp.getCondition().getCompareType())) {
                    String[] valueArray = value.toString().split(",");
                    for (int j = 0; j < sqlText.length; j++) {
                        if (sqlText[j].contains(":" + temp.getCondition().getColumnCode())) {
                            String sqlParam = "";
                            for (int i = 0; i < valueArray.length; i++) {
                                sqlParam += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                                if ("number".equalsIgnoreCase(temp.getCondition().getDataType())) {
                                    params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, Double.valueOf(valueArray[i]));
                                } else if ("text".equalsIgnoreCase(temp.getCondition().getDataType())) {
                                    params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, valueArray[i]);
                                }
                            }
                            sqlParam = sqlParam.substring(0, sqlParam.length() - 1);
                            sqlText[j] = sqlText[j].replace(":" + temp.getCondition().getColumnCode(), sqlParam);

                        }
                    }
                } else if ("TextBox".equalsIgnoreCase(temp.getCondition().getControlType()) && "text".equalsIgnoreCase(temp.getCondition().getDataType())) {
                    for (int j = 0; j < sqlText.length; j++) {
                        if (sqlText[j].contains(":" + temp.getCondition().getColumnCode())) {
                            sqlText[j] = sqlText[j].replace(":" + temp.getCondition().getColumnCode(), ":" + temp.getCondition().getColumnCode() + " escape '!' ");
                        }
                    }
                    if ("=".equals(temp.getCondition().getCompareType())) {
                        params.put(temp.getCondition().getColumnCode(), (String) value);
                    } else {
                        params.put(temp.getCondition().getColumnCode(), "%" + (value.toString().toLowerCase()).replaceAll("!", "!!").replaceAll("%", "!%").replaceAll("_", "!_") + "%");
                    }
                } else {
                    if ("date".equalsIgnoreCase(temp.getCondition().getDataType())) {
                        params.put(temp.getCondition().getColumnCode(), (Date) value);
                    } else if ("number".equalsIgnoreCase(temp.getCondition().getDataType())) {
                        params.put(temp.getCondition().getColumnCode(), Double.valueOf(value.toString()));
                    } else if ("text".equalsIgnoreCase(temp.getCondition().getDataType())) {
                        params.put(temp.getCondition().getColumnCode(), value.toString());
                    }
                }
            } else {
                for (int i = 0; i < sqlText.length; i++) {
                    if (sqlText[i].contains(":" + temp.getCondition().getColumnCode())) {
                        sqlText[i] = "";
                    }
                }
            }
        }
        Map result = new HashMap();
        String sql = "";
        for (String s : sqlText) {
            sql += s + "\n";
        }

        result.put(
                "sql", sql);
        result.put(
                "params", params);
        return result;
    }

    public static Map getQueryStrAndParamsDecode(V2AdmFuncQueryBO queryBO, AdmFuncQueryUnMapBO queryUnMap) throws Exception {

        Map<String, Object> params = new HashMap();
        Object value = null;
        List<Long> compareControlId = new ArrayList();
        List<V2AdmFuncQueryColCalBO> listColumn = queryUnMap.getColCals();
        List<AdmFuncQueryCondUnMap> listCond = queryUnMap.getConditions();
        String sqlFrom = "select ";
        for (V2AdmFuncQueryColCalBO column : listColumn) {
            for (AdmFuncQueryCondUnMap cond : listCond) {
                if (column.getCompareWithControl().equals(cond.getCondition().getId().getControlId())) {
                    value = cond.getValueSearch();
                    break;
                }
            }
            if ("GROUP".equalsIgnoreCase(column.getId().getCalType())) {
                sqlFrom += queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ",";
            } else if ("DECODE".equalsIgnoreCase(column.getId().getCalType())) {
                if (column.getColumnStyle() != null && "Date".equalsIgnoreCase(column.getColumnStyle())) {
                    sqlFrom += "max(decode(trunc(" + column.getKpiColumnCode() + "),trunc(:c" + column.getCompareWithControl() + ")"
                            + (column.getCompareWithValue() == null ? "" : column.getCompareWithValue()) + ",to_char(" + column.getValueColumnCode() + ",'dd/MM/yyyy'),null))"
                            + " AS " + column.getId().getColumnCode() + ",";
                    params.put("c" + column.getCompareWithControl(), value);
                    compareControlId.add(column.getCompareWithControl());
                } else {
                    sqlFrom += "max(decode(trunc(" + column.getKpiColumnCode() + "),trunc(:c" + column.getCompareWithControl() + ")"
                            + (column.getCompareWithValue() == null ? "" : column.getCompareWithValue()) + "," + column.getValueColumnCode() + ",null))"
                            + " AS " + column.getId().getColumnCode() + ",";
                    params.put("c" + column.getCompareWithControl(), value);
                    compareControlId.add(column.getCompareWithControl());
                }
            }
        }
        TreeSet tree = new TreeSet(compareControlId);
        Iterator it = tree.iterator();
        List<Long> listCompareDecodeControlId = new ArrayList();

        while (it.hasNext()) {
            listCompareDecodeControlId.add((Long) it.next());
        }
        String sqlTable = queryBO.getQueryString();

        if (sqlTable.contains(
                "(1=1)")) {
//            int count = InocAdmFuncListDAO.countMatcher(sqlTable, "(1=1)");
            Long controlId = listCompareDecodeControlId.get(0);
            String columnCode = null;
            for (AdmFuncQueryCondUnMap cond : listCond) {
                if (controlId.equals(cond.getCondition().getId().getControlId())) {
                    value = cond.getValueSearch();
                    columnCode = cond.getCondition().getColumnCode();
                    break;
                }
            }
            if (value instanceof Date) {
                sqlTable = sqlTable.replace("(1=1)", " " + columnCode + " between :fromDate11 and :toDate11 ");
                Date toDate = (Date) value;
                Date fromDate = new Date();
                fromDate.setTime(toDate.getTime() - 8 * 24 * 60 * 60 * 1000);
                toDate.setTime(toDate.getTime() + 24 * 60 * 60 * 1000 - 1);
                params.put("fromDate11", fromDate);
                params.put("toDate11", toDate);
            }
        }
        sqlFrom = sqlFrom.substring(0, sqlFrom.length() - 1) + " from (" + sqlTable + ") " + queryBO.getId().getFunctionCode();
        //Danh sach cac dieu kien binh thuong - WHERE
        String sqlWhere = " where 1=1 ";
        int countTime = countMatcher(sqlFrom, ":time");
        if (countTime
                > 0) {
            Date time = null;

            Long controlId = listCompareDecodeControlId.get(0);
            for (AdmFuncQueryCondUnMap cond : listCond) {
                if (controlId.equals(cond.getCondition().getId().getControlId())) {
                    value = cond.getValueSearch();
                    break;
                }
            }
            if (value instanceof Date) {
                time = (Date) value;
            }
            if (countTime > 0) {
                params.put("time", time);
            }
        }
        for (AdmFuncQueryCondUnMap temp : listCond) {
            value = temp.getValueSearch();
            if (value != null && !"".equals(value.toString())) {
                String dataType = temp.getCondition().getDataType();
                if ("Text".equalsIgnoreCase(dataType)) {
                    if ("MultiSelect".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                        Object[] valueArray = (Object[]) value;

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, ((String) valueArray[i]));
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } else if ("TextArea".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") in ( ";
                        String[] valueArray = value.toString().split(",");

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, (valueArray[i]).toLowerCase());
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } else if ("SelectedBox".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") = :c" + temp.getCondition().getId().getControlId();
                        params.put("c" + temp.getCondition().getId().getControlId(), ((String) value).toLowerCase());

                    } else {
                        sqlWhere += " and LOWER(" + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + ") like :c" + temp.getCondition().getId().getControlId() + " escape '!' ";
                        params.put("c" + temp.getCondition().getId().getControlId(), "%" + ((String) value).toLowerCase().replaceAll("!", "!!").replaceAll("%", "!%").replaceAll("_", "!_") + "%");
                    }
                } else if (dataType.indexOf("date") >= 0) {
                    sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + temp.getCondition().getCompareType() + " :c" + temp.getCondition().getId().getControlId();
                    params.put("c" + temp.getCondition().getId().getControlId(), (Date) value);

                } else if ("Number".equalsIgnoreCase(dataType)) {
                    if ("MultiSelect".equalsIgnoreCase(temp.getCondition().getControlType())) {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + " in ( ";
                        String[] valueArray = (String[]) value;

                        for (int i = 0; i < valueArray.length; i++) {
                            sqlWhere += ":c" + temp.getCondition().getId().getControlId() + "_" + i + ",";
                            params.put("c" + temp.getCondition().getId().getControlId() + "_" + i, (Double.valueOf(valueArray[i])));
                        }
                        sqlWhere = sqlWhere.substring(0, sqlWhere.length() - 1) + ") ";
                    } else {
                        sqlWhere += " and " + queryBO.getId().getFunctionCode() + "." + temp.getCondition().getColumnCode() + temp.getCondition().getCompareType() + " :c" + temp.getCondition().getId().getControlId();
                        params.put("c" + temp.getCondition().getId().getControlId(), Double.valueOf(value.toString()));
                    }
                }
            }
        }
        String sqlGroup = " GROUP BY ";
        String sqlOrder = " ORDER BY ";
        for (V2AdmFuncQueryColCalBO column : listColumn) {
            if ("GROUP".equalsIgnoreCase(column.getId().getCalType())) {
                sqlGroup += queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ",";
                sqlOrder += queryBO.getId().getFunctionCode() + "." + column.getId().getColumnCode() + ",";
            }
        }
        sqlGroup = sqlGroup.substring(0, sqlGroup.length() - 1);
        sqlOrder = sqlOrder.substring(0, sqlOrder.length() - 1);
        Map result = new HashMap();
        String sql = "select ";
        for (V2AdmFuncQueryColCalBO column : listColumn) {
            if ("GROUP".equalsIgnoreCase(column.getId().getCalType())) {
                sql += column.getId().getColumnCode() + ",";
            } else if ("DECODE".equalsIgnoreCase(column.getId().getCalType())) {
                sql += column.getId().getColumnCode() + ",";
            } else if ("SQL".equalsIgnoreCase(column.getId().getCalType())) {
                sql += column.getKpiColumnCode() + " as " + column.getId().getColumnCode() + ",";
            }
        }
        sql = sql.substring(0, sql.length() - 1) + " from (" + sqlFrom + sqlWhere + sqlGroup + sqlOrder + ")";

        result.put(
                "sql", sql);
        result.put(
                "params", params);
        return result;
    }

    public List<AdmCommonBO> getDatabind(V2AdmFuncQueryCondBO queryCondUnMap) throws Exception {
        List<AdmCommonBO> list = new ArrayList<AdmCommonBO>();
        AdmCommonBO commonBO = new AdmCommonBO();
        boolean isEscapeXml = false;

        if ("MultiSelect".equalsIgnoreCase(queryCondUnMap.getControlType())) {
            isEscapeXml = true;
        }

        if (!"MultiSelect".equalsIgnoreCase(queryCondUnMap.getControlType()) && queryCondUnMap.getIsRequire() == null) {
            commonBO.setValue("");
            commonBO.setLabel(MessageUtil.getResourceBundleMessage("label.choose"));
            list.add(commonBO);
        }
        //Bo sung them loc theo dieu kien loc session
        List<String> allowValueStr = new ArrayList();
        String databind = queryCondUnMap.getDatabind();
        List<String> listId = new ArrayList();

        listId.add("-1");
        if (databind != null) {
            if (databind.toUpperCase().contains("SELECT")) {
                PreparedStatement st = null;
                ResultSet rs = null;
                Connection conn = null;
                try {
                    conn = HibernateUtil.connectDbOracle();
                    st = conn.prepareStatement(databind.trim());
                    rs = st.executeQuery();
                    ResultSetMetaData md = rs.getMetaData();
                    int col = md.getColumnCount();
                    while (rs.next()) {
                        commonBO = new AdmCommonBO();
                        if (col > 1) {
                            commonBO.setValue(rs.getString(1));
                            if (isEscapeXml) {
                                commonBO.setLabel(StringUtils.escapeHTMLString(rs.getString(2)));
                            } else {
                                commonBO.setLabel(rs.getString(2));
                            }
                        } else if (col == 1) {
                            commonBO.setValue(rs.getString(1));
                            if (isEscapeXml) {
                                commonBO.setLabel(StringUtils.escapeHTMLString(rs.getString(1)));
                            } else {
                                commonBO.setLabel(rs.getString(1));
                            }
                        }
                        if (!allowValueStr.isEmpty()) {
                            if (allowValueStr.contains(commonBO.getValue())) {
                                if (!listId.contains(commonBO.getValue())) {
                                    list.add(commonBO);
                                    listId.add(commonBO.getValue());
                                }
                            }
                        } else {
                            if (!listId.contains(commonBO.getValue())) {
                                list.add(commonBO);
                                listId.add(commonBO.getValue());
                            }
                        }
                    }
                } catch (SQLException e) {
                    Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, e);
                } finally {
                    if (rs != null) {
                        rs.close();
                    }
                    if (st != null) {
                        st.close();
                    }
                    if (conn != null) {
                        conn.close();
                    }
                }
            } else {
                String[] array = databind.trim().split(",");
                for (int i = 0; i < array.length; i += 2) {
                    commonBO = new AdmCommonBO();
                    if (!allowValueStr.isEmpty()) {
                        if (allowValueStr.contains(array[i].trim())) {
                            commonBO.setValue(array[i].trim());
                            commonBO.setLabel(array[i + 1].trim());
                            list.add(commonBO);
                        }
                    } else {
                        commonBO.setValue(array[i].trim());
                        commonBO.setLabel(array[i + 1].trim());
                        list.add(commonBO);
                    }
                }
            }
        }
        //Remove option Lua chon neu chi co 1 phan tu

        if (list.size()
                == 2) {
            commonBO = list.get(0);
            if ("-1".equalsIgnoreCase(commonBO.getValue())) {
                list.remove(0);
            }
        }
        return list;
    }

    public DefaultStreamedContent onExport() {
        try {
            if (selectedObj == null) {
                return null;
            }
            String functionCode = selectedObj.getFunctionCode();
            String reportName = "";
            boolean isManySheet = true;
            List<String> listSheetName = new ArrayList();
            List listNumSheetOfRS = new ArrayList();
            List listOfListColumnTitle = new ArrayList();
            List listOfListColumnNameDB = new ArrayList();
            List listOfListColumnType = new ArrayList(); //List of List
            List listColumnSize = new ArrayList(); //List column size
            List listRsDataTitle = new ArrayList(); // List title column of resultset (Sub title for resultset)
            List<ResultSet> listRsData = new ArrayList(); //List data
            List<GroupAndSumBO> listColumnGroupAndSum = new ArrayList(); //Group &sum
            List<MergTitleBO> listMergeTitleBO = new ArrayList();
            List<NamedParameterStatement> listPs = new ArrayList();
            List<ChartByJfreeBO> listChartBo = new ArrayList(); // Truyen vao de xac dinh doi tuong ve
            List<List<String>> listOfListColumnFormula = new ArrayList();
            Date reportDate = null;
            Date fromDateValue = null;
            Date toDateValue = null;
            String strReportDate = "";
            String fromDate = "";
            String toDate = "";
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat dateTimeForMat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String subTitle = "";
            ReportCSV reportCSV = new ReportCSV();
            ReportExcelInterface reportExcel = null;
            try {
                if (listConfigFunc != null && !listConfigFunc.isEmpty()) {
                    for (AdmFuncQueryUnMapBO queryBO : listConfigFunc) {
                        //searc
                        if (StringUtils.isNotNull(queryBO.getQuery().getChartType()) && !"cal".equalsIgnoreCase(queryBO.getQuery().getQueryType())) {
                            ChartByJfreeBO chartBo = new ChartByJfreeBO();
                            chartBo.setChartType(queryBO.getQuery().getQueryType());
                            listChartBo.add(chartBo);
                        } else {
                            listChartBo.add(null);
                        }
                        Map result = getResultSet(queryBO.getQuery(), queryBO, true);
                        ResultSet rs = (ResultSet) result.get("rs");
                        NamedParameterStatement ps = (NamedParameterStatement) result.get("ps");
                        listPs.add(ps);
                        listRsData.add(rs);
                        listRsDataTitle.add(queryBO.getQuery().getQueryName());
                        //Export many sheet
                        if (!StringUtils.isNotNull(queryBO.getQuery().getSheetName())) {
                            isManySheet = false;
                        }
                        if (isManySheet) {
                            String sheetName = queryBO.getQuery().getSheetName();
                            int sheetIndex = listSheetName.indexOf(sheetName);
                            if (sheetIndex < 0) {
                                listSheetName.add(sheetName);
                                listNumSheetOfRS.add(listSheetName.size() - 1);
                            } else {
                                listNumSheetOfRS.add(sheetIndex);
                            }
                        }

                        //Group-Sum
                        List<String> listColumGroup = new ArrayList();
                        List<String> listColumGroupMerge = new ArrayList();
                        List<String> listColumSum = new ArrayList();
                        List<V2AdmFuncQueryColGroupBO> listColGroupBO = queryBO.getColGroups();
                        if (!listColGroupBO.isEmpty()) {
                            GroupAndSumBO groupAndSumBO = new GroupAndSumBO();
                            groupAndSumBO.setSttRsData(listRsData.size() - 1);
                            for (V2AdmFuncQueryColGroupBO bo : listColGroupBO) {
                                if ("horizontal".equalsIgnoreCase(bo.getGroupType())) {
                                    listColumGroup.add(bo.getId().getColumnCode());
                                } else if ("vertical".equalsIgnoreCase(bo.getGroupType())) {
                                    listColumGroupMerge.add(bo.getId().getColumnCode());
                                } else if ("sum".equalsIgnoreCase(bo.getGroupType())) {
                                    listColumSum.add(bo.getId().getColumnCode());
                                }
                            }
                            groupAndSumBO.setListColumGroup(listColumGroup);
                            groupAndSumBO.setListColumGroupMerge(listColumGroupMerge);
                            groupAndSumBO.setListColumSum(listColumSum);
                            listColumnGroupAndSum.add(groupAndSumBO);
                        }

                        //Lay ra ngay xuat bao cao
                        List<AdmFuncQueryCondUnMap> listCond = queryBO.getConditions();
                        if (!listCond.isEmpty()) {
                            for (AdmFuncQueryCondUnMap bo : listCond) {
                                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                                    Object value = bo.getValueSearch();
                                    if (value != null) {
                                        reportDate = (Date) value;
                                        strReportDate = dateFormat.format(reportDate);
                                    }
                                }
                                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && ">".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                                    Object value = bo.getValueSearch();
                                    fromDateValue = (Date) value;
                                    if (value != null) {
                                        if ("DatePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            fromDate = dateFormat.format((Date) value);
                                        } else if ("DateTimePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            fromDate = dateTimeForMat.format((Date) value);
                                        }
                                    }
                                }
                                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && ">=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                                    Object value = bo.getValueSearch();
                                    fromDateValue = (Date) value;
                                    if (value != null) {
                                        if ("DatePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            fromDate = dateFormat.format((Date) value);
                                        } else if ("DateTimePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            fromDate = dateTimeForMat.format((Date) value);
                                        }
                                    }
                                }
                                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "<".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                                    Object value = bo.getValueSearch();
                                    toDateValue = (Date) value;
                                    if (value != null) {
                                        if ("DatePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            toDate = dateFormat.format((Date) value);
                                        } else if ("DateTimePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            toDate = dateTimeForMat.format((Date) value);
                                        }
                                    }
                                }
                                if ("Date".equalsIgnoreCase(bo.getCondition().getDataType()) && "<=".equalsIgnoreCase(bo.getCondition().getCompareType())) {
                                    Object value = bo.getValueSearch();
                                    toDateValue = (Date) value;
                                    if (value != null) {
                                        if ("DatePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            toDate = dateFormat.format((Date) value);
                                        } else if ("DateTimePicker".equalsIgnoreCase(bo.getCondition().getControlType())) {
                                            toDate = dateTimeForMat.format((Date) value);
                                        }
                                    }
                                }
                            }
                        }

                        ChartByJfreeBO chartBo = listChartBo.get(listChartBo.size() - 1);

                        if (StringUtils.isNotNull(queryBO.getQuery().getQueryType())) {
                            if ("normal".equalsIgnoreCase(queryBO.getQuery().getQueryType()) || "direct".equalsIgnoreCase(queryBO.getQuery().getQueryType())) {
                                //Query binh thuong
                                List<V2AdmFuncQueryColumnBO> listColumn = queryBO.getColumns();

                                if (!listColumn.isEmpty()) {
                                    List listColumnName = new ArrayList();
                                    List listColumnNameDB = new ArrayList();
                                    List listColumnType = new ArrayList();
                                    List listColumnFormula = new ArrayList();
                                    boolean level1 = false;
                                    boolean level2 = false;
                                    List listOfListColumnTitleMerge = new ArrayList();
                                    List<String> columnNameLevel1 = new ArrayList();
                                    List<String> columnNameLevel2 = new ArrayList();
                                    for (V2AdmFuncQueryColumnBO column : listColumn) {
                                        if (StringUtils.isNotNull(column.getColumnNameL1())) {
                                            level1 = true;
                                            columnNameLevel1.add(getFormatColumn(column.getColumnNameL1(), reportDate, dateFormat, 1));
                                        } else {
                                            columnNameLevel1.add(" ");
                                        }

                                        if (StringUtils.isNotNull(column.getColumnNameL2())) {
                                            level2 = true;
                                            columnNameLevel2.add(getFormatColumn(column.getColumnNameL2(), reportDate, dateFormat, 1));
                                        } else {
                                            columnNameLevel2.add(" ");
                                        }

                                        listColumnNameDB.add(column.getId().getColumnCode());
                                        if (column.getId().getColumnCode().toUpperCase().contains("DAY")) {
                                            listColumnName.add(getFormatColumn(column.getColumnName(), reportDate, dateFormat, 1));
                                        } else {
                                            listColumnName.add(getFormatColumn(column.getColumnName(), reportDate, dateFormat, 1));
                                        }

                                        listColumnFormula.add(column.getFormula());
                                        //Truyen tham so ve bieu do
                                        if (chartBo != null) {
                                            if (column.getChartIndex() != null && "time".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setTimeColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "group".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setGroupColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "label".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setLabelColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "group|label".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setLabelColumn(column.getId().getColumnCode());
                                                chartBo.setGroupColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())) {
                                                List<String> listDrawColumn = chartBo.getDrawColumn();
                                                List<String> listDrawColumnName = chartBo.getDrawColumnName();
                                                List<String> listDrawType = chartBo.getDrawType();
                                                if (listDrawColumn == null) {
                                                    listDrawColumn = new ArrayList();
                                                }
                                                if (listDrawColumnName == null) {
                                                    listDrawColumnName = new ArrayList();
                                                }
                                                if (listDrawType == null) {
                                                    listDrawType = new ArrayList();
                                                }
                                                listDrawColumn.add((String) listColumnNameDB.get(listColumnNameDB.size() - 1));
                                                listDrawColumnName.add((String) listColumnName.get(listColumnName.size() - 1));
                                                listDrawType.add(column.getChartIndex());
                                                chartBo.setDrawColumn(listDrawColumn);
                                                chartBo.setDrawColumnName(listDrawColumnName);
                                                chartBo.setDrawType(listDrawType);
                                            }
                                        }
                                        //End truyen tham so ve bieu do

                                        String dataType = column.getColumnStyle();
                                        if (StringUtils.isNotNull(dataType) && "Date".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Date");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "DateTime".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("DateTime");
                                            listColumnSize.add("20");
                                        } else if (StringUtils.isNotNull(dataType) && "Float".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Float");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Integer".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Number");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Number".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Double");
                                            listColumnSize.add("12");
                                        } else {
                                            listColumnType.add("String");
                                            listColumnSize.add("20");
                                        }
                                    }

                                    listOfListColumnTitle.add(listColumnName);
                                    listOfListColumnNameDB.add(listColumnNameDB);
                                    listOfListColumnType.add(listColumnType);
                                    listOfListColumnFormula.add(listColumnFormula);
                                    //Add nguoc tu cuoi len
                                    if (level2) {
                                        listOfListColumnTitleMerge.add(columnNameLevel2);
                                    }
                                    if (level1) {
                                        listOfListColumnTitleMerge.add(columnNameLevel1);
                                    }
                                    MergTitleBO mergeTitleBO = new MergTitleBO();
                                    mergeTitleBO.setListOfListTitleMerge(listOfListColumnTitleMerge);
                                    mergeTitleBO.setSttRS(listRsData.size() - 1);
                                    listMergeTitleBO.add(mergeTitleBO);
                                }
                            } else if ("cal".equalsIgnoreCase(queryBO.getQuery().getQueryType())) {
                                //Truong hop query special
                                List<V2AdmFuncQueryColCalBO> listColumn = queryBO.getColCals();

                                if (!listColumn.isEmpty()) {
                                    int i = 1;
                                    List listColumnName = new ArrayList();
                                    List listColumnNameDB = new ArrayList();
                                    List listColumnType = new ArrayList();
                                    List listColumnFormula = new ArrayList();
                                    boolean level1 = false;
                                    boolean level2 = false;
                                    List listOfListColumnTitleMerge = new ArrayList();
                                    List<String> columnNameLevel1 = new ArrayList();
                                    List<String> columnNameLevel2 = new ArrayList();
                                    for (V2AdmFuncQueryColCalBO column : listColumn) {
                                        if (StringUtils.isNotNull(column.getColumnNameL1())) {
                                            level1 = true;
                                            columnNameLevel1.add(column.getColumnNameL1());
                                        } else {
                                            columnNameLevel1.add(" ");
                                        }

                                        if (StringUtils.isNotNull(column.getColumnNameL2())) {
                                            level2 = true;
                                            columnNameLevel2.add(column.getColumnNameL2());
                                        } else {
                                            columnNameLevel2.add(" ");
                                        }
                                        if (reportDate != null) {
                                            Date date = new Date();
                                            date.setTime(reportDate.getTime());
                                            if ("GROUP".equalsIgnoreCase(column.getId().getCalType()) || "SQL".equalsIgnoreCase(column.getId().getCalType())) {
                                                listColumnNameDB.add(column.getId().getColumnCode());
                                            } else {
                                                listColumnNameDB.add(column.getId().getCalType() + "_" + column.getId().getColumnCode());
                                            }
                                            Long oneDay = 24 * 60 * 60 * 1000L;
                                            if ("day1".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setTime(reportDate.getTime() - 7 * oneDay);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day2".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setTime(reportDate.getTime() - 6 * oneDay);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day3".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setTime(reportDate.getTime() - 5 * oneDay);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day4".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setTime(reportDate.getTime() - 4 * oneDay);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day5".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setTime(reportDate.getTime() - 3 * oneDay);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day6".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setTime(reportDate.getTime() - 2 * oneDay);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day7".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setTime(reportDate.getTime() - 1 * oneDay);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day8".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                listColumnName.add(dateFormat.format(reportDate));
                                            } else if ("null".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                listColumnName.add(" ");
                                            } else {
                                                listColumnName.add(column.getColumnName());
                                            }
                                        } else {
                                            if ("GROUP".equalsIgnoreCase(column.getId().getCalType())
                                                    || "SQL".equalsIgnoreCase(column.getId().getCalType())) {
                                                listColumnNameDB.add(column.getId().getColumnCode());
                                            } else {
                                                listColumnNameDB.add(column.getId().getCalType() + "_" + column.getId().getColumnCode());
                                            }
                                            listColumnName.add(column.getColumnName());
                                        }
                                        String dataType = column.getColumnStyle();
                                        if (StringUtils.isNotNull(dataType) && "DateTime".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("DateTime");
                                            listColumnSize.add("20");
                                        } else if (StringUtils.isNotNull(dataType) && "Date".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Date");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Float".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Float");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Integer".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Number");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Number".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Double");
                                            listColumnSize.add("12");
                                        } else {
                                            listColumnType.add("String");
                                            listColumnSize.add("20");
                                        }
                                    }
                                    listOfListColumnTitle.add(listColumnName);
                                    listOfListColumnNameDB.add(listColumnNameDB);
                                    listOfListColumnType.add(listColumnType);
                                    listOfListColumnFormula.add(listColumnFormula);
                                    //Add nguoc tu cuoi len
                                    if (level2) {
                                        listOfListColumnTitleMerge.add(columnNameLevel2);
                                    }
                                    if (level1) {
                                        listOfListColumnTitleMerge.add(columnNameLevel1);
                                    }
                                    MergTitleBO mergeTitleBO = new MergTitleBO();
                                    mergeTitleBO.setListOfListTitleMerge(listOfListColumnTitleMerge);
                                    mergeTitleBO.setSttRS(listRsData.size() - 1);
                                    listMergeTitleBO.add(mergeTitleBO);
                                }
                            } else if ("decode".equalsIgnoreCase(queryBO.getQuery().getQueryType())) {
                                //Truong hop query decode
                                List<V2AdmFuncQueryColCalBO> listColumn = queryBO.getColCals();

                                if (!listColumn.isEmpty()) {
                                    int i = 1;
                                    List listColumnName = new ArrayList();
                                    List listColumnNameDB = new ArrayList();
                                    List listColumnType = new ArrayList();
                                    List listColumnFormula = new ArrayList();
                                    boolean level1 = false;
                                    boolean level2 = false;
                                    List listOfListColumnTitleMerge = new ArrayList();
                                    List<String> columnNameLevel1 = new ArrayList();
                                    List<String> columnNameLevel2 = new ArrayList();
                                    for (V2AdmFuncQueryColCalBO column : listColumn) {
                                        if (StringUtils.isNotNull(column.getColumnNameL1())) {
                                            level1 = true;
                                            columnNameLevel1.add(column.getColumnNameL1());
                                        } else {
                                            columnNameLevel1.add(" ");
                                        }

                                        if (StringUtils.isNotNull(column.getColumnNameL2())) {
                                            level2 = true;
                                            columnNameLevel2.add(column.getColumnNameL2());
                                        } else {
                                            columnNameLevel2.add(" ");
                                        }
                                        if (reportDate != null) {
                                            Date date = new Date();
                                            date.setTime(reportDate.getTime());
                                            listColumnNameDB.add(column.getId().getColumnCode());
                                            if ("day1".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setDate(reportDate.getDate() - 7);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day2".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setDate(reportDate.getDate() - 6);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day3".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setDate(reportDate.getDate() - 5);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day4".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setDate(reportDate.getDate() - 4);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day5".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setDate(reportDate.getDate() - 3);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day6".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setDate(reportDate.getDate() - 2);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day7".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                date.setDate(reportDate.getDate() - 1);
                                                listColumnName.add(dateFormat.format(date));
                                            } else if ("day8".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                listColumnName.add(dateFormat.format(reportDate));
                                            } else if ("null".equalsIgnoreCase(column.getId().getColumnCode())) {
                                                listColumnName.add(" ");
                                            } else {
                                                listColumnName.add(column.getColumnName());
                                            }
                                        } else {
                                            listColumnNameDB.add(column.getId().getColumnCode());
                                            listColumnName.add(column.getColumnName());
                                        }
                                        //Truyen tham so ve bieu do
                                        if (chartBo != null) {
                                            if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "group".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setGroupColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "label".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setLabelColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "group|label".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setLabelColumn(column.getId().getColumnCode());
                                                chartBo.setGroupColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())) {
                                                String chartTitle = column.getColumnName();
                                                if (StringUtils.isNotNull(chartTitle)) {
                                                    chartBo.setChartTitle(chartTitle);
                                                }
                                                List<String> listDrawColumn = chartBo.getDrawColumn();
                                                List<String> listDrawColumnName = chartBo.getDrawColumnName();
                                                List<String> listDrawType = chartBo.getDrawType();
                                                if (listDrawColumn == null) {
                                                    listDrawColumn = new ArrayList();
                                                }
                                                if (listDrawColumnName == null) {
                                                    listDrawColumnName = new ArrayList();
                                                }
                                                if (listDrawType == null) {
                                                    listDrawType = new ArrayList();
                                                }
                                                listDrawColumn.add((String) listColumnNameDB.get(listColumnNameDB.size() - 1));
                                                listDrawColumnName.add((String) listColumnName.get(listColumnName.size() - 1));
                                                listDrawType.add(column.getChartIndex());
                                                chartBo.setDrawColumn(listDrawColumn);
                                                chartBo.setDrawColumnName(listDrawColumnName);
                                                chartBo.setDrawType(listDrawType);
                                            }
                                        }
                                        //End truyen tham so ve bieu do

                                        String dataType = column.getColumnStyle();
                                        if (StringUtils.isNotNull(dataType) && "Date".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Date");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "DateTime".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("DateTime");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Float".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Float");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Integer".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Number");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Number".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Double");
                                            listColumnSize.add("12");
                                        } else {
                                            listColumnType.add("String");
                                            listColumnSize.add("20");
                                        }
                                    }
                                    listOfListColumnTitle.add(listColumnName);
                                    listOfListColumnNameDB.add(listColumnNameDB);
                                    listOfListColumnType.add(listColumnType);
                                    listOfListColumnFormula.add(listColumnFormula);
                                    //Add nguoc tu cuoi len
                                    if (level2) {
                                        listOfListColumnTitleMerge.add(columnNameLevel2);
                                    }
                                    if (level1) {
                                        listOfListColumnTitleMerge.add(columnNameLevel1);
                                    }
                                    MergTitleBO mergeTitleBO = new MergTitleBO();
                                    mergeTitleBO.setListOfListTitleMerge(listOfListColumnTitleMerge);
                                    mergeTitleBO.setSttRS(listRsData.size() - 1);
                                    listMergeTitleBO.add(mergeTitleBO);
                                }
                            } else if ("full_decode".equalsIgnoreCase(queryBO.getQuery().getQueryType())) {
                                //Truong hop query decode
                                List<V2AdmFuncQueryColCalBO> listColumn = queryBO.getColCals();

                                if (!listColumn.isEmpty()) {
                                    int i = 1;
                                    List listColumnName = new ArrayList();
                                    List listColumnNameDB = new ArrayList();
                                    List listColumnType = new ArrayList();
                                    List listColumnFormula = new ArrayList();
                                    boolean level1 = false;
                                    boolean level2 = false;
                                    List listOfListColumnTitleMerge = new ArrayList();
                                    List<String> columnNameLevel1 = new ArrayList();
                                    List<String> columnNameLevel2 = new ArrayList();
                                    for (V2AdmFuncQueryColCalBO column : listColumn) {
                                        if (StringUtils.isNotNull(column.getColumnNameL1())) {
                                            level1 = true;
                                            columnNameLevel1.add(getFormatColumn(column.getColumnNameL1(), reportDate, dateFormat, 1));
                                        } else {
                                            columnNameLevel1.add(" ");
                                        }

                                        if (StringUtils.isNotNull(column.getColumnNameL2())) {
                                            level2 = true;
                                            columnNameLevel2.add(getFormatColumn(column.getColumnNameL2(), reportDate, dateFormat, 1));
                                        } else {
                                            columnNameLevel2.add(" ");
                                        }

                                        listColumnNameDB.add(column.getId().getColumnCode());
                                        if (column.getId().getColumnCode().toUpperCase().contains("DAY")) {
                                            listColumnName.add(getFormatColumn(column.getColumnName(), reportDate, dateFormat, 1));
                                        } else {
                                            listColumnName.add(getFormatColumn(column.getColumnName(), reportDate, dateFormat, 1));
                                        }
                                        //Truyen tham so ve bieu do
                                        if (chartBo != null) {
                                            if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "group".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setGroupColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "label".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setLabelColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())
                                                    && "group|label".equalsIgnoreCase(column.getChartIndex())) {
                                                chartBo.setLabelColumn(column.getId().getColumnCode());
                                                chartBo.setGroupColumn(column.getId().getColumnCode());
                                            } else if (StringUtils.isNotNull(column.getChartIndex())) {
                                                String chartTitle = column.getColumnName();
                                                if (StringUtils.isNotNull(chartTitle)) {
                                                    chartBo.setChartTitle(chartTitle);
                                                }
                                                List<String> listDrawColumn = chartBo.getDrawColumn();
                                                List<String> listDrawColumnName = chartBo.getDrawColumnName();
                                                List<String> listDrawType = chartBo.getDrawType();
                                                if (listDrawColumn == null) {
                                                    listDrawColumn = new ArrayList();
                                                }
                                                if (listDrawColumnName == null) {
                                                    listDrawColumnName = new ArrayList();
                                                }
                                                if (listDrawType == null) {
                                                    listDrawType = new ArrayList();
                                                }
                                                listDrawColumn.add((String) listColumnNameDB.get(listColumnNameDB.size() - 1));
                                                listDrawColumnName.add((String) listColumnName.get(listColumnName.size() - 1));
                                                listDrawType.add(column.getChartIndex());
                                                chartBo.setDrawColumn(listDrawColumn);
                                                chartBo.setDrawColumnName(listDrawColumnName);
                                                chartBo.setDrawType(listDrawType);
                                            }
                                        }
                                        //End truyen tham so ve bieu do

                                        String dataType = column.getColumnStyle();
                                        if (StringUtils.isNotNull(dataType) && "Date".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Date");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "DateTime".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("DateTime");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Float".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Float");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Integer".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Number");
                                            listColumnSize.add("12");
                                        } else if (StringUtils.isNotNull(dataType) && "Number".equalsIgnoreCase(dataType)) {
                                            listColumnType.add("Double");
                                            listColumnSize.add("15");
                                        } else {
                                            listColumnType.add("String");
                                            listColumnSize.add("24");
                                        }
                                    }
                                    listOfListColumnTitle.add(listColumnName);
                                    listOfListColumnNameDB.add(listColumnNameDB);
                                    listOfListColumnType.add(listColumnType);
                                    listOfListColumnFormula.add(listColumnFormula);
                                    //Add nguoc tu cuoi len
                                    if (level2) {
                                        listOfListColumnTitleMerge.add(columnNameLevel2);
                                    }
                                    if (level1) {
                                        listOfListColumnTitleMerge.add(columnNameLevel1);
                                    }
                                    MergTitleBO mergeTitleBO = new MergTitleBO();
                                    mergeTitleBO.setListOfListTitleMerge(listOfListColumnTitleMerge);
                                    mergeTitleBO.setSttRS(listRsData.size() - 1);
                                    listMergeTitleBO.add(mergeTitleBO);
                                }
                            }

                        }
                        listChartBo.set(listChartBo.size() - 1, chartBo);
                    }

                }
                V2AdmFuncBO func = v2AdmFuncServiceImpl.findById(functionCode);
                if (!"csv".equalsIgnoreCase(func.getFunctionType()) || func.getFunctionType() == null) {
                    if (!listColumnGroupAndSum.isEmpty()) {
                        reportExcel = new ReportExcel2007();
                    } else {
                        reportExcel = new ReportExcel2007New();
                    }
                    if (StringUtils.isNotNull(strReportDate)) {
                        reportExcel.setStrSubTitle("Ngày: " + strReportDate);
                    }

                    if (StringUtils.isNotNull(fromDate) && StringUtils.isNotNull(toDate)) {
                        reportExcel.setStrSubTitle("Từ: " + fromDate + " - Tới: " + toDate);
                    }
                    reportExcel.setStrCoporationName("Tập đoàn viễn thông quân đội".toUpperCase());
                    reportExcel.setStrCompanyName("Tổng công ty mạng lưới Viettel");
                    reportExcel.setStrNationalName("Cộng hòa xã hội chủ nghĩa Việt Nam");
                    reportExcel.setStrDeclarationName("Độc lập - Tự do - Hạnh phúc");
                    reportExcel.setStrTitle(func.getFunctionTitle().toUpperCase());
                    reportExcel.setListOfListColumnFormula(listOfListColumnFormula);
                    reportExcel.setListOfListColumnTitle(listOfListColumnTitle);        //Set danh sach cac list column Title cho bao cao
                    reportExcel.setListOfListColumnNameDB(listOfListColumnNameDB);        //Set danh sach cac List column cua DB de lay du lieu
                    reportExcel.setListOfListColumnType(listOfListColumnType);        //Set danh sach cac column Type co cac result Set cho bao cao
                    reportExcel.setListColumnSize(listColumnSize);
                    reportExcel.setListRsDataTitle(listRsDataTitle);      //Danh sach cac tieu de cho tung ResultSet
                    reportExcel.setListColumnGroupAndSum(listColumnGroupAndSum); //Danh sach cac column duoc group/sum
                    
                    ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                            .getExternalContext().getContext();
                    
                    String exportFolder = ctx.getRealPath("/") + ".."
                            + MessageUtil.getResourceBundleConfig("auto_delete_dir");
                    if (!StringUtils.isNotNull(exportFolder)) {
                        exportFolder = ctx.getRealPath("/") + ".." + File.separator
                                + ".." + File.separator + ".." + File.separator
                                + "report" + File.separator;
                    }
                    FileUtil.forceFolderExist(exportFolder);
                    reportExcel.setStrFolder(exportFolder);  //Duong dang export bao cao
                    SimpleDateFormat dateFormatFile = new SimpleDateFormat("yyyyMMdd");
//                    String reportName;
                    if (reportDate != null) {
                        reportName = dateFormatFile.format(reportDate)
                                + "_" + functionCode + "_"
                                + System.currentTimeMillis() + ".xlsx";
                    } else if (fromDateValue != null && toDateValue != null) {
                        reportName = dateFormatFile.format(fromDateValue)
                                + "_" + functionCode + "_"
                                + dateFormatFile.format(toDateValue) + "_"
                                + System.currentTimeMillis() + ".xlsx";
                    } else {
                        reportName = dateFormatFile.format(new Date())
                                + "_" + functionCode + "_"
                                + System.currentTimeMillis() + ".xlsx";
                    }
                    reportExcel.setStrFileName(reportName);
                    reportExcel.setListMergTitle(listMergeTitleBO);
                    reportExcel.setListRsData(listRsData);
                    reportExcel.setListChartBo(listChartBo);
                    //Export Many Sheet
                    if (isManySheet) {
                        reportExcel.setListSheetName(listSheetName);
                        reportExcel.setListNumSheetOfRS(listNumSheetOfRS);
                    }
                    String filePath = reportExcel.createReport();

                    String fileDownloadFileName = reportName;
                    File reportFile = new File(filePath);
                    InputStream inputStream = new FileInputStream(reportFile);
                    ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
                    return new DefaultStreamedContent(inputStream, extContext.getMimeType(fileDownloadFileName), fileDownloadFileName);
                } else {
                    if (!listColumnGroupAndSum.isEmpty()) {
                        reportExcel = new ReportExcel2007();
                    } else {
                        reportExcel = new ReportExcel2007New();
                    }
                    if (StringUtils.isNotNull(strReportDate)) {
                        reportExcel.setStrSubTitle("Ngày: " + strReportDate);
                    }

                    if (StringUtils.isNotNull(fromDate) && StringUtils.isNotNull(toDate)) {
                        reportExcel.setStrSubTitle("Từ: " + fromDate + " - Tới: " + toDate);
                    }
                    reportExcel.setStrCoporationName("Tập đoàn viễn thông quân đội".toUpperCase());
                    reportExcel.setStrCompanyName("Tổng công ty mạng lưới Viettel");
                    reportExcel.setStrNationalName("Cộng hòa xã hội chủ nghĩa Việt Nam");
                    reportExcel.setStrDeclarationName("Độc lập - Tự do - Hạnh phúc");
                    
                    ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                            .getExternalContext().getContext();
                    
                    String exportFolder = ctx.getRealPath("/") + ".."
                            + MessageUtil.getResourceBundleConfig("auto_delete_dir");
                    if (!StringUtils.isNotNull(exportFolder)) {
                        exportFolder = ctx.getRealPath("/") + ".." + File.separator
                                + ".." + File.separator + ".." + File.separator
                                + "report" + File.separator;
                    }
                    FileUtil.forceFolderExist(exportFolder);
                    SimpleDateFormat dateFormatFile = new SimpleDateFormat("yyyyMMdd");

                    if (reportDate != null) {
                        reportName = dateFormatFile.format(reportDate)
                                + "_" + functionCode + "_"
                                + System.currentTimeMillis();
                    } else if (fromDateValue != null && toDateValue != null) {
                        reportName = dateFormatFile.format(fromDateValue)
                                + "_" + functionCode + "_"
                                + dateFormatFile.format(toDateValue) + "_"
                                + System.currentTimeMillis();
                    } else {
                        reportName = dateFormatFile.format(new Date())
                                + "_" + functionCode + "_"
                                + System.currentTimeMillis();
                    }
                    if (StringUtils.isNotNull(strReportDate)) {
                        subTitle = "Ngày: " + strReportDate;
                    }

                    if (StringUtils.isNotNull(fromDate) && StringUtils.isNotNull(toDate)) {
                        subTitle = "Từ: " + fromDate + " - Tới: " + toDate;
                    }
                    String filePath = exportFolder + reportName;
                    String zipPath = filePath + ".zip";
                    String fileDownloadFileName = reportCSV.writeCsv(exportFolder, reportName, func.getFunctionTitle().toUpperCase(), subTitle, listOfListColumnNameDB, listOfListColumnTitle, listRsData);
                    File reportFile = new File(zipPath);
                    InputStream inputStream = new FileInputStream(reportFile);
                    ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
                    return new DefaultStreamedContent(inputStream, extContext.getMimeType(fileDownloadFileName), fileDownloadFileName);
                }
            } catch (Exception | Error ex) {
                Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
                MessageUtil.setErrorMessage(ex.getMessage());
            } finally {
                for (ResultSet resultSet : listRsData) {
                    resultSet.close();
                }
                for (NamedParameterStatement ps : listPs) {
                    ps.close();
                }
                try {
                    reportCSV = null;
                    reportExcel = null;
                } catch (Exception ex) {
                    Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(AdmViewDataController.class.getName()).log(Level.SEVERE, null, ex);
            MessageUtil.setErrorMessageFromRes("msg.download.failure");
        }
        return null;
    }

    public String getFormatColumn(String columnName, Date reportDate, SimpleDateFormat dateFormat, int type) {
        if (reportDate != null) {
            Date date = (Date) reportDate.clone();
            Date toDate = (Date) reportDate.clone();
            for (int j = 50; j >= 0; j--) {
                if (columnName.toUpperCase().contains("DAY" + j)) {
                    date.setDate(date.getDate() - j);
                    return columnName.toUpperCase().replace("DAY" + j, dateFormat.format(date));
                } else if ("null".equalsIgnoreCase(columnName)) {
                    return " ";
                }
            }
            for (int j = 10; j >= 0; j--) {
                if (columnName.contains("W" + j)) {
                    date.setDate(date.getDate() - 7 * j);
                    toDate.setDate(toDate.getDate() - 7 * j - 6);
                    return columnName.replace("W" + j, "W" + j + " (" + dateFormat.format(toDate) + " - " + dateFormat.format(date) + ")");
                }
            }
            for (int j = 20; j >= 0; j--) {
                if (columnName.contains("MON" + j)) {
                    date.setMonth(date.getMonth() - j);
                    return columnName.replace("MON" + j, "T" + (date.getMonth() + 1) + "/" + (date.getYear() + 1900));
                }
            }

//            for (int j = 0; j <= 10; j++) {                
//                if (columnName.contains("Q" + j)) {
//                    date.setMonth(date.getMonth() - j*3);
//                    
//                    return columnName.replace("Q" + j, "Q" + ((date.getMonth()+3)/3) + "/" + (date.getYear() + 1900));
//                }
//            }
        }
        return columnName;
    }

    //<editor-fold defaultstate="collapsed" desc="Setter & getter">
    public List<AdmFuncQueryUnMapBO> getListConfigFunc() {
        return listConfigFunc;
    }

    public void setListConfigFunc(List<AdmFuncQueryUnMapBO> listConfigFunc) {
        this.listConfigFunc = listConfigFunc;
    }

    public List<AdmFuncQueryCondUnMap> getListCondsUltility() {
        return listCondsUltility;
    }

    public void setListCondsUltility(List<AdmFuncQueryCondUnMap> listCondsUltility) {
        this.listCondsUltility = listCondsUltility;
    }

    public V2AdmFuncQueryButtonServiceImpl getV2AdmFuncQueryButtonServiceImpl() {
        return v2AdmFuncQueryButtonServiceImpl;
    }

    public void setV2AdmFuncQueryButtonServiceImpl(V2AdmFuncQueryButtonServiceImpl v2AdmFuncQueryButtonServiceImpl) {
        this.v2AdmFuncQueryButtonServiceImpl = v2AdmFuncQueryButtonServiceImpl;
    }

    public V2AdmFuncQueryServiceImpl getV2AdmFuncQueryServiceImpl() {
        return v2AdmFuncQueryServiceImpl;
    }

    public void setV2AdmFuncQueryServiceImpl(V2AdmFuncQueryServiceImpl v2AdmFuncQueryServiceImpl) {
        this.v2AdmFuncQueryServiceImpl = v2AdmFuncQueryServiceImpl;
    }

    public V2AdmFuncQueryCondServiceImpl getV2AdmFuncQueryCondServiceImpl() {
        return v2AdmFuncQueryCondServiceImpl;
    }

    public void setV2AdmFuncQueryCondServiceImpl(V2AdmFuncQueryCondServiceImpl v2AdmFuncQueryCondServiceImpl) {
        this.v2AdmFuncQueryCondServiceImpl = v2AdmFuncQueryCondServiceImpl;
    }

    public V2AdmFuncQueryColumnServiceImpl getV2AdmFuncQueryColumnServiceImpl() {
        return v2AdmFuncQueryColumnServiceImpl;
    }

    public void setV2AdmFuncQueryColumnServiceImpl(V2AdmFuncQueryColumnServiceImpl v2AdmFuncQueryColumnServiceImpl) {
        this.v2AdmFuncQueryColumnServiceImpl = v2AdmFuncQueryColumnServiceImpl;
    }

    public V2AdmFuncQueryColGroupServiceImpl getV2AdmFuncQueryColGroupServiceImpl() {
        return v2AdmFuncQueryColGroupServiceImpl;
    }

    public void setV2AdmFuncQueryColGroupServiceImpl(V2AdmFuncQueryColGroupServiceImpl v2AdmFuncQueryColGroupServiceImpl) {
        this.v2AdmFuncQueryColGroupServiceImpl = v2AdmFuncQueryColGroupServiceImpl;
    }

    public V2AdmFuncQueryCondDependServiceImpl getV2AdmFuncQueryCondDependServiceImpl() {
        return v2AdmFuncQueryCondDependServiceImpl;
    }

    public void setV2AdmFuncQueryCondDependServiceImpl(V2AdmFuncQueryCondDependServiceImpl v2AdmFuncQueryCondDependServiceImpl) {
        this.v2AdmFuncQueryCondDependServiceImpl = v2AdmFuncQueryCondDependServiceImpl;
    }

    public V2AdmFuncServiceImpl getV2AdmFuncServiceImpl() {
        return v2AdmFuncServiceImpl;
    }

    public void setV2AdmFuncServiceImpl(V2AdmFuncServiceImpl v2AdmFuncServiceImpl) {
        this.v2AdmFuncServiceImpl = v2AdmFuncServiceImpl;
    }

    public List<V2AdmFuncBO> getSelectedFunction() {
        return selectedFunction;
    }

    public void setSelectedFunction(List<V2AdmFuncBO> selectedFunction) {
        this.selectedFunction = selectedFunction;
    }

    public List<V2AdmFuncBO> getLstFunctions() {
        return lstFunctions;
    }

    public void setLstFunctions(List<V2AdmFuncBO> lstFunctions) {
        this.lstFunctions = lstFunctions;
    }

    public List<String> getSelectedFunctionCode() {
        return selectedFunctionCode;
    }

    public V2AdmFuncBO getSelectedObj() {
        return selectedObj;
    }

    public void setSelectedObj(V2AdmFuncBO selectedObj) {
        this.selectedObj = selectedObj;
    }

    public void setSelectedFunctionCode(List<String> selectedFunctionCode) {
        this.selectedFunctionCode = selectedFunctionCode;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public List<AdmCommonBO> getListFunctionCommon() {
        return listFunctionCommon;
    }

    public void setListFunctionCommon(List<AdmCommonBO> listFunctionCommon) {
        this.listFunctionCommon = listFunctionCommon;
    }
    //</editor-fold>
}
