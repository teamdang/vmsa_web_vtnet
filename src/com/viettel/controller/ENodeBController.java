package com.viettel.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.ENodeBPlan;
import com.viettel.model.FlowRunAction;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.model.ParamValue;
import com.viettel.model.PlanIpServiceOam;
import com.viettel.model.PlanIpVirtualEthernet;
import com.viettel.model.Province;
import com.viettel.model.ServiceTemplate;
import com.viettel.object.MessageException;
import com.viettel.persistence.ENodeBServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.PlanIpServiceOamServiceImpl;
import com.viettel.persistence.PlanIpVirtualEthernetServiceImpl;
import com.viettel.persistence.ProvinceServiceImpl;
import com.viettel.persistence.ServiceTemplateServiceImpl;
import com.viettel.util.CommonExport;
import com.viettel.util.Config;
import com.viettel.util.Config.ACTION;
import com.viettel.util.Config.NODE_TYPE;
import com.viettel.util.Config.SERVICE;
import com.viettel.util.Config.VERSION;
import com.viettel.util.Importer;
import com.viettel.util.IpUtil;
import com.viettel.util.MessageUtil;
import com.viettel.util.ParamUtil;
import com.viettel.util.ParamUtil.PARAMCODE_ENODEB;
import com.viettel.util.RelationNodeUtil;
import com.viettel.util.SessionUtil;

/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Oct 26, 2016
 * @version 1.0
 */
/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Nov 8, 2016
 * @version 1.0
 */
@SuppressWarnings("serial")
@ManagedBean(name = "eNodeBController")
@ViewScoped
public class ENodeBController implements Serializable {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @ManagedProperty(value = "#{generateFlowRunController}")
    private GenerateFlowRunController generateFlowRunController;

    private LazyDataModel<ENodeBPlan> lazyENodeB;

    @ManagedProperty("#{eNodeBService}")
    private ENodeBServiceImpl eNodeBService;

    private ENodeBPlan currEnodePlan;

    private List<ENodeBPlan> eNodeBPlanImporteds;
    private List<ENodeBPlan> selectedENodeBPlans;

    @PostConstruct
    public void onStart() {
        Object filters = null;
        Map<String, String> orders = new LinkedHashMap<>();
        orders.put("eNodeBName", "ASC");
        eNodeBPlanImporteds = new ArrayList<ENodeBPlan>();
        currEnodePlan = new ENodeBPlan();
        lazyENodeB = new LazyDataModelBaseNew<>(eNodeBService, filters, orders);
    }

    /**
     * Import enode b
     *
     * @author huynx6
     *
     */
    public void handleImportEnodeB(FileUploadEvent event) {
        Importer<ENodeBPlan> excelUtil = new Importer<ENodeBPlan>() {

            @Override
            protected Map<Integer, String> getIndexMapFieldClass() {
                Map<Integer, String> map = new HashMap<>();
                int i = 0;
                map.put(i++, "stationCode");
                map.put(i, "eNodeBName");

                return map;
            }

            @Override
            protected String getDateFormat() {
                return null;
            }

        };
        InputStream inputStream = null;
        try {
            inputStream = event.getFile().getInputstream();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.import.no.inputstream");
        }
        try {
            List<ENodeBPlan> eNodeBPlans = excelUtil.getDatas(inputStream, 0, "2-");
            Map<String, Object> filters = new HashMap<String, Object>();
            filters.put("nodeType.typeId", NODE_TYPE.SRT.value);
            List<String> stationCodes = new ArrayList<>();
            for (ENodeBPlan eNodeBPlan2 : eNodeBPlans) {
                stationCodes.add(eNodeBPlan2.getStationCode());
            }
            filters.put("status", 1L);
            filters.put("stationCode-EXAC", stationCodes);
            filters.put("stationCode-NEQ", null);
            List<Node> nodes = new NodeServiceImpl().findList(filters);
            Multimap<String, Node> mapNodes = ArrayListMultimap.create();
            for (Node node : nodes) {
                mapNodes.put(node.getStationCode(), node);
            }
            List<ENodeBPlan> bPlanNotFounds = new ArrayList<ENodeBPlan>();
            for (Iterator<ENodeBPlan> iterator = eNodeBPlans.iterator(); iterator.hasNext();) {
                ENodeBPlan eNodeBPlan = iterator.next();
                if (mapNodes.containsKey(eNodeBPlan.getStationCode())) {
                    List<Node> collection = (List<Node>) mapNodes.get(eNodeBPlan.getStationCode());
                    if (collection.size() == 1) {
                        eNodeBPlan.setNode(collection.get(0));
                        eNodeBPlan.setDeptManage(collection.get(0).getDepartmentName());;

                    } else {
                        String nodeIds = "";
                        for (Node node : collection) {
                            nodeIds += node.getNodeId().toString() + ",";
                        }
                        nodeIds = nodeIds.replaceAll(",$", "");
                        eNodeBPlan.setNodeIdTemp(nodeIds);
                    }
                } else {
                    iterator.remove();
                    bPlanNotFounds.add(eNodeBPlan);
                }
            }
            generateIP(eNodeBPlans);
            eNodeBPlanImporteds = eNodeBPlans;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            MessageUtil.setWarnMessageFromRes("error.import.no.record");
        }
    }

    /**
     * Quy hoach ip
     *
     * @param eNodeBPlans
     * @author huynx6
     *
     */
    private void generateIP(List<ENodeBPlan> eNodeBPlans) {
        List<ENodeBPlan> eNodeBPlanUnSaves = new ArrayList<ENodeBPlan>();
        for (int i = 0; i < eNodeBPlans.size(); i++) {
            try {
                ENodeBPlan eNodeBPlan = eNodeBPlans.get(i);
                Map<String, Object> filters = new HashMap<String, Object>();
                filters.put("eNodeBName-EXAC", eNodeBPlan.geteNodeBName());
                if (eNodeBService.findList(filters).size() > 0) {
                    continue;
                }
                boolean ipServiceOam = getIpServiceOam(eNodeBPlan);
                if (ipServiceOam) {
                    eNodeBPlan.setIpServiceEnodeb(IpUtil.plusIp(eNodeBPlan.getIpService(), 1));
                    eNodeBPlan.setIpServiceGateway(IpUtil.plusIp(eNodeBPlan.getIpService(), 6));
                    eNodeBPlan.setIpOamEnodeb(IpUtil.plusIp(eNodeBPlan.getIpOam(), 1));
                    eNodeBPlan.setIpOamGateway(IpUtil.plusIp(eNodeBPlan.getIpOam(), 6));
                    Node node = eNodeBPlan.getNode();
                    if (node != null) {
                        eNodeBPlan.setStationCode(node.getStationCode());
                        eNodeBPlan.setDeptManage(node.getDepartmentName());
                    }
                    Transaction tx = null;
                    Session session = null;
                    try {
                        Object[] openTransaction = eNodeBService.openTransaction();
                        tx = (Transaction) openTransaction[1];
                        session = (Session) openTransaction[0];
                        eNodeBService.saveOrUpdate(eNodeBPlan, session, tx, false);
                        new PlanIpServiceOamServiceImpl().execteBulk2("update PlanIpServiceOam set curIdex=curIdex+1 where provinceCode = ?", session, tx, true, eNodeBPlan.getProvinceCode());

                    } catch (Exception e) {
                        eNodeBPlanUnSaves.add(eNodeBPlan);
                        if (tx != null && tx.getStatus() != TransactionStatus.ROLLED_BACK) {
                            tx.rollback();
                        }
                        LOGGER.error(e.getMessage(), e);

                    } finally {
                        if (session != null && session.isOpen()) {
                            session.close();
                        }
                    }
                } else {
                    eNodeBService.saveOrUpdate(eNodeBPlan);
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        MessageUtil.setInfoMessageFromRes("info.save.success");

    }

    public void exportResult() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse servletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            servletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            servletResponse.setHeader("Expires", "0");
            servletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            servletResponse.setHeader("Pragma", "public");
            servletResponse.setHeader("Content-disposition", "attachment;filename=" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + "-exportEnodeB.xlsx");
            String file = CommonExport.getTemplateMultiExport("Template_Export_EnodeB.xlsx");
            try (InputStream is = new FileInputStream(file)) {
                //servletResponse.getOutputStream()
                try (OutputStream os = servletResponse.getOutputStream()) {

                    Context context = new Context();
                    context.putVar("enodebs", this.eNodeBPlanImporteds);
                    JxlsHelper.getInstance().processTemplate(is, os, context);
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        facesContext.responseComplete();
    }

    public void saveEnodeB(ENodeBPlan eNodeBPlan) {
        try {
            eNodeBPlan.setNode(eNodeBPlan.getNode2());
            eNodeBPlan.setDeptManage(eNodeBPlan.getNode2().getDepartmentName());
            eNodeBService.saveOrUpdate(eNodeBPlan);
            MessageUtil.setInfoMessageFromRes("info.save.success");
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("error.save.unsuccess");
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void saveOrUpdate() {
        Transaction tx = null;
        Session session = null;
        try {
            if (currEnodePlan.getIpService() == null || currEnodePlan.getIpOam() == null) {
                getIpServiceOam(currEnodePlan);
            }
            currEnodePlan.setIpServiceEnodeb(IpUtil.plusIp(currEnodePlan.getIpService(), 1));
            currEnodePlan.setIpServiceGateway(IpUtil.plusIp(currEnodePlan.getIpService(), 6));
            currEnodePlan.setIpOamEnodeb(IpUtil.plusIp(currEnodePlan.getIpOam(), 1));
            currEnodePlan.setIpOamGateway(IpUtil.plusIp(currEnodePlan.getIpOam(), 6));
            currEnodePlan.setStationCode(currEnodePlan.getNode().getStationCode());
            currEnodePlan.setDeptManage(currEnodePlan.getNode().getDepartmentName());
            Object[] openTransaction = eNodeBService.openTransaction();
            tx = (Transaction) openTransaction[1];
            session = (Session) openTransaction[0];

            eNodeBService.saveOrUpdate(currEnodePlan, session, tx, false);
            new PlanIpServiceOamServiceImpl().execteBulk2("update PlanIpServiceOam set curIdex=curIdex+1 where provinceCode = ?", session, tx, true, currEnodePlan.getProvinceCode());
            MessageUtil.setInfoMessageFromRes("info.save.success");
        } catch (Exception e) {
            if (tx != null && tx.getStatus() != TransactionStatus.ROLLED_BACK) {
                tx.rollback();
            }
            LOGGER.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.save.unsuccess");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public void preAddEnodeB() {
        currEnodePlan = new ENodeBPlan();
    }

    public void preEditEnodeB(ENodeBPlan bPlan) {
        currEnodePlan = bPlan;
    }

    public void preDeleteEnodeB() {
        if (selectedENodeBPlans != null && selectedENodeBPlans.size() > 0) {
            currEnodePlan = selectedENodeBPlans.get(0);
        } else {
            currEnodePlan = null;
        }
    }

    public void delete() {
        try {
            eNodeBService.delete(currEnodePlan);
            MessageUtil.setInfoMessageFromRes("info.delete.suceess");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.delete.unsuceess");
        }
    }

    public void refreshState(ENodeBPlan eNodeBPlan) {
        try {
            eNodeBPlan.setState("Up");
            eNodeBService.saveOrUpdate(eNodeBPlan);
            Thread.sleep(2000);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void ping() {

    }

    private boolean getIpServiceOam(ENodeBPlan eNodeBPlan) {
        //get ip quy hoach theo tinh
        try {
            Node node;
            if (eNodeBPlan.getNode() == null) {
                List<Node> nodeIdTemps = eNodeBPlan.getNodeIdTemps();
                if (nodeIdTemps.size() > 0) {
                    node = nodeIdTemps.get(0);
                } else {
                    return false;
                }
            } else {
                node = eNodeBPlan.getNode();
            }
            String bccsCode = node.getProvince().getProvinceCode();
            if (bccsCode == null) {
                return false;
            }
            List<Province> provinces = new ProvinceServiceImpl().findList("from Province where bccsCode =?", -1, -1, bccsCode);
            if (provinces.size() != 1) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.plan.ip.not.found"));
            }
            List<PlanIpServiceOam> planIpServiceOams = new PlanIpServiceOamServiceImpl().findList("from PlanIpServiceOam where provinceCode = ? and curIdex < maxIdex-1", -1, -1, provinces.get(0).getProvinceCode());
            if (planIpServiceOams.size() != 1) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.plan.ip.not.found"));
            }
            eNodeBPlan.setProvinceCode(provinces.get(0).getProvinceCode());
            PlanIpServiceOam planIpServiceOam = planIpServiceOams.get(0);
            List<String> cidr2listAddr = IpUtil.cidr2listAddrOctet4div8(planIpServiceOam.getIpService() + planIpServiceOam.getSubnetmask());

            String ipService = cidr2listAddr.get(planIpServiceOam.getCurIdex().intValue() + 1);
            String ipOam = IpUtil.plusIpOctet2(ipService, 10);
            eNodeBPlan.setIpService(ipService);
            eNodeBPlan.setIpOam(ipOam);
            return true;
        } catch (Exception e) {
            if (e instanceof MessageException) {
                MessageUtil.setErrorMessage(e.toString());
            }
            LOGGER.error(e.getMessage(), e);
        }
        return false;
    }

    public Map<String, String> getIpVirtualEthernet(boolean isSubring, Long serviceCode, ENodeBPlan eNodeBPlan) {
        Map<String, String> results = new HashMap<>();
        if (serviceCode == SERVICE.ENODEB_L3.value) {
            return results;
        }
        try {
            Map<String, Object> filter = new HashMap<>();
            filter.put("provinceCode", eNodeBPlan.getProvinceCode());

            List<Province> provinces = new ProvinceServiceImpl().findListExac(filter, null);
            if (provinces.size() != 1) {
                return results;
            }
            List<PlanIpVirtualEthernet> planIpVirtualEthernets = new PlanIpVirtualEthernetServiceImpl().findList("from PlanIpVirtualEthernet where areaCode = ? and curIdex < (maxIdex/?)-1", -1, -1, provinces.get(0).getAreaCode(), isSubring ? 16 : 4);
            if (planIpVirtualEthernets.size() != 1) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.plan.ip.not.found"));
            }
            if (isSubring) {
                List<String> addrs = IpUtil.cidr2listAddrOctet4div16(planIpVirtualEthernets.get(0).getIpL2Subring());
                String ip = addrs.get(planIpVirtualEthernets.get(0).getCurIdex().intValue() + 1);
                results.put(PARAMCODE_ENODEB.IP_CONNECT_ENODEB_AGG_MASTER_MAIN.getValue(), IpUtil.plusIp(ip, 1));
                results.put(PARAMCODE_ENODEB.IP_CONNECT_ENODEB_AGG_BACKUP_MAIN.getValue(), IpUtil.plusIp(ip, 2));
            }
        } catch (Exception e) {
            if (e instanceof MessageException) {
                MessageUtil.setErrorMessage(e.toString());
            }
            LOGGER.error(e.getMessage(), e);
        }
        return results;

    }

    public void deployService() {
        try {
            int countS = 0;
            for (ENodeBPlan eNodeBPlan : selectedENodeBPlans) {
                if (createFlowRunAction(ACTION.NEW.value, eNodeBPlan)) {
                    countS++;
                }
            }
            if (countS == selectedENodeBPlans.size()) {
                MessageUtil.setErrorMessageFromRes("info.create.dt.deploy.4g");
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    protected boolean createFlowRunAction(Long action, ENodeBPlan currEnodePlan) {

        try {
            Map<String, Object> filter = new HashMap<String, Object>();
            Node nodeSrt = currEnodePlan.getNode();

            List<Node> nodes = autoLoadNode(nodeSrt.getNodeCode());
            RelationNodeUtil relationNodeUtil = new RelationNodeUtil();
            int ringType = relationNodeUtil.getRingType(nodes);
            filter.put("id.vendorId", nodeSrt.getVendor().getVendorId());
            filter.put("id.versionId", nodeSrt.getVersion().getVersionId());
            filter.put("id.serviceCode", new Long[]{SERVICE.ENODEB_L2.value, SERVICE.ENODEB_L3.value});
            filter.put("id.action", action);
            if (ringType == ParamUtil.SUB_RING) {
                filter.put("id.subring", 1L);
            } else {
                filter.put("id.subring-NEQ", 1L);
            }
            filter.put("id.provinceCode-EXAC", currEnodePlan.getNode().getProvince().getProvinceCode());
            List<ServiceTemplate> serviceTemplates = new ServiceTemplateServiceImpl().findList(filter, null);
            if (serviceTemplates.size() == 1) {
                ServiceTemplate serviceTemplate = serviceTemplates.get(0);
                FlowTemplates flowTemplates = serviceTemplate.getFlowTemplates();
                if (flowTemplates.getStatus() != 9) {
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                }
                generateFlowRunController = new GenerateFlowRunController();
                FlowRunAction flowRunAction = new FlowRunAction();
                flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
                flowRunAction.setFlowRunName((currEnodePlan.geteNodeBName() + " - " + currEnodePlan.getNode().getNodeCode()));
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());
                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);

                Map<String, String> mapParamValues = autoLoadParams(serviceTemplate.getId().getServiceCode(), nodes, currEnodePlan);

                if (mapParamValues != null) {
                    currEnodePlan.setInterfacePort(mapParamValues.get(PARAMCODE_ENODEB.INTERFACE.getValue()));
                }
                if (serviceTemplate.getId().getServiceCode() == SERVICE.ENODEB_L3.value) {
                    generateFlowRunController.setNodes(new ArrayList<Node>());
                    generateFlowRunController.getNodes().add(nodeSrt);
                } else {
                    generateFlowRunController.setNodes(nodes);

                }
                generateFlowRunController.loadGroupAction(0l);
                for (Node node : generateFlowRunController.getNodes()) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT,node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT,node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            if (mapParamValues != null) {
                                value = mapParamValues.get((paramValue.getParamCode()));
                            }
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString().substring(0, Math.min(250, value.toString().length())));
                        }
                    }
                }

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {
                    if (currEnodePlan.getInterfacePort() != null) {
                        eNodeBService.execteBulk("update ENodeBPlan set interfacePort = ? where id = ?", currEnodePlan.getInterfacePort(), currEnodePlan.getId());
                    }

                }
                return saveDT;
            } else {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.found"));
            }

        } catch (Exception e) {
            if (e instanceof MessageException) {
                MessageUtil.setErrorMessage(e.getMessage());
            } else {
                LOGGER.error(e.getMessage(), e);
            }
        }

        return false;
    }

    private List<Node> autoLoadNode(String srtNodecode) throws Exception {
        RelationNodeUtil relationNodeUtil = new RelationNodeUtil();
        List<Node> ringSrt = relationNodeUtil.getRingSrt(srtNodecode);
        if (ringSrt == null) {
            throw new MessageException(relationNodeUtil.getMessageInfo());
        }
        return ringSrt;
    }

    private Map<String, String> autoLoadParams(Long serviceCode, List<Node> nodeinRings, ENodeBPlan enodeBPlan) throws Exception {
        Map<String, String> mapParamValues = new HashMap<String, String>();
        RelationNodeUtil relationNodeUtil = new RelationNodeUtil();

        try {
            try {

                if (nodeinRings != null) {
                    if (serviceCode == SERVICE.ENODEB_L3.value) {
                        mapParamValues = relationNodeUtil.getParamL3For4G(nodeinRings, enodeBPlan);
                    } else if (serviceCode == SERVICE.ENODEB_L2.value) {
                        mapParamValues = relationNodeUtil.getParamL2For4G(nodeinRings, enodeBPlan);

                    }
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        if (relationNodeUtil.getMessageInfo() != null) {
            throw new Exception(relationNodeUtil.getMessageInfo());
        }

        return mapParamValues;
    }

    public StreamedContent getTemplateFileImport() {
        try {
            String path = CommonExport.getTemplateMultiExport("Template_Import_EnodeB.xlsx");
            File importFile = new File(path);
            if (!importFile.exists()) {
                return null;
            }
            InputStream input = new FileInputStream(importFile);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            return new DefaultStreamedContent(input, externalContext.getMimeType(importFile.getName()), importFile.getName());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public GenerateFlowRunController getGenerateFlowRunController() {
        return generateFlowRunController;
    }

    public void setGenerateFlowRunController(GenerateFlowRunController generateFlowRunController) {
        this.generateFlowRunController = generateFlowRunController;
    }

    public LazyDataModel<ENodeBPlan> getLazyENodeB() {
        return lazyENodeB;
    }

    public void setLazyENodeB(LazyDataModel<ENodeBPlan> lazyENodeB) {
        this.lazyENodeB = lazyENodeB;
    }

    public ENodeBServiceImpl geteNodeBService() {
        return eNodeBService;
    }

    public void seteNodeBService(ENodeBServiceImpl eNodeBService) {
        this.eNodeBService = eNodeBService;
    }

    public ENodeBPlan getCurrEnodePlan() {
        return currEnodePlan;
    }

    public void setCurrEnodePlan(ENodeBPlan currEnodePlan) {
        this.currEnodePlan = currEnodePlan;
    }
    //Test
    private volatile int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void increment() {
        count++;

        EventBus eventBus = EventBusFactory.getDefault().eventBus();
        eventBus.publish("/counter", String.valueOf(count));
    }

    public List<ENodeBPlan> getSelectedENodeBPlans() {
        return selectedENodeBPlans;
    }

    public void setSelectedENodeBPlans(List<ENodeBPlan> selectedENodeBPlans) {
        this.selectedENodeBPlans = selectedENodeBPlans;
    }
}
