package com.viettel.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.viettel.model.*;
import com.viettel.persistence.*;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.viettel.controller.RescueInfomationController.logger;

import com.viettel.exception.AppException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.object.MessageException;
import com.viettel.util.Config;
import com.viettel.util.Constants;
import com.viettel.util.Importer;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionUtil;
import com.viettel.util.SessionWrapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * @author Dunglv
 * @version 1.0
 * @sin Mar 10, 2017
 */
@SuppressWarnings("serial")
@ManagedBean(name = "changeHrController")
@ViewScoped
public class ChangeHrController implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="Param">

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private LazyDataModel<ChangeHrSdResource> lazyChangeHrSdResource;
    @ManagedProperty("#{changeHrSdResourceService}")
    private ChangeHrSdResourceServiceImpl changeHrSdResourceServiceImpl;
    @ManagedProperty("#{changeFlowRunActionService}")
    private ChangeFlowRunActionServiceImpl changeFlowRunActionServiceImpl;
    @ManagedProperty("#{changePlanService}")
    private ChangePlanServiceImpl changePlanService;
    private List<ChangeHrSdResource> selectedChangeHrSdResources;
    private LazyDataModel<ChangeFlowRunAction> lazyChangeDT;
    private int eventType = Constants.EventType.CHANGE_HR_DAILY;
    ;
    private ChangePlan currChangePlan;
    private List<Vendor> vendors;
    private LazyDataModel<ChangePlan> lazyChangePlan;
    private List<ChangePlan> selectChangePlans = new ArrayList<>();
    private Long hotspot1SDRightValue = 200L;
    private Long hotspot2SDLeftValue = 200L;
    private Long hotspot2SDRightValue = 250L;
    private Long hotspot3SDLeftValue = 250L;
    private Long normal1SDRightValue = 80L;
    private Long normal2SDLeftValue = 80L;
    private Long normal2SDRightValue = 120L;
    private Long normal3SDLeftValue = 120L;
    private Long normal3SDRightValue = 150L;
    private Long normal4SDLeftValue = 150L;
    private Long normal1HRRightValue = 90L;
    private Long normal1HRValue = 20L;
    private Long normal2HRLeftValue = 90L;
    private Long normal2HRRightValue = 120L;
    private Long normal2HRValue = 30L;
    private Long normal3HRLeftValue = 120L;
    private Long normal3HRRightValue = 150L;
    private Long normal3HRValue = 50L;
    private Long normal4HRLeftValue = 150L;
    private Long normal4HRRightValue = 180L;
    private Long normal4HRValue = 70L;
    private Long normal5HRLeftValue = 180L;
    private Long normal5HRValue = 90L;

    private String hotspot1SDValue = "2 * NTRx";
    private String hotspot2SDValue = "1 + 2 * NTRx";
    private String hotspot3SDValue = "2 + 2 * NTRx";
    private String normal1SDValue = "NTRx";
    private String normal2SDValue = "NTRx + 1";
    private String normal3SDValue = "1.5 * NTRx";
    private String normal4SDValue = "2 * NTRx";

    public String getHotspot1SDValue() {
        return hotspot1SDValue;
    }

    public void setHotspot1SDValue(String hotspot1SDValue) {
        this.hotspot1SDValue = hotspot1SDValue;
    }

    public String getHotspot2SDValue() {
        return hotspot2SDValue;
    }

    public void setHotspot2SDValue(String hotspot2SDValue) {
        this.hotspot2SDValue = hotspot2SDValue;
    }

    public String getHotspot3SDValue() {
        return hotspot3SDValue;
    }

    public void setHotspot3SDValue(String hotspot3SDValue) {
        this.hotspot3SDValue = hotspot3SDValue;
    }

    public String getNormal1SDValue() {
        return normal1SDValue;
    }

    public void setNormal1SDValue(String normal1SDValue) {
        this.normal1SDValue = normal1SDValue;
    }

    public String getNormal2SDValue() {
        return normal2SDValue;
    }

    public void setNormal2SDValue(String normal2SDValue) {
        this.normal2SDValue = normal2SDValue;
    }

    public String getNormal3SDValue() {
        return normal3SDValue;
    }

    public void setNormal3SDValue(String normal3SDValue) {
        this.normal3SDValue = normal3SDValue;
    }

    public String getNormal4SDValue() {
        return normal4SDValue;
    }

    public void setNormal4SDValue(String normal4SDValue) {
        this.normal4SDValue = normal4SDValue;
    }

    public Long getHotspot1SDRightValue() {
        return hotspot1SDRightValue;
    }

    public void setHotspot1SDRightValue(Long hotspot1SDRightValue) {
        this.hotspot1SDRightValue = hotspot1SDRightValue;
    }

    public Long getHotspot2SDLeftValue() {
        return hotspot2SDLeftValue;
    }

    public void setHotspot2SDLeftValue(Long hotspot2SDLeftValue) {
        this.hotspot2SDLeftValue = hotspot2SDLeftValue;
    }

    public Long getHotspot2SDRightValue() {
        return hotspot2SDRightValue;
    }

    public void setHotspot2SDRightValue(Long hotspot2SDRightValue) {
        this.hotspot2SDRightValue = hotspot2SDRightValue;
    }

    public Long getHotspot3SDLeftValue() {
        return hotspot3SDLeftValue;
    }

    public void setHotspot3SDLeftValue(Long hotspot3SDLeftValue) {
        this.hotspot3SDLeftValue = hotspot3SDLeftValue;
    }

    public Long getNormal1SDRightValue() {
        return normal1SDRightValue;
    }

    public void setNormal1SDRightValue(Long normal1SDRightValue) {
        this.normal1SDRightValue = normal1SDRightValue;
    }

    public Long getNormal2SDLeftValue() {
        return normal2SDLeftValue;
    }

    public void setNormal2SDLeftValue(Long normal2SDLeftValue) {
        this.normal2SDLeftValue = normal2SDLeftValue;
    }

    public Long getNormal2SDRightValue() {
        return normal2SDRightValue;
    }

    public void setNormal2SDRightValue(Long normal2SDRightValue) {
        this.normal2SDRightValue = normal2SDRightValue;
    }

    public Long getNormal3SDLeftValue() {
        return normal3SDLeftValue;
    }

    public void setNormal3SDLeftValue(Long normal3SDLeftValue) {
        this.normal3SDLeftValue = normal3SDLeftValue;
    }

    public Long getNormal3SDRightValue() {
        return normal3SDRightValue;
    }

    public void setNormal3SDRightValue(Long normal3SDRightValue) {
        this.normal3SDRightValue = normal3SDRightValue;
    }

    public Long getNormal4SDLeftValue() {
        return normal4SDLeftValue;
    }

    public void setNormal4SDLeftValue(Long normal4SDLeftValue) {
        this.normal4SDLeftValue = normal4SDLeftValue;
    }

    public Long getNormal1HRRightValue() {
        return normal1HRRightValue;
    }

    public void setNormal1HRRightValue(Long normal1HRRightValue) {
        this.normal1HRRightValue = normal1HRRightValue;
    }

    public Long getNormal1HRValue() {
        return normal1HRValue;
    }

    public void setNormal1HRValue(Long normal1HRValue) {
        this.normal1HRValue = normal1HRValue;
    }

    public Long getNormal2HRLeftValue() {
        return normal2HRLeftValue;
    }

    public void setNormal2HRLeftValue(Long normal2HRLeftValue) {
        this.normal2HRLeftValue = normal2HRLeftValue;
    }

    public Long getNormal2HRRightValue() {
        return normal2HRRightValue;
    }

    public void setNormal2HRRightValue(Long normal2HRRightValue) {
        this.normal2HRRightValue = normal2HRRightValue;
    }

    public Long getNormal2HRValue() {
        return normal2HRValue;
    }

    public void setNormal2HRValue(Long normal2HRValue) {
        this.normal2HRValue = normal2HRValue;
    }

    public Long getNormal3HRLeftValue() {
        return normal3HRLeftValue;
    }

    public void setNormal3HRLeftValue(Long normal3HRLeftValue) {
        this.normal3HRLeftValue = normal3HRLeftValue;
    }

    public Long getNormal3HRRightValue() {
        return normal3HRRightValue;
    }

    public void setNormal3HRRightValue(Long normal3HRRightValue) {
        this.normal3HRRightValue = normal3HRRightValue;
    }

    public Long getNormal3HRValue() {
        return normal3HRValue;
    }

    public void setNormal3HRValue(Long normal3HRValue) {
        this.normal3HRValue = normal3HRValue;
    }

    public Long getNormal4HRLeftValue() {
        return normal4HRLeftValue;
    }

    public void setNormal4HRLeftValue(Long normal4HRLeftValue) {
        this.normal4HRLeftValue = normal4HRLeftValue;
    }

    public Long getNormal4HRRightValue() {
        return normal4HRRightValue;
    }

    public void setNormal4HRRightValue(Long normal4HRRightValue) {
        this.normal4HRRightValue = normal4HRRightValue;
    }

    public Long getNormal4HRValue() {
        return normal4HRValue;
    }

    public void setNormal4HRValue(Long normal4HRValue) {
        this.normal4HRValue = normal4HRValue;
    }

    public Long getNormal5HRLeftValue() {
        return normal5HRLeftValue;
    }

    public void setNormal5HRLeftValue(Long normal5HRLeftValue) {
        this.normal5HRLeftValue = normal5HRLeftValue;
    }

    public Long getNormal5HRValue() {
        return normal5HRValue;
    }

    public void setNormal5HRValue(Long normal5HRValue) {
        this.normal5HRValue = normal5HRValue;
    }

    public ChangePlanServiceImpl getChangePlanService() {
        return changePlanService;
    }

    public void setChangePlanService(ChangePlanServiceImpl changePlanService) {
        this.changePlanService = changePlanService;
    }

    public LazyDataModel<ChangePlan> getLazyChangePlan() {
        return lazyChangePlan;
    }

    public void setLazyChangePlan(LazyDataModel<ChangePlan> lazyChangePlan) {
        this.lazyChangePlan = lazyChangePlan;
    }

    public List<ChangePlan> getSelectChangePlans() {
        return selectChangePlans;
    }

    public void setSelectChangePlans(List<ChangePlan> selectChangePlans) {
        this.selectChangePlans = selectChangePlans;
    }

    public LazyDataModel<ChangeHrSdResource> getLazyChangeHrSdResource() {
        return lazyChangeHrSdResource;
    }

    public void setLazyChangeHrSdResource(LazyDataModel<ChangeHrSdResource> lazyChangeHrSdResource) {
        this.lazyChangeHrSdResource = lazyChangeHrSdResource;
    }

    public ChangeHrSdResourceServiceImpl getChangeHrSdResourceServiceImpl() {
        return changeHrSdResourceServiceImpl;
    }

    public ChangePlan getCurrChangePlan() {
        return currChangePlan;
    }

    public void setCurrChangePlan(ChangePlan currChangePlan) {
        this.currChangePlan = currChangePlan;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set&Get">
    public void setChangeHrSdResourceServiceImpl(ChangeHrSdResourceServiceImpl changeHrSdResourceServiceImpl) {
        this.changeHrSdResourceServiceImpl = changeHrSdResourceServiceImpl;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public List<ChangeHrSdResource> getSelectedChangeHrSdResources() {
        return selectedChangeHrSdResources;
    }

    public void setSelectedChangeHrSdResources(List<ChangeHrSdResource> selectedChangeHrSdResources) {
        this.selectedChangeHrSdResources = selectedChangeHrSdResources;
    }

    public LazyDataModel<ChangeFlowRunAction> getLazyChangeDT() {
        return lazyChangeDT;
    }

    public void setLazyChangeDT(LazyDataModel<ChangeFlowRunAction> lazyChangeDT) {
        this.lazyChangeDT = lazyChangeDT;
    }

    public ChangeFlowRunActionServiceImpl getChangeFlowRunActionServiceImpl() {
        return changeFlowRunActionServiceImpl;
    }

    public void setChangeFlowRunActionServiceImpl(ChangeFlowRunActionServiceImpl changeFlowRunActionServiceImpl) {
        this.changeFlowRunActionServiceImpl = changeFlowRunActionServiceImpl;
    }

    //</editor-fold>
    @PostConstruct
    public void onStart() {
        try {
            Map<String, String> orderVendor = new LinkedHashMap<>();
            orderVendor.put("vendorName", "ASC");
            vendors = new VendorServiceImpl().findList(null, orderVendor);
            loadChangeHrSdResourceByType();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    /**
     * Import plan
     *
     * @author quytv7
     */
    //<editor-fold defaultstate="collapsed" desc="Plan">
    public void preAddChangePlan() {
        currChangePlan = new ChangePlan();

    }

    public void loadChangeHrSdResourceByType() {
        Map<String, Object> filters = new HashMap<>();
        Map<String, String> orders = new LinkedHashMap<>();
        Map<String, Object> filtersForForceChangePlan = new HashMap<>();
        Map<String, String> ordersForForceChangePlan = new LinkedHashMap<>();
        orders.put("updateTime", "DESC");
        orders.put("rncBsc", "ASC");
        filtersForForceChangePlan.put("eventType", "5");
        ordersForForceChangePlan.put("updateTime", "DESC");
        ordersForForceChangePlan.put("crNumber", "ASC");
        switch (eventType) {
            case Constants.EventType.CHANGE_HR_DAILY:
                filters.put("type", 0L);
                filters.put("scr", 0D);
                filters.put("tcr-GT", 0D);
                lazyChangeHrSdResource = new LazyDataModelBaseNew<>(changeHrSdResourceServiceImpl, filters, orders);
                break;
            case Constants.EventType.CHANGE_HR_WEEKLY:
                filters.put("type", 1L);
                filters.put("countTcr", 7L);
                lazyChangeHrSdResource = new LazyDataModelBaseNew<>(changeHrSdResourceServiceImpl, filters, orders);
                break;
            case Constants.EventType.CHANGE_SD_DAILY:
                filters.put("type", 0L);
                filters.put("tcr", 0D);
                filters.put("scr-GT", 0D);
                lazyChangeHrSdResource = new LazyDataModelBaseNew<>(changeHrSdResourceServiceImpl, filters, orders);
                break;
            case Constants.EventType.CHANGE_SD_WEEKLY:
                filters.put("type", 1L);
                filters.put("countScr", 7L);
                lazyChangeHrSdResource = new LazyDataModelBaseNew<>(changeHrSdResourceServiceImpl, filters, orders);
                break;
            case Constants.EventType.CHANGE_FORCE_HR_SD:
                lazyChangePlan = new LazyDataModelBaseNew<>(changePlanService, filtersForForceChangePlan, ordersForForceChangePlan);
                break;
        }
    }

    public String getEventTypeStr() {
        switch (eventType) {
            case Constants.EventType.CHANGE_HR_DAILY:
                return MessageUtil.getResourceBundleMessage("label.change.hr.daily");
            case Constants.EventType.CHANGE_HR_WEEKLY:
                return MessageUtil.getResourceBundleMessage("label.change.hr.weekly");
            case Constants.EventType.CHANGE_SD_DAILY:
                return MessageUtil.getResourceBundleMessage("label.change.sd.daily");
            case Constants.EventType.CHANGE_SD_WEEKLY:
                return MessageUtil.getResourceBundleMessage("label.change.sd.weekly");
            case Constants.EventType.CHANGE_FORCE_HR_SD:
                return MessageUtil.getResourceBundleMessage("label.change.force.sd.hr");
            default:
                return MessageUtil.getResourceBundleMessage("label.change.hr.daily");
        }
    }

    public void createChangeDT() throws Exception {
        try {
            //Lay cau hinh import
            Map<String, Object> filters = new HashMap<String, Object>();
            filters.put("fileName", Constants.ChangeDTType.CHANGE_HR_SD);
//            List<StationConfigImport> stationConfigImports = new ArrayList<>();

            HashMap<String, HashMap<String, String>> rncChangeHrSdResourceParamMap = new HashMap<>();
            HashMap<String, String> changeHrSdResourceParamMap;
            String dtName = "";
            switch (eventType) {
                case Constants.EventType.CHANGE_HR_DAILY:
                    filters.put("sheetName", Constants.ChangeDTType.CHANGE_HR_DAILY);
//                    stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);

                    for (ChangeHrSdResource changeHrSdResource : selectedChangeHrSdResources) {
                        String hr;
//                        if (changeHrSdResource.getTuNonHr() > 180) {
//                            hr = "90";
//                        } else if (changeHrSdResource.getTuNonHr() > 150) {
//                            hr = "70";
//                        } else if (changeHrSdResource.getTuNonHr() > 120) {
//                            hr = "50";
//                        } else if (changeHrSdResource.getTuNonHr() > 100) {
//                            hr = "30";
//                        } else {
//                            hr = "-1";
//                        }

                        // Build param for new mop
                        if (null == rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc())) {
                            changeHrSdResourceParamMap = new HashMap<>();
                            changeHrSdResourceParamMap.put("cell", changeHrSdResource.getCellCode());
                            changeHrSdResourceParamMap.put("tu_non_hr", changeHrSdResource.getTuNonHr().toString());
                            changeHrSdResourceParamMap.put("tch_congestion", changeHrSdResource.getTchCongestion().toString());
                            rncChangeHrSdResourceParamMap.put(changeHrSdResource.getRncBsc(), changeHrSdResourceParamMap);
                        } else {
                            String cells = rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).get("cell") + ";" + changeHrSdResource.getCellCode();
                            String tuNonHrs = rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).get("tu_non_hr") + ";" + changeHrSdResource.getTuNonHr().toString();
                            String tchCongestion = rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).get("tch_congestion") + ";" + changeHrSdResource.getTchCongestion().toString();
                            rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).put("cell", cells);
                            rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).put("tu_non_hr", tuNonHrs);
                            rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).put("tch_congestion", tchCongestion);
                        }
                    }
                    dtName = Constants.ChangeDTType.CHANGE_HR_DAILY;
                    break;
                case Constants.EventType.CHANGE_HR_WEEKLY:
                    filters.put("sheetName", Constants.ChangeDTType.CHANGE_HR_WEEKLY);
//                    stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);

                    for (ChangeHrSdResource changeHrSdResource : selectedChangeHrSdResources) {
                        // Build param for new mop
                        if (null == rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc())) {
                            changeHrSdResourceParamMap = new HashMap<>();
                            changeHrSdResourceParamMap.put("cell", changeHrSdResource.getCellCode());
                            rncChangeHrSdResourceParamMap.put(changeHrSdResource.getRncBsc(), changeHrSdResourceParamMap);
                        } else {
                            String cells = rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).get("cell") + ";" + changeHrSdResource.getCellCode();
                            rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).put("cell", cells);
                        }
                    }
                    dtName = Constants.ChangeDTType.CHANGE_HR_WEEKLY;
                    break;
                case Constants.EventType.CHANGE_SD_DAILY:
                    filters.put("sheetName", Constants.ChangeDTType.CHANGE_SD_DAILY);
//                    stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                    for (ChangeHrSdResource changeHrSdResource : selectedChangeHrSdResources) {
                        long maxSd = (2 * changeHrSdResource.getTrx()) + 2;

                        // Build param for new mop
                        if (null == rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc())) {
                            changeHrSdResourceParamMap = new HashMap<>();
                            changeHrSdResourceParamMap.put("cell", changeHrSdResource.getCellCode());
                            changeHrSdResourceParamMap.put("maxsd", String.valueOf(maxSd));
                            rncChangeHrSdResourceParamMap.put(changeHrSdResource.getRncBsc(), changeHrSdResourceParamMap);
                        } else {
                            String cells = rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).get("cell") + ";" + changeHrSdResource.getCellCode();
                            String maxSds = rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).get("maxsd") + ";" + String.valueOf(maxSd);
                            rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).put("cell", cells);
                            rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).put("maxsd", maxSds);
                        }
                    }
                    dtName = Constants.ChangeDTType.CHANGE_SD_DAILY;
                    break;
                case Constants.EventType.CHANGE_SD_WEEKLY:
                    filters.put("sheetName", Constants.ChangeDTType.CHANGE_SD_WEEKLY);
//                    stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                    for (ChangeHrSdResource changeHrSdResource : selectedChangeHrSdResources) {
                        // Build param for new mop
                        if (null == rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc())) {
                            changeHrSdResourceParamMap = new HashMap<>();
                            changeHrSdResourceParamMap.put("cell", changeHrSdResource.getCellCode());
                            rncChangeHrSdResourceParamMap.put(changeHrSdResource.getRncBsc(), changeHrSdResourceParamMap);
                        } else {
                            String cells = rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).get("cell") + ";" + changeHrSdResource.getCellCode();
                            rncChangeHrSdResourceParamMap.get(changeHrSdResource.getRncBsc()).put("cell", cells);
                        }
                    }
                    dtName = Constants.ChangeDTType.CHANGE_SD_WEEKLY;
                    break;
            }

            createFlowRunActionChangeParam(dtName, rncChangeHrSdResourceParamMap);
            RequestContext.getCurrentInstance().execute("PF('comfirmCreateChangeDT').hide()");
        } catch (Exception ex) {
            if (ex instanceof MessageException) {
                MessageUtil.setErrorMessage(ex.getMessage());
            } else {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }

    private boolean createFlowRunActionChangeParam(String dtName, HashMap<String, HashMap<String, String>> rncChangeHrSdResourceParamMap) throws MessageException, Exception {
        GenerateFlowRunController generateFlowRunController;
        List<ChangeFlowRunAction> changeFlowRunActions = new ArrayList<>();
        FlowTemplates flowTemplates = null;

        try {
            for (String rnc : rncChangeHrSdResourceParamMap.keySet()) {
                //Quytv7 sua code phan chia theo tung vendor version
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode", rnc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new Exception("Khong tim thay node mang hoac tim thay lon hon 1 ban ghi");
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                filters.clear();
                filters.put("vendorName", nodeRncBsc.getVendor().getVendorName());
                filters.put("isRule", Config.IS_RULE_CHANGE_FORCE);
                filters.put("sheetName", dtName);
                List<StationConfigImport> stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);

                flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());

                if (flowTemplates == null) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                    continue;
                } else {
                    if (flowTemplates.getStatus() != 9) {
                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                        continue;
                    }
                }

                try {
                    String filePutOSS;
                    filePutOSS = "CHANGE_ " + dtName.toUpperCase()
                            + "-BSC_" + rnc.toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                    filters.clear();
                    filters.put("vendor.vendorId", nodeRncBsc.getVendor().getVendorId());
                    filters.put("proficientType", Config.IS_RULE_CHANGE_FORCE);
                    List<StationParamDefault> lstParamDefault = new StationParamDefaultServiceImpl().findList(filters);
                    HashMap<String, String> mapParamValues;
                    mapParamValues = rncChangeHrSdResourceParamMap.get(rnc);
                    mapParamValues.put("file_name", filePutOSS);
                    mapParamValues.put("rnc", rnc);
                    mapParamValues.put("cellname", mapParamValues.get("cell"));
                    calculateParamDetail(mapParamValues,lstParamDefault,logger);


                    generateFlowRunController = new GenerateFlowRunController();
                    FlowRunAction flowRunAction = new FlowRunAction();
                    flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
                    flowRunAction.setFlowRunName(filePutOSS);
                    while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                        flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                    }
                    flowRunAction.setTimeRun(new Date());
                    flowRunAction.setFlowTemplates(flowTemplates);
                    flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());

                    generateFlowRunController.setFlowRunAction(flowRunAction);
                    generateFlowRunController.setSelectedFlowTemplates(flowTemplates);

                    //Lay danh sach param tu bang 
                    generateFlowRunController.setNodes(new ArrayList<Node>());

                    generateFlowRunController.loadGroupAction(0l);
                    List<Node> nodeInPlan;


                    nodeInPlan = new ArrayList<>();
                    generateFlowRunController.getNodes().add(nodeRncBsc);
                    nodeInPlan.add(nodeRncBsc);

                    for (Node node : nodeInPlan) {
                        generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                        List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                        for (ParamValue paramValue : paramValues) {
                            LOGGER.info("Show ParamCode: " + paramValue.getParamCode());

                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }

                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }

                            ResourceBundle bundle = ResourceBundle.getBundle("cas");
                            if (bundle.getString("service").contains("10.61.127.190")) {
                                if (value == null || value.toString().isEmpty()) {
                                    value = "TEST_NOT_FOUND";
                                }
                            }
                            if (value != null) {
                                paramValue.setParamValue(value.toString());
                            }
                        }
                    }
                    mapParamValues.clear();

                    boolean saveDT = generateFlowRunController.saveDT();
                    if (saveDT) {
                        try {
                            // Save flow to database                           
                            ChangeFlowRunAction changeFlowRunAction = new ChangeFlowRunAction();
                            changeFlowRunAction.setFlowRunAction(flowRunAction);
                            changeFlowRunAction.setNode(nodeRncBsc);
                            changeFlowRunAction.setType(2L);
                            changeFlowRunAction.setUpdateTime(new Date());
                            changeFlowRunActions.add(changeFlowRunAction);
                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), rnc));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                    }
                } catch (Exception e) {
                    if (e instanceof MessageException) {
                        MessageUtil.setErrorMessage(e.getMessage());
                    } else {
                        LOGGER.error(e.getMessage(), e);
                    }
                    throw new Exception(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                } finally {
                    //Tam biet
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
        changeFlowRunActionServiceImpl.saveOrUpdate(changeFlowRunActions);
        return true;
    }

    public void handleImportChangePlan(FileUploadEvent event) throws Exception {
        HashMap<String, List<?>> mapCellObjectImports = new HashMap<String, List<?>>();
        try {
            if (!validateInput(currChangePlan)) {
                MessageUtil.setWarnMessageFromRes("error.import.data.input");
                throw new Exception(MessageUtil.getResourceBundleConfig("error.import.data.input"));
            }
            //Lay cau hinh import
            Map<String, Object> filters = new HashMap<String, Object>();
            filters.put("vendorName", currChangePlan.getVendor().getVendorName());
            filters.put("networkType", currChangePlan.getNetworkType());
            filters.put("isRule", Config.IS_RULE_CHANGE_FORCE);
            List<StationConfigImport> stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
            final HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile = new HashMap<>();
            HashMap<String, List<StationConfigImport>> mapImportSheet;
            List<StationConfigImport> lstStationConfigImports;
            for (StationConfigImport stationConfigImport : stationConfigImports) {
                if (mapImportFile.containsKey(stationConfigImport.getFileName())) {
                    mapImportSheet = mapImportFile.get(stationConfigImport.getFileName());
                    if (mapImportSheet.containsKey(stationConfigImport.getSheetName())) {
                        mapImportSheet.get(stationConfigImport.getSheetName()).add(stationConfigImport);
                    } else {
                        lstStationConfigImports = new ArrayList<>();
                        lstStationConfigImports.add(stationConfigImport);
                        mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                    }
                    mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
                } else {
                    mapImportSheet = new HashMap<>();
                    lstStationConfigImports = new ArrayList<>();
                    lstStationConfigImports.add(stationConfigImport);
                    mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                    mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
                }
            }
            if (mapImportFile.containsKey(event.getFile().getFileName())) {
                Workbook workbook = null;
                try {
                    InputStream inputstream = event.getFile().getInputstream();

                    if (inputstream == null) {
                        throw new NullPointerException("inputstream is null");
                    }
                    //Get the workbook instance for XLS/xlsx file 
                    try {
                        workbook = WorkbookFactory.create(inputstream);
//                        if (workbook == null) {
//                            throw new NullPointerException("workbook is null");
//                        }
                    } catch (InvalidFormatException e2) {
                        LOGGER.error(e2.getMessage(), e2);
                        throw new AppException("File import phải là Excel 97-2012 (xls, xlsx)!");
                    }

                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    MessageUtil.setErrorMessageFromRes("meassage.import.fail2");
                    return;
                } finally {
                    if (workbook != null) {
                        try {
                            workbook.close();
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                }

                try {
                    for (String key : mapImportFile.get(event.getFile().getFileName()).keySet()) {
                        List<?> objectImports = new LinkedList<>();
                        Importer<Serializable> importer = new Importer<Serializable>() {

                            @Override
                            protected Map<Integer, String> getIndexMapFieldClass() {
                                return null;
                            }

                            @Override
                            protected String getDateFormat() {
                                return null;
                            }
                        };

                        importer.setRowHeaderNumber(1);
                        List<Serializable> objects = importer.getDatas(workbook, key, "2-");
                        if (objects != null) {
                            ((List<Object>) objectImports).addAll(objects);
                            mapCellObjectImports.put(key, objectImports);
                        } else {
                            throw new AppException(MessageUtil.getResourceBundleConfig("error.import.sheetname"));
                        }

                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    MessageUtil.setErrorMessageFromRes("error.import.param.fail");
                    throw e;
                }
                //Lay xong tham so do vao bang mapCellObjectImports
                filters.clear();
                filters.put("vendor.vendorId", currChangePlan.getVendor().getVendorId());

                processImportedParams(event.getFile().getFileName(), mapImportFile, mapCellObjectImports);
            }
            RequestContext.getCurrentInstance().execute("PF('addChangeDlg').hide()");
        } catch (Exception ex) {
            if (ex instanceof MessageException) {
                MessageUtil.setErrorMessage(ex.getMessage());
            } else {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }

    private void processImportedParams(String importedFileName, final HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            Map<String, Map<String, String>> bscParamMapForMopHR = new HashMap<>();
            Map<String, Map<String, String>> bscParamMapForMopSD = new HashMap<>();
            Map<String, String> bscMopParam;
            List<StationConfigImport> stationConfigImports;
            ChangeHrSdResource changeHrSdResource;
            Long nTRx;
            Double offerTraffic;

            // Get data from imported xls file
            for (String sheetName : mapImportFile.get(importedFileName).keySet()) {
                List<BasicDynaBean> rows = (List<BasicDynaBean>) mapCellObjectImports.get(sheetName);
                for (BasicDynaBean row : rows) {
                    String bsc = (String) row.get("bsc");
                    String cell = (String) row.get("cell");
                    String estimateTraffic = (String) row.get("estimate_traffic");
                    String zone = (String) row.get("zone");
                    String hr = (String) row.get("hr");
                    String sd = (String) row.get("sd");

                    // Get traffic offer from DB
                    Map<String, Object> filters = new HashMap<>();
                    filters.put("rncBsc", bsc);
                    filters.put("cellCode", cell);
                    filters.put("type", 0L);
                    List<ChangeHrSdResource> changeHrSdResources = changeHrSdResourceServiceImpl.findList(filters);
                    if (changeHrSdResources.size() > 0) {
                        changeHrSdResource = changeHrSdResources.get(0);
                        nTRx = changeHrSdResource.getTrx();
                        offerTraffic = changeHrSdResource.getTrafficOffer();
                    } else {
                        MessageUtil.setErrorMessage("Cell data is not exited. Cell code: " + cell);
                        return;
                    }

                    if (nTRx == null || offerTraffic == null) {
                        MessageUtil.setErrorMessage("Cell data is not exited. Cell code: " + cell);
                        continue;
                    }
                    Double tu = null;
                    if (estimateTraffic != null && !"".equals(estimateTraffic)) {
                        tu = (Double.parseDouble(estimateTraffic) / offerTraffic) * 100;
                    }

                    Long hrval = null;
                    Long sdval = null;
                    try {
                        if (null == hr || "".equals(hr)) {
                            if (tu != null) {
                                hrval = calculateHR(tu);
                            }
                        } else {
                            hrval = Long.valueOf(hr);
                        }
                        if (null == sd || "".equals(sd)) {
                            if (tu != null) {
                                sdval = calculateSD(zone, tu, nTRx);
                            }
                        } else {
                            sdval = Long.valueOf(sd);
                        }
                    } catch (Exception ex) {
                        MessageUtil.setErrorMessage("Rule Error!");
                        throw ex;
                    }
                    if (hrval != null) {
                        if (null == bscParamMapForMopHR.get(bsc)) {
                            bscMopParam = new HashMap<>();
                            bscMopParam.put("cell", cell);
                            bscMopParam.put("hrval", String.valueOf(hrval));
                            bscMopParam.put("sdval", String.valueOf(sdval));
                            bscParamMapForMopHR.put(bsc, bscMopParam);
                        } else {
                            bscMopParam = bscParamMapForMopHR.get(bsc);
                            bscMopParam.put("cell", bscMopParam.get("cell") + ";" + cell);
                            bscMopParam.put("hrval", bscMopParam.get("hrval") + ";" + String.valueOf(hrval));
                            bscMopParam.put("sdval", bscMopParam.get("sdval") + ";" + String.valueOf(sdval));
                        }
                    }

                    if (sdval != null) {
                        if (null == bscParamMapForMopSD.get(bsc)) {
                            bscMopParam = new HashMap<>();
                            bscMopParam.put("cell", cell);
                            bscMopParam.put("hrval", String.valueOf(hrval));
                            bscMopParam.put("sdval", String.valueOf(sdval));
                            bscParamMapForMopSD.put(bsc, bscMopParam);
                        } else {
                            bscMopParam = bscParamMapForMopSD.get(bsc);
                            bscMopParam.put("cell", bscMopParam.get("cell") + ";" + cell);
                            bscMopParam.put("hrval", bscMopParam.get("hrval") + ";" + hrval);
                            bscMopParam.put("sdval", bscMopParam.get("sdval") + ";" + sdval);
                        }
                    }

                }
            }

            // Insert data to database
            currChangePlan.setUpdateTime(new Date());
            currChangePlan.setUserCreate(SessionWrapper.getCurrentUsername());

            // Save change plan
            currChangePlan.setEventType((long) Constants.EventType.CHANGE_FORCE_HR_SD);
            changePlanService.saveOrUpdate(currChangePlan);
            Map<String, Object> filters = new HashMap<String, Object>();
            if (bscParamMapForMopHR.size() > 0) {
                // Sinh mop BSC cho HR
                filters.put("fileName", "Form_Force_Change_HR_SD.xlsx");
                filters.put("bscType", "HR");
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                createFlowRunActionForceChangeParam(Constants.ChangeDTType.CHANGE_HR_FORCE, bscParamMapForMopHR, stationConfigImports);
            }
            if (bscParamMapForMopSD.size() > 0) {
                // Sinh mop BSC cho SD
                filters.put("fileName", "Form_Force_Change_HR_SD.xlsx");
                filters.put("bscType", "SD");
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                createFlowRunActionForceChangeParam(Constants.ChangeDTType.CHANGE_SD_FORCE, bscParamMapForMopSD, stationConfigImports);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    private boolean createFlowRunActionForceChangeParam(String dtName, Map<String, Map<String, String>> bscParamMapForMop, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
        GenerateFlowRunController generateFlowRunController;
        List<ChangeFlowRunAction> changeFlowRunActions = new ArrayList<>();
        FlowTemplates flowTemplates = null;

        try {
            for (String bsc : bscParamMapForMop.keySet()) {
                flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());

                if (flowTemplates == null) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                    continue;
                } else {
                    if (flowTemplates.getStatus() != 9) {
                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                        continue;
                    }
                }

                try {
                    String filePutOSS;
                    filePutOSS = currChangePlan.getCrNumber().toUpperCase()
                            + "-" + dtName
                            + "-BSC_" + bsc.toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                    generateFlowRunController = new GenerateFlowRunController();
                    FlowRunAction flowRunAction = new FlowRunAction();
                    flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
                    flowRunAction.setFlowRunName(filePutOSS);
//                    filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                    while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                        flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                    }
                    flowRunAction.setTimeRun(new Date());
                    flowRunAction.setFlowTemplates(flowTemplates);
                    flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());

                    generateFlowRunController.setFlowRunAction(flowRunAction);
                    generateFlowRunController.setSelectedFlowTemplates(flowTemplates);

                    //Lay danh sach param tu bang 
                    generateFlowRunController.setNodes(new ArrayList<Node>());
                    Map<String, String> mapParamValues;
                    generateFlowRunController.loadGroupAction(0l);
                    List<Node> nodeInPlan;
                    HashMap<String, Object> filters = new HashMap<>();
                    filters.put("nodeCode-EXAC", bsc);
                    List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                    Node nodeRncBsc;
                    if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                        throw new Exception("Khong tim thay node RNC hoac tim thay lon hon 1 ban ghi");
                    } else {
                        nodeRncBsc = nodeRncs.get(0);
                    }

                    mapParamValues = bscParamMapForMop.get(bsc);

                    nodeInPlan = new ArrayList<>();
                    generateFlowRunController.getNodes().add(nodeRncBsc);
                    nodeInPlan.add(nodeRncBsc);

                    for (Node node : nodeInPlan) {
                        generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                        List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                        for (ParamValue paramValue : paramValues) {
                            LOGGER.info("Show ParamCode: " + paramValue.getParamCode());

                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }

                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }

                            ResourceBundle bundle = ResourceBundle.getBundle("cas");
                            if (bundle.getString("service").contains("10.61.127.190")) {
                                if (value == null || value.toString().isEmpty()) {
                                    value = "TEST_NOT_FOUND";
                                }
                            }
                            if (value != null) {
                                paramValue.setParamValue(value.toString());
                            }
                        }
                    }

                    boolean saveDT = generateFlowRunController.saveDT();
                    if (saveDT) {
                        try {
                            // Save flow to database                           
                            ChangeFlowRunAction changeFlowRunAction = new ChangeFlowRunAction();
                            changeFlowRunAction.setFlowRunAction(flowRunAction);
                            changeFlowRunAction.setNode(nodeRncBsc);
                            changeFlowRunAction.setType(2L);
                            changeFlowRunAction.setUpdateTime(new Date());
                            changeFlowRunActions.add(changeFlowRunAction);

                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), bsc));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                    }
                } catch (Exception e) {
                    if (e instanceof MessageException) {
                        MessageUtil.setErrorMessage(e.getMessage());
                    } else {
                        LOGGER.error(e.getMessage(), e);
                    }
                    throw new Exception(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                } finally {
                    //Tam biet
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
        changeFlowRunActionServiceImpl.saveOrUpdate(changeFlowRunActions);
        return true;
    }

    public static void main(String[] args) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        try {
            Object result = engine.eval("5.5*3");
            System.out.println("result:" + ((Number) result).longValue());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private Long calculateSD(String zone, Double tu, Long nTRx) throws Exception {
        Long sd;
        String clause;
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        if (zone.equals(Constants.sdZoneType.HOTSPOT)) {
            if (tu < hotspot1SDRightValue) {
                clause = hotspot1SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else if (tu >= hotspot2SDLeftValue && tu <= hotspot2SDRightValue) {
                clause = hotspot2SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else {
                clause = hotspot3SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            }
        } else {
            if (tu < normal1SDRightValue) {
                clause = normal1SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else if (tu > normal2SDLeftValue && tu <= normal2SDRightValue) {
                clause = normal2SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else if (tu > normal3SDLeftValue && tu <= normal3SDRightValue) {
                clause = normal3SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else {
                clause = normal4SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            }
        }
        Object result = engine.eval(clause);
        sd = ((Number) result).longValue();
        return sd;
    }

    private Long calculateHR(Double tu) {
        Long hr;
        if (tu < normal1HRRightValue) {
            hr = normal1HRValue;
        } else if (tu >= normal2HRLeftValue && tu < normal2HRRightValue) {
            hr = normal2HRValue;
        } else if (tu >= normal3HRLeftValue && tu <= normal3HRRightValue) {
            hr = normal3HRValue;
        } else if (tu > normal4HRLeftValue && tu <= normal4HRRightValue) {
            hr = normal4HRValue;
        } else {
            hr = normal5HRValue;
        }
        return hr;
    }

    public void preShowChangeDT(ChangePlan changePlan) throws Exception {
        HashMap<String, Object> filters = new HashMap<>();
        filters.put("changePlan.id", changePlan.getId());
        Map<String, String> orders = new LinkedHashMap<>();

        orders.put("node.nodeCode", "ASC");
        orders.put("flowRunAction.flowRunName", "ASC");
        lazyChangeDT = new LazyDataModelBaseNew<>(new ChangeFlowRunActionServiceImpl(), filters, orders);
    }

    public StreamedContent onDownloadFilePutOSS(ChangeFlowRunAction DT) {
        try {
            if (DT.getFileName() != null && DT.getFilePath() != null) {
                File fileExport = new File(DT.getFilePath());
                if (fileExport.exists()) {
                    return new DefaultStreamedContent(new FileInputStream(fileExport), ".txt", fileExport.getName());
                } else {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                            MessageUtil.getResourceBundleMessage("label.dowload.fileName")));

                }
            } else {
                MessageUtil.setErrorMessage("Khong co file");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public void preCreateChangeDT() {
        if (selectedChangeHrSdResources != null && selectedChangeHrSdResources.size() > 0) {
            RequestContext.getCurrentInstance().execute("PF('comfirmCreateChangeDT').show()");
        } else {
            MessageUtil.setInfoMessageFromRes("label.form.choose.one");
        }
    }

    public void onChangeSelectVendor() {
        if (currChangePlan != null && currChangePlan.getVendor() != null && currChangePlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)) {
            RequestContext.getCurrentInstance().execute("PF('defineRuleDlg').show()");
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Common">
    public static String getFolderSave() {
        String pathOut;
        ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                .getExternalContext().getContext();
        pathOut = ctx.getRealPath("/") + Config.PATH_OUT;
        File folderOut = new File(pathOut);
        if (!folderOut.exists()) {
            folderOut.mkdirs();
        }
        return pathOut;
    }

    public File createFilePutOSS(String filename, StringBuilder stringBuilder) throws Exception {
        BufferedWriter bw = null;
        FileWriter fw = null;
        File file;
        try {
            file = new File(getFolderSave() + "IntegrateStation" + File.separator + "PutFileOSS" + File.separator + filename);
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(stringBuilder.toString());
            bw.close();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return file;
    }

    public boolean validateInput(ChangePlan change) {
        if (change != null
                && change.getVendor() != null && change.getNetworkType() != null) {
            return true;
        } else {
            return false;
        }
    }
    public void calculateParamDetail(HashMap<String, String> mapValue, List<StationParamDefault> lstParamDefault, Logger logger) {
        // Tinh toan cac tham so cua lenh theo cong thuc duoc cau hinh

        for (StationParamDefault paramInput : lstParamDefault) {
            if (paramInput.getParamFormula() != null && !paramInput.getParamFormula().trim().isEmpty()) {
                if (paramInput.getParamFormula().contains("-->")) {
                    List<String> array = getParamList(paramInput.getParamFormula());

                    Map<String, String> mapVar = new HashMap<>();
                    for (String str : array) {
                        if (mapValue.containsKey(str)) {
                            mapVar.put(str, mapValue.get(str) == null ? "" : mapValue.get(str).trim());
                        }
                    }
                    List<String> lstValues = getIfValue(paramInput.getParamFormula(), mapVar, logger);
                    if (!lstValues.isEmpty()) {
                        String vl = StringUtils.join(lstValues, ";");
                        mapValue.put(paramInput.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_"), vl);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(vl);
//                            }
//                        }
                    } else {
//                        mapValue.put(paramInput.getParamCode(), null);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(null);
//                            }
//                        }
                    }
                } else {
                    List<String> array = getParamList(paramInput.getParamFormula());

                    Map<String, String> mapVar = new HashMap<>();
                    for (String str : array) {
                        if (mapValue.containsKey(str)) {
                            mapVar.put(str, mapValue.get(str) == null ? "" : mapValue.get(str).trim());
                        }
                    }
                    List<Long> lstValues = getExpressValue(paramInput.getParamFormula(), mapVar);
                    if (!lstValues.isEmpty()) {
                        String vl = StringUtils.join(lstValues, ";");
                        mapValue.put(paramInput.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_"), vl);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(vl);
//                            }
//                        }
                    } else {
//                        mapValue.put(paramInput.getParamCode(), null);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(null);
//                            }
//                        }
                    }
                }
            } else {
                mapValue.put(paramInput.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_"), paramInput.getParamDefault() == null ? "" : paramInput.getParamDefault());
            }
        }
    }
    private List<String> getParamList(String commandPattern) {
        List<String> lstParam = new ArrayList<>();

        if (commandPattern != null && !commandPattern.trim().isEmpty() && commandPattern.contains("@{")) {
            int preIndex = 0, endIndex;
            while (true) {
                preIndex = commandPattern.indexOf("@{", preIndex);

                if (preIndex < 0) {
                    break;
                } else {
                    preIndex += 2;
                }

                endIndex = commandPattern.indexOf("}", preIndex);

                if (endIndex < 0) {
                    break;
                } else {
                    lstParam.add(commandPattern.substring(preIndex, endIndex));
                    preIndex = endIndex;
                }
            }
        }

        return lstParam;
    }

    private List<String> getIfValue(String expressStr, Map<String, String> mapVar, Logger logger) {
        List<String> lstValue = new ArrayList<>();
        try {
            int maxNumber = 1;
            for (String value : mapVar.values()) {
                maxNumber = maxNumber > value.split(";").length ? maxNumber : value.split(";").length;
            }
            for (int i = 0; i < maxNumber; i++) {
                //try {
                Map<String, String> mapVarD = new HashMap<>();
                for (String code : mapVar.keySet()) {
                    String vl = mapVar.get(code);
                    String[] arr = vl.split(";");
                    if (i <= arr.length - 1) {
                        mapVarD.put(code, arr[i]);
                    } else {
                        mapVarD.put(code, arr[arr.length - 1]);
                    }
                }

                String[] ifConditions = expressStr.split(";");
                String value = null;
                boolean paramEnough = true;
                for (String condition : ifConditions) {
                    //try {
                    if (condition.contains("-->")) {
                        String[] arr = condition.split("-->");
                        if (arr != null && arr.length == 2) {
                            String ifBody = arr[0].trim();
                            String ifThen = arr[1].trim();

                            for (String var : mapVarD.keySet()) {
                                ifBody = ifBody.replace("@{" + var + "}", mapVarD.get(var));
                                ifThen = ifThen.replace("@{" + var + "}", mapVarD.get(var));
                            }

                            ScriptEngineManager factory = new ScriptEngineManager();
                            ScriptEngine engine = factory.getEngineByName("JavaScript");

                            if (!ifBody.contains("@{")) {
                                Boolean ifValue = (Boolean) engine.eval(ifBody);
                                if (ifValue && ifThen != null && !ifThen.contains("@{")) {
                                    value = engine.eval(ifThen).toString().replaceAll("\\.0+$", "");
                                    break;
                                }
                            } else {
                                paramEnough = false;
                            }
                        }
                    }
//                        } catch (Exception ex) {
//                            logger.error(ex.getMessage(), ex);
//                        }
                }

                if (value == null && paramEnough) {
                    for (String condition : ifConditions) {
                        if (condition.toLowerCase().contains("else") && !condition.contains("-->")) {
                            for (String var : mapVarD.keySet()) {
                                condition = condition.replace("@{" + var + "}", mapVarD.get(var));
                            }
                            value = condition.replace("else", "").trim();
                            break;
                        }
                    }
                }
                if (value != null) {
                    lstValue.add(value);
                }
//                } catch (Exception ex) {
//                    return lstValue;
//                }
            }
            return lstValue;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new ArrayList<>();
        }
    }

    private List<Long> getExpressValue(final String expressStr, Map<String, String> mapVar) {
        List<Long> lstValue = new ArrayList<>();
        try {
            int maxNumber = 1;
            for (String value : mapVar.values()) {
                maxNumber = maxNumber > value.split(";").length ? maxNumber : value.split(";").length;
            }
            for (int i = 0; i < maxNumber; i++) {
                String expressStrLocal = expressStr;
                //try {
                Map<String, Double> mapVarD = new HashMap<>();
                for (String code : mapVar.keySet()) {
                    String vl = mapVar.get(code);
                    String[] arr = vl.split(";");
                    if (i <= arr.length - 1) {
                        mapVarD.put(code, Double.parseDouble(arr[i]));
                    } else {
                        mapVarD.put(code, Double.parseDouble(arr[arr.length - 1]));
                    }
                }

                for (String var : mapVarD.keySet()) {
                    expressStrLocal = expressStrLocal.replace("@{" + var + "}", String.valueOf(mapVarD.get(var)));
                }

                Expression e = new ExpressionBuilder(expressStrLocal).build();

                lstValue.add((long) e.evaluate());
//                } catch (Exception ex) {
//                    return lstValue;
//                }
            }
            return lstValue;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new ArrayList<>();
        }
    }
    //</editor-fold>
}
