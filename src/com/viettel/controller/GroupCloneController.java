/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.controller;

import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.ActionOfFlow;
import com.viettel.model.FlowTemplates;
import com.viettel.model.GroupActionClone;
import com.viettel.persistence.GroupActionCloneServiceImpl;
import com.viettel.util.MessageUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hienhv4
 */
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class GroupCloneController implements Serializable {

    List<GroupActionClone> groupClones = new LinkedList<>();

    @ManagedProperty(value = "#{buildTemplateFlowController}")
    BuildTemplateFlowController buildTemplateFlowController;

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public void buildGroupClone() throws Exception {
        FlowTemplates selectedFlowTemplate = buildTemplateFlowController.getSelectedFlowTemplate();

        Map<String, Object> filters = new HashMap<>();
        filters.put("flowTemplateId", selectedFlowTemplate.getFlowTemplatesId());
        groupClones = new GroupActionCloneServiceImpl().findList(filters);

        List<String> groupNames = getGroupAction();
        Map<String, Boolean> mapGroupClone = new HashMap<>();
        if (groupClones != null && !groupClones.isEmpty()) {
            for (GroupActionClone group : groupClones) {
                mapGroupClone.put(group.getGroupName(), Boolean.FALSE);
            }
        }

        if (!groupNames.isEmpty()) {
            for (String groupName : groupNames) {
                if (mapGroupClone.containsKey(groupName)) {
                    continue;
                }
                GroupActionClone groupAction = new GroupActionClone();

                groupAction.setFlowTemplateId(selectedFlowTemplate.getFlowTemplatesId());
                groupAction.setGroupName(groupName);
                groupAction.setClone(Boolean.FALSE);
                groupAction.setStop(Boolean.FALSE);
                if (groupClones != null) {
                    groupClones.add(groupAction);
                }
            }
        }
    }

    public void saveGroupConfig() {
        Session _session = null;
        Transaction _tx = null;
        try {
            Long flowTemplatesId = buildTemplateFlowController.getSelectedFlowTemplate().getFlowTemplatesId();

            GroupActionCloneServiceImpl groupActionService = new GroupActionCloneServiceImpl();

            Object[] trs = groupActionService.openTransaction();
            _session = (Session) trs[0];
            _tx = (Transaction) trs[1];

            groupActionService.saveOrUpdate(groupClones, _session, _tx, true);
            MessageUtil.setInfoMessageFromRes("info.save.success");
        } catch (AppException | SysException e) {
            if (_tx != null && _tx.getStatus() != TransactionStatus.ROLLED_BACK) {
                _tx.rollback();
            }
            LOGGER.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.save.unsuccess");
        } finally {
            if (_session != null && _session.isOpen()) {
                _session.close();
            }
        }
    }

    public List<String> getGroupAction() {
        List<String> groupNames = new ArrayList<>();
        if (buildTemplateFlowController.getSelectedFlowTemplate() != null) {
            for (ActionOfFlow actionOfFlow : buildTemplateFlowController.getSelectedFlowTemplate().getActionOfFlows()) {
                if (!groupNames.contains(actionOfFlow.getGroupActionName())) {
                    groupNames.add(actionOfFlow.getGroupActionName());
                }
            }
        }
        return groupNames;
    }

    public BuildTemplateFlowController getBuildTemplateFlowController() {
        return buildTemplateFlowController;
    }

    public void setBuildTemplateFlowController(BuildTemplateFlowController buildTemplateFlowController) {
        this.buildTemplateFlowController = buildTemplateFlowController;
    }

    public List<GroupActionClone> getGroupClones() {
        return groupClones;
    }

    public void setGroupClones(List<GroupActionClone> groupClones) {
        this.groupClones = groupClones;
    }
}
