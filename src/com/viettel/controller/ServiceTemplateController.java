package com.viettel.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rits.cloning.Cloner;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.Province;
import com.viettel.model.ServiceTemplate;
import com.viettel.model.ServiceTemplateId;
import com.viettel.persistence.ProvinceServiceImpl;
import com.viettel.persistence.ServiceTemplateServiceImpl;
import com.viettel.util.MessageUtil;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class ServiceTemplateController implements Serializable {
	
	private ServiceTemplate serviceTemplate; 
	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@ManagedProperty("#{serviceTemplateService}")
	private ServiceTemplateServiceImpl serviceTemplateService;
	
	private LazyDataModel<ServiceTemplate> lazyDataService;
	
	private List<Province> provinces ;
	
	@PostConstruct
	public void onStart(){
		lazyDataService = new LazyDataModelBaseNew<>(serviceTemplateService);
		try {
			provinces = new ProvinceServiceImpl().findList();
		} catch (SysException | AppException e) {
			LOGGER.error(e.getMessage(), e);
			provinces =new ArrayList<Province>();
		}
	}

	public void clean(){
		serviceTemplate =  new ServiceTemplate();
		serviceTemplate.setId(new ServiceTemplateId());
	}
	
	public void saveOrUpdate(){
		try {
			List<ServiceTemplate> serviceTemplates = new ArrayList<ServiceTemplate>();
			//Check data existed
			Map<String, Object> filters = new HashMap<String, Object>();
			//"VENDOR_ID", "VERSION_ID", "SERVICE_CODE", "ACTION"
			filters.put("id.vendorId", serviceTemplate.getVendor().getVendorId());
			filters.put("id.versionId", serviceTemplate.getVersion().getVersionId());
			filters.put("id.serviceCode", serviceTemplate.getId().getServiceCode());
			filters.put("id.action", serviceTemplate.getId().getAction());
			filters.put("id.subring", serviceTemplate.getId().getSubring());
			
			Cloner cloner = new Cloner();
			for (String provinceCode : serviceTemplate.getProvinceCodes()) {
				filters.put("id.provinceCode-EXAC", provinceCode);
				if(serviceTemplateService.findList(filters ).size()>0){
					MessageUtil.setWarnMessage(MessageUtil.getResourceBundleMessage(("warn.service.template.existed"))+": "+provinceCode);;
					continue;
				}
				serviceTemplate.getId().setFlowTemplateId(serviceTemplate.getFlowTemplates().getFlowTemplatesId());
				serviceTemplate.getId().setVendorId(serviceTemplate.getVendor().getVendorId());
				serviceTemplate.getId().setVersionId(serviceTemplate.getVersion().getVersionId());
				serviceTemplate.getId().setProvinceCode(provinceCode);
				serviceTemplates.add(cloner.deepClone(serviceTemplate));
			}
				
			serviceTemplateService.saveOrUpdate(serviceTemplates);
			MessageUtil.setInfoMessageFromRes("common.message.success");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			MessageUtil.setErrorMessageFromRes("error.save.unsuccess");
		}
		
	}
	
	public void preEdit(ServiceTemplate serviceTemplate){
		this.serviceTemplate = serviceTemplate;
		
	}
	
	public void delete(){
		try {
			serviceTemplateService.delete(serviceTemplate);
			MessageUtil.setInfoMessageFromRes("common.message.success");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
	
	
	public ServiceTemplateServiceImpl getServiceTemplateService() {
		return serviceTemplateService;
	}

	public void setServiceTemplateService(ServiceTemplateServiceImpl serviceTemplateService) {
		this.serviceTemplateService = serviceTemplateService;
	}

	public ServiceTemplate getServiceTemplate() {
		return serviceTemplate;
	}

	public void setServiceTemplate(ServiceTemplate serviceTemplate) {
		this.serviceTemplate = serviceTemplate;
	}

	public LazyDataModel<ServiceTemplate> getLazyDataService() {
		return lazyDataService;
	}

	public void setLazyDataService(LazyDataModel<ServiceTemplate> lazyDataService) {
		this.lazyDataService = lazyDataService;
	}

	public List<Province> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<Province> provinces) {
		this.provinces = provinces;
	}
}
