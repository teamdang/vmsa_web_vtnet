package com.viettel.controller;

import com.viettel.exception.AppException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.MapUserCountryBO;
import com.viettel.persistence.DaoSimpleService;
import com.viettel.persistence.MapUserCountryServiceImpl;
import com.viettel.util.MessageUtil;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;

/**
 * Created by xuanhuy on 6/5/2017.
 */
@ManagedBean
@ViewScoped
public class UserCountryController {
    LazyDataModel<MapUserCountryBO> lazyDataModel;
    MapUserCountryBO userCountry;
    List<MapUserCountryBO> userCountries;
    MapUserCountryServiceImpl userCountryService;
    protected static final Logger logger = LoggerFactory.getLogger(UserCountryController.class);

    @PostConstruct
    public void onStart(){
        userCountryService = new MapUserCountryServiceImpl();
        LinkedHashMap<String, String> order = new LinkedHashMap<>();
        order.put("userName","ASC");
        lazyDataModel = new LazyDataModelBaseNew<MapUserCountryBO, Long>(userCountryService, null,order);
        clean();
    }

    public void saveOrUpdate(){
        try {

            for (String countryCode : userCountry.getCountryCodes()) {
                String sqlInsMapUserCountry = "MERGE INTO MAP_USER_COUNTRY D\n" +
                        "USING (SELECT ? username, ? COUNTRY_CODE from dual) S ON (D.USER_NAME = S.username and D.COUNTRY_CODE = S.COUNTRY_CODE)\n" +
                        "WHEN NOT MATCHED THEN INSERT (ID,USER_NAME,COUNTRY_CODE,STATUS) VALUES (MAP_USER_COUNTRY_SEQ.nextval, S.username,S.COUNTRY_CODE ,1)";

                new DaoSimpleService().execteNativeBulk("DELETE MAP_USER_COUNTRY WHERE USER_NAME = ? AND COUNTRY_CODE is NULL", userCountry.getUserName());
                new DaoSimpleService().execteNativeBulk(sqlInsMapUserCountry, userCountry.getUserName(),countryCode);
            }
            MessageUtil.setInfoMessageFromRes("info.save.success");
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            MessageUtil.setErrorMessageFromRes("error.save.unsuccess");
        }
    }

    public void delete(){
        try {
            userCountryService.delete(userCountry);
            MessageUtil.setInfoMessageFromRes("info.delete.suceess");
        } catch (AppException e) {
            logger.error(e.getMessage(),e);
            MessageUtil.setErrorMessageFromRes("error.delete.unsuceess");
        }
    }
    public void preEdit(MapUserCountryBO userCountry){
        this.userCountry = userCountry;
        this.userCountry.setCountryCodes(new ArrayList<String>());
        Map<String, Object> filter = new HashMap<>();
        filter.put("userName-EXAC",userCountry.getUserName());
        try {
            List<MapUserCountryBO> users = userCountryService.findList(filter);
            for (MapUserCountryBO user : users) {
                this.userCountry.getCountryCodes().add(user.getCountryCode());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }

    }

    public void clean(){
        userCountry = new MapUserCountryBO();
    }

    public LazyDataModel<MapUserCountryBO> getLazyDataModel() {
        return lazyDataModel;
    }

    public void setLazyDataModel(LazyDataModel<MapUserCountryBO> lazyDataModel) {
        this.lazyDataModel = lazyDataModel;
    }

    public MapUserCountryBO getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(MapUserCountryBO userCountry) {
        this.userCountry = userCountry;
    }

    public List<MapUserCountryBO> getUserCountries() {
        return userCountries;
    }

    public void setUserCountries(List<MapUserCountryBO> userCountries) {
        this.userCountries = userCountries;
    }
}
