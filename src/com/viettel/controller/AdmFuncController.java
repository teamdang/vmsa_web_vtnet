package com.viettel.controller;

/**
 * @author thuypq<thuypq@viettel.com.vn>
 *
 */
import com.viettel.lazy.LazyDataModelBaseNewV2;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.adm.AdmCommonBO;
import com.viettel.model.adm.AdmFuncQueryButtonId;
import com.viettel.model.adm.AdmFuncQueryColCalId;
import com.viettel.model.adm.AdmFuncQueryColGroupId;
import com.viettel.model.adm.AdmFuncQueryColumnId;
import com.viettel.model.adm.AdmFuncQueryCondDependId;
import com.viettel.model.adm.AdmFuncQueryCondId;
import com.viettel.model.adm.AdmFuncQueryId;
import com.viettel.model.adm.V2AdmDatabase;
import com.viettel.model.adm.V2AdmFuncBO;
import com.viettel.model.adm.V2AdmFuncQueryBO;
import com.viettel.model.adm.V2AdmFuncQueryButtonBO;
import com.viettel.model.adm.V2AdmFuncQueryColCalBO;
import com.viettel.model.adm.V2AdmFuncQueryColGroupBO;
import com.viettel.model.adm.V2AdmFuncQueryColumnBO;
import com.viettel.model.adm.V2AdmFuncQueryCondBO;
import com.viettel.model.adm.V2AdmFuncQueryCondDependBO;
import com.viettel.persistence.adm.V2AdmDatabaseServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryButtonServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryColCalServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryColGroupServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryColumnServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryCondDependServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryCondServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncQueryServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncServiceImpl;
import com.viettel.util.HibernateUtil;
import com.viettel.util.MessageUtil;
import com.viettel.util.NamedParameterStatement;
import com.viettel.util.ServiceUtil;
import com.viettel.util.StringUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import org.primefaces.model.LazyDataModel;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@ViewScoped
@ManagedBean
public class AdmFuncController implements Serializable {

    private static final String OK = "OK";
    private static final Long COMMON_CONDITION = 0L;

    private interface QUERY_TYPE {

        String NORMAL = "normal";
        String DIRECT = "direct";
    }
    public String functionCodes;
    public List<AdmCommonBO> lstComapareType;
    public List<AdmCommonBO> listColumnAvailable;
    public List<AdmCommonBO> listConditions;
    public String[] selectedRequireAndCommon;
    @ManagedProperty(value = "#{v2AdmFuncService}")
    private V2AdmFuncServiceImpl v2AdmFuncServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryService}")
    private V2AdmFuncQueryServiceImpl v2AdmFuncQueryServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryCondService}")
    private V2AdmFuncQueryCondServiceImpl v2AdmFuncQueryCondServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryColumnService}")
    private V2AdmFuncQueryColumnServiceImpl v2AdmFuncQueryColumnServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryColGroupService}")
    private V2AdmFuncQueryColGroupServiceImpl v2AdmFuncQueryColGroupServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryCondDependService}")
    private V2AdmFuncQueryCondDependServiceImpl v2AdmFuncQueryCondDependServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryColCalService}")
    private V2AdmFuncQueryColCalServiceImpl v2AdmFuncQueryColCalServiceImpl;
    @ManagedProperty(value = "#{v2AdmFuncQueryButtonService}")
    private V2AdmFuncQueryButtonServiceImpl v2AdmFuncQueryButtonServiceImpl;

    private V2AdmFuncBO selectedObj;
    private V2AdmFuncQueryBO selectedQueryObj;
    private V2AdmFuncQueryCondBO selectedQueryCondObj;
    private V2AdmFuncBO addOrUpdateObj = new V2AdmFuncBO();
    private V2AdmFuncQueryBO addOrUpdateQueryObj = new V2AdmFuncQueryBO();
    private V2AdmFuncQueryCondBO addOrUpdateQueryCondObj = new V2AdmFuncQueryCondBO();
    private V2AdmFuncQueryColumnBO addOrUpdateQueryColumnObj = new V2AdmFuncQueryColumnBO();
    private V2AdmFuncQueryColGroupBO addOrUpdateQueryColGroupObj = new V2AdmFuncQueryColGroupBO();
    private V2AdmFuncQueryCondDependBO addOrUpdateQueryCondDependObj = new V2AdmFuncQueryCondDependBO();
    private LazyDataModel<V2AdmFuncBO> lazyModel;
    private LazyDataModel<V2AdmFuncQueryBO> lazyModelFuncQuery;
    private LazyDataModel<V2AdmFuncQueryColumnBO> lazyModelFuncQueryColumns;
    private LazyDataModel<V2AdmFuncQueryCondBO> lazyModelFuncQueryCond;
    private LazyDataModel<V2AdmFuncQueryColGroupBO> lazyModelFuncQueryColGroup;
    private LazyDataModel<V2AdmFuncQueryCondDependBO> lazyModelFuncQueryCondDepend;

    private boolean isEdit = false;
    private boolean isImport = false;
    private boolean isDirectSql = false;
    private int tabIndex = -1;
    private boolean showChildOfQuery = false;
    private List<V2AdmDatabase> listDatabases;

    @PostConstruct
    public void onStart() {
        search();
        lstComapareType = new ArrayList<>();
        lstComapareType.add(new AdmCommonBO("<", "<"));
        lstComapareType.add(new AdmCommonBO("<=", "<="));
        lstComapareType.add(new AdmCommonBO("=", "="));
        lstComapareType.add(new AdmCommonBO(">=", ">="));
        lstComapareType.add(new AdmCommonBO(">", ">"));
        listDatabases = buildListDatabase();
    }

    public List<V2AdmDatabase> buildListDatabase() {
        List<V2AdmDatabase> list = null;
        Map<String, Object> filters = new HashMap<>();
        LinkedHashMap<String, String> orders = new LinkedHashMap<>();
        orders.put("dbId", "ASC");
        try {
            list = new V2AdmDatabaseServiceImpl().findList(filters, orders);
        } catch (Exception e) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
        return list;
    }

    //<editor-fold defaultstate="collapsed" desc="V2AdmFunc">
    public void prepareEdit(V2AdmFuncBO obj, boolean isEdit) {
        this.isEdit = isEdit;
        if (isEdit) {
            addOrUpdateObj = obj.cloneObject();
        } else {
            addOrUpdateObj = new V2AdmFuncBO();
        }
    }

    public void search() {
        Map<String, Object> filters = new HashMap<>();
        LinkedHashMap<String, String> orders = new LinkedHashMap<>();
        orders.put("functionCode", "ASC");
        lazyModel = new LazyDataModelBaseNewV2<>(v2AdmFuncServiceImpl, filters, orders);
    }

    public void saveOrUpdate() {
        try {
            String validateForm = validateForm(addOrUpdateObj);
            if (OK.equalsIgnoreCase(validateForm)) {
                V2AdmFuncBO bo = createAdmFuncBO(addOrUpdateObj, isEdit);
                v2AdmFuncServiceImpl.saveOrUpdate(bo);
                if (isEdit) {
                    MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
                } else {
                    MessageUtil.setInfoMessageFromRes("validator.addSuccess");
                }
                RequestContext.getCurrentInstance().execute("PF('dlgAdd').hide();");
            } else {
                MessageUtil.setWarnMessage(validateForm);
            }
        } catch (AppException e) {
            if (isEdit) {
                MessageUtil.setErrorMessageFromRes("validator.updateFail");
            } else {
                MessageUtil.setErrorMessageFromRes("validator.addFail");
            }
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public String validateForm(V2AdmFuncBO form) {
        //validate form start
        if (form == null) {
            return "Form is NULL";
        } else {
            if (form.getFunctionCode() == null || "".equals(form.getFunctionCode())) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionCode"));
            } else if (form.getFunctionCode().length() > 64) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionCode"), 64);
            } else if (!form.getFunctionCode().matches("^([_]*[0-9]*[A-Za-z])+([A-Za-z0-9_]*)$")) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.invalid"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionCode"));
            } else if (!isEdit) {
                Map<String, Object> filter = new HashMap<>();
                filter.put("functionCode", form.getFunctionCode());
                try {
                    List<V2AdmFuncBO> listFind = v2AdmFuncServiceImpl.findList(filter);
                    if (!listFind.isEmpty()) {
                        return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.exist"),
                                MessageUtil.getResourceBundleMessage("label.adm.functionCode"));
                    }
                } catch (Exception e) {
                    Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (form.getFunctionName() == null || "".equals(form.getFunctionName())) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionName"));
            } else if (form.getFunctionName().length() > 256) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionName"), 256);
            }
            if (form.getFunctionTitle() == null || "".equals(form.getFunctionTitle())) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionTitle"));
            } else if (form.getFunctionTitle().length() > 512) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionTitle"), 512);
            }
        }
        //validate form end
        return OK;
    }

    public V2AdmFuncBO createAdmFuncBO(V2AdmFuncBO form, boolean isEdit) throws AppException {
        V2AdmFuncBO bo;
        if (!"".equals(form.getFunctionCode()) && isEdit) {
            bo = v2AdmFuncServiceImpl.findById(form.getFunctionCode());
        } else {
            bo = new V2AdmFuncBO();
        }
        bo.setFunctionCode(form.getFunctionCode());
        bo.setFunctionName(form.getFunctionName());
        bo.setFunctionTitle(form.getFunctionTitle());
        bo.setFunctionType(form.getFunctionType());
        bo.setRefreshInterval(form.getRefreshInterval());

        return bo;
    }

    public void onDelete(V2AdmFuncBO obj) {
        if (obj != null) {
            try {
                Map<String, Object> filter = new HashMap<>();
                filter.put("id.functionCode-EXAC", obj.getFunctionCode());
                List<V2AdmFuncQueryBO> listFind = v2AdmFuncQueryServiceImpl.findList(filter);
                if (listFind != null && !listFind.isEmpty()) {
                    for (V2AdmFuncQueryBO item : listFind) {
                        String result = deleteQuery(item);
                        if (!OK.equals(result)) {
                            MessageUtil.setErrorMessageFromRes("validator.deleteFail");
                            return;
                        }
                    }
                }
                v2AdmFuncServiceImpl.delete(obj);
                MessageUtil.setInfoMessageFromRes("validator.deleteSuccess");
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("validator.deleteFail");
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="V2 AdmFuncQuery">
    public void prepareConfigQuery(V2AdmFuncBO obj) {
        selectedObj = obj;
        if (obj == null) {
            MessageUtil.setErrorMessageFromRes("object.is.null");
        } else {
            try {
                Map<String, Object> filters = new HashMap<>();
                filters.put("id.functionCode-EXAC", obj.getFunctionCode());
                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                orders.put("queryIndex", "ASC");
                lazyModelFuncQuery = new LazyDataModelBaseNewV2<>(v2AdmFuncQueryServiceImpl, filters, orders);
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("ERROR");
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        clearDataTableReference();
    }

    public void clearDataTableReference() {
        lazyModelFuncQueryColumns = null;
        lazyModelFuncQueryCond = null;
        lazyModelFuncQueryCondDepend = null;
        lazyModelFuncQueryColGroup = null;
    }

    public void prepareEditQuery(V2AdmFuncQueryBO obj, boolean isEdit) {
        selectedQueryObj = obj;
        this.isEdit = isEdit;
        if (isEdit) {
            addOrUpdateQueryObj = obj.cloneObject();
        } else {
            addOrUpdateQueryObj = new V2AdmFuncQueryBO();
            addOrUpdateQueryObj.setId(new AdmFuncQueryId(null, selectedObj != null ? selectedObj.getFunctionCode() : null));
        }
    }

    public void saveOrUpdateQuery() {
        Connection connection = null;
        NamedParameterStatement st = null;
        ResultSet rs = null;
        try {
            String validateForm = validateQueryForm(addOrUpdateQueryObj);
            if (OK.equalsIgnoreCase(validateForm)) {
                V2AdmFuncQueryBO bo = createAdmFuncQueryBO(addOrUpdateQueryObj);
                if (QUERY_TYPE.DIRECT.equalsIgnoreCase(bo.getQueryType())) {
                    List<String> listParams = validateDirectQuery(bo.getQueryString());
                    if (listParams == null) {
                        MessageUtil.setErrorMessageFromRes("common.adm.query.invalid");
                        return;
                    } else if (listParams.isEmpty()) {
                        MessageUtil.setErrorMessageFromRes("common.adm.query.havent.param");
                        return;
                    }
                    Map<String, Object> filters = new HashMap<>();
                    filters.put("id.functionCode-EXAC", bo.getId().getFunctionCode());
//                    filters.put("id.queryId", bo.getId().getQueryId());
                    LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                    Map<String, Object> sqlRes = new HashMap<>();
                    sqlRes.put("(query_id in( 0 , ?)) ", bo.getId().getQueryId());
                    List<V2AdmFuncQueryCondBO> listOldCondtion = v2AdmFuncQueryCondServiceImpl.findList(filters, sqlRes, orders);
                    List<String> listOldCode = new ArrayList();
                    for (V2AdmFuncQueryCondBO oldCondition : listOldCondtion) {
                        listOldCode.add(oldCondition.getColumnCode());
                    }
                    for (int i = 0; i < listParams.size(); i++) {
                        if (!listOldCode.contains(listParams.get(i))) {
                            V2AdmFuncQueryCondBO condition = new V2AdmFuncQueryCondBO();
                            condition.setId(new AdmFuncQueryCondId(bo.getId().getFunctionCode(),
                                    bo.getId().getQueryId(),
                                    ServiceUtil.getSequence("SQ_V2_ADM_FUNC_QUERY_COND")));
                            condition.setColumnCode(listParams.get(i));
                            condition.setIsDisplay(1L);
                            condition.setCompareType(null);
                            v2AdmFuncQueryCondServiceImpl.saveOrUpdate(condition);
                        }
                    }
                } else if (QUERY_TYPE.NORMAL.equalsIgnoreCase(bo.getQueryType())) {
                    boolean haveStatement = true;
                    if (bo.getDbId() != null && bo.getDbId() != 0) {
                        V2AdmDatabase db = new V2AdmDatabaseServiceImpl().findById(bo.getDbId());
                        if (db != null) {
                            ServiceUtil service = new ServiceUtil();
                            connection = service.connect(db);
                            if (!"com.facebook.presto.jdbc.PrestoDriver".equalsIgnoreCase(db.getDriver())) {
                                st = new NamedParameterStatement(connection, bo.getQueryString());
                            } else {
                                haveStatement = false;
                            }
                        }
                    } else {
                        st = new NamedParameterStatement(HibernateUtil.connectDbOracle(), bo.getQueryString());
                        haveStatement = true;
                    }
                    if (haveStatement && st != null) {
                        int countFromTime = countMatcher(bo.getQueryString(), ":fromTime");
                        int countToTime = countMatcher(bo.getQueryString(), ":toTime");
                        int countTime = countMatcher(bo.getQueryString(), ":time");
                        if (countFromTime > 0 || countToTime > 0 || countTime > 0) {
                            Date dateNow = new Date();
                            Timestamp fromTime = new Timestamp(dateNow.getTime() - 864000);
                            Timestamp toTime = new Timestamp(dateNow.getTime());
                            Timestamp time = new Timestamp(dateNow.getTime());
                            if (countFromTime > 0) {
                                st.setTimestamp("fromTime", fromTime);
                            }
                            if (countToTime > 0) {
                                st.setTimestamp("toTime", toTime);
                            }
                            if (countTime > 0) {
                                st.setTimestamp("time", time);
                            }
                        }
                        try {
                            rs = st.executeQuery();
                        } catch (SQLException ex) {
                            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
                            MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("common.adm.query.error.execute") + " (" + ex.getMessage() + ")");
                            return;
                        }
                        saveFuncQueryColumn(rs, bo);
                    } else {
                        Statement stmt = connection.createStatement();
                        try {
                            String query = "with tbl as (select *, row_number() OVER () rowid from ( " + bo.getQueryString() + " ) ) select * from tbl "
                                    + " where  rowid  < 2 ";

                            rs = stmt.executeQuery(query);
                        } catch (SQLException ex) {
                            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
                            MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("common.adm.query.error.execute") + " (" + ex.getMessage() + ")");
                            return;
                        } finally {
                            if (stmt != null) {
                                try {
                                    stmt.close();
                                } catch (Exception ex) {
                                    Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                        }
                        saveFuncQueryColumn(rs, bo);
                    }
                }
                v2AdmFuncQueryServiceImpl.saveOrUpdate(bo);
                if (isEdit) {
                    MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
                } else {
                    MessageUtil.setInfoMessageFromRes("validator.addSuccess");
                }
                RequestContext.getCurrentInstance().execute("PF('dlgAddQuery').hide();");
            } else {
                MessageUtil.setWarnMessage(validateForm);
            }
        } catch (Exception e) {
            if (isEdit) {
                MessageUtil.setErrorMessageFromRes("validator.updateFail");
            } else {
                MessageUtil.setErrorMessageFromRes("validator.addFail");
            }
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }

    private void saveFuncQueryColumn(ResultSet rs, V2AdmFuncQueryBO bo) throws AppException, Exception, SQLException, SysException {
        ResultSetMetaData md = rs.getMetaData();
        int col = md.getColumnCount();
        Map<String, Object> filters = new HashMap<>();
        filters.put("id.functionCode-EXAC", bo.getId().getFunctionCode());
        filters.put("id.queryId", bo.getId().getQueryId());
        List<V2AdmFuncQueryColumnBO> listOldColumns = v2AdmFuncQueryColumnServiceImpl.findList(filters);
        if (!listOldColumns.isEmpty()) {
            v2AdmFuncQueryColumnServiceImpl.delete(listOldColumns);
        }
        V2AdmFuncQueryColumnBO columnBO;
        for (int i = 1; i <= col; i++) {
            columnBO = new V2AdmFuncQueryColumnBO();
            columnBO.setId(new AdmFuncQueryColumnId(bo.getId().getFunctionCode(), bo.getId().getQueryId(), md.getColumnName(i)));
            V2AdmFuncQueryColumnBO oldColumn = getOldColumn(bo.getId().getFunctionCode(), bo.getId().getQueryId(), md.getColumnName(i), listOldColumns);
            if (oldColumn != null) {
                columnBO.setColumnName(oldColumn.getColumnName());
                columnBO.setColumnNameL1(oldColumn.getColumnNameL1());
                columnBO.setColumnNameL2(oldColumn.getColumnNameL2());
                columnBO.setChartIndex(oldColumn.getChartIndex());
                columnBO.setColumnIndex(oldColumn.getColumnIndex());
                columnBO.setColumnStyle(oldColumn.getColumnStyle());
                columnBO.setIsDisplay(oldColumn.getIsDisplay());

            } else {
                columnBO.setColumnName(md.getColumnName(i).replace("_", " "));
                String columnType = md.getColumnTypeName(i);
                if ("Date".equalsIgnoreCase(columnType) || "datetime".equalsIgnoreCase(columnType)) {
                    columnBO.setColumnStyle("DateTime");
                } else if ("Number".equalsIgnoreCase(columnType) || "float".equalsIgnoreCase(columnType)) {
                    columnBO.setColumnStyle("Float");
                } else if ("int".equalsIgnoreCase(columnType)) {
                    columnBO.setColumnStyle("Integer");
                }
                columnBO.setColumnIndex(Long.valueOf(i));
                columnBO.setIsDisplay(1L);
            }
            v2AdmFuncQueryColumnServiceImpl.saveOrUpdate(columnBO);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        V2AdmFuncQueryBO param = (V2AdmFuncQueryBO) event.getObject();
        if (param != null) {
            try {
                v2AdmFuncQueryServiceImpl.saveOrUpdate(param);
                MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
            } catch (AppException e) {
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public void onRowCancel(RowEditEvent event) {
        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.edit.cancel"), ((V2AdmFuncQueryBO) event.getObject()).getQueryName()));
    }

    public void onTestDirectQuery(V2AdmFuncQueryBO obj) {
        if (obj == null) {
            MessageUtil.setErrorMessageFromRes("object.is.null");
        } else {
            try {
                if (QUERY_TYPE.DIRECT.equalsIgnoreCase(obj.getQueryType())) {
                    Connection connection = null;
                    NamedParameterStatement st = null;
                    ResultSet rs = null;
                    try {
                        String connectType = "ORACLE";
                        if (obj.getDbId() != null && obj.getDbId() != 0) {
                            V2AdmDatabase db = new V2AdmDatabaseServiceImpl().findById(obj.getDbId());
                            if (db != null) {
                                ServiceUtil service = new ServiceUtil();
                                connection = service.connect(db);
                                if ("com.facebook.presto.jdbc.PrestoDriver".equalsIgnoreCase(db.getDriver())) {
                                    connectType = "HIVE";
                                } else {
                                    st = new NamedParameterStatement(connection, obj.getQueryString());
                                }
                            }
                        } else {
                            st = new NamedParameterStatement(HibernateUtil.connectDbOracle(), obj.getQueryString());
                        }
                        if ("ORACLE".equalsIgnoreCase(connectType)) {
                            if (st != null) {
                                Map<String, Object> filters = new HashMap<>();
                                String functionCode = obj.getId().getFunctionCode();
                                Long queryId = obj.getId().getQueryId();
                                filters.put("id.functionCode-EXAC", functionCode);
                                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                                orders.put("controlIndex", "ASC");
                                Map<String, Object> sqlRes = new HashMap<>();
                                sqlRes.put("(query_id in( 0 , ?)) ", queryId);
                                List<V2AdmFuncQueryCondBO> lstConds = v2AdmFuncQueryCondServiceImpl.findList(filters, sqlRes, orders);
                                for (V2AdmFuncQueryCondBO condition : lstConds) {
                                    if ("date".equalsIgnoreCase(condition.getDataType())) {
                                        st.setTimestamp(condition.getColumnCode(), new Timestamp((new Date()).getTime()));
                                    } else if ("number".equalsIgnoreCase(condition.getDataType())) {
                                        st.setDouble(condition.getColumnCode(), 1D);
                                    } else if ("text".equalsIgnoreCase(condition.getDataType())) {
                                        st.setString(condition.getColumnCode(), "");
                                    }
                                }
                                st.setFetchSize(1);
                                try {
                                    rs = st.executeQuery();
                                } catch (SQLException ex) {
                                    Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
                                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("common.adm.query.error.execute") + " (" + ex.getMessage() + ")");
                                    return;
                                }
                                saveFuncQueryColumn(rs, obj);
                            }
                        } else if ("HIVE".equalsIgnoreCase(connectType)) {
                            Map<String, Object> filters = new HashMap<>();
                            String functionCode = obj.getId().getFunctionCode();
                            Long queryId = obj.getId().getQueryId();
                            filters.put("id.functionCode-EXAC", functionCode);
                            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                            orders.put("controlIndex", "ASC");
                            Map<String, Object> sqlRes = new HashMap<>();
                            sqlRes.put("(query_id in( 0 , ?)) ", queryId);
                            List<V2AdmFuncQueryCondBO> lstConds = v2AdmFuncQueryCondServiceImpl.findList(filters, sqlRes, orders);
                            String queryCore = obj.getQueryString();
                            for (V2AdmFuncQueryCondBO condition : lstConds) {
                                if ("date".equalsIgnoreCase(condition.getDataType())) {
                                    queryCore = queryCore.replace(":" + condition.getColumnCode(), " to_date('01/10/2017') ");
                                } else if ("number".equalsIgnoreCase(condition.getDataType())) {
                                    queryCore = queryCore.replace(":" + condition.getColumnCode(), " 1 ");
                                } else if ("text".equalsIgnoreCase(condition.getDataType())) {
                                    queryCore = queryCore.replace(":" + condition.getColumnCode(), " '' ");
                                }
                            }
                            Statement stmt = connection.createStatement();
                            try {
                                String sqlRun = "with tbl as (select *, row_number() OVER () rowid from ( " + queryCore + " ) ) select * from tbl "
                                        + " where  rowid  < 2 ";
                                rs = stmt.executeQuery(sqlRun);
                            } catch (SQLException ex) {
                                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("common.adm.query.error.execute") + " (" + ex.getMessage() + ")");
                                return;
                            } finally {
                                if (stmt != null) {
                                    try {
                                        stmt.close();
                                    } catch (Exception ex) {
                                        Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                }
                            }
                            saveFuncQueryColumn(rs, obj);
                        }
                        MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
                    } catch (Exception e) {
                        MessageUtil.setErrorMessageFromRes("validator.updateFail");
                        Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
                    } finally {
                        if (connection != null) {
                            connection.close();
                        }
                        if (rs != null) {
                            rs.close();
                        }
                    }
                }
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("validator.updateFail");
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }

    }

    public V2AdmFuncQueryColumnBO getOldColumn(String functionCode, Long queryId, String columnCode, List<V2AdmFuncQueryColumnBO> oldColumn) {
        for (V2AdmFuncQueryColumnBO bo : oldColumn) {
            if (bo.getId().getFunctionCode().equalsIgnoreCase(functionCode)
                    && bo.getId().getQueryId().equals(queryId)
                    && bo.getId().getColumnCode().equalsIgnoreCase(columnCode)) {
                return bo;
            }
        }
        return null;
    }

    public String validateQueryForm(V2AdmFuncQueryBO form) {
        //validate form start
        if (form == null) {
            return "Form is NULL";
        } else {
//            form.getId().setFunctionCode(selectedObj.getFunctionCode());
            if (!validateQueryString(form.getQueryString())) {
                return MessageUtil.getResourceBundleMessage("sql.is.not.select.query");
            }
        }
        //validate form end
        return OK;
    }

    public V2AdmFuncQueryBO createAdmFuncQueryBO(V2AdmFuncQueryBO form) throws Exception {
        V2AdmFuncQueryBO bo;
        if (form.getId() != null && form.getId().getQueryId() != null && !form.getId().getQueryId().equals(0L)) {
//            Map<String, Object> filters = new HashMap<>();
//            filters.put("id.functionCode-EXAC", form.getId().getFunctionCode());
//            filters.put("id.queryId", form.getId().getQueryId());
//            List<V2AdmFuncQueryBO> listFind = v2AdmFuncQueryServiceImpl.findList(filters);
//            if (!listFind.isEmpty()) {
//                bo = listFind.get(0);
//            } else {
//                bo = new V2AdmFuncQueryBO();
//            }
            bo = v2AdmFuncQueryServiceImpl.findById(form.getId());
        } else {
            bo = new V2AdmFuncQueryBO();
            bo.setId(new AdmFuncQueryId(ServiceUtil.getSequence("SQ_V2_ADM_FUNC_QUERY"), form.getId().getFunctionCode()));
        }
        bo.setQueryName(form.getQueryName());
        bo.setQueryType(form.getQueryType());
        bo.setQueryIndex(form.getQueryIndex());
        bo.setQueryString(form.getQueryString() != null ? form.getQueryString().trim() : null);
        bo.setDbId(form.getDbId());
        return bo;
    }

    public static boolean validateQueryString(String queryString) {
        boolean rs = true;
        String[] blackList = {"truncate ", "update ", "insert ", "delete ", "alter ", "drop ", "create "};
        for (String item : blackList) {
            if (queryString.toLowerCase().contains(item)) {
                return false;
            }
        }
        return rs;
    }

    public static int countMatcher(String input, String regex) {
        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = input.indexOf(regex, lastIndex + 1);
            if (lastIndex != -1) {
                count++;
            }
        }
        return count;
    }

    public static List<String> validateDirectQuery(String sql) {
        List<String> listParams = new ArrayList();

        int lastIndex = 0;
        int count = 0;
        while (lastIndex != -1) {
            lastIndex = sql.indexOf(":", lastIndex + 1);
            if (lastIndex == sql.length() - 1) {
                return null;
            }
            if (lastIndex != -1) {
                //Kiem tra ky tu truoc dau :
                if (sql.charAt(lastIndex - 1) != ' ' && sql.charAt(lastIndex - 1) != '=') {
                    return null;
                } else {
                    //Kiem tra so luong ky tu ' phia truoc dau :
                    int countSingle = countMatcher(sql.substring(0, lastIndex), "'");
                    if (countSingle % 2 == 0) {
                        int spaceIndex = sql.indexOf(" ", lastIndex + 1);
                        int lineIndex = sql.indexOf("\n", lastIndex + 1);
                        int rIndex = sql.indexOf("\r", lastIndex + 1);
                        int index = -1;
                        if (spaceIndex >= 0) {
                            if (index >= 0) {
                                index = Math.min(index, spaceIndex);
                            } else {
                                index = spaceIndex;
                            }
                        }
                        if (lineIndex >= 0) {
                            if (index >= 0) {
                                index = Math.min(index, lineIndex);
                            } else {
                                index = lineIndex;
                            }
                        }
                        if (rIndex >= 0) {
                            if (index >= 0) {
                                index = Math.min(index, rIndex);
                            } else {
                                index = rIndex;
                            }
                        }

                        if (index >= 0) {
                            String variable = sql.substring(lastIndex + 1, index);
                            if (!listParams.contains(variable)) {
                                listParams.add(variable.trim());
                            }

                        } else {
                            String variable = sql.substring(lastIndex + 1);
                            if (!listParams.contains(variable)) {
                                listParams.add(variable.trim());
                            }
                        }
                    }

                }
                count++;

            }
        }
        System.out.println(count);
        return listParams;
    }

    public void onDeleteQuery(V2AdmFuncQueryBO obj) {
        if (obj != null) {
            try {
                String deleteResult = deleteQuery(obj);
                if (OK.equalsIgnoreCase(deleteResult)) {
                    MessageUtil.setInfoMessageFromRes("validator.deleteSuccess");
                } else {
                    MessageUtil.setErrorMessageFromRes("validator.deleteFail");
                }
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("validator.deleteFail");
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public String deleteQuery(V2AdmFuncQueryBO obj) {
        if (obj != null) {
            try {
                Map<String, Object> filter = new HashMap<>();
                filter.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
                filter.put("id.queryId", obj.getId().getQueryId());
                List<V2AdmFuncQueryColumnBO> listColumns = v2AdmFuncQueryColumnServiceImpl.findList(filter);
                if (listColumns != null && !listColumns.isEmpty()) {
                    v2AdmFuncQueryColumnServiceImpl.delete(listColumns);
                }
                List<V2AdmFuncQueryCondBO> listConds = v2AdmFuncQueryCondServiceImpl.findList(filter);
                if (listConds != null && !listConds.isEmpty()) {
                    for (V2AdmFuncQueryCondBO item : listConds) {
                        String result = deleteQueryCond(item);
                        if (!OK.equals(result)) {
                            return "ERROR";
                        }
                    }
                }
                v2AdmFuncQueryServiceImpl.delete(obj);
                return OK;
            } catch (Exception e) {
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
                return "ERROR";
            }
        }
        return OK;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Children of V2AdmFuncQuery">
    public void showTabChildOfQuery(V2AdmFuncQueryBO obj, String tab) {
        showChildOfQuery = true;
        selectedQueryObj = obj;
        System.out.println(tab);
        if ("columns".equalsIgnoreCase(tab)) {
            tabIndex = 0;
            prepareConfigQueryColumns(obj);

        } else if ("conditions".equalsIgnoreCase(tab)) {
            tabIndex = 1;
            prepareConfigQueryConditions(obj);

        } else if ("groupSum".equalsIgnoreCase(tab)) {
            tabIndex = 2;
            prepareConfigQueryColGroup(obj);

        }

    }
    //<editor-fold defaultstate="collapsed" desc="Columns">

    public void prepareConfigQueryColumns(V2AdmFuncQueryBO obj) {
        selectedQueryObj = obj;
        if (QUERY_TYPE.DIRECT.equalsIgnoreCase(selectedQueryObj.getQueryType())) {
            isDirectSql = true;
        } else {
            isDirectSql = false;
        }
//        if (obj == null) {
//            MessageUtil.setErrorMessageFromRes("object.is.null");
//        } else {
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
            filters.put("id.queryId", obj.getId().getQueryId());
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("columnIndex", "ASC");
            lazyModelFuncQueryColumns = new LazyDataModelBaseNewV2<>(v2AdmFuncQueryColumnServiceImpl, filters, orders);
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("ERROR");
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
//        }
    }

    public void prepareEditQueryColumn(V2AdmFuncQueryColumnBO obj, boolean isEdit) {
        this.isEdit = isEdit;
        if (isEdit) {
            addOrUpdateQueryColumnObj = obj.cloneObject();
        } else {
            addOrUpdateQueryCondObj = new V2AdmFuncQueryCondBO();
            addOrUpdateQueryColumnObj.setId(new AdmFuncQueryColumnId(selectedQueryObj.getId().getFunctionCode(), selectedQueryObj.getId().getQueryId(), null));
        }
    }

    public void onRowEditColumn(RowEditEvent event) {
        V2AdmFuncQueryColumnBO param = (V2AdmFuncQueryColumnBO) event.getObject();
        if (param != null) {
            try {
                v2AdmFuncQueryColumnServiceImpl.saveOrUpdate(param);
                MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
            } catch (AppException e) {
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public void onRowEditColumnCancel(RowEditEvent event) {
        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.edit.cancel"), ((V2AdmFuncQueryColumnBO) event.getObject()).getColumnName()));
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Conditions">
    public void prepareConfigQueryConditions(V2AdmFuncQueryBO obj) {
        selectedQueryObj = obj;
        if (QUERY_TYPE.DIRECT.equalsIgnoreCase(selectedQueryObj.getQueryType())) {
            isDirectSql = true;
        } else {
            isDirectSql = false;
        }
//        if (obj == null) {
//            MessageUtil.setErrorMessageFromRes("object.is.null");
//        } else {
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
//                filters.put("id.queryId", obj.getId().getQueryId());
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("controlIndex", "ASC");
            Map<String, Object> sqlRes = new HashMap<>();
            sqlRes.put("(query_id in( 0 , ?)) ", obj.getId().getQueryId());
            lazyModelFuncQueryCond = new LazyDataModelBaseNewV2<>(v2AdmFuncQueryCondServiceImpl, filters, sqlRes, orders);
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("ERROR");
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
//        }
    }

    public void onRowEditCond(RowEditEvent event) {
        V2AdmFuncQueryCondBO param = (V2AdmFuncQueryCondBO) event.getObject();
        if (param != null) {
            try {
                v2AdmFuncQueryCondServiceImpl.saveOrUpdate(param);
                MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
            } catch (AppException e) {
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public void onRowEditCondCancel(RowEditEvent event) {
        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.edit.cancel"), ((V2AdmFuncQueryCondBO) event.getObject()).getControlLabel()));
    }

    public void prepareEditQueryCond(V2AdmFuncQueryCondBO obj, boolean isEdit) {
        this.isEdit = isEdit;
        if (isEdit) {
            addOrUpdateQueryCondObj = obj.cloneObject();
            selectedRequireAndCommon = new String[2];
            if (addOrUpdateQueryCondObj.getIsRequire() != null) {
                selectedRequireAndCommon[0] = "isRequire";
            }
            if (addOrUpdateQueryCondObj.getId().getQueryId().equals(0L)) {
                selectedRequireAndCommon[1] = "isCommon";
            }
            if (QUERY_TYPE.DIRECT.equalsIgnoreCase(selectedQueryObj.getQueryType())) {
                isDirectSql = true;
            } else {
                isDirectSql = false;
            }
        } else {
            addOrUpdateQueryCondObj = new V2AdmFuncQueryCondBO();
            addOrUpdateQueryCondObj.setId(new AdmFuncQueryCondId(selectedQueryObj.getId().getFunctionCode(), selectedQueryObj.getId().getQueryId(), null));
            selectedRequireAndCommon = new String[2];
        }
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("id.functionCode-EXAC", selectedQueryObj.getId().getFunctionCode());
            filters.put("id.queryId", selectedQueryObj.getId().getQueryId());
            List<V2AdmFuncQueryColumnBO> lstFind = v2AdmFuncQueryColumnServiceImpl.findList(filters);
//            List<V2AdmFuncQueryCondBO> lstConds = v2AdmFuncQueryCondServiceImpl.findList(filters);
            if (!lstFind.isEmpty()) {
                listColumnAvailable = new ArrayList<>();
                for (V2AdmFuncQueryColumnBO item : lstFind) {
//                    boolean availbale = true;
//                    for (V2AdmFuncQueryCondBO cond : lstConds) {
//                        if (item.getId().getColumnCode().equals(cond.getColumnCode())) {
//                            availbale = false;
//                            break;
//                        }
//                    }
//                    if (availbale) {
                    listColumnAvailable.add(new AdmCommonBO(item.getId().getColumnCode(), item.getId().getColumnCode() + " (" + item.getColumnName() + ")"));
//                    }
                }
            }

        } catch (Exception e) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void onSaveOrUpdateQueryCondition() {
        ResultSet rs = null;
        try {
            String functionCode = addOrUpdateQueryCondObj.getId().getFunctionCode();
            Long queryId = addOrUpdateQueryCondObj.getId().getQueryId();
            String columnCode = addOrUpdateQueryCondObj.getColumnCode();
            if (columnCode == null || "".equals(columnCode)) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.columnCode")));
                return;
            }
            String compareType = addOrUpdateQueryCondObj.getCompareType();
            String dataType = addOrUpdateQueryCondObj.getDataType();
            String defaultValue = addOrUpdateQueryCondObj.getDefaultValue();
            String controlLabel = addOrUpdateQueryCondObj.getControlLabel();
            String databind = addOrUpdateQueryCondObj.getDatabind();
            Long controlIndex = addOrUpdateQueryCondObj.getControlIndex();
            String controlType = addOrUpdateQueryCondObj.getControlType();
            String isRequire = "";
            String isCommon = "";
            if (selectedRequireAndCommon.length > 0) {
                for (int i = 0; i < selectedRequireAndCommon.length; i++) {
                    String item = selectedRequireAndCommon[i];
                    if ("isRequire".equals(item)) {
                        isRequire = item;
                    } else if ("isCommon".equals(item)) {
                        isCommon = item;
                    }
                }
            }

//            databind = databind.replace("\r", " ");
//            databind = databind.replace("\n", " ");
            if (databind != null && !"".equals(databind)) {
                int selectIndex = databind.toLowerCase().indexOf("select");
                if (selectIndex > 0) {
                    Connection conn = null;
                    V2AdmFuncQueryBO obj = v2AdmFuncQueryServiceImpl.findById(new AdmFuncQueryId(queryId, functionCode));
                    if (obj.getDbId() != null && obj.getDbId() != 0L) {
                        V2AdmDatabase db = new V2AdmDatabaseServiceImpl().findById(obj.getDbId());
                        if (db != null) {
                            ServiceUtil service = new ServiceUtil();
                            conn = service.connect(db);
                        }
                    } else {
                        conn = HibernateUtil.connectDbOracle();
                    }
                    Statement st = conn.createStatement();
                    rs = st.executeQuery(databind);
                    rs.close();
                    st.close();
                    conn.close();
                }
            }
            V2AdmFuncQueryCondBO temp;
            if (!"".equals(isCommon)) {
                queryId = COMMON_CONDITION;
            }
            if (addOrUpdateQueryCondObj.getId() != null && addOrUpdateQueryCondObj.getId().getControlId() != null && !addOrUpdateQueryCondObj.getId().equals(0L)) {
                temp = v2AdmFuncQueryCondServiceImpl.findById(addOrUpdateQueryCondObj.getId());
                if (temp == null) {
                    temp = new V2AdmFuncQueryCondBO();
                }
                if (!"".equals(isCommon)) {
                    queryId = COMMON_CONDITION;
                    if (!queryId.equals(temp.getId().getQueryId())) {
                        v2AdmFuncQueryCondServiceImpl.delete(temp);
                    }
                    temp.getId().setQueryId(queryId);
                }
            } else {
                temp = new V2AdmFuncQueryCondBO();
                temp.setId(new AdmFuncQueryCondId(functionCode, queryId, ServiceUtil.getSequence("SQ_V2_ADM_FUNC_QUERY_COND")));
            }
            temp.setColumnCode(columnCode);
            temp.setDataType(dataType);
            temp.setCompareType(compareType);
            if (!"".equals(defaultValue.trim())) {
                temp.setDefaultValue(defaultValue);
            }
            temp.setControlLabel(controlLabel);
            if (!"".equals(databind.trim())) {
                temp.setDatabind(databind);
            }
            temp.setControlIndex(controlIndex);
            if (!"".equals(isRequire)) {
                temp.setIsRequire(isRequire);
            } else {
                temp.setIsRequire(null);
            }
            temp.setControlType(controlType);
            if ("SessionValue".equalsIgnoreCase(controlType)) {
                temp.setIsDisplay(0L);
            } else {
                temp.setIsDisplay(1L);
            }
            v2AdmFuncQueryCondServiceImpl.saveOrUpdate(temp);
            if (isEdit) {
                MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
            } else {
                MessageUtil.setInfoMessageFromRes("validator.addSuccess");
            }
            RequestContext.getCurrentInstance().execute("PF('dlgAddCondition').hide();");
        } catch (Exception ex) {
            if (isEdit) {
                MessageUtil.setErrorMessageFromRes("validator.updateFail");
            } else {
                MessageUtil.setErrorMessageFromRes("validator.addFail");
            }
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void preShowChildCond(V2AdmFuncQueryCondBO obj) {
        try {
            selectedQueryCondObj = obj;
            addOrUpdateQueryCondDependObj = new V2AdmFuncQueryCondDependBO();
            addOrUpdateQueryCondDependObj.setId(new AdmFuncQueryCondDependId(selectedQueryCondObj.getId().getFunctionCode(), selectedQueryCondObj.getId().getQueryId(), selectedQueryCondObj.getId().getControlId(), null));
            try {
                Map<String, Object> filters = new HashMap<>();
                filters.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
                filters.put("id.queryId", obj.getId().getQueryId());
                filters.put("id.controlParent", obj.getId().getControlId());
                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                orders.put("condDependIndex", "ASC");
                lazyModelFuncQueryCondDepend = new LazyDataModelBaseNewV2<>(v2AdmFuncQueryCondDependServiceImpl, filters, orders);
            } catch (Exception e) {
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
            Map<String, Object> filters = new HashMap<>();
            filters.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
            filters.put("id.queryId", obj.getId().getQueryId());
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("controlIndex", "ASC");
            Map<String, Object> sqlRes = new HashMap<>();
            sqlRes.put(" (control_id <> ? )", obj.getId().getControlId());
            List<V2AdmFuncQueryCondBO> listFind = v2AdmFuncQueryCondServiceImpl.findList(filters, sqlRes, orders);
            filters.clear();
            filters.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
            filters.put("id.queryId", obj.getId().getQueryId());
            filters.put("id.controlParent", obj.getId().getControlId());
            List<V2AdmFuncQueryCondDependBO> lstCondDepends = v2AdmFuncQueryCondDependServiceImpl.findList(filters);
            listConditions = new ArrayList<>();
            if (listFind != null && !listFind.isEmpty()) {
                for (V2AdmFuncQueryCondBO item : listFind) {
                    boolean availbale = true;
                    for (V2AdmFuncQueryCondDependBO cond : lstCondDepends) {
                        if (item.getId().getControlId().equals(cond.getId().getControlChild())) {
                            availbale = false;
                            break;
                        }
                    }
                    if (availbale) {
                        listConditions.add(new AdmCommonBO(item.getId().getControlId().toString(), item.getControlLabel()));
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onDeleteCondDepend(V2AdmFuncQueryCondDependBO obj) {
        try {
            v2AdmFuncQueryCondDependServiceImpl.delete(obj);
            MessageUtil.setInfoMessageFromRes("validator.deleteSuccess");
        } catch (AppException | SysException ex) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
            MessageUtil.setErrorMessageFromRes("validator.deleteFail");
        }
    }

    public void onAddChildrenCondition() {
        try {
            if (addOrUpdateQueryCondDependObj.getCondDependIndex() == null || addOrUpdateQueryCondDependObj.getCondDependIndex().equals(0L)) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.condDependIndex")));
                return;
            }
            if (addOrUpdateQueryCondDependObj.getId().getControlChild() == null || addOrUpdateQueryCondDependObj.getId().getControlChild().equals(0L)) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.columnCode")));
                return;
            }
            V2AdmFuncQueryCondDependBO bo = addOrUpdateQueryCondDependObj.cloneObject();
            bo.setId(new AdmFuncQueryCondDependId(selectedQueryCondObj.getId().getFunctionCode(), selectedQueryCondObj.getId().getQueryId(), selectedQueryCondObj.getId().getControlId(), addOrUpdateQueryCondDependObj.getId().getControlChild()));
            bo.setCondDependIndex(addOrUpdateQueryCondDependObj.getCondDependIndex());
            v2AdmFuncQueryCondDependServiceImpl.save(addOrUpdateQueryCondDependObj);
            MessageUtil.setInfoMessageFromRes("validator.addSuccess");
        } catch (AppException | SysException ex) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
            MessageUtil.setErrorMessageFromRes("validator.addFail");
        }
    }

    public void onDeleteQueryCond(V2AdmFuncQueryCondBO obj) {
        try {
            String deleteResult = deleteQueryCond(obj);
            if (OK.equalsIgnoreCase(deleteResult)) {
                MessageUtil.setInfoMessageFromRes("validator.deleteSuccess");
            } else {
                MessageUtil.setErrorMessageFromRes("validator.deleteFail");
            }
        } catch (Exception e) {
            MessageUtil.setInfoMessageFromRes("validator.deleteFail");
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public String deleteQueryCond(V2AdmFuncQueryCondBO obj) {
        try {
            Map<String, Object> filter = new HashMap<>();
            filter.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
            filter.put("id.queryId", obj.getId().getQueryId());
            filter.put("id.controlParent", obj.getId().getControlId());
            List<V2AdmFuncQueryCondDependBO> listFind = v2AdmFuncQueryCondDependServiceImpl.findList(filter);
            v2AdmFuncQueryCondServiceImpl.delete(obj);
            if (listFind != null && !listFind.isEmpty()) {
                v2AdmFuncQueryCondDependServiceImpl.delete(listFind);
            }
            return OK;
        } catch (Exception e) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            return "ERROR";
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Group/Sum">
    public void prepareConfigQueryColGroup(V2AdmFuncQueryBO obj) {
        selectedQueryObj = obj;
        if (obj == null) {
            MessageUtil.setErrorMessageFromRes("object.is.null");
        } else {
            try {
                Map<String, Object> filters = new HashMap<>();
                filters.put("id.functionCode-EXAC", obj.getId().getFunctionCode());
                filters.put("id.queryId", obj.getId().getQueryId());
                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                orders.put("groupIndex", "ASC");
                lazyModelFuncQueryColGroup = new LazyDataModelBaseNewV2<>(v2AdmFuncQueryColGroupServiceImpl, filters, orders);
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("ERROR");
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public void prepareEditQueryColGroup(V2AdmFuncQueryColGroupBO obj, boolean isEdit) {
        this.isEdit = isEdit;
        if (isEdit) {
            addOrUpdateQueryColGroupObj = obj.cloneObject();
        } else {
            addOrUpdateQueryColGroupObj = new V2AdmFuncQueryColGroupBO();
            addOrUpdateQueryColGroupObj.setId(new AdmFuncQueryColGroupId(selectedQueryObj.getId().getFunctionCode(), selectedQueryObj.getId().getQueryId(), null));
        }
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("id.functionCode-EXAC", selectedQueryObj.getId().getFunctionCode());
            filters.put("id.queryId", selectedQueryObj.getId().getQueryId());
            List<V2AdmFuncQueryColumnBO> lstFind = v2AdmFuncQueryColumnServiceImpl.findList(filters);
            List<V2AdmFuncQueryColGroupBO> lstConds = v2AdmFuncQueryColGroupServiceImpl.findList(filters);
            if (!lstFind.isEmpty()) {
                listColumnAvailable = new ArrayList<>();
                for (V2AdmFuncQueryColumnBO item : lstFind) {
                    boolean availbale = true;
                    for (V2AdmFuncQueryColGroupBO cond : lstConds) {
                        if (item.getId().getColumnCode().equals(cond.getId().getColumnCode())) {
                            availbale = false;
                            break;
                        }
                    }
                    if (availbale) {
                        listColumnAvailable.add(new AdmCommonBO(item.getId().getColumnCode(), item.getId().getColumnCode() + " (" + item.getColumnName() + ")"));
                    }
                }
            }

        } catch (Exception e) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void onSaveOrUpdateQueryColGroup() {
        try {
            String functionCode = addOrUpdateQueryColGroupObj.getId().getFunctionCode();
            Long queryId = addOrUpdateQueryColGroupObj.getId().getQueryId();
            String columnCode = addOrUpdateQueryColGroupObj.getId().getColumnCode();
            if (columnCode == null || "".equals(columnCode)) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.columnCode")));
                return;
            }
            String groupType = addOrUpdateQueryColGroupObj.getGroupType();
            Long groupIndex = addOrUpdateQueryColGroupObj.getGroupIndex();
            V2AdmFuncQueryColGroupBO temp;
            temp = v2AdmFuncQueryColGroupServiceImpl.findById(addOrUpdateQueryColGroupObj.getId());
            if (temp == null) {
                temp = new V2AdmFuncQueryColGroupBO();
                temp.setId(new AdmFuncQueryColGroupId(functionCode, queryId, columnCode));
            }
            temp.setGroupType(groupType);
            temp.setGroupIndex(groupIndex);
            v2AdmFuncQueryColGroupServiceImpl.saveOrUpdate(temp);
            if (isEdit) {
                MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
            } else {
                MessageUtil.setInfoMessageFromRes("validator.addSuccess");
            }
            RequestContext.getCurrentInstance().execute("PF('dlgAddColGroup').hide();");
        } catch (Exception ex) {
            if (isEdit) {
                MessageUtil.setErrorMessageFromRes("validator.updateFail");
            } else {
                MessageUtil.setErrorMessageFromRes("validator.addFail");
            }
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowEditColGroup(RowEditEvent event) {
        V2AdmFuncQueryColGroupBO param = (V2AdmFuncQueryColGroupBO) event.getObject();
        if (param != null) {
            try {
                v2AdmFuncQueryColGroupServiceImpl.saveOrUpdate(param);
                MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
            } catch (AppException e) {
                Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }

    public void onRowEditColGroupCancel(RowEditEvent event) {
        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.edit.cancel"), ((V2AdmFuncQueryColGroupBO) event.getObject()).getId().getColumnCode()));
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Import & Export">
    public DefaultStreamedContent onExportXml() {
        Document dom;
        // instance of a DocumentBuilderFactory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {

            DocumentBuilder db = dbf.newDocumentBuilder();
            dom = db.newDocument();
            Element rootEle = dom.createElement("root");
            List<String> listTrimFunc = new ArrayList();
            List<String> listExportFunction = new ArrayList();

            if (StringUtils.isNotNull(functionCodes)) {
                String[] listFunc = functionCodes.split(",");
                for (String func : listFunc) {
                    listTrimFunc.add(func.trim());
                }
            }
            Map<String, Object> filters = new HashMap<>();
            if (!listTrimFunc.isEmpty()) {
                filters.put("functionCode", listTrimFunc);
            }
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("functionCode", "ASC");
            List<V2AdmFuncBO> listAdmFuncBO = v2AdmFuncServiceImpl.findList(filters, orders);
            if (listAdmFuncBO != null) {
                for (V2AdmFuncBO func : listAdmFuncBO) {
                    listExportFunction.add(func.getFunctionCode());
                    Element function = dom.createElement("function");
                    function.setAttribute("function_code", func.getFunctionCode());
                    function.setAttribute("function_name", func.getFunctionName() == null ? "" : func.getFunctionName());
                    function.setAttribute("function_type", func.getFunctionType() == null ? "" : func.getFunctionType());
                    function.setAttribute("function_title", func.getFunctionTitle() == null ? "" : func.getFunctionTitle());
                    function.setAttribute("index_order", func.getIndexOrder() == null ? "" : func.getIndexOrder().toString());
                    function.setAttribute("refresh_interval", func.getRefreshInterval() == null ? "" : func.getRefreshInterval().toString());
//                    function.setAttribute("notify_class", func.getNotifyClass() == null ? "" : func.getNotifyClass().toString());
                    //r3929_sonnh26_08/07/2013_start
//                    function.setAttribute("moduleId", func.getModules() == null ? "" : func.getModules().toString());
                    //r3929_sonnh26_08/07/2013_end

                    filters.clear();
                    filters.put("id.functionCode-EXAC", func.getFunctionCode());
                    orders.clear();
                    orders.put("queryIndex", "ASC");

                    List<V2AdmFuncQueryBO> listQuery = v2AdmFuncQueryServiceImpl.findList(filters, orders);
                    function.setAttribute("query_size", String.valueOf(listQuery.size()));
                    List<String> listQueryId = new ArrayList();
                    for (int i = 0; i < listQuery.size(); i++) {
                        V2AdmFuncQueryBO queryBo = listQuery.get(i);
                        Element query = dom.createElement("query");
                        query.setAttribute("query_id", queryBo.getId().getQueryId().toString());
                        listQueryId.add(queryBo.getId().getQueryId().toString());
                        query.setAttribute("query_name", queryBo.getQueryName() == null ? "" : queryBo.getQueryName());
                        query.setAttribute("query_index", queryBo.getQueryIndex() == null ? "" : queryBo.getQueryIndex().toString());
                        query.setAttribute("query_type", queryBo.getQueryType() == null ? "" : queryBo.getQueryType());
                        query.setAttribute("chart_type", queryBo.getChartType() == null ? "" : queryBo.getChartType());
                        query.setAttribute("grid_name", queryBo.getGridName() == null ? "" : queryBo.getGridName());
                        query.setAttribute("is_large", queryBo.getIsLarge() == null ? "" : queryBo.getIsLarge());
                        query.setAttribute("sheet_name", queryBo.getSheetName() == null ? "" : queryBo.getSheetName());

                        Element querySql = dom.createElement("query_string_sql");
                        querySql.appendChild(dom.createTextNode(queryBo.getQueryString() == null ? "" : queryBo.getQueryString()));
                        query.appendChild(querySql);
                        function.appendChild(query);
                    }
                    List<V2AdmFuncQueryColumnBO> lstFuncQueryColumn = v2AdmFuncQueryColumnServiceImpl.findList(filters);
                    for (V2AdmFuncQueryColumnBO queryColumn : lstFuncQueryColumn) {
                        //a.column_code, a.column_name, a.column_index, a.is_display, a.column_style, a.chart_index, a.column_name_l1, a.column_name_l2, a.formula,

                        Element column = dom.createElement("column");
                        column.setAttribute("query_id", queryColumn.getId().getQueryId().toString());
                        column.setAttribute("column_code", queryColumn.getId().getColumnCode());
                        column.setAttribute("column_name", queryColumn.getColumnName() == null ? "" : queryColumn.getColumnName());
                        column.setAttribute("column_index", queryColumn.getColumnIndex() == null ? "" : queryColumn.getColumnIndex().toString());
                        column.setAttribute("is_display", queryColumn.getIsDisplay() == null ? "" : queryColumn.getIsDisplay().toString());
                        column.setAttribute("column_style", queryColumn.getColumnStyle() == null ? "" : queryColumn.getColumnStyle());
                        column.setAttribute("chart_index", queryColumn.getChartIndex() == null ? "" : queryColumn.getChartIndex());
                        column.setAttribute("column_name_l1", queryColumn.getColumnNameL1() == null ? "" : queryColumn.getColumnNameL1());
                        column.setAttribute("column_name_l2", queryColumn.getColumnNameL2() == null ? "" : queryColumn.getColumnNameL2());
                        column.setAttribute("formula", queryColumn.getFormula() == null ? "" : queryColumn.getFormula());
                        function.appendChild(column);
                    }

                    List<V2AdmFuncQueryColCalBO> lstFuncQueryColCal = v2AdmFuncQueryColCalServiceImpl.findList(filters);
                    for (V2AdmFuncQueryColCalBO queryColumn : lstFuncQueryColCal) {
                        Element column = dom.createElement("column_caculate");
                        column.setAttribute("query_id", queryColumn.getId().getQueryId().toString());
                        column.setAttribute("column_code", queryColumn.getId().getColumnCode());
                        column.setAttribute("cal_type", queryColumn.getId().getCalType());

                        column.setAttribute("column_name", queryColumn.getColumnName() == null ? "" : queryColumn.getColumnName());
                        column.setAttribute("column_index", queryColumn.getColumnIndex() == null ? "" : queryColumn.getColumnIndex().toString());
                        column.setAttribute("is_display", queryColumn.getIsDisplay() == null ? "" : queryColumn.getIsDisplay().toString());
                        column.setAttribute("column_style", queryColumn.getColumnStyle() == null ? "" : queryColumn.getColumnStyle());
                        column.setAttribute("chart_index", queryColumn.getChartIndex() == null ? "" : queryColumn.getChartIndex());
                        column.setAttribute("kpi_column_code", queryColumn.getKpiColumnCode() == null ? "" : queryColumn.getKpiColumnCode());
                        column.setAttribute("compare_with_control", queryColumn.getCompareWithControl() == null ? "" : queryColumn.getCompareWithControl().toString());
                        column.setAttribute("compare_with_value", queryColumn.getCompareWithValue() == null ? "" : queryColumn.getCompareWithValue());
                        column.setAttribute("value_column_code", queryColumn.getValueColumnCode() == null ? "" : queryColumn.getValueColumnCode());
                        column.setAttribute("column_name_l1", queryColumn.getColumnNameL1() == null ? "" : queryColumn.getColumnNameL1());
                        column.setAttribute("column_name_l2", queryColumn.getColumnNameL2() == null ? "" : queryColumn.getColumnNameL2());
                        function.appendChild(column);
                    }
                    List<V2AdmFuncQueryColGroupBO> lstFuncQueryColGroup = v2AdmFuncQueryColGroupServiceImpl.findList(filters);
                    for (V2AdmFuncQueryColGroupBO colGroupBo : lstFuncQueryColGroup) {
                        Element colGroup = dom.createElement("colGroup");
                        //column_code, a.group_type, a.group_index
                        colGroup.setAttribute("query_id", colGroupBo.getId().getQueryId().toString());
                        colGroup.setAttribute("column_code", colGroupBo.getId().getColumnCode());
                        colGroup.setAttribute("group_type", colGroupBo.getGroupType() == null ? "" : colGroupBo.getGroupType());
                        colGroup.setAttribute("group_index", colGroupBo.getGroupIndex() == null ? "" : colGroupBo.getGroupIndex().toString());
                        function.appendChild(colGroup);
                    }
                    List<V2AdmFuncQueryCondBO> lstFuncQueryCond = v2AdmFuncQueryCondServiceImpl.findList(filters);
                    for (V2AdmFuncQueryCondBO cond : lstFuncQueryCond) {
                        Element condition = dom.createElement("condition");
                        condition.setAttribute("query_id", cond.getId().getQueryId().toString());
                        condition.setAttribute("control_id", cond.getId().getControlId().toString());
                        condition.setAttribute("column_code", cond.getColumnCode() == null ? "" : cond.getColumnCode());
                        condition.setAttribute("compare_type", cond.getCompareType() == null ? "" : cond.getCompareType());
                        condition.setAttribute("data_type", cond.getDataType() == null ? "" : cond.getDataType());
                        condition.setAttribute("default_value", cond.getDefaultValue() == null ? "" : cond.getDefaultValue());
                        condition.setAttribute("control_label", cond.getControlLabel() == null ? "" : cond.getControlLabel());
                        condition.setAttribute("control_index", cond.getControlIndex() == null ? "" : cond.getControlIndex().toString());
                        condition.setAttribute("control_type", cond.getControlType() == null ? "" : cond.getControlType());
                        condition.setAttribute("is_require", cond.getIsRequire() == null ? "" : cond.getIsRequire());
                        condition.setAttribute("cal_type", cond.getCalType() == null ? "" : cond.getCalType());
                        condition.setAttribute("min_value", cond.getMinValue() == null ? "" : cond.getMinValue());
                        condition.setAttribute("max_value", cond.getMaxValue() == null ? "" : cond.getMaxValue());
                        condition.setAttribute("is_display", cond.getIsDisplay() == null ? "" : cond.getIsDisplay().toString());
                        condition.appendChild(dom.createTextNode(cond.getDatabind() == null ? "" : cond.getDatabind()));
                        function.appendChild(condition);
                    }
                    List<V2AdmFuncQueryCondDependBO> lstFuncQueryCondDepend = v2AdmFuncQueryCondDependServiceImpl.findList(filters);
                    for (V2AdmFuncQueryCondDependBO condDepend : lstFuncQueryCondDepend) {
                        //a.query_id, a.control_parrent, a.control_child, a.compare_type
                        if (listQueryId.contains(condDepend.getId().getQueryId().toString()) || condDepend.getId().getQueryId() == 0) {
                            Element condition = dom.createElement("condition_depend");
                            condition.setAttribute("query_id", condDepend.getId().getQueryId().toString());
                            condition.setAttribute("control_child", condDepend.getId().getControlChild().toString());
                            condition.setAttribute("control_parrent", condDepend.getId().getControlParent().toString());
                            condition.setAttribute("compare_type", condDepend.getCompareType() == null ? "" : condDepend.getCompareType());
                            condition.setAttribute("cond_depend_index", condDepend.getCondDependIndex() == null ? "" : condDepend.getCondDependIndex().toString());
                            function.appendChild(condition);
                        }
                    }
                    List<V2AdmFuncQueryButtonBO> listButton = v2AdmFuncQueryButtonServiceImpl.findList(filters);
                    for (V2AdmFuncQueryButtonBO button : listButton) {
                        Element role = dom.createElement("button");
                        role.setAttribute("query_id", button.getId().getQueryId().toString());
                        role.setAttribute("button_code", button.getId().getButtonCode());
                        role.setAttribute("button_name", button.getButtonName() == null ? "" : button.getButtonName());
                        role.setAttribute("button_type", button.getButtonType() == null ? "" : button.getButtonType());
                        role.setAttribute("action_str", button.getActionStr() == null ? "" : button.getActionStr());
                        role.setAttribute("num_rows_grid", button.getNumRowsGrid() == null ? "" : button.getNumRowsGrid());
                        role.setAttribute("icon", button.getIcon() == null ? "" : button.getIcon());
                        role.setAttribute("btn_index", button.getBtnIndex() == null ? "" : button.getBtnIndex().toString());
                        function.appendChild(role);
                    }

                    rootEle.appendChild(function);
                }

                for (String str : listTrimFunc) {

                    if (!listExportFunction.contains(str)) {
                        Element error = dom.createElement("export_error");
                        error.appendChild(dom.createTextNode(str));
                        rootEle.appendChild(error);
                    }
                }

                try {
                    dom.appendChild(rootEle);
                    // send DOM to file
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    DOMSource source = new DOMSource(dom);
                    Result result = new StreamResult(outputStream);
                    transformer.transform(source, result);
                    String fileDownloadFileName = "Export" + new Date().getTime() + ".xml";
                    ExternalContext extContext = FacesContext.getCurrentInstance().getExternalContext();
                    return new DefaultStreamedContent(new ByteArrayInputStream(outputStream.toByteArray()), extContext.getMimeType(fileDownloadFileName), fileDownloadFileName);
                } catch (Exception e) {
                    Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
                    MessageUtil.setErrorMessage(e.getMessage());
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public void onImportXml(FileUploadEvent event) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        List<String> listFunctionSuccess = new ArrayList();
        List<String> listTrimFunc = new ArrayList();
        UploadedFile fileUpload = event.getFile();
        Map<String, Object> filters = new HashMap<>();
        try {
            if (fileUpload != null) {

                if (StringUtils.isNotNull(functionCodes)) {
                    String[] listFunc = functionCodes.split(",");
                    for (String func : listFunc) {
                        listTrimFunc.add(func.trim());
                    }
                }

                Document doc = db.parse(fileUpload.getInputstream());
                Element rootEle = doc.getDocumentElement();
                NodeList functionList = rootEle.getElementsByTagName("function");
                if (functionList != null && functionList.getLength() > 0) {
                    for (int i = 0; i < functionList.getLength(); i++) {
                        Object[] objs = v2AdmFuncServiceImpl.openTransaction();
                        Session session = (Session) objs[0];
                        Transaction tx = (Transaction) objs[1];
                        tx.begin();
                        try {
                            Map<String, Object> mapOld2New = new HashMap();

                            //Function
                            Element function = (Element) functionList.item(i);
                            String functionCode = getAttribute(function, "function_code");
                            String functionName = getAttribute(function, "function_name");
                            String functionType = getAttribute(function, "function_type");
                            String functionTitle = getAttribute(function, "function_title");
                            String indexOrder = getAttribute(function, "index_order");
                            String refreshInterval = getAttribute(function, "refresh_interval");
                            if (listTrimFunc.isEmpty() || listTrimFunc.contains(functionCode)) {
                                V2AdmFuncBO functionBO = v2AdmFuncServiceImpl.findById(functionCode);
                                if (functionBO == null) {
                                    functionBO = new V2AdmFuncBO(functionCode);
                                }
                                functionBO.setFunctionName(functionName);
                                functionBO.setFunctionTitle(functionTitle);
                                functionBO.setFunctionType(functionType);
                                functionBO.setIndexOrder(StringUtils.isNotNull(indexOrder) ? Long.parseLong(indexOrder) : null);
                                functionBO.setRefreshInterval(StringUtils.isNotNull(refreshInterval) ? Long.parseLong(refreshInterval) : null);
                                v2AdmFuncServiceImpl.saveOrUpdate(functionBO, session, tx, false);
                                //Query
                                filters.put("id.functionCode-EXAC", functionCode);
                                List<V2AdmFuncQueryBO> listObjExist = v2AdmFuncQueryServiceImpl.findList(filters);
                                for (V2AdmFuncQueryBO obj : listObjExist) {
                                    v2AdmFuncQueryServiceImpl.delete(obj, session, tx, false);
                                }
                                String queryStringSql = "";
                                NodeList queryList = function.getElementsByTagName("query");
                                if (queryList != null && queryList.getLength() > 0) {
                                    for (int j = 0; j < queryList.getLength(); j++) {
                                        Element query = (Element) queryList.item(j);
                                        Long queryNew = ServiceUtil.getSequence("SQ_V2_ADM_FUNC_QUERY");
                                        String queryIdOld = getAttribute(query, "query_id");
                                        mapOld2New.put("query" + queryIdOld, queryNew);

                                        String queryName = getAttribute(query, "query_name");
                                        String queryIndex = getAttribute(query, "query_index");
                                        String queryType = getAttribute(query, "query_type");
                                        String chartType = getAttribute(query, "chart_type");
                                        String gridName = getAttribute(query, "grid_name");
                                        String isLarge = getAttribute(query, "is_large");
                                        String sheetName = getAttribute(query, "sheet_name");

                                        NodeList querySqlList = query.getElementsByTagName("query_string_sql");
                                        if (querySqlList != null && querySqlList.getLength() > 0) {
                                            Node querySqlNode = querySqlList.item(0);
                                            queryStringSql = querySqlNode.getTextContent();
                                        }
                                        V2AdmFuncQueryBO queryBO = new V2AdmFuncQueryBO(new AdmFuncQueryId(queryNew, functionCode), queryName,
                                                queryStringSql, (StringUtils.isNotNull(queryIndex) ? Long.parseLong(queryIndex) : null),
                                                queryType, chartType, gridName, isLarge,
                                                sheetName);
                                        v2AdmFuncQueryServiceImpl.saveOrUpdate(queryBO, session, tx, false);

                                    }
                                }
//                                commonDAO.log(session, userLogin, "functionCode(" + functionCode + ")" + " import dữ liệu, câu SQL: " + queryStringSql, Constant.FUNCTION.REPORT);

                                filters.clear();
                                filters.put("id.functionCode-EXAC", functionCode);
                                List<V2AdmFuncQueryCondBO> listQueryCondExist = v2AdmFuncQueryCondServiceImpl.findList(filters);
                                for (V2AdmFuncQueryCondBO obj : listQueryCondExist) {
                                    v2AdmFuncQueryCondServiceImpl.delete(obj, session, tx, false);
                                }

                                NodeList conditionList = function.getElementsByTagName("condition");
                                if (conditionList != null && conditionList.getLength() > 0) {
                                    for (int j = 0; j < conditionList.getLength(); j++) {
                                        Node node = conditionList.item(j);
                                        String queryIdOld = getAttribute(node, "query_id");
                                        Long queryIdNew;//= null;
                                        if ("0".equalsIgnoreCase(queryIdOld)) {
                                            queryIdNew = 0L;
                                        } else {
                                            queryIdNew = (Long) mapOld2New.get("query" + queryIdOld);
                                        }
                                        if (queryIdNew != null) {
                                            String controlIdOld = getAttribute(node, "control_id");
                                            Long controlIdNew = ServiceUtil.getSequence("SQ_V2_ADM_FUNC_QUERY_COND");
                                            mapOld2New.put("condition" + controlIdOld, controlIdNew);
                                            String columnCode = getAttribute(node, "column_code");
                                            String compareType = getAttribute(node, "compare_type");
                                            String dataType = getAttribute(node, "data_type");
                                            String defaultValue = getAttribute(node, "default_value");
                                            String controlLabel = getAttribute(node, "control_label");
                                            String controlIndex = getAttribute(node, "control_index");
                                            String controlType = getAttribute(node, "control_type");
                                            String isRequire = getAttribute(node, "is_require");
                                            String calType = getAttribute(node, "cal_type");
                                            String minValue = getAttribute(node, "min_value");
                                            String maxValue = getAttribute(node, "max_value");
                                            String isDisplay = getAttribute(node, "is_display");

                                            String databind = node.getTextContent();

                                            V2AdmFuncQueryCondBO conditionBO = new V2AdmFuncQueryCondBO(
                                                    new AdmFuncQueryCondId(functionCode, queryIdNew, controlIdNew),
                                                    columnCode, compareType, dataType, defaultValue, controlLabel, databind,
                                                    (StringUtils.isNotNull(controlIndex) ? Long.parseLong(controlIndex) : null),
                                                    controlType, isRequire, calType, minValue, maxValue,
                                                    (StringUtils.isNotNull(isDisplay) ? Long.parseLong(isDisplay) : null));
                                            v2AdmFuncQueryCondServiceImpl.saveOrUpdate(conditionBO, session, tx, false);
                                        }
                                    }
                                }

                                filters.clear();
                                filters.put("id.functionCode-EXAC", functionCode);
                                List<V2AdmFuncQueryColumnBO> listQueryColumnExist = v2AdmFuncQueryColumnServiceImpl.findList(filters);
                                for (V2AdmFuncQueryColumnBO obj : listQueryColumnExist) {
                                    v2AdmFuncQueryColumnServiceImpl.delete(obj, session, tx, false);
                                }

                                NodeList columnList = function.getElementsByTagName("column");
                                if (columnList != null && columnList.getLength() > 0) {
                                    for (int j = 0; j < columnList.getLength(); j++) {
                                        Node column = columnList.item(j);
                                        String queryIdOld = getAttribute(column, "query_id");
                                        Long queryIdNew = (Long) mapOld2New.get("query" + queryIdOld);
                                        if (queryIdNew != null) {
                                            String columnCode = getAttribute(column, "column_code");
                                            String columnName = getAttribute(column, "column_name");
                                            String columnIndex = getAttribute(column, "column_index");
                                            String isDisplay = getAttribute(column, "is_display");
                                            String columnStyle = getAttribute(column, "column_style");
                                            String chartIndex = getAttribute(column, "chart_index");
                                            String columnNameL1 = getAttribute(column, "column_name_l1");
                                            String columnNameL2 = getAttribute(column, "column_name_l2");
                                            String formula = getAttribute(column, "formula");
//                                            String factor = getAttribute(column, "factor");
                                            V2AdmFuncQueryColumnBO columnBO = new V2AdmFuncQueryColumnBO(new AdmFuncQueryColumnId(functionCode, queryIdNew, columnCode),
                                                    columnName, (StringUtils.isNotNull(columnIndex) ? Long.parseLong(columnIndex) : null),
                                                    (StringUtils.isNotNull(isDisplay) ? Long.parseLong(isDisplay) : null), columnStyle,
                                                    chartIndex, columnNameL1, columnNameL2, formula);
                                            v2AdmFuncQueryColumnServiceImpl.saveOrUpdate(columnBO, session, tx, false);
                                        }
                                    }
                                }
                                List<V2AdmFuncQueryColCalBO> listQueryColumnCalExist = v2AdmFuncQueryColCalServiceImpl.findList(filters);
                                for (V2AdmFuncQueryColCalBO obj : listQueryColumnCalExist) {
                                    v2AdmFuncQueryColCalServiceImpl.delete(obj, session, tx, false);
                                }

                                NodeList colCalList = function.getElementsByTagName("column_caculate");
                                if (colCalList != null && colCalList.getLength() > 0) {
                                    for (int j = 0; j < colCalList.getLength(); j++) {
                                        Node column = colCalList.item(j);
                                        String queryIdOld = getAttribute(column, "query_id");
                                        Long queryIdNew = (Long) mapOld2New.get("query" + queryIdOld);
                                        if (queryIdNew != null) {
                                            String columnCode = getAttribute(column, "column_code");
                                            String calType = getAttribute(column, "cal_type");
                                            String columnName = getAttribute(column, "column_name");
                                            String columnIndex = getAttribute(column, "column_index");
                                            String isDisplay = getAttribute(column, "is_display");
                                            String columnStyle = getAttribute(column, "column_style");
                                            String chartIndex = getAttribute(column, "chart_index");
                                            String kpiColumnCode = getAttribute(column, "kpi_column_code");
                                            String compareWithControlOld = getAttribute(column, "compare_with_control");
                                            Long compareWithControlNew = (Long) mapOld2New.get("condition" + compareWithControlOld);
                                            String compareWithValue = getAttribute(column, "compare_with_value");
                                            String valueColumnCode = getAttribute(column, "value_column_code");
                                            String columnNameL1 = getAttribute(column, "column_name_l1");
                                            String columnNameL2 = getAttribute(column, "column_name_l2");
                                            V2AdmFuncQueryColCalBO columnBO = new V2AdmFuncQueryColCalBO(
                                                    new AdmFuncQueryColCalId(functionCode, queryIdNew, columnCode, calType),
                                                    columnName,
                                                    (StringUtils.isNotNull(columnIndex) ? Long.parseLong(columnIndex) : null),
                                                    columnStyle, chartIndex, kpiColumnCode, compareWithControlNew,
                                                    compareWithValue, valueColumnCode, columnNameL1, columnNameL2,
                                                    (StringUtils.isNotNull(isDisplay) ? Long.parseLong(isDisplay) : null));
                                            v2AdmFuncQueryColCalServiceImpl.saveOrUpdate(columnBO, session, tx, false);

                                        }
                                    }
                                }
                                List<V2AdmFuncQueryColGroupBO> listQueryColumnGroupExist = v2AdmFuncQueryColGroupServiceImpl.findList(filters);
                                for (V2AdmFuncQueryColGroupBO obj : listQueryColumnGroupExist) {
                                    v2AdmFuncQueryColGroupServiceImpl.delete(obj, session, tx, false);
                                }

                                NodeList colGroupList = function.getElementsByTagName("colGroup");
                                if (colGroupList != null && colGroupList.getLength() > 0) {
                                    for (int j = 0; j < colGroupList.getLength(); j++) {
                                        Node column = colGroupList.item(j);
                                        String queryIdOld = getAttribute(column, "query_id");
                                        Long queryIdNew = (Long) mapOld2New.get("query" + queryIdOld);
                                        if (queryIdNew != null) {
                                            String columnCode = getAttribute(column, "column_code");
                                            String groupType = getAttribute(column, "group_type");
                                            String groupIndex = getAttribute(column, "group_index");
                                            V2AdmFuncQueryColGroupBO columnBO = new V2AdmFuncQueryColGroupBO(
                                                    new AdmFuncQueryColGroupId(functionCode, queryIdNew, columnCode),
                                                    groupType,
                                                    (StringUtils.isNotNull(groupIndex) ? Long.parseLong(groupIndex) : null));
                                            v2AdmFuncQueryColGroupServiceImpl.saveOrUpdate(columnBO, session, tx, false);
                                        }
                                    }
                                }
                                List<V2AdmFuncQueryCondDependBO> listQueryCondDependExist = v2AdmFuncQueryCondDependServiceImpl.findList(filters);
                                for (V2AdmFuncQueryCondDependBO obj : listQueryCondDependExist) {
                                    v2AdmFuncQueryCondDependServiceImpl.delete(obj, session, tx, false);
                                }

                                NodeList condDependList = function.getElementsByTagName("condition_depend");
                                if (condDependList != null && condDependList.getLength() > 0) {
                                    for (int j = 0; j < condDependList.getLength(); j++) {
                                        Node condition = condDependList.item(j);
                                        String queryIdOld = getAttribute(condition, "query_id");
                                        Long queryIdNew;//= null;
                                        if ("0".equalsIgnoreCase(queryIdOld)) {
                                            queryIdNew = 0L;
                                        } else {
                                            queryIdNew = (Long) mapOld2New.get("query" + queryIdOld);
                                        }
                                        if (queryIdNew != null) {
                                            String controlChildOld = getAttribute(condition, "control_child");
                                            Long controlChildNew = (Long) mapOld2New.get("condition" + controlChildOld);
                                            String controlParentOld = getAttribute(condition, "control_parrent");
                                            Long controlParentNew = (Long) mapOld2New.get("condition" + controlParentOld);
                                            String compareType = getAttribute(condition, "compare_type");
                                            String index = getAttribute(condition, "cond_depend_index");
                                            if (controlChildNew != null && controlParentNew != null) {
                                                V2AdmFuncQueryCondDependBO condDependBO = new V2AdmFuncQueryCondDependBO(
                                                        new AdmFuncQueryCondDependId(functionCode, queryIdNew, controlParentNew, controlChildNew));
                                                condDependBO.setCompareType(compareType);
                                                condDependBO.setCondDependIndex(index != null && "".equals(index) ? Long.valueOf(index) : null);
                                                v2AdmFuncQueryCondDependServiceImpl.saveOrUpdate(condDependBO, session, tx, false);
                                            }
                                        }
                                    }
                                }

                                List<V2AdmFuncQueryButtonBO> listQueryButtonExist = v2AdmFuncQueryButtonServiceImpl.findList(filters);
                                for (V2AdmFuncQueryButtonBO obj : listQueryButtonExist) {
                                    v2AdmFuncQueryButtonServiceImpl.delete(obj, session, tx, false);
                                }
                                NodeList buttonList = function.getElementsByTagName("button");
                                if (buttonList != null && buttonList.getLength() > 0) {
                                    for (int j = 0; j < buttonList.getLength(); j++) {
                                        Node button = buttonList.item(j);
                                        String queryIdOld = getAttribute(button, "query_id");
                                        Long queryIdNew = (Long) mapOld2New.get("query" + queryIdOld);
                                        if (queryIdNew != null) {
                                            String buttonCode = getAttribute(button, "button_code");
                                            String buttonName = getAttribute(button, "button_name");
                                            String buttonType = getAttribute(button, "button_type");
                                            String actionStr = getAttribute(button, "action_str");
                                            String numRowsGrid = getAttribute(button, "num_rows_grid");
                                            String icon = getAttribute(button, "icon");
                                            String btnIndex = getAttribute(button, "btn_index");
                                            V2AdmFuncQueryButtonBO buttonBO = new V2AdmFuncQueryButtonBO(
                                                    new AdmFuncQueryButtonId(functionCode, queryIdNew, buttonCode),
                                                    buttonName, buttonType, actionStr, numRowsGrid, icon,
                                                    (StringUtils.isNotNull(btnIndex) ? Long.parseLong(btnIndex) : null));
                                            v2AdmFuncQueryButtonServiceImpl.saveOrUpdate(buttonBO, session, tx, false);
                                        }
                                    }
                                }
                                listFunctionSuccess.add(functionCode);
                                tx.commit();
                            }
                        } catch (Exception e) {
                            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
                            tx.rollback();
                        }
                    }

                }
                String alert = "";
                for (String successFunc : listFunctionSuccess) {
                    alert += "Import function: " + successFunc + " -> success\n";
                }
                boolean hasError = false;
                for (String function : listTrimFunc) {
                    if (!listFunctionSuccess.contains(function)) {
                        alert += "Import function: " + function + " -> error\n";
                        hasError = true;
                    }
                }
                if (hasError) {
                    MessageUtil.setWarnMessage(alert);
                } else {
                    MessageUtil.setInfoMessage(alert);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
            MessageUtil.setInfoMessage(e.getMessage());
        }
    }

    public void preImportXml(boolean isImport) {
        this.isImport = isImport;
        this.functionCodes = "";
    }

    public static String getAttribute(Node element, String attName) {
        NamedNodeMap attrs = element.getAttributes();
        if (attrs == null) {
            return null;
        }
        Node attN = attrs.getNamedItem(attName);
        if (attN == null) {
            return null;
        }
        if (!StringUtils.isNotNull(attN.getNodeValue())) {
            return null;
        }
        return attN.getNodeValue();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="setter & getter">
    public V2AdmFuncQueryColCalServiceImpl getV2AdmFuncQueryColCalServiceImpl() {
        return v2AdmFuncQueryColCalServiceImpl;
    }

    public void setV2AdmFuncQueryColCalServiceImpl(V2AdmFuncQueryColCalServiceImpl v2AdmFuncQueryColCalServiceImpl) {
        this.v2AdmFuncQueryColCalServiceImpl = v2AdmFuncQueryColCalServiceImpl;
    }

    public V2AdmFuncQueryButtonServiceImpl getV2AdmFuncQueryButtonServiceImpl() {
        return v2AdmFuncQueryButtonServiceImpl;
    }

    public void setV2AdmFuncQueryButtonServiceImpl(V2AdmFuncQueryButtonServiceImpl v2AdmFuncQueryButtonServiceImpl) {
        this.v2AdmFuncQueryButtonServiceImpl = v2AdmFuncQueryButtonServiceImpl;
    }

    public String getFunctionCodes() {
        return functionCodes;
    }

    public void setFunctionCodes(String functionCodes) {
        this.functionCodes = functionCodes;
    }

    public V2AdmFuncQueryCondDependServiceImpl getV2AdmFuncQueryCondDependServiceImpl() {
        return v2AdmFuncQueryCondDependServiceImpl;
    }

    public void setV2AdmFuncQueryCondDependServiceImpl(V2AdmFuncQueryCondDependServiceImpl v2AdmFuncQueryCondDependServiceImpl) {
        this.v2AdmFuncQueryCondDependServiceImpl = v2AdmFuncQueryCondDependServiceImpl;
    }

    public LazyDataModel<V2AdmFuncQueryCondDependBO> getLazyModelFuncQueryCondDepend() {
        return lazyModelFuncQueryCondDepend;
    }

    public void setLazyModelFuncQueryCondDepend(LazyDataModel<V2AdmFuncQueryCondDependBO> lazyModelFuncQueryCondDepend) {
        this.lazyModelFuncQueryCondDepend = lazyModelFuncQueryCondDepend;
    }

    public List<AdmCommonBO> getListColumnAvailable() {
        return listColumnAvailable;
    }

    public void setListColumnAvailable(List<AdmCommonBO> listColumnAvailable) {
        this.listColumnAvailable = listColumnAvailable;
    }

    public List<AdmCommonBO> getLstComapareType() {
        return lstComapareType;
    }

    public void setLstComapareType(List<AdmCommonBO> lstComapareType) {
        this.lstComapareType = lstComapareType;
    }

    public V2AdmFuncQueryColGroupServiceImpl getV2AdmFuncQueryColGroupServiceImpl() {
        return v2AdmFuncQueryColGroupServiceImpl;
    }

    public void setV2AdmFuncQueryColGroupServiceImpl(V2AdmFuncQueryColGroupServiceImpl v2AdmFuncQueryColGroupServiceImpl) {
        this.v2AdmFuncQueryColGroupServiceImpl = v2AdmFuncQueryColGroupServiceImpl;
    }

    public V2AdmFuncQueryCondBO getSelectedQueryCondObj() {
        return selectedQueryCondObj;
    }

    public void setSelectedQueryCondObj(V2AdmFuncQueryCondBO selectedQueryCondObj) {
        this.selectedQueryCondObj = selectedQueryCondObj;
    }

    public V2AdmFuncQueryCondBO getAddOrUpdateQueryCondObj() {
        return addOrUpdateQueryCondObj;
    }

    public void setAddOrUpdateQueryCondObj(V2AdmFuncQueryCondBO addOrUpdateQueryCondObj) {
        this.addOrUpdateQueryCondObj = addOrUpdateQueryCondObj;
    }

    public V2AdmFuncQueryColumnBO getAddOrUpdateQueryColumnObj() {
        return addOrUpdateQueryColumnObj;
    }

    public void setAddOrUpdateQueryColumnObj(V2AdmFuncQueryColumnBO addOrUpdateQueryColumnObj) {
        this.addOrUpdateQueryColumnObj = addOrUpdateQueryColumnObj;
    }

    public LazyDataModel<V2AdmFuncQueryColGroupBO> getLazyModelFuncQueryColGroup() {
        return lazyModelFuncQueryColGroup;
    }

    public void setLazyModelFuncQueryColGroup(LazyDataModel<V2AdmFuncQueryColGroupBO> lazyModelFuncQueryColGroup) {
        this.lazyModelFuncQueryColGroup = lazyModelFuncQueryColGroup;
    }

    public LazyDataModel<V2AdmFuncQueryColumnBO> getLazyModelFuncQueryColumns() {
        return lazyModelFuncQueryColumns;
    }

    public void setLazyModelFuncQueryColumns(LazyDataModel<V2AdmFuncQueryColumnBO> lazyModelFuncQueryColumns) {
        this.lazyModelFuncQueryColumns = lazyModelFuncQueryColumns;
    }

    public V2AdmFuncQueryServiceImpl getV2AdmFuncQueryServiceImpl() {
        return v2AdmFuncQueryServiceImpl;
    }

    public void setV2AdmFuncQueryServiceImpl(V2AdmFuncQueryServiceImpl v2AdmFuncQueryServiceImpl) {
        this.v2AdmFuncQueryServiceImpl = v2AdmFuncQueryServiceImpl;
    }

    public LazyDataModel<V2AdmFuncQueryBO> getLazyModelFuncQuery() {
        return lazyModelFuncQuery;
    }

    public void setLazyModelFuncQuery(LazyDataModel<V2AdmFuncQueryBO> lazyModelFuncQuery) {
        this.lazyModelFuncQuery = lazyModelFuncQuery;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public V2AdmFuncServiceImpl getV2AdmFuncServiceImpl() {
        return v2AdmFuncServiceImpl;
    }

    public void setV2AdmFuncServiceImpl(V2AdmFuncServiceImpl v2AdmFuncServiceImpl) {
        this.v2AdmFuncServiceImpl = v2AdmFuncServiceImpl;
    }

    public V2AdmFuncBO getSelectedObj() {
        return selectedObj;
    }

    public void setSelectedObj(V2AdmFuncBO selectedObj) {
        this.selectedObj = selectedObj;
    }

    public V2AdmFuncBO getAddOrUpdateObj() {
        return addOrUpdateObj;
    }

    public void setAddOrUpdateObj(V2AdmFuncBO addOrUpdateObj) {
        this.addOrUpdateObj = addOrUpdateObj;
    }

    public LazyDataModel<V2AdmFuncBO> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<V2AdmFuncBO> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public boolean isIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public V2AdmFuncQueryBO getAddOrUpdateQueryObj() {
        return addOrUpdateQueryObj;
    }

    public void setAddOrUpdateQueryObj(V2AdmFuncQueryBO addOrUpdateQueryObj) {
        this.addOrUpdateQueryObj = addOrUpdateQueryObj;
    }

    public V2AdmFuncQueryCondServiceImpl getV2AdmFuncQueryCondServiceImpl() {
        return v2AdmFuncQueryCondServiceImpl;
    }

    public void setV2AdmFuncQueryCondServiceImpl(V2AdmFuncQueryCondServiceImpl v2AdmFuncQueryCondServiceImpl) {
        this.v2AdmFuncQueryCondServiceImpl = v2AdmFuncQueryCondServiceImpl;
    }

    public V2AdmFuncQueryColumnServiceImpl getV2AdmFuncQueryColumnServiceImpl() {
        return v2AdmFuncQueryColumnServiceImpl;
    }

    public void setV2AdmFuncQueryColumnServiceImpl(V2AdmFuncQueryColumnServiceImpl v2AdmFuncQueryColumnServiceImpl) {
        this.v2AdmFuncQueryColumnServiceImpl = v2AdmFuncQueryColumnServiceImpl;
    }

    public V2AdmFuncQueryBO getSelectedQueryObj() {
        return selectedQueryObj;
    }

    public void setSelectedQueryObj(V2AdmFuncQueryBO selectedQueryObj) {
        this.selectedQueryObj = selectedQueryObj;
    }

    public LazyDataModel<V2AdmFuncQueryCondBO> getLazyModelFuncQueryCond() {
        return lazyModelFuncQueryCond;
    }

    public void setLazyModelFuncQueryCond(LazyDataModel<V2AdmFuncQueryCondBO> lazyModelFuncQueryCond) {
        this.lazyModelFuncQueryCond = lazyModelFuncQueryCond;
    }

    public boolean isShowChildOfQuery() {
        return showChildOfQuery;
    }

    public void setShowChildOfQuery(boolean showChildOfQuery) {
        this.showChildOfQuery = showChildOfQuery;
    }

    public int getTabIndex() {
        return tabIndex;
    }

    public void setTabIndex(int tabIndex) {
        this.tabIndex = tabIndex;
    }

    public V2AdmFuncQueryColGroupBO getAddOrUpdateQueryColGroupObj() {
        return addOrUpdateQueryColGroupObj;
    }

    public void setAddOrUpdateQueryColGroupObj(V2AdmFuncQueryColGroupBO addOrUpdateQueryColGroupObj) {
        this.addOrUpdateQueryColGroupObj = addOrUpdateQueryColGroupObj;
    }

    public boolean isIsImport() {
        return isImport;
    }

    public void setIsImport(boolean isImport) {
        this.isImport = isImport;
    }

    public String[] getSelectedRequireAndCommon() {
        return selectedRequireAndCommon;
    }

    public void setSelectedRequireAndCommon(String[] selectedRequireAndCommon) {
        this.selectedRequireAndCommon = selectedRequireAndCommon;
    }

    public List<AdmCommonBO> getListConditions() {
        return this.listConditions;
    }

    public void setListConditions(List<AdmCommonBO> listConditions) {
        this.listConditions = listConditions;
    }

    public V2AdmFuncQueryCondDependBO getAddOrUpdateQueryCondDependObj() {
        return addOrUpdateQueryCondDependObj;
    }

    public void setAddOrUpdateQueryCondDependObj(V2AdmFuncQueryCondDependBO addOrUpdateQueryCondDependObj) {
        this.addOrUpdateQueryCondDependObj = addOrUpdateQueryCondDependObj;
    }

    public boolean isIsDirectSql() {
        return isDirectSql;
    }

    public void setIsDirectSql(boolean isDirectSql) {
        this.isDirectSql = isDirectSql;
    }

    public List<V2AdmDatabase> getListDatabases() {
        return listDatabases;
    }

    public void setListDatabases(List<V2AdmDatabase> listDatabases) {
        this.listDatabases = listDatabases;
    }

    //</editor-fold>
}
