package com.viettel.controller;

/**
 * @author thuypq<thuypq@viettel.com.vn>
 *
 */
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.lazy.LazyDataModelBaseNewV2;
import com.viettel.model.adm.V2AdmDatabase;
import com.viettel.persistence.adm.V2AdmDatabaseServiceImpl;
import com.viettel.security.PassTranformer;
import com.viettel.util.MessageUtil;
import org.primefaces.model.LazyDataModel;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.event.RowEditEvent;

@ViewScoped
@ManagedBean
public class DatabaseController implements Serializable {

    private static final long serialVersionUID = -3642317500315969057L;

    @ManagedProperty(value = "#{v2AdmDatabaseService}")
    V2AdmDatabaseServiceImpl v2AdmDatabaseService;

    private V2AdmDatabase selectObj;
    private LazyDataModel<V2AdmDatabase> lazyModel;

    private Long dbId;
    private String dbName;
    private String license;
    private String url;
    private String driver;
    private String username;
    private String password;
    private Long status;
    private boolean isEdit = false;

    @PostConstruct
    public void onStart() {
        Map<String, String> filter = new HashMap<>();
        LinkedHashMap<String, String> order = new LinkedHashMap<>();
        lazyModel = new LazyDataModelBaseNewV2<>(v2AdmDatabaseService, filter, order);
    }

    //<editor-fold desc="Add, Update & Delete">
    public void prepareEdit(V2AdmDatabase selectObj) {
        isEdit = true;
        this.selectObj = selectObj;
        dbId = selectObj.getDbId();
        dbName = selectObj.getDbName();
        license = selectObj.getLicense();
        url = selectObj.getUrl();
        username = selectObj.getUsername();
        password = selectObj.getPassword();
        status = selectObj.getStatus();
        driver = selectObj.getDriver();
    }

    public void clean() {
        isEdit = false;
        dbId = null;
        dbName = null;
        license = null;
        url = null;
        username = null;
        password = null;
        status = null;
        driver = null;
    }

    public void saveOrUpdate() {
        if (dbName == null || "".equals(dbName)) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"), MessageUtil.getResourceBundleMessage("label.dbName")));
            return;
        }

        if (url == null || "".equals(url)) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"), MessageUtil.getResourceBundleMessage("label.ip")));
            return;
        }

        if (username == null || "".equals(username)) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"), MessageUtil.getResourceBundleMessage("label.username")));
            return;
        }
        if (driver == null || "".equals(driver)) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"), MessageUtil.getResourceBundleMessage("label.driver")));
            return;
        }
        if (status == null || status == -1L) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"), MessageUtil.getResourceBundleMessage("label.status")));
            return;
        }
        if (!isEdit) {
            if (password == null || "".equals(password)) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"), MessageUtil.getResourceBundleMessage("table.header.password")));
                return;
            }
        }

        V2AdmDatabase config = new V2AdmDatabase();
        config.setDbName(dbName);
        config.setLicense(license);
        config.setUrl(url);
        config.setUsername(username);
        config.setStatus(status);
        config.setDriver(driver);
        if (password != null && !"".equals(password)) {
            String vietelSecKey = ResourceBundle.getBundle("dbConfig").getString("viettel_secure_key");
            PassTranformer.setInputKey(vietelSecKey);
            config.setPassword(PassTranformer.encrypt(password));
        } else {
            if (selectObj.getPassword() != null) {
                config.setPassword(selectObj.getPassword());
            }
        }

        if (isEdit) {
            config.setDbId(dbId);
        }
        try {
            v2AdmDatabaseService.saveOrUpdate(config);
            MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
            clean();
        } catch (SysException | AppException e) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void delete(V2AdmDatabase obj) {
        try {
            v2AdmDatabaseService.delete(obj);
            MessageUtil.setInfoMessageFromRes("validator.deleteSuccess");
        } catch (SysException | AppException e) {
            Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        V2AdmDatabase param = (V2AdmDatabase) event.getObject();
        if (param != null) {
            try {
                isEdit = true;
                dbId = param.getDbId();
                selectObj = v2AdmDatabaseService.findById(dbId);
                dbName = param.getDbName();
                license = param.getLicense();
                url = param.getUrl();
                username = param.getUsername();
                password = param.getPassword();
                status = param.getStatus();
                driver = param.getDriver();
                saveOrUpdate();
            } catch (AppException | SysException ex) {
                Logger.getLogger(DatabaseController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    public void onRowCancel(RowEditEvent event) {
        System.out.println(event);
    }

    //</editor-fold>
    public V2AdmDatabase getSelectObj() {
        return selectObj;
    }

    public void setSelectObj(V2AdmDatabase selectObj) {
        this.selectObj = selectObj;
    }

    public LazyDataModel<V2AdmDatabase> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<V2AdmDatabase> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public Long getDbId() {
        return dbId;
    }

    public void setDbId(Long dbId) {
        this.dbId = dbId;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public boolean isIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public V2AdmDatabaseServiceImpl getV2AdmDatabaseService() {
        return v2AdmDatabaseService;
    }

    public void setV2AdmDatabaseService(V2AdmDatabaseServiceImpl v2AdmDatabaseService) {
        this.v2AdmDatabaseService = v2AdmDatabaseService;
    }

}
