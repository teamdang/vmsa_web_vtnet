package com.viettel.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import com.viettel.model.ENodeBPlan;
import com.viettel.util.ObjectConcurrentMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean()
@ViewScoped
public class TraceController implements Serializable{
	
    protected final Logger logger = LoggerFactory.getLogger(getClass());
	private String statusCurrTask;
	private String pathLog;
	private ENodeBPlan eNodeBPlan;
	private String log1;
	private String log2;
	private String log3;
	private int progress;
	private String IP_CLIENT;
	
	private String infoPing1;
	private String infoPing2;
	private String infoPing3;
	
	@PostConstruct
	public void onStart(){
		try {
			IP_CLIENT = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteHost();
			IP_CLIENT = "localhost";
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
	}
	public void ping(int type){
		switch (type) {
			case 1 :
				log1="";
				break;
			case 2 :
				log2="";
				break;
			case 3 :
				log3="";
				break;
			default :
				break;
		}
		trace(type);
	}
	public void checklist(){
		ping(3);
	}
	private void trace(int type) {
		statusCurrTask = IP_CLIENT+eNodeBPlan.getId()+"-"+type;
		ObjectConcurrentMap.getInstance().remove(IP_CLIENT+eNodeBPlan.getId()+"-"+type);
		RequestContext.getCurrentInstance().execute("PF('pollProgress"+type+"').start()");
		RequestContext.getCurrentInstance().execute("PF('pollLog"+type+"').start()");
		
	}
	
	@SuppressWarnings("unchecked")
	public void loadLog(int type) {
		ConcurrentMap<Integer, String> objectReceive = null;
		if (statusCurrTask != null)
			objectReceive = (ConcurrentMap<Integer, String>) ObjectConcurrentMap.getInstance().get(statusCurrTask);
			
		if (objectReceive != null) {
//			if (traceRouteThread != null){
//				log = objectReceive.get(traceRouteThread.getIndexThread());
//			}
		}
	}
	public void loadProgress(int type) {
//		if (traceRouteThread.getState().status == BaseConnectThread.STOP) {
//			if (keyClientRoot.TRACE_ROUTE.equals(statusCurrTask)) {
//				MessageUtil.setInfoMessageFromRes("info.trace.route.success");
//			}else if (keyClientRoot.TRACE_PORT.equals(statusCurrTask)) {
//				MessageUtil.setInfoMessageFromRes("info.trace.port.success");
//				
//			} else if (keyClientRoot.SHOW_ROUTE.equals(statusCurrTask)) {
//				MessageUtil.setInfoMessageFromRes("info.show.route.success");
//			}
//			RequestContext.getCurrentInstance().update(":form");
//			RequestContext.getCurrentInstance().execute("pollProgress2.stop()");
//			RequestContext.getCurrentInstance().execute("pollLog2.stop()");
//			progress=100;
//		} else {
//			progress = 50;
//		}

	}
	public String getLog1() {
		return log1;
	}
	public void setLog1(String log) {
		this.log1 = log;
	}
	public ENodeBPlan geteNodeBPlan() {
		return eNodeBPlan;
	}
	public void seteNodeBPlan(ENodeBPlan eNodeBPlan) {
		this.eNodeBPlan = eNodeBPlan;
	}
	public String getInfoPing1() {
		return infoPing1;
	}
	public void setInfoPing1(String infoPing1) {
		this.infoPing1 = infoPing1;
	}
	public String getInfoPing2() {
		return infoPing2;
	}
	public void setInfoPing2(String infoPing2) {
		this.infoPing2 = infoPing2;
	}
	public String getInfoPing3() {
		return infoPing3;
	}
	public void setInfoPing3(String infoPing3) {
		this.infoPing3 = infoPing3;
	}
	public String getLog2() {
		return log2;
	}
	public void setLog2(String log2) {
		this.log2 = log2;
	}
	public String getLog3() {
		return log3;
	}
	public void setLog3(String log3) {
		this.log3 = log3;
	}
	
}
