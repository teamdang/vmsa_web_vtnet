package com.viettel.controller;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.FacesContext;

import com.viettel.model.*;
import com.viettel.util.*;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.context.RequestContext;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.ReorderEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.faces.component.visit.FullVisitContext;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.persistence.ActionCommandServiceImpl;
import com.viettel.persistence.ActionDetailServiceImpl;
import com.viettel.persistence.ActionServiceImpl;
import com.viettel.persistence.CommandDetailServiceImpl;
import com.viettel.persistence.CommandTelnetParserServiceImpl;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.NodeTypeServiceImpl;
import com.viettel.persistence.ParamInOutServiceImpl;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.persistence.VersionServiceImpl;
import org.primefaces.json.JSONObject;

@ViewScoped
@ManagedBean
public class ActionController implements Serializable {

    protected static final Logger logger = LoggerFactory.getLogger(ActionController.class);

    public static final Long ROOT_TREE_ID = 1L;
    public static final Integer NODE_EXPENDED = 1;
    public static final Integer NODE_NOT_EXPEND = 0;
    public static final String PARENT_NODE = "parent";
    public static final String CHILD_NODE = "child";
    public static final String IMPACT_LABEL_NODE = "impact_label";
    public static final String ROLLBACK_LABEL_NODE = "rollback_label";
    public static final Integer MAX_LENGTH_ACTION_NAME = 500;

    @ManagedProperty(value = "#{actionService}")
    private ActionServiceImpl actionServiceImpl;

    @ManagedProperty(value = "#{actionDetailService}")
    private ActionDetailServiceImpl actionDetailServiceImpl;

    @ManagedProperty(value = "#{vendorService}")
    private VendorServiceImpl vendorServiceImpl;

    @ManagedProperty(value = "#{nodeTypeService}")
    private NodeTypeServiceImpl nodeTypeServiceImpl;

    @ManagedProperty(value = "#{commandTelnetParserService}")
    private CommandTelnetParserServiceImpl commandTelnetParserServiceImpl;

    private TreeNode rootNode;
    private TreeNode selectedNode;
    private TreeNode selectedNodeForMove;

    private Action insertNode;
    private TreeNode moveNode;
    private boolean isEdit = false;
    private boolean isClone = false;

    private Action selectedAction; // action ma action detail link den
    private Vendor selectedVendor;
    private NodeType selectedNodeType;
    private Version selectedVersion;
    private ActionDetail actionDetail;
    private ActionDetail selectedActionDetail;
    private CommandDetail selectedCmdDetail;
    private CommandTelnetParser selectedCmdTelnetParser;
    private ActionCommand selectedActionCommand;

    private Long stationDetailStatus;
    private boolean isEditActionDetail;

    private List<ActionCommand> lstActionCommand = new LinkedList<>();
    private String userName;

    private List<ActionDetail> lstActionDetail = new ArrayList<>();
    private List<ActionCommand> lstActionCmdDel = new ArrayList<>();

    private String keyActionSearch = null;
    private String actionNameToClone;

    private boolean actionIsAproved; // bien xac nhan action da tham chieu den 1 template dc phe duyet hay chua
    //Quytv7 ghi log action
    private String logAction = "";
    private String className = ActionController.class.getName();

    @PostConstruct
    public void onStart() {
        try {
            actionIsAproved = false;
            //selectedNodeType = new NodeTypeServiceImpl().findById(Config.NODE_TYPE_ID_DEFAULT);
            userName = SessionWrapper.getCurrentUsername();
            actionDetail = new ActionDetail();
            insertNode = new Action();
            selectedAction = new Action();
            createTree();
            selectedActionDetail = new ActionDetail();
            selectedActionDetail.setActionCommands(new ArrayList<ActionCommand>(0));
            logAction = LogUtils.addContent("", "Login Function");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * @param dragDropEvent
     * @author huynx6 Keo action vao template
     */
    public void onDrop(DragDropEvent dragDropEvent) {
        Object data = dragDropEvent.getData();
        System.err.println(data);
        final String dargId = dragDropEvent.getDragId();
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
        final UIComponent[] found = new UIComponent[1];
        viewRoot.visitTree(new FullVisitContext(context), new VisitCallback() {
            @Override
            public VisitResult visit(VisitContext context, UIComponent component) {
                System.err.println(component.getId());
                if ("0_9_0_0".equals(component.getId())) {
                    found[0] = component;
                    return VisitResult.COMPLETE;
                }
                return VisitResult.ACCEPT;
            }
        });

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String left = params.get(dargId + "_left");
        String top = params.get(dargId + "_top");
        System.err.println("Left: " + left + " Top: " + top);
    }

    /**
     * @author huynx6 Tim kiem tren tree
     */
    public void searchActionNode() {
        Action root;
        logAction = LogUtils.addContent("", "Search action");
        try {
            root = actionServiceImpl.findById(ROOT_TREE_ID);
            if (root != null) {
                rootNode = new DefaultTreeNode("action", "Root", null);

                Map<String, Object> filter = new HashMap<>();
                if (keyActionSearch != null) {
                    logAction = LogUtils.addContent(logAction, "keyActionSearch" + keyActionSearch);
                    filter.put("name", keyActionSearch);
                }
                logAction = LogUtils.addContent(logAction, "Action: " + root.getName());
                List<Action> resultSearch = actionServiceImpl.findList(filter);

                Set<Action> actionParents = new HashSet<>();
                actionParents.addAll(resultSearch);
                for (Action action : resultSearch) {
                    logAction = LogUtils.addContent(logAction, "result aciton: " + action.getName());

                    action.setExpanded(true);
                    getParent(action, actionParents);
                }

                buildTree(root, rootNode, new ArrayList<>(actionParents), 1);
                rootNode.setExpanded(true);
                logAction = LogUtils.addContent(logAction, "Result: Success");
            }
        } catch (Exception e) {
            logAction = LogUtils.addContent(logAction, "Result: fail:" + e.getMessage());
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void getParent(Action action, Set<Action> actionParents) {
        if (action.getAction() != null) {
            action.setExpanded(true);
            actionParents.add(action.getAction());
            getParent(action.getAction(), actionParents);
        } else {
            return;
        }
    }

    public void prepareShowCmdInAction() {
        selectedAction = new Action();
        try {
            logAction = LogUtils.addContent("", "Show cmd in action");
            selectedAction = (Action) selectedNode.getData();
            selectedAction = actionServiceImpl.findById(selectedAction.getActionId());
            logAction = LogUtils.addContent(logAction, "Action id: " + selectedAction.getActionId());
            logAction = LogUtils.addContent(logAction, "Action name: " + selectedAction.getName());
            if (selectedAction.getActionDetails() != null
                    && !selectedAction.getActionDetails().isEmpty()) {
                RequestContext.getCurrentInstance().execute("PF('dlgShowCmdsInAction').show()");
            } else {
                MessageUtil.setErrorMessageFromRes("datatable.empty");
            }
            logAction = LogUtils.addContent("", "Result: Success");
        } catch (Exception e) {
            logAction = LogUtils.addContent("", "Result: fail: " + e.getMessage());
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void preAddActionToTemplate() {

        if (selectedNode != null) {
            try {
                selectedAction = (Action) selectedNode.getData();
                selectedAction = actionServiceImpl.findById(selectedAction.getActionId());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

        }
    }

    /*
     * Tao cay danh muc action
     */
    public void createTree() {
        Action root;
        try {
            root = actionServiceImpl.findById(ROOT_TREE_ID);
            if (root != null) {
                rootNode = new DefaultTreeNode("action", "Root", null);

                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                orders.put("name", "ASC");

                List<Action> lstAllData = null;
                try {
                    lstAllData = actionServiceImpl.findList(null, orders);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                buildTree(root, rootNode, lstAllData, 1);
                rootNode.setExpanded(true);
            }
        } catch (SysException | AppException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /*
     * Ham tao cay theo nut
     */
    public TreeNode buildTree(Action treeObj, TreeNode parent, List<Action> lstAllData, int level) {
        TreeNode newNode = null;
        try {
            List<Action> childNode = getLstChid(lstAllData, treeObj.getActionId());

            if (childNode != null && childNode.size() > 0) {
                newNode = new DefaultTreeNode(PARENT_NODE, treeObj, parent);
                newNode.setRowKey(treeObj.getActionId() + "");
                if (level == 1) {
                    newNode.setExpanded(true);
                }
                for (Action tt : childNode) {
                    buildTree(tt, newNode, lstAllData, (level + 1));
                }
            } else if (level <= 3) {
                newNode = new DefaultTreeNode(PARENT_NODE, treeObj, parent);
                newNode.setRowKey(treeObj.getActionId() + "");
            } else {
                newNode = new DefaultTreeNode(CHILD_NODE, treeObj, parent);
                newNode.setRowKey(treeObj.getActionId() + "");
            }
            if (treeObj.isExpanded()) {
                newNode.setExpanded(true);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return newNode;
    }

    private List<Action> getLstChid(List<Action> lstAction, Long parentId) {
        List<Action> lstSubAction = new ArrayList<>();
        try {
            for (Iterator<Action> iterator = lstAction.iterator(); iterator.hasNext(); ) {
                Action ac = iterator.next();
                if ((ac.getAction() != null) && (ac.getAction().getActionId() == parentId)) {
                    lstSubAction.add(ac);
                    iterator.remove();
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        Collections.sort(lstSubAction, new Comparator<Action>() {

            @Override
            public int compare(Action o1, Action o2) {
                String preName1 = o1.getName().substring(0, Math.max(o1.getName().indexOf(" "), 0));
                String preName2 = o2.getName().substring(0, Math.max(o2.getName().indexOf(" "), 0));

                String name1 = o1.getName().substring(o1.getName().indexOf(" ") + 1);
                String name2 = o2.getName().substring(o2.getName().indexOf(" ") + 1);

                String[] tmpPre1s = preName1.split("[,.]", -1);
                String[] tmpPre2s = preName2.split("[,.]", -1);
                int r = comp(tmpPre1s, tmpPre2s);
                if (r == 0) {
                    return name1.trim().compareTo(name2.trim());
                }
                return r;
            }

            public int comp(String[] as, String[] bs) {
                int min = Math.min(as.length, bs.length);
                if (as.length < bs.length) {
                    return 1;
                }
                if (as.length > bs.length) {
                    return -1;
                }
                if (min == 0) {
                    return 0;
                }
                int i = Math.min(0, min - 1);
                if (as[i].equals(bs[i])) {
                    as = ArrayUtils.remove(as, i);
                    bs = ArrayUtils.remove(bs, i);
                    return comp(as, bs);
                }
                return as[i].compareTo(bs[i]);
            }
        });
        return lstSubAction;
    }

    public void preShowControllChidNode() {
        try {
//			System.out.println("vao prepare show context action: " + ((Action) selectedAction).getName());

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void prepareMoveAction() {
        if (selectedNode != null) {
            try {
                moveNode = selectedNode;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    public void moveAction() {
        logAction = LogUtils.addContent("", "Move action");
        if (selectedNodeForMove != null && moveNode != null) {
            try {
                if (getNodeLevel(selectedNodeForMove) > 4) {
                    MessageUtil.setErrorMessageFromRes("message.move.leave");
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("message.move.leave"));
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                    return;
                }
                Action moveAction = actionServiceImpl.findById(((Action) moveNode.getData()).getActionId());
                logAction = LogUtils.addContent(logAction, "action: " + moveAction.getName());
                Action parentAction = actionServiceImpl.findById(((Action) selectedNodeForMove.getData()).getActionId());
                if (parentAction != null && !parentAction.getActionId().equals(moveAction.getAction().getActionId())) {
                    moveAction.setAction(parentAction);

                    actionServiceImpl.saveOrUpdate(moveAction);
                }

                moveNode.getParent().getChildren().remove(moveNode);

                String label = (getNodeLevel(selectedNodeForMove) < 4 ? PARENT_NODE : CHILD_NODE);
                TreeNode node = new DefaultTreeNode(label, moveAction, selectedNodeForMove);
                selectedNodeForMove.getChildren().add(node);

                RequestContext.getCurrentInstance().execute("PF('dlgMoveAction').hide()");
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateOk"));
                moveNode = null;
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                MessageUtil.setErrorMessageFromRes("label.action.updateFail");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateFail"));
            }

        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
    }

    public void prepareCloneAction() {
        logAction = LogUtils.addContent("", "Prepare clone action");
        logger.info("vao prepare clone");
        if (selectedNode != null) {
            try {

                if (selectedNode.getParent() == null) {
                    logger.error("parent null cmnr !");
                }

                logger.info("chuan bi du lieu");
                isClone = true;
                isEdit = false;
                Action action = (Action) selectedNode.getData();
                action = actionServiceImpl.findById(action.getActionId());

                actionNameToClone = action.getName();
                logAction = LogUtils.addContent(logAction, "Action : " + action.getName());

                insertNode = new Action();
                insertNode.setName("");
                insertNode.setDescription("");
                insertNode.setActionId(null);
                insertNode.setActionDetails(action.getActionDetails());
                insertNode.setActionOfFlows(null);
                insertNode.setActions(null);
                insertNode.setAction(action.getAction());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
        }
    }

    public void prepareEdit() {
        isEdit = true;
        logAction = LogUtils.addContent("", "Prepare Edit action");
        if (selectedNode != null) {
            try {
                Action action = (Action) selectedNode.getData();
                logAction = LogUtils.addContent(logAction, "Action old : " + action.toString());
                action = actionServiceImpl.findById(action.getActionId());

                /*
                 * Kiem tra xem action neu da duoc tham chieu den template approve.
                 * Neu co tham chieu thi khong co cap nhat
                 */
                if (ActionUtil.isActionReferToTempApprove(action)) {
                    actionIsAproved = true;
                } else {
                    actionIsAproved = false;
                }

                insertNode = new Action();
                insertNode.setName(action.getName());
                insertNode.setDescription(action.getDescription());
                insertNode.setActionId(action.getActionId());
                insertNode.setActionDetails(action.getActionDetails());
                insertNode.setActionOfFlows(action.getActionOfFlows());
                insertNode.setActions(action.getActions());
                insertNode.setAction(action.getAction());

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            logger.error(">>>>>>>> ERROR selectedNode is null");
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
    }

    public void prepareDelAction() {
        logAction = LogUtils.addContent("", "Prepare delete action");
        boolean checkShowDlg = true;
        try {
            switch (selectedNode.getType()) {
                case PARENT_NODE:
                    if (selectedNode.getChildCount() > 0) {
                        checkShowDlg = false;
                        MessageUtil.setErrorMessageFromRes("label.err.action.exist.child");
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.action.exist.child"));
                    }
                    break;
                case CHILD_NODE:
                    Action actionDel = (Action) selectedNode.getData();
                    //19092017 Quytv7 sua check action khong duoc xoa neu dang thuoc mop dang phe duyet start
                    try {
                        actionDel = actionServiceImpl.findById(actionDel.getActionId());
                    }catch (Exception ex){
                        logger.error(ex.getMessage(),ex);
                    }
                    for (ActionOfFlow aofw : actionDel.getActionOfFlows()) {
                        if (aofw.getFlowTemplates().getStatus() != null
                                && aofw.getFlowTemplates().getStatus() == 9) {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil
                                    .getResourceBundleMessage("message.action.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
                            logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil
                                    .getResourceBundleMessage("message.action.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
                            LogUtils.writelog(new Date(), className, new Object() {
                            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
                            return;
                        }
                    }
                    //19092017 Quytv7 sua check action khong duoc xoa neu dang thuoc mop dang phe duyet end


                    actionDel = new ActionServiceImpl().findById(actionDel.getActionId());
                    logAction = LogUtils.addContent(logAction, "Action: " + actionDel.toString());
                    if ((actionDel.getActionDetails() != null && !actionDel.getActionDetails().isEmpty())
                            || actionDel.getActionOfFlows() != null && !actionDel.getActionOfFlows().isEmpty()) {
                        checkShowDlg = false;
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.error.action.have.detail"));
                        MessageUtil.setErrorMessageFromRes("label.error.action.have.detail");
                    }
                    break;

                default:
                    break;
            }
            logAction = LogUtils.addContent(logAction, "Result: Success");
            if (checkShowDlg) {
                RequestContext.getCurrentInstance().execute("PF('confDlgDelAction').show()");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    /**
     * Ham them node con moi cho node hien tai
     */
    public void insertChildNode() {
        logAction = LogUtils.addContent("", "Insert child node");
        try {
            if (insertNode.getName() == null || "".equals(insertNode.getName().trim())) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.error.no.input.value"));
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                return;
            } else if (insertNode.getName().trim().length() > MAX_LENGTH_ACTION_NAME) {
                MessageUtil.setErrorMessageFromRes("label.validate.length.action");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.validate.length.action"));
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                return;
            }

            if (insertNode.getDescription() == null || "".equals(insertNode.getDescription().trim())) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.error.no.input.value"));
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                return;
            } else if (insertNode.getDescription().trim().length() > MAX_LENGTH_ACTION_NAME) {
                MessageUtil.setErrorMessageFromRes("label.validate.length.action.desc");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.validate.length.action.desc"));
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                return;
            }

            insertNode.setName(insertNode.getName().replaceAll(" +", " ").trim());
            insertNode.setDescription(insertNode.getDescription().replaceAll(" +", " ").trim());

            if (!isClone) {
                if (selectedNode == null || selectedNode.getData() == null) {
                    MessageUtil.setErrorMessageFromRes("message.err.no.node.selected");
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("message.err.no.node.selected"));
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                    return;
                }
                if (isEdit) {
                    insertNode.setActionId(((Action) selectedNode.getData()).getActionId());
                } else {
                    insertNode.setAction(actionServiceImpl.findById(((Action) selectedNode.getData()).getActionId()));
                }
                actionServiceImpl.saveOrUpdate(insertNode);
            } else {

                /*
                 * Ham clone du lieu cho action
                 */
                List<ActionDetail> actionsDetail = insertNode.getActionDetails();
                insertNode.setActionDetails(null);
                // luu action clone
                Long actionId = actionServiceImpl.save(insertNode);
                insertNode = actionServiceImpl.findById(actionId);

                // Tao moi cac action detail di theo action clone
                if (actionsDetail != null && !actionsDetail.isEmpty()) {
                    for (int i = 0; i < actionsDetail.size(); i++) {

                        List<ActionCommand> lstActionCommand = actionsDetail.get(i).getActionCommands();
                        actionsDetail.get(i).setDetailId(null);
                        actionsDetail.get(i).setAction(insertNode);
                        actionsDetail.get(i).setActionCommands(null);

                        Long actionDetailId = actionDetailServiceImpl.save(actionsDetail.get(i));
                        ActionDetail actionDetail = actionsDetail.get(i);
                        actionDetail.setDetailId(actionDetailId);

                        if (lstActionCommand != null) {
                            for (int j = 0; j < lstActionCommand.size(); j++) {
                                lstActionCommand.get(j).setActionCommandId(null);
                                lstActionCommand.get(j).setActionDetail(actionDetail);
                            }
                            new ActionCommandServiceImpl().saveOrUpdate(lstActionCommand);
                        }
                    }

                }

            }

            // them moi node con vao node hien tai
            if (!isEdit && !isClone) {
                String label = (getNodeLevel(selectedNode) < 4 ? PARENT_NODE : CHILD_NODE);
                TreeNode node = new DefaultTreeNode(label, insertNode, selectedNode);
                selectedNode.getChildren().add(node);

            } else {

                // hanhnv68 20161017
                // Cap nhat lai trang thai cua cac template co action nay
                if (!isClone) {
                    updateTemplateReference(insertNode);
                }
                // end hanhnv68 20161017

                List<TreeNode> lstChildOfCurNode = selectedNode.getChildren();
                TreeNode parent = selectedNode.getParent();
                if (parent == null || parent.getChildren() == null) {
                    logger.info("Error parent null");
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                    return;
                }

                List<TreeNode> lstChild = parent.getChildren();
                if (lstChild != null) {

                    String label = (getNodeLevel(selectedNode) <= 4 ? PARENT_NODE : CHILD_NODE);
                    if (!isClone) {
                        for (TreeNode node : lstChild) {
                            if (((Action) node.getData()).getActionId() == ((Action) selectedNode.getData()).getActionId()) {
                                lstChild.remove(node);
                                break;
                            }
                        } // end loop for
                    }

                    TreeNode newNode = new DefaultTreeNode(label, insertNode, parent);
                    newNode.setParent(parent);
                    newNode.getChildren().addAll(lstChildOfCurNode);

                    // add and sort list child node
                    parent.getChildren().add(0, newNode);
                    Collections.sort(parent.getChildren(), new Comparator<TreeNode>() {
                        @Override
                        public int compare(final TreeNode object1, final TreeNode object2) {
                            return ((Action) object1.getData()).getName().compareTo(((Action) object2.getData()).getName());
                        }
                    });

                }
            }

            RequestContext.getCurrentInstance().execute("PF('dlgAddAction').hide()");
            MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateOk"));
        } catch (Exception e) {

            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateFail"));
        } finally {
            clean();
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    private void updateTemplateReference(Action action) {
        if (action != null) {
            List<ActionOfFlow> lstActionFlow = action.getActionOfFlows();
            if (lstActionFlow != null && !lstActionFlow.isEmpty()) {
                List<FlowTemplates> lstFlowTemplate = new ArrayList<>();
                try {
                    for (ActionOfFlow actionFlow : lstActionFlow) {
                        FlowTemplates flowTemplate = actionFlow.getFlowTemplates();
                        if (flowTemplate != null) {
                            flowTemplate.setStatus(Config.APPROVAL_STATUS_DEFAULT);
                            lstFlowTemplate.add(flowTemplate);
                        }
                    }

                    new FlowTemplatesServiceImpl().saveOrUpdate(lstFlowTemplate);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

            }
        }
    }

    private Integer getNodeLevel(TreeNode node) {
        int level = 1;
        while (node != null) {
            node = node.getParent();
            if (node != null) {
                level++;
            }
        }
        return level;
    }

    /**
     * Ham xoa du lieu node con
     */
    public void deleteActionNode() {
        logAction = LogUtils.addContent("", "Delete action");
        if (selectedNode != null) {
            try {
                logAction = LogUtils.addContent(logAction, "Action: " + ((Action) selectedNode.getData()).toString());
                deleteNodeOfTree(selectedNode);
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.delelteOk"));
                MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            } catch (Exception e) {
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.deleteFail"));
                MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
                logger.error(e.getMessage(), e);
            } finally {
                clean();
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        }
    }

    public void deleteNodeOfTree(TreeNode selectedNode) {
        logAction = LogUtils.addContent("", "Delete node of tree");
        if (selectedNode != null) {
            try {
                selectedNode.getChildren().clear();
                selectedNode.getParent().getChildren().remove(selectedNode);

                List<TreeNode> lstNodeDelete = selectedNode.getChildren();
                if (lstNodeDelete != null && !lstNodeDelete.isEmpty()) {
                    for (TreeNode node : lstNodeDelete) {
                        try {
                            // goi de quy 
                            deleteNodeOfTree(node);
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    } // end loop for
                }

                actionServiceImpl.delete((Action) selectedNode.getData());
                logAction = LogUtils.addContent(logAction, "Action: " + ((Action) selectedNode.getData()).toString());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public boolean checkAddAction() {
        boolean check = false;
        logAction = LogUtils.addContent("", "Check add action");
        try {
            if (selectedNode != null) {
                logAction = LogUtils.addContent(logAction, "Action: " + ((Action) selectedNode.getData()).toString());
//				System.out.println((Action) selec);
                if (selectedNode.getChildCount() > 0) {
                    check = true;
                } else {
                    Action action = (Action) selectedNode.getData();
                    if (action.getActionOfFlows() == null || action.getActionOfFlows().isEmpty()) {
                        check = true;
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        logAction = LogUtils.addContent(logAction, "Check: " + check);

        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        return check;
    }

    public boolean checkIsLeft() {
        boolean check = false;
        try {
            if (selectedNode != null) {
                if (selectedNode.getChildCount() == 0) {
                    check = true;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return check;
    }

    public boolean checkAddActionToTemplate() {
        boolean check = false;
        try {

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return check;
    }

    public void clean() {
        isClone = false;
        isEdit = false;
        selectedNode = null;
        insertNode = new Action();
        actionIsAproved = false;

        actionNameToClone = "";
        selectedAction = new Action();
        selectedActionCommand = new ActionCommand();
        selectedActionDetail = new ActionDetail();
        selectedVersion = new Version();
        selectedCmdDetail = new CommandDetail();
        selectedCmdTelnetParser = new CommandTelnetParser();
        selectedNodeType = new NodeType();
        selectedVendor = new Vendor();
    }

    public String getNameActionSelected() {
        if (selectedNode != null) {
            return ((Action) selectedNode.getData()).getName();
        }
        return "";
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public ActionServiceImpl getActionServiceImpl() {
        return actionServiceImpl;
    }

    public void setActionServiceImpl(ActionServiceImpl actionServiceImpl) {
        this.actionServiceImpl = actionServiceImpl;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public Action getInsertNode() {
        return insertNode;
    }

    public void setInsertNode(Action insertNode) {
        this.insertNode = insertNode;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public List<Action> autoCompleAction(String actionName) {
        List<Action> lstAction = new ArrayList<>();
        try {
            Map<String, Object> filters = new HashMap<>();
            if (actionName != null) {
                filters.put("action.name", actionName);
            }
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("action.name", "ASC");
            lstAction = actionServiceImpl.findList(filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstAction;
    }

    public List<Vendor> autoCompleVendor(String actionName) {
        List<Vendor> lstAction = new ArrayList<>();
        try {
            //lstAction = vendorServiceImpl.findList();
            Map<String, Object> filters = new HashMap<>();
            if (actionName != null) {
                filters.put("vendorName", actionName);
            }
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("vendorName", "ASC");
            lstAction = vendorServiceImpl.findList(filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstAction;
    }

    public void addActionDetail() {
        logAction = LogUtils.addContent("", "Add action detail");
        if (selectedVendor != null
                && selectedNodeType != null
                && selectedAction != null
                && selectedVersion != null) {
            try {
                // kiem tra xem action detail them moi da ton tai hay chua
                if (!ActionUtil.checkExistActionDetail(selectedVendor.getVendorId(),
                        selectedNodeType.getTypeId(), selectedVersion.getVersionId(),
                        selectedAction.getActionId())) {

                    if (!isEditActionDetail) {
                        ActionDetail newAcDetail = new ActionDetail();
                        newAcDetail.setAction(selectedAction);
                        newAcDetail.setNodeType(selectedNodeType);
                        newAcDetail.setVendor(selectedVendor);
                        newAcDetail.setUserName(userName);
                        newAcDetail.setActionCommands(new ArrayList<ActionCommand>(0));
                        newAcDetail.setIsActive(1l);
                        newAcDetail.setVersion(selectedVersion);

                        new ActionDetailServiceImpl().saveOrUpdate(newAcDetail);
                        isEditActionDetail = false;
                        selectedVendor = null;
                        selectedNodeType = null;
                        selectedVersion = null;

                    } else {
                        selectedActionDetail.setVendor(selectedVendor);
                        selectedActionDetail.setNodeType(selectedNodeType);
                        selectedActionDetail.setIsActive(stationDetailStatus);
                        new ActionDetailServiceImpl().saveOrUpdate(selectedActionDetail);
                    }

                    selectedAction = new ActionServiceImpl().findById(selectedAction.getActionId());

                    // hanhnv68 20161017
                    // Cap nhat lai trang thai cac template co tham chieu den action
                    updateTemplateReference(selectedAction);
                    logAction = LogUtils.addContent(logAction, "Action: " + selectedAction.toString());
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateOk"));
                    // end hanhnv68 20161017
                    MessageUtil.setInfoMessageFromRes("label.action.updateOk");

                } else {
                    // thong bao loi ban ghi da ton tai
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.error.exist"));
                    MessageUtil.setErrorMessageFromRes("label.error.exist");
                }
            } catch (Exception e) {

                MessageUtil.setErrorMessageFromRes("label.action.updateFail");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateFail"));
                logger.error(e.getMessage(), e);
            }

        } else {
            // thong bao loi chua nhap day du thong tin
            MessageUtil.setErrorMessageFromRes("label.error.notFillAllData");
            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.error.notFillAllData"));
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public List<NodeType> autoCompleNodeType(String actionName) {
        List<NodeType> lstAction = new ArrayList<>();
        try {
            Map<String, Object> filters = new HashMap<>();
            if (actionName != null) {
                filters.put("typeName", actionName);
            }
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("typeName", "ASC");
            lstAction = nodeTypeServiceImpl.findList(filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstAction;
    }

    public List<Version> autoCompleteVersion(String version) {
        List<Version> lstVertion = new ArrayList<>();
        try {
            Map<String, Object> filters = new HashMap<>();
            if (version != null && !version.trim().isEmpty()) {
                filters.put("versionName", version);
            }
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("versionName", "ASC");
            lstVertion = new VersionServiceImpl().findList(filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstVertion;
    }

    public void onSelectActionDetail(SelectEvent event) {
        logAction = LogUtils.addContent("", "Select action detail");
        try {
            selectedActionDetail = (ActionDetail) event.getObject();
            lstActionCommand = new LinkedList<>();
            lstActionCmdDel = new ArrayList<>();
            logAction = LogUtils.addContent(logAction, "Action detail: " + selectedActionDetail.toString());
            int size = selectedActionDetail.getActionCommands().size();
            for (int i = 0; i < size; i++) {
                ActionCommand actionCmd = selectedActionDetail.getActionCommands().get(i);
                logAction = LogUtils.addContent(logAction, "Action command: " + actionCmd.toString());
                actionCmd.setOrderRun(Long.valueOf(i));
                lstActionCommand.add(actionCmd);
            }

            if (lstActionCommand.size() > 0) {
                Collections.sort(lstActionCommand, new Comparator<ActionCommand>() {
                    @Override
                    public int compare(final ActionCommand object1, final ActionCommand object2) {
                        return object1.getOrderRun().compareTo(object2.getOrderRun());
                    }
                });
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void prepareEditActionDetail() {
        try {
            isEditActionDetail = true;
            selectedNodeType = selectedActionDetail.getNodeType();
            selectedVendor = selectedActionDetail.getVendor();
            selectedVersion = selectedActionDetail.getVersion();
            stationDetailStatus = selectedActionDetail.getIsActive();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void deleteActionDetail() {
        try {
            logAction = LogUtils.addContent("", "Delete action detail");
            if (selectedActionDetail != null) {
                if (selectedActionDetail.getActionCommands() != null && !selectedActionDetail.getActionCommands().isEmpty()) {
                    MessageUtil.setErrorMessageFromRes("label.err.del.action.have.cmd");
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.del.action.have.cmd"));
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                    return;
                }
                new ActionDetailServiceImpl().delete(selectedActionDetail);
                lstActionCommand = new LinkedList<>();
                selectedActionDetail = new ActionDetail();
                selectedAction = new ActionServiceImpl().findById(selectedAction.getActionId());

                // hanhnv68 20161017
                // Cap nhat lai trang thai cua cac template co action nay
                updateTemplateReference(selectedAction);
                // end hanhnv68 20161017

                MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.del.action.have.cmd"));

            } else {
                MessageUtil.setErrorMessageFromRes("message.choose.delete");
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("message.choose.delete"));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.deleteFail"));
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void closeDlgActionDetail() {
        rebuildTree();
        clean();
    }

    public void rebuildTree() {
        try {
            List<TreeNode> lstChildOfCurNode = selectedNode.getChildren();
            if (selectedNode.getParent() != null) {
                TreeNode parent = selectedNode.getParent();
                if (parent.getChildren() != null) {
                    List<TreeNode> lstChild = parent.getChildren();
                    if (lstChild != null) {
                        int count = 0;
                        for (TreeNode node : lstChild) {
                            count++;
                            if (((Action) node.getData()).getActionId() == ((Action) selectedNode.getData()).getActionId()) {
                                lstChild.remove(node);
                                break;
                            }
                        } // end loop for

                        TreeNode newNode = new DefaultTreeNode(CHILD_NODE, selectedAction, parent);
                        newNode.getChildren().addAll(lstChildOfCurNode);
                        parent.getChildren().add((count > 0 ? (count - 1) : count), newNode);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void onUnSelectActionDetail(SelectEvent event) {

    }

    public List<CommandDetail> autoCmdMethod(String cmd) {

        Map<String, Object> filters = new HashMap<>();
        if (cmd != null && !cmd.trim().isEmpty()) {
            filters.put("commandName", cmd);
        }
        filters.put("vendor.vendorId", selectedActionDetail.getVendor().getVendorId());
        filters.put("nodeType.typeId", selectedActionDetail.getNodeType().getTypeId());
        filters.put("version.versionId", selectedActionDetail.getVersion().getVersionId());
        List<CommandDetail> lstCmd = null;
        try {
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("commandTelnetParser.cmd", "ASC");

            lstCmd = new CommandDetailServiceImpl().findList(0, 20, filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return lstCmd;
    }

    public static void main(String[] args) {
        try {
            JSONObject re = new JSONObject("{\"id\":5,\"eNodeBName\":\"eAG00153\",\"node\":{\"nodeId\":150584,\"vendor\":{\"vendorId\":1,\"vendorName\":\"Huawei\",\"description\":\"HW\",\"nodes\":[],\"commandDetails\":[]},\"nodeType\":{\"typeId\":7,\"typeName\":\"SRT\",\"description\":\"SITE_ROUTER\",\"commandDetails\":[],\"nodes\":[]},\"nodeCode\":\"AGG0153SRT01\",\"nodeIp\":\"10.253.48.4\",\"actionOfFlows\":[],\"groupActions\":[],\"version\":{\"versionId\":3,\"versionName\":\"S5328\",\"nodes\":[],\"commandDetails\":[]},\"subnetwork\":\"10.253.48.0\",\"networkType\":\"TRUYEN TAI\",\"isLab\":0,\"provinceCode\":\"A076\",\"status\":1,\"nodeName\":\"AGG0153SRT01\",\"flag\":-1,\"index\":-1,\"lstInterface\":[],\"lstNodeNeighbour\":[],\"stationCode\":\"AGG0153\",\"departmentName\":\"Long Xuyên\"},\"stationCode\":\"AGG0153\",\"deptManage\":\"Long Xuyên - KV3\",\"interfacePort\":\"Gi0/0/11\",\"vlanService\":\"2636\",\"vlanOam\":\"2637\",\"ipServiceEnodeb\":\"10.176.160.33\",\"ipServiceGateway\":\"10.176.160.38\",\"ipOamEnodeb\":\"10.186.160.33\",\"ipOamGateway\":\"10.186.160.38\",\"module\":\"Vendor PN: null,\\r\\nVendor Name: null\",\"status\":1,\"ipService\":\"10.176.160.32\",\"ipOam\":\"10.186.160.32\",\"provinceCode\":\"AGG\",\"vlanControl\":\"3\",\"vrrpId\":\"21\",\"vsiIdService\":\"2149534\",\"vsiIdOam\":\"2149574\",\"vsiNameService\":\"4G_Ring2_Service\",\"vsiNameOam\":\"4G_Ring2_OAM\",\"veIdService\":\"2668\",\"veIdOam\":\"2688\",\"ipVirtualEthernet\":\"100.66.0.212\",\"lastUpdate\":\"Nov 28, 2016 12:48:47 PM\",\"isOldIpVE\":false,\"isNormalStation\":true,\"isVibaStation\":false,\"isSdhStation\":false,\"statusLabel\":\"view.label.enodeb.status1\"}");

            String content = "";
            content += "Mã eNodeB: " + (re.getString("eNodeBName") == null ? "" : re.getString("eNodeBName"));

            if (re.has("node")) {
                JSONObject node = re.getJSONObject("node");

                content += "\nSRT: " + (node.getString("nodeCode") == null ? "" : node.getString("nodeCode"));
            }

            content += "\nMã nhà trạm: " + (re.has("stationCode") ? re.getString("stationCode") : "");
            content += "\nCổng giao tiếp: " + (re.has("interfacePort") ? re.getString("interfacePort") : "");
            content += "\nIP Service eNodeB: " + (re.has("ipServiceEnodeb") ? re.getString("ipServiceEnodeb") : "");
            content += "\nIP Service gateway: " + (re.has("ipServiceGateway") ? re.getString("ipServiceGateway") : "");
            content += "\nIP OAM eNodeB: " + (re.has("ipOamEnodeb") ? re.getString("ipOamEnodeb") : "");
            content += "\nIP OAM gateway: " + (re.has("ipOamGateway") ? re.getString("ipOamGateway") : "");
            content += "\nTrạng thái: " + (re.has("state") ? re.getString("state") : "");
            content += "\nThu: " + (re.has("rx") ? re.getString("rx") : "");
            content += "\nPhát: " + (re.has("tx") ? re.getString("tx") : "");
            content += "\nCRC: " + (re.has("crc") ? re.getString("crc") : "");
            content += "\nNegotiation: " + (re.has("negotiation") ? re.getString("negotiation") : "");
            content += "\nTốc độ: " + (re.has("speed") ? re.getString("speed") : "");
            content += "\nDuplex: " + (re.has("duplex") ? re.getString("duplex") : "");
            content += "\nModule: " + (re.has("module") ? re.getString("module") : "");
            content += "\nMAC: " + (re.has("mac") ? re.getString("mac") : "");
            content += "\nGIANT: " + (re.has("gian") ? re.getString("gian") : "");

            System.out.print(content);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
//		Map<String, Object> filters = new HashMap<>();
//		filters.put("commandTelnetParser.cmd", "e");
//		filters.put("vendor.vendorId", 1L);
//		filters.put("nodeType.typeId", 3L);
//		List<CommandDetail> lstCmd = null;
//		try {
////			lstCmd = new CommandDetailServiceImpl().findList(0, 20, filters, null);
////			System.out.println(lstCmd.size());
//			System.out.println(MessageUtil.getResourceBundleMessage("label.scheduleImpact"));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
    }

    public void prepareAddActionDetail() {
        logAction = LogUtils.addContent("", "Prepare add action detail");
        selectedAction = new Action();
        lstActionCommand = new LinkedList<>();
        lstActionDetail = new ArrayList<>();

        if (selectedNode != null) {
            selectedAction = ((Action) selectedNode.getData());
            try {
                selectedAction = actionServiceImpl.findById(selectedAction.getActionId());
            }catch (Exception ex){
                logger.error(ex.getMessage(),ex);
            }
            logAction = LogUtils.addContent(logAction, "Action: " + selectedAction.toString());
            //19092017 Quytv7 sua check lenh vao action khong duoc sua neu dang thuoc mop dang phe duyet start
            for (ActionOfFlow aofw : selectedAction.getActionOfFlows()) {
                if (aofw.getFlowTemplates().getStatus() != null
                        && aofw.getFlowTemplates().getStatus() == 9) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil
                            .getResourceBundleMessage("message.action.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil
                            .getResourceBundleMessage("message.action.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
                    return;
                }
            }
            //19092017 Quytv7 sua check lenh vao action khong duoc sua neu dang thuoc mop dang phe duyet end

            try {
                selectedAction = actionServiceImpl.findById(selectedAction.getActionId());
                if (ActionUtil.isActionReferToTempApprove(selectedAction)) {
                    actionIsAproved = true;
                } else {
                    actionIsAproved = false;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
            logger.info("selected action: " + selectedAction.getName());
        } else {
            selectedAction = new Action();
            selectedAction.setActionDetails(new ArrayList<ActionDetail>());
        }
        RequestContext.getCurrentInstance().execute("PF('dlgActionDetail').show()");
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void addActionCommand() {
        logAction = LogUtils.addContent("", "Add action command");
        try {
            if (selectedCmdDetail != null) {
                ActionCommand actionCmd = new ActionCommand();
                actionCmd.setActionDetail(selectedActionDetail);
                actionCmd.setCreateTime(new Date());
                actionCmd.setUserName(userName);
                actionCmd.setType(1L);
                actionCmd.setIsActive(1L);
                actionCmd.setCommandDetail(selectedCmdDetail);
                actionCmd.setOrderRun(Long.valueOf(lstActionCommand.size()));

                lstActionCommand.add(actionCmd);
                logAction = LogUtils.addContent(logAction, "Action command: " + actionCmd.toString());
            }
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);

        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void deleteCmdDetail(ActionCommand cmdDel, int indexRow) {
        logAction = LogUtils.addContent("", "Delete command detail");
        try {
            boolean isDelete = true;
            if (cmdDel.getActionCommandId() != null) {
                // Kiem tra xem cau lenh da duoc sinh MOP hay chua
//				if (cmdDel.getActionDetail().getAction().getActionOfFlows() != null
//						&& !cmdDel.getActionDetail().getAction().getActionOfFlows().isEmpty()) {
//					List<ActionOfFlow> lstActionFlow = cmdDel.getActionDetail().getAction().getActionOfFlows();
//					//huynx6 edited Nov 29, 2016
//					List<Long> tmps = new ArrayList<Long>();
//					for (ActionOfFlow actionFLow : lstActionFlow) {
//						tmps.add(actionFLow.getStepNum());
//					}
//					if(tmps.size()>0){
//						Map<String, Collection<?>> map = new HashMap<String, Collection<?>>();
//						map.put("stepNums", tmps);
//						List<NodeRunGroupAction> tmps2 = new NodeRunGroupActionServiceImpl().findListWithIn("from NodeRunGroupAction where id.stepNum in (:stepNums)", -1, -1, map);
//						if (tmps2.size()>0) {
//							MessageUtil.setErrorMessageFromRes("label.cmd.created.mop");
//							return;
//						}
//					}
//				}

                Map<String, Object> filters = new HashMap<>();
                filters.put("actionCommandByActionCommandOutputId.actionCommandId", cmdDel.getActionCommandId());
                if (new ParamInOutServiceImpl().count2(filters) > 0) {
                    MessageUtil.setErrorMessageFromRes("message.choose.using");
                    isDelete = false;
                } else {
                    //20170725_hienhv4_fix loi xoa lenh_start
                    filters.clear();
                    filters.put("actionCommandByActionCommandInputId.actionCommandId", cmdDel.getActionCommandId());
                    if (new ParamInOutServiceImpl().count2(filters) > 0) {
                        MessageUtil.setErrorMessageFromRes("message.choose.using");
                        isDelete = false;
                    } else {
                        lstActionCmdDel.add(cmdDel);
                    }
                    //20170725_hienhv4_fix loi xoa lenh_end
                }
                logAction = LogUtils.addContent(logAction, "cmdDel: " + cmdDel.toString());
            }

            if (isDelete) {
                lstActionCommand.remove(indexRow);
                for (int i = 0; i < lstActionCommand.size(); i++) {
                    lstActionCommand.get(i).setOrderRun(Long.valueOf(i));
                }
            }
            logAction = LogUtils.addContent(logAction, "Result: " + isDelete);
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void noAction() {

    }

    public void submitActionCommandData() {
        Session session = null;
        logAction = LogUtils.addContent("", "Submit action command data");
        try {
            if (selectedActionDetail.getDetailId() == null) {
                return;
            }

            List<ActionCommand> lstActionCmdId = new ArrayList<>();
            List<ActionCommand> lstActionCmdNoId = new ArrayList<>();
            for (ActionCommand actionCmd : lstActionCommand) {
                if (actionCmd.getActionCommandId() == null) {
                    lstActionCmdNoId.add(actionCmd);
                } else {
                    lstActionCmdId.add(actionCmd);
                }
                logAction = LogUtils.addContent(logAction, "actionCmd: " + actionCmd.toString());
            }

            Object[] objs = new ActionCommandServiceImpl().openTransaction();

            session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];

            if (lstActionCmdDel != null && !lstActionCmdDel.isEmpty()) {
                new ActionCommandServiceImpl().delete(lstActionCmdDel, session, tx, false);
                //Quytv7 Delete in table param in_out
                //Set<Long> listIdActionCommand = new HashSet<>();
                //for(ActionCommand acm :lstActionCmdDel){
                //   listIdActionCommand.add(acm.getActionCommandId());
                //}
                //Map<String, Object> filter = new HashMap<>();
                // filter.put("actionCommandByActionCommandInputId.actionCommandId", listIdActionCommand);
                // List<ParamInOut> lstParamInoutDel = new ParamInOutServiceImpl().findList(filter);
                // new ParamInOutServiceImpl().delete(lstParamInoutDel);
            }

            new ActionCommandServiceImpl().saveOrUpdate(lstActionCmdNoId, session, tx, false);
            new ActionCommandServiceImpl().saveOrUpdate(lstActionCmdId, session, tx, false);

            tx.commit();

            selectedActionDetail = new ActionDetailServiceImpl().findById(selectedActionDetail.getDetailId());

            // hanhnv68 20161017
            // Cap nhat lai trang thai cua cac template co action nay
            updateTemplateReference(selectedAction);
            // end hanhnv68 20161017

            MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateOk"));
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.action.updateFail"));
            logger.error(e.getMessage(), e);
        } finally {
            lstActionCmdDel = new ArrayList<>();
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e2) {
                    logger.error(e2.getMessage(), e2);
                }
            }
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void reorderDataTable(ReorderEvent event) {
        // luu lai danh sach action command moi
        for (int i = 0; i < lstActionCommand.size(); i++) {
            lstActionCommand.get(i).setOrderRun(Long.valueOf(i));
        }
    }

    public void closeActionDetailDlg() {
        selectedAction = new Action();
        selectedAction.setActionDetails(new ArrayList<ActionDetail>());
    }

    public ActionDetailServiceImpl getActionDetailServiceImpl() {
        return actionDetailServiceImpl;
    }

    public void setActionDetailServiceImpl(ActionDetailServiceImpl actionDetailServiceImpl) {
        this.actionDetailServiceImpl = actionDetailServiceImpl;
    }

    public Action getSelectedAction() {
        return selectedAction;
    }

    public void setSelectedAction(Action selectedAction) {
        this.selectedAction = selectedAction;
    }

    public VendorServiceImpl getVendorServiceImpl() {
        return vendorServiceImpl;
    }

    public void setVendorServiceImpl(VendorServiceImpl vendorServiceImpl) {
        this.vendorServiceImpl = vendorServiceImpl;
    }

    public NodeTypeServiceImpl getNodeTypeServiceImpl() {
        return nodeTypeServiceImpl;
    }

    public void setNodeTypeServiceImpl(NodeTypeServiceImpl nodeTypeServiceImpl) {
        this.nodeTypeServiceImpl = nodeTypeServiceImpl;
    }

    public Vendor getSelectedVendor() {
        return selectedVendor;
    }

    public void setSelectedVendor(Vendor selectedVendor) {
        this.selectedVendor = selectedVendor;
    }

    public NodeType getSelectedNodeType() {
        return selectedNodeType;
    }

    public void setSelectedNodeType(NodeType selectedNodeType) {
        this.selectedNodeType = selectedNodeType;
    }

    public ActionDetail getActionDetail() {
        return actionDetail;
    }

    public void setActionDetail(ActionDetail actionDetail) {
        this.actionDetail = actionDetail;
    }

    public CommandTelnetParserServiceImpl getCommandTelnetParserServiceImpl() {
        return commandTelnetParserServiceImpl;
    }

    public void setCommandTelnetParserServiceImpl(
            CommandTelnetParserServiceImpl commandTelnetParserServiceImpl) {
        this.commandTelnetParserServiceImpl = commandTelnetParserServiceImpl;
    }

    public List<ActionDetail> getLstActionDetail() {
        return lstActionDetail;
    }

    public void setLstActionDetail(List<ActionDetail> lstActionDetail) {
        this.lstActionDetail = lstActionDetail;
    }

    public ActionDetail getSelectedActionDetail() {
        return selectedActionDetail;
    }

    public void setSelectedActionDetail(ActionDetail selectedActionDetail) {
        this.selectedActionDetail = selectedActionDetail;
    }

    public CommandDetail getSelectedCmdDetail() {
        return selectedCmdDetail;
    }

    public void setSelectedCmdDetail(CommandDetail selectedCmdDetail) {
        this.selectedCmdDetail = selectedCmdDetail;
    }

    public CommandTelnetParser getSelectedCmdTelnetParser() {
        return selectedCmdTelnetParser;
    }

    public void setSelectedCmdTelnetParser(CommandTelnetParser selectedCmdTelnetParser) {
        this.selectedCmdTelnetParser = selectedCmdTelnetParser;
    }

    public List<ActionCommand> getLstActionCommand() {
        return lstActionCommand;
    }

    public void setLstActionCommand(List<ActionCommand> lstActionCommand) {
        this.lstActionCommand = lstActionCommand;
    }

    public Long getStationDetailStatus() {
        return stationDetailStatus;
    }

    public void setStationDetailStatus(Long stationDetailStatus) {
        this.stationDetailStatus = stationDetailStatus;
    }

    public boolean isEditActionDetail() {
        return isEditActionDetail;
    }

    public void setEditActionDetail(boolean isEditActionDetail) {
        this.isEditActionDetail = isEditActionDetail;
    }

    public ActionCommand getSelectedActionCommand() {
        return selectedActionCommand;
    }

    public void setSelectedActionCommand(ActionCommand selectedActionCommand) {
        this.selectedActionCommand = selectedActionCommand;
    }

    public List<ActionCommand> getLstActionCmdDel() {
        return lstActionCmdDel;
    }

    public void setLstActionCmdDel(List<ActionCommand> lstActionCmdDel) {
        this.lstActionCmdDel = lstActionCmdDel;
    }

    public Version getSelectedVersion() {
        return selectedVersion;
    }

    public void setSelectedVersion(Version selectedVersion) {
        this.selectedVersion = selectedVersion;
    }

    public String getKeyActionSearch() {
        return keyActionSearch;
    }

    public void setKeyActionSearch(String keyActionSearch) {
        this.keyActionSearch = keyActionSearch;
    }

    public boolean isClone() {
        return isClone;
    }

    public void setClone(boolean isClone) {
        this.isClone = isClone;
    }

    public String getActionNameToClone() {
        return actionNameToClone;
    }

    public void setActionNameToClone(String actionNameToClone) {
        this.actionNameToClone = actionNameToClone;
    }

    public boolean isActionIsAproved() {
        return actionIsAproved;
    }

    public void setActionIsAproved(boolean actionIsAproved) {
        this.actionIsAproved = actionIsAproved;
    }

    public TreeNode getSelectedNodeForMove() {
        return selectedNodeForMove;
    }

    public void setSelectedNodeForMove(TreeNode selectedNodeForMove) {
        this.selectedNodeForMove = selectedNodeForMove;
    }
}
