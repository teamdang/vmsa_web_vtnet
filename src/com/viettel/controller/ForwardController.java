/*
 * Created on Jun 7, 2013
 *
 * Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpServletRequest;

import com.viettel.exception.SysException;
import com.viettel.util.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Chức năng chính để forward các url tới phần phân quyền.
 *
 * @author Nguyen Hai Ha (hanh45@viettel.com.vn)
 * @since Jun 7, 2013
 * @version 1.0.0
 */
@RequestScoped
@ManagedBean(name = "forwardService")
public class ForwardController implements Serializable {

    private static final long serialVersionUID = 4870520554535423726L;
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    // Trang home.
    private static final String _HOME_PAGE = "/home";

    /**
     * Dieu huong den trang home page.
     *
     */
    private void homeForward() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        try {
            fc.getExternalContext().redirect(req.getContextPath() + _HOME_PAGE);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    /**
     * Redirect toi trang home.
     *
     * @param event
     * @throws IOException
     */
    public void doForward(final ComponentSystemEvent event) throws IOException {
        homeForward();
    }

    /**
     * Dieu huong den trang mac dinh cua user.
     *
     * @param event
     * @throws IOException
     */
    public void doRedirect(final ComponentSystemEvent event) throws IOException {
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
//            //20170510_HaNV15_Add_Start_Set default attribute country code is VNM to session
//            req.getSession().setAttribute(Constants.COUNTRY_CODE_CURRENT, Constants.VNM);
//            //20170510_HaNV15_Add_End
            // Lay gia tri menu default cua user dang nhap.
            String defaultUrl = SessionUtil.getMenuDefault();
            if ("".equals(defaultUrl)) {
                homeForward();
            } else {
                fc.getExternalContext().redirect(req.getContextPath() + defaultUrl);
            }
        } catch (SysException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
}
