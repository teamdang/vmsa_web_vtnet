/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.controller;

import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.*;
import com.viettel.object.AccountObj;
import com.viettel.object.ComboBoxObject;
import com.viettel.passprotector.PassProtector;
import com.viettel.persistence.CatCountryServiceImpl;
import com.viettel.persistence.MapUserCountryServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.NodeTypeServiceImpl;
import com.viettel.persistence.ProvinceServiceImpl;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.persistence.VersionServiceImpl;
import com.viettel.util.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.poi.ss.usermodel.*;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hienhv4
 */
@ViewScoped
@ManagedBean
public class NodeController {

    protected static final Logger logger = LoggerFactory.getLogger(NodeController.class);

    @ManagedProperty(value = "#{nodeService}")
    private NodeServiceImpl nodeService;
    @ManagedProperty(value = "#{vendorService}")
    private VendorServiceImpl vendorService;
    @ManagedProperty(value = "#{nodeTypeService}")
    private NodeTypeServiceImpl nodeTypeService;
    @ManagedProperty(value = "#{versionService}")
    private VersionServiceImpl versionService;
    //20170419_HaNV15_Add_Start
    @ManagedProperty(value = "#{catCountryService}")
    private CatCountryServiceImpl catCountryService;
    @ManagedProperty(value = "#{mapUserCountryService}")
    private MapUserCountryServiceImpl mapUserCountryService;
    private List<CatCountryBO> countrys;
    //20170419_HaNV15_Add_End

    private LazyDataModel<Node> lazyNode;
    private Node selectedNode;
    private boolean isEdit;
    private StreamedContent file;

    private List<Vendor> vendors;
    private List<NodeType> nodeTypes;
    private List<Version> versions;
    private List<Province> provinces;
    private StreamedContent resultImport;
    //Quytv7 ghi log action
    private String logAction = "";
    private String className = NodeController.class.getName();

    @PostConstruct
    public void onStart() {
        try {
            Map<String, String> orderVendor = new HashMap<>();
            orderVendor.put("vendorName", "asc");
            vendors = vendorService.findList(null, orderVendor);

            Map<String, String> orderNodeType = new HashMap<>();
            orderNodeType.put("typeName", "asc");
            nodeTypes = nodeTypeService.findList(null, orderNodeType);

            Map<String, String> orderVersion = new HashMap<>();
            orderVersion.put("versionName", "asc");
            versions = versionService.findList(null, orderVersion);

            Map<String, String> orderProv = new HashMap<>();
            orderProv.put("provinceName", "asc");
            provinces = (new ProvinceServiceImpl()).findList(null, orderProv);

            //20170419_HaNV15_Add_Start
            List<String> lstCountry = mapUserCountryService.getListCountryForUser();
            Map<String, Object> filterCountry = new HashMap<>();
            if (lstCountry != null && lstCountry.size() > 0) {
                filterCountry.put("countryCode-EXAC", lstCountry);
                countrys = catCountryService.findList(filterCountry);
            }
            Map<String, Object> filters = new HashMap<>();
//            if (lstCountry != null && lstCountry.size() > 0) {
//                filters.put("countryCode.countryCode-EXAC", lstCountry);
//            }
            //20170419_HaNV15_Add_End

            //20170419_HaNV15_Edit_Start
            lazyNode = new LazyDataModelBaseNew<>(nodeService, filters, null);
            //20170419_HaNV15_Edit_End
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void prepareEdit(Node node) {
        logAction = LogUtils.addContent("", "Prepare edit node");
        selectedNode = node;
        try {
            selectedNode.setAccount(PassProtector.decrypt(node.getAccount() == null ? "" : node.getAccount(), "ipchange"));
            selectedNode.setPassword(PassProtector.decrypt(node.getPassword() == null ? "" : node.getPassword(), "ipchange"));

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        if(node != null){
            logAction = LogUtils.addContent(logAction, "Node: " + node.toString());
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
        isEdit = true;
    }

    public void prepareDel(Node node) {
        logAction = LogUtils.addContent("", "Prepare delete node");
        if (node != null) {
            selectedNode = node;
            logAction = LogUtils.addContent(logAction, "Node: " + selectedNode.toString());
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);

    }

    public void delNode() {
        if (selectedNode != null) {
            logAction = LogUtils.addContent("", "Prepare delete node");
            try {
                nodeService.delete(selectedNode);
                selectedNode = new Node();
                logAction = LogUtils.addContent(logAction, "Node: " + selectedNode.toString());
                MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
                logAction = LogUtils.addContent("Result", "True");
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
                logAction = LogUtils.addContent("Result", "False");
                logger.error(e.getMessage(), e);
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
        }
    }

    public void saveNode() {
        logAction = LogUtils.addContent("", "Save node");
        try {
            //20170510_HaNV15_Add_Start
//            String countryCode = new SessionUtil().getCountryCodeCurrent();
//            CatCountryBO catCountryBO = new CatCountryBO();
//            if (countryCode != null) {
//                catCountryBO.setCountryCode(countryCode);
//            } else {
//                catCountryBO.setCountryCode(Constants.VNM);
//            }
//            selectedNode.setCountryCode(catCountryBO);
            //20170510_HaNV15_Add_End
            if (validateData()) {

                Node nodeSave = new Node();
                if (isEdit) {
                    nodeSave.setNodeId(selectedNode.getNodeId());
                }
                nodeSave.setNodeCode(selectedNode.getNodeCode());
                nodeSave.setNodeIp(selectedNode.getNodeIp());
                nodeSave.setVendor(selectedNode.getVendor());
                nodeSave.setVersion(selectedNode.getVersion());
                nodeSave.setNodeType(selectedNode.getNodeType());
                nodeSave.setPort(selectedNode.getPort());
                nodeSave.setPortSsh(selectedNode.getPortSsh());
                nodeSave.setOsType(selectedNode.getOsType() == null ? null : selectedNode.getOsType().trim());
                nodeSave.setJdbcUrl(selectedNode.getJdbcUrl() == null ? null : selectedNode.getJdbcUrl().trim());
                nodeSave.setSubnetwork(selectedNode.getSubnetwork() == null ? null : selectedNode.getSubnetwork().trim());
                nodeSave.setNetworkType(selectedNode.getNetworkType() == null ? null : selectedNode.getNetworkType().trim());
                nodeSave.setIsLab(selectedNode.getIsLab());
                nodeSave.setProvince(selectedNode.getProvince());
                nodeSave.setEffectIp(selectedNode.getEffectIp() == null ? null : selectedNode.getEffectIp().trim());
                //20170419_HaNV15_Add_Start
                nodeSave.setCountryCode(selectedNode.getCountryCode());
                //20170419_HaNV15_Add_End
                nodeSave.setFormRun(selectedNode.getFormRun() == null ? 0L : selectedNode.getFormRun());
                nodeSave.setAccount(PassProtector.encrypt(selectedNode.getAccount() == null ? "" : selectedNode.getAccount(), "ipchange"));
                nodeSave.setPassword(PassProtector.encrypt(selectedNode.getPassword() == null ? "" : selectedNode.getPassword(), "ipchange"));
                if (nodeSave.getEffectIp() == null || nodeSave.getEffectIp().trim().isEmpty()) {
                    nodeSave.setEffectIp(selectedNode.getNodeIp());
                }

                nodeService.saveOrUpdate(nodeSave);
                clear();
                logAction = LogUtils.addContent(logAction, "Node Save: " + nodeSave.toString());
                logAction = LogUtils.addContent("Result", "True");
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");

                RequestContext.getCurrentInstance().execute("PF('dlgNodeInfo').hide()");
            }

        } catch (Exception e) {
            logAction = LogUtils.addContent("Result", "False");
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
    }

    private boolean validateData() {
        boolean val = true;
        // Kiem tra xem cac truong da duoc nhap day du du lieu chua
        if (selectedNode.getNodeCode() == null
                || selectedNode.getNodeCode().trim().isEmpty()
                || selectedNode.getVendor() == null
                || selectedNode.getVersion() == null
                || selectedNode.getNodeType() == null
                || selectedNode.getNodeIp() == null
                || selectedNode.getNodeIp().trim().isEmpty() //                || !new SessionUtil().checkCountryCode(selectedNode.getCountryCode().getCountryCode())//20170510_HaNV15_Add
                ) {
            MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
            val = false;
        }

        return val;
    }

    public List<Node> autoCompleNode(String nodeCode) {
        List<Node> lstNode;
        Map<String, Object> filters = new HashMap<>();
        if (nodeCode != null) {
            filters.put("nodeCode", nodeCode);
        }
        try {
            lstNode = new NodeServiceImpl().findList(0, 100, filters);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            lstNode = new ArrayList<>();
        }
        return lstNode;
    }

    //20170907 Quyvt7 import account/pass node start
    public void onImport(FileUploadEvent event) {

        List<Node> result = new ArrayList<>();
//        boolean hasError = false;
        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(event.getFile().getInputstream());
            Sheet sheet = wb.getSheetAt(0);
            Row rowHeader = sheet.getRow(3);

            boolean check = checkHeader(rowHeader);
            List<Node> lstNodeResult = new ArrayList<>();
            if (check) {
                List<Node> lstNode = nodeService.findList();
                Map<String, AccountObj> mapNodeAccountPass = new HashMap<>();
                int rowNum = sheet.getLastRowNum();
                for (int i = 4; i <= rowNum; i++) {
                    AccountObj accountObj = new AccountObj();
                    String err = checkRow(sheet, i, accountObj);
                    if (err != null) {
                        //Truong hop row co du lieu
                        if ("".equals(err)) {
                            mapNodeAccountPass.put(accountObj.getIp(), accountObj);
                        }
                    }
                }
                //Set lai account pass cho cac node mang
                if (mapNodeAccountPass.size() > 0) {
                    for (Node node : lstNode) {
                        if (mapNodeAccountPass.containsKey(node.getEffectIp())) {
                            node.setAccount(PassProtector.encrypt(mapNodeAccountPass.get(node.getEffectIp()).getAccount() == null ? "" : mapNodeAccountPass.get(node.getEffectIp()).getAccount(), "ipchange"));
                            node.setPassword(PassProtector.encrypt(mapNodeAccountPass.get(node.getEffectIp()).getPassword() == null ? "" : mapNodeAccountPass.get(node.getEffectIp()).getPassword(), "ipchange"));
                            lstNodeResult.add(node);
                            result.add(node);
                        }
                    }
                }
                if (lstNodeResult.size() > 0) {
                    nodeService.saveOrUpdate(lstNodeResult);
                }
            } else {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.invalid.header"));
                return;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("title.import.node.account.pass")));
            return;
        } finally {
            if (wb != null) {
                try {
                    wb.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        try {
            String[] header;
            String[] align;
            header = new String[]{"nodeCode=key.nodeCode", "effectIp=label.effectip", "account=label.account"};
            align = new String[]{
                "LEFT", "LEFT", "LEFT"};
            List<AbstractMap.SimpleEntry<String, String>> headerAlign = CommonExport.buildExportHeader(header, align);

            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                    .getExternalContext().getContext();
            String fileTemplate = ctx.getRealPath("/")
                    + File.separator + "templates" + File.separator + "import" + File.separator + "Template_export_acount_pass_node_result.xlsx";

            for (Node node : result) {
                if (node.getAccount() != null && "".equals(node.getAccount())) {
                    try {
                        node.setAccount(PassProtector.decrypt(node.getAccount(), "ipchange"));
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                }
            }
            File fileExport = CommonExport.exportFile(result, headerAlign, "", fileTemplate,
                    "CommandImportResult", 4, "", 2,
                    MessageUtil.getResourceBundleMessage("title.nodeImportResult"));

            resultImport = new DefaultStreamedContent(new FileInputStream(fileExport), ".xlsx", fileExport.getName());

            RequestContext.getCurrentInstance().execute("PF('importDialog').hide();");
            RequestContext.getCurrentInstance().execute("PF('resultImportDialog').show();");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("title.import.node.account.pass")));
        }
    }

    public boolean checkHeader(Row rowHeader) {
        boolean check = true;
        int i = 0;
        String STT = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String ip = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String account = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String pass = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
//        i++;
        if (!STT.equals(MessageUtil.getResourceBundleMessage("datatable.header.stt").toLowerCase())
                || !ip.equals(MessageUtil.getResourceBundleMessage("label.effectip").toLowerCase())
                || !account.equals(MessageUtil.getResourceBundleMessage("label.account").toLowerCase())
                || !pass.equals(MessageUtil.getResourceBundleMessage("label.pass").toLowerCase())) {
            check = false;
        }
        return check;
    }

    public String checkRow(Sheet sheet, int i, AccountObj accountObj) {
        String err = "";
        Row row = sheet.getRow(i);
        if (row != null) {
            int j = 0;
            String STT = getCellValue(row.getCell(j));
            j++;
            String ip = getCellValue(row.getCell(j));
            j++;
            String account = getCellValue(row.getCell(j));
            j++;
            String pass = getCellValue(row.getCell(j));
//            j++;

            if (!isNullOrEmpty(STT)
                    || !isNullOrEmpty(ip)
                    || !isNullOrEmpty(account)
                    || !isNullOrEmpty(pass)) {

                if (isNullOrEmpty(ip)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.effectip")) + "\n";
                } else {
                    accountObj.setIp(ip);
                }
                if (isNullOrEmpty(account)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.account")) + "\n";
                } else {
                    accountObj.setAccount(account);
                }
                if (isNullOrEmpty(pass)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.pass")) + "\n";
                } else {
                    accountObj.setPassword(pass);
                }

                return err;
            } else {
                return null;
            }
        }
        return null;
    }

    public StreamedContent onDownloadTemplate() {
        try {
            InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/templates/import/Template_import_acount_pass_node.xlsx");
            return new DefaultStreamedContent(stream, "application/xls", "Template_import_acount_pass_node.xlsx");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
//                    MessageUtil.getResourceBundleMessage("button.import")));
        }
        return null;
    }

    public StreamedContent getImportResult() {
        return resultImport;
    }

    private boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    private String getCellValue(Cell cell) {
        String result = "";
        if (cell == null) {
            return result;
        }
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                result = cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                result = cell.getStringCellValue();
                break;

            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    result = cell.getDateCellValue().toString();
                } else {
                    Double number = cell.getNumericCellValue();
                    if (Math.round(number) == number) {
                        result = Long.toString(Math.round(number));
                    } else {
                        result = Double.toString(cell.getNumericCellValue());
                    }
                }
                break;

            case Cell.CELL_TYPE_BLANK:
                result = "";
                break;

            case Cell.CELL_TYPE_BOOLEAN:
                result = Boolean.toString(cell.getBooleanCellValue());
                break;
        }
        return result.trim();
    }
    //20170907 Quyvt7 import account/pass node end

    public void clear() {
        selectedNode = new Node();
        isEdit = false;
    }

    public boolean isChecked() {
        if (selectedNode != null) {
            return selectedNode.getIsLab() != null && selectedNode.getIsLab() > 0;
        }
        return false;
    }

    public void setChecked(boolean checked) {
        if (selectedNode != null) {
            selectedNode.setIsLab(checked ? 1 : 0);
        }
    }

    public NodeServiceImpl getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeServiceImpl nodeService) {
        this.nodeService = nodeService;
    }

    public LazyDataModel<Node> getLazyNode() {
        return lazyNode;
    }

    public void setLazyNode(LazyDataModel<Node> lazyNode) {
        this.lazyNode = lazyNode;
    }

    public Node getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(Node selectedNode) {
        this.selectedNode = selectedNode;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public List<NodeType> getNodeTypes() {
        return nodeTypes;
    }

    public void setNodeTypes(List<NodeType> nodeTypes) {
        this.nodeTypes = nodeTypes;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    public VendorServiceImpl getVendorService() {
        return vendorService;
    }

    public void setVendorService(VendorServiceImpl vendorService) {
        this.vendorService = vendorService;
    }

    public NodeTypeServiceImpl getNodeTypeService() {
        return nodeTypeService;
    }

    public void setNodeTypeService(NodeTypeServiceImpl nodeTypeService) {
        this.nodeTypeService = nodeTypeService;
    }

    public VersionServiceImpl getVersionService() {
        return versionService;
    }

    public void setVersionService(VersionServiceImpl versionService) {
        this.versionService = versionService;
    }

    public List<Province> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<Province> provinces) {
        this.provinces = provinces;
    }

    public CatCountryServiceImpl getCatCountryService() {
        return catCountryService;
    }

    public void setCatCountryService(CatCountryServiceImpl catCountryService) {
        this.catCountryService = catCountryService;
    }

    public MapUserCountryServiceImpl getMapUserCountryService() {
        return mapUserCountryService;
    }

    public void setMapUserCountryService(MapUserCountryServiceImpl mapUserCountryService) {
        this.mapUserCountryService = mapUserCountryService;
    }

    public List<CatCountryBO> getCountrys() {
        return countrys;
    }

    public void setCountrys(List<CatCountryBO> countrys) {
        this.countrys = countrys;
    }

    public StreamedContent getResultImport() {
        return resultImport;
    }

    public void setResultImport(StreamedContent resultImport) {
        this.resultImport = resultImport;
    }
}
