
package com.viettel.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.google.common.base.Throwables;
import com.viettel.util.ConnectDbService;
import com.viettel.util.HibernateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @version 1.0
 */
@ViewScoped
@ManagedBean()
public class ConsoleController implements Serializable{

    private String urlDb;
    private String usernameDb;
    private String passwordDb;
    private String fileSql;
    private String usernameLogin;
    private String passwordLogin;
    private Object result;
    private List<List<Object>> data;
    private List<String> columns;


    protected static final Logger logger = LoggerFactory.getLogger(ConsoleController.class);

	@PostConstruct
	public void onStart(){


    }



    public void execute(){

	    if (!"vmsa".equals(usernameLogin) || !"vmsa!@#123".equals(passwordLogin))
	        return;
        ConnectDbService eniqService =null;
        try {
            data = new LinkedList<>();
            columns=new LinkedList<>();
            if (urlDb==null || urlDb.isEmpty()){
                Properties properties = HibernateUtil.getProperties();
                String passwordDb = properties.getProperty("hibernate.connection.password");
                String urlDb = properties.getProperty("hibernate.connection.url");
                String usernameDb= properties.getProperty("hibernate.connection.username");
                eniqService =new ConnectDbService(urlDb, usernameDb, passwordDb);
            }else{
                eniqService =new ConnectDbService(urlDb, usernameDb, passwordDb);
            }
            eniqService.connectionDb();
            result = eniqService.getResults(fileSql,data,columns);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            result += Throwables.getStackTraceAsString(e);
        }finally{
            if (eniqService != null)
                try {
                    Connection connection = eniqService.getConnection();
                    if (connection!=null) {
                        eniqService.getConnection().close();
                        System.err.println(urlDb + " was closed!");
                    }
                } catch (SQLException e) {
                    logger.error(e.getMessage(),e);
                    result += Throwables.getStackTraceAsString(e);
                }
        }
    }

    public String getUrlDb() {
        return urlDb;
    }

    public void setUrlDb(String urlDb) {
        this.urlDb = urlDb;
    }

    public String getUsernameDb() {
        return usernameDb;
    }

    public void setUsernameDb(String usernameDb) {
        this.usernameDb = usernameDb;
    }

    public String getPasswordDb() {
        return passwordDb;
    }

    public void setPasswordDb(String passwordDb) {
        this.passwordDb = passwordDb;
    }

    public String getFileSql() {
        return fileSql;
    }

    public void setFileSql(String fileSql) {
        this.fileSql = fileSql;
    }

    public String getUsernameLogin() {
        return usernameLogin;
    }

    public void setUsernameLogin(String usernameLogin) {
        this.usernameLogin = usernameLogin;
    }

    public String getPasswordLogin() {
        return passwordLogin;
    }

    public void setPasswordLogin(String passwordLogin) {
        this.passwordLogin = passwordLogin;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public List<List<Object>> getData() {
        return data;
    }

    public void setData(List<List<Object>> data) {
        this.data = data;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(List<String> columns) {
        this.columns = columns;
    }
}
