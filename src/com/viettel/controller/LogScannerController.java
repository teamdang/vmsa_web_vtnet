package com.viettel.controller;

import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.io.*;
import java.nio.file.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by huynx6 on 10/18/2017.
 */
@ManagedBean
@ViewScoped
public class LogScannerController {
    int tailLine = 100;
    String log;
    private StringBuilder logBuffer = new StringBuilder();
    Path logFile;
    private int lines = 0;
    private int characters = 0;
    protected static final org.slf4j.Logger logger = LoggerFactory.getLogger(LogScannerController.class);
    private TailLog thread;

    @PostConstruct
    public void onStart() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        File TOMCAT_DIR = new File(((ServletContext) externalContext.getContext())
                .getRealPath("")).getParentFile().getParentFile();    //...../tomcat
        File LOG_DIR = new File(TOMCAT_DIR.getPath() + File.separator + "logs"); //...../tomcat/temp
        logFile = Paths.get(LOG_DIR.getAbsolutePath() + File.separator + "catalina.out");
        logBuffer = new StringBuilder();
        if (thread != null) {
            thread.stopWatch();
            thread.stop();
        }
    }

    public void run() {
        if (thread != null) {
            thread.stopWatch();
            thread.stop();
        }
        lines = 0;
        characters=0;
        logBuffer = new StringBuilder();
        try {
            int numberOfline = 100;
            try (LineNumberReader lnr = new LineNumberReader(new FileReader(logFile.toFile()))) {
                lnr.skip(Long.MAX_VALUE);
                numberOfline = (lnr.getLineNumber() + 1);
            }
            try (BufferedReader in = new BufferedReader(new FileReader(logFile.toFile()))) {
                String line;
                while ((line = in.readLine()) != null) {
                    lines++;
                    if (lines > numberOfline - tailLine) {
                        logBuffer.append(line);
                        logBuffer.append(System.lineSeparator());
                    }
                    characters += line.length() + System.lineSeparator().length();

                }
            }
            thread = new TailLog();
            thread.start();
            log = logBuffer.toString();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    class TailLog extends Thread implements Runnable {

        protected final org.slf4j.Logger logger = LoggerFactory.getLogger(LogScannerController.class);
        boolean forceStop = false;
        WatchService watcher;

        public void stopWatch() {
            if (watcher != null) {
                try {
                    watcher.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }

        public TailLog() {
            try {
                watcher = FileSystems.getDefault().newWatchService();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }

        @Override
        public void run() {
            try {

                logFile.toAbsolutePath().getParent().register(watcher, StandardWatchEventKinds.ENTRY_MODIFY);
                do {

                    if (forceStop)
                        return;
                    WatchKey key = watcher.take();
                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent<Path> pathEvent = (WatchEvent<Path>) event;
                        Path path = pathEvent.context();
                        if (logFile.endsWith(path)) {
                            try (BufferedReader in = new BufferedReader(new FileReader(logFile.toFile()))) {
                                String line;

                                in.skip(characters);
                                while ((line = in.readLine()) != null) {
                                    lines++;
                                    characters += line.length() + System.lineSeparator().length();
                                    if (logBuffer.length() < 50000) {
                                        logBuffer.append(line);
                                        logBuffer.append(System.lineSeparator());
                                    }else {
                                        logBuffer.delete(0,line.length()+System.lineSeparator().length());
                                        logBuffer.append(line);
                                        logBuffer.append(System.lineSeparator());
                                    }
                                }
                            }
                        }
                    }
                    key.reset();
                } while (true);
            } catch (InterruptedException e) {
                logger.error(e.getMessage(), e);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }


        public void setForceStop(boolean forceStop) {
            this.forceStop = forceStop;
        }
    }

    public void buildLog() {
        log = logBuffer.toString();
    }


    public int getTailLine() {
        return tailLine;
    }

    public void setTailLine(int tailLine) {
        this.tailLine = tailLine;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
