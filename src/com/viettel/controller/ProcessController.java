package com.viettel.controller;

import com.jcraft.jsch.ChannelSftp;
import com.viettel.model.ManageProcess;
import com.viettel.model.ManageServer;
import com.viettel.object.ConfigFile;
import com.viettel.persistence.GenericDaoImplNewV2;
import com.viettel.persistence.GenericDaoServiceNewV2;
import com.viettel.util.JSchSshUtil;
import com.viettel.util.MessageUtil;
import com.viettel.util.Result;
import org.apache.axis.utils.ByteArrayOutputStream;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * Created by xuanhuy on 6/22/2017.
 */
@ManagedBean
@ViewScoped
public class ProcessController {

    protected static final Logger logger = LoggerFactory.getLogger(ProcessController.class);
    private static final int TIME_DELAY = 2000;

    List<ManageProcess> processes;

    private GenericDaoServiceNewV2<ManageProcess, Long> manageProcessService;
    private GenericDaoServiceNewV2<ManageServer, Long> manageServerService;
    private Map<String, ManageServer> mapServer;
    private ManageProcess selectedProcess;
    private String action;
    private String processPassword;
    private final String DEFAULT_PASSWORD = "Vmsa!@#123";
    List<ConfigFile> logFiles;

    @PostConstruct
    public void onStart() {

        manageProcessService = new GenericDaoImplNewV2<ManageProcess, Long>() {
        };
        manageServerService = new GenericDaoImplNewV2<ManageServer, Long>() {
        };
        try {
            LinkedHashMap<String, String> order = new LinkedHashMap<>();
            order.put("ipServer", "ASC");
            order.put("processName", "ASC");
            processes = manageProcessService.findList(null, order);
            List<ManageServer> servers = manageServerService.findList();
            mapServer = new HashMap<>();
            for (ManageServer server : servers) {
                mapServer.put(server.getIpServer(), server);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        getStatusAll();

    }

    private long configPressed;

    public void saveConfig(ConfigFile configFile) {
        if (configPressed + TIME_DELAY > System.currentTimeMillis()) {
            JSchSshUtil sshClient = null;
            try {
                ManageServer server = mapServer.get(selectedProcess.getIpServer());
                sshClient = new JSchSshUtil(selectedProcess.getIpServer(), 22, server.getUserDecode(),
                        server.getPassDecode(), null, null, 15000, false, null, null, null);
                sshClient.connect();
                if (sshClient.isConnect()) {
                    sshClient.getSftpChanel().put(new ByteArrayInputStream(configFile.getContent().getBytes()), configFile.getFilePath(), ChannelSftp.OVERWRITE);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);

            } finally {
                try {
                    if (sshClient != null) {
                        sshClient.disconnect();
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
            MessageUtil.setInfoMessage("Save file " + configFile.getFilePath() + " successfully");
        } else {
            MessageUtil.setWarnMessage("Click once again to confirm edit file " + configFile.getFilePath());
        }
        configPressed = System.currentTimeMillis();
    }

    public void preLoadLog(ManageProcess process) {
        selectedProcess = process;
        JSchSshUtil sshClient = null;
        try {
            ManageServer server = mapServer.get(selectedProcess.getIpServer());
            String cmdEnd = "find " + selectedProcess.getLogDir() + " -type f | xargs du -sh";
            String user = server.getUserDecode();
            String pass = server.getPassDecode();

            sshClient = new JSchSshUtil(process.getIpServer(), 22, user,
                    pass, null, null, 15000, false, null, null, null);
            sshClient.connect();
            if (sshClient.isConnect()) {
                Result result = sshClient.sendLineWithTimeOutNew(cmdEnd, 15000, true);
                String content = result.getResult();
                if (content != null) {
                    content = content.trim();
                } else {
                    content = "";
                }
                String firstLine = content.contains("\n") ? content.substring(0, content.indexOf("\n")) : null;
                if (firstLine != null) {
                    content = content.substring(content.indexOf("\n"));
                } else {
                    content = "";
                }
                if (!content.isEmpty()) {
                    selectedProcess.setFileLogs(new ArrayList<ConfigFile>());
                    List<ConfigFile> fileLogs = selectedProcess.getFileLogs();
                    for (String file : content.split("\r\n", -1)) {
                        ConfigFile configFile = new ConfigFile();
                        String[] split = file.trim().split("\\s+", -1);
                        file = split[1];
                        configFile.setFilePath(file);
                        configFile.setSize(split[0]);
                        fileLogs.add(configFile);
                    }

                    cmdEnd = "find " + selectedProcess.getLogDir() + " -type f -printf \"%T+\\t%p\\n\" | sort -r";
                    Result result2 = sshClient.sendLineWithTimeOutNew(cmdEnd, 15000, true);
                    String content2 = result2.getResult();
                    firstLine = content2.contains("\n") ? content2.substring(0, content2.indexOf("\n")) : null;
                    if (firstLine != null) {
                        content2 = content2.substring(content2.indexOf("\n"));
                    } else {
                        content2 = "";
                    }
                    Map<String, Integer> mapFileOrder = new HashMap<>();
                    Map<String, String> mapFileDate = new HashMap<>();
                    int i = 0;
                    for (String file : content2.split("\r\n", -1)) {
                        String[] split = file.trim().split("\\s+", -1);
                        mapFileOrder.put(split[1], i++);
                        mapFileDate.put(split[1], split[0]);
                    }

                    for (ConfigFile fileLog : fileLogs) {
                        fileLog.setIndex(mapFileOrder.get(fileLog.getFilePath()));
                        fileLog.setDateChanged(mapFileDate.get(fileLog.getFilePath()));
                    }

                    Collections.sort(fileLogs, new Comparator<ConfigFile>() {
                        @Override
                        public int compare(ConfigFile o1, ConfigFile o2) {
                            return o1.getIndex().compareTo(o2.getIndex());
                        }
                    });
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);

        } finally {
            try {
                if (sshClient != null) {
                    sshClient.disconnect();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        logFiles = selectedProcess.getFileLogs();
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("formLog:logTable");
        dataTable.getFilters().clear();

    }

    public void setSelectedFile(ConfigFile configFile) {
        System.out.println(configFile);
    }

    String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void downloadLog(ActionEvent actionEvent) {
        CommandButton commandButton = (CommandButton) actionEvent.getSource();
        System.out.println(commandButton.getValue());
        System.out.println((String) actionEvent.getComponent().getAttributes().get("action"));

    }

    public void downloadLog(ConfigFile configFile) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map<String, String> params = facesContext.getExternalContext().getRequestParameterMap();
        String filePath = params.get("filePath");

        try {
            HttpServletResponse servletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            servletResponse.setContentType("application/octet-stream");
            servletResponse.setHeader("Expires", "0");
            servletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            servletResponse.setHeader("Pragma", "public");
            servletResponse.setHeader("Content-disposition", "attachment;filename="
                    + configFile.getFilePath().replace("/", "_"));

            JSchSshUtil sshClient = null;
            try {
                ManageServer server = mapServer.get(selectedProcess.getIpServer());
                sshClient = new JSchSshUtil(selectedProcess.getIpServer(), 22, server.getUserDecode(),
                        server.getPassDecode(), null, null, 15000, false, null, null, null);
                sshClient.connect();
                if (sshClient.isConnect()) {
                    OutputStream out = servletResponse.getOutputStream();
                    sshClient.getSftpChanel().get(configFile.getFilePath(), out);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            } finally {
                try {
                    if (sshClient != null) {
                        sshClient.disconnect();
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        facesContext.responseComplete();
    }

    public void getConfigfile() {
        JSchSshUtil sshClient = null;
        try {
            ManageServer server = mapServer.get(selectedProcess.getIpServer());
            String cmdEnd = "find " + selectedProcess.getConfigDir() + " -type f";

            sshClient = new JSchSshUtil(selectedProcess.getIpServer(), 22, server.getUserDecode(),
                    server.getPassDecode(), null, null, 15000, false, null, null, null);
            sshClient.connect();
            if (sshClient.isConnect()) {
                Result result = sshClient.sendLineWithTimeOutNew(cmdEnd, 15000, true);
                String content = result.getResult();
                if (content != null) {
                    content = content.trim();
                } else {
                    content = "";
                }
                String firstLine = content.contains("\n") ? content.substring(0, content.indexOf("\n")) : null;
                if (firstLine != null) {
                    content = content.substring(content.indexOf("\n"));
                } else {
                    content = "";
                }
                if (!content.isEmpty()) {
                    ChannelSftp sftpChanel = sshClient.getSftpChanel();
                    selectedProcess.setFileConfigs(new ArrayList<ConfigFile>());
                    for (String file : content.split("\r\n", -1)) {
                        ConfigFile configFile = new ConfigFile();
                        file = file.trim();
                        if (file.endsWith("xls") || file.endsWith("xlsx") || file.endsWith("doc") || file.endsWith("docx")) {
                            continue;
                        }

                        configFile.setFilePath(file);
                        OutputStream out = new ByteArrayOutputStream();
                        sftpChanel.get(file, out);
                        configFile.setContent(out.toString());
                        selectedProcess.getFileConfigs().add(configFile);
                    }
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);

        } finally {
            try {
                if (sshClient != null) {
                    sshClient.disconnect();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

    }

    public void processAction() {
        if (processPassword.equals(DEFAULT_PASSWORD)) {
            String msg = executeCommand(selectedProcess, action);
            selectedProcess.setStatusLog(msg);
            MessageUtil.setInfoMessage(msg);
        }
    }

    public void preConfirm(ManageProcess process, String action) {
        processPassword = null;
        this.selectedProcess = process;
        this.action = action;
    }

    public void preConfig(ManageProcess process) {
        this.selectedProcess = process;
        getConfigfile();
    }

    public void getStatusAll() {
        Map<String, JSchSshUtil> jSchSshUtilMap = new HashMap<>();
        for (ManageProcess process : processes) {
            JSchSshUtil sshClient;
            try {
                if (jSchSshUtilMap.containsKey(process.getIpServer())) {
                    sshClient = jSchSshUtilMap.get(process.getIpServer());
                } else {
                    ManageServer server = mapServer.get(process.getIpServer());
                    String user = server.getUserDecode();
                    String pass = server.getPassDecode();

                    sshClient = new JSchSshUtil(process.getIpServer(), 22, user,
                            pass, null, null, 15000, false, null, null, null);
                    sshClient.connect();
                    jSchSshUtilMap.put(process.getIpServer(), sshClient);
                }
                String cmdEnd = (process.getBaseDir() + "/").replaceAll("//", "/")
                        + process.getStatusCmd();

                if (sshClient.isConnect()) {
                    if (cmdEnd != null) {
                        Result result = sshClient.sendLineWithTimeOutNew(cmdEnd, 15000, true);
                        String content = result.getResult();
                        if (content != null) {
                            content = content.trim();
                        } else {
                            content = "";
                        }
                        String firstLine = content.contains("\n") ? content.substring(0, content.indexOf("\n")) : null;
                        if (firstLine != null) {
                            content = content.substring(content.indexOf("\n"));
                        } else {
                            content = "";
                        }
                        process.setStatusLog(content);
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);

            }
        }
        for (String s : jSchSshUtilMap.keySet()) {
            try {
                JSchSshUtil sshClient = jSchSshUtilMap.get(s);
                if (sshClient != null) {
                    sshClient.disconnect();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

    }

    public String executeCommand(ManageProcess process, String action) {
        JSchSshUtil sshClient = null;
        try {
            ManageServer server = mapServer.get(process.getIpServer());
            String cmdEnd = (process.getBaseDir() + "/").replaceAll("//", "/");
            switch (action) {
                case "status":
                    cmdEnd += process.getStatusCmd();
                    break;
                case "start":
                    cmdEnd += process.getStartCmd();
                    break;
                case "stop":
                    cmdEnd += process.getStopCmd();
                    break;
                case "restart":
                    cmdEnd += process.getRestartCmd();
                    break;
            }
            sshClient = new JSchSshUtil(process.getIpServer(), 22, server.getUserDecode(),
                    server.getPassDecode(), null, null, 15000, false, null, null, null);
            sshClient.connect();
            if (sshClient.isConnect()) {
                Result result = sshClient.sendLineWithTimeOutNew(cmdEnd, 15000, false);
                String content = result.getResult();
                if (content != null) {
                    content = content.trim();
                } else {
                    content = "";
                }
                String firstLine = content.contains("\n") ? content.substring(0, content.indexOf("\n")) : null;
                if (firstLine != null) {
                    content = content.substring(content.indexOf("\n"));
                } else {
                    content = "";
                }
                return content;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return e.getMessage();
        } finally {
            try {
                if (sshClient != null) {
                    sshClient.disconnect();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return null;
    }

    public List<ManageProcess> getProcesses() {
        return processes;
    }

    public void setProcesses(List<ManageProcess> processes) {
        this.processes = processes;
    }

    public ManageProcess getSelectedProcess() {
        return selectedProcess;
    }

    public void setSelectedProcess(ManageProcess selectedProcess) {
        this.selectedProcess = selectedProcess;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getProcessPassword() {
        return processPassword;
    }

    public void setProcessPassword(String processPassword) {
        this.processPassword = processPassword;
    }

    public String getDEFAULT_PASSWORD() {
        return DEFAULT_PASSWORD;
    }

    public List<ConfigFile> getLogFiles() {
        return logFiles;
    }

    public void setLogFiles(List<ConfigFile> logFiles) {
        this.logFiles = logFiles;
    }
}
