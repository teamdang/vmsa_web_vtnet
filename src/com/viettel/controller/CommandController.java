/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.controller;

import com.jcraft.jsch.ChannelSftp;
import com.rits.cloning.Cloner;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.ActionCommand;
import com.viettel.model.ActionOfFlow;
import com.viettel.model.CommandDetail;
import com.viettel.model.CommandTelnetParser;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.model.NodeType;
import com.viettel.model.ParamInput;
import com.viettel.model.Vendor;
import com.viettel.model.Version;
import com.viettel.object.ComboBoxObject;
import com.viettel.object.CommandObject;
import com.viettel.passprotector.PassProtector;
import com.viettel.persistence.ActionCommandServiceImpl;
import com.viettel.persistence.CommandDetailServiceImpl;
import com.viettel.persistence.CommandTelnetParserServiceImpl;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.NodeTypeServiceImpl;
import com.viettel.persistence.ParamGroupServiceImpl;
import com.viettel.persistence.ParamInputServiceImpl;
import com.viettel.persistence.ParamValueServiceImpl;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.persistence.VersionServiceImpl;
import com.viettel.util.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;

/**
 * @author hienhv4
 */
@ViewScoped
@ManagedBean
public class CommandController implements Serializable {

    private static final Logger logger = LogManager.getLogger(CommandController.class);

    @ManagedProperty(value = "#{commandDetailService}")
    private CommandDetailServiceImpl commandDetailService;
    @ManagedProperty(value = "#{commandTelnetParserService}")
    private CommandTelnetParserServiceImpl commandTelnetParserService;
    @ManagedProperty(value = "#{vendorService}")
    private VendorServiceImpl vendorService;
    @ManagedProperty(value = "#{nodeTypeService}")
    private NodeTypeServiceImpl nodeTypeService;
    @ManagedProperty(value = "#{paramInputService}")
    private ParamInputServiceImpl paramInputService;
    @ManagedProperty(value = "#{actionCommandService}")
    private ActionCommandServiceImpl actionCommandService;
    @ManagedProperty(value = "#{versionService}")
    private VersionServiceImpl versionService;
    @ManagedProperty(value = "#{nodeService}")
    private NodeServiceImpl nodeService;
    @ManagedProperty(value = "#{paramGroupService}")
    private ParamGroupServiceImpl paramGroupService;
    @ManagedProperty(value = "#{paramValueService}")
    private ParamValueServiceImpl paramValueService;
    @ManagedProperty(value = "#{flowTemplatesService}")
    private FlowTemplatesServiceImpl flowTemplatesService;

    private LazyDataModel<CommandDetail> lazyModel;
    private CommandDetail obj;
    private CommandDetail oldObj;
    private List<Vendor> vendors;
    private List<NodeType> nodeTypes;
    private List<Version> versions;
    private List<ComboBoxObject> operators;
    private List<ComboBoxObject> protocols;
    private List<Node> nodeRuns;
    private List<FlowTemplates> templates;

    private List<Vendor> vendorSeleteds;
    private List<NodeType> nodeTypeSelecteds;
    private List<Version> versionSelecteds;

    private Vendor vendorSelected;
    private NodeType nodeTypeSelected;
    private Version versionSelected;
    private ComboBoxObject opeSelected;
    private ComboBoxObject protocolSelected;

    private boolean isEdit, isClone;

    private List<ParamInput> params = new ArrayList<>();

    private String dialogHeader;
    private StreamedContent resultImport;
    private boolean hasError;
    private CommandObject commandSend;

    private boolean standardValueRequired;
    private boolean standardValueReadonly;
    private String standardValuePrompt;
    private boolean isShowNodeType;
    //Quytv7 ghi log action
    private String logAction = "";
    private String className = CommandController.class.getName();

    @PostConstruct
    protected void initialize() {
        try {

            obj = new CommandDetail();

            vendorSeleteds = new ArrayList<>();
            nodeTypeSelecteds = new ArrayList<>();
            versionSelecteds = new ArrayList<>();

            Map<String, String> orderVendor = new HashMap<>();
            orderVendor.put("vendorName", "asc");
            vendors = vendorService.findList(null, orderVendor);

            isShowNodeType = Boolean.parseBoolean(MessageUtil.getResourceBundleConfig("SHOW_NODE_TYPE").toLowerCase());
            if (isShowNodeType) {
                Map<String, String> orderNodeType = new HashMap<>();
                orderNodeType.put("typeName", "asc");
                nodeTypes = nodeTypeService.findList(null, orderNodeType);
            } else {
                nodeTypes = new ArrayList<>();
                nodeTypes.add(nodeTypeService.get(-1l));
            }

            Map<String, String> orderVersion = new HashMap<>();
            orderVersion.put("versionName", "asc");
            versions = versionService.findList(null, orderVersion);

            operators = new ArrayList<>();
            operators.add(new ComboBoxObject("CONTAIN", "CONTAIN"));
            operators.add(new ComboBoxObject("CONTAIN ALL", "CONTAIN ALL"));
            operators.add(new ComboBoxObject("NOT CONTAIN", "NOT CONTAIN"));
            operators.add(new ComboBoxObject("IS NULL", "IS NULL"));
            operators.add(new ComboBoxObject("NOT NULL", "NOT NULL"));
            operators.add(new ComboBoxObject("=", "="));
            operators.add(new ComboBoxObject("<", "<"));
            operators.add(new ComboBoxObject("<=", "<="));
            operators.add(new ComboBoxObject(">", ">"));
            operators.add(new ComboBoxObject(">=", ">="));
            operators.add(new ComboBoxObject("<>", "<>"));
            operators.add(new ComboBoxObject("IN", "IN"));
            operators.add(new ComboBoxObject("BETWEEN", "BETWEEN"));
            operators.add(new ComboBoxObject("LIKE", "LIKE"));
            operators.add(new ComboBoxObject("NOT LIKE", "NOT LIKE"));
            operators.add(new ComboBoxObject("NOT IN", "NOT IN"));
            operators.add(new ComboBoxObject("NO CHECK", "NO CHECK"));
            operators.add(new ComboBoxObject("IS NULL OR CONTAIN", "IS NULL OR CONTAIN"));

            Collections.sort(operators, new Comparator<ComboBoxObject>() {
                @Override
                public int compare(ComboBoxObject a, ComboBoxObject b) {
                    return a.getLabel().compareTo(b.getLabel());
                }
            });

            protocols = new ArrayList<>();
            protocols.add(new ComboBoxObject(Config.PROTOCOL_TELNET, Config.PROTOCOL_TELNET));
            protocols.add(new ComboBoxObject(Config.PROTOCOL_SSH, Config.PROTOCOL_SSH));
            protocols.add(new ComboBoxObject(Config.PROTOCOL_SQL, Config.PROTOCOL_SQL));
            protocols.add(new ComboBoxObject(Config.PROTOCOL_EXCHANGE, Config.PROTOCOL_EXCHANGE));
            protocols.add(new ComboBoxObject(Config.PROTOCOL_FTP, Config.PROTOCOL_FTP));

//            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
//            orders.put("commandTelnetParser.cmd", "ASC");
//            orders.put("vendor.vendorName", "ASC");
//            orders.put("nodeType.typeName", "ASC");
            Map<String, Object> filters = getFilter();
//            if (new SessionUtil().isOnlyViewCommand()) {
//                filters.put("commandType", 1l);
//            }
            lazyModel = new LazyDataModelBaseNew(commandDetailService, filters, new LinkedHashMap<>());
            logAction = LogUtils.addContent("", "Login Function");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);

        } catch (Exception e) {
            isShowNodeType = true;
            logger.error(e.getMessage(), e);
        }
    }

    public void onSearch() {
        try {
//            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
//            orders.put("commandTelnetParser.cmd", "ASC");
//            orders.put("vendor.vendorName", "ASC");
//            orders.put("nodeType.typeName", "ASC");

            Map<String, Object> filters = getFilter();

            lazyModel = new LazyDataModelBaseNew(commandDetailService, filters, new LinkedHashMap<>());
            DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("tableCommand");
            dataTable.setFirst(0);
            logAction = LogUtils.addContent("", "Search");
            logAction = LogUtils.addContent(logAction, "filters size: " + filters.size());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private Map<String, Object> getFilter() {
        Map<String, Object> filters = new HashMap<>();
        if (obj.getCommandTelnetParser().getCmd() != null && !"".equals(obj.getCommandTelnetParser().getCmd().trim())) {
            filters.put("commandTelnetParser.cmd", obj.getCommandTelnetParser().getCmd().trim());
        }

        if (obj.getCommandName() != null && !"".equals(obj.getCommandName().trim())) {
            filters.put("commandName", obj.getCommandName().trim());
        }

        if (obj.getVendor() != null && obj.getVendor().getVendorId() != null && obj.getVendor().getVendorId() != -1) {
            filters.put("vendor.vendorId", obj.getVendor().getVendorId());
        }

        if (obj.getNodeType() != null && obj.getNodeType().getTypeId() != null && obj.getNodeType().getTypeId() != -1) {
            filters.put("nodeType.typeId", obj.getNodeType().getTypeId());
        }

        if (obj.getVersion() != null && obj.getVersion().getVersionId() != null
                && obj.getVersion().getVersionId() != -1) {
            filters.put("version.versionId", obj.getVersion().getVersionId());
        }

        if (obj.getCommandType() != null && obj.getCommandType() != -1) {
            filters.put("commandType", obj.getCommandType());
        }

        if (opeSelected != null && opeSelected.getValue() != null) {
            filters.put("operator", opeSelected.getValue());
        }

        if (protocolSelected != null && protocolSelected.getValue() != null) {
            filters.put("protocol", protocolSelected.getValue());
        }
        return filters;
    }

    public void onSaveOrUpdate() {
        try {
            if (!validateInput(isEdit)) {
                return;
            }

            if (isClone) {
                    nodeTypeSelecteds.clear();
                vendorSeleteds.clear();
                versionSelecteds.clear();

                nodeTypeSelecteds.add(nodeTypeSelected);
                vendorSeleteds.add(vendorSelected);
                versionSelecteds.add(versionSelected);
            }

            if (isEdit && !isClone) {
                logAction = LogUtils.addContent("", "Edit command");
                logAction = LogUtils.addContent(logAction, "Command Old: " + obj.toString());
                logAction = LogUtils.addContent(logAction, "Command New: " + obj.toString());
                Map<String, Object> filters = new HashMap<>();
                filters.put("commandTelnetParser.cmd-" + commandDetailService.EXAC_IGNORE_CASE, obj.getCommandTelnetParser().getCmd().trim());
                filters.put("commandName-" + commandDetailService.EXAC_IGNORE_CASE, obj.getCommandName().trim());
                filters.put("commandDetailId-" + commandDetailService.NEQ, obj.getCommandDetailId());

                List<CommandDetail> lstCmd = commandDetailService.findList(filters);
                String msg = "";
                if (lstCmd != null && !lstCmd.isEmpty()) {
                    if (isShowNodeType) {
                        for (CommandDetail cmd : lstCmd) {
                            if (cmd.getVendor().getVendorId().equals(vendorSelected.getVendorId())
                                    && cmd.getNodeType().getTypeId().equals(nodeTypeSelected.getTypeId())
                                    && cmd.getVersion().getVersionId().equals(versionSelected.getVersionId())) {
                                msg = "\n" + vendorSelected.getVendorName() + " - " + nodeTypeSelected.getTypeName() + " - " + versionSelected.getVersionName();
                                break;
                            }
                        }
                    } else {
                        for (CommandDetail cmd : lstCmd) {
                            if (cmd.getVendor().getVendorId().equals(vendorSelected.getVendorId())
                                    && cmd.getVersion().getVersionId().equals(versionSelected.getVersionId())) {
                                msg = "\n" + vendorSelected.getVendorName() + " - " + versionSelected.getVersionName();
                                break;
                            }
                        }
                    }
                }

                if (!msg.isEmpty()) {
                    if (isShowNodeType) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist"), msg));
                        logAction = LogUtils.addContent(logAction, "Result " + MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist"), msg));
                    } else {
                        logAction = LogUtils.addContent(logAction, "Result " + MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist.noNodeType"), msg));
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist.noNodeType"), msg));
                    }

                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);

                    return;
                }

                CommandDetail cmdDetail = new CommandDetail();
                CommandTelnetParser parser = new CommandTelnetParser();
                if (obj.getCommandDetailId() != null) {
                    cmdDetail = commandDetailService.findById(obj.getCommandDetailId());
                }
                //19092017 Quytv7 sua check lenh khong duoc sua neu dang thuoc mop dang phe duyet start
                for (ActionCommand acd : cmdDetail.getActionCommands()) {
                    for (ActionOfFlow aofw : acd.getActionDetail().getAction().getActionOfFlows()) {
                        if (aofw.getFlowTemplates().getStatus() != null
                                && aofw.getFlowTemplates().getStatus() == 9) {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil
                                    .getResourceBundleMessage("message.cmd.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
                            logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil
                                    .getResourceBundleMessage("message.cmd.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
                            LogUtils.writelog(new Date(), className, new Object() {
                            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
                            return;
                        }
                    }
                }
                //19092017 Quytv7 sua check lenh khong duoc sua neu dang thuoc mop dang phe duyet end

                if (obj.getCommandTelnetParser().getTelnetParserId() != null) {
                    parser = commandTelnetParserService.findById(obj.getCommandTelnetParser().getTelnetParserId());
                }

                getParser(parser, obj);

                commandTelnetParserService.saveOrUpdate(parser);

                cmdDetail.setCommandTelnetParser(parser);
                cmdDetail.setCreateTime(new Date());
                cmdDetail.setIsActive(1l);
                cmdDetail.setNodeType(nodeTypeSelected);
                cmdDetail.setVendor(vendorSelected);
                cmdDetail.setVersion(versionSelected);
                cmdDetail.setOperator(opeSelected.getValue());
                cmdDetail.setProtocol(protocolSelected.getValue());
                cmdDetail.setStandardValue(obj.getStandardValue() == null ? "" : obj.getStandardValue().trim());
                cmdDetail.setCommandName(obj.getCommandName() == null ? "" : obj.getCommandName().trim());
                cmdDetail.setDescription(obj.getDescription() == null ? "" : obj.getDescription().trim());
                //quytv7_20171011 fix ATTT loi XSS cho truong start
                cmdDetail.setExpectedResult(obj.getExpectedResult() == null ? "" : StringEscapeUtils.escapeEcmaScript(obj.getExpectedResult().trim()));

                //quytv7_20171011 fix ATTT loi XSS cho truong end
                cmdDetail.setUserName(SessionWrapper.getCurrentUsername());
                cmdDetail.setCommandType(obj.getCommandType());
                cmdDetail.setSourceGetParam(obj.getSourceGetParam() == null ? 0L : obj.getSourceGetParam());
//                if (isModify(oldObj, cmdDetail)) {
//                    for (ActionCommand acd : cmdDetail.getActionCommands()) {
//                        for (ActionOfFlow aofw : acd.getActionDetail().getAction().getActionOfFlows()) {
//                            if (aofw.getFlowTemplates().getStatus() != null
//                                    && aofw.getFlowTemplates().getStatus() == 9) {
//                                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil
//                                        .getResourceBundleMessage("message.cmd.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
//                                logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil
//                                        .getResourceBundleMessage("message.cmd.template.approved"), aofw.getFlowTemplates().getFlowTemplateName()));
//                                LogUtils.writelog(new Date(), className, new Object() {
//                                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
//                                return;
//                            }
//                        }
//                    }
//                }

                commandDetailService.saveOrUpdate(cmdDetail);

                reloadParam(parser.getCmd(), parser.getRegexOutput(), parser.getRowStart(), parser.getRowEnd(), obj.getStandardValue());

                List<ParamInput> currParam = obj.getParamInputs();
                for (ParamInput pr : params) {
                    ParamInput cur = checkValueInListDTO(currParam, pr.getParamCode());
                    if (cur != null) {
                        pr.setParamInputId(cur.getParamInputId());
                    }
//                    if (obj.getParammKey() != null && pr.getParamCode().equals(obj.getParammKey())) {
//                        pr.setIsParamKey(1L);
//                    } else {
//                        pr.setIsParamKey(0L);
//                    }
                    pr.setCommandDetail(cmdDetail);
                    pr.setCreateTime(new Date());
                    pr.setIsActive(1l);
                    if(pr.getIsParamKey() == null){
                        pr.setIsParamKey(false);
                    }
                    //pr.setParamType(0l);
                    if (pr.getReadOnly() == null) {
                        pr.setReadOnly(false);
                    }
                    pr.setUserName(SessionWrapper.getCurrentUsername());
                }

                paramInputService.saveOrUpdate(params);

                for (ParamInput pr : currParam) {
                    if (checkValueInListDTO(params, pr.getParamCode()) == null) {
                        paramGroupService.execteBulk("delete from ParamGroup where paramInput.paramInputId =?", pr.getParamInputId());

                        paramValueService.execteBulk("delete from ParamValue where paramInput.paramInputId =?", pr.getParamInputId());

                        paramInputService.execteBulk("delete from ParamInput where paramInputId =?", pr.getParamInputId());
                    }
                }

//                if (isModify(oldObj, cmdDetail)) {
//                    List<FlowTemplates> lstTemplate = new ArrayList<>();
//                    if (cmdDetail.getActionCommands() != null && !cmdDetail.getActionCommands().isEmpty()) {
//                        for (ActionCommand actionCmd : cmdDetail.getActionCommands()) {
//                            if (actionCmd.getActionDetail().getAction().getActionOfFlows() != null
//                                    && !actionCmd.getActionDetail().getAction().getActionOfFlows().isEmpty()) {
//                                for (ActionOfFlow actionOfFlow : actionCmd.getActionDetail().getAction().getActionOfFlows()) {
//                                    if ((actionOfFlow.getFlowTemplates().getStatus() == null || actionOfFlow.getFlowTemplates().getStatus() == 9)
//                                            && !lstTemplate.contains(actionOfFlow.getFlowTemplates())) {
//                                        lstTemplate.add(actionOfFlow.getFlowTemplates());
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    for (FlowTemplates template : lstTemplate) {
//                        template.setStatus(0);
//                    }
//                    flowTemplatesService.saveOrUpdate(lstTemplate);
//                }
                logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.success"),
                        MessageUtil.getResourceBundleMessage("title.updateCommand")));
                MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.success"),
                        MessageUtil.getResourceBundleMessage("title.updateCommand")));
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
            } else {
                if (isClone) {
                    logAction = LogUtils.addContent("", "Clone command");
                } else {
                    logAction = LogUtils.addContent("", "Add command");
                }
                logAction = LogUtils.addContent(logAction, "Command save: " + obj.toString());
                Map<String, Object> filters = new HashMap<>();
                filters.put("commandTelnetParser.cmd-" + commandDetailService.EXAC_IGNORE_CASE, obj.getCommandTelnetParser().getCmd().trim());
                filters.put("commandName-" + commandDetailService.EXAC_IGNORE_CASE, obj.getCommandName().trim());

                List<CommandDetail> lstCmd = commandDetailService.findList(filters);
                String msg = "";
                if (lstCmd != null && !lstCmd.isEmpty()) {
                    if (isShowNodeType) {
                        for (NodeType node : nodeTypeSelecteds) {
                            for (Vendor vendor : vendorSeleteds) {
                                for (Version version : versionSelecteds) {
                                    for (CommandDetail cmd : lstCmd) {
                                        if (cmd.getVendor().getVendorId().equals(vendor.getVendorId())
                                                && cmd.getNodeType().getTypeId().equals(node.getTypeId())
                                                && cmd.getVersion().getVersionId().equals(version.getVersionId())) {
                                            msg = "\n" + vendor.getVendorName() + " - " + node.getTypeName() + " - " + version.getVersionName();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        for (Vendor vendor : vendorSeleteds) {
                            for (Version version : versionSelecteds) {
                                for (CommandDetail cmd : lstCmd) {
                                    if (cmd.getVendor().getVendorId().equals(vendor.getVendorId())
                                            && cmd.getVersion().getVersionId().equals(version.getVersionId())) {
                                        msg = "\n" + vendor.getVendorName() + " - " + version.getVersionName();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (!msg.isEmpty()) {
                    if (isShowNodeType) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist"), msg));
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist"), msg));
                    } else {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist.noNodeType"), msg));
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist.noNodeType"), msg));
                    }
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
                    return;
                }

                for (NodeType node : nodeTypeSelecteds) {
                    for (Vendor vendor : vendorSeleteds) {
                        for (Version version : versionSelecteds) {
                            CommandDetail cmdDetail = new CommandDetail();
                            CommandTelnetParser parser = new CommandTelnetParser();

                            getParser(parser, obj);

                            commandTelnetParserService.save(parser);

                            cmdDetail.setCommandTelnetParser(parser);
                            cmdDetail.setCreateTime(new Date());
                            cmdDetail.setIsActive(1l);
                            cmdDetail.setNodeType(node);
                            cmdDetail.setVendor(vendor);
                            cmdDetail.setVersion(version);
                            cmdDetail.setOperator(opeSelected.getValue());
                            cmdDetail.setProtocol(protocolSelected.getValue());
                            cmdDetail.setStandardValue(obj.getStandardValue() == null ? "" : obj.getStandardValue().trim());
                            cmdDetail.setCommandName(obj.getCommandName() == null ? "" : obj.getCommandName().trim());
                            cmdDetail.setDescription(obj.getDescription() == null ? "" : obj.getDescription().trim());
                            cmdDetail.setExpectedResult(obj.getExpectedResult() == null ? "" : obj.getExpectedResult().trim());
                            cmdDetail.setUserName(SessionWrapper.getCurrentUsername());
                            cmdDetail.setCommandType(obj.getCommandType());
                            cmdDetail.setSourceGetParam(obj.getSourceGetParam() == null ? 0L : obj.getSourceGetParam());

                            commandDetailService.save(cmdDetail);

                            reloadParam(obj.getCommandTelnetParser().getCmd(), obj.getCommandTelnetParser().getRegexOutput(), obj.getCommandTelnetParser().getRowStart(), obj.getCommandTelnetParser().getRowEnd(), obj.getStandardValue());

                            for (ParamInput pr : params) {
                                pr.setCommandDetail(cmdDetail);
                                pr.setCreateTime(new Date());
                                pr.setIsActive(1l);
//                                if (obj.getParammKey() != null && pr.getParamCode().equals(obj.getParammKey())) {
//                                    pr.setIsParamKey(1L);
//                                } else {
//                                    pr.setIsParamKey(0L);
//                                }
                                //pr.setParamType(0l);
                                if (pr.getReadOnly() == null) {
                                    pr.setReadOnly(false);
                                }
                                pr.setUserName(SessionWrapper.getCurrentUsername());
                            }

                            paramInputService.saveOrUpdate(params);
                        }
                    }
                }

                if (!isClone) {
                    MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.success"),
                            MessageUtil.getResourceBundleMessage("title.insertCommand")));
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.success"),
                            MessageUtil.getResourceBundleMessage("title.insertCommand")));
                } else {
                    MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.success"),
                            MessageUtil.getResourceBundleMessage("title.cloneCommand")));
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.success"),
                            MessageUtil.getResourceBundleMessage("title.cloneCommand")));
                }
            }

            obj = new CommandDetail();
            RequestContext.getCurrentInstance().execute("PF('insertCmdConfigDialog').hide();");

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    isEdit ? MessageUtil.getResourceBundleMessage("title.updateCommand") : MessageUtil.getResourceBundleMessage("title.insertCommand")));
            logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    isEdit ? MessageUtil.getResourceBundleMessage("title.updateCommand") : MessageUtil.getResourceBundleMessage("title.insertCommand")));
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
    }

    public void onDelete() {
        try {
            if (obj == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.choose.delete"));
                return;
            }

            Map<String, Object> filters = new HashMap<>();
            filters.put("commandDetail.commandDetailId", obj.getCommandDetailId());

            List<ActionCommand> lstActionCmd = actionCommandService.findList(filters);

            if (lstActionCmd != null && !lstActionCmd.isEmpty()) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.choose.using"));
                return;
            }

            paramInputService.execteBulk("delete from ParamInput where commandDetail.commandDetailId =?", obj.getCommandDetailId());

            commandDetailService.delete(obj);

//            commandTelnetParserService.delete(obj.getCommandTelnetParser());
            commandTelnetParserService.execteBulk("delete from CommandTelnetParser where telnetParserId in (select commandTelnetParser.telnetParserId from CommandDetail where commandDetailId = ?)", obj.getCommandDetailId());

            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.success"),
                    MessageUtil.getResourceBundleMessage("title.deleteCommand")));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("title.deleteCommand")));
        }
    }

    public void noAction(CellEditEvent event) {
        reloadParam(obj.getCommandTelnetParser().getCmd(), obj.getCommandTelnetParser().getRegexOutput(), obj.getCommandTelnetParser().getRowStart(), obj.getCommandTelnetParser().getRowEnd(), obj.getStandardValue());
    }

    private void reloadParam(String cmd, String regex, String rowStart, String rowEnd, String standardValue) {
        try {
            List<String> lstParam = getParamList(cmd);
            List<String> lstParamRegex = getParamList(regex);
            List<String> lstParamRowStart = getParamList(rowStart);
            List<String> lstParamRowEnd = getParamList(rowEnd);
            List<String> lstParamStandardValue = getParamList(standardValue);
            List<String> lstParamFormula = new ArrayList<>();
            List<ParamInput> paramList = new ArrayList<>();
            List<ParamInput> paramListRegex = new ArrayList<>();
            List<ParamInput> paramListRowStart = new ArrayList<>();
            List<ParamInput> paramListRowEnd = new ArrayList<>();
            List<ParamInput> paramListFormula = new ArrayList<>();
            List<ParamInput> paramListStandardValue = new ArrayList<>();
            for (ParamInput dto : params) {
                ParamInput pr = new ParamInput();
                pr.setParamCode(dto.getParamCode());
                pr.setReadOnly(dto.getReadOnly());
                pr.setParamFormula(dto.getParamFormula());
                pr.setParamType(dto.getParamType());
                pr.setDescription(dto.getDescription());
                pr.setIsParamKey(dto.getIsParamKey());
                pr.setConfigCode(dto.getConfigCode());
                if (dto.getParamType() == 0) {
                    paramList.add(pr);
                } else if (dto.getParamType() == 1) {
                    paramListRegex.add(pr);
                } else if (dto.getParamType() == 2) {
                    paramListFormula.add(pr);
                }

                lstParamFormula.addAll(getParamList(dto.getParamFormula()));
            }

            boolean haveParams = false;
            params.clear();
            if (lstParam != null && !lstParam.isEmpty()) {
                for (String str : lstParam) {
                    if (checkValueInListDTO(paramList, str) == null) {
                        ParamInput dto = new ParamInput();
                        dto.setParamCode(str);
                        dto.setParamType(0l);
                        paramList.add(dto);
                    }
                }

                List<ParamInput> paramLocal = new ArrayList<>();
                for (ParamInput dto : paramList) {
                    if (checkValueInList(lstParam, dto.getParamCode())) {
                        paramLocal.add(dto);
                    }
                }
                addParam(paramLocal);
                haveParams = true;
            }

            if (lstParamRegex != null && !lstParamRegex.isEmpty()) {
                for (String str : lstParamRegex) {
                    if (checkValueInListDTO(paramListRegex, str) == null) {
                        ParamInput dto = new ParamInput();
                        dto.setParamCode(str);
                        dto.setParamType(1l);
                        paramListRegex.add(dto);
                    }
                }

                List<ParamInput> paramLocal = new ArrayList<>();
                for (ParamInput dto : paramListRegex) {
                    if (checkValueInList(lstParamRegex, dto.getParamCode())) {
                        paramLocal.add(dto);
                    }
                }
                addParam(paramLocal);
                haveParams = true;
            }

            if (!lstParamFormula.isEmpty()) {
                for (String str : lstParamFormula) {
                    if (checkValueInListDTO(paramListFormula, str) == null) {
                        ParamInput dto = new ParamInput();
                        dto.setParamCode(str);
                        dto.setParamType(2l);
                        paramListFormula.add(dto);
                    }
                }

                List<ParamInput> paramLocal = new ArrayList<>();
                for (ParamInput dto : paramListFormula) {
                    if (checkValueInList(lstParamFormula, dto.getParamCode())) {
                        paramLocal.add(dto);
                    }
                }
                addParam(paramLocal);
                haveParams = true;
            }
            if (lstParamRowStart != null && !lstParamRowStart.isEmpty()) {
                for (String str : lstParamRowStart) {
                    if (checkValueInListDTO(paramListRowStart, str) == null) {
                        ParamInput dto = new ParamInput();
                        dto.setParamCode(str);
                        dto.setParamType(1l);
                        paramListRowStart.add(dto);
                    }
                }

                List<ParamInput> paramLocal = new ArrayList<>();
                for (ParamInput dto : paramListRowStart) {
                    if (checkValueInList(lstParamRowStart, dto.getParamCode())) {
                        paramLocal.add(dto);
                    }
                }
                addParam(paramLocal);
                haveParams = true;
            }
            if (lstParamRowEnd != null && !lstParamRowEnd.isEmpty()) {
                for (String str : lstParamRowEnd) {
                    if (checkValueInListDTO(paramListRowEnd, str) == null) {
                        ParamInput dto = new ParamInput();
                        dto.setParamCode(str);
                        dto.setParamType(1l);
                        paramListRowEnd.add(dto);
                    }
                }

                List<ParamInput> paramLocal = new ArrayList<>();
                for (ParamInput dto : paramListRowEnd) {
                    if (checkValueInList(lstParamRowEnd, dto.getParamCode())) {
                        paramLocal.add(dto);
                    }
                }
                addParam(paramLocal);
                haveParams = true;
            }
            if (lstParamStandardValue != null && !lstParamStandardValue.isEmpty()) {
                for (String str : lstParamStandardValue) {
                    if (checkValueInListDTO(paramListStandardValue, str) == null) {
                        ParamInput dto = new ParamInput();
                        dto.setParamCode(str);
                        dto.setParamType(1l);
                        paramListStandardValue.add(dto);
                    }
                }

                List<ParamInput> paramLocal = new ArrayList<>();
                for (ParamInput dto : paramListStandardValue) {
                    if (checkValueInList(lstParamStandardValue, dto.getParamCode())) {
                        paramLocal.add(dto);
                    }
                }
                addParam(paramLocal);
                haveParams = true;
            }

            if (!haveParams) {
//                logger.info("Param is empty");
                params = new ArrayList<>();
            }

//            Collections.sort(params, new Comparator<ParamInput>() {
//                @Override
//                public int compare(ParamInput a, ParamInput b) {
//                    return a.getParamCode().compareTo(b.getParamCode());
//                }
//            });
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private void addParam(List<ParamInput> paramLocal) {
        for (ParamInput pr : paramLocal) {
            boolean exist = false;
            for (ParamInput param : params) {
                if (param.getParamCode().equals(pr.getParamCode())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                params.add(pr);
            }
        }
    }

    public void reloadParam() {
        reloadParam(obj.getCommandTelnetParser().getCmd(), obj.getCommandTelnetParser().getRegexOutput(), obj.getCommandTelnetParser().getRowStart(), obj.getCommandTelnetParser().getRowEnd(), obj.getStandardValue());
    }

    public StreamedContent onDownloadTemplate() {
        Workbook wb = null;
        try {
            ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String templatePath = context.getRealPath("/") + File.separator + "templates" + File.separator
                    + "import" + File.separator + (isShowNodeType ? "ImportCmdTemplate.xlsx" : "ImportCmdTemplate_noNodeType.xlsx");

            wb = WorkbookFactory.create(new File(templatePath));
            Sheet sheet = wb.getSheetAt(0);
            
            if (wb.getNumberOfSheets() > 1) {
                Sheet existSheet = wb.getSheetAt(1);
                if (existSheet != null) {
                    wb.removeSheetAt(1);
                }
            }
            Sheet hiddenSheet = wb.createSheet("hidden");

            int maxValue = vendors == null ? 0 : vendors.size();
            if (nodeTypes != null && isShowNodeType) {
                maxValue = (maxValue >= nodeTypes.size()) ? maxValue : nodeTypes.size();
            }
            if (versions != null) {
                maxValue = (maxValue >= versions.size()) ? maxValue : versions.size();
            }
            if (operators != null) {
                maxValue = (maxValue >= operators.size()) ? maxValue : operators.size();
            }

            for (int i = 0; i < maxValue; i++) {
                Row row = hiddenSheet.createRow(i);
                if (vendors != null && vendors.size() > i) {
                    row.createCell(0).setCellValue(vendors.get(i).getVendorName());
                }
                if (versions != null && versions.size() > i) {
                    row.createCell(1).setCellValue(versions.get(i).getVersionName());
                }
                if (operators != null && operators.size() > i) {
                    row.createCell(2).setCellValue(operators.get(i).getLabel());
                }
                if (nodeTypes != null && nodeTypes.size() > i && isShowNodeType) {
                    row.createCell(3).setCellValue(nodeTypes.get(i).getTypeName());
                }
            }

            int i = 1;
            if (vendors != null && !vendors.isEmpty()) {
                DataValidationHelper dvHelper = sheet.getDataValidationHelper();
                DataValidationConstraint dvConstraint = dvHelper.createFormulaListConstraint("hidden!$A$1:$A$" + vendors.size());
                CellRangeAddressList addressList = new CellRangeAddressList(8, 100, i, i);
                DataValidation validation = dvHelper.createValidation(dvConstraint, addressList);
                validation.setShowErrorBox(true);
                sheet.addValidationData(validation);
            }

            if (isShowNodeType) {
                i++;
            }
            if (nodeTypes != null && !nodeTypes.isEmpty() && isShowNodeType) {
                DataValidationHelper dvHelper = sheet.getDataValidationHelper();
                DataValidationConstraint dvConstraint = dvHelper.createFormulaListConstraint("hidden!$D$1:$D$" + nodeTypes.size());
                CellRangeAddressList addressList = new CellRangeAddressList(8, 100, i, i);
                DataValidation validation = dvHelper.createValidation(dvConstraint, addressList);
                validation.setShowErrorBox(true);
                sheet.addValidationData(validation);
            }

            i++;
            if (versions != null && !versions.isEmpty()) {
                DataValidationHelper dvHelper = sheet.getDataValidationHelper();
                DataValidationConstraint dvConstraint = dvHelper.createFormulaListConstraint("hidden!$B$1:$B$" + versions.size());
                CellRangeAddressList addressList = new CellRangeAddressList(8, 100, i, i);
                DataValidation validation = dvHelper.createValidation(dvConstraint, addressList);
                validation.setShowErrorBox(true);
                sheet.addValidationData(validation);
            }

            i += 5;
            if (operators != null && !operators.isEmpty()) {
                DataValidationHelper dvHelper = sheet.getDataValidationHelper();
                DataValidationConstraint dvConstraint = dvHelper.createFormulaListConstraint("hidden!$C$1:$C$" + operators.size());
                CellRangeAddressList addressList = new CellRangeAddressList(8, 100, i, i);
                DataValidation validation = dvHelper.createValidation(dvConstraint, addressList);
                validation.setShowErrorBox(true);
                sheet.addValidationData(validation);
            }

            wb.setSheetHidden(1, true);

            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                    .getExternalContext().getContext();
            String pathOut = ctx.getRealPath("/") + Config.PATH_OUT + (isShowNodeType ? "ImportCmdTemplate.xlsx" : "ImportCmdTemplate_noNodeType.xlsx");

            FileOutputStream fileOut = null;
            try {
                fileOut = new FileOutputStream(pathOut);
                wb.write(fileOut);

                return new DefaultStreamedContent(new FileInputStream(pathOut), ".xlsx", "ImportCmdTemplate.xlsx");
            } catch (Exception ex) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                        MessageUtil.getResourceBundleMessage("button.download.template")));
                logger.error(ex.getMessage(), ex);
            } finally {
                try {
                    if (fileOut != null) {
                        fileOut.close();
                    }
                } catch (IOException ex) {
                    logger.error(ex);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("button.download.template")));
        } finally {
            if (wb != null) {
                try {
                    wb.close();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
        return null;
    }

    public void onImport(FileUploadEvent event) {
        logAction = LogUtils.addContent("", "Import command");
        List<CommandDetail> result = new ArrayList<>();
        hasError = false;
        try {
            Workbook wb = WorkbookFactory.create(event.getFile().getInputstream());
            Sheet sheet = wb.getSheetAt(0);
            Row rowHeader = sheet.getRow(7);

            boolean check = checkHeader(rowHeader);

            if (check) {
                List<Vendor> lstVendor = vendorService.findList();
                List<NodeType> lstNodeType = nodeTypeService.findList();
                List<Version> lstVersion = versionService.findList();
                NodeType typeDefault = nodeTypeService.findById(-1l);

                Map<String, Long> mapVendor = new HashMap<>();
                Map<String, Long> mapNodeType = new HashMap<>();
                Map<String, Long> mapVersion = new HashMap<>();
                Map<String, String> mapOpers = new HashMap<>();

                for (Vendor vd : lstVendor) {
                    mapVendor.put(vd.getVendorName(), vd.getVendorId());
                }

                for (NodeType nt : lstNodeType) {
                    mapNodeType.put(nt.getTypeName(), nt.getTypeId());
                }

                for (Version vs : lstVersion) {
                    mapVersion.put(vs.getVersionName(), vs.getVersionId());
                }

                for (ComboBoxObject vs : operators) {
                    mapOpers.put(vs.getLabel(), vs.getValue());
                }

                int rowNum = sheet.getLastRowNum();
                for (int i = 8; i <= rowNum; i++) {
                    CommandDetail cmdDetail = new CommandDetail();
                    String err = checkRow(sheet, i, cmdDetail);
                    if (err != null) {
                        //Truong hop row co du lieu
                        if ("".equals(err)) {
                            Long vendorId = mapVendor.get(cmdDetail.getVendor().getVendorName());
                            Long nodeTypeId = mapNodeType.get(cmdDetail.getNodeType().getTypeName());
                            Long versionId = mapVersion.get(cmdDetail.getVersion().getVersionName());
                            String oper = mapOpers.get(cmdDetail.getOperator().toUpperCase());
                            Long cmdType = null;
                            if (cmdDetail.getCommandTypeStr() != null) {
                                if (cmdDetail.getCommandTypeStr().trim().toLowerCase().equals(MessageUtil.getResourceBundleMessage("label.command.type.impact").toLowerCase())) {
                                    cmdType = 0l;
                                } else if (cmdDetail.getCommandTypeStr().trim().toLowerCase().equals(MessageUtil.getResourceBundleMessage("label.command.type.view").toLowerCase())) {
                                    cmdType = 1l;
                                }
                            }

                            if (vendorId == null) {
                                err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.message.noexists"),
                                        MessageUtil.getResourceBundleMessage("label.vendorName")) + "\n";
                            }

                            if (nodeTypeId == null && isShowNodeType) {
                                err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.message.noexists"),
                                        MessageUtil.getResourceBundleMessage("label.nodeTypeName")) + "\n";
                            }

                            if (versionId == null) {
                                err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.message.noexists"),
                                        MessageUtil.getResourceBundleMessage("label.versionName")) + "\n";
                            }

                            if (cmdType == null) {
                                err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.message.noexists"),
                                        MessageUtil.getResourceBundleMessage("label.command.type")) + "\n";
                            } else {
                                cmdDetail.setCommandType(cmdType);
                            }

                            if (cmdDetail.getProtocol() == null || (!"TELNET".equals(cmdDetail.getProtocol().toUpperCase())
                                    && !"SSH".equals(cmdDetail.getProtocol().toUpperCase())
                                    && !"SQL".equals(cmdDetail.getProtocol().toUpperCase()))) {
                                err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.message.noexists"),
                                        MessageUtil.getResourceBundleMessage("label.protocol")) + "\n";
                            }

                            if (isShowNodeType) {
                                if (vendorId != null && nodeTypeId != null && versionId != null) {
                                    Map<String, Object> filters = new HashMap<>();
                                    filters.put("commandTelnetParser.cmd-" + commandDetailService.EXAC_IGNORE_CASE, cmdDetail.getCommandTelnetParser().getCmd().trim());
                                    filters.put("commandName-" + commandDetailService.EXAC_IGNORE_CASE, cmdDetail.getCommandName().trim());
                                    filters.put("vendor.vendorId", vendorId);
                                    filters.put("nodeType.typeId", nodeTypeId);
                                    filters.put("version.versionId", versionId);
                                    filters.put("isActive", 1l);

                                    List<CommandDetail> lstCmd = commandDetailService.findList(filters);
                                    if (lstCmd != null && !lstCmd.isEmpty()) {
                                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("message.command.exists"),
                                                cmdDetail.getVendor().getVendorName() + "-" + cmdDetail.getNodeType().getTypeName()
                                                + "-" + cmdDetail.getVersion().getVersionName());
                                    } else {
                                        cmdDetail.getVendor().setVendorId(vendorId);
                                        cmdDetail.getNodeType().setTypeId(nodeTypeId);
                                        cmdDetail.getVersion().setVersionId(versionId);
                                    }
                                }
                            } else {
                                if (vendorId != null && versionId != null) {
                                    Map<String, Object> filters = new HashMap<>();
                                    filters.put("commandTelnetParser.cmd-" + commandDetailService.EXAC_IGNORE_CASE, cmdDetail.getCommandTelnetParser().getCmd().trim());
                                    filters.put("commandName-" + commandDetailService.EXAC_IGNORE_CASE, cmdDetail.getCommandName().trim());
                                    filters.put("vendor.vendorId", vendorId);
                                    filters.put("version.versionId", versionId);
                                    filters.put("isActive", 1l);

                                    List<CommandDetail> lstCmd = commandDetailService.findList(filters);
                                    if (lstCmd != null && !lstCmd.isEmpty()) {
                                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("message.choose.cmdExist.noNodeType"),
                                                cmdDetail.getVendor().getVendorName() + "-" + cmdDetail.getVersion().getVersionName());
                                    } else {
                                        cmdDetail.getVendor().setVendorId(vendorId);
                                        cmdDetail.getNodeType().setTypeId(typeDefault.getTypeId());
                                        cmdDetail.getVersion().setVersionId(versionId);
                                    }
                                }
                            }

                            if (oper == null) {
                                err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.message.noexists"),
                                        MessageUtil.getResourceBundleMessage("label.operator")) + "\n";
                            } else {
                                cmdDetail.setOperator(oper);

                                switch (oper.toUpperCase()) {
                                    case "IS NULL":
                                    case "NOT NULL":
                                    case "NO CHECK":
                                        cmdDetail.setStandardValue("");
                                        break;
                                    case "IN":
                                    case "NOT IN":
                                    case "CONTAIN":
                                    case "IS NULL OR CONTAIN":
                                    case "NOT CONTAIN":
                                    case "=":
                                    case ">":
                                    case ">=":
                                    case "<":
                                    case "<=":
                                    case "<>":
                                        if (isNullOrEmpty(cmdDetail.getStandardValue())) {
                                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                                    MessageUtil.getResourceBundleMessage("label.standardValue")) + "\n";
                                        }
                                        break;
                                    case "BETWEEN":
                                        if (isNullOrEmpty(cmdDetail.getStandardValue())) {
                                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                                    MessageUtil.getResourceBundleMessage("label.standardValue")) + "\n";
                                            break;
                                        }

                                        if (!cmdDetail.getStandardValue().trim().contains(",")) {
                                            err += MessageUtil.getResourceBundleMessage("label.standardValue") + " "
                                                    + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.between") + "\n";
                                            break;
                                        }

                                        String[] str = cmdDetail.getStandardValue().trim().split(",");
                                        if (str.length != 2) {
                                            err += MessageUtil.getResourceBundleMessage("label.standardValue") + " "
                                                    + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.between") + "\n";
                                            break;
                                        }

                                        try {
                                            System.out.println(Double.parseDouble(str[0]));
                                            System.out.println(Double.parseDouble(str[1]));
                                        } catch (Exception ex) {
                                            logger.error(ex.getMessage(), ex);
                                            err += MessageUtil.getResourceBundleMessage("label.standardValue") + " "
                                                    + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.between") + "\n";
                                            break;
                                        }
                                        break;
                                    case "LIKE":
                                    case "NOT LIKE":
                                        if (isNullOrEmpty(cmdDetail.getStandardValue())) {
                                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                                    MessageUtil.getResourceBundleMessage("label.standardValue")) + "\n";
                                            break;
                                        }
                                        if (!cmdDetail.getStandardValue().trim().contains("%")) {
                                            err += MessageUtil.getResourceBundleMessage("label.standardValue") + " "
                                                    + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.like") + "\n";
                                            break;
                                        }
                                        break;
                                }
                            }
                        }

                        if ("".equals(err)) {//neu khong co loi
                            try {

                                CommandTelnetParser parser = new CommandTelnetParser();

                                getParser(parser, cmdDetail);

                                commandTelnetParserService.saveOrUpdate(parser);

                                cmdDetail.setCommandTelnetParser(parser);
                                cmdDetail.setCreateTime(new Date());
                                cmdDetail.setIsActive(1l);
                                //cmdDetail.setProtocol("TELNET");
                                cmdDetail.setUserName(SessionWrapper.getCurrentUsername());

                                commandDetailService.saveOrUpdate(cmdDetail);

                                List<String> lstParam = getParamList(cmdDetail.getCommandTelnetParser().getCmd());

                                List<ParamInput> lstPr = new ArrayList<>();
                                if (lstParam != null && !lstParam.isEmpty()) {
                                    for (String str : lstParam) {
                                        if (checkValueInListDTO(lstPr, str) == null) {
                                            ParamInput dto = new ParamInput();
                                            dto.setParamCode(str);
                                            dto.setReadOnly(false);
                                            lstPr.add(dto);
                                        }
                                    }

                                    for (ParamInput dto : lstPr) {
                                        if (!checkValueInList(lstParam, dto.getParamCode())) {
                                            lstPr.remove(dto);
                                        }
                                    }
                                }
                                for (ParamInput pr : lstPr) {
                                    pr.setCommandDetail(cmdDetail);
                                    pr.setCreateTime(new Date());
                                    pr.setIsActive(1l);
                                    pr.setParamType(0l);
                                    pr.setUserName(SessionWrapper.getCurrentUsername());
                                }

                                paramInputService.saveOrUpdate(lstPr);

                                cmdDetail.setResultImport("Ok");
                                cmdDetail.setResultImportDetail(MessageUtil.getResourceBundleMessage("common.message.success"));

                            } catch (Exception ex) {

                                logger.error(ex.getMessage(), ex);

                                hasError = true;
                                cmdDetail.setResultImport("NOK");
                                cmdDetail.setResultImportDetail(MessageUtil.getResourceBundleMessage("common.message.fail"));
                            }

                            result.add(cmdDetail);
                        } else {
                            hasError = true;
                            cmdDetail.setResultImport("NOK");
                            cmdDetail.setResultImportDetail(err);
                            result.add(cmdDetail);
                        }
                        logAction = LogUtils.addContent(logAction, "command detail: " + cmdDetail.toString());
                    }
                }
            } else {
                logAction = LogUtils.addContent(logAction, "Result: " + (MessageUtil.getResourceBundleMessage("message.invalid.header")));
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.invalid.header"));
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPORT.name(), logAction);
                return;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("title.importCommand")));
            logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("title.importCommand")));
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPORT.name(), logAction);
            return;
        }

        try {
            String[] header;
            String[] align;
            if (isShowNodeType) {
                header = new String[]{"vendor.vendorName=label.vendorName", "nodeType.typeName=label.nodeTypeName",
                    "version.versionName=label.versionName", "commandName=label.cmdName", "protocol=label.protocol", "commandTypeStr=label.command.type",
                    "commandTelnetParser.cmd=label.cmd", "operator=label.operator", "standardValue=label.standardValue",
                    "commandTelnetParser.cmdEnd=label.cmdEnd", "commandTelnetParser.cmdTimeout=label.cmdTimeout",
                    "commandTelnetParser.rowStart=label.rowStart", "commandTelnetParser.rowEnd=label.rowEnd",
                    "commandTelnetParser.rowOutput=label.rowOutput", "commandTelnetParser.columnOutput=label.columnOutput",
                    "commandTelnetParser.countRow=label.countRow", "commandTelnetParser.splitColumnRegex=label.splitColumnRegex",
                    "commandTelnetParser.regexOutput=label.regexOutput", "commandTelnetParser.pagingCmd=label.pagingCmd",
                    "commandTelnetParser.pagingRegex=label.pagingRegex", "description=label.cmdDescription",
                    "commandTelnetParser.confirmCmd=label.confirmCmd", "commandTelnetParser.confirmRegex=label.confirmRegex",
                    "resultImportDetail=report.result"};

                align = new String[]{
                    "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT",
                    "LEFT", "CENTER", "LEFT", "LEFT", "CENTER",
                    "CENTER", "CENTER", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT"};
            } else {
                header = new String[]{"vendor.vendorName=label.vendorName",
                    "version.versionName=label.versionName", "commandName=label.cmdName", "protocol=label.protocol", "commandTypeStr=label.command.type",
                    "commandTelnetParser.cmd=label.cmd", "operator=label.operator", "standardValue=label.standardValue",
                    "commandTelnetParser.cmdEnd=label.cmdEnd", "commandTelnetParser.cmdTimeout=label.cmdTimeout",
                    "commandTelnetParser.rowStart=label.rowStart", "commandTelnetParser.rowEnd=label.rowEnd",
                    "commandTelnetParser.rowOutput=label.rowOutput", "commandTelnetParser.columnOutput=label.columnOutput",
                    "commandTelnetParser.countRow=label.countRow", "commandTelnetParser.splitColumnRegex=label.splitColumnRegex",
                    "commandTelnetParser.regexOutput=label.regexOutput", "commandTelnetParser.pagingCmd=label.pagingCmd",
                    "commandTelnetParser.pagingRegex=label.pagingRegex", "description=label.cmdDescription",
                    "commandTelnetParser.confirmCmd=label.confirmCmd", "commandTelnetParser.confirmRegex=label.confirmRegex",
                    "resultImportDetail=report.result"};

                align = new String[]{
                    "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT",
                    "LEFT", "CENTER", "LEFT", "LEFT", "CENTER",
                    "CENTER", "CENTER", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT"};
            }

            List<AbstractMap.SimpleEntry<String, String>> headerAlign = CommonExport.buildExportHeader(header, align);

            String fileTemplate = CommonExport.getTemplateExport();

            File fileExport = CommonExport.exportFile(result, headerAlign, "", fileTemplate,
                    "CommandImportResult", 7, "", 4,
                    MessageUtil.getResourceBundleMessage("title.commandImportResult"));

            resultImport = new DefaultStreamedContent(new FileInputStream(fileExport), ".xlsx", fileExport.getName());

            RequestContext.getCurrentInstance().execute("PF('importDialog').hide();");
            RequestContext.getCurrentInstance().execute("PF('resultImportDialog').show();");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("title.importCommand")));
            logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("title.importCommand")));
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPORT.name(), logAction);
    }

    public StreamedContent getImportResult() {
        return resultImport;
    }

    public boolean checkHeader(Row rowHeader) {
        boolean check = true;

        int i = 0;
        String STT = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String vendorName = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String nodeTypeName = "";
        if (isShowNodeType) {
            nodeTypeName = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
            i++;
        }
        String versionName = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String cmdName = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String protocol = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String cmdType = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String cmd = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String operator = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String standardValue = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String cmdEnd = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String cmdTimeout = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String rowStart = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String rowEnd = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String rowOutput = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String columnOutput = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String countRow = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String splitColumnRegex = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String regexOutput = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String pagingCmd = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String pagingRegex = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String confirmCmd = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String confirmRegex = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";
        i++;
        String description = rowHeader.getCell(i) != null && getCellValue(rowHeader.getCell(i)) != null ? getCellValue(rowHeader.getCell(i)).trim().toLowerCase() : "";

        if (!STT.equals(MessageUtil.getResourceBundleMessage("datatable.header.stt").toLowerCase())
                || !vendorName.equals(MessageUtil.getResourceBundleMessage("label.vendorName").toLowerCase())
                || (isShowNodeType && !nodeTypeName.equals(MessageUtil.getResourceBundleMessage("label.nodeTypeName").toLowerCase()))
                || !versionName.equals(MessageUtil.getResourceBundleMessage("label.versionName").toLowerCase())
                || !cmdName.equals(MessageUtil.getResourceBundleMessage("label.cmdName").toLowerCase())
                || !protocol.equals(MessageUtil.getResourceBundleMessage("label.protocol").toLowerCase())
                || !cmdType.equals(MessageUtil.getResourceBundleMessage("label.command.type").toLowerCase())
                || !cmd.equals(MessageUtil.getResourceBundleMessage("label.cmd").toLowerCase())
                || !operator.equals(MessageUtil.getResourceBundleMessage("label.operator").toLowerCase())
                || !standardValue.equals(MessageUtil.getResourceBundleMessage("label.standardValue").toLowerCase())
                || !cmdEnd.equals(MessageUtil.getResourceBundleMessage("label.cmdEnd").toLowerCase())
                || !cmdTimeout.equals(MessageUtil.getResourceBundleMessage("label.cmdTimeout").toLowerCase())
                || !rowStart.equals(MessageUtil.getResourceBundleMessage("label.rowStart").toLowerCase())
                || !rowEnd.equals(MessageUtil.getResourceBundleMessage("label.rowEnd").toLowerCase())
                || !rowOutput.equals(MessageUtil.getResourceBundleMessage("label.rowOutput").toLowerCase())
                || !columnOutput.equals(MessageUtil.getResourceBundleMessage("label.columnOutput").toLowerCase())
                || !countRow.equals(MessageUtil.getResourceBundleMessage("label.countRow").toLowerCase())
                || !splitColumnRegex.equals(MessageUtil.getResourceBundleMessage("label.splitColumnRegex").toLowerCase())
                || !regexOutput.equals(MessageUtil.getResourceBundleMessage("label.regexOutput").toLowerCase())
                || !pagingCmd.equals(MessageUtil.getResourceBundleMessage("label.pagingCmd").toLowerCase())
                || !pagingRegex.equals(MessageUtil.getResourceBundleMessage("label.pagingRegex").toLowerCase())
                || !confirmCmd.equals(MessageUtil.getResourceBundleMessage("label.confirmCmd").toLowerCase())
                || !confirmRegex.equals(MessageUtil.getResourceBundleMessage("label.confirmRegex").toLowerCase())
                || !description.equals(MessageUtil.getResourceBundleMessage("label.cmdDescription").toLowerCase())) {
            check = false;
        }
        return check;
    }

    public String checkRow(Sheet sheet, int i, CommandDetail cmdDetail) {
        String err = "";
        Row row = sheet.getRow(i);
        if (row != null) {
            int j = 0;
            String STT = getCellValue(row.getCell(j));
            j++;
            String vendorName = getCellValue(row.getCell(j));
            j++;
            String nodeTypeName = "";
            if (isShowNodeType) {
                nodeTypeName = getCellValue(row.getCell(j));
                j++;
            }
            String versionName = getCellValue(row.getCell(j));
            j++;
            String cmdName = getCellValue(row.getCell(j));
            j++;
            String protocol = getCellValue(row.getCell(j));
            j++;
            String cmdType = getCellValue(row.getCell(j));
            j++;
            String cmd = getCellValue(row.getCell(j));
            j++;
            String operator = getCellValue(row.getCell(j));
            j++;
            String standardValue = getCellValue(row.getCell(j));
            j++;
            String cmdEnd = getCellValue(row.getCell(j));
            j++;
            String cmdTimeout = getCellValue(row.getCell(j));
            j++;
            String rowStart = getCellValue(row.getCell(j));
            j++;
            String rowEnd = getCellValue(row.getCell(j));
            j++;
            String rowOutput = getCellValue(row.getCell(j));
            j++;
            String columnOutput = getCellValue(row.getCell(j));
            j++;
            String countRow = getCellValue(row.getCell(j));
            j++;
            String splitColumnRegex = getCellValue(row.getCell(j));
            j++;
            String regexOutput = getCellValue(row.getCell(j));
            j++;
            String pagingCmd = getCellValue(row.getCell(j));
            j++;
            String pagingRegex = getCellValue(row.getCell(j));
            j++;
            String confirmCmd = getCellValue(row.getCell(j));
            j++;
            String confirmRegex = getCellValue(row.getCell(j));
            j++;
            String description = getCellValue(row.getCell(j));

            if (!isNullOrEmpty(STT)
                    || !isNullOrEmpty(vendorName)
                    || (isShowNodeType && !isNullOrEmpty(nodeTypeName))
                    || !isNullOrEmpty(versionName)
                    || !isNullOrEmpty(cmdName)
                    || !isNullOrEmpty(protocol)
                    || !isNullOrEmpty(cmdType)
                    || !isNullOrEmpty(cmd)
                    || !isNullOrEmpty(operator)
                    || !isNullOrEmpty(standardValue)
                    || !isNullOrEmpty(cmdEnd)
                    || !isNullOrEmpty(cmdTimeout)
                    || !isNullOrEmpty(rowStart)
                    || !isNullOrEmpty(rowEnd)
                    || !isNullOrEmpty(rowOutput)
                    || !isNullOrEmpty(columnOutput)
                    || !isNullOrEmpty(countRow)
                    || !isNullOrEmpty(splitColumnRegex)
                    || !isNullOrEmpty(regexOutput)
                    || !isNullOrEmpty(pagingCmd)
                    || !isNullOrEmpty(pagingRegex)
                    || !isNullOrEmpty(confirmCmd)
                    || !isNullOrEmpty(confirmRegex)
                    || !isNullOrEmpty(description)) {

                if (isNullOrEmpty(vendorName)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.vendorName")) + "\n";
                } else {
                    cmdDetail.getVendor().setVendorName(vendorName);
                }

                if (isShowNodeType) {
                    if (isNullOrEmpty(nodeTypeName)) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                MessageUtil.getResourceBundleMessage("label.nodeTypeName")) + "\n";
                    } else {
                        cmdDetail.getNodeType().setTypeName(nodeTypeName);
                    }
                }

                if (isNullOrEmpty(versionName)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.versionName")) + "\n";
                } else {
                    cmdDetail.getVersion().setVersionName(versionName);
                }

                if (!isNullOrEmpty(standardValue)) {
                    if (standardValue.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.standardValue"), 500) + "\n";
                    }
                    cmdDetail.setStandardValue(standardValue);
                }

                if (isNullOrEmpty(cmdName)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.cmdName")) + "\n";
                } else {
                    if (cmdName.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.cmdName"), 500) + "\n";
                    }
                    cmdDetail.setCommandName(cmdName);
                }

                if (isNullOrEmpty(protocol)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.protocol")) + "\n";
                } else {
                    cmdDetail.setProtocol(protocol);
                }

                if (isNullOrEmpty(cmdType)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.command.type")) + "\n";
                } else {
                    cmdDetail.setCommandTypeStr(cmdType);
                }

                if (isNullOrEmpty(cmd)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.cmd")) + "\n";
                } else {
                    if (cmd.length() > 4000) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.cmd"), 4000) + "\n";
                    } else {
                        List<String> lstPr = getParamList(cmd);
                        for (String pr : lstPr) {
                            if (!pr.matches("^[0-9a-zA-Z_|.| |-]+$")) {
                                err += MessageUtil.getResourceBundleMessage("message.param.invalid");
                                break;
                            }
                        }
                    }
                    cmdDetail.getCommandTelnetParser().setCmd(cmd);
                }

                if (!isNullOrEmpty(cmdEnd)) {
//                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"), 
//                            MessageUtil.getResourceBundleMessage("label.cmdEnd")) + "\n";
//                } else {
                    if (cmdEnd.length() > 100) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.cmdEnd"), 100) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setCmdEnd(cmdEnd);
                }

                if (isNullOrEmpty(operator)) {
                    err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.operator")) + "\n";
                } else {
                    cmdDetail.setOperator(operator);
                }

                if (!isNullOrEmpty(cmdTimeout)) {
                    if (!isInteger(cmdTimeout)) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.integer"),
                                MessageUtil.getResourceBundleMessage("label.cmdTimeout")) + "\n";
                    } else {
                        if (Long.valueOf(cmdTimeout) <= 0) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.greater"),
                                    MessageUtil.getResourceBundleMessage("label.cmdTimeout"), 0) + "\n";
                        } else if (Long.valueOf(cmdTimeout) > 999) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.lesser"),
                                    MessageUtil.getResourceBundleMessage("label.cmdTimeout"), 1000) + "\n";
                        }
                        cmdDetail.getCommandTelnetParser().setCmdTimeout(Long.valueOf(cmdTimeout));
                    }
                }

                if (!isNullOrEmpty(rowStart)) {
                    if (rowStart.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.rowStart"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setRowStart(rowStart);
                }

                if (!isNullOrEmpty(rowEnd)) {
                    if (rowEnd.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.rowEnd"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setRowEnd(rowEnd);
                }

                if (!isNullOrEmpty(rowOutput)) {
                    if (!isInteger(rowOutput)) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.integer"),
                                MessageUtil.getResourceBundleMessage("label.rowOutput")) + "\n";
                    } else {
                        if (Long.valueOf(rowOutput) <= 0) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.greater"),
                                    MessageUtil.getResourceBundleMessage("label.rowOutput"), 0) + "\n";
                        } else if (Long.valueOf(rowOutput) > 99999) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.lesser"),
                                    MessageUtil.getResourceBundleMessage("label.rowOutput"), 100000) + "\n";
                        }
                        cmdDetail.getCommandTelnetParser().setRowOutput(Long.valueOf(rowOutput));
                    }
                }

                if (!isNullOrEmpty(columnOutput)) {
                    if (!isInteger(columnOutput)) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.integer"),
                                MessageUtil.getResourceBundleMessage("label.columnOutput")) + "\n";
                    } else {
                        if (Long.valueOf(columnOutput) <= 0) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.greater"),
                                    MessageUtil.getResourceBundleMessage("label.columnOutput"), 0) + "\n";
                        } else if (Long.valueOf(columnOutput) > 99999) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.lesser"),
                                    MessageUtil.getResourceBundleMessage("label.columnOutput"), 100000) + "\n";
                        }
                        cmdDetail.getCommandTelnetParser().setColumnOutput(Long.valueOf(columnOutput));
                    }
                }

                if (!isNullOrEmpty(countRow)) {
                    if (!isInteger(countRow)) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.integer"),
                                MessageUtil.getResourceBundleMessage("label.countRow")) + "\n";
                    } else {
                        if (Long.valueOf(countRow) <= 0) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.greater"),
                                    MessageUtil.getResourceBundleMessage("label.countRow"), 0) + "\n";
                        } else if (Long.valueOf(countRow) > 99999) {
                            err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.lesser"),
                                    MessageUtil.getResourceBundleMessage("label.countRow"), 100000) + "\n";
                        }
                        cmdDetail.getCommandTelnetParser().setCountRow(Long.valueOf(countRow));
                    }
                }

                if (!isNullOrEmpty(splitColumnRegex)) {
                    if (splitColumnRegex.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.splitColumnRegex"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setSplitColumnRegex(splitColumnRegex);
                }

                if (!isNullOrEmpty(regexOutput)) {
                    if (regexOutput.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.regexOutput"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setRegexOutput(regexOutput);
                }

                if (!isNullOrEmpty(pagingCmd)) {
                    if (pagingCmd.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.pagingCmd"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setPagingCmd(pagingCmd);
                }

                if (!isNullOrEmpty(pagingRegex)) {
                    if (pagingRegex.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.pagingRegex"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setPagingRegex(pagingRegex);
                }

                if (!isNullOrEmpty(confirmCmd)) {
                    if (confirmCmd.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.confirmCmd"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setConfirmCmd(confirmCmd);
                }

                if (!isNullOrEmpty(confirmRegex)) {
                    if (confirmRegex.length() > 500) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.confirmRegex"), 500) + "\n";
                    }
                    cmdDetail.getCommandTelnetParser().setConfirmRegex(confirmRegex);

                    if (isNullOrEmpty(confirmCmd)) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                MessageUtil.getResourceBundleMessage("label.confirmCmd")) + "\n";
                        cmdDetail.getCommandTelnetParser().setConfirmCmd("");
                    }
                }

                if (!isNullOrEmpty(description)) {
                    if (description.length() > 1000) {
                        err += MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                                MessageUtil.getResourceBundleMessage("label.cmdDescription"), 1000) + "\n";
                    }
                    cmdDetail.setDescription(description);
                }

                cmdDetail.setCommandType(0l);
                cmdDetail.getCommandTelnetParser().setConfirmCmdType(0l);
                cmdDetail.getCommandTelnetParser().setRowStartOperator("CONTAIN");
                cmdDetail.getCommandTelnetParser().setRowEndOperator("CONTAIN");

                return err;
            } else {
                return null;
            }
        }
        return null;
    }

    private boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    private boolean isNullOrEmpty(Object obj) {
        return obj == null;
    }

    private boolean isInteger(String str) {
        try {
            System.out.println(Integer.parseInt(str));
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return false;
    }

    private String getCellValue(Cell cell) {
        String result = "";
        if (cell == null) {
            return result;
        }
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                result = cell.getStringCellValue();
                break;
            case Cell.CELL_TYPE_FORMULA:
                result = cell.getStringCellValue();
                break;

            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    result = cell.getDateCellValue().toString();
                } else {
                    Double number = cell.getNumericCellValue();
                    if (Math.round(number) == number) {
                        result = Long.toString(Math.round(number));
                    } else {
                        result = Double.toString(cell.getNumericCellValue());
                    }
                }
                break;

            case Cell.CELL_TYPE_BLANK:
                result = "";
                break;

            case Cell.CELL_TYPE_BOOLEAN:
                result = Boolean.toString(cell.getBooleanCellValue());
                break;
        }
        return result.trim();
    }

    public StreamedContent onExport() {
        try {
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("createTime", "DESC");
            logAction = LogUtils.addContent("", "Export command");

            Map<String, Object> filters = getFilter();

            List<CommandDetail> lstCmd = commandDetailService.findList(filters, orders);
            logAction = LogUtils.addContent(logAction, "lstCmd size: " + lstCmd.size());

            String[] header;
            String[] align;
            if (isShowNodeType) {
                header = new String[]{"vendor.vendorName=label.vendorName",
                    "nodeType.typeName=label.nodeTypeName", "version.versionName=label.versionName", "protocol=label.protocol", "commandTypeStr=label.command.type",
                    "commandName=label.cmdName", "commandTelnetParser.cmd=label.cmd", "operator=label.operator",
                    "standardValue=label.standardValue", "commandTelnetParser.cmdEnd=label.cmdEnd",
                    "commandTelnetParser.cmdTimeout=label.cmdTimeout",
                    "commandTelnetParser.rowStart=label.rowStart", "commandTelnetParser.rowEnd=label.rowEnd",
                    "commandTelnetParser.rowOutput=label.rowOutput",
                    "commandTelnetParser.columnOutput=label.columnOutput",
                    "commandTelnetParser.countRow=label.countRow",
                    "commandTelnetParser.splitColumnRegex=label.splitColumnRegex",
                    "commandTelnetParser.regexOutput=label.regexOutput",
                    "commandTelnetParser.pagingCmd=label.pagingCmd",
                    "commandTelnetParser.pagingRegex=label.pagingRegex",
                    "commandTelnetParser.confirmCmd=label.confirmCmd", "commandTelnetParser.confirmRegex=label.confirmRegex",
                    "description=label.cmdDescription"};

                align = new String[]{
                    "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT",
                    "LEFT", "CENTER", "LEFT", "LEFT", "CENTER",
                    "CENTER", "CENTER", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT",};
            } else {
                header = new String[]{"vendor.vendorName=label.vendorName",
                    "version.versionName=label.versionName", "protocol=label.protocol", "commandTypeStr=label.command.type",
                    "commandName=label.cmdName", "commandTelnetParser.cmd=label.cmd", "operator=label.operator",
                    "standardValue=label.standardValue", "commandTelnetParser.cmdEnd=label.cmdEnd",
                    "commandTelnetParser.cmdTimeout=label.cmdTimeout",
                    "commandTelnetParser.rowStart=label.rowStart", "commandTelnetParser.rowEnd=label.rowEnd",
                    "commandTelnetParser.rowOutput=label.rowOutput",
                    "commandTelnetParser.columnOutput=label.columnOutput",
                    "commandTelnetParser.countRow=label.countRow",
                    "commandTelnetParser.splitColumnRegex=label.splitColumnRegex",
                    "commandTelnetParser.regexOutput=label.regexOutput",
                    "commandTelnetParser.pagingCmd=label.pagingCmd",
                    "commandTelnetParser.pagingRegex=label.pagingRegex",
                    "commandTelnetParser.confirmCmd=label.confirmCmd", "commandTelnetParser.confirmRegex=label.confirmRegex",
                    "description=label.cmdDescription"};

                align = new String[]{
                    "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT",
                    "LEFT", "CENTER", "LEFT", "LEFT", "CENTER",
                    "CENTER", "CENTER", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT", "LEFT",};
            }

            List<AbstractMap.SimpleEntry<String, String>> headerAlign = CommonExport.buildExportHeader(header, align);

            String fileTemplate = CommonExport.getTemplateExport();

            File fileExport = CommonExport.exportFile(lstCmd, headerAlign, "", fileTemplate,
                    "CommandReport", 7, "", 4,
                    MessageUtil.getResourceBundleMessage("title.commandReport"));
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
            return new DefaultStreamedContent(new FileInputStream(fileExport), ".xlsx", fileExport.getName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("button.export")));
            logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("button.export")));
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
        }
        return null;
    }

    private ParamInput checkValueInListDTO(List<ParamInput> lst, String value) {
        for (ParamInput dto : lst) {
            if (dto.getParamCode().equals(value)) {
                return dto;
            }
        }
        return null;
    }

    private boolean checkValueInList(List<String> lst, String value) {
        for (String str : lst) {
            if (str.equals(value)) {
                return true;
            }
        }
        return false;
    }

    private boolean isModify(CommandDetail oldCommand, CommandDetail currCommand) {
        if (oldCommand == null || currCommand == null) {
            return true;
        }

        if (isNullOrEmpty(oldCommand.getOperator())) {
            if (!isNullOrEmpty(currCommand.getOperator())) {
                return true;
            }
        } else {
            if (isNullOrEmpty(currCommand.getOperator())) {
                return true;
            } else if (!oldCommand.getOperator().trim().equals(currCommand.getOperator().trim())) {
                return true;
            }
        }

        if (isNullOrEmpty(oldCommand.getProtocol())) {
            if (!isNullOrEmpty(currCommand.getProtocol())) {
                return true;
            }
        } else {
            if (isNullOrEmpty(currCommand.getProtocol())) {
                return true;
            } else if (!oldCommand.getProtocol().trim().equals(currCommand.getProtocol().trim())) {
                return true;
            }
        }

        if (isNullOrEmpty(oldCommand.getStandardValue())) {
            if (!isNullOrEmpty(currCommand.getStandardValue())) {
                return true;
            }
        } else {
            if (isNullOrEmpty(currCommand.getStandardValue())) {
                return true;
            } else if (!oldCommand.getStandardValue().trim().equals(currCommand.getStandardValue().trim())) {
                return true;
            }
        }

        if (isNullOrEmpty(oldCommand.getVendor())) {
            if (!isNullOrEmpty(currCommand.getVendor())) {
                return true;
            }
        } else {
            if (isNullOrEmpty(currCommand.getVendor())) {
                return true;
            } else if (!oldCommand.getVendor().equals(currCommand.getVendor())) {
                return true;
            }
        }

        if (isNullOrEmpty(oldCommand.getVersion())) {
            if (!isNullOrEmpty(currCommand.getVersion())) {
                return true;
            }
        } else {
            if (isNullOrEmpty(currCommand.getVersion())) {
                return true;
            } else if (!oldCommand.getVersion().equals(currCommand.getVersion())) {
                return true;
            }
        }

        if (isShowNodeType) {
            if (isNullOrEmpty(oldCommand.getNodeType())) {
                if (!isNullOrEmpty(currCommand.getNodeType())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getNodeType())) {
                    return true;
                } else if (!oldCommand.getNodeType().equals(currCommand.getNodeType())) {
                    return true;
                }
            }
        }

        if ("telnet".equals(currCommand.getProtocol().toLowerCase())
                || "ssh".equals(currCommand.getProtocol().toLowerCase())) {
            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getCmd())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getCmd())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getCmd())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getCmd().trim().equals(currCommand.getCommandTelnetParser().getCmd().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getCmdEnd())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getCmdEnd())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getCmdEnd())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getCmdEnd().trim().equals(currCommand.getCommandTelnetParser().getCmdEnd().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getCmdTimeout())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getCmdTimeout())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getCmdTimeout())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getCmdTimeout().equals(currCommand.getCommandTelnetParser().getCmdTimeout())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getRowStartOperator())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getRowStartOperator())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getRowStartOperator())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getRowStartOperator().trim().equals(currCommand.getCommandTelnetParser().getRowStartOperator().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getRowStart())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getRowStart())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getRowStart())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getRowStart().trim().equals(currCommand.getCommandTelnetParser().getRowStart().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getRowEndOperator())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getRowEndOperator())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getRowEndOperator())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getRowEndOperator().trim().equals(currCommand.getCommandTelnetParser().getRowEndOperator().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getRowEnd())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getRowEnd())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getRowEnd())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getRowEnd().trim().equals(currCommand.getCommandTelnetParser().getRowEnd().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getRowOutput())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getRowOutput())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getRowOutput())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getRowOutput().equals(currCommand.getCommandTelnetParser().getRowOutput())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getColumnOutput())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getColumnOutput())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getColumnOutput())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getColumnOutput().equals(currCommand.getCommandTelnetParser().getColumnOutput())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getCountRow())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getCountRow())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getCountRow())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getCountRow().equals(currCommand.getCommandTelnetParser().getCountRow())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getSplitColumnRegex())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getSplitColumnRegex())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getSplitColumnRegex())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getSplitColumnRegex().trim().equals(currCommand.getCommandTelnetParser().getSplitColumnRegex().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getRegexOutput())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getRegexOutput())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getRegexOutput())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getRegexOutput().trim().equals(currCommand.getCommandTelnetParser().getRegexOutput().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getPagingCmd())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getPagingCmd())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getPagingCmd())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getPagingCmd().trim().equals(currCommand.getCommandTelnetParser().getPagingCmd().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getPagingRegex())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getPagingRegex())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getPagingRegex())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getPagingRegex().trim().equals(currCommand.getCommandTelnetParser().getPagingRegex().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getValueCounter())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getValueCounter())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getValueCounter())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getValueCounter().equals(currCommand.getCommandTelnetParser().getValueCounter())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getValueConvert())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getValueConvert())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getValueConvert())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getValueConvert().equals(currCommand.getCommandTelnetParser().getValueConvert())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getValueDirect())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getValueDirect())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getValueDirect())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getValueDirect().equals(currCommand.getCommandTelnetParser().getValueDirect())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getValueStart())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getValueStart())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getValueStart())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getValueStart().equals(currCommand.getCommandTelnetParser().getValueStart())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getValueEnd())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getValueEnd())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getValueEnd())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getValueEnd().equals(currCommand.getCommandTelnetParser().getValueEnd())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getValueJoinChar())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getValueJoinChar())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getValueJoinChar())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getValueJoinChar().equals(currCommand.getCommandTelnetParser().getValueJoinChar())) {
                    return true;
                }
            }
        } else if ("sql".equals(currCommand.getProtocol().toLowerCase())) {
            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getCmd())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getCmd())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getCmd())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getCmd().trim().equals(currCommand.getCommandTelnetParser().getCmd().trim())) {
                    return true;
                }
            }

            if (isNullOrEmpty(oldCommand.getCommandTelnetParser().getColumnOutput())) {
                if (!isNullOrEmpty(currCommand.getCommandTelnetParser().getColumnOutput())) {
                    return true;
                }
            } else {
                if (isNullOrEmpty(currCommand.getCommandTelnetParser().getColumnOutput())) {
                    return true;
                } else if (!oldCommand.getCommandTelnetParser().getColumnOutput().equals(currCommand.getCommandTelnetParser().getColumnOutput())) {
                    return true;
                }
            }
        }
        return false;
    }

    private void getParser(CommandTelnetParser parser, CommandDetail obj) throws Exception {
        parser.setCmd(obj.getCommandTelnetParser().getCmd() == null ? "" : obj.getCommandTelnetParser().getCmd().trim());
        parser.setCmdEnd(obj.getCommandTelnetParser().getCmdEnd() == null ? "" : obj.getCommandTelnetParser().getCmdEnd().trim());
        parser.setCmdTimeout(obj.getCommandTelnetParser().getCmdTimeout());
        parser.setColumnOutput(obj.getCommandTelnetParser().getColumnOutput());
        parser.setCountRow(obj.getCommandTelnetParser().getCountRow());
        parser.setRowOutput(obj.getCommandTelnetParser().getRowOutput());
        parser.setPagingCmd(obj.getCommandTelnetParser().getPagingCmd() == null ? "" : obj.getCommandTelnetParser().getPagingCmd().trim());
        parser.setPagingRegex(obj.getCommandTelnetParser().getPagingRegex() == null ? "" : obj.getCommandTelnetParser().getPagingRegex().trim());
        parser.setRegexOutput(obj.getCommandTelnetParser().getRegexOutput() == null ? "" : obj.getCommandTelnetParser().getRegexOutput().trim());
        parser.setRowEnd(obj.getCommandTelnetParser().getRowEnd() == null ? "" : obj.getCommandTelnetParser().getRowEnd().trim());
        parser.setRowStart(obj.getCommandTelnetParser().getRowStart() == null ? "" : obj.getCommandTelnetParser().getRowStart().trim());
        parser.setSplitColumnRegex(obj.getCommandTelnetParser().getSplitColumnRegex() == null ? "" : obj.getCommandTelnetParser().getSplitColumnRegex().trim());
        parser.setRowStartOperator(obj.getCommandTelnetParser().getRowStartOperator());
        parser.setConfirmCmdType(obj.getCommandTelnetParser().getConfirmCmdType() == null ? 0l : obj.getCommandTelnetParser().getConfirmCmdType());
        if (obj.getCommandTelnetParser().getConfirmCmdType() != null && obj.getCommandTelnetParser().getConfirmCmdType() == 2L) {
            parser.setConfirmCmd(PassProtector.encrypt(obj.getCommandTelnetParser().getConfirmCmdRoot() == null ? "" : obj.getCommandTelnetParser().getConfirmCmdRoot().trim(), "ipchange"));
        } else {
            parser.setConfirmCmd(obj.getCommandTelnetParser().getConfirmCmd() == null ? "" : obj.getCommandTelnetParser().getConfirmCmd().trim());
        }
        parser.setRowEndOperator(obj.getCommandTelnetParser().getRowEndOperator());
        parser.setConfirmRegex(obj.getCommandTelnetParser().getConfirmRegex() == null ? "" : obj.getCommandTelnetParser().getConfirmRegex().trim());
        parser.setValueCounter(obj.getCommandTelnetParser().getValueCounter() == null ? 0 : obj.getCommandTelnetParser().getValueCounter());
        parser.setValueDirect(obj.getCommandTelnetParser().isIsCounterValue() ? 0l : (obj.getCommandTelnetParser().getValueDirect() == null ? 0 : obj.getCommandTelnetParser().getValueDirect()));
        parser.setValueEnd(obj.getCommandTelnetParser().isIsCounterValue() ? null : obj.getCommandTelnetParser().getValueEnd());
        parser.setValueStart(obj.getCommandTelnetParser().isIsCounterValue() ? null : obj.getCommandTelnetParser().getValueStart());
        parser.setValueJoinChar(obj.getCommandTelnetParser().getValueJoinChar() == null ? "" : obj.getCommandTelnetParser().getValueJoinChar().trim());
        parser.setValueConvert(obj.getCommandTelnetParser().isIsCounterValue() ? 0l : (obj.getCommandTelnetParser().getValueConvert() == null ? 0 : obj.getCommandTelnetParser().getValueConvert()));
        parser.setValueOutResult((!obj.getCommandTelnetParser().isIsValueOutResult()) ? 0l : (obj.getCommandTelnetParser().getValueOutResult() == null ? 0 : obj.getCommandTelnetParser().getValueOutResult()));
        parser.setValueInterval((!obj.getCommandTelnetParser().isIsValueOutResult()) ? "" : (obj.getCommandTelnetParser().getValueInterval() == null ? "" : obj.getCommandTelnetParser().getValueInterval().trim()));
        parser.setValueSequence((!obj.getCommandTelnetParser().isIsValueSequence()) ? 0l : (obj.getCommandTelnetParser().getValueSequence() == null ? 0l : obj.getCommandTelnetParser().getValueSequence()));
        parser.setColumnOutputAdvance(obj.getCommandTelnetParser().getColumnOutputAdvance() == null ? 0 : obj.getCommandTelnetParser().getColumnOutputAdvance());
        parser.setSplitColumnRegexAdvance(obj.getCommandTelnetParser().getSplitColumnRegexAdvance() == null ? "" : obj.getCommandTelnetParser().getSplitColumnRegexAdvance().trim());
        parser.setRemoveDuplicate(obj.getCommandTelnetParser().getRemoveDuplicate() == null ? 0l : obj.getCommandTelnetParser().getRemoveDuplicate());
        parser.setStructureCmd(obj.getCommandTelnetParser().getStructureCmd() == null ? 0l : obj.getCommandTelnetParser().getStructureCmd());
        parser.setFtpIp(obj.getCommandTelnetParser().getFtpIp() == null ? "" : obj.getCommandTelnetParser().getFtpIp());
        parser.setFtpAccount(obj.getCommandTelnetParser().getFtpAccount() == null ? "" : obj.getCommandTelnetParser().getFtpAccount());
        parser.setFtpPassword(obj.getCommandTelnetParser().getFtpPassword() == null ? "" : PassProtector.encrypt(obj.getCommandTelnetParser().getFtpPassword(), Config.SALT));
        parser.setFtpDestination(obj.getCommandTelnetParser().getFtpDestination() == null ? "" : obj.getCommandTelnetParser().getFtpDestination());
        parser.setCmdEndOrder(obj.getCommandTelnetParser().getCmdEndOrder() == null ? "" : obj.getCommandTelnetParser().getCmdEndOrder().trim());
    }

    private boolean validateInput(boolean isEdit) {
        if ((vendorSelected == null && isEdit) || (!isEdit && (vendorSeleteds == null || vendorSeleteds.isEmpty()))) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.vendorName")));
            return false;
        }

        if ((nodeTypeSelected == null && isEdit) || (!isEdit && (nodeTypeSelecteds == null || nodeTypeSelecteds.isEmpty()))) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.nodeTypeName")));
            return false;
        }

        if ((versionSelected == null && isEdit) || (!isEdit && (vendorSeleteds == null || vendorSeleteds.isEmpty()))) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.versionName")));
            return false;
        }

        if (obj.getCommandType() == null || obj.getCommandType() == -1) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.command.type")));
            return false;
        }

        if (opeSelected == null) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.operator")));
            return false;
        }

        if (obj.getCommandName() == null || obj.getCommandName().trim().isEmpty()) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.cmdName")));
            return false;
        }

        if (obj.getCommandName().length() > 500) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                    MessageUtil.getResourceBundleMessage("label.cmdName"), 500));
            return false;
        }

        if (protocolSelected == null) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.protocol")));
            return false;
        }

        if (obj.getCommandTelnetParser().getCmd() == null || obj.getCommandTelnetParser().getCmd().trim().isEmpty()) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                    MessageUtil.getResourceBundleMessage("label.cmd")));
            return false;
        }

        if (obj.getCommandTelnetParser().getCmd().length() > 4000) {
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                    MessageUtil.getResourceBundleMessage("label.cmd"), 4000));
            return false;
        }

        List<String> lstPr = getParamList(obj.getCommandTelnetParser().getCmd());
        lstPr.addAll(getParamList(obj.getCommandTelnetParser().getRegexOutput()));
        for (String pr : lstPr) {
            if (!pr.matches("^[0-9a-zA-Z_|.| |-]+$")) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.param.invalid"));
                return false;
            }
        }

        if (Config.PROTOCOL_TELNET.equals(protocolSelected.getValue())
                || Config.PROTOCOL_SSH.equals(protocolSelected.getValue())) {
            if (!isNullOrEmpty(obj.getCommandTelnetParser().getCmdEnd()) && obj.getCommandTelnetParser().getCmdEnd().length() > 100) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                        MessageUtil.getResourceBundleMessage("label.cmdEnd"), 100));
                return false;
            }
        }

        switch (opeSelected.getValue().toUpperCase()) {
            case "IN":
            case "NOT IN":
            case "=":
            case "<":
            case ">":
            case "<=":
            case ">=":
            case "<>":
                if (isNullOrEmpty(obj.getStandardValue())) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.standardValue")));
                    return false;
                }
                break;
            case "CONTAIN":
            case "IS NULL OR CONTAIN":
            case "NOT CONTAIN":
                if (isNullOrEmpty(obj.getStandardValue())) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.standardValue")));
                    return false;
                }
                break;
            case "BETWEEN":
                if (isNullOrEmpty(obj.getStandardValue())) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.standardValue")));
                    return false;
                }

                if (!obj.getStandardValue().trim().contains(",")) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("label.standardValue")
                            + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.between"));
                    return false;
                }

                String[] str = obj.getStandardValue().trim().split(",");
                if (str.length != 2) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("label.standardValue")
                            + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.between"));
                    return false;
                }

                try {
                    System.out.println(Double.parseDouble(str[0]));
                    System.out.println(Double.parseDouble(str[1]));
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("label.standardValue")
                            + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.between"));
                    return false;
                }
                break;
            case "LIKE":
            case "NOT LIKE":
                if (isNullOrEmpty(obj.getStandardValue())) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.standardValue")));
                    return false;
                }
                if (!obj.getStandardValue().trim().contains("%")) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("label.standardValue")
                            + MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.like"));
                    return false;
                }
                break;
        }

        return true;
    }

    private void clear() {
        obj = new CommandDetail();

        if (vendorSeleteds != null) {
            vendorSeleteds.clear();
        }
        if (nodeTypeSelecteds != null) {
            nodeTypeSelecteds.clear();
        }
        if (versionSelecteds != null) {
            versionSelecteds.clear();
        }
        vendorSelected = null;
        nodeTypeSelected = null;
        versionSelected = null;
        opeSelected = null;
        params.clear();

        standardValuePrompt = "";
        standardValueReadonly = false;
        standardValueRequired = false;
    }

    public void prepareEdit(CommandDetail vCmd) {
        logAction = LogUtils.addContent("", "Prepare Edit command");
        try {
            clear();
            if (vCmd == null) {
                return;
            }
            obj = vCmd;
            //quytv7_20170913 them moi giao thuc ftp_start
            try {
                if (obj.getCommandTelnetParser().getFtpPassword() != null && !"".equals(obj.getCommandTelnetParser().getFtpPassword())) {
                    obj.getCommandTelnetParser().setFtpPassword(PassProtector.decrypt(obj.getCommandTelnetParser().getFtpPassword(), Config.SALT));
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
            //quytv7_20170913 them moi giao thuc ftp_end

            logAction = LogUtils.addContent(logAction, "Command Old: " + obj.toString());

            oldObj = (CommandDetail) BeanUtils.cloneBean(vCmd);//new Cloner().deepClone(vCmd);
            oldObj.setCommandTelnetParser((CommandTelnetParser) BeanUtils.cloneBean(vCmd.getCommandTelnetParser()));
            oldObj.setVendor((Vendor) BeanUtils.cloneBean(vCmd.getVendor()));
            oldObj.setVersion((Version) BeanUtils.cloneBean(vCmd.getVersion()));
            oldObj.setNodeType((NodeType) BeanUtils.cloneBean(vCmd.getNodeType()));
            isEdit = true;
            isClone = false;
            vendorSelected = vCmd.getVendor();
            nodeTypeSelected = vCmd.getNodeType();
            versionSelected = vCmd.getVersion();
            opeSelected = new ComboBoxObject(vCmd.getOperator(), vCmd.getOperator());
            protocolSelected = new ComboBoxObject(vCmd.getProtocol(), vCmd.getProtocol());
            //Quytv7 Set lai command
            if (obj.getCommandTelnetParser().getConfirmCmdType() != null && obj.getCommandTelnetParser().getConfirmCmdType().equals(0L)) {
                obj.getCommandTelnetParser().setConfirmCmdRoot("");
            } else if (obj.getCommandTelnetParser().getConfirmCmdType() != null && obj.getCommandTelnetParser().getConfirmCmdType().equals(2L)) {
                obj.getCommandTelnetParser().setConfirmCmdRoot((obj.getCommandTelnetParser().getConfirmCmd() == null || "".equals(obj.getCommandTelnetParser().getConfirmCmd())) ? " " : PassProtector.decrypt(obj.getCommandTelnetParser().getConfirmCmd().trim(), "ipchange"));
                obj.getCommandTelnetParser().setConfirmCmd("");
            }

            params = new ArrayList(vCmd.getParamInputs());
//            for (ParamInput paramInput : params) {
//                if (paramInput.getIsParamKey() != null && paramInput.getIsParamKey().equals(1L)) {
//                    obj.setParammKey(paramInput.getParamCode());
//                    break;
//                }
//            }

//            Collections.sort(params, new Comparator<ParamInput>() {
//                @Override
//                public int compare(ParamInput a, ParamInput b) {
//                    return a.getParamCode().compareTo(b.getParamCode());
//                }
//            });
            onChangeOperator(vCmd.getOperator());

            logAction = LogUtils.addContent(logAction, "Result: Sucsses ");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
            dialogHeader = MessageUtil.getResourceBundleMessage("title.updateCommand");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logAction = LogUtils.addContent(logAction, "Result Fail :" + e.getMessage());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
        }
    }

    public void prepareDelete(CommandDetail vCmd) {
        try {
            logAction = LogUtils.addContent("", "Prepare Delete command");

            obj = vCmd;
            logAction = LogUtils.addContent(logAction, "Command delete: " + obj.toString());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
        } catch (Exception e) {
            logAction = LogUtils.addContent(logAction, "Result fail: " + e.getMessage());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
            logger.error(e.getMessage(), e);
        }
    }

    public void prepareClone(CommandDetail vCmd) {
        logAction = LogUtils.addContent("", "Prepare Clone command");
        try {
            clear();

            if (vCmd == null) {
                return;
            }
            obj = vCmd;
            //quytv7_20170913 them moi giao thuc ftp_start
            try {
                if (obj.getCommandTelnetParser().getFtpPassword() != null && !"".equals(obj.getCommandTelnetParser().getFtpPassword())) {
                    obj.getCommandTelnetParser().setFtpPassword(PassProtector.decrypt(obj.getCommandTelnetParser().getFtpPassword(), Config.SALT));
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
            //quytv7_20170913 them moi giao thuc ftp_end
            logAction = LogUtils.addContent(logAction, "Command Old: " + obj.toString());
            isEdit = true;
            isClone = true;

            vendorSelected = vCmd.getVendor();
            nodeTypeSelected = vCmd.getNodeType();
            versionSelected = vCmd.getVersion();
            opeSelected = new ComboBoxObject(vCmd.getOperator(), vCmd.getOperator());
            protocolSelected = new ComboBoxObject(vCmd.getProtocol(), vCmd.getProtocol());

            params = new ArrayList(vCmd.getParamInputs());
//            for (ParamInput paramInput : params) {
//                if (paramInput.getIsParamKey() != null && paramInput.getIsParamKey().equals(1L)) {
//                    obj.setParammKey(paramInput.getParamCode());
//                    break;
//                }
//            }

//            Collections.sort(params, new Comparator<ParamInput>() {
//                @Override
//                public int compare(ParamInput a, ParamInput b) {
//                    return a.getParamCode().compareTo(b.getParamCode());
//                }
//            });
            onChangeOperator(vCmd.getOperator());

            logAction = LogUtils.addContent(logAction, "Result success");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
            dialogHeader = MessageUtil.getResourceBundleMessage("title.cloneCommand");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logAction = LogUtils.addContent(logAction, "Result fail " + e.getMessage());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
        }
    }

    public void prepareViewTemplate(CommandDetail vCmd) {
        logAction = LogUtils.addContent("", "Prepare View template");
        logAction = LogUtils.addContent(logAction, "Template id: " + templates);

        templates = new ArrayList<>();
        for (ActionCommand acd : vCmd.getActionCommands()) {
            for (ActionOfFlow aofw : acd.getActionDetail().getAction().getActionOfFlows()) {
                if (!templates.contains(aofw.getFlowTemplates())) {
                    templates.add(aofw.getFlowTemplates());
                    logAction = LogUtils.addContent(logAction, "Template id: " + aofw.getFlowTemplates().getFlowTemplatesId());
                    logAction = LogUtils.addContent(logAction, "Template name: " + aofw.getFlowTemplates().getFlowTemplateName());
                }
            }
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
    }

    public void prepareCheck(CommandDetail vCmd) {
        logAction = LogUtils.addContent("", "Prepare check command");
        commandSend = new CommandObject();
        try {
            commandSend.setCmd(vCmd.getCommandTelnetParser().getCmd());
            commandSend.setCmdSend(vCmd.getCommandTelnetParser().getCmd());
            commandSend.setCommandDetail(vCmd);
            params = new ArrayList(vCmd.getParamInputs());
            for (ParamInput pr : params) {
                pr.setParamValue("");
            }

            commandSend.setFilterNodeTypeId(vCmd.getNodeType().getTypeId());
            commandSend.setFilterVendorId(vCmd.getVendor().getVendorId());
            commandSend.setFilterVersionId(vCmd.getVersion().getVersionId());
            //Quytv7 decrype passs
            try {
                if (commandSend.getCommandDetail().getCommandTelnetParser().getFtpPassword() != null
                        && !"".equals(commandSend.getCommandDetail().getCommandTelnetParser().getFtpPassword())) {
                    commandSend.getCommandDetail().getCommandTelnetParser().setFtpPassword(PassProtector.decrypt(commandSend.getCommandDetail().getCommandTelnetParser().getFtpPassword(), Config.SALT));
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
            logAction = LogUtils.addContent(logAction, "Command Old: " + obj.toString());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logAction = LogUtils.addContent(logAction, "Result fail: " + e.getMessage());
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    private String calculateParam(Map<String, ParamInput> mapParams, Map<String, String> mapValues, ParamInput paramInput) {
        if (paramInput.getIsFormula()) {
            if (mapValues.containsKey(paramInput.getParamCode())) {
                return mapValues.get(paramInput.getParamCode());
            } else {
                if (paramInput.getParamFormula().contains("-->")) {
                    List<String> array = getParamList(paramInput.getParamFormula());

                    Map<String, String> mapVar = new HashMap<>();
                    for (String pr : array) {
                        if (mapParams.containsKey(pr)) {
                            if (mapValues.containsKey(pr)) {
                                mapVar.put(pr, mapValues.get(pr).trim());
                            } else {
                                String val = calculateParam(mapParams, mapValues, mapParams.get(pr));
                                mapVar.put(pr, val);
                            }
                        }
                    }

                    String[] ifConditions = paramInput.getParamFormula().split(";");
                    String value = null;
                    for (String condition : ifConditions) {
                        try {
                            if (condition.contains("-->")) {
                                String[] arr = condition.split("-->");
                                if (arr != null && arr.length == 2) {
                                    String ifBody = arr[0].trim();
                                    String ifThen = arr[1].trim();

                                    for (String var : mapVar.keySet()) {
                                        ifBody = ifBody.replace("@{" + var + "}", String.valueOf(mapVar.get(var)));
                                        ifThen = ifThen.replace("@{" + var + "}", String.valueOf(mapVar.get(var)));
                                    }

//                                    ScriptEngineManager factory = new ScriptEngineManager();
//                                    ScriptEngine engine = factory.getEngineByName("JavaScript");
                                    Boolean ifValue = Boolean.parseBoolean((new EvalUtil()).evalScript(ifBody));
                                    if (ifValue) {
                                        value = (new EvalUtil()).evalScript(ifThen).replaceAll("\\.0+$", "");
                                        break;
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                        }
                    }
                    if (value == null) {
                        for (String condition : ifConditions) {
                            if (condition.toLowerCase().contains("else") && !condition.contains("-->")) {
                                for (String var : mapVar.keySet()) {
                                    condition = condition.replace("@{" + var + "}", String.valueOf(mapVar.get(var)));
                                }
                                value = condition.replace("else", "").trim();
                                break;
                            }
                        }
                    }

                    if (value != null) {
                        paramInput.setParamValue(value);
                        mapValues.put(paramInput.getParamCode(), value);
                        return value;
                    } else {
                        paramInput.setParamValue(null);
                        mapValues.put(paramInput.getParamCode(), null);
                        return null;
                    }
                } else {
                    List<String> array = getParamList(paramInput.getParamFormula());

                    Map<String, Double> mapVar = new HashMap<>();
                    for (String pr : array) {
                        if (mapParams.containsKey(pr)) {
                            if (mapValues.containsKey(pr)) {
                                try {
                                    mapVar.put(pr, Double.parseDouble(mapValues.get(pr).trim()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            } else {
                                String val = calculateParam(mapParams, mapValues, mapParams.get(pr));
                                try {
                                    mapVar.put(pr, Double.parseDouble(val));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                        }
                    }

                    Double vl = getExpressValue(paramInput.getParamFormula(), mapVar);
                    if (vl != null) {
                        paramInput.setParamValue(String.valueOf(vl.longValue()));
                        mapValues.put(paramInput.getParamCode(), String.valueOf(vl.longValue()));
                        return String.valueOf(vl.longValue());
                    } else {
                        paramInput.setParamValue(null);
                        mapValues.put(paramInput.getParamCode(), null);
                        return null;
                    }
                }
            }
        }
        return paramInput.getParamValue();
    }

    public void updateParam() {
        Map<String, String> mapValues = new HashMap<>();
        Map<String, ParamInput> mapParams = new HashMap<>();

        for (ParamInput paramInput : params) {
            mapParams.put(paramInput.getParamCode(), paramInput);
        }

        for (ParamInput paramInput : params) {
            calculateParam(mapParams, mapValues, paramInput);
        }
    }

    public String buildCommand() {
        if (commandSend == null) {
            return "";
        }
        //20171309_Quytv7_Add them giao thuc FTP_Start
        try {
            if (commandSend.getCommandDetail().getProtocol().equals(Config.PROTOCOL_FTP)) {
//                logger.info("set lai ftpPathFile, filename cho lenh FTP");
                for (ParamInput paramInput : params) {
                    if (paramInput.getParamCode().equalsIgnoreCase(Config.CmdProtocolFtp_ftpPathFile)) {
                        if (paramInput.getParamValue() != null && !paramInput.getParamValue().trim().isEmpty()) {
                            commandSend.getCommandDetail().getCommandTelnetParser().setFtpPathFile(paramInput.getParamValue());
                        }
                    }
                    if (paramInput.getParamCode().equalsIgnoreCase(Config.CmdProtocolFtp_ftpFileName)) {
                        if (paramInput.getParamValue() != null && !paramInput.getParamValue().trim().isEmpty()) {
                            commandSend.getCommandDetail().getCommandTelnetParser().setFtpFileName(paramInput.getParamValue());
                        }
                    }
                    if (paramInput.getParamCode().equalsIgnoreCase(Config.CmdProtocolFtp_destination)) {
                        if (paramInput.getParamValue() != null && !paramInput.getParamValue().trim().isEmpty()) {
                            commandSend.getCommandDetail().getCommandTelnetParser().setFtpDestination(paramInput.getParamValue());
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        //20171309_Quytv7_Add them giao thuc FTP_End

        String cmdFormat = StringEscapeUtils.escapeHtml4(commandSend.getCmd());
        String cmd = commandSend.getCmd();
        try {
            for (ParamInput paramInput : params) {
                String value = "";
                if (paramInput.getParamValue() != null && !paramInput.getParamValue().trim().isEmpty()) {
                    value = paramInput.getParamValue();
                }
                cmd = cmd.replace("@{" + paramInput.getParamCode() + "}", value);
                cmdFormat = cmdFormat.replace("@{" + paramInput.getParamCode() + "}",
                        "<b style=\"background: yellow\">" + StringEscapeUtils.escapeHtml4(value) + "</b>");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        commandSend.setCmdSend(cmd);
        return cmdFormat;
    }

    private Double getExpressValue(String expressStr, Map<String, Double> mapVar) {
        try {
            for (String var : mapVar.keySet()) {
                expressStr = expressStr.replace("@{" + var + "}", String.valueOf(mapVar.get(var)));
            }
            Expression e = new ExpressionBuilder(expressStr).build();

            return e.evaluate();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return null;
        }
    }

    private String buildRegexOutput(String regexOutput) {
        if (regexOutput == null || regexOutput.isEmpty()) {
            return regexOutput;
        }
        for (ParamInput param : params) {
            regexOutput = regexOutput.replace("@{" + param.getParamCode() + "}", param.getParamValue());
        }
        return regexOutput;
    }

    public void checkCommand() {
        logAction = LogUtils.addContent("", "Check command");
        try {
            boolean isValid = true;
            logAction = LogUtils.addContent(logAction, "Command send: " + commandSend.toString());
            commandSend.setResult("");
            commandSend.setResultDetail("");
            commandSend.setResultFinal("");

            if (commandSend.getCommandDetail().getProtocol() != null && !commandSend.getCommandDetail().getProtocol().equals(Config.PROTOCOL_FTP)) {
                if (commandSend.getNodeRun() == null) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.node.run")));
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.node.run")));
                    isValid = false;
                }
//Quytv7_01092017_Lay account tu bang node mang_Start
                if (commandSend.getAccount() == null || commandSend.getAccount().trim().isEmpty()) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.account")));
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.account")));
                    isValid = false;
                }

                if (commandSend.getPassword() == null || commandSend.getPassword().trim().isEmpty()) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.pass")));
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("label.pass")));
                    isValid = false;
                }
            }
            //Quytv7_01092017_Lay account tu bang node mang end

//            for (ParamInput pr : params) {
//                if (pr.getParamValue() == null || pr.getParamValue().trim().isEmpty()) {
//                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
//                            pr.getParamCode()));
//                    isValid = false;
//                }
//            }
            logger.info("commandSend.getCmdSend(): " + commandSend.getCmdSend());

            if (!isValid) {

                return;
            }

            String prompt = commandSend.getCommandDetail().getCommandTelnetParser().getCmdEnd();

            List<String> paramValue = new ArrayList<>();
            List<String> paramValueNoDistinct = new ArrayList<>();
            int timeout = commandSend.getCommandDetail().getCommandTelnetParser().getCmdTimeout() == null
                    || commandSend.getCommandDetail().getCommandTelnetParser().getCmdTimeout() <= 0
                            ? 30000 : commandSend.getCommandDetail().getCommandTelnetParser().getCmdTimeout().intValue() * 1000;
            String resultDetail;
            switch (commandSend.getCommandDetail().getProtocol().toUpperCase()) {
                case Config.PROTOCOL_TELNET:
                    resultDetail = getValueTelnet(
                            //Quytv7_01092017_Lay account tu bang node mang_Start
                            commandSend.getAccount(),
                            commandSend.getPassword(),
                            //                            PassProtector.decrypt(commandSend.getNodeRun().getAccount(), "ipchange"),
                            //                            PassProtector.decrypt(commandSend.getNodeRun().getPassword(), "ipchange"),
                            //Quytv7_01092017_Lay account tu bang node mang end
                            commandSend.getNodeRun().getEffectIp(),
                            commandSend.getNodeRun().getPort() == null ? 23 : commandSend.getNodeRun().getPort(), prompt, "exit", "VT100", commandSend.getCmdSend(), timeout,
                            commandSend.getCommandDetail().getCommandTelnetParser().getCmdEnd(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getPagingCmd(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getPagingRegex(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getConfirmCmd(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getConfirmCmdType(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getConfirmRegex(),
                            commandSend.getNodeRun().getVendor().getVendorName(), commandSend.getNodeRun().getOsType(),
                            commandSend.getNodeRun().getVersion().getVersionName(),
                            commandSend.getNodeRun().getNodeType().getTypeName(), commandSend.getNodeRun().getNodeCode(),
                            commandSend.getMeid(), commandSend.getNodeRun().getFormRun(), commandSend.getNodeRun().getNodeIp(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getCmdEndOrder()
                    );

                    commandSend.setResultDetail(resultDetail);

                    String regexOutput = buildRegexOutput(commandSend.getCommandDetail().getCommandTelnetParser().getRegexOutput());
                    String rowStart = buildRegexOutput(commandSend.getCommandDetail().getCommandTelnetParser().getRowStart());
                    String rowEnd = buildRegexOutput(commandSend.getCommandDetail().getCommandTelnetParser().getRowEnd());

                    getValueRowColumnIndex(paramValue, paramValueNoDistinct, resultDetail,
                            commandSend.getCommandDetail().getCommandTelnetParser().getRowOutput() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getRowOutput().intValue(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutput() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutput().intValue(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getCountRow() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getCountRow().intValue(),
                            rowStart,
                            commandSend.getCommandDetail().getCommandTelnetParser().getRowStartOperator(),
                            rowEnd,
                            commandSend.getCommandDetail().getCommandTelnetParser().getRowEndOperator(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getSplitColumnRegex(),
                            regexOutput, commandSend.getCommandDetail().getCommandTelnetParser().getRemoveDuplicate());

                    paramValue = getAdvancedValue(paramValue, paramValueNoDistinct,
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueCounter(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueStart(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueEnd(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueDirect(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueJoinChar(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueConvert(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueOutResult(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueInterval(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueSequence(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getSplitColumnRegexAdvance(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutputAdvance() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutputAdvance().intValue()
                    );
                    break;
                case Config.PROTOCOL_SSH:
                    resultDetail = getValueSsh(commandSend.getAccount(), commandSend.getPassword(),
                            commandSend.getNodeRun().getEffectIp(),
                            commandSend.getNodeRun().getPortSsh() == null ? 22 : commandSend.getNodeRun().getPortSsh(), commandSend.getCmdSend(), timeout, prompt,
                            commandSend.getCommandDetail().getCommandTelnetParser().getPagingRegex(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getPagingCmd(),
                            commandSend.getNodeRun().getVendor().getVendorName(),
                            commandSend.getNodeRun().getNodeType().getTypeName(),
                            commandSend.getNodeRun().getVersion().getVersionName(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getConfirmCmdType(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getConfirmCmd(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getConfirmRegex()
                    );

                    commandSend.setResultDetail(resultDetail);

                    regexOutput = buildRegexOutput(commandSend.getCommandDetail().getCommandTelnetParser().getRegexOutput());
                    rowStart = buildRegexOutput(commandSend.getCommandDetail().getCommandTelnetParser().getRowStart());
                    rowEnd = buildRegexOutput(commandSend.getCommandDetail().getCommandTelnetParser().getRowEnd());

                    getValueRowColumnIndex(paramValue, paramValueNoDistinct, resultDetail,
                            commandSend.getCommandDetail().getCommandTelnetParser().getRowOutput() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getRowOutput().intValue(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutput() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutput().intValue(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getCountRow() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getCountRow().intValue(),
                            rowStart,
                            commandSend.getCommandDetail().getCommandTelnetParser().getRowStartOperator(),
                            rowEnd,
                            commandSend.getCommandDetail().getCommandTelnetParser().getRowEndOperator(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getSplitColumnRegex(),
                            regexOutput, commandSend.getCommandDetail().getCommandTelnetParser().getRemoveDuplicate());

                    paramValue = getAdvancedValue(paramValue, paramValueNoDistinct,
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueCounter(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueStart(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueEnd(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueDirect(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueJoinChar(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueConvert(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueOutResult(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueInterval(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getValueSequence(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getSplitColumnRegexAdvance(),
                            commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutputAdvance() == null ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutputAdvance().intValue()
                    );
                    break;
                case Config.PROTOCOL_SQL:
                    try {
                        paramValue = getValueSql(commandSend.getAccount(), commandSend.getPassword(), commandSend.getNodeRun().getJdbcUrl(), commandSend.getCommandDetail().getProtocol(), commandSend.getCmdSend(),
                                commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutput() == null
                                        ? 0 : commandSend.getCommandDetail().getCommandTelnetParser().getColumnOutput().intValue(), params);
                    } catch (Exception ex) {
                        commandSend.setResultDetail(ex.getMessage());
                        throw ex;
                    }
                    break;
                case Config.PROTOCOL_EXCHANGE:
                    break;
                case Config.PROTOCOL_FTP:
                    try {

                        commandSend.setResultDetail(getResultFtp(commandSend.getCommandDetail().getCommandTelnetParser().getFtpAccount(),
                                commandSend.getCommandDetail().getCommandTelnetParser().getFtpPassword(),
                                commandSend.getCommandDetail().getCommandTelnetParser().getFtpIp(),
                                22,
                                timeout,
                                prompt,
                                commandSend.getCommandDetail().getVendor().getVendorName(),
                                commandSend.getCommandDetail().getNodeType().getTypeName(),
                                commandSend.getCommandDetail().getVersion().getVersionName(),
                                commandSend.getCommandDetail().getCommandTelnetParser().getFtpFileName(),
                                commandSend.getCommandDetail().getCommandTelnetParser().getFtpPathFile(),
                                commandSend.getCommandDetail().getCommandTelnetParser().getFtpDestination() + File.separator
                                + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
                        ));
                        paramValue.add(commandSend.getResultDetail());
                    } catch (Exception ex) {
                        commandSend.setResultDetail(ex.getMessage());
                        throw ex;
                    }
                    break;
            }

            commandSend.setResult(StringUtils.join(paramValue, "\n"));
            String standardValue = buildRegexOutput(commandSend.getCommandDetail().getStandardValue());
            boolean resultFinal = getValue(paramValue, standardValue,
                    commandSend.getCommandDetail().getOperator());
            commandSend.setResultFinal(resultFinal ? "TRUE" : "FALSE");
            logAction = LogUtils.addContent(logAction, "Result: " + commandSend.getResult());
            logAction = LogUtils.addContent(logAction, "Result final: " + commandSend.getResultFinal());
            logAction = LogUtils.addContent(logAction, "Result detail: " + commandSend.getResultDetail());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            if ("CONN_TIMEOUT".equals(e.getMessage())) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.login.fail"));
                logAction = LogUtils.addContent(logAction, "Result: Login fail");
            } else if ("LOGIN_FAIL".equals(e.getMessage())) {
                logAction = LogUtils.addContent(logAction, "Result: Connect fail");
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.connect.fail"));
            } else {
                logAction = LogUtils.addContent(logAction, "Result: Run fail");
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("message.error.run"));
            }
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public static void main(String[] args) {
        List<String> paramValue = Arrays.asList(new String[]{"22", "34", "76", "45", "20"});
        paramValue = new CommandController().getAdvancedValue(paramValue, paramValue, 0l, 1l, 2l, 3l, null, 2l, 1l, "0-1000", 0l, null, 0);
        System.out.println(StringUtils.join(paramValue, "\n"));
    }

    //Quytv7 eidt account/pass tu DB
    public void onChangeNodeCheckCmd() {
        try {
            if (commandSend.getNodeRun().getAccount() != null && !"".equals(commandSend.getNodeRun().getAccount())) {
                commandSend.setAccount(PassProtector.decrypt(commandSend.getNodeRun().getAccount(), Config.SALT));
            }
            if (commandSend.getNodeRun().getPassword() != null && !"".equals(commandSend.getNodeRun().getPassword())) {
                commandSend.setPassword(PassProtector.decrypt(commandSend.getNodeRun().getPassword(), Config.SALT));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private boolean getValue(List<String> values, String compeValue, String comOper) {
        String value = null;
        if (values != null && values.size() > 0) {
            value = values.get(0);
        }
        boolean flag;
        switch (comOper.toLowerCase()) {
            case "no check":
                return true;
            case ">":
                if (values == null) {
                    return false;
                }
                for (String vl : values) {
                    if (vl == null) {
                        return false;
                    }
                    if (NumberUtils.isNumber(vl) && NumberUtils.isNumber(compeValue)) {
                        flag = Double.parseDouble(vl) > Double.parseDouble(compeValue);
                    } else {
                        flag = vl.compareTo(compeValue) > 0;
                    }
                    if (!flag) {
                        return false;
                    }
                }
                return true;
            case ">=":
                if (values == null) {
                    return false;
                }
                for (String vl : values) {
                    if (vl == null) {
                        return false;
                    }
                    if (NumberUtils.isNumber(vl) && NumberUtils.isNumber(compeValue)) {
                        flag = Double.parseDouble(vl) >= Double.parseDouble(compeValue);
                    } else {
                        flag = vl.compareTo(compeValue) >= 0;
                    }
                    if (!flag) {
                        return false;
                    }
                }
                return true;
            case "<":
                if (values == null) {
                    return false;
                }
                for (String vl : values) {
                    if (vl == null) {
                        return false;
                    }
                    if (NumberUtils.isNumber(vl) && NumberUtils.isNumber(compeValue)) {
                        flag = Double.parseDouble(vl) < Double.parseDouble(compeValue);
                    } else {
                        flag = vl.compareTo(compeValue) < 0;
                    }
                    if (!flag) {
                        return false;
                    }
                }
                return true;
            case "<=":
                if (values == null) {
                    return false;
                }
                for (String vl : values) {
                    if (vl == null) {
                        return false;
                    }
                    if (NumberUtils.isNumber(vl) && NumberUtils.isNumber(compeValue)) {
                        flag = Double.parseDouble(vl) <= Double.parseDouble(compeValue);
                    } else {
                        flag = vl.compareTo(compeValue) <= 0;
                    }
                    if (!flag) {
                        return false;
                    }
                }
                return true;
            case "=":
                if (values == null) {
                    return false;
                }
                for (String vl : values) {
                    if (!vl.equals(compeValue)) {
                        return false;
                    }
                }
                return true;
            case "<>":
                if (values == null) {
                    return false;
                }
                for (String vl : values) {
                    if (vl.equals(compeValue)) {
                        return false;
                    }
                }
                return true;
            case "is null":
                return isNullOrEmpty(value);
            case "not null":
                return !isNullOrEmpty(value);
            case "in":
                if (value == null || compeValue == null) {
                    return false;
                }
                String[] strArr = compeValue.split(",");
                for (String str : strArr) {
                    if (value.equals(str)) {
                        return true;
                    }
                }
                break;
            case "between":
                if (value == null || compeValue == null) {
                    return false;
                }
                String[] strArr2 = compeValue.split(",");

                if (strArr2 != null && strArr2.length > 1) {
                    String below = strArr2[0];
                    String behind = strArr2[1];

                    if (NumberUtils.isNumber(value) && NumberUtils.isNumber(below)
                            && NumberUtils.isNumber(behind)) {
                        return Double.parseDouble(value) <= Double.parseDouble(behind)
                                && Double.parseDouble(value) >= Double.parseDouble(below);
                    } else {
                        return value.compareTo(behind) <= 0 && value.compareTo(below) >= 0;
                    }
                }
                break;
            case "is null or contain":
                if (isNullOrEmpty(value)) {
                    return true;
                } else {
                    if (values != null && compeValue != null) {
                        String[] strArr1 = compeValue.split(",");
                        for (String str : strArr1) {
                            for (String vl : values) {
                                if (vl != null && vl.contains(str)) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    } else {
                        return false;
                    }
                }
            case "contain":
                if (values != null && compeValue != null) {
                    String[] strArr1 = compeValue.split(",");
                    for (String str : strArr1) {
                        for (String vl : values) {
                            if (vl != null && vl.contains(str)) {
                                return true;
                            }
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            case "contain all":
                if (values != null && compeValue != null) {
                    String[] strArr1 = compeValue.split(",");
                    int i = 0;
                    for (String str : strArr1) {
                        for (String vl : values) {
                            if (vl != null && vl.contains(str)) {
                                i++;
                                break;
                            }
                        }
                    }
                    if (i == strArr1.length) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            case "not contain":
                if (values != null && compeValue != null) {
                    String[] strArr1 = compeValue.split(",");
                    for (String str : strArr1) {
                        for (String vl : values) {
                            if (vl == null || vl.contains(str)) {
                                return false;
                            }
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            case "like":
                if (value == null) {
                    return false;
                }
                return like(value, compeValue);
            case "not like":
                if (value == null) {
                    return false;
                }
                return !like(value, compeValue);
            case "not in":
                if (value == null || compeValue == null) {
                    return false;
                }
                String[] strArr1 = compeValue.split(",");
                for (String str : strArr1) {
                    if (value.equals(str)) {
                        return false;
                    }
                }
                return true;
        }

        return false;
    }

    private boolean like(String str, String expr) {
        expr = expr.toLowerCase(); // ignoring locale for now
        expr = expr.replace(".", "\\."); // "\\" is escaped to "\" 
        // ... escape any other potentially problematic characters here
        expr = expr.replace("?", ".");
        expr = expr.replace("%", ".*");
        str = str.toLowerCase();
        return str.matches(expr);
    }

    public void prepareInsert() {
        try {
            clear();

            if (!isShowNodeType) {
                nodeTypeSelecteds.add(nodeTypeService.get(-1l));
            }
            protocolSelected = new ComboBoxObject(Config.PROTOCOL_TELNET, Config.PROTOCOL_TELNET);

            isEdit = false;
            isClone = false;

            dialogHeader = MessageUtil.getResourceBundleMessage("title.insertCommand");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private List<String> getParamList(String commandPattern) {
        List<String> lstParam = new ArrayList<>();

        if (commandPattern != null && !commandPattern.trim().isEmpty() && commandPattern.contains("@{")) {
            int preIndex = 0;
            while (true) {
                preIndex = commandPattern.indexOf("@{", preIndex);

                if (preIndex < 0) {
                    break;
                } else {
                    preIndex += 2;
                }

                int endIndex = commandPattern.indexOf("}", preIndex);

                if (endIndex < 0) {
                    break;
                } else {
                    lstParam.add(commandPattern.substring(preIndex, endIndex));
                    preIndex = endIndex;
                }
            }
        }

        return lstParam;
    }

    public List<String> getAdvancedValue(List<String> values, List<String> valuesNoDistinct, Long valueCounter, Long valueStart,
            Long valueEnd, Long valueDirect, String joinChar, Long valueConvert, Long valueOutResult, String valueInterval, Long valueSequence, String splitColumnChar, int column) {
        List<String> collectValues = new ArrayList<>();
        List<String> collectValuesOutResult = new ArrayList<>();
        if (values != null) {
//            logger.info("values: " + values.size());
            if (valueCounter != null && valueCounter > 0) {
                //Truong hop count khong distinct
                collectValues.add(String.valueOf(values.size()));
                //Truong hop count khong distinct
//                collectValues.add(String.valueOf(values.size()));
            } else {
                //Lay tham so ngoai result
                collectValues.add(String.valueOf(values.size()));
                if (valueOutResult != null && valueOutResult > 0 && valueInterval != null) {
//                    logger.info("Da vao ham lay tham so ngoai");
                    if (valueInterval.contains("-")) {
                        String[] valueIntervals = valueInterval.split("-");
                        Integer valueIntervalStart = 0;
                        Integer valueIntervalEnd = 0;
                        if (valueIntervals[0] != null) {
                            try {
                                valueIntervalStart = Integer.valueOf(valueIntervals[0].trim());
                            } catch (Exception ex) {
                                logger.debug(ex.getMessage(), ex);
//                                logger.info("valueIntervalStart Loi: " + ex.getMessage());
                            }
                        }
                        if (valueIntervals[1] != null) {
                            try {
                                valueIntervalEnd = Integer.valueOf(valueIntervals[1].trim());
                            } catch (Exception ex) {
                                logger.debug(ex.getMessage(), ex);
//                                logger.info("valueIntervalEnd Loi: " + ex.getMessage());
                            }
                        }
                        if (valueIntervalEnd > valueIntervalStart) {
//                            logger.info("Bat dau chay vong for tu: " + valueIntervalStart + " toi: " + valueIntervalEnd);
                            for (int i = valueIntervalStart; i < valueIntervalEnd; i++) {
                                if (!values.contains(String.valueOf(i))) {
                                    collectValuesOutResult.add(String.valueOf(i));
                                }
                            }
                        }
                        if (!collectValuesOutResult.isEmpty()) {
//                            logger.info("Gan lai gia tri cho collectValues " + collectValuesOutResult.size());
                            values = new ArrayList<>(collectValuesOutResult);

                        }
                    }
//                    logger.info("values sau khi result ngoai: " + values.size());
                }

                int valueStartLocal = (valueStart == null || valueStart <= 0) ? 0 : valueStart.intValue() - 1;
                int valueEndLocal;
                if (valueEnd == null) {
                    valueEndLocal = values.size() - 1;
                } else {
                    if (valueEnd <= 0) {
                        valueEndLocal = 0;
                    } else {
                        valueEndLocal = valueEnd.intValue() - 1;
                        valueEndLocal = valueEndLocal > (values.size() - 1) ? (values.size() - 1) : valueEndLocal;
                    }
                }
                valueEndLocal = valueEndLocal > (values.size() - 1) ? (values.size() - 1) : valueEndLocal;

                if (valueStartLocal <= valueEndLocal) {
                    int sequence = valueEndLocal - valueStartLocal;
                    int locationBreak;
                    if (valueDirect == null) {
                        collectValues = values.subList(valueStartLocal, valueEndLocal + 1);
                    } else {
//                        logger.info("valueDirect.intValue()" + valueDirect.intValue());
                        switch (valueDirect.intValue()) {

                            case 1:
                                // Tu duoi len tren
//                                logger.info("tu duoi len tren");
                                List<String> reverseValues = new ArrayList<>(values);
                                Collections.reverse(reverseValues);
                                if (valueSequence > 0) {
                                    collectValues = reverseValues.subList(valueStartLocal, valueEndLocal + 1);
                                    while (valueEndLocal < reverseValues.size()) {
                                        locationBreak = checkSequenceList(collectValues);
                                        if (locationBreak > 0) {
                                            collectValues = new ArrayList<>();
//                                            collectValues.add("0");
                                            valueStartLocal = valueStartLocal + locationBreak;
                                            valueEndLocal = sequence + valueStartLocal;
                                            if (valueEndLocal >= reverseValues.size()) {

                                                break;
                                            }
                                            collectValues = reverseValues.subList(valueStartLocal, valueEndLocal + 1);
                                        } else {
                                            break;
                                        }
                                    }
                                } else {
                                    collectValues = reverseValues.subList(valueStartLocal, valueEndLocal + 1);
                                }
                                break;
                            case 2:
//                                logger.info("tu thap len cao");
                                // Tu thap len cao
                                List<String> compareValues = new ArrayList<>(values);
                                boolean isNumber = true;
                                for (String vl : compareValues) {
                                    try {
                                        System.out.println(Double.parseDouble(vl));
                                    } catch (Exception ex) {
                                        logger.error(ex.getMessage(), ex);
                                        isNumber = false;
                                    }
                                }
                                if (!isNumber) {
                                    Collections.sort(compareValues, new Comparator<String>() {
                                        @Override
                                        public int compare(String a, String b) {
                                            return a.compareTo(b);
                                        }
                                    });
                                } else {
                                    Collections.sort(compareValues, new Comparator<String>() {
                                        @Override
                                        public int compare(String a, String b) {
                                            double aVl = Double.parseDouble(a);
                                            double bVl = Double.parseDouble(b);
                                            if (aVl > bVl) {
                                                return 1;
                                            } else if (aVl < bVl) {
                                                return -1;
                                            } else {
                                                return 0;
                                            }
                                        }
                                    });
                                }
                                //Xet truong hop lay gia tri lien tiep hay khong
                                if (valueSequence != null && valueSequence > 0) {
                                    collectValues = compareValues.subList(valueStartLocal, valueEndLocal + 1);
                                    while (valueEndLocal < compareValues.size()) {
                                        locationBreak = checkSequenceList(collectValues);
                                        if (locationBreak > 0) {
                                            collectValues = new ArrayList<>();
//                                            collectValues.add("0");
                                            valueStartLocal = valueStartLocal + locationBreak;
                                            valueEndLocal = sequence + valueStartLocal;
                                            if (valueEndLocal >= compareValues.size()) {

                                                break;
                                            }
                                            collectValues = compareValues.subList(valueStartLocal, valueEndLocal + 1);
                                        } else {
                                            break;
                                        }
                                    }
                                } else {
                                    collectValues = compareValues.subList(valueStartLocal, valueEndLocal + 1);
                                }

                                break;
                            case 3:
                                // Tu cao xuong thap
//                                logger.info("tu cao xuong thap");
                                compareValues = new ArrayList<>(values);
                                isNumber = true;
                                for (String vl : compareValues) {
                                    try {
                                        System.out.println(Double.parseDouble(vl));
                                    } catch (Exception ex) {
                                        logger.error(ex.getMessage(), ex);
                                        isNumber = false;
                                    }
                                }
                                if (!isNumber) {
                                    Collections.sort(compareValues, new Comparator<String>() {
                                        @Override
                                        public int compare(String a, String b) {
                                            return b.compareTo(a);
                                        }
                                    });
                                } else {
                                    Collections.sort(compareValues, new Comparator<String>() {
                                        @Override
                                        public int compare(String a, String b) {
                                            double aVl = Double.parseDouble(a);
                                            double bVl = Double.parseDouble(b);
                                            if (aVl < bVl) {
                                                return 1;
                                            } else if (aVl > bVl) {
                                                return -1;
                                            } else {
                                                return 0;
                                            }
                                        }
                                    });
                                }
                                //Xet truong hop lay gia tri lien tiep hay khong
                                if (valueSequence != null && valueSequence > 0) {
                                    collectValues = compareValues.subList(valueStartLocal, valueEndLocal + 1);
                                    while (valueEndLocal < compareValues.size()) {
                                        locationBreak = checkSequenceList(collectValues);
                                        if (locationBreak > 0) {
                                            collectValues = new ArrayList<>();
//                                            collectValues.add("0");
                                            valueStartLocal = valueStartLocal + locationBreak;
                                            valueEndLocal = sequence + valueStartLocal;
                                            if (valueEndLocal >= compareValues.size()) {

                                                break;
                                            }
                                            collectValues = compareValues.subList(valueStartLocal, valueEndLocal + 1);
                                        } else {
                                            break;
                                        }
                                    }
                                } else {
                                    collectValues = compareValues.subList(valueStartLocal, valueEndLocal + 1);
                                }
                                break;
                            case 4:
                                //Thu tu xuat hien it nhat den nhieu nhat
//                                logger.info("tu it den nhieu");
//                                logger.info("Vao day roi sap xep tu nho toi lon");
                                //Gan lai tham so start and end row get data
                                valueStartLocal = (valueStart == null || valueStart <= 0) ? 0 : valueStart.intValue() - 1;

                                if (valueEnd == null) {
                                    valueEndLocal = valuesNoDistinct.size() - 1;
                                } else {
                                    if (valueEnd <= 0) {
                                        valueEndLocal = 0;
                                    } else {
                                        valueEndLocal = valueEnd.intValue() - 1;
                                        valueEndLocal = valueEndLocal > (valuesNoDistinct.size() - 1) ? (valuesNoDistinct.size() - 1) : valueEndLocal;
                                    }
                                }
                                valueEndLocal = valueEndLocal > (valuesNoDistinct.size() - 1) ? (valuesNoDistinct.size() - 1) : valueEndLocal;

                                List<String> sortQuantityValues = new ArrayList<>();
                                final Map<String, List<String>> mapSortQuantity = new HashMap<>();
                                for (String vl : valuesNoDistinct) {
                                    if (mapSortQuantity.containsKey(vl)) {
                                        mapSortQuantity.get(vl).add(vl);
                                    } else {
                                        List<String> lstStringTemp = new ArrayList<>();
                                        lstStringTemp.add(vl);
                                        mapSortQuantity.put(vl, lstStringTemp);
                                    }
                                }

                                final Comparator<String> c = new Comparator<String>() {
                                    @Override
                                    public int compare(final String o1, final String o2) {
                                        // Compare the size of the lists. If they are the same, compare
                                        // the keys themsevles.
                                        final int sizeCompare = mapSortQuantity.get(o1).size() - mapSortQuantity.get(o2).size();
                                        return sizeCompare != 0 ? sizeCompare : o1.compareTo(o2);
                                    }
                                };

                                final Map<String, List<String>> mapSortQuantitySort = new TreeMap<String, List<String>>(c);
                                mapSortQuantitySort.putAll(mapSortQuantity);
                                for (String vl : mapSortQuantitySort.keySet()) {
                                    for (String vlTemp : mapSortQuantitySort.get(vl)) {
                                        sortQuantityValues.add(vlTemp);
                                    }
//                                    sortQuantityValues.addAll(mapSortQuantitySort.get(vl));
                                }

                                collectValues = sortQuantityValues.subList(valueStartLocal, valueEndLocal + 1);

                                break;
                            case 5:
                                //Thu tu xuat hien nhieu nhat den it nhat
//                                logger.info("tu nhieu den it");
                                sortQuantityValues = new ArrayList<>();
                                //Gan lai tham so start and end row get data
                                valueStartLocal = (valueStart == null || valueStart <= 0) ? 0 : valueStart.intValue() - 1;

                                if (valueEnd == null) {
                                    valueEndLocal = valuesNoDistinct.size() - 1;
                                } else {
                                    if (valueEnd <= 0) {
                                        valueEndLocal = 0;
                                    } else {
                                        valueEndLocal = valueEnd.intValue() - 1;
                                        valueEndLocal = valueEndLocal > (valuesNoDistinct.size() - 1) ? (valuesNoDistinct.size() - 1) : valueEndLocal;
                                    }
                                }
                                valueEndLocal = valueEndLocal > (valuesNoDistinct.size() - 1) ? (valuesNoDistinct.size() - 1) : valueEndLocal;

                                final Map<String, List<String>> mapSortQuantity1 = new HashMap<>();
                                for (String vl : valuesNoDistinct) {
                                    if (mapSortQuantity1.containsKey(vl)) {
                                        mapSortQuantity1.get(vl).add(vl);
                                    } else {
                                        List<String> lstStringTemp = new ArrayList<>();
                                        lstStringTemp.add(vl);
                                        mapSortQuantity1.put(vl, lstStringTemp);
                                    }
                                }

                                final Comparator<String> c1 = new Comparator<String>() {
                                    @Override
                                    public int compare(final String o1, final String o2) {
                                        // Compare the size of the lists. If they are the same, compare
                                        // the keys themsevles.
                                        final int sizeCompare = mapSortQuantity1.get(o2).size() - mapSortQuantity1.get(o1).size();
                                        return sizeCompare != 0 ? sizeCompare : o2.compareTo(o1);
                                    }
                                };

                                final Map<String, List<String>> mapSortQuantitySort1 = new TreeMap<String, List<String>>(c1);
                                mapSortQuantitySort1.putAll(mapSortQuantity1);
                                for (String vl : mapSortQuantitySort1.keySet()) {
                                    sortQuantityValues.addAll(mapSortQuantitySort1.get(vl));
                                }

                                collectValues = sortQuantityValues.subList(valueStartLocal, valueEndLocal + 1);

                                break;

                            default:
//                                logger.info("tu tren xuong duoi");
                                // Tu tren xuong duoi
//                                collectValues = values.subList(valueStartLocal, valueEndLocal + 1);
                                if (valueSequence != null && valueSequence > 0) {

                                    collectValues = values.subList(valueStartLocal, valueEndLocal + 1);
//                                    logger.info("collectValues size =" + collectValues);
//                                    logger.info("values size =" + values);
//                                    logger.info("valueStartLocal size =" + valueStartLocal);
//                                    logger.info("valueEndLocal size =" + valueEndLocal);
                                    while (valueEndLocal < values.size()) {
                                        locationBreak = checkSequenceList(collectValues);
//                                        logger.info("locationBreak size =" + locationBreak);
                                        if (locationBreak > 0) {
                                            collectValues = new ArrayList<>();
//                                            collectValues.add("0");
                                            valueStartLocal = valueStartLocal + locationBreak;
                                            valueEndLocal = sequence + valueStartLocal;
                                            if (valueEndLocal >= values.size()) {

                                                break;
                                            }
                                            collectValues = values.subList(valueStartLocal, valueEndLocal + 1);
                                        } else {
                                            break;
                                        }
                                    }
                                } else {
                                    collectValues = values.subList(valueStartLocal, valueEndLocal + 1);
                                }
                                break;

                        }
                        if (!isNullOrEmpty(splitColumnChar)) {
                            List<String> collectValuesColumn = new ArrayList<>();
                            for (String vl : collectValues) {
                                if (!isNullOrEmpty(splitColumnChar)) {
                                    String[] arr = vl.trim().split(splitColumnChar);
                                    if (column > 0 && arr != null && arr.length >= column) {
                                        collectValuesColumn.add(arr[column - 1]);
                                    }
                                }
                            }
                            if (!collectValuesColumn.isEmpty()) {
                                collectValues = collectValuesColumn;
                            }
                        }
                    }

                }
            }
        } else {
            if (valueCounter != null && valueCounter > 0) {
//                collectValues.add("0");
            }
        }

        if (joinChar != null && !joinChar.trim().isEmpty() && !collectValues.isEmpty()) {
            String joinContent = StringUtils.join(collectValues, joinChar);
            collectValues.clear();
            collectValues.add(joinContent);
        }

        if (!collectValues.isEmpty() && valueConvert != null) {
            List<String> returnValues = new ArrayList<>();
            switch (valueConvert.intValue()) {
                case 1:
                    // DEC --> HEX
                    for (String vl : collectValues) {
                        returnValues.add(Integer.toHexString(Integer.parseInt(vl)));
                    }
                    return returnValues;
                case 2:
                    // HEX --> DEC
                    for (String vl : collectValues) {
                        returnValues.add(String.valueOf(Integer.parseInt(vl, 16)));
                    }
                    return returnValues;
            }
        }

        return collectValues;
    }

    public static int checkSequenceList(List<String> collectValues) {
        boolean check = true;
        Double vlOld = null;
        Double vlSequence = 1D;
        int locationBreak = 0;
//        logger.info("collectValues: " + collectValues);
        for (String value : collectValues) {
            try {
//                logger.info("value: " + value);
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                check = false;
                break;
            }
            if (vlOld == null) {
                vlOld = Double.parseDouble(value);
//                logger.info("vlOld: " + vlOld);
            } else {
                if (!new BigDecimal(Double.parseDouble(value) - vlOld).equals(new BigDecimal(vlSequence))) {
                    check = false;
                    break;
                }

                vlOld = Double.parseDouble(value);
//                logger.info("vlOld after: " + vlOld);
            }
            locationBreak++;
//            logger.info("locationBreak: " + locationBreak);
        }
        if (check) {
            return 0;
        } else {
            return locationBreak;
        }
    }

    public void getValueRowColumnIndex(List<String> lstValue, List<String> lstValueNoDistinct, String content, int row, int column,
            int countRow, String rowHeader, String rowHeaderOperator, String rowFooter,
            String rowFooterOperator, String splitColumnChar, String regex, long removeDuplicate) {
        BufferedReader bf = null;
        StringReader sr = null;
        try {
            sr = new StringReader(content);
            bf = new BufferedReader(sr);

            String line = "";
            int i = 1;
            int count = 0;
//            lstValue = new ArrayList<>();
//            lstValueNoDistinct = new ArrayList<>();
            boolean isHeader = false;
            while ((line = bf.readLine()) != null) {
                if (rowHeader != null && !rowHeader.trim().isEmpty()) {
                    switch (rowHeaderOperator) {
                        case "CONTAIN":
                            if (line.trim().contains(rowHeader)) {
                                isHeader = true;
                            }
                            break;
                        case "START WITH":
                            if (line.trim().startsWith(rowHeader)) {
                                isHeader = true;
                            }
                            break;
                        case "END WITH":
                            if (line.trim().endsWith(rowHeader)) {
                                isHeader = true;
                            }
                            break;
                        case "REGEX":
                            if (line.trim().matches(rowHeader)) {
                                isHeader = true;
                            }
                            break;
                    }
                    if (isHeader) {
                        break;
                    }
                } else {
                    i = 1;
                    break;
                }
            }

            do {
                if (line == null) {
                    break;
                }
                boolean isFooter = false;
                if (!isNullOrEmpty(rowFooter)) {
                    switch (rowFooterOperator) {
                        case "CONTAIN":
                            if (line.trim().contains(rowFooter)) {
                                isFooter = true;
                            }
                            break;
                        case "START WITH":
                            if (line.trim().startsWith(rowFooter)) {
                                isFooter = true;
                            }
                            break;
                        case "END WITH":
                            if (line.trim().endsWith(rowFooter)) {
                                isFooter = true;
                            }
                            break;
                        case "REGEX":
                            if (line.trim().matches(rowFooter)) {
                                isFooter = true;
                            }
                            break;
                    }
                    if (isFooter) {
                        break;
                    }
                }
                if (row <= i) {
                    if (countRow > 0) {
                        count++;
                        if (count <= countRow) {
                            if (isNullOrEmpty(splitColumnChar)) {
                                List<String> vls = getValueRegex(line.trim(), regex);

                                for (String vl : vls) {
                                    if (!isNullOrEmpty(vl) && (removeDuplicate == 1 || !lstValue.contains(vl))) {
                                        lstValue.add(vl.trim());
                                    }
                                    if (!isNullOrEmpty(vl)) {
                                        lstValueNoDistinct.add(vl.trim());
                                    }
                                }
                            } else {
                                String[] arr = line.trim().split(splitColumnChar);
                                if (arr != null && arr.length >= column) {
                                    List<String> vls = getValueRegex(arr[column - 1], regex);

                                    for (String vl : vls) {
                                        if (!isNullOrEmpty(vl) && (removeDuplicate == 1 || !lstValue.contains(vl))) {
                                            lstValue.add(vl.trim());
                                        }
                                        if (!isNullOrEmpty(vl)) {
                                            lstValueNoDistinct.add(vl.trim());
                                        }
                                    }
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
//                        if (line.trim().isEmpty() && isNullOrEmpty(rowFooter)) {
//                            break;
//                        } else {
                        if (isNullOrEmpty(splitColumnChar)) {
                            List<String> vls = getValueRegex(line.trim(), regex);

                            for (String vl : vls) {
                                if (!isNullOrEmpty(vl) && (removeDuplicate == 1 || !lstValue.contains(vl))) {
                                    lstValue.add(vl.trim());
                                }
                                if (!isNullOrEmpty(vl)) {
                                    lstValueNoDistinct.add(vl.trim());
                                }
                            }
                        } else {
                            String[] arr = line.trim().split(splitColumnChar);
                            if (arr != null && arr.length >= column) {
                                List<String> vls = getValueRegex(arr[column - 1], regex);

                                for (String vl : vls) {
                                    if (!isNullOrEmpty(vl) && (removeDuplicate == 1 || !lstValue.contains(vl))) {
                                        lstValue.add(vl.trim());
                                    }
                                    if (!isNullOrEmpty(vl)) {
                                        lstValueNoDistinct.add(vl.trim());
                                    }
                                }
                            }
                        }
//                        }
                    }
                }
                i++;
            } while ((line = bf.readLine()) != null);

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (sr != null) {
                sr.close();
            }

            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

    }

    private List<String> getValueRegex(String content, String regex) {
        List<String> values = new ArrayList<>();
        try {
            if (isNullOrEmpty(regex) || isNullOrEmpty(content)) {
                values.add(content);
                return values;
            }
            Pattern patRegex = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
            Matcher matcher = patRegex.matcher(content.trim());
            while (matcher.find()) {
                String value = matcher.group(1);
                if (!values.contains(value)) {
                    values.add(value);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return values;
    }

    private List<String> getValueSql(String username, String pass, String url, String protocol, String command,
            int column, List<ParamInput> params) throws Exception {
        Connection conn = null;
        NamedParameterStatement pst = null;
        ResultSet rs = null;

        try {
            ConnectionPool pool = new ConnectionPool(protocol, url, username, pass);
            conn = pool.reserveConnection();

            pst = new NamedParameterStatement(conn, command);

//            if (params != null & !params.isEmpty()) {
//                for (ParamInput param : params) {
//                    pst.setString(param.getParamCode(), param.getParamValue());
//                }
//            }
            List<String> lstValue = new ArrayList<>();
            if (isImpactCmdSql(command.toLowerCase())) {
                lstValue.add(pst.executeUpdate() + "");
            } else {
                rs = pst.executeQuery();

                if (rs.getMetaData().getColumnCount() >= 1) {
                    while (rs.next()) {
                        lstValue.add(rs.getString(column));
                    }
                }
            }
            return lstValue;
        } catch (Exception ex) {
            throw ex;
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }

            try {
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }

            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    private boolean isImpactCmdSql(String cmd) {
        String[] keywords = new String[]{" update ", " delete ", " insert ", " alter ", " drop ",
            " create ", " grant ", " revoke ", " analyze ", " audit ", " comment "};

        for (String keyword : keywords) {
            if (cmd.contains(keyword)) {
                return true;
            }
        }
        return false;
    }

    public static String getValueSsh(String username, String pass, String ip, int port, String command,
            int timeout, String cmdEnd, String morePrompt, String moreCommand, String vendor, String nodeType,
            String version, Long confirmCmdType, String confirmCommand, String confirmPrompt) throws Exception {
        JSchSshUtil sshClient = null;
        try {
            try {
                sshClient = new JSchSshUtil(ip, port, username, pass, cmdEnd, "\r\n", timeout, false, vendor, version, nodeType);
                sshClient.connect();
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                if (ex.getMessage().contains("Protocol error: no matching DH grp found")
                        || ex.getMessage().contains("Prime size must be multiple of 64")) {
                    try {
                        String vmsaSshUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_ssh_account"), "ipchange");
                        String vmsaSshPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_ssh_pass"), "ipchange");

                        sshClient = new JSchSshUtil(ip, port, vmsaSshUser, vmsaSshPass, cmdEnd, "\r\n", timeout, true, vendor, version, nodeType);
                        if (sshClient.connect()) {
                            if (!sshClient.connectShellSsh(username, pass)) {
                                throw new Exception("LOGIN_FAIL");
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        throw new Exception("LOGIN_FAIL");
                    }
                } else {
                    throw new Exception("LOGIN_FAIL");
                }
            }
            logger.info("confirmCommand: " + confirmCommand);
            if (confirmCmdType != null && confirmCmdType == 1) {
                moreCommand = pass;
            } else if (confirmCmdType != null && confirmCmdType == 2) {
                moreCommand = (confirmCommand == null || "".equals(confirmCommand)) ? " " : PassProtector.decrypt(confirmCommand.trim(), "ipchange");
            } else {
                if (confirmCommand != null && !"".equals(confirmCommand)) {
                    moreCommand = confirmCommand;
                }
                if (moreCommand == null || moreCommand.trim().isEmpty()) {
                    moreCommand = " ";
                }
            }
            logger.info("moreCommand: " + moreCommand);
            logger.info("confirmPrompt: " + confirmPrompt);
            if (confirmPrompt != null && !"".equals(confirmPrompt)) {
                morePrompt = confirmPrompt;
            }
            logger.info("morePrompt: " + morePrompt);
            Result result = (cmdEnd == null || cmdEnd.trim().isEmpty())
                    ? ((morePrompt == null || morePrompt.trim().isEmpty()) ? sshClient.sendLineWithTimeOutNew(command, timeout, true) : sshClient.sendLineWithMore(command, morePrompt, moreCommand, timeout))
                    : ((morePrompt == null || morePrompt.trim().isEmpty()) ? sshClient.sendLineWithTimeOutNew(command, timeout, true, cmdEnd) : sshClient.sendLineWithMore(command, morePrompt, moreCommand, timeout, cmdEnd));

            //logger.info(result != null ? result.getResult() : "");
            if (result != null && result.isSuccessSent()) {
                String content = result.getResult();

                String firstLine = content.contains("\n") ? content.substring(0, content.indexOf("\n")) : null;
                if (firstLine != null) {
                    content = content.substring(content.indexOf("\n"));
                }
//                if (content.lastIndexOf("\n") > 0) {
//                    content = content.substring(0, content.lastIndexOf("\n"));
//                } else if (content.endsWith(cmdEnd)) {
//                    content = "";
//                }

                return content.trim();
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (sshClient != null) {
                sshClient.disconnect();
            }
        }
        return "";
    }

    private String getValueTelnet(String username, String pass, String ip, int port, String prompt,
            String cmdExit, String terminator, String command, int timeout, String cmdEnd,
            String pagingCmd, String pagingRegex, String confirmCmd, Long confirmCmdType, String confirmRegex,
            String vendor, String osType, String version, String nodeType, String nodeCode, String meid, Long formRun, String nodeIp, String cmdEndOrder) throws Exception {

        TelnetClientUtil telnetClient = null;
        logger.info("Ip: " + ip);
        logger.info("Port: " + port);
        try {
            switch (vendor.toLowerCase()) {
                case "tekelec":
                    telnetClient = new TelnetStpTekelec(ip, port, vendor);
                    telnetClient.setShellPromt("(>|#)");
                    break;
                case "nokia":
                    telnetClient = new TelnetNokia(ip, port, vendor);
                    break;
                case "huawei":
                    if (port != 23) {
                        telnetClient = new TelnetHuawei(ip, port, vendor, version, nodeType, formRun, nodeIp);
                        break;
                    } else {
//                        logger.info("---TelnetHuawei port 23---");
                        telnetClient = new TelnetClientUtil(ip, port, vendor);
                        telnetClient.setShellPromt("(>|#)");
                        break;
                    }

                case "ericsson":
                    telnetClient = new TelnetEricsson(ip, port, vendor, osType, nodeType, nodeCode);
                    break;
                case "zte":
                    telnetClient = new TelnetZte(ip, port, vendor);
                    break;
                case "viettel":
                    telnetClient = new TelnetViettel(ip, port, vendor, version, nodeType);
                    break;
                default:
                    telnetClient = new TelnetClientUtil(ip, port, vendor);
                    telnetClient.setShellPromt("(>|#)");
                    break;
            }
//            telnetClient.setCmdExit(cmdExit);
            telnetClient.setTerminalType(terminator);
            telnetClient.setTimeoutDefault(timeout);

            String strLogin = "";
            try {
                strLogin = telnetClient.connect(username, pass).replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");

                if (telnetClient instanceof TelnetHuawei && meid != null && !"0".equals(meid)) {
                    ((TelnetHuawei) telnetClient).enterMode(meid);
                }
            } catch (Exception ex) {
                if (!"CONN_TIMEOUT".equals(ex.getMessage())) {
                    throw new Exception("LOGIN_FAIL");
                } else {
                    throw ex;
                }
            }

//            if (!strLogin.endsWith(prompt)) {
//                throw new Exception("LOGIN_FAIL");
//            }
//            if (strLogin != null && strLogin.contains("\n")) {
//                String loginPrompt = strLogin.substring(strLogin.lastIndexOf("\n"));
//                telnetClient.setShellPromt(loginPrompt);
//                
//                logger.info("loginPrompt: " + loginPrompt);
//            }
            logger.info("loginPrompt: " + telnetClient.getPrompt());

            if (!isNullOrEmpty(prompt)) {
                telnetClient.setShellPromt(prompt);
            }
            if (!isNullOrEmpty(cmdEndOrder)) {
                telnetClient.setPatternOrderCmd(cmdEndOrder);
            } else {
                telnetClient.setPatternOrderCmd(telnetClient.getPrompt());
            }

            String content;
            if (isNullOrEmpty(pagingRegex) && isNullOrEmpty(confirmRegex)) {
                content = telnetClient.sendWait(command, prompt, true, timeout);
            } else if (isNullOrEmpty(confirmRegex) && !isNullOrEmpty(pagingRegex)) {
                if (pagingCmd == null) {
                    pagingCmd = " ";
                }
                content = telnetClient.sendWaitHasMore(command, prompt, pagingRegex, pagingCmd, timeout);
            } else {
                if (pagingCmd == null) {
                    pagingCmd = " ";
                }
                if (confirmCmdType != null && confirmCmdType == 1) {
                    confirmCmd = pass;
                }
//                logger.info("vao day roi ne");
                content = telnetClient.sendWaitHasConfirm(command, prompt, confirmRegex, confirmCmd, pagingRegex, pagingCmd, timeout);
            }

            if (content == null) {
                return null;
            }

            if (!"TOLL".equalsIgnoreCase(nodeType) || !"ZTE".equalsIgnoreCase(vendor)) {
                String firstLine = content.contains("\n") ? content.substring(0, content.indexOf("\n")) : null;
                if (firstLine != null) {
                    content = content.substring(content.indexOf("\n"));
                }
//                logger.info("firstLine: " + firstLine + ", content: " + content);
            }

            if (content.lastIndexOf("\n") > 0) {
                content = content.substring(0, content.lastIndexOf("\n"));
            } else if (telnetClient.checkRegex(content, isNullOrEmpty(cmdEnd) ? telnetClient.getPrompt() : cmdEnd)) {
                content = "";
            }

            content = content.replaceAll("\\b", "")
                    .replaceAll("\u001B7+", "").replaceAll("[\\x00\\x08\\x0B\\x0C\\x0E-\\x1F]", "")
                    .replaceAll("[\\p{Cf}]", "")
                    .replace("[42D", "")
                    .trim();

            return content;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (telnetClient != null) {
                try {
                    TelnetClientUtil.disConnect(telnetClient, username);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
    }

    public static String getResultFtp(String username, String pass, String ip, int port,
            int timeout, String cmdEnd, String vendor, String nodeType,
            String version, String fileName, String pathFile, String destination) throws Exception {
        JSchSshUtil sshClient = null;
        try {

            //Quytv7 Dang nhap ssh
            try {
                sshClient = new JSchSshUtil(ip, port, username, pass, cmdEnd, "\r\n", timeout, false, vendor, version, nodeType);
                sshClient.connect();
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                if (ex.getMessage().contains("Protocol error: no matching DH grp found")
                        || ex.getMessage().contains("Prime size must be multiple of 64")) {
                    try {
                        String vmsaSshUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_ssh_account"), "ipchange");
                        String vmsaSshPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vmsa_ssh_pass"), "ipchange");

                        sshClient = new JSchSshUtil(ip, port, vmsaSshUser, vmsaSshPass, cmdEnd, "\r\n", timeout, true, vendor, version, nodeType);
                        if (sshClient.connect()) {
                            if (!sshClient.connectShellSsh(username, pass)) {
                                return "LOGIN_FAIL";
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        return "LOGIN_FAIL";
                    }
                } else {
                    return "LOGIN_FAIL";
                }
            }
            //Day ftp len server
            try {
                logger.info("Bat dau day file ftp len server");
                ChannelSftp sftp = sshClient.getSftpChanel();
                logger.info("Ten file " + fileName);
                logger.info("Day file thanh cong");

//                    commandRun.setResult("Day file thanh cong");
                try {
                    sftp.cd(destination);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    sftp.mkdir(destination);
                    sftp.cd(destination);
                }
                sftp.put(new FileInputStream(pathFile), fileName);
                return "PUT FILE SUCCESS";
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                return "PUT FILE FAIL";
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return "PUT FILE FAIL";
        } finally {
            if (sshClient != null) {
                sshClient.disconnect();
            }
        }
    }

    private void onChangeOperator(String value) {
        switch (value) {
            case "IS NULL":
            case "NOT NULL":
            case "NO CHECK":
                standardValueReadonly = true;
                obj.setStandardValue("");
                break;
            case "CONTAIN":
            case "IS NULL OR CONTAIN":
            case "NOT CONTAIN":
            case "=":
            case "<":
            case ">":
            case ">=":
            case "<=":
            case "<>":
                standardValueReadonly = false;
                standardValueRequired = true;
                break;
            case "IN":
            case "NOT IN":
                standardValueReadonly = false;
                standardValueRequired = true;
                standardValuePrompt = MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.in");
                break;
            case "BETWEEN":
                standardValueReadonly = false;
                standardValueRequired = true;
                standardValuePrompt = MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.between");
                break;
            case "LIKE":
            case "NOT LIKE":
                standardValueReadonly = false;
                standardValueRequired = true;
                standardValuePrompt = MessageUtil.getResourceBundleMessage("paramMngt.compOpe.format.like");
                break;
            default:
                standardValueReadonly = false;
                standardValueRequired = false;
                standardValuePrompt = "";
                break;
        }
    }

    public void onChangeOperator() {
        try {
            String operator = opeSelected == null ? "" : opeSelected.getValue();

            onChangeOperator(operator);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public List<Node> autoCompleNode(String nodeCode) {
        List<Node> lstNode;
        Map<String, Object> filters = new HashMap<>();
        filters.put("version.versionId", commandSend.getFilterVersionId());
        filters.put("vendor.vendorId", commandSend.getFilterVendorId());
        if (isShowNodeType) {
            filters.put("nodeType.typeId", commandSend.getFilterNodeTypeId());
        }
        if (nodeCode != null) {
            filters.put("nodeCode", nodeCode.trim());
        }
        try {
            lstNode = nodeService.findList(0, 100, filters, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            lstNode = new ArrayList<>();
        }
        return lstNode;
    }

    public void afterProtocolSelected() {
        if (protocolSelected != null && protocolSelected.getValue().equals(Config.PROTOCOL_FTP)) {
            obj.getCommandTelnetParser().setCmd(MessageUtil.getResourceBundleMessage("label.cmd.ftp.valueCmd"));
            reloadParam();
        }
    }

    public ComboBoxObject getProtocolSelected() {
        return protocolSelected;
    }

    public void setProtocolSelected(ComboBoxObject protocolSelected) {
        this.protocolSelected = protocolSelected;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public List<NodeType> getNodeTypes() {
        return nodeTypes;
    }

    public void setNodeTypes(List<NodeType> nodeTypes) {
        this.nodeTypes = nodeTypes;
    }

    public VendorServiceImpl getVendorService() {
        return vendorService;
    }

    public void setVendorService(VendorServiceImpl vendorService) {
        this.vendorService = vendorService;
    }

    public NodeTypeServiceImpl getNodeTypeService() {
        return nodeTypeService;
    }

    public void setNodeTypeService(NodeTypeServiceImpl nodeTypeService) {
        this.nodeTypeService = nodeTypeService;
    }

    public CommandDetailServiceImpl getCommandDetailService() {
        return commandDetailService;
    }

    public void setCommandDetailService(CommandDetailServiceImpl commandDetailService) {
        this.commandDetailService = commandDetailService;
    }

    public LazyDataModel<CommandDetail> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<CommandDetail> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public CommandDetail getObj() {
        return obj;
    }

    public void setObj(CommandDetail obj) {
        this.obj = obj;
    }

    public CommandTelnetParserServiceImpl getCommandTelnetParserService() {
        return commandTelnetParserService;
    }

    public void setCommandTelnetParserService(CommandTelnetParserServiceImpl commandTelnetParserService) {
        this.commandTelnetParserService = commandTelnetParserService;
    }

    public boolean isIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public List<Vendor> getVendorSeleteds() {
        return vendorSeleteds;
    }

    public void setVendorSeleteds(List<Vendor> vendorSeleteds) {
        this.vendorSeleteds = vendorSeleteds;
    }

    public List<NodeType> getNodeTypeSelecteds() {
        return nodeTypeSelecteds;
    }

    public void setNodeTypeSelecteds(List<NodeType> nodeTypeSelecteds) {
        this.nodeTypeSelecteds = nodeTypeSelecteds;
    }

    public Vendor getVendorSelected() {
        return vendorSelected;
    }

    public void setVendorSelected(Vendor vendorSelected) {
        this.vendorSelected = vendorSelected;
    }

    public NodeType getNodeTypeSelected() {
        return nodeTypeSelected;
    }

    public void setNodeTypeSelected(NodeType nodeTypeSelected) {
        this.nodeTypeSelected = nodeTypeSelected;
    }

    public List<ParamInput> getParams() {
        return params;
    }

    public void setParams(List<ParamInput> params) {
        this.params = params;
    }

    public String getDialogHeader() {
        return dialogHeader;
    }

    public void setDialogHeader(String dialogHeader) {
        this.dialogHeader = dialogHeader;
    }

    public ParamInputServiceImpl getParamInputService() {
        return paramInputService;
    }

    public void setParamInputService(ParamInputServiceImpl paramInputService) {
        this.paramInputService = paramInputService;
    }

    public ActionCommandServiceImpl getActionCommandService() {
        return actionCommandService;
    }

    public void setActionCommandService(ActionCommandServiceImpl actionCommandService) {
        this.actionCommandService = actionCommandService;
    }

    public boolean getHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    public VersionServiceImpl getVersionService() {
        return versionService;
    }

    public void setVersionService(VersionServiceImpl versionService) {
        this.versionService = versionService;
    }

    public Version getVersionSelected() {
        return versionSelected;
    }

    public void setVersionSelected(Version versionSelected) {
        this.versionSelected = versionSelected;
    }

    public List<ComboBoxObject> getOperators() {
        return operators;
    }

    public void setOperators(List<ComboBoxObject> operators) {
        this.operators = operators;
    }

    public ComboBoxObject getOpeSelected() {
        return opeSelected;
    }

    public void setOpeSelected(ComboBoxObject opeSelected) {
        this.opeSelected = opeSelected;
    }

    public CommandObject getCommandSend() {
        return commandSend;
    }

    public void setCommandSend(CommandObject commandSend) {
        this.commandSend = commandSend;
    }

    public List<Node> getNodeRuns() {
        return nodeRuns;
    }

    public void setNodeRuns(List<Node> nodeRuns) {
        this.nodeRuns = nodeRuns;
    }

    public NodeServiceImpl getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeServiceImpl nodeService) {
        this.nodeService = nodeService;
    }

    public ParamGroupServiceImpl getParamGroupService() {
        return paramGroupService;
    }

    public void setParamGroupService(ParamGroupServiceImpl paramGroupService) {
        this.paramGroupService = paramGroupService;
    }

    public ParamValueServiceImpl getParamValueService() {
        return paramValueService;
    }

    public void setParamValueService(ParamValueServiceImpl paramValueService) {
        this.paramValueService = paramValueService;
    }

    public boolean isStandardValueRequired() {
        return standardValueRequired;
    }

    public void setStandardValueRequired(boolean standardValueRequired) {
        this.standardValueRequired = standardValueRequired;
    }

    public String getStandardValuePrompt() {
        return standardValuePrompt;
    }

    public void setStandardValuePrompt(String standardValuePrompt) {
        this.standardValuePrompt = standardValuePrompt;
    }

    public boolean isStandardValueReadonly() {
        return standardValueReadonly;
    }

    public void setStandardValueReadonly(boolean standardValueReadonly) {
        this.standardValueReadonly = standardValueReadonly;
    }

    public List<Version> getVersionSelecteds() {
        return versionSelecteds;
    }

    public void setVersionSelecteds(List<Version> versionSelecteds) {
        this.versionSelecteds = versionSelecteds;
    }

    public FlowTemplatesServiceImpl getFlowTemplatesService() {
        return flowTemplatesService;
    }

    public void setFlowTemplatesService(FlowTemplatesServiceImpl flowTemplatesService) {
        this.flowTemplatesService = flowTemplatesService;
    }

    public List<ComboBoxObject> getProtocols() {
        return protocols;
    }

    public void setProtocols(List<ComboBoxObject> protocols) {
        this.protocols = protocols;
    }

    public List<FlowTemplates> getTemplates() {
        return templates;
    }

    public void setTemplates(List<FlowTemplates> templates) {
        this.templates = templates;
    }
}
