package com.viettel.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.viettel.util.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.ReorderEvent;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.model.Action;
import com.viettel.model.ActionCommand;
import com.viettel.model.ActionDetail;
import com.viettel.model.ActionOfFlow;
import com.viettel.model.FlowTemplates;
import com.viettel.model.GroupActionClone;
import com.viettel.model.NodeRunGroupAction;
import com.viettel.model.ParamGroup;
import com.viettel.model.ParamGroupId;
import com.viettel.model.ParamInOut;
import com.viettel.model.ParamInOutId;
import com.viettel.model.ParamInput;
import com.viettel.model.TemplateGroup;
import com.viettel.persistence.ActionDetailServiceImpl;
import com.viettel.persistence.ActionOfFlowServiceImpl;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.GroupActionCloneServiceImpl;
import com.viettel.persistence.NodeRunGroupActionServiceImpl;
import com.viettel.persistence.ParamGroupServiceImpl;
import com.viettel.persistence.ParamInOutServiceImpl;
import com.viettel.persistence.TemplateGroupServiceImpl;
import com.viettel.util.Config;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionUtil;
import com.viettel.util.SessionWrapper;
import java.util.Objects;

@ViewScoped
@ManagedBean
public class BuildTemplateFlowController implements Serializable {

    protected static final Logger logger = LoggerFactory.getLogger(BuildTemplateFlowController.class);

    public static final Long startIndexOrder = 1l;
    public static final Long DEFAULT_PARAM_GROUP_ID = -1l;
    private static final Long MAX_LENGTH_TEMPLATE = 200L;
    private static final Long MAX_LENGTH_GROUP_NAME = 200L;

    private FlowTemplates selectedFlowTemplate;
    private List<FlowTemplates> lstFlowTemplate = new ArrayList<>();
    private Map<String, Integer> mapGroupName = new LinkedHashMap<>();
    private List<List<ActionOfFlow>> actionOfFlowss = new LinkedList<>();
    private List<SelectItem> lstItemGroupName = new ArrayList<>();
    private List<ActionOfFlow> lstActionFlowDel = new ArrayList<>();
    private List<ParamGroup> lstParamGroup = new ArrayList<>();
    private List<ParamInOut> lstParamInOutObject = new ArrayList<>();
    private List<ActionOfFlow> lstActionFlow = new ArrayList<>();
    private List<ActionCommand> lstActionCommand = new ArrayList<>();

    private String copyFlowTemplateName;
    private String flowTemplateName;
    private Long cloneType;
    private Map<String, Long> mapCloneType = new HashMap<>();
    private Long groupTemplateId;
    private TemplateGroup selectTemplateGroup;
    private Long selectedResetType;

    private String userName;
    private TreeNode selectedTreeNodeAction;
    private String selectedGroupActionName;
    private boolean isAddNewGroupName;
    private ActionOfFlow selectedActionFlow;
    private Action selectedActionView;
    private ActionDetail selectedActionDetail;
    private ActionCommand selectedActionCmd;

    private ParamGroup selectedParamGroup;

    private boolean isEdit;
    private boolean isChangeGroupActionName;
    private boolean isChangeGroupCode;
    private String newGroupActionName;
    private String oldGroupActionName;

    private ParamInOut selectedParamInOut;

    private Boolean[] defaultAddToTemplate = new Boolean[10];

    private Integer indexOfGroupActionToDel;

    private boolean isApproveTemplate;
    private boolean isPreApproveTemplate;

    private List<String> preStepsActionSelected;
    //Quytv7 ghi log action
    private String logAction = "";
    private String className = BuildTemplateFlowController.class.getName();
    //2018/01/11 Gan action fail khi co 1 lenh fail(Khong chay action clone nua) Stop luon start
    private boolean isStopCmdFail = false;
    private Map<String, Boolean> mapIsStopCmdFail = new HashMap<>();
    //2018/01/11 Gan action fail khi co 1 lenh fail(Khong chay action clone nua) Stop luon end

    @PostConstruct
    public void onStart() {
        try {
            isApproveTemplate = new SessionUtil().isApproveTemplate();
            isPreApproveTemplate = new SessionUtil().isPreApproveTemplate();
            isEdit = false;
            isAddNewGroupName = false;
            userName = SessionWrapper.getCurrentUsername();
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("flowTemplateName", "ASC");
            lstFlowTemplate = new FlowTemplatesServiceImpl().findList(null, orders);
            //selectedFlowTemplate=lstFlowTemplate.get(0);
            selectedFlowTemplate = null;
            onChangeFlowTemplate();
            logAction = LogUtils.addContent("", "Login Function");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void onChangeFlowTemplate() {
        try {
            clean();
            getFlowTemplateSelected();

            if (selectedFlowTemplate != null) {
                selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(selectedFlowTemplate.getFlowTemplatesId());
                flowTemplateName = selectedFlowTemplate.getFlowTemplateName();
                isEdit = true;
                rebuildMapFlowAction(new ArrayList<ActionOfFlow>(selectedFlowTemplate.getActionOfFlows()));
//				System.out.println("status: " + selectedFlowTemplate.getStatus());
//				System.out.println("isApprove: " + isApproveTemplate);
//				System.out.println("isPreApprove: " + isPreApproveTemplate);
//				System.out.println("========================================");
            }
            logAction = LogUtils.addContent("", "FlowTeplate: " + (selectedFlowTemplate == null ? "" : selectedFlowTemplate.toString()));
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        loadDefaultAddToTemplate();
    }

    private void rebuildMapFlowAction(List<ActionOfFlow> lstActionFlow) {
        try {
            mapGroupName = new LinkedHashMap<>();
            if (lstActionFlow.size() > 0) {
                Collections.sort(lstActionFlow, new Comparator<ActionOfFlow>() {

                    public int compare(ActionOfFlow object1, ActionOfFlow object2) {
                        if (Long.compare(object1.getGroupActionOrder(), object2.getGroupActionOrder()) == 0) {
                            return Long.compare(object1.getStepNumberLabel(), object2.getStepNumberLabel());
                        } else {
                            return Long.compare(object1.getGroupActionOrder(), object2.getGroupActionOrder());
                        }
                    }
                });

                int index = 0;
                Map<String, Integer> mapActionName = new HashMap<>();
// 			    for (ActionOfFlow actionFlow : lstActionFlow) {
                int size = lstActionFlow.size();
                for (int i = 0; i < size; i++) {
                    if (mapGroupName.get(lstActionFlow.get(i).getGroupActionName()) == null) {
                        mapGroupName.put(lstActionFlow.get(i).getGroupActionName(), index);
                        mapCloneType.put(lstActionFlow.get(i).getGroupActionName(), lstActionFlow.get(i).getCloneType());
                        mapIsStopCmdFail.put(lstActionFlow.get(i).getGroupActionName(), lstActionFlow.get(i).getStopCmdFail());
                        actionOfFlowss.add(new LinkedList<ActionOfFlow>());
                        index++;
                    }

                    ActionOfFlow actionFlow = lstActionFlow.get(i);
                    if (mapActionName.get(actionFlow.getStepNumberLabel() + "_" + actionFlow.getGroupActionOrder() + "_" + actionFlow.getAction().getActionId()) == null) {

                        mapActionName.put(actionFlow.getStepNumberLabel() + "_" + actionFlow.getGroupActionOrder() + "_" + actionFlow.getAction().getActionId(), 1);

                        actionFlow.setPreStepsNumber(new ArrayList<String>(Arrays.asList(lstActionFlow.get(i).getPreviousStep() + "")));
                        actionFlow.setPreStepsNumberLabel(lstActionFlow.get(i).getPreviousStep() + "");

                        actionFlow.setPreStepsCondition(lstActionFlow.get(i).getIfValue());
                        actionFlow.setActionFlowIds(new ArrayList<Long>(Arrays.asList(lstActionFlow.get(i).getStepNum())));
                        actionFlow.setLstNodeRunGroupAction(new ArrayList<List<NodeRunGroupAction>>());
                        actionFlow.getLstNodeRunGroupAction().add(lstActionFlow.get(i).getNodeRunGroupActions());
                        if (lstActionFlow.get(i).getIsRollback() != null) {
                            actionFlow.setRollbackStatus(lstActionFlow.get(i).getIsRollback().equals(Config.ROLLBACK_ACTION) ? true : false);
                        }
                        //20170824_hienhv4_add_start
                        if (lstActionFlow.get(i).getRemoveDuplicate() != null) {
                            actionFlow.setRemoveDuplicateStatus(lstActionFlow.get(i).getRemoveDuplicate().equals(1l));
                        }
                        if (lstActionFlow.get(i).getCloneCmdMap()!= null) {
                            actionFlow.setCloneCmdMapStatus(lstActionFlow.get(i).getCloneCmdMap().equals(1l));
                        }
                        if (lstActionFlow.get(i).getDecisionResultClone()!= null) {
                            actionFlow.setDecisionResultCloneStatus(lstActionFlow.get(i).getDecisionResultClone().equals(1l));
                        }
                        //20170824_hienhv4_add_end
                        if (lstActionFlow.get(i).getActionType()!= null) {
                            actionFlow.setActionManual(lstActionFlow.get(i).getActionType().equals(Config.ACTION_TYPE_MANUAL) ? true : false);
                        }
                        for (int j = i + 1; j < size; j++) {
                            if ((lstActionFlow.get(i).getStepNumberLabel().equals(lstActionFlow.get(j).getStepNumberLabel()))
                                    && (lstActionFlow.get(i).getGroupActionOrder().equals(lstActionFlow.get(j).getGroupActionOrder()))
                                    && (lstActionFlow.get(i).getAction().getActionId().equals(lstActionFlow.get(j).getAction().getActionId()))) {

                                actionFlow.getPreStepsNumber().add(lstActionFlow.get(j).getPreviousStep() + "");
                                actionFlow.setPreStepsNumberLabel(actionFlow.getPreStepsNumberLabel().concat(",").concat(lstActionFlow.get(j).getPreviousStep() + ""));

                                actionFlow.getActionFlowIds().add(lstActionFlow.get(j).getStepNum());
                                actionFlow.setPreStepsCondition(actionFlow.getPreStepsCondition().concat(",").concat(lstActionFlow.get(j).getIfValue()));
                                actionFlow.getLstNodeRunGroupAction().add(lstActionFlow.get(j).getNodeRunGroupActions());
                            } else {
                                break;
                            }
                        }

                        if (actionFlow.getPreStepsNumberLabel().endsWith(",")) {
                            actionFlow.setPreStepsNumberLabel(actionFlow.getPreStepsNumberLabel().substring(0, actionFlow.getPreStepsNumberLabel().length() - 1));
                        }
                        if (actionFlow.getPreStepsCondition().endsWith(",")) {
                            actionFlow.setPreStepsCondition(actionFlow.getPreStepsCondition().substring(0, actionFlow.getPreStepsCondition().length() - 1));
                        }
                        actionOfFlowss.get(actionOfFlowss.size() - 1).add(actionFlow);
                    }
                } // end loop for

                lstItemGroupName = new ArrayList<>();
                for (Map.Entry<String, Integer> entry : mapGroupName.entrySet()) {
                    lstItemGroupName.add(new SelectItem(entry.getKey(), entry.getKey()));
                }
            } else {
                MessageUtil.setWarnMessageFromRes("label.war.template.empty");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public boolean isStopCmdFail() {
        return isStopCmdFail;
    }

    public void setStopCmdFail(boolean stopCmdFail) {
        isStopCmdFail = stopCmdFail;
    }

    public Map<String, Boolean> getMapIsStopCmdFail() {
        return mapIsStopCmdFail;
    }

    public void setMapIsStopCmdFail(Map<String, Boolean> mapIsStopCmdFail) {
        this.mapIsStopCmdFail = mapIsStopCmdFail;
    }

    public void addActiontoFlowTemplate(TreeNode selectedTreeNodeAction) {
        try {
            if (selectedTreeNodeAction != null
                    && selectedGroupActionName != null
                    && !selectedGroupActionName.trim().isEmpty()) {
                Action action = (Action) selectedTreeNodeAction.getData();
                ActionOfFlow actionOfFlow = new ActionOfFlow();
                actionOfFlow.setAction(action);
                actionOfFlow.setFlowTemplates(selectedFlowTemplate);
                actionOfFlow.setStepNumberLabel(Long.valueOf(actionOfFlowss.get(mapGroupName.get(selectedGroupActionName)).size() + 1));
                actionOfFlow.setGroupActionName(selectedGroupActionName);
                actionOfFlow.setCloneType(mapCloneType.get(selectedGroupActionName));
                actionOfFlow.setStopCmdFail(mapIsStopCmdFail.get(selectedGroupActionName));
                actionOfFlow.setIsRollback(Config.EXECUTE_ACTION);
                actionOfFlow.setCloneCmdMap(0l);
                actionOfFlow.setRemoveDuplicate(1l);
                actionOfFlow.setRemoveDuplicateStatus(true);
                actionOfFlow.setDecisionResultClone(0l);

                if (actionOfFlowss.get(mapGroupName.get(selectedGroupActionName)) == null
                        || actionOfFlowss.get(mapGroupName.get(selectedGroupActionName)).isEmpty()) {
                    actionOfFlow.setGroupActionOrder(Long.valueOf(getIndexGroupOrder(actionOfFlowss)));
                } else {
                    List<ActionOfFlow> lstActionFlow = actionOfFlowss.get(mapGroupName.get(selectedGroupActionName));
                    actionOfFlow.setGroupActionOrder(lstActionFlow.get(0).getGroupActionOrder());
                }
                actionOfFlowss.get(mapGroupName.get(selectedGroupActionName)).add(actionOfFlow);
                selectedGroupActionName = null;
                RequestContext.getCurrentInstance().execute("PF('dlgActionFlow').hide()");
            } else {
                MessageUtil.setErrorMessageFromRes("label.error.templateEmpty");
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private int getIndexGroupOrder(List<List<ActionOfFlow>> lstOfActionFlows) {
        int indexGroupOrder = 0;

        int indexOfActionFlowss = mapGroupName.get(selectedGroupActionName);
        if (actionOfFlowss.size() == 1) {
            indexGroupOrder = 1;

        } else if (indexOfActionFlowss <= (actionOfFlowss.size() - 1)) {

            boolean isGettedIndxGroup = false;
            // duyet ve cuoi cua list
            int countDown = 0;
            for (int i = indexOfActionFlowss + 1; i < actionOfFlowss.size(); i++) {
                countDown++;
                if (!actionOfFlowss.get(i).isEmpty()) {
                    indexGroupOrder = actionOfFlowss.get(i).get(0).getGroupActionOrder().intValue() - countDown;
                    isGettedIndxGroup = true;
                    break;
                }
            }

            // Duyet nguoc lai dau cua list
            if (!isGettedIndxGroup) {
                int countUp = 0;
                for (int i = indexOfActionFlowss - 1; i >= 0; i--) {
                    countUp++;
                    if (!actionOfFlowss.get(i).isEmpty()) {
                        indexGroupOrder = actionOfFlowss.get(i).get(0).getGroupActionOrder().intValue() + countUp;
                        isGettedIndxGroup = true;
                        break;
                    }
                }
            }

            // Neu tat ca cac action group deu trong thi tra ve index group trong actions
            if (!isGettedIndxGroup) {
                indexGroupOrder = indexOfActionFlowss + 1;
            }

        } else {
            for (List<ActionOfFlow> lstActionFlow : lstOfActionFlows) {
                if (!lstActionFlow.isEmpty()
                        && indexGroupOrder < lstActionFlow.get(0).getGroupActionOrder().intValue()) {
                    indexGroupOrder = lstActionFlow.get(0).getGroupActionOrder().intValue();
                }
            }
            indexGroupOrder += 1;
        }
        return indexGroupOrder;
    }

    //hienhv4_20160926_di chuyen dau viec_start
    public void moveUp(int indexToUp) {
        try {
            if (indexToUp == 0) {
                MessageUtil.setErrorMessageFromRes("message.move.up.cannot");
            }
            int indexToDown = indexToUp - 1;

            swapDvOrder(indexToDown, indexToUp);

            MessageUtil.setInfoMessageFromRes("message.move.up.success");
        } catch (Exception ex) {
            MessageUtil.setErrorMessageFromRes("message.move.up.fail");
            logger.error(ex.getMessage(), ex);
        }
    }

    private void swapDvOrder(int indexToDown, int indexToUp) {
        String groupNameDown = "";
        String groupNameUp = "";
        for (String key : mapGroupName.keySet()) {
            if (indexToDown == mapGroupName.get(key)) {
                groupNameDown = key;
            } else if (indexToUp == mapGroupName.get(key)) {
                groupNameUp = key;
            }
        }
        mapGroupName.put(groupNameDown, -1);
        mapGroupName.put(groupNameUp, indexToDown);
        mapGroupName.put(groupNameDown, indexToUp);

        List<ActionOfFlow> actionFlowDowns = new ArrayList<>();
        List<ActionOfFlow> actionFlowUps = new ArrayList<>();
        for (int i = 0; i < actionOfFlowss.size(); i++) {
            if (i == indexToDown) {
                actionFlowDowns = actionOfFlowss.get(indexToDown);
            } else if (i == indexToUp) {
                actionFlowUps = actionOfFlowss.get(indexToUp);
            }
        }
        actionOfFlowss.set(indexToUp, actionFlowDowns);
        actionOfFlowss.set(indexToDown, actionFlowUps);
        if (!actionFlowDowns.isEmpty() && !actionFlowUps.isEmpty()) {
            long temp = actionFlowDowns.get(0).getGroupActionOrder();
            for (ActionOfFlow action : actionFlowDowns) {
                action.setGroupActionOrder(actionFlowUps.get(0).getGroupActionOrder());
            }

            for (ActionOfFlow action : actionFlowUps) {
                action.setGroupActionOrder(temp);
            }
        } else if (actionFlowUps.isEmpty()) {
            for (ActionOfFlow action : actionFlowDowns) {
                action.setGroupActionOrder(action.getGroupActionOrder() + 1);
            }
        } else {
            for (ActionOfFlow action : actionFlowUps) {
                action.setGroupActionOrder(action.getGroupActionOrder() - 1);
            }
        }

        SelectItem itemGroupUp = null;
        SelectItem itemGroupDown = null;
        for (int i = 0; i < lstItemGroupName.size(); i++) {
            if (i == indexToDown) {
                itemGroupDown = lstItemGroupName.get(indexToDown);
            } else if (i == indexToUp) {
                itemGroupUp = lstItemGroupName.get(indexToUp);
            }
        }
        lstItemGroupName.set(indexToUp, itemGroupDown);
        lstItemGroupName.set(indexToDown, itemGroupUp);
    }

    public void moveDown(int indexToDown) {
        try {
            if (indexToDown == lstItemGroupName.size() - 1) {
                MessageUtil.setErrorMessageFromRes("message.move.down.cannot");
            }
            int indexToUp = indexToDown + 1;

            swapDvOrder(indexToDown, indexToUp);

            MessageUtil.setInfoMessageFromRes("message.move.down.success");
        } catch (Exception ex) {
            MessageUtil.setErrorMessageFromRes("message.move.down.fail");
            logger.error(ex.getMessage(), ex);
        }
    }
    //hienhv4_20160926_di chuyen dau viec_end

    public void saveOrUpdateGroupName() {
        try {
            if (newGroupActionName != null && !newGroupActionName.trim().isEmpty()) {
                if (newGroupActionName.trim().length() > MAX_LENGTH_GROUP_NAME) {
                    MessageUtil.setErrorMessageFromRes("label.validate.length.groupname");
                    return;
                }
                // kiem tra xem ten group da ton tai hay chua
                int count = 0;
                for (SelectItem item : lstItemGroupName) {
                    if (item.getLabel().trim().equalsIgnoreCase(newGroupActionName.trim())) {
                        count++;
                    }
                }

                // Neu la them moi group name
                if (!isChangeGroupActionName) {
                    if (count > 0) {
                        MessageUtil.setErrorMessageFromRes("label.error.exist");
                    } else {
                        actionOfFlowss.add(new LinkedList<ActionOfFlow>());
                        mapGroupName.put(newGroupActionName, actionOfFlowss.size() - 1);

                        lstItemGroupName.add(new SelectItem(newGroupActionName, newGroupActionName));
                        mapCloneType.put(newGroupActionName, cloneType);
                        cloneType = 1L;
                        mapIsStopCmdFail.put(newGroupActionName, isStopCmdFail);
                        isStopCmdFail = false;
                        newGroupActionName = "";
                        MessageUtil.setInfoMessageFromRes("label.action.updateOk");
                        RequestContext.getCurrentInstance().execute("PF('dlgAddNewGroupName').hide()");
                    }
                    // Neu la cap nhat group name
                } else {
                    if (count > 1) {
                        MessageUtil.setErrorMessageFromRes("error.group.action.exist");
                    } else {
                        Integer indexGroupAction = mapGroupName.get(oldGroupActionName.trim());
                        if (indexGroupAction != null && actionOfFlowss.get(indexGroupAction) != null) {

                            for (int i = 0; i < actionOfFlowss.get(indexGroupAction).size(); i++) {
                                actionOfFlowss.get(indexGroupAction).get(i).setGroupActionName(newGroupActionName);
                            }
                        }

                        int index = 0;
                        for (SelectItem item : lstItemGroupName) {
                            if (item.getLabel().trim().equalsIgnoreCase(oldGroupActionName.trim())) {
                                lstItemGroupName.remove(index);
                                break;
                            }
                            index++;
                        }
                        lstItemGroupName.add(index, new SelectItem(newGroupActionName.trim(), newGroupActionName.trim()));
                        mapGroupName.remove(oldGroupActionName);
                        mapGroupName.put(newGroupActionName, indexGroupAction);
                        mapCloneType.put(newGroupActionName, cloneType);
                        mapIsStopCmdFail.put(newGroupActionName, isStopCmdFail);
                        newGroupActionName = "";
                        MessageUtil.setInfoMessageFromRes("label.action.updateOk");
                        RequestContext.getCurrentInstance().execute("PF('dlgAddNewGroupName').hide()");
                    }
                }
            } else {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
            }

        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.require.set.data");
            logger.error(e.getMessage(), e);
        } finally {
            isChangeGroupActionName = false;
            oldGroupActionName = null;
            isAddNewGroupName = false;
        }
        loadDefaultAddToTemplate();
    }

    private void loadDefaultAddToTemplate() {
        defaultAddToTemplate = new Boolean[mapGroupName.size()];
        for (int i = 0; i < mapGroupName.size(); i++) {
            defaultAddToTemplate[i] = false;
        }

    }

    public void prepareRenameGroup(int index) {
        oldGroupActionName = getActionGroupNameData(index);
        isChangeGroupActionName = true;
        newGroupActionName = getActionGroupNameData(index);
        cloneType = mapCloneType.get(oldGroupActionName);
        isStopCmdFail = mapIsStopCmdFail.get(oldGroupActionName);
    }

    public void prepareAddGroup() {
        isChangeGroupActionName = false;
        newGroupActionName = null;
        cloneType = 1L;
        isStopCmdFail = false;

    }

    public void deleteGroup(int index) {
        try {

            List<ActionOfFlow> listActionFlow = actionOfFlowss.get(index);

            // Kiem tra xem danh sach action flow da duoc sinh MOP hay chua
            if (listActionFlow != null && !listActionFlow.isEmpty()) {
                //huynx6 edited Nov 29, 2016
                List<Long> tmps = new ArrayList<Long>();
                for (ActionOfFlow actionFLow : listActionFlow) {
                    tmps.add(actionFLow.getStepNum());
                }
                if (tmps.size() > 0) {
                    Map<String, Collection<?>> map = new HashMap<String, Collection<?>>();
                    map.put("stepNums", tmps);
                    List<NodeRunGroupAction> tmps2 = new NodeRunGroupActionServiceImpl().findListWithIn("from NodeRunGroupAction where id.stepNum in (:stepNums)", -1, -1, map);
                    if (tmps2.size() > 0) {
                        MessageUtil.setErrorMessageFromRes("label.group.action.created.mop");
                        return;
                    }
                }
            }

            for (ActionOfFlow actionFlow : listActionFlow) {
                if (actionFlow.getActionFlowIds() != null) {
                    for (Long id : actionFlow.getActionFlowIds()) {
                        try {
                            ActionOfFlow actionFlowDel = new ActionOfFlowServiceImpl().findById(id);
                            if (actionFlowDel != null) {
                                lstActionFlowDel.add(actionFlowDel);
                            }
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                }
            }

//			lstActionFlowDel.addAll(listActionFlow);
            listActionFlow.clear();
            actionOfFlowss.remove(index);

            for (Iterator<String> iterator = mapGroupName.keySet().iterator(); iterator.hasNext();) {
                String groupName = iterator.next();
                if (mapGroupName.get(groupName) != null && mapGroupName.get(groupName) == index) {
                    iterator.remove();
                    mapCloneType.remove(groupName);
                    mapIsStopCmdFail.remove(groupName);
                    continue;
                }
                if (mapGroupName.get(groupName) != null && mapGroupName.get(groupName) > index) {
                    mapGroupName.put(groupName, mapGroupName.get(groupName) - 1);
                }
            }
        } catch (Exception e) {
            //MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
            logger.error(e.getMessage(), e);
        }
        lstItemGroupName.clear();
        for (Map.Entry<String, Integer> entry : mapGroupName.entrySet()) {
            lstItemGroupName.add(new SelectItem(entry.getKey(), entry.getKey()));
        }
        loadDefaultAddToTemplate();
    }

    public void renameGroup() {
//		if ( newGroupActionName != null && !newGroupActionName.trim().isEmpty()) {
//			if (newGroupActionName.trim().length() > MAX_LENGTH_GROUP_NAME) {
//				MessageUtil.setErrorMessageFromRes("label.validate.length.groupname");
//				return;
//			}
//			// kiem tra xem ten group da ton tai hay chua
//			int countExist = 0;
//			for (SelectItem item : lstItemGroupName) {
//				if (item.getLabel().trim().equals(newGroupActionName.trim())) {
//					countExist++;
//				}
//			}
//			
//			if (countExist > 1) {
//				MessageUtil.setErrorMessageFromRes("label.error.exist");
//			} else {
//				List<ActionOfFlow> lstActionFlow = new ArrayList<>();
//				lstActionFlow.addAll(mapActionOfFlow.get(oldGroupActionName));
//				mapActionOfFlow.remove(oldGroupActionName);
//				mapActionOfFlow.put(newGroupActionName, lstActionFlow);
//				
//				int index = 0;
//				for(SelectItem item : lstItemGroupName) {
//					if (item.getLabel().trim().equals(oldGroupActionName.trim())) {
//						lstItemGroupName.remove(index);
//						break;
//					}
//					index++;
//				}
//				
//				lstItemGroupName.add(index, new SelectItem(newGroupActionName.trim(), newGroupActionName.trim()));
//				newGroupActionName = "";
//				MessageUtil.setInfoMessageFromRes("label.action.updateOk");
//				RequestContext.getCurrentInstance().execute("PF('dlgAddNewGroupName').hide()");
//			}
//		} else {
//			MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
//		}
    }

    public void reorderDataTable(ReorderEvent event) {
        try {
            DataTable actionFlowData = (DataTable) event.getSource();
            ActionOfFlow actionFlow = (ActionOfFlow) actionFlowData.getRowData();

            for (int i = 0; i < actionOfFlowss.get(mapGroupName.get(actionFlow.getGroupActionName())).size(); i++) {
                actionOfFlowss.get(mapGroupName.get(actionFlow.getGroupActionName())).get(i).setStepNumberLabel(Long.valueOf(i + 1));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void approveTemplateLevel() {
        if (new SessionUtil().isActionAdmin()) {
            approvalTemplate(Config.APPROVALED_STATUS_LEVEL2);
        }
        if (isApproveTemplate) {
            if (selectedFlowTemplate.getStatus() == 1) {
                approvalTemplate(Config.APPROVALED_STATUS_LEVEL2);
            } else {
                MessageUtil.setWarnMessageFromRes("warn.template.must.pre.approved");
            }
        } else if (isPreApproveTemplate) {
            approvalTemplate(Config.APPROVALED_STATUS_LEVEL1);
        }
    }

    public void approvalTemplate(Integer templateStatus) {
        if (selectedFlowTemplate != null) {
            selectedFlowTemplate.setStatus(templateStatus);
            try {
                new FlowTemplatesServiceImpl().execteBulk("update FlowTemplates set status = ? where flowTemplatesId =?", templateStatus, selectedFlowTemplate.getFlowTemplatesId());
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("label.action.updateFail");
                logger.error(e.getMessage(), e);
            }

        } else {
            MessageUtil.setErrorMessageFromRes("datatable.empty");
        }
        logAction = LogUtils.addContent("", "FlowTeplate: " + (selectedFlowTemplate == null ? "" : selectedFlowTemplate.toString()));
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void cancelApprovalTemplate() {
        if (selectedFlowTemplate != null) {
            logAction = LogUtils.addContent("", "FlowTeplate: " + selectedFlowTemplate.toString());
            selectedFlowTemplate.setStatus(Config.APPROVAL_STATUS_DEFAULT);
            try {
                new FlowTemplatesServiceImpl().execteBulk("update FlowTemplates set status = ? where flowTemplatesId =?", Config.APPROVAL_STATUS_DEFAULT, selectedFlowTemplate.getFlowTemplatesId());
//				LinkedHashMap<String, String> orders = new LinkedHashMap<>();
//				orders.put("flowTemplateName", "ASC");
//				lstFlowTemplate = new FlowTemplatesServiceImpl().findList(null, orders);
                onChangeFlowTemplate();
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
                logAction = LogUtils.addContent(logAction, "Result: OK ");
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("label.action.updateFail");
                logAction = LogUtils.addContent(logAction, "Result: NOK");
                logger.error(e.getMessage(), e);
            }

        } else {
            MessageUtil.setErrorMessageFromRes("datatable.empty");
        }

        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void prepareRenameTemplate() {
        getFlowTemplateSelected();


        flowTemplateName = selectedFlowTemplate.getFlowTemplateName();

        if (selectedFlowTemplate.getTemplateGroup() != null) {
            selectedResetType = selectedFlowTemplate.getResetType();
        }
        selectTemplateGroup = selectedFlowTemplate.getTemplateGroup();
        isEdit = true;
        logAction = LogUtils.addContent("", "FlowTemplate: " + selectedFlowTemplate.toString());
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void updateFlowTemplate() {
        try {
            // kiem tra xem ten template da ton tai trong csdl hay chu
            if (flowTemplateName == null || flowTemplateName.trim().isEmpty() || selectTemplateGroup == null) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                return;
            } else if (!isEdit && checkExistTemplate(flowTemplateName)) {
                MessageUtil.setErrorMessageFromRes("label.error.exist");
                return;
            } else if (flowTemplateName.replaceAll(" +", " ").trim().length() > MAX_LENGTH_TEMPLATE) {
                MessageUtil.setErrorMessageFromRes("label.error.maxlength.temp");
                return;
            }

            if (!isEdit) {
                selectedFlowTemplate = new FlowTemplates();
                selectedFlowTemplate.setActionOfFlows(new ArrayList<ActionOfFlow>());
            }
            selectedFlowTemplate.setCreateBy(userName);
            selectedFlowTemplate.setCreateDate(new Date());
            selectedFlowTemplate.setFlowTemplateName(flowTemplateName.replaceAll(" +", " ").trim());
            selectedFlowTemplate.setTemplateGroup(selectTemplateGroup);
            if ("RESET".equals(selectTemplateGroup.getGroupName())) {
                selectedFlowTemplate.setResetType(selectedResetType);
            }
            new FlowTemplatesServiceImpl().saveOrUpdate(selectedFlowTemplate);
            Map<String, String> orders = new LinkedHashMap<>();
            orders.put("flowTemplateName", "ASC");
            lstFlowTemplate = new FlowTemplatesServiceImpl().findList(null, orders);
            RequestContext.getCurrentInstance().execute("PF('dlgAddNewTemplate').hide()");
            MessageUtil.setInfoMessageFromRes("label.action.updateOk");

            isEdit = false;
            flowTemplateName = "";
            actionOfFlowss = new LinkedList<>();
            selectedFlowTemplate = new FlowTemplates();
            selectTemplateGroup = new TemplateGroup();
            logAction = LogUtils.addContent("", "FlowTemplate: " + selectedFlowTemplate.toString());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
        }
    }

    public void onSelectCopyTemplate() {
        getFlowTemplateSelected();

        isEdit = false;
        copyFlowTemplateName = "";
    }

    public void prepareCopyTemplate() {
        try {
            actionOfFlowss.clear();

            // kiem tra xem ten template da ton tai trong csdl hay chua
            if (copyFlowTemplateName == null || copyFlowTemplateName.trim().isEmpty()) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                return;
            } else if (checkExistTemplate(copyFlowTemplateName)) {
                MessageUtil.setErrorMessageFromRes("label.error.exist");
                return;
            }

            List<ActionOfFlow> lstActionFlow = selectedFlowTemplate.getActionOfFlows();
            List<ParamGroup> lstParamGroup = selectedFlowTemplate.getParamGroups();

//			if (lstActionFlow != null) {
//				actionOfFlowss = new LinkedList<>(); 
//				rebuildMapFlowAction(lstActionFlow);
//			}
            selectedFlowTemplate = new FlowTemplates();
            selectedFlowTemplate.setFlowTemplateName(copyFlowTemplateName);
            selectedFlowTemplate.setCreateDate(new Date());
            selectedFlowTemplate.setCreateBy(userName);
            selectedFlowTemplate.setFlowTemplatesId(null);
            selectedFlowTemplate.setActionOfFlows(new ArrayList<ActionOfFlow>());
            selectedFlowTemplate.setParamGroups(null);
            selectedFlowTemplate.setFlowTemplates(null);
            selectedFlowTemplate.setFlowTemplateses(null);
            selectedFlowTemplate.setStatus(Config.APPROVAL_STATUS_DEFAULT);

            saveCopyTempalte(lstActionFlow, lstParamGroup);

            RequestContext.getCurrentInstance().execute("PF('dlgCopyTemplate').hide()");

        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }
    }

    public void saveCopyTempalte(List<ActionOfFlow> lstActionFlow, List<ParamGroup> lstParamGroup) {
        try {
            Long flowTemplateId = new FlowTemplatesServiceImpl().save(selectedFlowTemplate);
            selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(flowTemplateId);

            List<ParamInOut> lstParamInOutClone = getLstParamInOut(lstActionFlow);

            // Luu danh sach action flow cua template
            if (!lstActionFlow.isEmpty()) {

                for (int i = 0; i < lstActionFlow.size(); i++) {
                    lstActionFlow.get(i).setFlowTemplates(selectedFlowTemplate);
                    lstActionFlow.get(i).setStepNum(null);
                    lstActionFlow.get(i).setNodeRunGroupActions(null);
                }
                new ActionOfFlowServiceImpl().saveOrUpdate(lstActionFlow);
            }

            selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(flowTemplateId);
            List<ActionOfFlow> lstActionFlowTmp = selectedFlowTemplate.getActionOfFlows();

            /*
             *  Luu danh sach param group default cua template
             */
            if (lstParamGroup != null && !lstParamGroup.isEmpty()) {
                for (int i = 0; i < lstParamGroup.size(); i++) {
                    ParamGroupId paramGroupId = lstParamGroup.get(i).getId();
                    paramGroupId.setFlowTemplateId(flowTemplateId);
                    lstParamGroup.get(i).setFlowTemplates(selectedFlowTemplate);
                }
                new ParamGroupServiceImpl().saveOrUpdate(lstParamGroup);
            }

            /*
             * Luu danh sach param in out cua template
             */
            savedParamInOutClone(lstActionFlowTmp, lstParamInOutClone);

            /*
             * Build lai hien thi action flow cua template
             */
            actionOfFlowss = new LinkedList<>();
            rebuildMapFlowAction(lstActionFlowTmp);

            // add to list template selec item
            Map<String, String> orders = new LinkedHashMap<>();
            orders.put("flowTemplateName", "ASC");
            lstFlowTemplate = new FlowTemplatesServiceImpl().findList(null, orders);

            MessageUtil.setInfoMessageFromRes("label.action.updateOk");
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }

    }

    /*
     * Luu danh sach param in out cua template
     */
    private List<ParamInOut> savedParamInOutClone(List<ActionOfFlow> lstActionFlow, List<ParamInOut> lstParamInOut) {
        List<ParamInOut> lstParamInOutClone = new ArrayList<>();
        if (lstActionFlow != null
                && !lstActionFlow.isEmpty()
                && lstParamInOut != null
                && !lstParamInOut.isEmpty()) {
            try {
                Map<String, ActionOfFlow> mapActionFlow = new HashMap<>();
                for (ActionOfFlow actionFlow : lstActionFlow) {
                    mapActionFlow.put(actionFlow.getGroupActionOrder() + "_" + actionFlow.getStepNumberLabel(), actionFlow);
                }

                Map<String, Integer> mapParamInOutKey = new HashMap<>();
                for (ParamInOut param : lstParamInOut) {
                    ActionOfFlow actionFlowInput = mapActionFlow.get(param.getActionOfFlowByActionFlowInId().getGroupActionOrder() + "_" + param.getActionOfFlowByActionFlowInId().getStepNumberLabel());
                    ActionOfFlow actionFlowOutput = mapActionFlow.get(param.getActionOfFlowByActionFlowOutId().getGroupActionOrder() + "_" + param.getActionOfFlowByActionFlowOutId().getStepNumberLabel());

                    ParamInOutId paramId = param.getId();
                    String key = paramId.getActionCommandInputId() + "CMDIN_"
                            + actionFlowInput.getStepNum() + "ACIN_"
                            + paramId.getParamInputId() + "PAIN_"
                            + param.getActionCommandByActionCommandOutputId().getActionCommandId() + "CMDOUT_"
                            + param.getActionOfFlowByActionFlowOutId().getStepNum() + "ACOUT";

                    if (mapParamInOutKey.get(key) == null) {
                        mapParamInOutKey.put(key, 1);

                        paramId.setActionFlowInId(actionFlowInput.getStepNum());

                        param.setActionOfFlowByActionFlowInId(actionFlowInput);
                        param.setActionOfFlowByActionFlowOutId(actionFlowOutput);
                        lstParamInOutClone.add(param);
                    }
                }

                new ParamInOutServiceImpl().saveOrUpdate(lstParamInOut);

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return lstParamInOutClone;
    }

    /*
     * Lay ra danh sach param in out cua template can clone
     */
    private List<ParamInOut> getLstParamInOut(List<ActionOfFlow> lstActionFlow) {
        List<ParamInOut> lstParamInOut = new ArrayList<>();
        int size = lstActionFlow.size();
        for (int i = size - 1; i >= 0; i--) {

            ActionOfFlow actionFlow = lstActionFlow.get(i);
            for (ActionDetail actionDetail : actionFlow.getAction().getActionDetails()) {

                for (ActionCommand actionCmd : actionDetail.getActionCommands()) {

                    for (ParamInput param : actionCmd.getCommandDetail().getParamInputs()) {

                        ParamInOut paramInout = getParamInOut(param.getParamInputId(),
                                actionCmd.getActionCommandId(),
                                actionFlow.getStepNum());
                        if (paramInout != null) {
                            lstParamInOut.add(paramInout);
                        }
                    }
                }
            }
        }

        return lstParamInOut;
    }

    public void prepareDelTemplate() {
        getFlowTemplateSelected();
    }

    public void deleteTemplate() {
        if (selectedFlowTemplate != null) {
            if (selectedFlowTemplate.getFlowRunActions() != null
                    && !selectedFlowTemplate.getFlowRunActions().isEmpty()) {
                MessageUtil.setErrorMessageFromRes("label.error.delete.template");
            } else {
                try {
                    List<ActionOfFlow> actionOfFlows = selectedFlowTemplate.getActionOfFlows();
                    List<Long> stepNums = new ArrayList<Long>();
                    for (ActionOfFlow actionOfFlow : actionOfFlows) {
                        stepNums.add(actionOfFlow.getStepNum());
                    }
                    Object[] objs = new ParamGroupServiceImpl().openTransaction();

                    Session session = (Session) objs[0];
                    Transaction tx = (Transaction) objs[1];
                    new ParamGroupServiceImpl().execteBulk2("delete from ParamGroup where id.flowTemplateId = ?",
                            session, tx, false, selectedFlowTemplate.getFlowTemplatesId());
                    for (Long stepNum : stepNums) {
                        new ParamInOutServiceImpl().execteBulk2("delete from ParamInOut where actionOfFlowByActionFlowOutId.stepNum = ? or id.actionFlowInId = ?",
                                session, tx, false, stepNum, stepNum);

                    }
                    new FlowTemplatesServiceImpl().delete(selectedFlowTemplate, session, tx, true);
                    Map<String, String> orders = new LinkedHashMap<>();
                    orders.put("flowTemplateName", "ASC");
                    lstFlowTemplate = new FlowTemplatesServiceImpl().findList(null, orders);
                    actionOfFlowss = new LinkedList<>();
                    mapGroupName = new LinkedHashMap<>();
                    MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
                } catch (Exception e) {
                    MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
                    logger.error(e.getMessage(), e);
                }
            }
            clean();
        } else {
            MessageUtil.setErrorMessageFromRes("message.choose.delete");
        }
    }

    private boolean checkExistTemplate(String templateName) {
        boolean check = false;
        if (templateName != null && !templateName.trim().isEmpty()) {
            try {
                Map<String, Object> filters = new HashMap<String, Object>();
                filters.put("flowTemplateName", templateName);

                List<FlowTemplates> lstTemplate = new FlowTemplatesServiceImpl().findListExac(filters, null);
                if (lstTemplate != null && !lstTemplate.isEmpty()) {
                    check = true;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return check;
    }

    private void getFlowTemplateSelected() {
        Map<String, String> params
                = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String templateId = params.get("templateId");
        if (templateId == null) {
            return;
        }
        for (FlowTemplates flowTemplates : lstFlowTemplate) {
            if (flowTemplates.getFlowTemplatesId().toString().equalsIgnoreCase(templateId)) {
                selectedFlowTemplate = flowTemplates;
                break;
            }
        }
        logAction = LogUtils.addContent("", "FlowTemplate: " + selectedFlowTemplate.toString());
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void deleteActionOfFlow(ActionOfFlow actionOfFlow, Integer idx) {
        try {
            if (actionOfFlow != null) {

                if (actionOfFlow.getStepNum() != null) {
					// Kiem tra xem action flow da duoc sinh mop hay chua

					//huynx6 edited Nov 29, 2016
                    //List<NodeRunGroupAction> tmps2 = new NodeRunGroupActionServiceImpl().findList("from NodeRunGroupAction where id.stepNum = ?", -1, -1, actionOfFlow.getStepNum());
//					if (tmps2.size()>0) {
//						MessageUtil.setErrorMessageFromRes("label.action.created.mop");
//						return;
//						
//					} else 
                    if (actionOfFlow.getActionFlowIds() != null && !actionOfFlow.getActionFlowIds().isEmpty()) {
                        for (Long id : actionOfFlow.getActionFlowIds()) {
                            try {
                                ActionOfFlow action = new ActionOfFlowServiceImpl().findById(id);
                                if (action != null) {
                                    lstActionFlowDel.add(action);
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
//						lstActionFlowDel.add(actionOfFlow);
                    }
                }

                Integer indexOfListActionOfFlow = mapGroupName.get(actionOfFlow.getGroupActionName());
                List<ActionOfFlow> actionOfFlows = actionOfFlowss.get(indexOfListActionOfFlow);
                actionOfFlows.remove(idx.intValue());

                // sap xep lai danh sach order
                for (int i = 0; i < actionOfFlows.size(); i++) {
                    actionOfFlows.get(i).setStepNumberLabel(Long.valueOf(i + 1));
                }

                MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            }
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
            logger.error(e.getMessage(), e);
        }
    }

    public void onCellEdit(CellEditEvent event) {

    }

    public void saveActionOfFlow() {
        Session session = null;
        Transaction tx = null;

        try {
            if (selectedFlowTemplate == null || selectedFlowTemplate.getFlowTemplatesId() == null) {
                MessageUtil.setInfoMessageFromRes("label.error.templateEmpty");
                return;
            } else if (!validateDataBeforeSave()) {
                return;
            } else {
                //quytv7_11102017 them tham so noi suy node mang start
                if (selectedFlowTemplate.getFormulaInterpolateNode() != null && !"".equals(selectedFlowTemplate.getFormulaInterpolateNode())
                        ) {
                    try {
                        new FlowTemplatesServiceImpl().execteBulk("update FlowTemplates set formulaInterpolateNode = ? where flowTemplatesId = ? ", selectedFlowTemplate.getFormulaInterpolateNode(), selectedFlowTemplate.getFlowTemplatesId());
//                    MessageUtil.setInfoMessageFromRes("label.action.updateOk");
                    } catch (Exception e) {
//                    MessageUtil.setErrorMessageFromRes("label.action.updateFail");
                        logger.error(e.getMessage(), e);
                    }
                }
                //quytv7_11102017 them tham so noi suy node mang start
            }

            try {
                // Luu lai danh sach action of flow moi
                List<ActionOfFlow> lstActionSaveId = new ArrayList<>();
                List<ActionOfFlow> lstActionSaveNoId = new ArrayList<>();
                List<ActionOfFlow> lstActionAll = new ArrayList<>();
                List<ActionOfFlow> lstSubAction = new ArrayList<ActionOfFlow>();

                for (List<ActionOfFlow> actions : actionOfFlowss) {
                    for (ActionOfFlow a : actions) {

                        lstSubAction = buildActionFlows(a, lstActionFlowDel);
                        if (lstSubAction != null && !lstSubAction.isEmpty()) {
                            for (ActionOfFlow action : lstSubAction) {
                                lstActionAll.add(action);
                                if (action.getStepNum() == null) {
                                    lstActionSaveNoId.add(action);
                                } else {
                                    lstActionSaveId.add(action);
                                }
                            }
                        }
                    }
                }

                /*
                 * Kiem tra cac dieu kien logic truoc khi thuc hien luu du lieu
                 */
                if (!validateIfCondition(lstActionAll)) {
                    return;

                } else {

                    Object[] objs = new ActionOfFlowServiceImpl().openTransaction();
                    session = (Session) objs[0];
                    tx = (Transaction) objs[1];

                    if (!lstActionFlowDel.isEmpty()) {
                        // Xoa cac param in out tham chieu den cac action xoa
                        for (ActionOfFlow actionFlow : lstActionFlowDel) {
                            new ParamInOutServiceImpl().execteBulk2("delete from ParamInOut where actionOfFlowByActionFlowOutId.stepNum = ? or id.actionFlowInId = ?",
                                    session, tx, false, actionFlow.getStepNum(), actionFlow.getStepNum());
                            new ActionOfFlowServiceImpl().execteBulk2("delete ActionOfFlow where stepNum =? ", session, tx, false, actionFlow.getStepNum());
                        }
                        //new ActionOfFlowServiceImpl().delete(lstActionFlowDel,session,tx,false);
                    }

                    if (lstActionFlowDel != null && !lstActionFlowDel.isEmpty()) {

                        for (Iterator<ActionOfFlow> iterator = lstActionFlowDel.iterator(); iterator.hasNext();) {
                            ActionOfFlow action = iterator.next();
                            if (action.getStepNum() == null) {
                                iterator.remove();
                            }
                        }
                    }

                    new ActionOfFlowServiceImpl().saveOrUpdate(lstActionSaveId, session, tx, false);
                    new ActionOfFlowServiceImpl().saveOrUpdate(lstActionSaveNoId, session, tx, false);
                    
                    //20171018_hienhv4_clone dau viec_start
                    List<String> groupNames = new ArrayList<>();
                    for (ActionOfFlow actionOfFlow : lstActionSaveId) {
                        if (!groupNames.contains(actionOfFlow.getGroupActionName())) {
                            groupNames.add(actionOfFlow.getGroupActionName());
                        }
                    }
                    for (ActionOfFlow actionOfFlow : lstActionSaveNoId) {
                        if (!groupNames.contains(actionOfFlow.getGroupActionName())) {
                            groupNames.add(actionOfFlow.getGroupActionName());
                        }
                    }
                    Map<String, Object> filters = new HashMap<>();
                    filters.put("flowTemplateId", selectedFlowTemplate.getFlowTemplatesId());
                    List<GroupActionClone> groupClones = new GroupActionCloneServiceImpl().findList(filters);
                    if (groupClones != null && !groupClones.isEmpty()) {
                        List<GroupActionClone> groupCloneToDelete = new ArrayList<>();
                        for (GroupActionClone group : groupClones) {
                            if (!groupNames.contains(group.getGroupName())) {
                                groupCloneToDelete.add(group);
                            }
                        }
                        new GroupActionCloneServiceImpl().delete(groupCloneToDelete, session, tx, false);
                    }
                    //20171018_hienhv4_clone dau viec_start
                    
                    tx.commit();
                }

            } catch (Exception e) {
                if (tx != null) {
                    if (tx.getStatus() != TransactionStatus.ROLLED_BACK) {
                        tx.rollback();
                    }
                }
                logger.error(e.getMessage(), e);
            } finally {
                if (session != null) {
                    try {
                        session.close();
                    } catch (Exception e2) {
                        logger.error(e2.getMessage(), e2);
                    }
                }
            }

            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("flowTemplateName", "ASC");
            lstFlowTemplate = new FlowTemplatesServiceImpl().findList(null, orders);
            selectedFlowTemplate = new FlowTemplatesServiceImpl().findById(selectedFlowTemplate.getFlowTemplatesId());

            actionOfFlowss = new LinkedList<>();
            rebuildMapFlowAction(selectedFlowTemplate.getActionOfFlows());

            MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            lstActionFlowDel.clear();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
        }

        lstItemGroupName.clear();
        for (Map.Entry<String, Integer> entry : mapGroupName.entrySet()) {
            lstItemGroupName.add(new SelectItem(entry.getKey(), entry.getKey()));
        }
        logAction = LogUtils.addContent("", "FlowTemplate: " + selectedFlowTemplate.toString());
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    private List<ActionOfFlow> buildActionFlows(ActionOfFlow actionFlow, List<ActionOfFlow> lstActionDel) {
        List<ActionOfFlow> lstActionFlowSave = new ArrayList<>();
        if (actionFlow != null) {
            try {

                List<String> lstPreConditionVal = Arrays.asList(actionFlow.getPreStepsCondition().trim().split(","));

                int index = 0;
                for (String preStep : actionFlow.getPreStepsNumber()) {

                    ActionOfFlow newActionFlow = new ActionOfFlow();
                    newActionFlow.setAction(actionFlow.getAction());
                    if (actionFlow.getDelayTime() != null
                            && actionFlow.getDelayTime() != 0) {
                        newActionFlow.setDelayTime(actionFlow.getDelayTime());
                    }
                    newActionFlow.setFlowTemplates(actionFlow.getFlowTemplates());
                    newActionFlow.setGroupActionName(actionFlow.getGroupActionName());
                    newActionFlow.setGroupActionOrder(actionFlow.getGroupActionOrder());
                    newActionFlow.setIndexParamValue(actionFlow.getIndexParamValue());
                    newActionFlow.setIsRollback(actionFlow.isRollbackStatus() ? Config.ROLLBACK_ACTION : Config.EXECUTE_ACTION);
                    newActionFlow.setActionType(actionFlow.isActionManual()? Config.ACTION_TYPE_MANUAL : Config.ACTION_TYPE_NORMAL);
                    newActionFlow.setStepNumberLabel(actionFlow.getStepNumberLabel());
                    //20170824_hienhv4_add_start
                    newActionFlow.setRemoveDuplicate(actionFlow.isRemoveDuplicateStatus() ? 1l : 0l);
                    newActionFlow.setCloneCmdMap(actionFlow.isCloneCmdMapStatus() ? 1l : 0l);
                    newActionFlow.setDecisionResultClone(actionFlow.isDecisionResultCloneStatus() ? 1l : 0l);
                    //20170824_hienhv4_add_end

                    newActionFlow.setStepNum(null);
                    newActionFlow.setIfValue(lstPreConditionVal.get(index));
                    newActionFlow.setPreviousStep(Long.valueOf(preStep));
                    newActionFlow.setCloneType(mapCloneType.get(actionFlow.getGroupActionName()));
                    newActionFlow.setStopCmdFail(mapIsStopCmdFail.get(actionFlow.getGroupActionName()));

                    lstActionFlowSave.add(newActionFlow);
                    index++;
                }

                if (actionFlow.getActionFlowIds() != null && !actionFlow.getActionFlowIds().isEmpty()) {

                    int maxSize = actionFlow.getActionFlowIds().size();
                    if (actionFlow.getActionFlowIds().size() > actionFlow.getPreStepsNumber().size()) {

                        for (int i = actionFlow.getPreStepsNumber().size(); i < actionFlow.getActionFlowIds().size(); i++) {
                            lstActionDel.add(new ActionOfFlowServiceImpl().findById(actionFlow.getActionFlowIds().get(i)));
                        }
                        maxSize = actionFlow.getPreStepsNumber().size();
                    }

                    for (int i = 0; i < maxSize; i++) {
                        lstActionFlowSave.get(i).setStepNum(actionFlow.getActionFlowIds().get(i));
                        //lstActionFlowSave.get(i).setNodeRunGroupActions(actionFlow.getLstNodeRunGroupAction().get(i));
                    }
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return lstActionFlowSave;
    }

    private boolean validateIfCondition(List<ActionOfFlow> lstActionOfFlow) {
        boolean check = true;
        if (actionOfFlowss != null) {

            // kiem tra xem da dien du lieu day du hay chua
            for (ActionOfFlow action : lstActionOfFlow) {
                if (action.getStepNumberLabel() == 1) {
                    if (action.getIsRollback().equals(Config.ROLLBACK_ACTION)) {
                        MessageUtil.setErrorMessageFromRes("label.err.first.action.condition");
                        return false;
                    } else if (action.getPreviousStep().intValue() != 0) {
                        MessageUtil.setErrorMessageFromRes("label.val.first.action.prestep");
                        return false;
                    }
                }
            }

			// kiem tra 2 cap tham so previous step va dieu kien co trung nhau
            // hay khong
            // Kiem tra xem 1 action co vuot qua 2 action tham chieu den hay
            // khong
            int size = lstActionOfFlow.size();
            for (int i = 0; i < size; i++) {
                int countReference = 0; // bien dem so lan tham chieu den action

                for (int j = 0; j < size; j++) {
                    if (i != j) {
                        if (lstActionOfFlow
                                .get(i)
                                .getGroupActionOrder()
                                .equals(lstActionOfFlow.get(j)
                                        .getGroupActionOrder())
                                && lstActionOfFlow
                                .get(i)
                                .getPreviousStep()
                                .equals(lstActionOfFlow.get(j)
                                        .getPreviousStep())
                                && lstActionOfFlow
                                .get(i)
                                .getIfValue()
                                .equals(lstActionOfFlow.get(j)
                                        .getIfValue())
                                && "1".equals(lstActionOfFlow.get(i).getIfValue())) {
                            MessageUtil.setErrorMessageFromRes("label.validate.prestep.condition.val");
                            return false;
                        }
                        if (lstActionOfFlow
                                .get(i)
                                .getGroupActionOrder()
                                .equals(lstActionOfFlow.get(j)
                                        .getGroupActionOrder())
                                && lstActionOfFlow
                                .get(j)
                                .getPreviousStep()
                                .equals(lstActionOfFlow.get(i)
                                        .getStepNumberLabel())) {
                            countReference++;
                            if (countReference > 5) {
                                MessageUtil
                                        .setErrorMessageFromRes("message.err.max.action.reference");
                                return false;
                            }
                        }
                    }
                } // end loop for
            }

            // kiem tra dieu kien lenh thuc thi theo sau lenh rollback
            for (int i = 0; i < size; i++) {
                if (lstActionOfFlow.get(i).getIsRollback()
                        .equals(Config.ROLLBACK_ACTION)) {

                    for (int j = 0; j < size; j++) {
                        if (i != j
                                && lstActionOfFlow
                                .get(i)
                                .getGroupActionOrder()
                                .equals(lstActionOfFlow.get(j)
                                        .getGroupActionOrder())
                                && lstActionOfFlow
                                .get(j)
                                .getPreviousStep()
                                .equals(lstActionOfFlow.get(i)
                                        .getStepNumberLabel())
                                && lstActionOfFlow.get(j).getIsRollback()
                                .equals(Config.EXECUTE_ACTION)) {
                            MessageUtil
                                    .setErrorMessageFromRes("label.val.action.exec.after.rollback");
                            return false;
                        }
                    }
                }
            }

            // Kiem tra dieu kien 1 action khong co qua 2 dieu kien re nhanh
//			for (List<ActionOfFlow> actionOfFlows : actionOfFlowss) {
//				for (int i = 0; i < size - 1; i++) {
//					ActionOfFlow actionOfFlow = lstActionOfFlow.get(i);
//					
//					if(actionOfFlow.getGroupActionOrder().equals(lstActionOfFlow.get(i+1).getGroupActionOrder())
//							&& actionOfFlow.getIsRollback().equals(Config.ROLLBACK_ACTION) && lstActionOfFlow.get(i+1).getIsRollback().equals(Config.ROLLBACK_ACTION)){
//						MessageUtil.setErrorMessageFromRes("error.two.action.consecutive");
//						return false;
//					}
//				}
//			}
        }
        return check;
    }

    private boolean validateDataBeforeSave() {
        boolean check = true;
        try {
            if (actionOfFlowss != null) {
                for (List<ActionOfFlow> actions : actionOfFlowss) {
                    if (check) {
                        for (ActionOfFlow action : actions) {
                            if (action.getPreStepsNumber() == null
                                    || action.getPreStepsNumber().isEmpty()
                                    || action.getPreStepsCondition() == null
                                    || action.getPreStepsCondition().isEmpty()) {
                                MessageUtil.setErrorMessageFromRes("label.validate.add.action_flow");
                                check = false;
                                break;
                            } else {
                                String pattern = "^[01]$";
                                Pattern p = Pattern.compile(pattern);
                                List<String> lstConditionVal = Arrays.asList(action.getPreStepsCondition().trim().split(","));
                                if (lstConditionVal != null && !lstConditionVal.isEmpty()) {
                                    for (String val : lstConditionVal) {
                                        Matcher m = p.matcher(val);
                                        if (!m.matches()) {
                                            MessageUtil.setErrorMessageFromRes("label.condition.action.tip");
                                            check = false;
                                            break;
                                        }
                                    }

                                    if (check) {
                                        // kiem tra xem do dai cua previous step va conditions co bang nhau hay khong
                                        if (action.getPreStepsNumber().size() != lstConditionVal.size()) {
                                            MessageUtil.setErrorMessageFromRes("message.err.condition.not.equal.prestep");
                                            check = false;
                                        }
                                    }
                                }
                            }
                        }
                    } else {

                        break;
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return check;
    }

    public void clean() {
        flowTemplateName = null;
        copyFlowTemplateName = null;
        isEdit = false;
        lstActionFlowDel = new ArrayList<>();
        lstItemGroupName = new ArrayList<>();
//		mapActionOfFlow = new LinkedHashMap<>();
        actionOfFlowss = new LinkedList<>();
        mapGroupName = new LinkedHashMap<>();
        lstParamGroup = new ArrayList<>();
        selectedParamGroup = new ParamGroup();
    }

    public List<SelectItem> buidLstPreStepItem(ActionOfFlow cmd) {
        List<SelectItem> lstItem = new ArrayList<>();
        Integer indexOfGroupAction = mapGroupName.get(cmd.getGroupActionName());
        if (indexOfGroupAction != null) {
            List<ActionOfFlow> lstActionFlow = actionOfFlowss.get(indexOfGroupAction);
            if (lstActionFlow != null) {
                for (int i = 0; i <= lstActionFlow.size(); i++) {
                    if (i >= cmd.getStepNumberLabel()) {
                        continue;
                    }
                    lstItem.add(new SelectItem(i, i + ""));
                }
                try {
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return lstItem;
    }

    public void buildParamGroup() {
        lstParamGroup = new ArrayList<>();
        isChangeGroupCode = false;
        Map<String, String> mapParamGroupKey = new HashMap<>();
        if (selectedFlowTemplate != null) {
            try {
                List<Action> lstAction = new ArrayList<>();
                Map<String, Object> filters = new HashMap<String, Object>();
                filters.put("flowTemplates.flowTemplatesId", selectedFlowTemplate.getFlowTemplatesId());
                List<ActionOfFlow> actionOfFlows = new ActionOfFlowServiceImpl().findList("from ActionOfFlow where flowTemplates.flowTemplatesId =?", -1, -1, selectedFlowTemplate.getFlowTemplatesId());
                for (ActionOfFlow actionFlow : actionOfFlows) {
                    lstAction.add(actionFlow.getAction());
                }

                if (!lstAction.isEmpty()) {
                    List<ActionDetail> lstActionDetail = new ArrayList<>();
                    for (Action action : lstAction) {
                        lstActionDetail.addAll(action.getActionDetails());
                    }

                    if (!lstActionDetail.isEmpty()) {
                        List<ActionCommand> lstActionCommand = new ArrayList<>();
                        for (ActionDetail actionDetail : lstActionDetail) {
                            lstActionCommand.addAll(actionDetail.getActionCommands());
                        }

                        if (!lstActionCommand.isEmpty()) {
                            List<ParamInput> lstParamInput = new ArrayList<>();
                            for (ActionCommand actionCmd : lstActionCommand) {
                                lstParamInput.addAll(actionCmd.getCommandDetail().getParamInputs());
                            }

                            if (!lstParamInput.isEmpty()) {

                                List<ParamGroup> paramGroups = selectedFlowTemplate.getParamGroups();
                                Map<ParamGroupId, ParamGroup> mapParamGroups = new HashMap<ParamGroupId, ParamGroup>();

                                for (ParamGroup paramGroup2 : paramGroups) {
                                    mapParamGroups.put(paramGroup2.getId(), paramGroup2);
                                }

                                for (ParamInput paramInput : lstParamInput) {
                                    String key = paramInput.getParamInputId() + "_" + selectedFlowTemplate.getFlowTemplatesId();

                                    if (mapParamGroupKey.get(key) == null) {
                                        mapParamGroupKey.put(key, key);
                                        ParamGroupId paramGroupId = new ParamGroupId(paramInput.getParamInputId(), selectedFlowTemplate.getFlowTemplatesId());
                                        if (mapParamGroups.get(paramGroupId) != null) {
                                            ParamGroup paramGroup = mapParamGroups.get(paramGroupId);
                                            lstParamGroup.add(paramGroup);
                                        } else {
                                            lstParamGroup.add(new ParamGroup(paramGroupId, paramInput, selectedFlowTemplate));
                                        }
                                    }
                                } // end loop for param input
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

            if (lstParamGroup != null && !lstParamGroup.isEmpty()) {
                RequestContext.getCurrentInstance().execute("PF('dlgParamDefault').show()");
            } else {
                MessageUtil.setErrorMessageFromRes("datatable.empty");
            }
        }
    }

    public static void main(String[] args) {
        try {
            String sql = "select * from action_detail de where de.action_id in "
                    + "(select ac.action_id from action_of_flow ac where ac.flow_templates_id = ?)";

            Map<String, Object> mapParamList = new HashMap<String, Object>();
            mapParamList.put("flow_template_id", 336);
            List<ActionDetail> lstActionDetail = (List<ActionDetail>) new ActionDetailServiceImpl().findListSQLWithPosParameters(null, sql, -1, -1, ActionDetail.class, null, 336l);
            System.out.println(((ActionDetail) lstActionDetail.get(0)).getUserName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void buildParamInOutData() {
        lstParamInOutObject = new ArrayList<>();
        if (selectedFlowTemplate != null) {
            try {
                int size = selectedFlowTemplate.getActionOfFlows().size();
                List<ActionOfFlow> lstActionFlow = selectedFlowTemplate.getActionOfFlows();
                for (int i = size - 1; i >= 0; i--) {

                    ActionOfFlow actionFlow = lstActionFlow.get(i);
                    for (ActionDetail actionDetail : actionFlow.getAction().getActionDetails()) {

                        for (ActionCommand actionCmd : actionDetail.getActionCommands()) {

                            for (ParamInput param : actionCmd.getCommandDetail().getParamInputs()) {

                                ParamInOut paramInout = getParamInOut(param.getParamInputId(),
                                        actionCmd.getActionCommandId(),
                                        actionFlow.getStepNum());
                                if (paramInout == null) {
                                    paramInout = new ParamInOut();
                                    paramInout.setActionCommandByActionCommandInputId(actionCmd);
                                    paramInout.setActionOfFlowByActionFlowInId(actionFlow);
                                    paramInout.setParamInput(param);
                                }

                                paramInout.setParamInOutOrder(lstParamInOutObject.size());
                                lstParamInOutObject.add(paramInout);
                            }
                        }
                    }
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        if (!lstParamInOutObject.isEmpty()) {
            RequestContext.getCurrentInstance().execute("PF('dlgParamReference').show()");
        } else {
            MessageUtil.setErrorMessageFromRes("datatable.empty");
        }
    }

    private ParamInOut getParamInOut(Long paramInputId, Long actionCmdInId, Long actionFlowInId) {
        ParamInOut paramInOut = null;
        ParamInOutId id = new ParamInOutId(actionCmdInId, paramInputId, actionFlowInId);

        try {
            paramInOut = new ParamInOutServiceImpl().findById(id);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            paramInOut = null;
        }
        return paramInOut;
    }

    public void buildLstActionSelect(ParamInOut param) {
        try {
            selectedParamInOut = param;
            lstActionFlow = new ArrayList<>();
            lstActionCommand = new ArrayList<>();
            if (param != null) {
                if (param.getActionOfFlowByActionFlowInId().getIsRollback() == null
                        || param.getActionOfFlowByActionFlowInId().getIsRollback() == 0) {
                    Map<String, Integer> filters = new HashMap<>();
                    for (ActionOfFlow action : selectedFlowTemplate.getActionOfFlows()) {

                        if (action.getGroupActionOrder() <= param.getActionOfFlowByActionFlowInId().getGroupActionOrder()
                                && filters.get(action.getStepNumberLabel() + "#" + action.getGroupActionOrder()) == null) {

                            filters.put(action.getStepNumberLabel() + "#" + action.getGroupActionOrder(), 1);
                            lstActionFlow.add(action);
                        } else if (Objects.equals(action.getGroupActionOrder(), param.getActionOfFlowByActionFlowInId().getGroupActionOrder())
                                && action.getStepNumberLabel() < param.getActionOfFlowByActionFlowInId().getStepNumberLabel()) {
                            lstActionFlow.add(action);
                        }
                    }
                } else {
                    Map<String, Integer> filters = new HashMap<>();
                    for (ActionOfFlow action : selectedFlowTemplate.getActionOfFlows()) {

                        if (action.getGroupActionOrder() >= param.getActionOfFlowByActionFlowInId().getGroupActionOrder()
                                && filters.get(action.getStepNumberLabel() + "#" + action.getGroupActionOrder()) == null) {

                            filters.put(action.getStepNumberLabel() + "#" + action.getGroupActionOrder(), 1);
                            lstActionFlow.add(action);
                        } else if (Objects.equals(action.getGroupActionOrder(), param.getActionOfFlowByActionFlowInId().getGroupActionOrder())
                                && action.getStepNumberLabel() > param.getActionOfFlowByActionFlowInId().getStepNumberLabel()) {
                            lstActionFlow.add(action);
                        }
                    }
                }
            }

            if (lstActionFlow != null && !lstActionFlow.isEmpty()) {
                RequestContext.getCurrentInstance().execute("PF('dlgAddParamRefer').show()");
            } else {
                MessageUtil.setErrorMessageFromRes("datatable.empty");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void buildLstActionCmdSelect() {
        lstActionCommand = new ArrayList<>();
        if (selectedActionFlow != null) {
            try {
                boolean isRollback = selectedParamInOut.getActionOfFlowByActionFlowInId().getIsRollback() != null
                        && selectedParamInOut.getActionOfFlowByActionFlowInId().getIsRollback() > 0;
                boolean isInRollback = selectedActionFlow.getIsRollback() != null
                        && selectedActionFlow.getIsRollback() > 0;
                if (!isRollback) {
                    if (selectedActionFlow.getGroupActionOrder().intValue() <= selectedParamInOut.getActionOfFlowByActionFlowInId().getGroupActionOrder().intValue()) {

                        // lay ra command detail selected cua param
                        List<ActionDetail> lstActionDetail = selectedActionFlow.getAction().getActionDetails();
                        if (lstActionDetail != null && !lstActionDetail.isEmpty()) {
                            for (ActionDetail detail : lstActionDetail) {
                                if (detail.getVendor().equals(selectedParamInOut.getActionCommandByActionCommandInputId().getActionDetail().getVendor())
                                        && detail.getVersion().equals(selectedParamInOut.getActionCommandByActionCommandInputId().getActionDetail().getVersion())) {

                                    if ((selectedActionFlow.getGroupActionOrder().intValue() < selectedParamInOut.getActionOfFlowByActionFlowInId().getGroupActionOrder().intValue())
                                            || (selectedActionFlow.getStepNumberLabel().intValue() < selectedParamInOut.getActionOfFlowByActionFlowInId().getStepNumberLabel().intValue())) {
                                        lstActionCommand.addAll(detail.getActionCommands());

                                    } else if (selectedActionFlow.getStepNumberLabel().longValue() == selectedParamInOut.getActionOfFlowByActionFlowInId().getStepNumberLabel().longValue()) {
                                        for (ActionCommand action : detail.getActionCommands()) {
                                            // 1. khong them cau lenh neu trung voi cau lenh can xet tham chieu
                                            // 2. khong them cac lenh phia sau cau lenh can tham chieu
                                            if (!action.getCommandDetail().getCommandDetailId().equals(selectedParamInOut.getParamInput().getCommandDetail().getCommandDetailId())
                                                    && action.getOrderRun().intValue() < selectedParamInOut.getActionCommandByActionCommandInputId().getOrderRun().intValue()) {
                                                lstActionCommand.add(action);
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    if (selectedActionFlow.getGroupActionOrder().intValue() >= selectedParamInOut.getActionOfFlowByActionFlowInId().getGroupActionOrder().intValue()) {
                        // lay ra command detail selected cua param
                        List<ActionDetail> lstActionDetail = selectedActionFlow.getAction().getActionDetails();
                        if (lstActionDetail != null && !lstActionDetail.isEmpty()) {
                            for (ActionDetail detail : lstActionDetail) {
                                if (detail.getVendor().equals(selectedParamInOut.getActionCommandByActionCommandInputId().getActionDetail().getVendor())
                                        && detail.getVersion().equals(selectedParamInOut.getActionCommandByActionCommandInputId().getActionDetail().getVersion())) {

                                    if ((selectedActionFlow.getGroupActionOrder().intValue() > selectedParamInOut.getActionOfFlowByActionFlowInId().getGroupActionOrder().intValue())
                                            || (isInRollback && (selectedActionFlow.getStepNumberLabel().intValue() > selectedParamInOut.getActionOfFlowByActionFlowInId().getStepNumberLabel().intValue()))
                                            || (!isInRollback && (selectedActionFlow.getStepNumberLabel().intValue() < selectedParamInOut.getActionOfFlowByActionFlowInId().getStepNumberLabel().intValue()))) {
                                        lstActionCommand.addAll(detail.getActionCommands());

                                    } else if (selectedActionFlow.getStepNumberLabel().longValue() == selectedParamInOut.getActionOfFlowByActionFlowInId().getStepNumberLabel().longValue()) {
                                        for (ActionCommand action : detail.getActionCommands()) {
                                            // 1. khong them cau lenh neu trung voi cau lenh can xet tham chieu
                                            // 2. khong them cac lenh phia sau cau lenh can tham chieu
                                            if (!action.getCommandDetail().getCommandDetailId().equals(selectedParamInOut.getParamInput().getCommandDetail().getCommandDetailId())
                                                    && action.getOrderRun().intValue() > selectedParamInOut.getActionCommandByActionCommandInputId().getOrderRun().intValue()) {
                                                lstActionCommand.add(action);
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    public void closeDlgCmdReference() {
        lstActionCommand = new ArrayList<>();
        selectedParamInOut = null;
        selectedActionFlow = null;
    }

    public void saveSelectedCmdReference() {
        if (selectedActionCmd != null && selectedActionFlow != null) {
            try {
                selectedParamInOut.setActionCommandByActionCommandOutputId(selectedActionCmd);
                selectedParamInOut.setActionOfFlowByActionFlowOutId(selectedActionFlow);

                ParamInOutId paramInOutId = new ParamInOutId();
                paramInOutId.setActionCommandInputId(selectedParamInOut.getActionCommandByActionCommandInputId().getActionCommandId());
                paramInOutId.setActionFlowInId(selectedParamInOut.getActionOfFlowByActionFlowInId().getStepNum());
                paramInOutId.setParamInputId(selectedParamInOut.getParamInput().getParamInputId());
                selectedParamInOut.setId(paramInOutId);

                new ParamInOutServiceImpl().saveOrUpdate(selectedParamInOut);
				//lstParamInOutObject.remove(selectedParamInOut.getParamInOutOrder().intValue());
                //lstParamInOutObject.add(selectedParamInOut.getParamInOutOrder(), selectedParamInOut);

                selectedActionFlow = null;
                selectedParamInOut = null;

                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
                RequestContext.getCurrentInstance().execute("PF('dlgAddParamRefer').hide()");
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("label.action.updateFail");
                if (selectedParamInOut != null) {
                    selectedParamInOut.setActionCommandByActionCommandOutputId(null);
                    selectedParamInOut.setActionOfFlowByActionFlowOutId(null);
                }
                logger.error(e.getMessage(), e);
            }

        } else {
            MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
        }
    }

    public void prepareDelCmdRef(ParamInOut param) {
        if (param != null) {
            selectedParamInOut = param;
        }
    }

    public void delCmdReference() {
        if (selectedParamInOut != null) {
            try {
                new ParamInOutServiceImpl().delete(selectedParamInOut);
                selectedParamInOut.setId(null);
                selectedParamInOut.setActionOfFlowByActionFlowOutId(null);
                selectedParamInOut.setActionCommandByActionCommandOutputId(null);
                lstParamInOutObject.remove(selectedParamInOut.getParamInOutOrder());
                lstParamInOutObject.add(selectedParamInOut.getParamInOutOrder(), selectedParamInOut);

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

        }
    }

    public void onUpdateParamDefault() {

        int countParamNameEqual = 0;
        for (int i = 0; i < lstParamGroup.size(); i++) {
            if (lstParamGroup.get(i).getParamInput().getParamCode().trim()
                    .equalsIgnoreCase(selectedParamGroup.getParamInput().getParamCode().trim())) {
                lstParamGroup.get(i).setParamDefault(selectedParamGroup.getParamDefault());
                countParamNameEqual++;
            }
        }

        if (countParamNameEqual > 1) {
            RequestContext.getCurrentInstance().update("form:paramGroupTable");
        }

//		if (selectedParamGroup == null) {
//			System.out.println("<><><><><><><><> not change value");
//		} else {
//			if (selectedParamGroup.getGroupCode() != null
//					&& selectedParamGroup.getGroupCode() != -1) {
//				if (isChangeGroupCode) {
//					
//					// lay ra param group dau tien co group code trung voi selectedParamGroup
//					ParamGroup firstParamGroup = null;
//					for (int i = 0; i < lstParamGroup.size(); i++) {
//						if (lstParamGroup.get(i).equals(selectedParamGroup)) {
//							if (firstParamGroup != null) {
//								lstParamGroup.get(i).setParamDefault(firstParamGroup.getParamDefault());
//							} else {
//								for (int j = 0; j < lstParamGroup.size(); j++) {
//									if (lstParamGroup.get(j).getGroupCode() == selectedParamGroup.getGroupCode())
//										lstParamGroup.get(j).setParamDefault(selectedParamGroup.getParamDefault());
//								}
//								// thoat khoi vong for i
//								break;
//							}
//						} else if ((lstParamGroup.get(i).getGroupCode() == selectedParamGroup.getGroupCode())
//								&& (firstParamGroup == null)) {
//							firstParamGroup = lstParamGroup.get(i);
//						}
//					}
//					
//				} else {
//					for (int i = 0; i < lstParamGroup.size(); i++) {
//						if (lstParamGroup.get(i).getGroupCode() == selectedParamGroup.getGroupCode()) {
//							lstParamGroup.get(i).setParamDefault(selectedParamGroup.getParamDefault());
//						}
//					}
//				}
//			}
//			
//			RequestContext.getCurrentInstance().update("form:paramGroupTable");
//		}
    }

    public String getActionGroupNameData(int index) {
        if (mapGroupName != null) {
            for (Map.Entry<String, Integer> entry : mapGroupName.entrySet()) {
                if (entry.getValue() == index) {
                    return entry.getKey();
                }
            }
        }
        return "";
    }
    
    public void onCellEditParam(CellEditEvent event) {

        DataTable data = (DataTable) event.getSource();
        ParamGroup paramGroup = (ParamGroup) data.getRowData();
        selectedParamGroup = paramGroup;

//		String columnName = event.getColumn().getHeaderText();
//		if (columnName.trim().equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.paramDefault"))) {
//			
//			String oldValue = (String) event.getOldValue();
//			String newValue = (String) event.getNewValue();
//			
//			if (newValue != null) {
//				if (oldValue != null && oldValue.equals(newValue)) {
//					selectedParamGroup = null;
//					
//				} else {
//					DataTable data = (DataTable) event.getSource();
//					ParamGroup paramGroup = (ParamGroup) data.getRowData();
//					selectedParamGroup = paramGroup;
//				}
//			}
//			isChangeGroupCode = false;
//
//		} else if (columnName.trim().equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.group.code"))) {
//			Long oldValue = (Long) event.getOldValue();
//			Long newValue = (Long) event.getNewValue();
//			
//			if (newValue != null) {
//				if (oldValue != null && oldValue.equals(newValue)) {
//					selectedParamGroup = null;
//					
//				} else {
//					DataTable data = (DataTable) event.getSource();
//					ParamGroup paramGroup = (ParamGroup) data.getRowData();
//					selectedParamGroup = paramGroup;
//				}
//			}
//			
//			isChangeGroupCode = true;
//		} else {
//			isChangeGroupCode = false;
//			System.out.println(">>>>>>>> " + columnName);
//		}
    }

    public void saveParamGroupDefault() {
        try {
            /*
             * Xoa toan bo cac param default truoc khi thuc hien cap nhat
             */
            List<ParamGroup> lstParamDel = selectedFlowTemplate.getParamGroups();
            if (lstParamDel != null && !lstParamDel.isEmpty()) {
                new ParamGroupServiceImpl().delete(lstParamDel);
            }

            List<ParamGroup> lstParamGroupSave = new ArrayList<>();
            for (ParamGroup param : lstParamGroup) {
                if ((param.getParamDefault() != null && !"".equals(param.getParamDefault().trim()))) {
                    lstParamGroupSave.add(param);
                }
            }

            if (!lstParamGroupSave.isEmpty()) {
                new ParamGroupServiceImpl().saveOrUpdate(lstParamGroupSave);
            }
            MessageUtil.setInfoMessageFromRes("label.action.updateOk");
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }
    }

    public void prepareShowCmd(ActionOfFlow action) {
        if (action != null) {
            selectedActionView = action.getAction();
        }
    }

    public void setPreStepsData(ActionOfFlow actionFlow, int indexGroup, int indexActionFlow) {
        try {
            preStepsActionSelected = new ArrayList<>();
            for (int i = 0; i < actionFlow.getPreStepsNumber().size(); i++) {
                preStepsActionSelected.add(actionFlow.getPreStepsNumber().get(i));
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void updatePreStepAction() {
        try {
            if (preStepsActionSelected != null) {
                Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
                int groupIndex = Integer.valueOf(params.get("groupActionIndex"));
                int actionFlowIndex = Integer.valueOf(params.get("actionFlowIndex"));
                String actionFlowTableId = params.get("actionFlowRowId");

                String preStepActionTmp = "";
                for (String preStep : preStepsActionSelected) {
                    preStepActionTmp += preStep + ",";
                }

                preStepActionTmp = preStepActionTmp.endsWith(",") ? preStepActionTmp.substring(0, preStepActionTmp.length() - 1) : preStepActionTmp;
                actionOfFlowss.get(groupIndex).get(actionFlowIndex).setPreStepsNumberLabel(preStepActionTmp);
//				actionOfFlowss.get(groupIndex).get(actionFlowIndex).setPreStepsNumber(preStepsActionSelected);
                RequestContext.getCurrentInstance().update("form:" + actionFlowTableId);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public List<TemplateGroup> autoCompleTemplateGroup(String actionName) {
        List<TemplateGroup> lstAction = new ArrayList<>();
        try {
            //lstAction = vendorServiceImpl.findList();
            Map<String, Object> filters = new HashMap<>();
            if (actionName != null) {
                filters.put("groupName", actionName);
            }
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("groupName", "ASC");
            lstAction = new TemplateGroupServiceImpl().findList(filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstAction;
    }
    public List<FlowTemplates> autoCompleTemplate(String flowTemplateName) {
        List<FlowTemplates> lstTemplate = new ArrayList<>();
        try {
            Map<String, Object> filters = new HashMap<>();
            if (flowTemplateName != null && !"".equals(flowTemplateName)) {
                filters.put("flowTemplateName", flowTemplateName);
            }
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("flowTemplateName", "ASC");
            lstTemplate = new FlowTemplatesServiceImpl().findList(filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstTemplate;
    }

    public FlowTemplates getSelectedFlowTemplate() {
        return selectedFlowTemplate;
    }

    public void setSelectedFlowTemplate(FlowTemplates selectedFlowTemplate) {
        this.selectedFlowTemplate = selectedFlowTemplate;
    }

    public List<FlowTemplates> getLstFlowTemplate() {
        return lstFlowTemplate;
    }

    public void setLstFlowTemplate(List<FlowTemplates> lstFlowTemplate) {
        this.lstFlowTemplate = lstFlowTemplate;
    }

    public String getFlowTemplateName() {
        return flowTemplateName;
    }

    public void setFlowTemplateName(String flowTemplateName) {
        this.flowTemplateName = flowTemplateName;
    }

    public boolean isEdit() {
        return isEdit;
    }

    public void setEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public TreeNode getSelectedTreeNodeAction() {
        return selectedTreeNodeAction;
    }

    public void setSelectedTreeNodeAction(TreeNode selectedTreeNodeAction) {
        this.selectedTreeNodeAction = selectedTreeNodeAction;
    }

    public List<List<ActionOfFlow>> getActionOfFlowss() {
        return actionOfFlowss;
    }

    public void setActionOfFlowss(List<List<ActionOfFlow>> mapActionOfFlow) {
        this.actionOfFlowss = mapActionOfFlow;
    }

    public List<ActionOfFlow> getLstActionFlowDel() {
        return lstActionFlowDel;
    }

    public void setLstActionFlowDel(List<ActionOfFlow> lstActionFlowDel) {
        this.lstActionFlowDel = lstActionFlowDel;
    }

    public List<SelectItem> getLstItemGroupName() {
        return lstItemGroupName;
    }

    public void setLstItemGroupName(List<SelectItem> lstItemGroupName) {
        this.lstItemGroupName = lstItemGroupName;
    }

    public boolean isAddNewGroupName() {
        return isAddNewGroupName;
    }

    public void setAddNewGroupName(boolean isAddNewGroupName) {
        this.isAddNewGroupName = isAddNewGroupName;
    }

    public String getNewGroupActionName() {
        return newGroupActionName;
    }

    public void setNewGroupActionName(String newGroupActionName) {
        this.newGroupActionName = newGroupActionName;
    }

    public String getSelectedGroupActionName() {
        return selectedGroupActionName;
    }

    public void setSelectedGroupActionName(String selectedGroupActionName) {
        this.selectedGroupActionName = selectedGroupActionName;
    }

    public String getCopyFlowTemplateName() {
        return copyFlowTemplateName;
    }

    public void setCopyFlowTemplateName(String copyFlowTemplateName) {
        this.copyFlowTemplateName = copyFlowTemplateName;
    }

    public List<ParamGroup> getLstParamGroup() {
        return lstParamGroup;
    }

    public void setLstParamGroup(List<ParamGroup> lstParamGroup) {
        this.lstParamGroup = lstParamGroup;
    }

    public ParamGroup getSelectedParamGroup() {
        return selectedParamGroup;
    }

    public void setSelectedParamGroup(ParamGroup selectedParamGroup) {
        this.selectedParamGroup = selectedParamGroup;
    }

    public boolean isChangeGroupCode() {
        return isChangeGroupCode;
    }

    public void setChangeGroupCode(boolean isChangeGroupCode) {
        this.isChangeGroupCode = isChangeGroupCode;
    }

    public String getOldGroupActionName() {
        return oldGroupActionName;
    }

    public void setOldGroupActionName(String oldGroupActionName) {
        this.oldGroupActionName = oldGroupActionName;
    }

    public boolean isChangeGroupActionName() {
        return isChangeGroupActionName;
    }

    public void setChangeGroupActionName(boolean isChangeGroupActionName) {
        this.isChangeGroupActionName = isChangeGroupActionName;
    }

    public Map<String, Integer> getMapGroupName() {
        return mapGroupName;
    }

    public void setMapGroupName(Map<String, Integer> mapGroupName) {
        this.mapGroupName = mapGroupName;
    }

    public List<ParamInOut> getLstParamInOutObject() {
        return lstParamInOutObject;
    }

    public void setLstParamInOutObject(List<ParamInOut> lstParamInOutObject) {
        this.lstParamInOutObject = lstParamInOutObject;
    }

    public ActionOfFlow getSelectedActionFlow() {
        return selectedActionFlow;
    }

    public void setSelectedActionFlow(ActionOfFlow selectedActionFlow) {
        this.selectedActionFlow = selectedActionFlow;
    }

    public ActionCommand getSelectedActionCmd() {
        return selectedActionCmd;
    }

    public void setSelectedActionCmd(ActionCommand selectedActionCmd) {
        this.selectedActionCmd = selectedActionCmd;
    }

    public ActionDetail getSelectedActionDetail() {
        return selectedActionDetail;
    }

    public void setSelectedActionDetail(ActionDetail selectedActionDetail) {
        this.selectedActionDetail = selectedActionDetail;
    }

    public List<ActionOfFlow> getLstActionFlow() {
        return lstActionFlow;
    }

    public void setLstActionFlow(List<ActionOfFlow> lstActionFlow) {
        this.lstActionFlow = lstActionFlow;
    }

    public List<ActionCommand> getLstActionCommand() {
        return lstActionCommand;
    }

    public void setLstActionCommand(List<ActionCommand> lstActionCommand) {
        this.lstActionCommand = lstActionCommand;
    }

    public ParamInOut getSelectedParamInOut() {
        return selectedParamInOut;
    }

    public void setSelectedParamInOut(ParamInOut selectedParamInOut) {
        this.selectedParamInOut = selectedParamInOut;
    }

    public Action getSelectedActionView() {
        return selectedActionView;
    }

    public void setSelectedActionView(Action selectedActionView) {
        this.selectedActionView = selectedActionView;
    }

    public Boolean[] getDefaultAddToTemplate() {
        return defaultAddToTemplate;
    }

    public void setDefaultAddToTemplate(Boolean[] defaultAddToTemplate) {
        this.defaultAddToTemplate = defaultAddToTemplate;
    }

    public Integer getIndexOfGroupActionToDel() {
        return indexOfGroupActionToDel;
    }

    public void setIndexOfGroupActionToDel(Integer indexOfGroupActionToDel) {
        this.indexOfGroupActionToDel = indexOfGroupActionToDel;
    }

    public boolean isApproveTemplate() {
        return isApproveTemplate;
    }

    public void setApproveTemplate(boolean isApproveTemplate) {
        this.isApproveTemplate = isApproveTemplate;
    }

    public List<String> getPreStepsActionSelected() {
        return preStepsActionSelected;
    }

    public void setPreStepsActionSelected(List<String> preStepsActionSelected) {
        this.preStepsActionSelected = preStepsActionSelected;
    }

    public boolean isPreApproveTemplate() {
        return isPreApproveTemplate;
    }

    public void setPreApproveTemplate(boolean isPreApproveTemplate) {
        this.isPreApproveTemplate = isPreApproveTemplate;
    }

    public Long getCloneType() {
        return cloneType;
    }

    public void setCloneType(Long cloneType) {
        this.cloneType = cloneType;
    }

    public Map<String, Long> getMapCloneType() {
        return mapCloneType;
    }

    public void setMapCloneType(Map<String, Long> mapCloneType) {
        this.mapCloneType = mapCloneType;
    }

    public Long getGroupTemplateId() {
        return groupTemplateId;
    }

    public void setGroupTemplateId(Long groupTemplateId) {
        this.groupTemplateId = groupTemplateId;
    }

    public TemplateGroup getSelectTemplateGroup() {
        return selectTemplateGroup;
    }

    public void setSelectTemplateGroup(TemplateGroup selectTemplateGroup) {
        this.selectTemplateGroup = selectTemplateGroup;
    }

    public Long getSelectedResetType() {
        return selectedResetType;
    }

    public void setSelectedResetType(Long selectedResetType) {
        this.selectedResetType = selectedResetType;
    }
}
