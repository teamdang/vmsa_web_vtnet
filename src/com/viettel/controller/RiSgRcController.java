package com.viettel.controller;

import static com.viettel.controller.RescueInfomationController.logger;
import com.viettel.model.FlowRunAction;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.model.ParamValue;
import com.viettel.model.RiSgRcParamRncEricsson;
import com.viettel.model.RiSgRcParamRncHuawei;
import com.viettel.model.RiSgRcParamRncNokia;
import com.viettel.model.RiSgRcParam;
import com.viettel.model.RiSgRcParamLacRac;
import com.viettel.model.RiSgRcParamLinkSgsn;
import com.viettel.model.RiSgRcParamSgsnRnc;
import com.viettel.model.RiSgRcParamUnique;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.viettel.model.RiSgRcPlan;
import com.viettel.object.MessageException;
import com.viettel.object.RiSgRcModelExcel;
import com.viettel.object.RiSgRcResult;
import com.viettel.object.RiSgRcResultImport;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.RiSgRcParamRncEricssonServiceImpl;
import com.viettel.persistence.RiSgRcParamRncHuaweiServiceImpl;
import com.viettel.persistence.RiSgRcParamRncNokiaServiceImpl;
import com.viettel.persistence.RiSgRcParamLinkSgsnServiceImpl;
import com.viettel.persistence.RiSgRcParamLacRacServiceImpl;
import com.viettel.persistence.RiSgRcParamServiceImpl;
import com.viettel.persistence.RiSgRcParamSgsnRncServiceImpl;
import com.viettel.persistence.RiSgRcParamUniqueServiceImpl;
import com.viettel.persistence.RiSgRcPlanServiceImpl;
import com.viettel.util.CommonExport;
import com.viettel.util.Config;
import com.viettel.util.Constants;
import com.viettel.util.Importer;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionUtil;
import com.viettel.util.SessionWrapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.event.FileUploadEvent;

public class RiSgRcController {

    //<editor-fold defaultstate="collapsed" desc="CreatMop">
    protected boolean createFlowRunAction(FlowTemplates flowTemplates, RiSgRcPlan plan, HashMap<String, HashMap<String, RiSgRcParam>> mapParamList, RiSgRcResult riResult, Long mopType) throws MessageException {

        GenerateFlowRunController generateFlowRunController;
        try {

            generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();
            flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
            flowRunAction.setFlowRunName(("Rescue_Info - SGSN_ACT_" + plan.getNodeSgsnCurrent().getNodeCode() + " - RNC_" + plan.getNodeRnc().getNodeCode() + " - SGSN_BAK_" + plan.getNodeSgsnBackup().getNodeCode() + " - " + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())));
            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplates);
            flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());
            if (mopType != null) {
                flowRunAction.setMopType(mopType);
            } else {
                flowRunAction.setMopType(1l);
            }

            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
            //Lay danh sach param tu bang 
            generateFlowRunController.setNodes(new ArrayList<Node>());
            Map<String, RiSgRcParam> mapTableParam;
            Map<String, String> mapParamValues = new HashMap<>();
            generateFlowRunController.loadGroupAction(1l);
            HashMap<String, Node> mapNodeInPlan = new HashMap<>();
            List<Node> nodeInPlan;

            nodeInPlan = new ArrayList<>();
            if (!mapNodeInPlan.containsKey(plan.getNodeRnc().getNodeCode())) {
                generateFlowRunController.getNodes().add(plan.getNodeRnc());
                mapNodeInPlan.put(plan.getNodeRnc().getNodeCode(), plan.getNodeRnc());
            }
            if (!mapNodeInPlan.containsKey(plan.getNodeSgsnCurrent().getNodeCode())) {
                plan.getNodeSgsnCurrent().setFlag(Config.SGSN_ACTIVE);
                generateFlowRunController.getNodes().add(plan.getNodeSgsnCurrent());
                mapNodeInPlan.put(plan.getNodeSgsnCurrent().getNodeCode(), plan.getNodeSgsnCurrent());
            }
            if (!mapNodeInPlan.containsKey(plan.getNodeSgsnBackup().getNodeCode())) {
                plan.getNodeSgsnBackup().setFlag(Config.SGSN_BACKUP);
                generateFlowRunController.getNodes().add(plan.getNodeSgsnBackup());
                mapNodeInPlan.put(plan.getNodeSgsnBackup().getNodeCode(), plan.getNodeSgsnBackup());
            }
            nodeInPlan.add(plan.getNodeRnc());
            nodeInPlan.add(plan.getNodeSgsnCurrent());
            nodeInPlan.add(plan.getNodeSgsnBackup());

            String rncNameForSgsnNokia = "";
            if (plan.getNodeRnc().getNodeCode().trim().length() > 1) {
                rncNameForSgsnNokia = new StringBuilder(plan.getNodeRnc().getNodeCode().trim()).replace(1, 2, "").toString();
            }

            if (plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON) || plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.HUAWEI)) {

                if (mapParamList.containsKey(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeRnc().getNodeCode().trim())) {
                    mapTableParam = mapParamList.get(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeRnc().getNodeCode().trim());
                } else {
                    mapTableParam = new HashMap<>();
                }
                if (mapParamList.containsKey(rncNameForSgsnNokia.trim() + "#" + plan.getNodeRnc().getNodeCode().trim())) {
                    mapTableParam.putAll(mapParamList.get(rncNameForSgsnNokia.trim() + "#" + plan.getNodeRnc().getNodeCode().trim()));
                }
                if (mapTableParam != null) {
                    for (String map : mapTableParam.keySet()) {
                        mapParamValues.put("bak" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                    }
                }
                if (mapParamList.containsKey(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim())) {
                    mapTableParam = mapParamList.get(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim());
                } else {
                    mapTableParam = new HashMap<>();
                }

                if (mapParamList.containsKey(rncNameForSgsnNokia.trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim())) {
                    mapTableParam.putAll(mapParamList.get(rncNameForSgsnNokia.trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim()));
                }

                if (mapTableParam != null) {
                    for (String map : mapTableParam.keySet()) {
                        mapParamValues.put("act" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                    }
                }
            } else {
                if (mapParamList.containsKey(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim())) {
                    mapTableParam = mapParamList.get(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim());
                } else {
                    mapTableParam = new HashMap<>();
                }
                if (mapParamList.containsKey(rncNameForSgsnNokia.trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim())) {
                    mapTableParam.putAll(mapParamList.get(rncNameForSgsnNokia.trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim()));
                }
                if (mapParamList.containsKey(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeRnc().getNodeCode().trim())) {
                    mapTableParam.putAll(mapParamList.get(plan.getNodeRnc().getNodeCode().trim() + "#" + plan.getNodeRnc().getNodeCode().trim()));
                }
                if (mapParamList.containsKey(rncNameForSgsnNokia.trim() + "#" + plan.getNodeRnc().getNodeCode().trim())) {
                    mapTableParam.putAll(mapParamList.get(rncNameForSgsnNokia.trim() + "#" + plan.getNodeRnc().getNodeCode().trim()));
                }
                for (String map : mapTableParam.keySet()) {
                    mapParamValues.put("act" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                    mapParamValues.put("bak" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                }
            }

            mapParamValues.put("act" + Constants.paramCode.RNCNameNokia.toLowerCase(), rncNameForSgsnNokia);
            mapParamValues.put("act" + Constants.paramCode.RNCName.toLowerCase(), plan.getNodeRnc().getNodeCode());
            mapParamValues.put("bak" + Constants.paramCode.RNCNameNokia.toLowerCase(), rncNameForSgsnNokia);
            mapParamValues.put("bak" + Constants.paramCode.RNCName.toLowerCase(), plan.getNodeRnc().getNodeCode());

            mapTableParam = mapParamList.get(plan.getNodeSgsnCurrent().getNodeCode().trim() + "#" + plan.getNodeSgsnCurrent().getNodeCode().trim());
            if (mapTableParam != null) {
                for (String map : mapTableParam.keySet()) {
                    mapParamValues.put("act" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());

                }
            }
            mapParamValues.put("act" + Constants.paramCode.SGSNName.toLowerCase(), plan.getNodeSgsnCurrent().getNodeCode());

            mapTableParam = mapParamList.get(plan.getNodeSgsnBackup().getNodeCode().trim() + "#" + plan.getNodeSgsnBackup().getNodeCode().trim());
            if (mapTableParam != null) {
                for (String map : mapTableParam.keySet()) {
                    mapParamValues.put("bak" + mapTableParam.get(map).getParamCode().toLowerCase().trim(), mapTableParam.get(map).getParamValue() == null ? "" : mapTableParam.get(map).getParamValue().trim());
                }

            }
            mapParamValues.put("bak" + Constants.paramCode.SGSNName.toLowerCase(), plan.getNodeSgsnBackup().getNodeCode());

            List<RiSgRcParamUnique> uniqueParams = new ArrayList<>();
            setParamFromTableOther(plan, mapParamValues, riResult, uniqueParams, mopType);

            for (Node node : nodeInPlan) {
                generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                for (ParamValue paramValue : paramValues) {
                    if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                        continue;
                    }
                    Object value = null;
                    try {
                        value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim()));
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                    ResourceBundle bundle = ResourceBundle.getBundle("cas");
                    if (bundle.getString("service").contains("10.61.127.190")) {
                        if (value == null || value.toString().isEmpty()) {
                            value = "TEST_NOT_FOUND";
                        }
                    }
                    if (value != null) {
                        paramValue.setParamValue(value.toString());
                    }
//                    if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                .replace("{1}", paramValue.getParamCode()));
//                        return false;
//                    }
                }
            }
            mapParamValues.clear();

            boolean saveDT = generateFlowRunController.saveDT();
            if (saveDT) {
                try {
                    MessageUtil.setInfoMessageFromRes("Sinh Mop ứng cứu thành công node RNC:" + plan.getNodeRnc().getNodeCode());

                    if (!uniqueParams.isEmpty()) {
                        for (RiSgRcParamUnique prU : uniqueParams) {
                            prU.setFlowRunActionId(generateFlowRunController.getFlowRunAction().getFlowRunId());
                        }
                        new RiSgRcParamUniqueServiceImpl().saveOrUpdate(uniqueParams);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

            }
            return saveDT;
        } catch (Exception e) {
            if (e instanceof MessageException) {
                MessageUtil.setErrorMessage(e.getMessage());
            } else {
                MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                logger.error(e.getMessage(), e);
            }
            return false;
//            throw e;

        } finally {
            //Tam biet
        }

    }

    public void setParamFromTableOther(RiSgRcPlan plan, Map<String, String> mapParamValues,
            RiSgRcResult riResult, List<RiSgRcParamUnique> uniqueParams, Long mopType) {
        List<RiSgRcParamLinkSgsn> listPrLinkSgsn;
        List<RiSgRcParamRncNokia> listPrLinkRncNokia;
        List<RiSgRcParamRncEricsson> listPrLinkRncEricsson;
        List<RiSgRcParamRncHuawei> listPrLinkRncHuawei;

        List<RiSgRcParamLacRac> listPrLacRac;
        HashMap<String, RiSgRcParamLacRac> mapPrLacRac;
        List<RiSgRcParamSgsnRnc> listPrSgsnRnc;
        try {
            Map<String, Object> filters = new HashMap<>();
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
            int countIp;
            String RNCIuIPTemp1 = "";
            String RNCIuIPTemp2 = "";
            String temp;
            String SgsnIuIPTemp1 = "";
            int countBreak = 0;
            String[] parameterSetNames;
            if (mapParamValues.containsKey("bakRNCParameterSetName".toLowerCase())) {
                parameterSetNames = mapParamValues.get("bakRNCParameterSetName".toLowerCase()).split(";");
            } else {
                parameterSetNames = new String[0];
            }

            // Bien hien tai chi su dung cho SGSN Nokia
            riResult.setIndex("0;1;2;3");
            putParamValue("bak" + Constants.paramCode.number.toLowerCase(), riResult.getIndex(), mapParamValues);
            putParamValue("act" + Constants.paramCode.number.toLowerCase(),
                    mapParamValues.get("act" + Constants.paramCodeRnc.RNCAssocIndex.toLowerCase()), mapParamValues);
            riResult.setIndex("_1;_2;_3;_4");
            putParamValue("bak" + Constants.paramCode.index.toLowerCase(), riResult.getIndex(), mapParamValues);

            //Bang Rnc
            switch (plan.getNodeRnc().getVendor().getVendorName().toUpperCase()) {

                case Constants.vendorType.NOKIA:
                    for (String setName : parameterSetNames) {
                        if (setName.toUpperCase().contains("NSN")) {
                            putParamValue("bak" + Constants.paramCode.RNCParameterSetName.toLowerCase(),
                                    setName, mapParamValues);
                            break;
                        }
                    }
                    listPrLinkRncNokia = new RiSgRcParamRncNokiaServiceImpl().findList(filters);
                    countIp = listPrLinkRncNokia.size();
                    switch (plan.getNodeRnc().getVersion().getVersionName().toUpperCase()) {
                        case Constants.versionType.IPA2600:

                            for (RiSgRcParamRncNokia bo : listPrLinkRncNokia) {
                                riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getRncPort());
                                RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIp1();
                                RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIp2();
                                riResult.setAssocciationSetName(";" + bo.getAssocciationSetName());
                                riResult.setListSignallingLink(bo.getListSignallingLink());
                                riResult.setIcsu(riResult.getIcsu() + ";" + bo.getIcsu());
                                riResult.setCPNPGEP1(bo.getCpnpgepIp1());
                                riResult.setCPNPGEP2(bo.getCpnpgepIp2());
                                countBreak++;
                                if (countBreak >= 4) {
                                    break;
                                }
                            }
                            for (RiSgRcParamRncNokia bo : listPrLinkRncNokia) {
                                riResult.setActAssocID(riResult.getActAssocID() + ";" + bo.getAssocId());
                                riResult.setActAssocciationSetName(bo.getAssocciationSetName());
                                riResult.setActSignallingLink(bo.getSignallingLink());
                                riResult.setActSignallingLinkName(bo.getSignallingLinkName());
                                countBreak++;
                            }
                            if (countIp == 1) {
                                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                                riResult.setIcsu(riResult.getIcsu() + riResult.getIcsu() + riResult.getIcsu() + riResult.getIcsu());
                            } else if (countIp == 2) {
                                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                                riResult.setIcsu(riResult.getIcsu() + riResult.getIcsu());
                            } else {
                                riResult.setRNCIuIP1(RNCIuIPTemp1);
                                riResult.setRNCIuIP2(RNCIuIPTemp2);
                            }
                            if (mopType.equals(0L)) {
                                //Neu tac dong thuong i++ tu 1
                                for (int i = 0; i < 10; i++) {
                                    if (i == 0) {
                                        temp = "IUPS";
                                    } else {
                                        temp = "IUPS" + String.valueOf(i);
                                    }
                                    if (!riResult.getAssocciationSetName().contains(";" + temp)) {
                                        riResult.setAssocciationSetName(temp);
                                        break;
                                    }
                                }

                                for (int i = 0; i < 1023; i++) {
                                    temp = String.valueOf(i);
                                    if (!riResult.getListSignallingLink().contains("##" + temp)) {
                                        riResult.setSignallingLink(temp);
                                        break;
                                    }
                                }
                            } else {
                                //Neu tac dong UCTT i-- tu 10
                                for (int i = 10; i > 0; i--) {
                                    temp = "IUPS" + String.valueOf(i);
                                    if (!riResult.getAssocciationSetName().contains(";" + temp)) {
                                        riResult.setAssocciationSetName(temp);
                                        break;
                                    }
                                }

                                for (int i = 1023; i > 0; i--) {
                                    temp = String.valueOf(i);
                                    if (!riResult.getListSignallingLink().contains("##" + temp)) {
                                        riResult.setSignallingLink(temp);
                                        break;
                                    }
                                }
                            }
                            countBreak = 0;
                            temp = riResult.getRNCPort();
                            riResult.setRNCPort("");
                            for (int i = 8000; i < 9000; i++) {
                                if (!temp.contains(";" + String.valueOf(i))) {
                                    riResult.setRNCPort(riResult.getRNCPort() + ";" + String.valueOf(i));
                                    countBreak++;
                                }
                                if (countBreak >= 4) {
                                    break;
                                }
                            }
//                            if (!riResult.getRNCPort().contains("8000")) {
//                                riResult.setRNCPort("8000;8001;8002;8003");
//                            } else {
//                                riResult.setRNCPort("8010;8011;8012;8013");
//                            }
                            riResult.setAssocID("0;1;2;3");
                            if (plan.getNodeSgsnBackup().getNodeCode().length() > 5) {
                                riResult.setSignallingLinkName(plan.getNodeSgsnBackup().getNodeCode().substring(0, 4) + plan.getNodeSgsnBackup().getNodeCode().charAt(5));
                            }
                            putParamValue("bak" + Constants.paramCode.SignallingLink.toLowerCase(), riResult.getSignallingLink(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.SignallingLinkName.toLowerCase(), riResult.getSignallingLinkName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.AssocciationSetName.toLowerCase(), riResult.getAssocciationSetName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.icsu.toLowerCase(), riResult.getIcsu(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.AssocID.toLowerCase(), riResult.getAssocID(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.SignallingLink.toLowerCase(), riResult.getActSignallingLink(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.AssocID.toLowerCase(), riResult.getActAssocID(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.SignallingLinkName.toLowerCase(), riResult.getActSignallingLinkName(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.AssocciationSetName.toLowerCase(), riResult.getActAssocciationSetName(), mapParamValues);
                            break;
                        case Constants.versionType.mcRNC:
                            for (RiSgRcParamRncNokia bo : listPrLinkRncNokia) {
                                countBreak++;
                                RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIp1();
                                RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIp2();
                                riResult.setGateway1(riResult.getGateway1() + ";" + convertIp(bo.getRncIp1(), 1, 1));
                                if (countBreak == 2 || countBreak == 4) {
                                    riResult.setGateway2(riResult.getGateway2() + ";" + convertIp(bo.getRncIp2(), 1, -1));
                                } else {
                                    riResult.setGateway2(riResult.getGateway2() + ";" + convertIp(bo.getRncIp2(), 1, 1));
                                }
                                riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getLocalClientPort());
                                riResult.setLocalServerPort(riResult.getLocalServerPort() + ";" + bo.getRncPort());
                                riResult.setListRemoteAsId(bo.getListRemoteAsId());
                                riResult.setListAssocId(riResult.getListAssocId() + ";" + bo.getListAssocId());
                                riResult.setListPointCodeId(riResult.getListPointCodeId() + ";" + bo.getListPointCodeId());
                                riResult.setListSubSystemId(riResult.getListSubSystemId() + ";" + bo.getListSubSystemId());
                                riResult.setLocalAsName(riResult.getLocalAsName() + ";" + bo.getLocalAsName());
                                riResult.setLocalClientPort(riResult.getLocalClientPort() + ";" + bo.getLocalClientPort());
                                if (bo.getNodeAssoc().startsWith("/")) {
                                    bo.setNodeAssoc(bo.getNodeAssoc().substring(1));
                                }
                                riResult.setNodeAssoc(riResult.getNodeAssoc() + ";" + bo.getNodeAssoc());
                                riResult.setIpbrId(bo.getIpbrId());
                                if (countBreak >= 4) {
                                    break;
                                }
                            }
                            for (RiSgRcParamRncNokia bo : listPrLinkRncNokia) {
                                riResult.setActAssocID(riResult.getActAssocID() + ";" + bo.getAssocId());
                                riResult.setActSubSystemName(bo.getSubSystemName());
                                riResult.setActConcernSsnName(bo.getConcernSsnName());
                                riResult.setActRemoteAsName(bo.getRemoteAsName());
                                riResult.setActConcernPointCodeName(bo.getConcernPointCodeName());
                            }
                            if (countIp == 1) {
                                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                                riResult.setLocalAsName(riResult.getLocalAsName() + riResult.getLocalAsName() + riResult.getLocalAsName() + riResult.getLocalAsName());
                                riResult.setLocalClientPort(riResult.getLocalClientPort() + riResult.getLocalClientPort() + riResult.getLocalClientPort() + riResult.getLocalClientPort());
                                riResult.setLocalServerPort(riResult.getLocalServerPort() + riResult.getLocalServerPort() + riResult.getLocalServerPort() + riResult.getLocalServerPort());
                                riResult.setRNCPort(riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort());
                                riResult.setNodeAssoc(riResult.getNodeAssoc() + riResult.getNodeAssoc() + riResult.getNodeAssoc() + riResult.getNodeAssoc());
                                riResult.setGateway1(riResult.getGateway1() + riResult.getGateway1() + riResult.getGateway1() + riResult.getGateway1());
                                riResult.setGateway2(riResult.getGateway2() + riResult.getGateway2() + riResult.getGateway2() + riResult.getGateway2());
                            } else if (countIp == 2) {
                                riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                                riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                                riResult.setLocalAsName(riResult.getLocalAsName() + riResult.getLocalAsName());
                                riResult.setLocalClientPort(riResult.getLocalClientPort() + riResult.getLocalClientPort());
                                riResult.setLocalServerPort(riResult.getLocalServerPort() + riResult.getLocalServerPort());
                                riResult.setRNCPort(riResult.getRNCPort() + riResult.getRNCPort());
                                riResult.setNodeAssoc(riResult.getNodeAssoc() + riResult.getNodeAssoc());
                                riResult.setGateway1(riResult.getGateway1() + riResult.getGateway1());
                                riResult.setGateway2(riResult.getGateway2() + riResult.getGateway2());
                            } else {
                                riResult.setRNCIuIP1(RNCIuIPTemp1);
                                riResult.setRNCIuIP2(RNCIuIPTemp2);
                            }
                            if (mopType.equals(0L)) {
                                for (int i = 1; i < 150; i++) {
                                    if (!riResult.getListRemoteAsId().equals("##" + i)) {
                                        riResult.setRemoteAsId(String.valueOf(i));
                                        break;
                                    }
                                }
                                countBreak = 0;
                                for (int i = 1; i < 65535; i++) {
                                    if (!riResult.getListAssocId().equals("##" + i)) {
                                        countBreak++;
                                        riResult.setAssocID(riResult.getAssocID() + ";" + String.valueOf(i));
                                    }
                                    if (countBreak >= 4) {
                                        break;
                                    }
                                }
                                for (int i = 1; i < 150; i++) {
                                    if (!riResult.getListPointCodeId().equals("##" + i)) {
                                        riResult.setPointCodeId(String.valueOf(i));
                                        break;
                                    }
                                }
                                for (int i = 1; i < 150; i++) {
                                    if (!riResult.getListSubSystemId().equals("##" + i)) {
                                        riResult.setSubSystemId(String.valueOf(i));
                                        break;
                                    }
                                }
                            } else {
                                for (int i = 150; i > 0; i--) {
                                    if (!riResult.getListRemoteAsId().equals("##" + i)) {
                                        riResult.setRemoteAsId(String.valueOf(i));
                                        break;
                                    }
                                }
                                countBreak = 0;
                                for (int i = 65535; i > 0; i--) {
                                    if (!riResult.getListAssocId().equals("##" + i)) {
                                        countBreak++;
                                        riResult.setAssocID(riResult.getAssocID() + ";" + String.valueOf(i));
                                    }
                                    if (countBreak >= 4) {
                                        break;
                                    }
                                }
                                for (int i = 150; i > 0; i--) {
                                    if (!riResult.getListPointCodeId().equals("##" + i)) {
                                        riResult.setPointCodeId(String.valueOf(i));
                                        break;
                                    }
                                }
                                for (int i = 150; i > 0; i--) {
                                    if (!riResult.getListSubSystemId().equals("##" + i)) {
                                        riResult.setSubSystemId(String.valueOf(i));
                                        break;
                                    }
                                }
                            }
                            riResult.setRemoteAsName("RA_" + plan.getNodeSgsnBackup().getNodeCode());
                            riResult.setSubSystemName("SS_" + plan.getNodeSgsnBackup().getNodeCode());
                            riResult.setPointCodeName("RA_" + plan.getNodeSgsnBackup().getNodeCode());
                            riResult.setConcernSsnName("CS_" + plan.getNodeSgsnBackup().getNodeCode());
                            riResult.setAffectSsnName("SS_" + plan.getNodeSgsnBackup().getNodeCode());
                            riResult.setConcernPointCodeName("CP_" + plan.getNodeSgsnBackup().getNodeCode());
                            riResult.setIpbrName("PSUP-" + plan.getNodeSgsnBackup().getNodeCode());

                            putParamValue("bak" + Constants.paramCode.AssocID.toLowerCase(), riResult.getAssocID(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.RemoteAsID.toLowerCase(), riResult.getRemoteAsId(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.RemoteAsName.toLowerCase(), riResult.getRemoteAsName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.local_as_name.toLowerCase(), riResult.getLocalAsName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.local_client_port.toLowerCase(), riResult.getLocalClientPort(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.local_server_port.toLowerCase(), riResult.getLocalServerPort(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.NodeAssoc.toLowerCase(), riResult.getNodeAssoc(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.PointCodeId.toLowerCase(), riResult.getPointCodeId(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.PointCodeName.toLowerCase(), riResult.getPointCodeName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.SubSystemID.toLowerCase(), riResult.getSubSystemId(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.SubSystemName.toLowerCase(), riResult.getSubSystemName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.ConcernSSNName.toLowerCase(), riResult.getConcernSsnName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.AffectSSNName.toLowerCase(), riResult.getAffectSsnName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.ConcernPointCodeName.toLowerCase(), riResult.getConcernPointCodeName(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.gateway1.toLowerCase(), riResult.getGateway1(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.gateway2.toLowerCase(), riResult.getGateway2(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.ipbr_id.toLowerCase(), riResult.getIpbrId(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.ipbr_name.toLowerCase(), riResult.getIpbrName(), mapParamValues);

                            putParamValue("act" + Constants.paramCode.AssocID.toLowerCase(), riResult.getActAssocID(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.ConcernPointCodeName.toLowerCase(), riResult.getActConcernPointCodeName(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.ConcernSSNName.toLowerCase(), riResult.getActConcernSsnName(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.SubSystemName.toLowerCase(), riResult.getActSubSystemName(), mapParamValues);
                            putParamValue("act" + Constants.paramCode.RemoteAsName.toLowerCase(), riResult.getActRemoteAsName(), mapParamValues);
                            break;
                    }

                    break;
                case Constants.vendorType.ERICSSON:
                    for (String setName : parameterSetNames) {
                        if (setName.toUpperCase().contains("ERI")) {
                            putParamValue("bak" + Constants.paramCode.RNCParameterSetName.toLowerCase(),
                                    setName, mapParamValues);
                            break;
                        }
                    }
                    listPrLinkRncEricsson = new RiSgRcParamRncEricssonServiceImpl().findList(filters);
                    countIp = listPrLinkRncEricsson.size();

                    for (RiSgRcParamRncEricsson bo : listPrLinkRncEricsson) {
                        riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getRncPort());
                        RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIuIp1();
                        RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIuIp2();
                        if (!riResult.getSctpid().contains(bo.getSctpId())) {
                            riResult.setSctpid(riResult.getSctpid() + ";" + bo.getSctpId());
                        }
                        if (!riResult.getIpAccessHostPool().contains(bo.getIpAccessHostPool())) {
                            riResult.setIpAccessHostPool(riResult.getIpAccessHostPool() + ";" + bo.getIpAccessHostPool());
                        }
                        if (!riResult.getSccpApLocal().contains(bo.getSccpApLocal())) {
                            riResult.setSccpApLocal(riResult.getSccpApLocal() + ";" + bo.getSccpApLocal());
                        }
                        riResult.setDscp(bo.getDscp());
                        countBreak++;
                        if (countBreak >= 4) {
                            break;
                        }
                    }
                    for (RiSgRcParamRncEricsson bo : listPrLinkRncEricsson) {
                        riResult.setActIndexM3ua(riResult.getActIndexM3ua() + ";" + bo.getIndexM3ua());
                        riResult.setActDscp(bo.getDscp());
                    }
                    if (countIp == 1) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setRNCPort(riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort() + riResult.getRNCPort());

                    } else if (countIp == 2) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setRNCPort("2905;2906;2906;2905");

                    } else {
                        riResult.setRNCIuIP1(RNCIuIPTemp1);
                        riResult.setRNCIuIP2(RNCIuIPTemp2);
                    }
                    putParamValue("act" + Constants.paramCode.index.toLowerCase(), riResult.getActIndexM3ua(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.sctpid.toLowerCase(), riResult.getSctpid(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.IpAccessHostPool.toLowerCase(), riResult.getIpAccessHostPool(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.SccpApLocal.toLowerCase(), riResult.getSccpApLocal(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.dscp.toLowerCase(), riResult.getDscp(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.dscp.toLowerCase(), riResult.getDscp(), mapParamValues);
                    break;
                case Constants.vendorType.HUAWEI:
                    for (String setName : parameterSetNames) {
                        if (setName.toUpperCase().contains("HUA")) {
                            putParamValue("bak" + Constants.paramCode.RNCParameterSetName.toLowerCase(),
                                    setName, mapParamValues);
                            break;
                        }
                    }
                    listPrLinkRncHuawei = new RiSgRcParamRncHuaweiServiceImpl().findList(filters);
                    countIp = listPrLinkRncHuawei.size();
                    for (RiSgRcParamRncHuawei bo : listPrLinkRncHuawei) {
                        riResult.setSubrack(riResult.getSubrack() + ";" + bo.getSubrack());
                        riResult.setSlotCard(riResult.getSlotCard() + ";" + bo.getSlotCard());
                        riResult.setSctplink(riResult.getSctplink() + ";" + bo.getSctpLink());
                        riResult.setRNCPort(riResult.getRNCPort() + ";" + bo.getRncPort());
                        RNCIuIPTemp1 = RNCIuIPTemp1 + ";" + bo.getRncIp1();
                        RNCIuIPTemp2 = RNCIuIPTemp2 + ";" + bo.getRncIp2();
                        riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + bo.getSignallingLinkId());
                        riResult.setSignallingLinkSetID(bo.getSignallingLinkSetId());
                        riResult.setDSPIndex(bo.getDspIndex());
                        riResult.setCNOperatorID(bo.getCnOperatorId());
                        riResult.setCNNodeID(bo.getCnNodeId());
                        riResult.setAdjacentNodeID(bo.getAdjacentNodeId());
                        riResult.setDestinationEntity(bo.getDestinationEntity());
                        countBreak++;
                        if (countBreak > 4) {
                            break;
                        }

                    }
                    for (RiSgRcParamRncHuawei bo : listPrLinkRncHuawei) {
                        riResult.setActSignallingLinkId(riResult.getActSignallingLinkId() + ";" + bo.getSignallingLinkId());
                        riResult.setActSignallingLinkSetID(bo.getSignallingLinkSetId());
                        riResult.setActDSPIndex(bo.getDspIndex());
                        riResult.setActCNOperatorID(bo.getCnOperatorId());
                        riResult.setActCNNodeID(bo.getCnNodeId());
                        riResult.setActAdjacentNodeID(bo.getAdjacentNodeId());
                        riResult.setActDestinationEntity(bo.getDestinationEntity());
                        riResult.setActSctplink(riResult.getActSctplink() + ";" + bo.getSctpLink());
                    }
                    temp = riResult.getSignallingLinkId();
                    riResult.setSignallingLinkId("");
                    riResult.setSctplink("");
                    countBreak = 0;
                    switch (plan.getNodeRnc().getVersion().getVersionName().toUpperCase()) {
                        case Constants.versionType.HUAWEI_6900:
                            if ("0".equals(mopType)) {
                                for (int i = 0; i < 15; i++) {
                                    if (!temp.contains(";" + String.valueOf(i))) {
                                        riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + String.valueOf(i));
                                        riResult.setSctplink(riResult.getSctplink() + ";" + String.valueOf(i + 1000));
                                        countBreak++;
                                        if (countBreak >= 4) {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                for (int i = 15; i > 0; i--) {
                                    if (!temp.contains(";" + String.valueOf(i))) {
                                        riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + String.valueOf(i));
                                        riResult.setSctplink(riResult.getSctplink() + ";" + String.valueOf(i + 1000));
                                        countBreak++;
                                        if (countBreak >= 4) {
                                            break;
                                        }
                                    }
                                }
                            }

                            break;

                        case Constants.versionType.HUAWEI_6910:
                            if ("0".equals(mopType)) {
                                for (int i = 0; i < 15; i++) {
                                    if (!temp.contains(";" + String.valueOf(i))) {
                                        riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + String.valueOf(i));
                                        riResult.setSctplink(riResult.getSctplink() + ";" + String.valueOf(i + 10000));
                                        countBreak++;
                                        if (countBreak >= 4) {
                                            break;
                                        }
                                    }
                                }
                            } else {
                                for (int i = 15; i > 0; i--) {
                                    if (!temp.contains(";" + String.valueOf(i))) {
                                        riResult.setSignallingLinkId(riResult.getSignallingLinkId() + ";" + String.valueOf(i));
                                        riResult.setSctplink(riResult.getSctplink() + ";" + String.valueOf(i + 10000));
                                        countBreak++;
                                        if (countBreak >= 4) {
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                    }

                    if (countIp == 1) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1 + RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setSubrack(riResult.getSubrack() + riResult.getSubrack() + riResult.getSubrack() + riResult.getSubrack());
                        riResult.setSlotCard(riResult.getSlotCard() + riResult.getSlotCard() + riResult.getSlotCard() + riResult.getSlotCard());
                        riResult.setRNCPort("2905;2906;2906;2905");
                    } else if (countIp == 2) {
                        riResult.setRNCIuIP1(RNCIuIPTemp1 + RNCIuIPTemp2);
                        riResult.setRNCIuIP2(RNCIuIPTemp2 + RNCIuIPTemp1);
                        riResult.setSubrack(riResult.getSubrack() + riResult.getSubrack());
                        riResult.setSlotCard(riResult.getSlotCard() + riResult.getSlotCard());
                        riResult.setRNCPort("2905;2906;2906;2905");
                    } else {
                        riResult.setRNCIuIP1(RNCIuIPTemp1);
                        riResult.setRNCIuIP2(RNCIuIPTemp2);
                    }
                    putParamValue("bak" + Constants.paramCode.Subrack.toLowerCase(), riResult.getSubrack(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.Slot.toLowerCase(), riResult.getSlotCard(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.Sctplink.toLowerCase(), riResult.getSctplink(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.SignallingLinkSetID.toLowerCase(), riResult.getSignallingLinkSetID(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.SignallingLinkID.toLowerCase(), riResult.getSignallingLinkId(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.DSPIndex.toLowerCase(), riResult.getDSPIndex(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.CNOperatorID.toLowerCase(), riResult.getCNOperatorID(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.CNNodeID.toLowerCase(), riResult.getCNNodeID(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.AdjacentNodeID.toLowerCase(), riResult.getAdjacentNodeID(), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.DestinationEntityNo.toLowerCase(), riResult.getDestinationEntity(), mapParamValues);

                    putParamValue("act" + Constants.paramCode.SignallingLinkSetID.toLowerCase(), riResult.getActSignallingLinkSetID(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.SignallingLinkID.toLowerCase(), riResult.getActSignallingLinkId(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.DSPIndex.toLowerCase(), riResult.getActDSPIndex(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.CNOperatorID.toLowerCase(), riResult.getActCNOperatorID(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.CNNodeID.toLowerCase(), riResult.getActCNNodeID(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.AdjacentNodeID.toLowerCase(), riResult.getActAdjacentNodeID(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.DestinationEntityNo.toLowerCase(), riResult.getActDestinationEntity(), mapParamValues);
                    putParamValue("act" + Constants.paramCode.Sctplink.toLowerCase(), riResult.getActSctplink(), mapParamValues);
//                    switch (plan.getNodeRnc().getVersion().getVersionName().toUpperCase()) {
//                        case Constants.versionType.HUAWEI_6900:
//
//                            break;
//                        case Constants.versionType.HUAWEI_6910:
//                    break
//                    }
                    break;

                case Constants.vendorType.ZTE:
                    for (String setName : parameterSetNames) {
                        if (setName.toUpperCase().contains("ZTE")) {
                            putParamValue("bak" + Constants.paramCode.RNCParameterSetName.toLowerCase(),
                                    setName, mapParamValues);
                            break;
                        }
                    }
                    break;
            }
            //Lay cac tham so cho sgsn backup
            filters.clear();
            orders.clear();
            filters.put("nodeCode", plan.getNodeSgsnBackup().getNodeCode());
            orders.put("profilePgi", "ASC");

            if (null != plan.getNodeSgsnBackup().getVendor().getVendorName()) {
                switch (plan.getNodeSgsnBackup().getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        listPrLinkSgsn = new RiSgRcParamLinkSgsnServiceImpl().findList(filters, orders);

                        getParamErricson(plan, riResult, listPrLinkSgsn);
                        break;
                    case Constants.vendorType.NOKIA:
                        String pgi = String.valueOf(Math.round(Math.random() * 2 + 1));
                        filters.put("profilePgi", pgi);
                        listPrLinkSgsn = new RiSgRcParamLinkSgsnServiceImpl().findList(filters, orders);
                        int countLink = 0;
                        for (RiSgRcParamLinkSgsn prLinkSgsn : listPrLinkSgsn) {
                            if (countLink < 4) {
                                SgsnIuIPTemp1 = prLinkSgsn.getIpOne();
                                riResult.setSGSNIuIP1(riResult.getSGSNIuIP1() + ";" + prLinkSgsn.getIpOne());
                                riResult.setSGSNIuIP2(riResult.getSGSNIuIP2() + ";" + prLinkSgsn.getIpTwo());
                                riResult.setPapu(riResult.getPapu() + ";" + prLinkSgsn.getPapu());
                                riResult.setSGSNPort(riResult.getSGSNPort() + ";" + "2905");
                            } else {
                                break;
                            }
                            countLink++;
                        }
                        putParamValue("bak" + Constants.paramCode.PGI.toLowerCase(), pgi, mapParamValues);
                        riResult.setSgsnIuIP(convertIp(SgsnIuIPTemp1, 0, 0));
                        if (riResult.getCPNPGEP1() != null && riResult.getCPNPGEP2() != null) {
                            riResult.setGatewayCPNPGEP(convertIp(riResult.getCPNPGEP1(), 1, 5) + ";" + convertIp(riResult.getCPNPGEP2(), 1, 5));
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        listPrLinkSgsn = new RiSgRcParamLinkSgsnServiceImpl().findList(filters, orders);

                        List<String> lstIP1 = new ArrayList<>();
                        List<String> lstIP2 = new ArrayList<>();
                        for (RiSgRcParamLinkSgsn prLinkSgsn : listPrLinkSgsn) {
                            if (prLinkSgsn.getVrfname() != null && prLinkSgsn.getVrfname().toUpperCase().contains("IUC")) {
                                lstIP1.add(prLinkSgsn.getIpOne());
                                lstIP2.add(prLinkSgsn.getIpTwo());
                                riResult.setVRFName(prLinkSgsn.getVrfname());
                                if (lstIP1.size() == 4) {
                                    break;
                                }
                            }
                        }

                        putParamValue("bak" + Constants.paramCode.VRFName.toLowerCase(), riResult.getVRFName(), mapParamValues);
                        switch (lstIP1.size()) {
                            case 2:
                                riResult.setSGSNIuIP1(StringUtils.join(lstIP1, ";") + ";" + StringUtils.join(lstIP1, ";"));
                                riResult.setSGSNIuIP2(StringUtils.join(lstIP2, ";") + ";" + StringUtils.join(lstIP2, ";"));
                                break;
                            case 3:
                                lstIP1.add(lstIP1.get(1));
                                lstIP2.add(lstIP2.get(1));
                                riResult.setSGSNIuIP1(StringUtils.join(lstIP1, ";"));
                                riResult.setSGSNIuIP2(StringUtils.join(lstIP2, ";"));
                                break;
                            default:
                                riResult.setSGSNIuIP1(StringUtils.join(lstIP1, ";"));
                                riResult.setSGSNIuIP2(StringUtils.join(lstIP2, ";"));
                                break;
                        }
                        riResult.setSGSNPort("2905;2906;2906;2905");
                        if (!lstIP1.isEmpty()) {
                            riResult.setSgsnIuIP(convertIp(lstIP1.get(0), 0, 0));
                        }
                        if (riResult.getCPNPGEP1() != null && riResult.getCPNPGEP2() != null) {
                            riResult.setGatewayCPNPGEP(convertIp(riResult.getCPNPGEP1(), 1, 5) + ";" + convertIp(riResult.getCPNPGEP2(), 1, 5));
                        }
                        //Chuyen doi SPCRNC,SPCSGSN sang danh hecxa
                        if (mapParamValues.containsKey("bak" + Constants.paramCodeSgsn.RNCSPC.toLowerCase())) {
                            mapParamValues.put("bak" + Constants.paramCodeSgsn.RNCSPCh.toLowerCase(), DecToHex(mapParamValues.get("bak" + Constants.paramCodeSgsn.RNCSPC.toLowerCase())));
                        }
                        if (mapParamValues.containsKey("bak" + Constants.paramCodeSgsn.SGSNSPC.toLowerCase())) {
                            mapParamValues.put("bak" + Constants.paramCodeSgsn.SGSNSPCh.toLowerCase(), mapParamValues.get("bak" + Constants.paramCodeSgsn.SGSNSPC.toLowerCase()).toUpperCase());
                        }

                        break;
                }
            }

            //Put lac rac
            if (null != plan.getNodeSgsnBackup().getVendor().getVendorName()) {
                switch (plan.getNodeSgsnBackup().getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.NOKIA:
                        filters.clear();
                        if (Constants.vendorType.NOKIA.equalsIgnoreCase(plan.getNodeSgsnCurrent().getVendor().getVendorName())) {
                            String rncNameForSgsnNokia = new StringBuilder(plan.getNodeRnc().getNodeCode().trim()).replace(1, 2, "").toString();
                            if (plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)
                                    || plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.HUAWEI)) {
                                filters.put("rncCode", plan.getNodeRnc().getNodeCode().trim());
                                filters.put("sgsnActive", plan.getNodeRnc().getNodeCode().trim());
                            } else {
                                filters.put("rncCode", rncNameForSgsnNokia);
                                filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                            }
                        } else {
                            if (plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)
                                    || plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.HUAWEI)) {
                                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                                filters.put("sgsnActive", plan.getNodeRnc().getNodeCode());
                            } else {
                                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                                filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                            }

                        }
                        listPrLacRac = new RiSgRcParamLacRacServiceImpl().findList(filters);

                        int i = 0;
                        String lac1 = "",
                         rac1 = "";
                        mapPrLacRac = new HashMap<>();
                        for (RiSgRcParamLacRac bo : listPrLacRac) {
                            mapPrLacRac.put(bo.getLac() + "#" + bo.getRac(), bo);
                        }
                        for (String bo : mapPrLacRac.keySet()) {
                            if (i == 0) {
                                lac1 = mapPrLacRac.get(bo).getLac();
                                rac1 = mapPrLacRac.get(bo).getRac();
                                putParamValue("bak" + Constants.paramCode.RNCLac1.toLowerCase(), mapPrLacRac.get(bo).getLac(), mapParamValues);
                                putParamValue("bak" + Constants.paramCode.RNCRac1.toLowerCase(), mapPrLacRac.get(bo).getRac(), mapParamValues);
                            } else {
                                riResult.setRNCLac(riResult.getRNCLac() + ";" + mapPrLacRac.get(bo).getLac());
                                riResult.setRNCRac(riResult.getRNCRac() + ";" + mapPrLacRac.get(bo).getRac());
                            }
                            i++;
                        }
                        if (i == 1) {
                            // Truong hop chi co 1 cap LAC - RAC
                            putParamValue("bak" + Constants.paramCode.RNCLac.toLowerCase(), lac1, mapParamValues);
                            putParamValue("bak" + Constants.paramCode.RNCRac.toLowerCase(), rac1, mapParamValues);
                        } else {
                            putParamValue("bak" + Constants.paramCode.RNCLac.toLowerCase(), riResult.getRNCLac(), mapParamValues);
                            putParamValue("bak" + Constants.paramCode.RNCRac.toLowerCase(), riResult.getRNCRac(), mapParamValues);
                        }
                        filters.clear();
                        if (Constants.vendorType.NOKIA.equalsIgnoreCase(plan.getNodeSgsnCurrent().getVendor().getVendorName())) {
                            String rncNameForSgsnNokia = new StringBuilder(plan.getNodeRnc().getNodeCode().trim()).replace(1, 2, "").toString();
                            filters.put("rncCode", rncNameForSgsnNokia);
                            filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                        } else {
                            filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                            filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                        }
                        listPrLacRac = new RiSgRcParamLacRacServiceImpl().findList(filters);
                        for (RiSgRcParamLacRac bo : listPrLacRac) {
                            riResult.setActRNCLac(riResult.getActRNCLac() + ";" + bo.getLac());
                            riResult.setActRNCRac(riResult.getActRNCRac() + ";" + bo.getRac());
                        }
                        putParamValue("act" + Constants.paramCode.RNCLac.toLowerCase(), riResult.getActRNCLac(), mapParamValues);
                        putParamValue("act" + Constants.paramCode.RNCRac.toLowerCase(), riResult.getActRNCRac(), mapParamValues);
                        break;
                    default:
                        filters.clear();

                        if (Constants.vendorType.NOKIA.equalsIgnoreCase(plan.getNodeSgsnCurrent().getVendor().getVendorName())) {
                            String rncNameForSgsnNokia = new StringBuilder(plan.getNodeRnc().getNodeCode().trim()).replace(1, 2, "").toString();
                            if (plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)
                                    || plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.HUAWEI)) {
                                filters.put("rncCode", plan.getNodeRnc().getNodeCode().trim());
                                filters.put("sgsnActive", plan.getNodeRnc().getNodeCode().trim());
                            } else {
                                filters.put("rncCode", rncNameForSgsnNokia);
                                filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                            }
                        } else {
                            if (plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)
                                    || plan.getNodeRnc().getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.HUAWEI)) {
                                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                                filters.put("sgsnActive", plan.getNodeRnc().getNodeCode());
                            } else {
                                filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                                filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                            }
                        }
                        listPrLacRac = new RiSgRcParamLacRacServiceImpl().findList(filters);
                        mapPrLacRac = new HashMap<>();
                        for (RiSgRcParamLacRac bo : listPrLacRac) {
                            mapPrLacRac.put(bo.getLac() + "#" + bo.getRac(), bo);
                        }
                        if (plan.getNodeSgsnBackup().getVendor().getVendorName().toUpperCase().equals(Constants.vendorType.HUAWEI)) {
                            for (String bo : mapPrLacRac.keySet()) {
                                riResult.setRNCLac(riResult.getRNCLac() + ";" + DecToHex(mapPrLacRac.get(bo).getLac()));
                                riResult.setRNCRac(riResult.getRNCRac() + ";" + DecToHex(mapPrLacRac.get(bo).getRac()));
                            }
                        } else {
                            for (String bo : mapPrLacRac.keySet()) {
                                riResult.setRNCLac(riResult.getRNCLac() + ";" + mapPrLacRac.get(bo).getLac());
                                riResult.setRNCRac(riResult.getRNCRac() + ";" + mapPrLacRac.get(bo).getRac());
                            }
                        }

                        filters.clear();
                        if (Constants.vendorType.NOKIA.equalsIgnoreCase(plan.getNodeSgsnCurrent().getVendor().getVendorName())) {
                            String rncNameForSgsnNokia = new StringBuilder(plan.getNodeRnc().getNodeCode().trim()).replace(1, 2, "").toString();
                            filters.put("rncCode", rncNameForSgsnNokia);
                            filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                        } else {
                            filters.put("rncCode", plan.getNodeRnc().getNodeCode());
                            filters.put("sgsnActive", plan.getNodeSgsnCurrent().getNodeCode());
                        }
                        listPrLacRac = new RiSgRcParamLacRacServiceImpl().findList(filters);
                        if (plan.getNodeSgsnBackup().getVendor().getVendorName().toUpperCase().equals(Constants.vendorType.HUAWEI)) {
                            for (RiSgRcParamLacRac bo : listPrLacRac) {
                                riResult.setActRNCLac(riResult.getActRNCLac() + ";" + DecToHex(bo.getLac()));
                                riResult.setActRNCRac(riResult.getActRNCRac() + ";" + DecToHex(bo.getRac()));
                            }
                        } else {
                            for (RiSgRcParamLacRac bo : listPrLacRac) {
                                riResult.setActRNCLac(riResult.getActRNCLac() + ";" + bo.getLac());
                                riResult.setActRNCRac(riResult.getActRNCRac() + ";" + bo.getRac());
                            }
                        }
                        putParamValue("bak" + Constants.paramCode.RNCLac.toLowerCase(), riResult.getRNCLac(), mapParamValues);
                        putParamValue("bak" + Constants.paramCode.RNCRac.toLowerCase(), riResult.getRNCRac(), mapParamValues);
                        putParamValue("act" + Constants.paramCode.RNCLac.toLowerCase(), riResult.getActRNCLac(), mapParamValues);
                        putParamValue("act" + Constants.paramCode.RNCRac.toLowerCase(), riResult.getActRNCRac(), mapParamValues);
                        break;
                }
            }
            riResult.setNPGEPIndex("0;2");
            riResult.setUPNPGEPIndex("0;0;0;0;2;2;2;2");
            putParamValue("bak" + Constants.paramCode.SgsnSctpProfile.toLowerCase(), riResult.getSgsnSctpProfile(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCPort.toLowerCase(), riResult.getRNCPort(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCIuIP1.toLowerCase(), riResult.getRNCIuIP1(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCIuIP2.toLowerCase(), riResult.getRNCIuIP2(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SGSNIuIP1.toLowerCase(), riResult.getSGSNIuIP1(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SGSNIuIP2.toLowerCase(), riResult.getSGSNIuIP2(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.RNCPAPU.toLowerCase(), riResult.getPapu(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SGSNPort.toLowerCase(), riResult.getSGSNPort(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.NPGEPIndex.toLowerCase(), riResult.getNPGEPIndex(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.UPNPGEPIndex.toLowerCase(), riResult.getUPNPGEPIndex(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.GatewayCPNPGEP.toLowerCase(), riResult.getGatewayCPNPGEP(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SgsnIuIP.toLowerCase(), riResult.getSgsnIuIP(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.CPNPGEP1.toLowerCase(), riResult.getCPNPGEP1(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.CPNPGEP2.toLowerCase(), riResult.getCPNPGEP2(), mapParamValues);
            putParamValue("bak" + Constants.paramCode.SGSNName.toLowerCase(), plan.getNodeSgsnBackup().getNodeCode(), mapParamValues);

            //Gan tham so cho RNC ERRICSSON
            // Lay tham so tu bang ri_param_sgsn_rnc cho viec delete and rolback DITY
            if (Constants.vendorType.ERICSSON.equalsIgnoreCase(plan.getNodeSgsnCurrent().getVendor().getVendorName())) {
                filters.clear();
                filters.put("sgsnName", plan.getNodeSgsnCurrent().getNodeCode());
                filters.put("rncName", plan.getNodeRnc().getNodeCode());
                listPrSgsnRnc = new RiSgRcParamSgsnRncServiceImpl().findList(filters);

                for (RiSgRcParamSgsnRnc bo : listPrSgsnRnc) {
                    riResult.setActRNCLasid(bo.getLasid());
                    riResult.setActRncSPC(bo.getScp());
                    riResult.setActRNCLspid(riResult.getActRNCLspid() + ";" + bo.getLspid());
                    riResult.setActRNCRspid(riResult.getActRNCRspid() + ";" + bo.getRspid());
                    riResult.setActRNCRasid(bo.getRasid());
                    riResult.setActRNCIuIP1(riResult.getActRNCIuIP1() + ";" + bo.getIp1());
                    riResult.setActRNCIuIP2(riResult.getActRNCIuIP2() + ";" + bo.getIp2());
                }
                putParamValue("act" + Constants.paramCode.RNCLasid.toLowerCase(), riResult.getActRNCLasid(), mapParamValues);
                putParamValue("act" + Constants.paramCode.RncSPC.toLowerCase(), riResult.getActRncSPC(), mapParamValues);
                putParamValue("act" + Constants.paramCode.RNCLspid.toLowerCase(), riResult.getActRNCLspid(), mapParamValues);
                putParamValue("act" + Constants.paramCode.RNCRspid.toLowerCase(), riResult.getActRNCRspid(), mapParamValues);
                putParamValue("act" + Constants.paramCode.RNCRasid.toLowerCase(), riResult.getActRNCRasid(), mapParamValues);
                putParamValue("act" + Constants.paramCode.RNCIuIP1.toLowerCase(), riResult.getActRNCIuIP1(), mapParamValues);
                putParamValue("act" + Constants.paramCode.RNCIuIP2.toLowerCase(), riResult.getActRNCIuIP2(), mapParamValues);
            }

            filters.clear();
            filters.put("sgsnCode", plan.getNodeSgsnBackup().getNodeCode());
            List<RiSgRcParamUnique> lstUniqueParam = new RiSgRcParamUniqueServiceImpl().findList(filters);

            filters.clear();
            filters.put("processNode", plan.getNodeSgsnBackup().getNodeCode());
            List<RiSgRcParam> lstParamValues = new RiSgRcParamServiceImpl().findList(filters);

            //RNCDEX, RNCLinkSetIndex, RNCRouteIndex, SignallingLink, RNCDPCIndex, RNCSCMGID, RNCRnapID (Dung cho SGSN Huawei Backup)
            if (Constants.vendorType.HUAWEI.equalsIgnoreCase(plan.getNodeSgsnBackup().getVendor().getVendorName())) {
                long rncIndex = getRncIndex(plan, lstUniqueParam, lstParamValues);
                if (rncIndex >= 0) {
                    putParamValue("bak" + Constants.paramCode.RNCIndex.toLowerCase(), String.valueOf(rncIndex), mapParamValues);
                    RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.RNCIndex,
                            String.valueOf(rncIndex), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                    uniqueParams.add(prU);
                }

                //ECUSubRack, ECUSlot
                String ecuSubRack = mapParamValues.get("bak" + Constants.paramCode.ECUSubrack.toLowerCase());
                String ecuSlot = mapParamValues.get("bak" + Constants.paramCode.ECUSlot.toLowerCase());
                List<String> ecuSubracks = new ArrayList<>(Arrays.asList(ecuSubRack.split(";")));
                List<String> ecuSlots = new ArrayList<>(Arrays.asList(ecuSlot.split(";")));
                int countCard = ecuSubracks.size();
                if (!ecuSubRack.isEmpty()) {
                    if (countCard == 2) {
                        putParamValue("bak" + Constants.paramCode.ECUSubrack.toLowerCase(),
                                StringUtils.join(ecuSubracks, ";") + ";" + StringUtils.join(ecuSubracks, ";"), mapParamValues);

                        putParamValue("bak" + Constants.paramCode.ECUSlot.toLowerCase(),
                                StringUtils.join(ecuSlots, ";") + ";" + StringUtils.join(ecuSlots, ";"), mapParamValues);
                    }

                    if (countCard == 3) {
                        int index = (int) rncIndex % 3;
                        putParamValue("bak" + Constants.paramCode.ECUSubrack.toLowerCase(),
                                StringUtils.join(ecuSubracks, ";") + ";" + ecuSubracks.get(index), mapParamValues);

                        putParamValue("bak" + Constants.paramCode.ECUSlot.toLowerCase(),
                                StringUtils.join(ecuSlots, ";") + ";" + ecuSlots.get(index), mapParamValues);
                    }

                    if (countCard > 4) {
                        putParamValue("bak" + Constants.paramCode.ECUSubrack.toLowerCase(),
                                StringUtils.join(ecuSubracks.subList(0, 4), ";"), mapParamValues);

                        putParamValue("bak" + Constants.paramCode.ECUSlot.toLowerCase(),
                                StringUtils.join(ecuSlots.subList(0, 4), ";"), mapParamValues);
                    }
                }

                long rncDex = getRncDex(plan, lstUniqueParam, lstParamValues);
                if (rncDex >= 0) {
                    putParamValue("bak" + Constants.paramCode.RNCDEX.toLowerCase(), String.valueOf(rncDex), mapParamValues);
                    RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.RNCDEX,
                            String.valueOf(rncDex), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                    uniqueParams.add(prU);
                }

                long rncLinkSetIndex = getRncLinkSetIndex(plan, rncDex, lstUniqueParam, lstParamValues);
                if (rncLinkSetIndex >= 0) {
                    putParamValue("bak" + Constants.paramCode.RNCLinkSetIndex.toLowerCase(), String.valueOf(rncLinkSetIndex), mapParamValues);
                    RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.RNCLinkSetIndex,
                            String.valueOf(rncLinkSetIndex), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                    uniqueParams.add(prU);
                }

                long rncRouteIndex = getRncRouteIndex(plan, rncDex, lstUniqueParam, lstParamValues);
                if (rncRouteIndex >= 0) {
                    putParamValue("bak" + Constants.paramCode.RNCRouteIndex.toLowerCase(), String.valueOf(rncRouteIndex), mapParamValues);
                    RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.RNCRouteIndex,
                            String.valueOf(rncRouteIndex), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                    uniqueParams.add(prU);
                }

                if (countCard >= 4) {
                    String signallingLink = getSignallingLink(plan, lstUniqueParam, lstParamValues, 1);
                    if (!"-1".equals(signallingLink)) {
                        putParamValue("bak" + Constants.paramCode.SignallingLink.toLowerCase(), String.valueOf(signallingLink), mapParamValues);
                        RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.SignallingLink,
                                String.valueOf(signallingLink), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                        uniqueParams.add(prU);
                    }
                } else {
                    String signallingLink = getSignallingLink(plan, lstUniqueParam, lstParamValues, 2);
                    String[] values = signallingLink.split(";");
                    if (countCard == 2) {
                        putParamValue("bak" + Constants.paramCode.SignallingLink.toLowerCase(),
                                values[0] + ";" + values[0] + ";" + values[1] + ";" + values[1], mapParamValues);
                        RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.SignallingLink,
                                signallingLink, new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                        uniqueParams.add(prU);
                    } else if (countCard == 3) {
                        putParamValue("bak" + Constants.paramCode.SignallingLink.toLowerCase(),
                                values[0] + ";" + values[0] + ";" + values[0] + ";" + values[1], mapParamValues);
                        RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.SignallingLink,
                                signallingLink, new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                        uniqueParams.add(prU);
                    }
                }
                //Check trung
                filters.clear();
                filters.put("processNode", plan.getNodeSgsnBackup().getNodeCode());
                List<RiSgRcParam> lstParam = new RiSgRcParamServiceImpl().findList(filters);

                Map<String, String> mapSlot = new HashMap<>();
                Map<String, String> mapSignallingLink = new HashMap<>();
                for (RiSgRcParam param : lstParam) {
                    if (param.getParamCode().equalsIgnoreCase(Constants.paramCode.Slot)) {
                        mapSlot.put(param.getNodeCode(), param.getParamValue());
                    }
                    if (param.getParamCode().equalsIgnoreCase(Constants.paramCode.SignallingLink)) {
                        mapSignallingLink.put(param.getNodeCode(), param.getParamValue());
                    }

                }
                Map<String, String> mapSlotSignalling = new HashMap<>();
                for (String rncCode : mapSlot.keySet()) {
                    if (mapSignallingLink.containsKey(rncCode)) {
                        String[] slots = mapSlot.get(rncCode).split(";");
                        String[] signallingLink = mapSignallingLink.get(rncCode).split(";");
                        for (int i = 0; i < slots.length; i++) {
                            if (i <= signallingLink.length) {
                                mapSlotSignalling.put(slots[i] + "_" + signallingLink[i], rncCode);
                            }
                        }
                    }
                }
                ecuSlot = mapParamValues.get("bak" + Constants.paramCode.ECUSlot.toLowerCase());
                String signallingLink = mapParamValues.get("bak" + Constants.paramCode.SignallingLink.toLowerCase());
                ecuSubracks = new ArrayList<>(Arrays.asList(ecuSubRack.split(";")));
                ecuSlots = new ArrayList<>(Arrays.asList(ecuSlot.split(";")));
                List<String> signallingLinks = new ArrayList<>(Arrays.asList(signallingLink.split(";")));
                Map<Integer, String> mapTrung = new HashMap<>();
                if (!ecuSubracks.isEmpty()) {

                    for (int i = 0; i < ecuSlots.size(); i++) {
                        if (i <= signallingLinks.size() && i <= ecuSlots.size()) {
                            if (mapSlotSignalling.containsKey(ecuSlots.get(i) + "_" + signallingLinks.get(i))) {
                                mapTrung.put(i, ecuSlots.get(i) + "_" + signallingLinks.get(i));
                            } else {
                                mapSlotSignalling.put(ecuSlots.get(i) + "_" + signallingLinks.get(i), plan.getNodeRnc().getNodeCode());
                            }
                        }
                    }
                }
                int count = 0;
                int sizeMapTrung = mapTrung.size();
                if (!mapTrung.isEmpty()) {
                    while (sizeMapTrung == 0) {
                        count++;
                        for (int i : mapTrung.keySet()) {
                            if (mapSlotSignalling.containsKey(ecuSlots.get(i) + "_" + String.valueOf(Long.parseLong(signallingLinks.get(0)) - count))) {
                                sizeMapTrung--;
                                signallingLinks.add(i, String.valueOf(Long.parseLong(signallingLinks.get(0)) - count));
                            }
                        }
                    }
                    putParamValue("bak" + Constants.paramCode.SignallingLink.toLowerCase(),
                            StringUtils.join(signallingLinks, ";"), mapParamValues);
//                RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.SignallingLink,
//                                String.valueOf(StringUtils.join(signallingLinks, ";")), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
//                        uniqueParams.add(prU);

                }
                mapParamValues.get("bak" + Constants.paramCode.ECUSubrack.toLowerCase());

                long rncDpcIndex = getRncDpcIndex(plan, rncDex, lstUniqueParam, lstParamValues);
                if (rncDpcIndex >= 0) {
                    putParamValue("bak" + Constants.paramCode.RNCDPCIndex.toLowerCase(), String.valueOf(rncDpcIndex), mapParamValues);
                    RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.RNCDPCIndex,
                            String.valueOf(rncDpcIndex), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                    uniqueParams.add(prU);
                }

                long rncScmgid = getRncScmgid(plan, lstUniqueParam, lstParamValues);
                if (rncScmgid >= 0) {
                    putParamValue("bak" + Constants.paramCode.RNCSCMGID.toLowerCase(), String.valueOf(rncScmgid), mapParamValues);
                    putParamValue("bak" + Constants.paramCode.RNCRnapID.toLowerCase(), String.valueOf(rncScmgid + 1), mapParamValues);
                    RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.RNCSCMGID,
                            String.valueOf(rncScmgid), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                    uniqueParams.add(prU);
                }

                setSgsnUserIP(mapParamValues);

                //SGSNSPC
                String sgsnSpc = mapParamValues.get("bak" + Constants.paramCode.SGSNSPC.toLowerCase());
                mapParamValues.put("bak" + Constants.paramCode.SGSNSPC.toLowerCase(), hexToDec(sgsnSpc));
            }

            //SgsnUserIP, SGSNSPC, bakAssocLinkIndex (Dung cho SGSN NOKIA Backup)
            if (Constants.vendorType.NOKIA.equalsIgnoreCase(plan.getNodeSgsnBackup().getVendor().getVendorName())) {
                //bakAssocLinkIndex
                long assocLinkIndex = getAssocLinkIndex(plan, lstUniqueParam, lstParamValues);
                if (assocLinkIndex >= 0) {
                    putParamValue("bak" + Constants.paramCode.AssocLinkIndex.toLowerCase(), String.valueOf(assocLinkIndex), mapParamValues);
                    RiSgRcParamUnique prU = new RiSgRcParamUnique(plan.getNodeRnc().getNodeCode(), Constants.paramCode.AssocLinkIndex,
                            String.valueOf(assocLinkIndex), new Date(), plan.getNodeSgsnBackup().getNodeCode(), plan.getId());
                    uniqueParams.add(prU);
                }

                //SGSNSPC
                String sgsnSpc = mapParamValues.get("bak" + Constants.paramCode.SGSNSPC.toLowerCase());
                mapParamValues.put("bak" + Constants.paramCode.SGSNSPC.toLowerCase(), hexToDec(sgsnSpc));

                //SgsnUserIP
                setSgsnUserIP(mapParamValues);
            }

            //RNCSPC
            if (Constants.vendorType.NOKIA.equalsIgnoreCase(plan.getNodeSgsnCurrent().getVendor().getVendorName())
                    || Constants.vendorType.HUAWEI.equalsIgnoreCase(plan.getNodeSgsnCurrent().getVendor().getVendorName())) {
                String rncSPC = mapParamValues.get("bak" + Constants.paramCode.RncSPC.toLowerCase());
                mapParamValues.put("bak" + Constants.paramCode.RncSPC.toLowerCase(), hexToDec(rncSPC));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private void getParamErricson(RiSgRcPlan plan, RiSgRcResult riResult, List<RiSgRcParamLinkSgsn> listPrLinkSgsn) {
        String SgsnSctpProfiletemp = "";
        int countCheck = 0;
        String SgsnIuIPTemp1 = "";
        OUTER:
        for (RiSgRcParamLinkSgsn prLinkSgsn : listPrLinkSgsn) {
            switch (plan.getNodeRnc().getVendor().getVendorName().toUpperCase()) {
                case Constants.vendorType.ERICSSON:
                    if (prLinkSgsn.getProfilePgi().equals(2L) || prLinkSgsn.getProfilePgi().equals(4L)) {
                        countCheck++;
                        SgsnSctpProfiletemp = SgsnSctpProfiletemp + ";" + prLinkSgsn.getProfilePgi();
                        riResult.setSGSNIuIP1(riResult.getSGSNIuIP1() + ";" + prLinkSgsn.getIpOne());
                        riResult.setSGSNIuIP2(riResult.getSGSNIuIP2() + ";" + prLinkSgsn.getIpTwo());
                        riResult.setSGSNPort(riResult.getSGSNPort() + ";" + prLinkSgsn.getPort());
                        SgsnIuIPTemp1 = prLinkSgsn.getIpOne();
                        if (countCheck == 2) {
                            break OUTER;
                        }
                    }
                    break;
                case Constants.vendorType.NOKIA:
                    if (prLinkSgsn.getProfilePgi().equals(5L) || prLinkSgsn.getProfilePgi().equals(6L)) {
                        countCheck++;
                        SgsnSctpProfiletemp = SgsnSctpProfiletemp + ";" + prLinkSgsn.getProfilePgi();
                        riResult.setSGSNIuIP1(riResult.getSGSNIuIP1() + ";" + prLinkSgsn.getIpOne());
                        riResult.setSGSNIuIP2(riResult.getSGSNIuIP2() + ";" + prLinkSgsn.getIpTwo());
                        riResult.setSGSNPort(riResult.getSGSNPort() + ";" + prLinkSgsn.getPort());
                        SgsnIuIPTemp1 = prLinkSgsn.getIpOne();
                        if (countCheck == 2) {
                            break OUTER;
                        }
                    }
                    break;
                case Constants.vendorType.HUAWEI:
                    if (prLinkSgsn.getProfilePgi().equals(7L) || prLinkSgsn.getProfilePgi().equals(8L)) {
                        countCheck++;
                        SgsnSctpProfiletemp = SgsnSctpProfiletemp + ";" + prLinkSgsn.getProfilePgi();
                        riResult.setSGSNIuIP1(riResult.getSGSNIuIP1() + ";" + prLinkSgsn.getIpOne());
                        riResult.setSGSNIuIP2(riResult.getSGSNIuIP2() + ";" + prLinkSgsn.getIpTwo());
                        riResult.setSGSNPort(riResult.getSGSNPort() + ";" + prLinkSgsn.getPort());
                        SgsnIuIPTemp1 = prLinkSgsn.getIpOne();
                        if (countCheck == 2) {
                            break OUTER;
                        }
                    }
                    break;
                case Constants.vendorType.ZTE:
                    if (prLinkSgsn.getProfilePgi().equals(9L) || prLinkSgsn.getProfilePgi().equals(10L)) {
                        countCheck++;
                        SgsnSctpProfiletemp = SgsnSctpProfiletemp + ";" + prLinkSgsn.getProfilePgi();
                        riResult.setSGSNIuIP1(riResult.getSGSNIuIP1() + ";" + prLinkSgsn.getIpOne());
                        riResult.setSGSNIuIP2(riResult.getSGSNIuIP2() + ";" + prLinkSgsn.getIpTwo());
                        riResult.setSGSNPort(riResult.getSGSNPort() + ";" + prLinkSgsn.getPort());
                        SgsnIuIPTemp1 = prLinkSgsn.getIpOne();
                        if (countCheck == 2) {
                            break OUTER;
                        }
                    }
                    break;
            }
        }
        if (countCheck != 2) {
            SgsnSctpProfiletemp = ";2;4";
        }
        //Ket qua cuoi cung 2;4;2;4
        riResult.setSgsnSctpProfile(riResult.getSgsnSctpProfile() + SgsnSctpProfiletemp);
        riResult.setSgsnSctpProfile(riResult.getSgsnSctpProfile() + SgsnSctpProfiletemp);

        riResult.setSGSNIuIP1(riResult.getSGSNIuIP1() + riResult.getSGSNIuIP1());
        riResult.setSGSNIuIP2(riResult.getSGSNIuIP2() + riResult.getSGSNIuIP2());
        riResult.setSGSNPort(riResult.getSGSNPort() + riResult.getSGSNPort());

        riResult.setSgsnIuIP(convertIp(SgsnIuIPTemp1, 0, 0));
        if (riResult.getCPNPGEP1() != null && riResult.getCPNPGEP2() != null) {
            riResult.setGatewayCPNPGEP(convertIp(riResult.getCPNPGEP1(), 1, 5) + ";" + convertIp(riResult.getCPNPGEP2(), 1, 5));
        }
    }

    private void setSgsnUserIP(Map<String, String> mapParamValues) {
        //SgsnUserIP
        String sgsnUserIP = mapParamValues.get("bak" + Constants.paramCode.SgsnUserIP.toLowerCase());
        if (sgsnUserIP.contains(";")) {
            String[] arrs = sgsnUserIP.split(";");
            if (arrs != null && arrs.length > 1) {
                String ip1 = arrs[0];
                String ip2 = arrs[1];
                String sgsnUserIPFix = ip1 + ";" + ip1 + ";" + ip2 + ";" + ip2;
                sgsnUserIPFix = sgsnUserIPFix + ";" + sgsnUserIPFix;
                mapParamValues.put("bak" + Constants.paramCode.SgsnUserIP.toLowerCase(), sgsnUserIPFix);
            }
        }
    }

    private String hexToDec(String hexStr) {
        if (hexStr == null || hexStr.isEmpty()) {
            return "";
        }
        return String.valueOf(Integer.parseInt(hexStr.trim().replaceFirst("0x", ""), 16));
    }

    public static String DecToHex(String hexStr) {
        try {
            if (hexStr == null || hexStr.isEmpty()) {
                return "";
            } else {
                return Integer.toHexString(Integer.parseInt(hexStr)).toUpperCase();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return "";
        }

    }

    private List<RiSgRcParamUnique> getListUniqueParam(List<RiSgRcParamUnique> lstUniqueParam, String paramCode) {
        List<RiSgRcParamUnique> params = new ArrayList<>();
        for (RiSgRcParamUnique pr : lstUniqueParam) {
            if (pr.getParamCode().equalsIgnoreCase(paramCode)) {
                params.add(pr);
            }
        }
        return params;
    }

    private List<RiSgRcParam> getListParam(List<RiSgRcParam> lstParam, String paramCode) {
        List<RiSgRcParam> params = new ArrayList<>();
        for (RiSgRcParam pr : lstParam) {
            if (pr.getParamCode().equalsIgnoreCase(paramCode)) {
                params.add(pr);
            }
        }
        return params;
    }

    private long getRncScmgid(RiSgRcPlan plan, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.RNCSCMGID);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.RNCSCMGID);

        long rncScmgid = -1;
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                rncScmgid = Long.parseLong(prU.getParamValue());
            }
        }
        if (lstValues != null && !lstValues.isEmpty() && rncScmgid < 0) {
            for (long i = 2046; i >= 0; i--) {
                boolean flagExist = false;
                for (RiSgRcParam pr : lstValues) {
                    if (pr.getParamValue().equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (RiSgRcParamUnique prU : lstUniqueParam) {
                        if (String.valueOf(i).equals(prU.getParamValue())) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        rncScmgid = i;
                        // check rncScmgid + 1 khong ton tai
                        flagExist = false;
                        for (RiSgRcParam pr : lstValues) {
                            if (pr.getParamValue().equals(String.valueOf(rncScmgid + 1))) {
                                flagExist = true;
                                break;
                            }
                        }
                        if (!flagExist) {
                            for (RiSgRcParamUnique prU : lstUniqueParam) {
                                if (String.valueOf(rncScmgid + 1).equals(prU.getParamValue())) {
                                    flagExist = true;
                                    break;
                                }
                            }
                            if (!flagExist) {
                                return rncScmgid;
                            }
                        }
                        break;
                    }
                }
            }
        }
        return rncScmgid;
    }

    private String getSignallingLink(RiSgRcPlan plan, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params, int countValue) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.SignallingLink);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.SignallingLink);

        String signallingLink = "-1";
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                signallingLink = prU.getParamValue();
            }
        }
        if (lstValues != null && !lstValues.isEmpty() && "-1".equals(signallingLink)) {
            List<String> values = new ArrayList<>();
            List<String> uniqValues = new ArrayList<>();
            for (RiSgRcParam pr : lstValues) {
                String[] arr = pr.getParamValue().split(";");
                if (arr != null && arr.length > 0) {
                    for (String vl : arr) {
                        if (!values.contains(vl)) {
                            values.add(vl);
                        }
                    }
                }
            }
            for (RiSgRcParamUnique pr : lstUniqueParam) {
                String[] arr = pr.getParamValue().split(";");
                if (arr != null && arr.length > 0) {
                    for (String vl : arr) {
                        if (!uniqValues.contains(vl)) {
                            uniqValues.add(vl);
                        }
                    }
                }
            }
            for (long i = 255; i >= 0; i--) {
                boolean flagExist = false;
                for (String vl : values) {
                    if (vl.equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (String prU : uniqValues) {
                        if (String.valueOf(i).equals(prU)) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        signallingLink = String.valueOf(i);
                        break;
                    }
                }
            }
            if (countValue == 2) {
                for (long i = Long.parseLong(signallingLink) - 1; i >= 0; i--) {
                    boolean flagExist = false;
                    for (String vl : values) {
                        if (vl.equals(String.valueOf(i))) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        for (String prU : uniqValues) {
                            if (String.valueOf(i).equals(prU)) {
                                flagExist = true;
                                break;
                            }
                        }
                        if (!flagExist) {
                            signallingLink += ";" + String.valueOf(i);
                            break;
                        }
                    }
                }
            }
        }
        return signallingLink;
    }

    private long getRncDpcIndex(RiSgRcPlan plan, long rncDex, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.RNCDPCIndex);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.RNCDPCIndex);

        long rncDpcIndex = -1;

        // Lay lai gia tri da sinh cho MOP truoc
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                rncDpcIndex = Long.parseLong(prU.getParamValue());
            }
        }

        // Lay gia tri rncDex neu khong trung
        if (rncDpcIndex < 0) {
            boolean flagExist = false;
            for (RiSgRcParam pr : lstValues) {
                if (pr.getParamValue().equals(String.valueOf(rncDex))) {
                    flagExist = true;
                    break;
                }
            }
            if (!flagExist) {
                for (RiSgRcParamUnique prU : lstUniqueParam) {
                    if (String.valueOf(rncDex).equals(prU.getParamValue())) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    return rncDex;
                }
            }
        }

        if (lstValues != null && !lstValues.isEmpty() && rncDpcIndex < 0) {
            for (long i = 1279; i >= 0; i--) {
                boolean flagExist = false;
                for (RiSgRcParam pr : lstValues) {
                    if (pr.getParamValue().equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (RiSgRcParamUnique prU : lstUniqueParam) {
                        if (String.valueOf(i).equals(prU.getParamValue())) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        rncDpcIndex = i;
                        break;
                    }
                }
            }
        }
        return rncDpcIndex;
    }

    private long getRncRouteIndex(RiSgRcPlan plan, long rncDex, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.RNCRouteIndex);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.RNCRouteIndex);

        long rncRouteIndex = -1;

        // Lay lai gia tri da sinh cho MOP truoc
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                rncRouteIndex = Long.parseLong(prU.getParamValue());
            }
        }

        // Lay gia tri rncDex neu khong trung
        if (rncRouteIndex < 0) {
            boolean flagExist = false;
            for (RiSgRcParam pr : lstValues) {
                if (pr.getParamValue().equals(String.valueOf(rncDex))) {
                    flagExist = true;
                    break;
                }
            }
            if (!flagExist) {
                for (RiSgRcParamUnique prU : lstUniqueParam) {
                    if (String.valueOf(rncDex).equals(prU.getParamValue())) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    return rncDex;
                }
            }
        }

        if (lstValues != null && !lstValues.isEmpty() && rncRouteIndex < 0) {
            for (long i = 1279; i >= 0; i--) {
                boolean flagExist = false;
                for (RiSgRcParam pr : lstValues) {
                    if (pr.getParamValue().equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (RiSgRcParamUnique prU : lstUniqueParam) {
                        if (String.valueOf(i).equals(prU.getParamValue())) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        rncRouteIndex = i;
                        break;
                    }
                }
            }
        }
        return rncRouteIndex;
    }

    private long getRncLinkSetIndex(RiSgRcPlan plan, long rncDex, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.RNCLinkSetIndex);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.RNCLinkSetIndex);

        long rncLinkSetIndex = -1;

        // Lay lai gia tri da sinh cho MOP truoc
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                rncLinkSetIndex = Long.parseLong(prU.getParamValue());
            }
        }

        // Lay gia tri rncDex neu khong trung
        if (rncLinkSetIndex < 0) {
            boolean flagExist = false;
            for (RiSgRcParam pr : lstValues) {
                if (pr.getParamValue().equals(String.valueOf(rncDex))) {
                    flagExist = true;
                    break;
                }
            }
            if (!flagExist) {
                for (RiSgRcParamUnique prU : lstUniqueParam) {
                    if (String.valueOf(rncDex).equals(prU.getParamValue())) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    return rncDex;
                }
            }
        }

        if (lstValues != null && !lstValues.isEmpty() && rncLinkSetIndex < 0) {
            for (long i = 1279; i >= 0; i--) {
                boolean flagExist = false;
                for (RiSgRcParam pr : lstValues) {
                    if (pr.getParamValue().equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (RiSgRcParamUnique prU : lstUniqueParam) {
                        if (String.valueOf(i).equals(prU.getParamValue())) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        rncLinkSetIndex = i;
                        break;
                    }
                }
            }
        }
        return rncLinkSetIndex;
    }

    private long getRncIndex(RiSgRcPlan plan, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.RNCIndex);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.RNCIndex);

        long rncIndex = -1;
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                rncIndex = Long.parseLong(prU.getParamValue());
            }
        }
        if (lstValues != null && !lstValues.isEmpty() && rncIndex < 0) {
            for (long i = 512; i >= 0; i--) {
                boolean flagExist = false;
                for (RiSgRcParam pr : lstValues) {
                    if (pr.getParamValue().equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (RiSgRcParamUnique prU : lstUniqueParam) {
                        if (String.valueOf(i).equals(prU.getParamValue())) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        rncIndex = i;
                        break;
                    }
                }
            }
        }
        return rncIndex;
    }

    private long getRncDex(RiSgRcPlan plan, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.RNCDEX);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.RNCDEX);

        long rncDex = -1;
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                rncDex = Long.parseLong(prU.getParamValue());
            }
        }
        if (lstValues != null && !lstValues.isEmpty() && rncDex < 0) {
            for (long i = 639; i >= 0; i--) {
                boolean flagExist = false;
                for (RiSgRcParam pr : lstValues) {
                    if (pr.getParamValue().equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (RiSgRcParamUnique prU : lstUniqueParam) {
                        if (String.valueOf(i).equals(prU.getParamValue())) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        rncDex = i;
                        break;
                    }
                }
            }
        }
        return rncDex;
    }

    private long getAssocLinkIndex(RiSgRcPlan plan, final List<RiSgRcParamUnique> uniqueParams,
            final List<RiSgRcParam> params) throws Exception {

        List<RiSgRcParamUnique> lstUniqueParam = getListUniqueParam(uniqueParams, Constants.paramCode.AssocLinkIndex);
        List<RiSgRcParam> lstValues = getListParam(params, Constants.paramCode.AssocLinkIndex);

        long assocLinkIndex = -1;
        for (RiSgRcParamUnique prU : lstUniqueParam) {
            if (plan.getNodeRnc().getNodeCode().equalsIgnoreCase(prU.getRncCode())) {
                assocLinkIndex = Long.parseLong(prU.getParamValue());
            }
        }
        if (lstValues != null && !lstValues.isEmpty() && assocLinkIndex < 0) {
            for (long i = 8175; i >= 0; i--) {
                boolean flagExist = false;
                for (RiSgRcParam pr : lstValues) {
                    if (pr.getParamValue().equals(String.valueOf(i))) {
                        flagExist = true;
                        break;
                    }
                }
                if (!flagExist) {
                    for (RiSgRcParamUnique prU : lstUniqueParam) {
                        if (String.valueOf(i).equals(prU.getParamValue())) {
                            flagExist = true;
                            break;
                        }
                    }
                    if (!flagExist) {
                        assocLinkIndex = i;
                        break;
                    }
                }
            }
        }
        return assocLinkIndex;
    }

    public void putParamValue(String paramCode, String paramValue, Map<String, String> mapParamValues) {
        try {
            if (paramValue != null) {
                while (paramValue.startsWith(";")) {
                    paramValue = paramValue.substring(1);
                }
                mapParamValues.put(paramCode, paramValue.trim());
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public String convertIp(String ip, int type, int i) {
        String result;
        try {
            if (ip != null && !"".equals(ip)) {
                String[] str = ip.split("\\.");
                if (type == 0) {
                    result = str[0] + "." + str[1] + "." + str[2] + "." + "0";
                } else if (type == 1) {
                    result = str[0] + "." + str[1] + "." + str[2] + "." + String.valueOf(Long.valueOf(str[3]) + i);
                } else {
                    result = ip;
                }
            } else {
                return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return ip;
        }
        return result;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Common">
    public HashMap<String, Node> mapNode() {
        HashMap<String, Node> mapNode = new HashMap<>();
        try {
            List<Node> lstNode = new NodeServiceImpl().findList();
            for (Node bo : lstNode) {
                mapNode.put(bo.getNodeCode(), bo);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return mapNode;
    }

    public HashMap<String, RiSgRcParam> getMapParam() {
        HashMap<String, RiSgRcParam> mapParam = new HashMap<>();
        List<RiSgRcParam> lstParam;
        Map<String, Object> filters = new HashMap<>();
        List<String> listParamCode = new ArrayList<>();
        try {
            listParamCode.add(Constants.paramCodeRnc.RNCSAU);
            listParamCode.add(Constants.paramCodeRnc.RNCPDP);
            listParamCode.add(Constants.paramCodeSgsn.currentSau);
            listParamCode.add(Constants.paramCodeSgsn.currentPdp);
            listParamCode.add(Constants.paramCodeSgsn.licenseSau);
            listParamCode.add(Constants.paramCodeSgsn.licensePDP);
            filters.put("paramCode-EXAC", listParamCode);
            lstParam = new RiSgRcParamServiceImpl().findList(filters);
            for (RiSgRcParam bo : lstParam) {
                try {
                    mapParam.put(bo.getNodeCode() + "##" + bo.getParamCode(), bo);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return mapParam;
    }

    public HashMap<String, HashMap<String, RiSgRcParam>> getMapParamList() {
        HashMap<String, HashMap<String, RiSgRcParam>> mapParam = new HashMap<>();
        List<RiSgRcParam> lstParam;
        Map<String, Object> filters = new HashMap<>();

        try {
            filters.put("processType", "1");
            lstParam = new RiSgRcParamServiceImpl().findList(filters);
            for (RiSgRcParam bo : lstParam) {
                try {
                    if (bo.getNodeCode() != null && bo.getProcessNode() != null) {
                        if (mapParam.containsKey(bo.getNodeCode().trim() + "#" + bo.getProcessNode().trim())) {
                            mapParam.get(bo.getNodeCode() + "#" + bo.getProcessNode().trim()).put(bo.getParamCode(), bo);
                        } else {
                            HashMap<String, RiSgRcParam> mapParamTemp = new HashMap<>();
                            mapParamTemp.put(bo.getParamCode(), bo);
                            mapParam.put(bo.getNodeCode().trim() + "#" + bo.getProcessNode().trim(), mapParamTemp);
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return mapParam;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CrossCheck">
    public boolean crossCheck(List<String> listNodeSgsnActive) {

        List<RiSgRcPlan> listRescuePlan;
        Map<String, Object> filters;
        Map<String, String> orders;
        boolean check = true;
        try {
            HashMap<String, RiSgRcParam> mapParam = getMapParam();
            for (String nodeSgsnActive : listNodeSgsnActive) {
                try {
                    filters = new HashMap<>();
                    filters.put("nodeSgsnCurrent.nodeCode", nodeSgsnActive);
                    orders = new LinkedHashMap<>();
                    orders.put("nodeSgsnCurrent.nodeCode", "ASC");
                    orders.put("nodeSgsnBackup.nodeCode", "ASC");
                    orders.put("nodeSgsnBackup.province.areaCode", "ASC");
                    orders.put("nodeRnc.province.areaCode", "ASC");
                    orders.put("nodeRnc.nodeCode", "ASC");
                    listRescuePlan = new RiSgRcPlanServiceImpl().findList(filters, orders);
                    Long sauBackupRnc = 0L;
                    Long pdpBackupRnc = 0L;
                    String nodeSgsnBackupOld = null;
                    for (RiSgRcPlan boPlan : listRescuePlan) {

                        try {
                            if (nodeSgsnBackupOld != null && !nodeSgsnBackupOld.equalsIgnoreCase(boPlan.getNodeSgsnBackup().getNodeCode())) {
                                sauBackupRnc = 0L;
                                pdpBackupRnc = 0L;
                            }
                            if (mapParam.containsKey(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCSAU)) {
                                try {
                                    boPlan.setSauRnc(Long.valueOf(mapParam.get(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCSAU).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCPDP)) {
                                try {
                                    boPlan.setPdpRnc(Long.valueOf(mapParam.get(boPlan.getNodeRnc().getNodeCode() + "##" + Constants.paramCodeRnc.RNCPDP).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }

                            if (mapParam.containsKey(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.currentSau)) {
                                try {
                                    boPlan.setSauSgsnCurrent(Long.valueOf(mapParam.get(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.currentSau).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.currentPdp)) {
                                try {
                                    boPlan.setPdpSgsnCurrent(Long.valueOf(mapParam.get(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.currentPdp).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.licenseSau)) {
                                try {
                                    boPlan.setLicenseSauSgsnCurrent(Long.valueOf(mapParam.get(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.licenseSau).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.licensePDP)) {
                                try {
                                    boPlan.setLicensePdpSgsnCurrent(Long.valueOf(mapParam.get(boPlan.getNodeSgsnCurrent().getNodeCode() + "##" + Constants.paramCodeSgsn.licensePDP).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }

                            if (mapParam.containsKey(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.currentSau)) {
                                try {
                                    boPlan.setSauSgsnBackup(Long.valueOf(mapParam.get(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.currentSau).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.currentPdp)) {
                                try {
                                    boPlan.setPdpSgsnBackup(Long.valueOf(mapParam.get(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.currentPdp).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.licenseSau)) {
                                try {
                                    boPlan.setLicenseSauSgsnBackup(Long.valueOf(mapParam.get(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.licenseSau).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            if (mapParam.containsKey(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.licensePDP)) {
                                try {
                                    boPlan.setLicensePdpSgsnBackup(Long.valueOf(mapParam.get(boPlan.getNodeSgsnBackup().getNodeCode() + "##" + Constants.paramCodeSgsn.licensePDP).getParamValue()));
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            sauBackupRnc = sauBackupRnc + boPlan.getSauRnc();
                            pdpBackupRnc = pdpBackupRnc + boPlan.getPdpRnc();
                            boPlan.setSauSgsnBackupAfter(boPlan.getSauSgsnBackup() + sauBackupRnc);
                            boPlan.setPdpSgsnBackupAfter(boPlan.getPdpSgsnBackup() + pdpBackupRnc);
                            if (!boPlan.getLicenseSauSgsnCurrent().equals(0L)) {
                                boPlan.setSaurSgsnCurrent(Double.valueOf(boPlan.getSauSgsnCurrent()) * 100 / boPlan.getLicenseSauSgsnCurrent());
                            } else {
                                boPlan.setSaurSgsnCurrent(0D);
                            }
                            if (!boPlan.getLicensePdpSgsnCurrent().equals(0L)) {
                                boPlan.setPdprSgsnCurrent(Double.valueOf(boPlan.getPdpSgsnCurrent()) * 100 / boPlan.getLicensePdpSgsnCurrent());
                            } else {
                                boPlan.setSaurSgsnCurrent(0D);
                            }
                            if (!boPlan.getLicenseSauSgsnBackup().equals(0L)) {
                                boPlan.setSaurSgsnBackup(Double.valueOf(boPlan.getSauSgsnBackup()) * 100 / boPlan.getLicenseSauSgsnBackup());
                                boPlan.setSaurSgsnBackupAfter(Double.valueOf(boPlan.getSauSgsnBackupAfter()) * 100 / boPlan.getLicenseSauSgsnBackup());
                            } else {
                                boPlan.setSaurSgsnBackup(0D);
                                boPlan.setSaurSgsnBackupAfter(0D);
                            }
                            if (!boPlan.getLicensePdpSgsnBackup().equals(0L)) {
                                boPlan.setPdprSgsnBackup(Double.valueOf(boPlan.getPdpSgsnBackup()) * 100 / boPlan.getLicensePdpSgsnBackup());
                                boPlan.setPdprSgsnBackupAfter(Double.valueOf(boPlan.getPdpSgsnBackupAfter()) * 100 / boPlan.getLicensePdpSgsnBackup());
                            } else {
                                boPlan.setPdprSgsnBackup(0D);
                                boPlan.setSaurSgsnBackupAfter(0D);
                            }
                            if (boPlan.getSaurSgsnBackupAfter() > 0 && boPlan.getSaurSgsnBackupAfter() < Constants.paramCodeCommon.paramCrossCheck) {
                                boPlan.setAutoCheck(1L);
                            } else {
                                boPlan.setAutoCheck(0L);
                            }
                            nodeSgsnBackupOld = boPlan.getNodeSgsnBackup().getNodeCode();
                        } catch (Exception ex) {
                            check = false;
                            logger.error(ex.getMessage(), ex);
                        }
                    }
                    new RiSgRcPlanServiceImpl().saveOrUpdate(listRescuePlan);
                } catch (Exception ex) {
                    check = false;
                    logger.error(ex.getMessage(), ex);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return check;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Export_Rescue">

    public File exportFile() throws Exception {

        String pathOut = CommonExport.getPathSaveFileExport(MessageUtil.getResourceBundleMessage("label.rescue.info.export.filename"));
        Workbook workbook = null;
        try {
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                    .getExternalContext().getContext();

            String pathTemplate = ctx.getRealPath("/")
                    + File.separator + "templates" + File.separator + "TEMPLATE_RESCUE_INFO_EXPORT.xlsx";

            InputStream fileTemplate = new FileInputStream(pathTemplate);
            workbook = WorkbookFactory.create(fileTemplate);
            Sheet worksheet = workbook.getSheetAt(0);
            Font font = workbook.createFont();
            font.setFontName("Times New Roman");
            CellStyle cellStyleLeft = workbook.createCellStyle();
            cellStyleLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);
            cellStyleLeft.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleLeft.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setFont(font);
            cellStyleLeft.setWrapText(false);
            //phai
            CellStyle cellStyleRight = workbook.createCellStyle();
            cellStyleRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            cellStyleRight.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setFont(font);
            cellStyleRight.setWrapText(false);
            //giua
            CellStyle cellStyleCenter = workbook.createCellStyle();
            cellStyleCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenter.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenter.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setFont(font);
            cellStyleCenter.setWrapText(false);

            CellStyle cellStyleCenterDate = workbook.createCellStyle();
            cellStyleCenterDate.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenterDate.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenterDate.setBorderLeft(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderBottom(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderRight(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderTop(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setFont(font);
            cellStyleCenterDate.setWrapText(false);
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("nodeSgsnCurrent.nodeCode", "ASC");
            orders.put("nodeSgsnBackup.nodeCode", "ASC");
            orders.put("nodeSgsnBackup.province.areaCode", "ASC");
            orders.put("nodeRnc.province.areaCode", "ASC");
            orders.put("nodeRnc.nodeCode", "ASC");

            Map<String, Object> filters = new HashMap<>();
            List<RiSgRcPlan> lstRescueInfo = new RiSgRcPlanServiceImpl().findList(filters, orders);
            int startRow = 8;
            int i = 0;
            Cell cell;
            Row row;
            row = worksheet.createRow(4);
            cell = row.createCell(6);
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");

            cell.setCellValue(MessageUtil.getResourceBundleMessage("report.dateTime") + dateFormat.format(new Date()));
            cell.setCellStyle(cellStyleCenterDate);
            for (RiSgRcPlan temp : lstRescueInfo) {
                row = worksheet.createRow(i + startRow);

                int currentColunmData = 0;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(i + 1);
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeRnc().getProvince().getProvinceCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeRnc().getNodeCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeRnc().getVendor().getVendorName());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauRnc());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpRnc());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeSgsnCurrent().getNodeCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeSgsnCurrent().getVendor().getVendorName());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauSgsnCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicenseSauSgsnCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSaurSgsnCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpSgsnCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicensePdpSgsnCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdprSgsnCurrent());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeSgsnBackup().getNodeCode());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeSgsnBackup().getVendor().getVendorName());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicenseSauSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSaurSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicensePdpSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdprSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSauSgsnBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicenseSauSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getSaurSgsnBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdpSgsnBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getLicensePdpSgsnBackup());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getPdprSgsnBackupAfter());
                cell.setCellStyle(cellStyleRight);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getAutoCheckName());
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(dateFormat.format(temp.getUpdateTime()));
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getBackOfficeEngineer());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getApproveManager());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;
                i++;
            }
            try {
                try (FileOutputStream fileOut = new FileOutputStream(pathOut)) {
                    workbook.write(fileOut);
                    fileOut.flush();
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return new File(pathOut);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Import_Rescue">
    public void onImportPlan(FileUploadEvent event, Node nodeRnc, Node nodeSgsnBackup, Node nodeSgsnCurrent, List<RiSgRcResultImport> lstRiSgRcImportResult, List<RiSgRcPlan> lstRescueImportSave, HashMap<String, Node> mapNode) {
        try {
            Importer<RiSgRcModelExcel> importer = new Importer<RiSgRcModelExcel>() {

                protected Class<RiSgRcModelExcel> getDomainClass() {
                    // TODO Auto-generated method stub
                    return RiSgRcModelExcel.class;
                }

                @Override
                protected Map<Integer, String> getIndexMapFieldClass() {
                    // TODO Auto-generated method stub
                    Map<Integer, String> model = new HashMap<Integer, String>();

                    model.put(1, "nodeCodeRnc");
                    model.put(2, "nodeCodeSgsnCurrent");
                    model.put(3, "nodeCodeSgsnBackup");
                    return model;
                }

                @Override
                protected String getDateFormat() {
                    // TODO Auto-generated method stub
                    return null;
                }
            };

            List<RiSgRcModelExcel> lstDataImport = importer.getDatas(event, 0, "2-");

            if (lstDataImport.isEmpty()) {
                MessageUtil.setErrorMessageFromRes("datatable.empty");
                return;
            } else {
                if (valFileImport(lstDataImport)) {

                    // Kiem tra neu cung node code thi thuc hien update du lieu
                    RiSgRcPlan riSgRcPlanImport;

                    Map<String, Object> filters = new HashMap<>();
                    List<RiSgRcPlan> listRescue;
                    for (RiSgRcModelExcel paramExcel : lstDataImport) {
                        try {
                            RiSgRcResultImport rescueInfoResultImport = new RiSgRcResultImport();
                            rescueInfoResultImport.setNodeRnc(paramExcel.getNodeCodeRnc().trim());
                            rescueInfoResultImport.setNodeSgsnActive(paramExcel.getNodeCodeSgsnCurrent().trim());
                            rescueInfoResultImport.setNodeSgsnBackup(paramExcel.getNodeCodeSgsnBackup().trim());
//                            if (mapNode.containsKey(paramExcel.getNodeCodeRnc().trim())
//                                    && mapNode.containsKey(paramExcel.getNodeCodeSgsnCurrent().trim())
//                                    && mapNode.containsKey(paramExcel.getNodeCodeSgsnBackup().trim())) {
                            nodeRnc = mapNode.get(paramExcel.getNodeCodeRnc().trim());
                            nodeSgsnBackup = mapNode.get(paramExcel.getNodeCodeSgsnBackup().trim());
                            nodeSgsnCurrent = mapNode.get(paramExcel.getNodeCodeSgsnCurrent().trim());

                            if ("".equals(validateDataImport(nodeRnc, nodeSgsnBackup, nodeSgsnCurrent))) {
                                if (nodeRnc != null) {
                                    filters.put("nodeRnc.nodeCode", nodeRnc.getNodeCode());
                                }

                                listRescue = new RiSgRcPlanServiceImpl().findList(filters);
                                if (listRescue.isEmpty()) {
                                    riSgRcPlanImport = new RiSgRcPlan();
                                } else {
                                    riSgRcPlanImport = listRescue.get(0);
                                }

                                riSgRcPlanImport.setNodeRnc(nodeRnc);
                                riSgRcPlanImport.setNodeSgsnBackup(nodeSgsnBackup);
                                riSgRcPlanImport.setNodeSgsnCurrent(nodeSgsnCurrent);
                                riSgRcPlanImport.setBackOfficeEngineer(SessionWrapper.getCurrentUsername());
                                riSgRcPlanImport.setAutoCheck(0L);
                                riSgRcPlanImport.setSauRnc(0L);
                                riSgRcPlanImport.setSauSgsnBackup(0L);
                                riSgRcPlanImport.setSauSgsnCurrent(0L);
                                riSgRcPlanImport.setSaurSgsnBackup(0D);
                                riSgRcPlanImport.setSaurSgsnCurrent(0D);
                                riSgRcPlanImport.setPdpRnc(0L);
                                riSgRcPlanImport.setPdpSgsnBackup(0L);
                                riSgRcPlanImport.setPdpSgsnCurrent(0L);
                                riSgRcPlanImport.setPdprSgsnBackup(0D);
                                riSgRcPlanImport.setPdprSgsnCurrent(0D);
                                riSgRcPlanImport.setLicensePdpSgsnBackup(0L);
                                riSgRcPlanImport.setLicensePdpSgsnCurrent(0L);
                                riSgRcPlanImport.setLicenseSauSgsnBackup(0L);
                                riSgRcPlanImport.setLicenseSauSgsnCurrent(0L);
                                riSgRcPlanImport.setSauSgsnBackupAfter(0L);
                                riSgRcPlanImport.setPdpSgsnBackupAfter(0L);
                                riSgRcPlanImport.setSaurSgsnBackupAfter(0D);
                                riSgRcPlanImport.setPdprSgsnBackupAfter(0D);
                                riSgRcPlanImport.setUpdateTime(new Date());

                                lstRescueImportSave.add(riSgRcPlanImport);
                                rescueInfoResultImport.setResultImport("OK");
                                rescueInfoResultImport.setDetailImport("OK");
                                lstRiSgRcImportResult.add(rescueInfoResultImport);

                            } else {
                                rescueInfoResultImport.setResultImport("NOK");
                                rescueInfoResultImport.setDetailImport(validateDataImport(nodeRnc, nodeSgsnBackup, nodeSgsnCurrent));
                                lstRiSgRcImportResult.add(rescueInfoResultImport);
                            }
                            CheckRncActiveSgsn(lstRescueImportSave);
                            new RiSgRcPlanServiceImpl().saveOrUpdate(lstRescueImportSave);
//                            }

                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                }
            }
//            RequestContext.getCurrentInstance().execute("PF('dlgUploadRescueInfo').hide()");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private String validateDataImport(Node nodeRnc, Node nodeSgsnBackup, Node nodeSgsnCurrent) {
        String result = "";
        try {
            // Kiem tra xem cac truong da duoc nhap day du du lieu chua
            if (nodeRnc == null) {
                result = result + "Chưa nhập node Rnc hoặc nhập chưa đúng\n";
            }
            if (nodeSgsnBackup == null) {
                result = result + "Chưa nhập node Sgsn active hoặc nhập chưa đúng\n";
            }
            if (nodeSgsnCurrent == null) {
                result = result + "Chưa nhập node Sgsn backup hoặc nhập chưa đúng\n";
            }

            // Kiem tra tinh logic cua du lieu
            if ("".equals(result)) {
                if (nodeSgsnCurrent != null && nodeSgsnBackup != null && nodeSgsnCurrent.getNodeCode().equals(nodeSgsnBackup.getNodeCode())) {
                    result = result + MessageUtil.getResourceBundleMessage("label.err.duplicate.sgsnCurrentBackup");
                }
//                filters.clear();
//                filters.put("rncName", nodeRnc.getNodeCode());
//                filters.put("sgsnName", nodeSgsnCurrent.getNodeCode());
//                List<RiSgRcParamSgsnRnc> lstSgsnRnc = new RiSgRcParamSgsnRncServiceImpl().findList(filters);
//                if (lstSgsnRnc.isEmpty()) {
//                    result = result + MessageUtil.getResourceBundleMessage("label.err.not.existed.rnc.sgsnCurrent");
//                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return result;
    }

    public boolean valFileImport(List<RiSgRcModelExcel> lstParamNodeVal) {
        boolean check = true;
        for (RiSgRcModelExcel param : lstParamNodeVal) {
            try {
                if (param.getNodeCodeRnc() == null
                        || param.getNodeCodeRnc().trim().isEmpty()
                        || param.getNodeCodeSgsnBackup() == null
                        || param.getNodeCodeSgsnBackup().trim().isEmpty()
                        || param.getNodeCodeSgsnCurrent() == null
                        || param.getNodeCodeSgsnCurrent().trim().isEmpty()) {
                    check = false;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        return check;
    }

    public File exportFileResultImport(List<RiSgRcResultImport> lstRiSgRcImportResult) throws Exception {

        String pathOut = CommonExport.getPathSaveFileExport(MessageUtil.getResourceBundleMessage("title.rescue.import.result"));
        Workbook workbook = null;
        try {
            ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                    .getExternalContext().getContext();

            String pathTemplate = ctx.getRealPath("/")
                    + File.separator + "templates" + File.separator + "Template_import_rescue_infomation_result_sgsn_rnc.xlsx";

            InputStream fileTemplate = new FileInputStream(pathTemplate);
            workbook = WorkbookFactory.create(fileTemplate);
            Sheet worksheet = workbook.getSheetAt(0);
            Font font = workbook.createFont();
            font.setFontName("Times New Roman");
            CellStyle cellStyleLeft = workbook.createCellStyle();
            cellStyleLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);
            cellStyleLeft.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleLeft.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleLeft.setFont(font);
            cellStyleLeft.setWrapText(false);
            //phai
            CellStyle cellStyleRight = workbook.createCellStyle();
            cellStyleRight.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            cellStyleRight.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleRight.setFont(font);
            cellStyleRight.setWrapText(false);
            //giua
            CellStyle cellStyleCenter = workbook.createCellStyle();
            cellStyleCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenter.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenter.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyleCenter.setFont(font);
            cellStyleCenter.setWrapText(false);

            CellStyle cellStyleCenterDate = workbook.createCellStyle();
            cellStyleCenterDate.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            cellStyleCenterDate.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
            cellStyleCenterDate.setBorderLeft(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderBottom(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderRight(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setBorderTop(HSSFCellStyle.NO_FILL);
            cellStyleCenterDate.setFont(font);
            cellStyleCenterDate.setWrapText(false);

            List<RiSgRcResultImport> lstRescueInfo = lstRiSgRcImportResult;
            int startRow = 8;
            int i = 0;
            Cell cell;
            Row row;
            row = worksheet.createRow(4);
            cell = row.createCell(1);
            SimpleDateFormat dateFormat = new SimpleDateFormat();
            dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");

            cell.setCellValue(MessageUtil.getResourceBundleMessage("report.dateTime") + dateFormat.format(new Date()));
            cell.setCellStyle(cellStyleCenterDate);
            for (RiSgRcResultImport temp : lstRescueInfo) {
                row = worksheet.createRow(i + startRow);

                int currentColunmData = 0;
                cell = row.createCell(currentColunmData);
                cell.setCellValue(i + 1);
                cell.setCellStyle(cellStyleCenter);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeRnc());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeSgsnActive());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getNodeSgsnBackup());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getResultImport());
                cell.setCellStyle(cellStyleLeft);
                currentColunmData++;

                cell = row.createCell(currentColunmData);
                cell.setCellValue(temp.getDetailImport());
                cell.setCellStyle(cellStyleLeft);
//                currentColunmData++;
                i++;

            }
            try {
                FileOutputStream fileOut = new FileOutputStream(pathOut);
                workbook.write(fileOut);
                fileOut.flush();
                fileOut.close();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return new File(pathOut);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Insert_Update_Rescue">
    public void savePlan(Node nodeRnc, Node nodeSgsnBackup, Node nodeSgsnCurrent, boolean isEdit, RiSgRcPlan riSgRcPlan) {
        try {
            if (validateData(nodeRnc, nodeSgsnBackup, nodeSgsnCurrent, isEdit, riSgRcPlan)) {

                RiSgRcPlan bo = new RiSgRcPlan();
                if (isEdit) {
                    bo.setId(riSgRcPlan.getId());
                }
                bo.setNodeRnc(nodeRnc);
                bo.setNodeSgsnBackup(nodeSgsnBackup);
                bo.setNodeSgsnCurrent(nodeSgsnCurrent);
                bo.setBackOfficeEngineer(SessionWrapper.getCurrentUsername());
//                if (!isEdit) {
                bo.setAutoCheck(0L);
                bo.setSauRnc(0L);
                bo.setSauSgsnBackup(0L);
                bo.setSauSgsnCurrent(0L);
                bo.setSaurSgsnBackup(0D);
                bo.setSaurSgsnCurrent(0D);
                bo.setPdpRnc(0L);
                bo.setPdpSgsnBackup(0L);
                bo.setPdpSgsnCurrent(0L);
                bo.setPdprSgsnBackup(0D);
                bo.setPdprSgsnCurrent(0D);
                bo.setLicensePdpSgsnBackup(0L);
                bo.setLicensePdpSgsnCurrent(0L);
                bo.setLicenseSauSgsnBackup(0L);
                bo.setLicenseSauSgsnCurrent(0L);
                bo.setSauSgsnBackupAfter(0L);
                bo.setPdpSgsnBackupAfter(0L);
                bo.setSaurSgsnBackupAfter(0D);
                bo.setPdprSgsnBackupAfter(0D);
                bo.setIsCreateMop(0L);
//                }
                bo.setUpdateTime(new Date());
                List<RiSgRcPlan> plans = new ArrayList<>();
                plans.add(bo);
                CheckRncActiveSgsn(plans);

                new RiSgRcPlanServiceImpl().saveOrUpdate(bo);
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            }
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }
    }

    private boolean validateData(Node nodeRnc, Node nodeSgsnBackup, Node nodeSgsnCurrent, boolean isEdit, RiSgRcPlan riSgRcPlan) {
        try {
            // Kiem tra xem cac truong da duoc nhap day du du lieu chua
            if (nodeRnc == null
                    || nodeSgsnBackup == null
                    || nodeSgsnCurrent == null) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                return false;
            }

            // Kiem tra tinh logic cua du lieu
            Map<String, Object> filters = new HashMap<>();
            filters.put("nodeRnc.nodeCode", nodeRnc.getNodeCode());
            if (isEdit) {
                filters.put("id", riSgRcPlan.getId());
            }
            List listRescue = new RiSgRcPlanServiceImpl().findList(filters);
            if ((isEdit && listRescue.size() > 1) || (!isEdit && listRescue.size() > 0)) {
                MessageUtil.setErrorMessageFromRes("label.err.existed.nodecode");
                return false;
            }
            if (nodeSgsnCurrent.getNodeCode().equals(nodeSgsnBackup.getNodeCode())) {
                MessageUtil.setErrorMessageFromRes("label.err.duplicate.sgsnCurrentBackup");
                return false;
            }
//                filters.clear();
//                filters.put("nodeCode", nodeRnc.getNodeCode());
//                filters.put("sgsnName", nodeSgsnCurrent.getNodeCode());
//                switch(nodeRnc.getVendor().getVendorName().toUpperCase()) {
//                    case Constants.vendorType.HUAWEI:
//                        List<RiSgRcParamRncHuawei> lstSgsnRnc = new RiSgRcParamRncHuaweiServiceImpl().findList(filters);
//                        if (lstSgsnRnc.isEmpty()) {
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
//                            val = false;
//                        }
//                        break;
//                    case Constants.vendorType.NOKIA:
//                        List<RiSgRcParamRncNokia> lstSgsnRnc1 = new RiSgRcParamRncNokiaServiceImpl().findList(filters);
//                        if (lstSgsnRnc1.isEmpty()) {
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
//                            val = false;
//                        }
//                        break;
//                    case Constants.vendorType.ERICSSON:
//                        List<RiSgRcParamRncEricsson> lstSgsnRnc2 = new RiSgRcParamRncEricssonServiceImpl().findList(filters);
//                        if (lstSgsnRnc2.isEmpty()) {
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
//                            val = false;
//                        }
//                        break;
//                }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return true;
    }

    public void CheckRncActiveSgsn(List<RiSgRcPlan> plans) {
        try {
            if (plans == null) {
                plans = new RiSgRcPlanServiceImpl().findList();
            }
            HashMap<String, Object> filters = new HashMap<>();
            for (RiSgRcPlan plan : plans) {

                switch (plan.getNodeRnc().getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.HUAWEI:
                        filters.clear();
                        filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
                        filters.put("sgsnName", plan.getNodeSgsnCurrent().getNodeCode());
                        List<RiSgRcParamRncHuawei> lstSgsnRnc = new RiSgRcParamRncHuaweiServiceImpl().findList(filters);
                        if (lstSgsnRnc.isEmpty()) {
                            plan.setCheckActive(0L);
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");

                        } else {
                            plan.setCheckActive(1L);
//                            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').show()");
                        }
                        break;
                    case Constants.vendorType.NOKIA:
                        filters.clear();
                        String SgsnNokia = new StringBuilder(plan.getNodeSgsnCurrent().getNodeCode().trim()).replace(1, 2, "").toString();
                        filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
                        filters.put("sgsnName", SgsnNokia);

                        List<RiSgRcParamRncNokia> lstSgsnRnc1 = new RiSgRcParamRncNokiaServiceImpl().findList(filters);
                        if (lstSgsnRnc1.isEmpty()) {
                            plan.setCheckActive(0L);
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
                        } else {
                            plan.setCheckActive(1L);
//                            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').show()");
                        }
                        break;
                    case Constants.vendorType.ERICSSON:
                        filters.clear();
                        filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
                        filters.put("sgsnName", plan.getNodeSgsnCurrent().getNodeCode());
                        List<RiSgRcParamRncEricsson> lstSgsnRnc2 = new RiSgRcParamRncEricssonServiceImpl().findList(filters);
                        if (lstSgsnRnc2.isEmpty()) {
                            plan.setCheckActive(0L);
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
                        } else {
                            plan.setCheckActive(1L);
//                            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').show()");
                        }
                        break;
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete_Rescue">
    //</editor-fold>

}
