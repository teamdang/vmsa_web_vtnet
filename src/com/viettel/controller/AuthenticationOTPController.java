package com.viettel.controller;

import com.viettel.util.AuthOTP;
import com.viettel.util.LanguageBean;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionUtil;
import com.viettel.vsa.token.UserToken;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean
@RequestScoped
public class AuthenticationOTPController {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationOTPController.class);
    public static final String SUMMARY = "Xác thực OTP";
    private String otpNumber;
    private String message;
    private Integer[] typeSends;

    @PostConstruct
    public void onStart() {
        this.otpNumber = null;
        checkLogin();
        this.typeSends = new Integer[1];
//        this.typeSends[0] = 1;
        this.typeSends[0] = 2;
    }

    public void reCreateOtp() {
        checkLogin();

        FacesMessage msg = null;
        LanguageBean language = (LanguageBean) FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{language}", LanguageBean.class);
        String timezone = language.getTimeZoneForCalendar();
        List<Integer> typeSendLists = Arrays.asList(this.typeSends);
        if (new AuthOTP().createOTP(timezone, typeSendLists)) {
            UserToken user = (UserToken) com.viettel.util.SessionUtil.getCurrentSession().getAttribute("vsaUserToken");
            String cellphone = user.getCellphone();
            String phone = cellphone.substring(0, 3) + "****" + cellphone.substring(cellphone.length() - 3);
            String email = user.getEmail();
            String obj = "";
            if ((typeSendLists.contains(0)) || (typeSendLists.contains(1))) {
                obj = phone;
            }
            if (typeSendLists.contains(2)) {
                obj = obj + " & " + email;
            }
            obj = obj.replaceAll("^\\s&\\s", "");
            if (!obj.isEmpty()) {
                msg = new FacesMessage(FacesMessage.SEVERITY_INFO, SUMMARY, MessageUtil.getResourceBundleMessage("info.otp.msg.create.otp", new Object[]{obj}));
            }
        } else {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, SUMMARY, MessageUtil.getResourceBundleMessage("error.otp.create"));
        }
        if (msg != null) {
            FacesContext.getCurrentInstance().addMessage("mainMessageAuth", msg);
        }
    }

    public void checkLogin() {
        try {
            if (SessionUtil.getCurrentSession().getAttribute("vsaUserToken") == null) {
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                externalContext.redirect(externalContext.getRequestContextPath() + "/home");
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public void autoAuth() {
        if ((this.otpNumber != null) && (this.otpNumber.length() == 6)) {
            authentication();
        }
    }

    public void authentication() {
        Object[] val = new AuthOTP().validateOTP(this.otpNumber);
        if (!(Boolean) val[0]) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, SUMMARY, (String) val[1]);
            FacesContext.getCurrentInstance().addMessage("mainMessageAuth", msg);
        } else {
            try {
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                externalContext.redirect(externalContext.getRequestContextPath() + "/home");
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    public String getOtpNumber() {
        return this.otpNumber;
    }

    public void setOtpNumber(String otpNumber) {
        this.otpNumber = otpNumber;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer[] getTypeSends() {
        return this.typeSends;
    }

    public void setTypeSends(Integer[] typeSends) {
        this.typeSends = typeSends;
    }
}
