package com.viettel.controller;

import static com.viettel.util.Config.NODE_TYPE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.Customer;
import com.viettel.model.CustomerNode;
import com.viettel.model.FlowRunAction;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.model.ParamValue;
import com.viettel.model.ServiceTemplate;
import com.viettel.object.MessageException;
import com.viettel.persistence.CustomerNodeServiceImpl;
import com.viettel.persistence.CustomerServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.ServiceTemplateServiceImpl;
import com.viettel.persistence.common.ConditionQuery;
import com.viettel.persistence.common.OrderBy;
import com.viettel.util.Config;
import com.viettel.util.Config.ACTION;
import com.viettel.util.Config.CONNECT_TYPE;
import com.viettel.util.Config.SERVICE;
import com.viettel.util.MessageUtil;
import com.viettel.util.RelationNodeUtil;
import com.viettel.util.SessionUtil;

/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Oct 26, 2016
 * @version 1.0 
 */
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class CustomerController implements Serializable {
	
	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	@ManagedProperty(value="#{generateFlowRunController}")
	private GenerateFlowRunController generateFlowRunController;
	
	private LazyDataModel<Customer> lazyCustomer;
	
	@ManagedProperty("#{customerService}")
	private CustomerServiceImpl customerService;
	
	private Customer currCustomer;
	private Customer searchCustomer;

	private CustomerNode;
	
	private boolean isCreateCustomer;
	private boolean isCreateEndPoint;
	private boolean isEditCustomer;

	private boolean isEditEndPoint;
	@PostConstruct
	public void onStart(){
		Object filters = null;
		Object orders=null;
		currCustomer = new Customer();
		clearSearch();
		lazyCustomer = new LazyDataModelBaseNew<>(customerService, filters, orders);
	}
	@SuppressWarnings("unchecked")
	public void onSearch(){
		Map<String,Object> filters = new HashMap<String, Object>();
		Map<String,String> orders = new LinkedHashMap<>();
		if(searchCustomer.getVrfName()!=null && !searchCustomer.getVrfName().isEmpty())
			filters.put("vrfName", searchCustomer.getVrfName());
		if(searchCustomer.getServiceCode()!=null && searchCustomer.getServiceCode()!=0)
			filters.put("serviceCode", searchCustomer.getServiceCode());
		
		
		if(searchCustomer.getCustomerNodes().get(0).getCustomerName()!=null 
					&& !searchCustomer.getCustomerNodes().get(0).getCustomerName().isEmpty()) {
			Map<String,Object> value = new HashMap<>();
			value.put("customerName", searchCustomer.getCustomerNodes().get(0).getCustomerName());
			value.put("nodeCode", "");
			value.put("address","");
			filters.put("CustomerNode_FILTER", value);
		}
		if(searchCustomer.getCustomerNodes().get(0).getNode().getNodeCode()!=null 
				&& !searchCustomer.getCustomerNodes().get(0).getNode().getNodeCode().isEmpty()) {
			Map<String,Object> value = (Map<String, Object>) filters.get("CustomerNode_FILTER");
			if(value==null){
				value = new HashMap<>();
				value.put("customerName", "");
				value.put("address","");
			}
			value.put("nodeCode", searchCustomer.getCustomerNodes().get(0).getNode().getNodeCode());
			filters.put("CustomerNode_FILTER", value);
		}
		if(searchCustomer.getAddress()!=null && !searchCustomer.getAddress().isEmpty()){
			Map<String,Object> value = (Map<String, Object>) filters.get("CustomerNode_FILTER");
			if(value==null){
				value = new HashMap<>();
				value.put("customerName", "");
				value.put("nodeCode", "");
			}
			value.put("address", searchCustomer.getAddress());
			filters.put("CustomerNode_FILTER", value);
		}
		lazyCustomer = new LazyDataModelBaseNew<>(customerService, filters, orders);
		DataTable dataTable = (DataTable)  FacesContext.getCurrentInstance().getViewRoot().findComponent("form:tableCusId");
		dataTable.setFirst(0);
		dataTable.setExpandedRow(true);
	}
	public void clearSearch(){
		searchCustomer = new Customer();
		searchCustomer.getCustomerNodes().add(new CustomerNode(new Node()));
	}
	public void deployService(){
		if(isEditEndPoint)
			deployService(ACTION.UPDATE.value);
		else
			deployService(ACTION.NEW.value);
	}
	
	public void deployService(long action){
		if(createFlowRunAction(action)){
			Session session = null;
			Transaction tx = null;
			try {
				Object[] objs = new CustomerServiceImpl().openTransaction();
				session = (Session) objs[0];
				tx = (Transaction) objs[1];
				if(saveCustomer(session, tx, false)){
					if(saveCustomerNode(session, tx, true)){
						MessageUtil.setInfoMessageFromRes("info.deploy.service.success");
						return ;
					}
				}
			} catch (Exception e) {
				if(tx!=null && tx.getStatus()!=TransactionStatus.ROLLED_BACK)
					tx.rollback();
				LOGGER.error(e.getMessage(), e);
			} finally{
				if(session!=null && session.isOpen())
					session.close();
			}
		}
		MessageUtil.setErrorMessageFromRes("error.deploy.service.unsuccess");
	}
	public void reDeployService(){
		deployService(ACTION.UPDATE.value);
	}
	public void destroyService(){
		if(createFlowRunAction(ACTION.DELETE.value))
			if(deleteCustomerNode()){
				MessageUtil.setInfoMessageFromRes("info.destroy.service.success");
				return;
			}
		MessageUtil.setErrorMessageFromRes("error.destroy.service.unsuccess");
	}
	public void destroyAllService(){
		for (CustomerNode customerNode : currCustomer.getCustomerNodes()) {
			currCustomerNode = customerNode;
			createFlowRunAction(ACTION.DELETE.value);
		}
		for (CustomerNode customerNode : currCustomer.getCustomerNodes()) {
			currCustomerNode = customerNode;
			deleteCustomerNode();
		}
		deleteCustomer();
	}
	
	public void preDeleteCustomer(Customer customer){
		currCustomer = customer;
	}
	public void preDeleteCustomerNode(CustomerNode customerNode){
		currCustomerNode = customerNode;
	}
	public boolean deleteCustomer(){
		Session _session = null;
		Transaction _tx = null;
                boolean flag = false;
		try {
			Object[] tmps = new CustomerServiceImpl().openTransaction();
			_session = (Session) tmps[0];
			_tx = (Transaction) tmps[1];
			new CustomerNodeServiceImpl().execteBulk2("delete from CustomerNode where customer.id = ?", _session, _tx, false, currCustomer.getId());
			new CustomerServiceImpl().delete(currCustomer, _session, _tx, true);
			flag = true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			if(_tx!=null && !TransactionStatus.ROLLED_BACK.equals(_tx.getStatus()))
				_tx.rollback();
				
		}finally{
			if(_session!=null && _session.isOpen()){
				_session.close();
			}
		}
		return flag;
	}
	public boolean deleteCustomerNode(){
		try {
			new CustomerNodeServiceImpl().delete(currCustomerNode);
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
		}
		return false;
	}
	private boolean createFlowRunAction(Long action){
		try {
			Map<String, Object> filter = new HashMap<String, Object>();
			Node nodeSrt = currCustomerNode.getNode();
			filter.put("id.vendorId", nodeSrt.getVendor().getVendorId());
			filter.put("id.versionId", nodeSrt.getVersion().getVersionId());
			filter.put("id.serviceCode", currCustomer.getServiceCode());
			filter.put("id.action", action);
			List<ServiceTemplate> serviceTemplates = new ServiceTemplateServiceImpl().findListExac(filter, null);
			if(serviceTemplates.size()==1){
				FlowTemplates flowTemplates = serviceTemplates.get(0).getFlowTemplates();
				if(flowTemplates.getStatus()!=9)
					throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
				generateFlowRunController = new GenerateFlowRunController();
				FlowRunAction flowRunAction = new FlowRunAction();
				flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
				flowRunAction.setFlowRunName((currCustomer.getVrfName()+" - "+currCustomerNode.getCustomerName()));
				while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())){
					flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
				}
				flowRunAction.setTimeRun(new Date());
				flowRunAction.setFlowTemplates(flowTemplates);
				flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());
				generateFlowRunController.setFlowRunAction(flowRunAction);
				generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
				List<Node> nodes = autoLoadNode(nodeSrt.getNodeCode());
				Map<String, String> mapParamValues = autoLoadParams(currCustomer.getServiceCode(), nodes,currCustomerNode);
				generateFlowRunController.setNodes(nodes);
				generateFlowRunController.loadGroupAction(0l);
				for (Node node : nodes) {
					generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT,node);
					List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT,node);
					for (ParamValue paramValue : paramValues) {
						if(paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut())
							continue;
						Object value = null;
						try {
							if(mapParamValues!=null)
								value = mapParamValues.get((paramValue.getParamCode()));
						} catch (Exception ex) {
							LOGGER.error(ex.getMessage(), ex);
						}
						if(value!=null){
							paramValue.setParamValue(value.toString().substring(0, Math.min(250, value.toString().length())));
						}
					}
				}
				
				return generateFlowRunController.saveDT();
			}else{
				throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.found"));
			}
			
		} catch (Exception e) {
			if(e instanceof MessageException)
				MessageUtil.setErrorMessage(e.getMessage());
			else
				LOGGER.error(e.getMessage(), e);
		}
		return false;
	}
	private List<Node> autoLoadNode(String srtNodecode) throws Exception {
		RelationNodeUtil relationNodeUtil = new RelationNodeUtil();
		List<Node> ringSrt = relationNodeUtil.getRingSrt(srtNodecode);
		if(ringSrt==null)
			throw new MessageException(relationNodeUtil.getMessageInfo());
		return ringSrt;
	}
	private Map<String, String> autoLoadParams(Long serviceCode, List<Node> nodeinRings, CustomerNode customerNode ) throws Exception{
		Map<String, String> mapParamValues = new HashMap<String, String>();
		RelationNodeUtil relationNodeUtil = new RelationNodeUtil();
		try {
			try {
				
				if(nodeinRings!=null)
					if(serviceCode==SERVICE.L3VPN.value)
						mapParamValues = relationNodeUtil.getParamL3(nodeinRings, customerNode);
					else
						mapParamValues = relationNodeUtil.getParamL2(nodeinRings, customerNode);
				
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		if(relationNodeUtil.getMessageInfo()!=null)
			throw new Exception(relationNodeUtil.getMessageInfo());
		return mapParamValues;
	}
	public boolean saveCustomer(Session session, Transaction tx, boolean isCommit) {
		try {
			currCustomer.setCreateBy(SessionUtil.getCurrentUsername());
			currCustomer.setCreateDate(new Date());
			new CustomerServiceImpl().saveOrUpdate(currCustomer, session, tx, isCommit);
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return false;
	}
	public boolean saveCustomerNode(Session session, Transaction tx, boolean isCommit){
		try {
			currCustomerNode.setCustomer(currCustomer);
			currCustomerNode.setNodeCode(currCustomerNode.getNode().getNodeCode());
			
			if(currCustomerNode.getNode().getNodeType().getTypeId()==NODE_TYPE.SRT.value)
				currCustomerNode.setConnectType(CONNECT_TYPE.DIRECT.value);
			else
				currCustomerNode.setConnectType(CONNECT_TYPE.L2_SW.value);
			currCustomerNode.setCreateBy(SessionUtil.getCurrentUsername());
			currCustomerNode.setCreateDate(new Date());
			new CustomerNodeServiceImpl().saveOrUpdate(currCustomerNode, session, tx, isCommit);
			return true;
		} catch (SysException | AppException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return false;
	}
	public void preAddCustomer(){
		isEditEndPoint=false;
		isCreateCustomer=true;
		isCreateEndPoint = true;
		isEditCustomer = false;
		currCustomer = new Customer();
		currCustomer.setServiceCode(3L);
		currCustomerNode = new CustomerNode();
		currCustomer.getCustomerNodes().clear();
		currCustomer.getCustomerNodes().add(currCustomerNode);
	}
	public void preEditCustomer(Customer customer){
		isEditEndPoint=false;
		isEditCustomer = true;
		isCreateCustomer = true;
		isCreateEndPoint = false;
		currCustomer = customer;
	}
	
	public void preAddEndPoint(Customer customer){
		isEditCustomer = false;
		isEditEndPoint=false;
		isCreateEndPoint = true;
		isCreateCustomer = false;
		currCustomer = customer;
		currCustomerNode = new CustomerNode();
		currCustomer.getCustomerNodes().clear();
		currCustomer.getCustomerNodes().add(currCustomerNode);
	}
	public void preEditEndPoint(Customer customer,CustomerNode customerNode){
		isEditEndPoint=true;
		isCreateEndPoint = true;
		isCreateCustomer = false;
		isEditCustomer=false;
		currCustomer = customer;
		currCustomerNode = customerNode;
		
	}
	public List<Node> completeEndPoint(String node){
		OrderBy orderBy = new OrderBy();
		orderBy.add(Order.asc("nodeCode"));
		ConditionQuery query = new ConditionQuery();
		List<Criterion> predicates = new ArrayList<Criterion>();
		if(node!=null && !node.isEmpty()){
			predicates.add(Restrictions.ilike("nodeCode", node, MatchMode.ANYWHERE));
		}
		query.add(Restrictions.or(predicates.toArray(new Criterion[predicates.size()])));
		
		List<Node> findList = new NodeServiceImpl().findList(query, orderBy, 1, 20);
		return findList;
	}
	public GenerateFlowRunController getGenerateFlowRunController() {
		return generateFlowRunController;
	}
	public void setGenerateFlowRunController(GenerateFlowRunController generateFlowRunController) {
		this.generateFlowRunController = generateFlowRunController;
	}
	public LazyDataModel<Customer> getLazyCustomer() {
		return lazyCustomer;
	}
	public void setLazyCustomer(LazyDataModel<Customer> lazyCustomer) {
		this.lazyCustomer = lazyCustomer;
	}


	public CustomerServiceImpl getCustomerService() {
		return customerService;
	}


	public void setCustomerService(CustomerServiceImpl customerService) {
		this.customerService = customerService;
	}

	public Customer getCurrCustomer() {
		return currCustomer;
	}

	public void setCurrCustomer(Customer currCustomer) {
		this.currCustomer = currCustomer;
	}

	public CustomerNode getCurrCustomerNode() {
		return currCustomerNode;
	}

	public void setCurrCustomerNode(CustomerNode crrCustomerNode) {
		this.currCustomerNode = crrCustomerNode;
	}

	public boolean isCreateEndPoint() {
		return isCreateEndPoint;
	}

	public void setCreateEndPoint(boolean isCreateEndPoint) {
		this.isCreateEndPoint = isCreateEndPoint;
	}

	public boolean isCreateCustomer() {
		return isCreateCustomer;
	}

	public void setCreateCustomer(boolean isCreateNewCustomer) {
		this.isCreateCustomer = isCreateNewCustomer;
	}

	public boolean isEditCustomer() {
		return isEditCustomer;
	}

	public void setEditCustomer(boolean isEditCustomer) {
		this.isEditCustomer = isEditCustomer;
	}
	public Customer getSearchCustomer() {
		return searchCustomer;
	}
	public void setSearchCustomer(Customer searchCustomer) {
		this.searchCustomer = searchCustomer;
	}
	public boolean isEditEndPoint() {
		return isEditEndPoint;
	}
	public void setEditEndPoint(boolean isEditEndPoint) {
		this.isEditEndPoint = isEditEndPoint;
	}
	
	
	
}
