package com.viettel.controller;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.ArrayUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.FlowTemplates;
import com.viettel.model.RiIncidentSolution;
import com.viettel.model.Node;
import com.viettel.model.NodeType;
import com.viettel.model.ParamValue;
import com.viettel.model.RiMsRcParam;
import com.viettel.model.RiMsRcParamRncEricsson;
import com.viettel.model.RiMsRcParamRncHuawei;
import com.viettel.model.RiMsRcParamRncNokia;
import com.viettel.model.RiMsRcPlan;
import com.viettel.model.RiSgRcParam;
import com.viettel.model.RiSgRcParamLog;
import com.viettel.model.RiSgRcParamLogCommand;
import com.viettel.model.RiSgRcParamRncEricsson;
import com.viettel.model.RiSgRcParamRncHuawei;
import com.viettel.model.RiSgRcParamRncNokia;
import com.viettel.model.RiSgRcPlan;
import com.viettel.model.Vendor;
import com.viettel.model.Version;
import com.viettel.object.AccountObj;
import com.viettel.object.RiSgRcResult;
import com.viettel.object.MessageException;
import com.viettel.object.MessageObjectRescue;
import com.viettel.object.RiMsRcResult;
import com.viettel.object.RiMsRcResultImport;
import com.viettel.object.RiSgRcResultImport;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.RiIncidentSolutionServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.NodeTypeServiceImpl;
import com.viettel.persistence.RiMsRcParamRncEricssonServiceImpl;
import com.viettel.persistence.RiMsRcParamRncHuaweiServiceImpl;
import com.viettel.persistence.RiMsRcParamRncNokiaServiceImpl;
import com.viettel.persistence.RiMsRcPlanServiceImpl;
import com.viettel.persistence.RiSgRcParamLogCommandServiceImpl;
import com.viettel.persistence.RiSgRcParamLogServiceImpl;
import com.viettel.persistence.RiSgRcParamRncEricssonServiceImpl;
import com.viettel.persistence.RiSgRcParamRncHuaweiServiceImpl;
import com.viettel.persistence.RiSgRcParamRncNokiaServiceImpl;
import com.viettel.persistence.RiSgRcPlanServiceImpl;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.persistence.VersionServiceImpl;
import com.viettel.util.Constants;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionWrapper;
import com.viettel.util.SocketClient;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Date;
import java.util.TreeSet;
import javax.faces.context.FacesContext;
import org.apache.commons.codec.binary.Base64;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;

@ViewScoped
@ManagedBean
public class RescueInfomationController implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Param">
    //<editor-fold defaultstate="collapsed" desc="Param_Main">
    protected static final Logger logger = LoggerFactory.getLogger(RescueInfomationController.class);

    public static final Long ROOT_TREE_ID = 1L;
    public static final Integer NODE_EXPENDED = 1;
    public static final Integer NODE_NOT_EXPEND = 0;
    public static final String PARENT_NODE = "parent";
    public static final String CHILD_NODE = "child";
    public static final String IMPACT_LABEL_NODE = "impact_label";
    public static final String ROLLBACK_LABEL_NODE = "rollback_label";
    public static final Integer MAX_LENGTH_SOLUTION_NAME = 500;

    @ManagedProperty(value = "#{riIncidentSolutionService}")
    private RiIncidentSolutionServiceImpl incidentSolutionService;
    @ManagedProperty("#{nodeTypeService}")
    private NodeTypeServiceImpl nodeTypeServiceImpl;

    private TreeNode rootNode;
    private TreeNode selectedNode;
    private Long isShowIncidentSolution = 0L;
    private RiIncidentSolution insertIncidentSolution;
    private boolean isEdit = false;
    private RiIncidentSolution selectedIncidentSolution;
    private NodeType selectedNodeType;
    private String userName;
    private StreamedContent file;
    private String account;
    private String password;
    private boolean oneAccount = true;
    private boolean useAccountSystem = true;
    private Map<String, AccountObj> mapAccount;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Param_Sgsn_Rnc">
    private LazyDataModel<RiSgRcPlan> lazyRiSgRcPlan;
    @ManagedProperty("#{riSgRcPlanService}")
    private RiSgRcPlanServiceImpl riSgRcPlanService;
    private Node nodeRnc;
    private Node nodeSgsnCurrent;
    private Node nodeSgsnBackup;
    private List<Node> listNodeSgsnDown;
    private List<Node> nodeSgsnDown;
    private List<RiSgRcPlan> selectedRiSgRcPlan;
    private RiSgRcPlan riSgRcPlan;
    private boolean isRiSgRcImport = false;
    List<RiSgRcResultImport> lstRiSgRcImportResult = new ArrayList<>();
    private Long riSgRcMopType = 0L;
    private HashMap<String, List<RiSgRcParamLogCommand>> mapLogCommand;
    private List<ParamValue> listParamValue;
    private RiSgRcParamLogCommand selectParamLogCommand;
    private List<Vendor> vendors;
    private List<Version> versions;
    private List<NodeType> nodeTypes;
    private List<Node> nodes;
    private Node nodeObj;
    private Vendor vendorObj;
    private Version versionObj;
    private NodeType nodeTypeObj;
    private LazyDataModel<RiSgRcParamLog> lazyModelParamLog;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Param_Sgsn_Msc">
    //</editor-fold>
    private LazyDataModel<RiMsRcPlan> lazyRiMsRcPlan;
    @ManagedProperty("#{riMsRcPlanService}")
    private RiMsRcPlanServiceImpl riMsRcPlanService;
    private Node nodeMscCurrent;
    private Node nodeMscBackup;
    private List<Node> listNodeMscDown;
    private List<Node> nodeMscDown;
    private List<RiMsRcPlan> selectedRiMsRcPlan;
    private RiMsRcPlan riMsRcPlan;
    private boolean isRiMsRcImport = false;
    List<RiMsRcResultImport> lstRiMsRcImportResult = new ArrayList<>();
    private Long riMsRcMopType = 0L;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get&Set">
    public RiIncidentSolutionServiceImpl getIncidentSolutionService() {
        return incidentSolutionService;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public List<ParamValue> getListParamValue() {
        return listParamValue;
    }

    public void setListParamValue(List<ParamValue> listParamValue) {
        this.listParamValue = listParamValue;
    }

    public LazyDataModel<RiSgRcParamLog> getLazyModelParamLog() {
        return lazyModelParamLog;
    }

    public void setLazyModelParamLog(LazyDataModel<RiSgRcParamLog> lazyModelParamLog) {
        this.lazyModelParamLog = lazyModelParamLog;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    public List<NodeType> getNodeTypes() {
        return nodeTypes;
    }

    public void setNodeTypes(List<NodeType> nodeTypes) {
        this.nodeTypes = nodeTypes;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public Node getNodeObj() {
        return nodeObj;
    }

    public void setNodeObj(Node nodeObj) {
        this.nodeObj = nodeObj;
    }

    public Vendor getVendorObj() {
        return vendorObj;
    }

    public void setVendorObj(Vendor vendorObj) {
        this.vendorObj = vendorObj;
    }

    public Version getVersionObj() {
        return versionObj;
    }

    public void setVersionObj(Version versionObj) {
        this.versionObj = versionObj;
    }

    public NodeType getNodeTypeObj() {
        return nodeTypeObj;
    }

    public void setNodeTypeObj(NodeType nodeTypeObj) {
        this.nodeTypeObj = nodeTypeObj;
    }

    public void setIncidentSolutionService(RiIncidentSolutionServiceImpl incidentSolutionService) {
        this.incidentSolutionService = incidentSolutionService;
    }

    public HashMap<String, List<RiSgRcParamLogCommand>> getMapLogCommand() {
        return mapLogCommand;
    }

    public RiSgRcParamLogCommand getSelectParamLogCommand() {
        return selectParamLogCommand;
    }

    public void setSelectParamLogCommand(RiSgRcParamLogCommand selectParamLogCommand) {
        this.selectParamLogCommand = selectParamLogCommand;
    }

    public void setMapLogCommand(HashMap<String, List<RiSgRcParamLogCommand>> mapLogCommand) {
        this.mapLogCommand = mapLogCommand;
    }

    public Long getRiSgRcMopType() {
        return riSgRcMopType;
    }

    public void setRiSgRcMopType(Long riSgRcMopType) {
        this.riSgRcMopType = riSgRcMopType;
    }

    public Long getRiMsRcMopType() {
        return riMsRcMopType;
    }

    public void setRiMsRcMopType(Long riMsRcMopType) {
        this.riMsRcMopType = riMsRcMopType;
    }

    public boolean isUseAccountSystem() {
        return useAccountSystem;
    }

    public void setUseAccountSystem(boolean useAccountSystem) {
        this.useAccountSystem = useAccountSystem;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isOneAccount() {
        return oneAccount;
    }

    public void setOneAccount(boolean oneAccount) {
        this.oneAccount = oneAccount;
    }

    public Map<String, AccountObj> getMapAccount() {
        return mapAccount;
    }

    public void setMapAccount(Map<String, AccountObj> mapAccount) {
        this.mapAccount = mapAccount;
    }

    public NodeTypeServiceImpl getNodeTypeServiceImpl() {
        return nodeTypeServiceImpl;
    }

    public void setNodeTypeServiceImpl(NodeTypeServiceImpl nodeTypeServiceImpl) {
        this.nodeTypeServiceImpl = nodeTypeServiceImpl;
    }

    public TreeNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(TreeNode rootNode) {
        this.rootNode = rootNode;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public Long getIsShowIncidentSolution() {
        return isShowIncidentSolution;
    }

    public void setIsShowIncidentSolution(Long isShowIncidentSolution) {
        this.isShowIncidentSolution = isShowIncidentSolution;
    }

    public RiIncidentSolution getInsertIncidentSolution() {
        return insertIncidentSolution;
    }

    public void setInsertIncidentSolution(RiIncidentSolution insertIncidentSolution) {
        this.insertIncidentSolution = insertIncidentSolution;
    }

    public boolean isIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }

    public RiIncidentSolution getSelectedIncidentSolution() {
        return selectedIncidentSolution;
    }

    public void setSelectedIncidentSolution(RiIncidentSolution selectedIncidentSolution) {
        this.selectedIncidentSolution = selectedIncidentSolution;
    }

    public NodeType getSelectedNodeType() {
        return selectedNodeType;
    }

    public void setSelectedNodeType(NodeType selectedNodeType) {
        this.selectedNodeType = selectedNodeType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public LazyDataModel<RiSgRcPlan> getLazyRiSgRcPlan() {
        return lazyRiSgRcPlan;
    }

    public void setLazyRiSgRcPlan(LazyDataModel<RiSgRcPlan> lazyRiSgRcPlan) {
        this.lazyRiSgRcPlan = lazyRiSgRcPlan;
    }

    public RiSgRcPlanServiceImpl getRiSgRcPlanService() {
        return riSgRcPlanService;
    }

    public void setRiSgRcPlanService(RiSgRcPlanServiceImpl riSgRcPlanService) {
        this.riSgRcPlanService = riSgRcPlanService;
    }

    public Node getNodeRnc() {
        return nodeRnc;
    }

    public void setNodeRnc(Node nodeRnc) {
        this.nodeRnc = nodeRnc;
    }

    public Node getNodeSgsnCurrent() {
        return nodeSgsnCurrent;
    }

    public void setNodeSgsnCurrent(Node nodeSgsnCurrent) {
        this.nodeSgsnCurrent = nodeSgsnCurrent;
    }

    public Node getNodeSgsnBackup() {
        return nodeSgsnBackup;
    }

    public void setNodeSgsnBackup(Node nodeSgsnBackup) {
        this.nodeSgsnBackup = nodeSgsnBackup;
    }

    public List<Node> getListNodeSgsnDown() {
        return listNodeSgsnDown;
    }

    public void setListNodeSgsnDown(List<Node> listNodeSgsnDown) {
        this.listNodeSgsnDown = listNodeSgsnDown;
    }

    public List<Node> getNodeSgsnDown() {
        return nodeSgsnDown;
    }

    public void setNodeSgsnDown(List<Node> nodeSgsnDown) {
        this.nodeSgsnDown = nodeSgsnDown;
    }

    public List<RiSgRcPlan> getSelectedRiSgRcPlan() {
        return selectedRiSgRcPlan;
    }

    public void setSelectedRiSgRcPlan(List<RiSgRcPlan> selectedRiSgRcPlan) {
        this.selectedRiSgRcPlan = selectedRiSgRcPlan;
    }

    public RiSgRcPlan getRiSgRcPlan() {
        return riSgRcPlan;
    }

    public void setRiSgRcPlan(RiSgRcPlan riSgRcPlan) {
        this.riSgRcPlan = riSgRcPlan;
    }

    public boolean isIsRiSgRcImport() {
        return isRiSgRcImport;
    }

    public void setIsRiSgRcImport(boolean isRiSgRcImport) {
        this.isRiSgRcImport = isRiSgRcImport;
    }

    public List<RiSgRcResultImport> getLstRiSgRcImportResult() {
        return lstRiSgRcImportResult;
    }

    public void setLstRiSgRcImportResult(List<RiSgRcResultImport> lstRiSgRcImportResult) {
        this.lstRiSgRcImportResult = lstRiSgRcImportResult;
    }

    public LazyDataModel<RiMsRcPlan> getLazyRiMsRcPlan() {
        return lazyRiMsRcPlan;
    }

    public void setLazyRiMsRcPlan(LazyDataModel<RiMsRcPlan> lazyRiMsRcPlan) {
        this.lazyRiMsRcPlan = lazyRiMsRcPlan;
    }

    public RiMsRcPlanServiceImpl getRiMsRcPlanService() {
        return riMsRcPlanService;
    }

    public void setRiMsRcPlanService(RiMsRcPlanServiceImpl riMsRcPlanService) {
        this.riMsRcPlanService = riMsRcPlanService;
    }

    public Node getNodeMscCurrent() {
        return nodeMscCurrent;
    }

    public void setNodeMscCurrent(Node nodeMscCurrent) {
        this.nodeMscCurrent = nodeMscCurrent;
    }

    public Node getNodeMscBackup() {
        return nodeMscBackup;
    }

    public void setNodeMscBackup(Node nodeMscBackup) {
        this.nodeMscBackup = nodeMscBackup;
    }

    public List<Node> getListNodeMscDown() {
        return listNodeMscDown;
    }

    public void setListNodeMscDown(List<Node> listNodeMscDown) {
        this.listNodeMscDown = listNodeMscDown;
    }

    public List<Node> getNodeMscDown() {
        return nodeMscDown;
    }

    public void setNodeMscDown(List<Node> nodeMscDown) {
        this.nodeMscDown = nodeMscDown;
    }

    public List<RiMsRcPlan> getSelectedRiMsRcPlan() {
        return selectedRiMsRcPlan;
    }

    public void setSelectedRiMsRcPlan(List<RiMsRcPlan> selectedRiMsRcPlan) {
        this.selectedRiMsRcPlan = selectedRiMsRcPlan;
    }

    public RiMsRcPlan getRiMsRcPlan() {
        return riMsRcPlan;
    }

    public void setRiMsRcPlan(RiMsRcPlan riMsRcPlan) {
        this.riMsRcPlan = riMsRcPlan;
    }

    public boolean isIsRiMsRcImport() {
        return isRiMsRcImport;
    }

    public void setIsRiMsRcImport(boolean isRiMsRcImport) {
        this.isRiMsRcImport = isRiMsRcImport;
    }

    public List<RiMsRcResultImport> getLstRiMsRcImportResult() {
        return lstRiMsRcImportResult;
    }

    public void setLstRiMsRcImportResult(List<RiMsRcResultImport> lstRiMsRcImportResult) {
        this.lstRiMsRcImportResult = lstRiMsRcImportResult;
    }

    //</editor-fold>
    @PostConstruct
    public void onStart() {
        try {
            //selectedNodeType = new NodeTypeServiceImpl().findById(Config.NODE_TYPE_ID_DEFAULT);
            riSgRcPlan = new RiSgRcPlan();
            userName = SessionWrapper.getCurrentUsername();
            insertIncidentSolution = new RiIncidentSolution();
            selectedIncidentSolution = new RiIncidentSolution();
            createTree();

            /**
             * ***********SGSN_RNC_START***************
             */
            Map<String, Object> filters = new HashMap<>();
            Map<String, String> orders = new LinkedHashMap<>();
            orders.put("nodeSgsnCurrent.nodeCode", "ASC");
            orders.put("nodeSgsnBackup.nodeCode", "ASC");
            orders.put("nodeSgsnBackup.province.areaCode", "ASC");
            orders.put("nodeRnc.province.areaCode", "ASC");
            orders.put("nodeRnc.nodeCode", "ASC");
            
            //Bat dau quet CrossCheck cho cac plan
            List<String> listSgsnDown = new ArrayList<>();
            List<RiSgRcPlan> listRescuePlan = riSgRcPlanService.findList(filters, orders);
            Map<String, String> mapString = new HashMap<String, String>();
            for (RiSgRcPlan bo : listRescuePlan) {
                if (mapString.containsKey(bo.getNodeSgsnCurrent().getNodeCode())) {

                } else {
                    mapString.put(bo.getNodeSgsnCurrent().getNodeCode(), bo.getNodeSgsnCurrent().getNodeCode());
                    listSgsnDown.add(bo.getNodeSgsnCurrent().getNodeCode());
                }
            }
            RiSgRcController riSgRcController = new RiSgRcController();
            riSgRcController.crossCheck(listSgsnDown);
            
            lazyRiSgRcPlan = new LazyDataModelBaseNew<>(riSgRcPlanService, filters, orders);

//            Map<String, String> ordersNode = new LinkedHashMap<>();
//            ordersNode.put("nodeCode", "ASC");
//            nodes = new NodeServiceImpl().findList(filters, ordersNode);
//            Map<String, String> ordersVendor = new LinkedHashMap<>();
//            ordersVendor.put("vendorName", "ASC");
//            vendors = new VendorServiceImpl().findList(filters, ordersVendor);
//            Map<String, String> ordersVersion = new LinkedHashMap<>();
//            ordersVersion.put("versionName", "ASC");
//            versions = new VersionServiceImpl().findList(filters, ordersVersion);
//            Map<String, String> ordersNodeType = new LinkedHashMap<>();
//            ordersNodeType.put("nodeTypeName", "ASC");
//            nodeTypes = new NodeTypeServiceImpl().findList(filters, ordersNodeType);
            /**
             * ***********SGSN_RNC_END***************
             */
            /**
             * ***********MSC_RNC_START***************
             */
            Map<String, String> ordersMscRnc = new LinkedHashMap<>();
            ordersMscRnc.put("nodeMscCurrent.nodeCode", "ASC");
            ordersMscRnc.put("nodeMscBackup.nodeCode", "ASC");
            ordersMscRnc.put("nodeMscBackup.province.areaCode", "ASC");
            ordersMscRnc.put("nodeRnc.province.areaCode", "ASC");
            ordersMscRnc.put("nodeRnc.nodeCode", "ASC");
            lazyRiMsRcPlan = new LazyDataModelBaseNew<>(riMsRcPlanService, filters, ordersMscRnc);
            listNodeMscDown = new ArrayList<>();
            /**
             * ***********MSC_RNC_END***************
             */
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    //<editor-fold defaultstate="collapsed" desc="RNC_SGSN">
    //<editor-fold defaultstate="collapsed" desc="Common">
    public List<NodeType> autoCompleNodeType(String actionName) {
        List<NodeType> lstAction = new ArrayList<>();
        try {
            Map<String, Object> filters = new HashMap<>();
            if (actionName != null) {
                filters.put("typeName", actionName);
            }
            LinkedHashMap<String, String> order = new LinkedHashMap<String, String>();
            order.put("typeName", "ASC");
            lstAction = nodeTypeServiceImpl.findList(filters, order);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstAction;
    }

    public List<Node> autoCompleNodeRNC(String nodeCode) {
        List<Node> lstNode = new ArrayList<Node>();
        Map<String, Object> filters = new HashMap<>();

        if (nodeCode != null) {
            filters.put("nodeCode", nodeCode);
            filters.put("nodeType.typeName", "RNC");
        }
        try {
            lstNode = new NodeServiceImpl().findList(0, 100, filters, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstNode;
    }

    public List<Node> autoCompleNodeSGSN(String nodeCode) {
        List<Node> lstNode = new ArrayList<Node>();
        Map<String, Object> filters = new HashMap<>();

        if (nodeCode != null) {
            filters.put("nodeCode", nodeCode);
            filters.put("nodeType.typeName", "SGSN");
        }
        try {
            lstNode = new NodeServiceImpl().findList(0, 100, filters, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstNode;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CrossCheck">
    public void preCrossCheckRiSgRcPlan() {
        try {
            listNodeSgsnDown = new ArrayList<>();
            List<RiSgRcPlan> listRescuePlan;
            Map<String, Object> filters;
            Map<String, String> orders;
            filters = new HashMap<String, Object>();
            orders = new LinkedHashMap<>();
            orders.put("nodeSgsnCurrent.nodeCode", "ASC");
            orders.put("nodeSgsnBackup.nodeCode", "ASC");
            orders.put("nodeSgsnBackup.province.areaCode", "ASC");
            orders.put("nodeRnc.province.areaCode", "ASC");
            orders.put("nodeRnc.nodeCode", "ASC");
            listRescuePlan = riSgRcPlanService.findList(filters, orders);
            Map<String, String> mapString = new HashMap<String, String>();
            for (RiSgRcPlan bo : listRescuePlan) {
                if (mapString.containsKey(bo.getNodeSgsnCurrent().getNodeCode())) {

                } else {
                    mapString.put(bo.getNodeSgsnCurrent().getNodeCode(), bo.getNodeSgsnCurrent().getNodeCode());
                    listNodeSgsnDown.add(bo.getNodeSgsnCurrent());
                }
            }
            if (nodeSgsnDown != null) {
                nodeSgsnDown.clear();
            }
//            Node nodeAll = new Node();
//            nodeAll.setNodeCode("All");
//            nodeAll.setNodeId(0L);
//            listNodeSgsnDown.add(0, nodeAll);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void onClickAutoCheckRiSgRcPlan() {
        try {
            RiSgRcController riSgRcController = new RiSgRcController();
            List<String> listSgsnDown = new ArrayList<>();
            List<String> listSgsnDownAll = new ArrayList<>();
            for (Node boAll : listNodeSgsnDown) {
                listSgsnDownAll.add(boAll.getNodeCode());
            }
            for (Node bo : nodeSgsnDown) {
                if ("all".equals(bo.getNodeCode().toLowerCase())) {
                    listSgsnDown = new ArrayList<>(listSgsnDownAll);
                    break;
                } else {
                    listSgsnDown.add(bo.getNodeCode());
                }
            }
            if (riSgRcController.crossCheck(listSgsnDown)) {
                MessageUtil.setInfoMessageFromRes("label.form.add.rescue.crosscheck.success");
                RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCrossCheck').hide()");
            } else {
                MessageUtil.setInfoMessageFromRes("label.form.add.rescue.crosscheck.fail");
            }

        } catch (Exception ex) {
            MessageUtil.setInfoMessageFromRes("label.form.add.rescue.crosscheck.fail");
            logger.error(ex.getMessage(), ex);
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CreatMop">
    public void preRiSgRcCreateMop() throws Exception {
        if ((this.selectedRiSgRcPlan != null) && (this.selectedRiSgRcPlan.size() > 0)) {
//            String Err = "";
//            for (RiSgRcPlan plan : selectedRiSgRcPlan) {
            riSgRcMopType = 0L;
            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').show()");
//                HashMap<String, Object> filters = new HashMap<>();
//                filters.clear();
//                filters.put("nodeCode", plan.getNodeRnc().getNodeCode());
//                filters.put("sgsnName", plan.getNodeSgsnCurrent().getNodeCode());
//                
//                switch (plan.getNodeRnc().getVendor().getVendorName().toUpperCase()) {
//                    case Constants.vendorType.HUAWEI:
//                        List<RiSgRcParamRncHuawei> lstSgsnRnc = new RiSgRcParamRncHuaweiServiceImpl().findList(filters);
//                        if (lstSgsnRnc.isEmpty()) {
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
//
//                        } else {
//                            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').show()");
//                        }
//                        break;
//                    case Constants.vendorType.NOKIA:
//                        List<RiSgRcParamRncNokia> lstSgsnRnc1 = new RiSgRcParamRncNokiaServiceImpl().findList(filters);
//                        if (lstSgsnRnc1.isEmpty()) {
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
//                        } else {
//                            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').show()");
//                        }
//                        break;
//                    case Constants.vendorType.ERICSSON:
//                        List<RiSgRcParamRncEricsson> lstSgsnRnc2 = new RiSgRcParamRncEricssonServiceImpl().findList(filters);
//                        if (lstSgsnRnc2.isEmpty()) {
//                            MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.sgsnCurrent");
//                        } else {
//                            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').show()");
//                        }
//                        break;
//                }
//            }
        } else {
            MessageUtil.setInfoMessageFromRes("label.form.choose.one");
        }
    }

    public void onClickRiSgRcCreateMop() {

        RiSgRcController riSgRcController = new RiSgRcController();
        try {

            FlowTemplates flowTemplates = new FlowTemplatesServiceImpl().findById(12116L);
            if (flowTemplates == null) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
            } else {
                if (flowTemplates.getStatus() != 9) {
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                }
            }
            HashMap<String, HashMap<String, RiSgRcParam>> mapParamList = riSgRcController.getMapParamList();
            List<RiSgRcResult> listRiResult = new ArrayList<>();
            boolean checkCreateMop = true;
            //Gan trang thai dang thuc hien sinh mop de cac user khac khong chay lay tham so
            riSgRcPlanService.saveOrUpdate(selectedRiSgRcPlan);
            try {
                for (RiSgRcPlan plan : selectedRiSgRcPlan) {
                    plan.setIsCreateMop(1L);
                    plan.setUsersCreateMop(SessionWrapper.getCurrentUsername());
                    riSgRcPlanService.saveOrUpdate(plan);
                    RiSgRcResult riResult = new RiSgRcResult();
                    checkCreateMop = riSgRcController.createFlowRunAction(flowTemplates, plan, mapParamList, riResult, riSgRcMopType);
                    listRiResult.add(riResult);
                    plan.setIsCreateMop(0L);
                    riSgRcPlanService.saveOrUpdate(plan);
                }
            } catch (Exception ex) {
                throw ex;
            }

            if (checkCreateMop) {
                RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcCreateMop').hide()");
            } else {
                MessageUtil.setInfoMessageFromRes("Sinh Mop ứng cứu thất bại");
            }
            selectedRiSgRcPlan.clear();
        } catch (Exception ex) {
            if (ex instanceof MessageException) {
                MessageUtil.setErrorMessage(ex.getMessage());
            } else {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    public void preRiSgRcRefreshParam(RiSgRcPlan nodeData) {
        List<RiSgRcPlan> lstRiSgRcPlanDb = new ArrayList<>();
        List<String> lstNodeSgsn = new ArrayList<>();
        String nodeUse = "";
        String userCreateMop = "";
        boolean check = true;
        try {
            riSgRcPlan = nodeData;
            lstNodeSgsn.add(riSgRcPlan.getNodeSgsnBackup().getNodeCode());
            lstNodeSgsn.add(riSgRcPlan.getNodeSgsnCurrent().getNodeCode());
            Map<String, Object> filters = new HashMap<>();
            Map<String, Object> filters1 = new HashMap<>();
            Map<String, Object> filters2 = new HashMap<>();
            filters.put("isCreateMop", 1L);
            filters.put("nodeRnc.nodeCode", riSgRcPlan.getNodeRnc().getNodeCode());

            filters1.put("isCreateMop", 1L);
            filters1.put("nodeSgsnBackup.nodeCode-EXAC", lstNodeSgsn);

            filters2.put("isCreateMop", 1L);
            filters2.put("nodeSgsnCurrent.nodeCode-EXAC", lstNodeSgsn);

            lstRiSgRcPlanDb = riSgRcPlanService.findList(filters);
            if (!lstRiSgRcPlanDb.isEmpty()) {
                check = false;
                nodeUse = nodeUse + "," + riSgRcPlan.getNodeRnc().getNodeCode();
                for (RiSgRcPlan plan : lstRiSgRcPlanDb) {
                    if (plan.getUsersCreateMop() != null) {
                        userCreateMop = userCreateMop + "," + plan.getUsersCreateMop();
                    }
                }
            }

            lstRiSgRcPlanDb = riSgRcPlanService.findList(filters1);
            if (!lstRiSgRcPlanDb.isEmpty()) {
                check = false;
                nodeUse = nodeUse + "," + riSgRcPlan.getNodeSgsnBackup().getNodeCode();
                for (RiSgRcPlan plan : lstRiSgRcPlanDb) {
                    if (plan.getUsersCreateMop() != null && userCreateMop.contains("," + plan.getUsersCreateMop())) {
                        userCreateMop = userCreateMop + "," + plan.getUsersCreateMop();
                    }
                }
            }

            lstRiSgRcPlanDb = riSgRcPlanService.findList(filters2);
            if (!lstRiSgRcPlanDb.isEmpty()) {
                check = false;
                nodeUse = nodeUse + "," + riSgRcPlan.getNodeSgsnCurrent().getNodeCode();
                for (RiSgRcPlan plan : lstRiSgRcPlanDb) {
                    if (plan.getUsersCreateMop() != null && userCreateMop.contains("," + plan.getUsersCreateMop())) {
                        userCreateMop = userCreateMop + "," + plan.getUsersCreateMop();
                    }
                }
            }

            if (check) {

                account = null;
                password = null;
                useAccountSystem = true;
                oneAccount = true;
                mapAccount = new HashMap<>();
                mapAccount.put(riSgRcPlan.getNodeRnc().getNodeCode(), new AccountObj());
                mapAccount.put(riSgRcPlan.getNodeSgsnBackup().getNodeCode(), new AccountObj());
                mapAccount.put(riSgRcPlan.getNodeSgsnCurrent().getNodeCode(), new AccountObj());

                RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcRefreshParam').show()");
            } else {
                if (userCreateMop.startsWith(",")) {
                    userCreateMop = userCreateMop.substring(1);
                }
                if (nodeUse.startsWith(",")) {
                    nodeUse = nodeUse.substring(1);
                }
                String mess = "Tài khoản: " + userCreateMop + " đang tạo mop với các node mạng: " + nodeUse + " của phương án ứng cứu này! \n Vui lòng thực hiện lại trong 30s sau ";
                MessageUtil.setWarnMessageFromRes(mess);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void onClickRiSgRcRefreshParam() {
        HashMap<String, String> mapAcc = new HashMap<>();
        HashMap<String, String> mapPass = new HashMap<>();
        HashMap<String, String> mapNodeCode = new HashMap<>();
        HashMap<String, String> mapRescueId = new HashMap<>();
        HashMap<String, Long> mapParamLogId = new HashMap<>();
        RiSgRcPlan plan = new RiSgRcPlan();
        try {
            boolean check = true;
            if (!useAccountSystem) {
                if (oneAccount) {
                    if (account == null || password == null) {
                        check = false;
                    } else {
                        mapAcc.put(riSgRcPlan.getNodeSgsnBackup().getNodeCode().trim(), account);
                        mapAcc.put(riSgRcPlan.getNodeSgsnCurrent().getNodeCode().trim(), account);
                        mapAcc.put(riSgRcPlan.getNodeRnc().getNodeCode().trim(), account);

                        mapPass.put(riSgRcPlan.getNodeSgsnBackup().getNodeCode().trim(), password);
                        mapPass.put(riSgRcPlan.getNodeSgsnCurrent().getNodeCode().trim(), password);
                        mapPass.put(riSgRcPlan.getNodeRnc().getNodeCode().trim(), password);
                    }
                } else {
                    for (String key : mapAccount.keySet()) {
                        if (mapAccount.get(key).getAccount() == null || mapAccount.get(key).getPassword() == null) {
                            check = false;
                            break;
                        }
                    }
                    if (check) {
                        for (String key : mapAccount.keySet()) {
                            mapAcc.put(key, mapAccount.get(key).getAccount());
                            mapPass.put(key, mapAccount.get(key).getPassword());
                        }
                    }
                }
            }
            if (check) {
                String serverIp = MessageUtil.getResourceBundleConfig("process_socket_riSgRc_ip");
                int serverPort = Integer.parseInt(MessageUtil.getResourceBundleConfig("process_socket_riSgRc_port"));
                MessageObjectRescue mesObj = new MessageObjectRescue();
                mapRescueId.put(riSgRcPlan.getId().toString(), riSgRcPlan.getId().toString());
                mesObj.setMapRescueId(mapRescueId);
                mapNodeCode.put(riSgRcPlan.getNodeSgsnBackup().getNodeCode().trim(), riSgRcPlan.getNodeSgsnBackup().getNodeCode().trim());
                mapNodeCode.put(riSgRcPlan.getNodeSgsnCurrent().getNodeCode().trim(), riSgRcPlan.getNodeSgsnCurrent().getNodeCode().trim());
                mapNodeCode.put(riSgRcPlan.getNodeRnc().getNodeCode().trim(), riSgRcPlan.getNodeRnc().getNodeCode().trim());
                mesObj.setMapNodeCode(mapNodeCode);
                if (!useAccountSystem) {
                    mesObj.setMapUserName(mapAcc);
                    mesObj.setMapPass(mapPass);
                }
                //Set mapParamLogId
                Date dateLog = new Date();
                RiSgRcParamLog paramLog = new RiSgRcParamLog();
                RiSgRcParamLogServiceImpl riSgRcParamLogServiceImpl = new RiSgRcParamLogServiceImpl();

                paramLog.setNodeCode(riSgRcPlan.getNodeRnc().getNodeCode());
                paramLog.setNodeIp(riSgRcPlan.getNodeRnc().getNodeIp());
                paramLog.setUserExcute(SessionWrapper.getCurrentUsername());
                paramLog.setInsertTime(dateLog);
                paramLog.setVendorName(riSgRcPlan.getNodeRnc().getVendor().getVendorName());
                paramLog.setVersionName(riSgRcPlan.getNodeRnc().getVersion().getVersionName());
                paramLog.setNodeTypeName(riSgRcPlan.getNodeRnc().getNodeType().getTypeName());
                riSgRcParamLogServiceImpl.save(paramLog);

                mapParamLogId.put(riSgRcPlan.getNodeRnc().getNodeCode(), paramLog.getId());

                paramLog.setNodeCode(riSgRcPlan.getNodeSgsnBackup().getNodeCode());
                paramLog.setNodeIp(riSgRcPlan.getNodeSgsnBackup().getNodeIp());
                paramLog.setVendorName(riSgRcPlan.getNodeSgsnBackup().getVendor().getVendorName());
                paramLog.setVersionName(riSgRcPlan.getNodeSgsnBackup().getVersion().getVersionName());
                paramLog.setNodeTypeName(riSgRcPlan.getNodeSgsnBackup().getNodeType().getTypeName());
                riSgRcParamLogServiceImpl.save(paramLog);
                mapParamLogId.put(riSgRcPlan.getNodeSgsnBackup().getNodeCode(), paramLog.getId());

                paramLog.setNodeCode(riSgRcPlan.getNodeSgsnCurrent().getNodeCode());
                paramLog.setNodeIp(riSgRcPlan.getNodeSgsnCurrent().getNodeIp());
                paramLog.setVendorName(riSgRcPlan.getNodeSgsnCurrent().getVendor().getVendorName());
                paramLog.setVersionName(riSgRcPlan.getNodeSgsnCurrent().getVersion().getVersionName());
                paramLog.setNodeTypeName(riSgRcPlan.getNodeSgsnCurrent().getNodeType().getTypeName());
                riSgRcParamLogServiceImpl.save(paramLog);
                mapParamLogId.put(riSgRcPlan.getNodeSgsnCurrent().getNodeCode(), paramLog.getId());

                mesObj.setMapParamLogId(mapParamLogId);

                //Check type use account
                String encrytedMess = new String(Base64.encodeBase64((new Gson()).toJson(mesObj).getBytes("UTF-8")), "UTF-8");

                SocketClient client = new SocketClient(serverIp, serverPort);
                client.sendMsg(encrytedMess);

                String socketResult = client.receiveResult();
                if (socketResult != null && socketResult.contains("NOK")) {
                    MessageUtil.getResourceBundleMessage("label.form.add.rescue.refreshParam.error");
                    throw new Exception(socketResult);
                } else {
                    //Gan trang thai cho resfreshParam
                    riSgRcPlan.setIsRefreshParam(1L);
                    riSgRcPlan.setUsersRefreshParam(SessionWrapper.getCurrentUsername());
                    riSgRcPlanService.saveOrUpdate(riSgRcPlan);

                    for (int i = 0; i < 10; i++) {
                        plan = riSgRcPlanService.findById(riSgRcPlan.getId());
                        if (plan.getIsRefreshParam().equals(0L)) {
                            MessageUtil.getResourceBundleMessage("label.form.add.rescue.refreshParam.success");
                            RequestContext.getCurrentInstance().execute("PF('dlgRiSgRcRefreshParam').hide()");
                            check = false;
                            showLogRiSgRcRefreshParam(mapParamLogId);
                            break;
                        }
                        Thread.sleep(10000);
                    }
                    if (check) {
                        MessageUtil.getResourceBundleMessage("label.form.add.rescue.refreshParam.error");
                    }

                }
            } else {
                MessageUtil.getResourceBundleMessage("label.form.rescue.refreshParam.inputAccPass.fail");
            }

        } catch (Exception ex) {
            MessageUtil.getResourceBundleMessage("label.form.add.rescue.refreshParam.error");
            logger.error(ex.getMessage(), ex);
        }
    }

    public void preshowLogRiSgRc() {
        try {

            Map<String, Object> filters = new HashMap();
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("insertTime", "DESC");
            orders.put("nodeTypeName", "ASC");
            orders.put("vendorName", "ASC");
            orders.put("versionName", "ASC");
            orders.put("nodeCode", "ASC");

            lazyModelParamLog = new LazyDataModelBaseNew<>(new RiSgRcParamLogServiceImpl(), filters, orders);

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void showLogRiSgRcRefreshParam(HashMap<String, Long> mapParamLogId) {
        try {
            mapLogCommand = new HashMap<>();
            Map<String, Object> filters = new HashMap<>();
            LinkedHashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("commandIndex", "ASC");
            for (String key : mapParamLogId.keySet()) {
                filters.clear();
                filters.put("paramLogId", mapParamLogId.get(key));
                List<RiSgRcParamLogCommand> lstLogCommand = new RiSgRcParamLogCommandServiceImpl().findList(filters, orders);
                mapLogCommand.put(key, lstLogCommand);
            }
            RequestContext.getCurrentInstance().execute("PF('dlgShowLogParam').show()");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void preShowCmdLogRiSgRc(RiSgRcParamLogCommand logCmd) {
        selectParamLogCommand = new RiSgRcParamLogCommand();
        if (logCmd != null) {
            selectParamLogCommand = logCmd;
        }
    }

    public void preShowCmdLogParamValueRiSgRc(RiSgRcParamLogCommand logCmd) {
        HashMap<String, String> mapLogCommandParamValue = new HashMap<>();
        if (logCmd != null) {
            String[] paramValues = logCmd.getParamValue().replace("\n", "").split("#");
            for (String value : paramValues) {
                String[] values = value.split(":");
                if (values[0] != null && values.length > 1) {
                    if (mapLogCommandParamValue.containsKey(values[0])) {
                        mapLogCommandParamValue.put(values[0], (mapLogCommandParamValue.get(values[0]) + ";" + (values[1] != null ? values[1] : "")));
                    } else {
                        mapLogCommandParamValue.put(values[0], values[1] != null ? values[1] : "");
                    }
                }
            }
            listParamValue = new ArrayList<>();
            ParamValue paramValue;
            for (String key : new TreeSet<>(mapLogCommandParamValue.keySet())) {
                paramValue = new ParamValue();
                paramValue.setParamCode(key);
                paramValue.setParamValue(mapLogCommandParamValue.get(key));
                listParamValue.add(paramValue);
            }
        }
    }

    public void preShowLogParamRiSgRc(RiSgRcParamLog log) {
        if (log != null) {
            HashMap<String, Long> mapParamLogId = new HashMap<>();
            mapParamLogId.put(log.getNodeCode(), log.getId());
            showLogRiSgRcRefreshParam(mapParamLogId);
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Export_Rescue">
    public StreamedContent onExportRiSgRcPlan() {
        try {
            RiSgRcController riSgRcController = new RiSgRcController();
            File fileExport = riSgRcController.exportFile();
            return new DefaultStreamedContent(new FileInputStream(fileExport), ".xlsx", fileExport.getName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("button.export")));
        }
        return null;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Import_Rescue">
    public StreamedContent onDownloadTemplateRiSgRcPlan() {
        try {
            InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/templates/import/Template_import_rescue_infomation.xlsx");
            return new DefaultStreamedContent(stream, "application/xls", "Template_import_rescue_infomation.xlsx");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
//                    MessageUtil.getResourceBundleMessage("button.import")));
        }
        return null;
    }

    public void onImportRiSgRcPlan(FileUploadEvent event) {
        try {
            isRiSgRcImport = true;
            RiSgRcController riSgRcController = new RiSgRcController();
            HashMap<String, Node> mapNode = riSgRcController.mapNode();
            List<RiSgRcPlan> lstRescueImportSave = new ArrayList<>();

            riSgRcController.onImportPlan(event, nodeRnc, nodeSgsnBackup, nodeSgsnCurrent, lstRiSgRcImportResult, lstRescueImportSave, mapNode);
            if (!lstRescueImportSave.isEmpty()) {
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            } else {
                MessageUtil.setErrorMessageFromRes("label.noData.save");
            }
            List<String> lstCheck = new ArrayList<>();
            lstRescueImportSave = riSgRcPlanService.findList();
            HashMap<String, String> mapCheck = new HashMap<>();
            for (RiSgRcPlan bo : lstRescueImportSave) {
                if (!mapCheck.containsKey(bo.getNodeSgsnCurrent().getNodeCode())) {
                    lstCheck.add(bo.getNodeSgsnCurrent().getNodeCode());
                    mapCheck.put(bo.getNodeSgsnCurrent().getNodeCode(), bo.getNodeSgsnCurrent().getNodeCode());
                }
            }
            riSgRcController.crossCheck(lstCheck);

//            RequestContext.getCurrentInstance().execute("PF('dlgUploadRescueInfo').hide()");
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }
    }

    public StreamedContent onRiSgRcResultImport() {
        try {
            RiSgRcController riSgRcController = new RiSgRcController();

            File fileExport = riSgRcController.exportFileResultImport(lstRiSgRcImportResult);
            return new DefaultStreamedContent(new FileInputStream(fileExport), ".xlsx", fileExport.getName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
//                    MessageUtil.getResourceBundleMessage("button.import")));
        }
        return null;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Incident_Rescue">
    public void prepareShowIncidentSolution() {
        selectedIncidentSolution = new RiIncidentSolution();
        try {
            RequestContext.getCurrentInstance().execute("PF('dlgShowIncidentSolution').show()");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void preAddActionToTemplate() {

        if (selectedNode != null) {
            try {
                selectedIncidentSolution = (RiIncidentSolution) selectedNode.getData();
                selectedIncidentSolution = incidentSolutionService.findById(selectedIncidentSolution.getId());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

        }
    }

    public void preClickNode() {
        if (selectedNode != null) {
            try {
                selectedIncidentSolution = (RiIncidentSolution) selectedNode.getData();
                selectedIncidentSolution = incidentSolutionService.findById(selectedIncidentSolution.getId());
                if ("RNC-SGSN".equals(selectedIncidentSolution.getName())) {
                    isShowIncidentSolution = 1L;
                } else if ("RNC-MSC".equals(selectedIncidentSolution.getName())) {
                    isShowIncidentSolution = 2L;
                } else {
                    isShowIncidentSolution = 0L;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            isShowIncidentSolution = 0L;
        }
    }

    /*
     * Tao cay danh muc action
     */
    public void createTree() {
        RiIncidentSolution root;
        try {
            root = incidentSolutionService.findById(ROOT_TREE_ID);
            if (root != null) {
                rootNode = new DefaultTreeNode("action", "Root", null);

                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                orders.put("name", "ASC");

                List<RiIncidentSolution> lstAllData = null;
                try {
                    lstAllData = incidentSolutionService.findList(null, orders);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                buildTree(root, rootNode, lstAllData, 1);
                rootNode.setExpanded(true);
            }
        } catch (SysException | AppException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /*
     * Ham tao cay theo nut
     */
    public TreeNode buildTree(RiIncidentSolution treeObj, TreeNode parent, List<RiIncidentSolution> lstAllData, int level) {
        TreeNode newNode = null;
        try {
            List<RiIncidentSolution> childNode = getLstChid(lstAllData, treeObj.getId());

            if (childNode != null && childNode.size() > 0) {
                newNode = new DefaultTreeNode(PARENT_NODE, treeObj, parent);
                if (level == 1) {
                    newNode.setExpanded(true);
                }
                for (RiIncidentSolution tt : childNode) {
                    buildTree(tt, newNode, lstAllData, (level + 1));
                }
            } else if (level <= 3) {
                newNode = new DefaultTreeNode(PARENT_NODE, treeObj, parent);

            } else {
                newNode = new DefaultTreeNode(CHILD_NODE, treeObj, parent);
            }
            if (treeObj.isExpanded()) {
                newNode.setExpanded(true);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return newNode;
    }

    private List<RiIncidentSolution> getLstChid(List<RiIncidentSolution> lst, Long parentId) {
        List<RiIncidentSolution> lstSub = new ArrayList<>();
        try {
            for (Iterator<RiIncidentSolution> iterator = lst.iterator(); iterator.hasNext();) {
                RiIncidentSolution ac = iterator.next();
                if ((ac.getIncidentSolution() != null) && (ac.getIncidentSolution().getId() == parentId)) {
                    lstSub.add(ac);
                    iterator.remove();
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        Collections.sort(lstSub, new Comparator<RiIncidentSolution>() {

            @Override
            public int compare(RiIncidentSolution o1, RiIncidentSolution o2) {
                String preName1 = o1.getName().substring(0, Math.max(o1.getName().indexOf(" "), 0));
                String preName2 = o2.getName().substring(0, Math.max(o2.getName().indexOf(" "), 0));

                String name1 = o1.getName().substring(o1.getName().indexOf(" ") + 1);
                String name2 = o2.getName().substring(o2.getName().indexOf(" ") + 1);

                String[] tmpPre1s = preName1.split("[,.]", -1);
                String[] tmpPre2s = preName2.split("[,.]", -1);
                int r = comp(tmpPre1s, tmpPre2s);
                if (r == 0) {
                    return name1.trim().compareTo(name2.trim());
                }
                return r;
            }

            public int comp(String[] as, String[] bs) {
                int min = Math.min(as.length, bs.length);
                if (as.length < bs.length) {
                    return 1;
                }
                if (as.length > bs.length) {
                    return -1;
                }
                if (min == 0) {
                    return 0;
                }
                int i = Math.min(0, min - 1);
                if (as[i].equals(bs[i])) {
                    as = ArrayUtils.remove(as, i);
                    bs = ArrayUtils.remove(bs, i);
                    return comp(as, bs);
                }
                return as[i].compareTo(bs[i]);
            }
        });
        return lstSub;
    }

    public void rebuildTree() {
        try {
            List<TreeNode> lstChildOfCurNode = selectedNode.getChildren();
            if (selectedNode.getParent() != null) {
                TreeNode parent = selectedNode.getParent();
                if (parent.getChildren() != null) {
                    List<TreeNode> lstChild = parent.getChildren();
                    if (lstChild != null) {
                        int count = 0;
                        for (TreeNode node : lstChild) {
                            count++;
                            if (((RiIncidentSolution) node.getData()).getId() == ((RiIncidentSolution) selectedNode.getData()).getId()) {
                                lstChild.remove(node);
                                break;
                            }
                        } // end loop for

                        TreeNode newNode = new DefaultTreeNode(CHILD_NODE, selectedIncidentSolution, parent);
                        newNode.getChildren().addAll(lstChildOfCurNode);
                        parent.getChildren().add((count > 0 ? (count - 1) : count), newNode);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void prepareEdit() {
        isEdit = true;
        if (selectedNode != null) {
            try {
                RiIncidentSolution incidentSolution = (RiIncidentSolution) selectedNode.getData();
                incidentSolution = incidentSolutionService.findById(incidentSolution.getId());
                insertIncidentSolution = new RiIncidentSolution();
                insertIncidentSolution.setName(incidentSolution.getName());
                insertIncidentSolution.setDescription(incidentSolution.getDescription());
                insertIncidentSolution.setId(incidentSolution.getId());
                insertIncidentSolution.setIncidentSolution(insertIncidentSolution.getIncidentSolution());
                insertIncidentSolution.setLstIncidentSolution(insertIncidentSolution.getLstIncidentSolution());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            logger.error(">>>>>>>> ERROR selectedNode is null");
        }
    }

    public void prepareDelIncidentSolution() {
        boolean checkShowDlg = true;
        try {
            switch (selectedNode.getType()) {
                case PARENT_NODE:
                    if (selectedNode.getChildCount() > 0) {
                        checkShowDlg = false;
                        MessageUtil.setErrorMessageFromRes("label.err.incidentSolution.exist.child");
                    }
                    break;
//                case CHILD_NODE:
//                    RiIncidentSolution incidentSolution = (RiIncidentSolution) selectedNode.getData();
//                    incidentSolution = new RiIncidentSolutionServiceImpl().findById(incidentSolution.getId());
//                    break;
                default:
                    break;
            }
            if (checkShowDlg) {
                RequestContext.getCurrentInstance().execute("PF('confDlgDelIncidentSolution').show()");
            }
        } catch (Exception e) {
//            checkShowDlg = false;
            logger.error(e.getMessage(), e);
        }

    }

    /**
     * Ham them node con moi cho node hien tai
     */
    public void insertChildNode() {

        try {
            if (insertIncidentSolution.getName() == null || "".equals(insertIncidentSolution.getName().trim())) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                return;
            } else if (insertIncidentSolution.getName().trim().length() > MAX_LENGTH_SOLUTION_NAME) {
                MessageUtil.setErrorMessageFromRes("label.validate.length.action");
                return;
            }

            if (insertIncidentSolution.getDescription() == null || "".equals(insertIncidentSolution.getDescription().trim())) {
                MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
                return;
            } else if (insertIncidentSolution.getDescription().trim().length() > MAX_LENGTH_SOLUTION_NAME) {
                MessageUtil.setErrorMessageFromRes("label.validate.length.action.desc");
                return;
            }

            insertIncidentSolution.setName(insertIncidentSolution.getName().replaceAll(" +", " ").trim());
            insertIncidentSolution.setDescription(insertIncidentSolution.getDescription().replaceAll(" +", " ").trim());

            if (selectedNode == null || selectedNode.getData() == null) {
                MessageUtil.setErrorMessageFromRes("message.err.no.node.selected");
                return;
            }
            if (isEdit) {
                insertIncidentSolution.setIncidentSolution(incidentSolutionService.findById(((RiIncidentSolution) selectedNode.getData()).getIncidentSolution().getId()));
                insertIncidentSolution.setId(((RiIncidentSolution) selectedNode.getData()).getId());
            } else {
                insertIncidentSolution.setIncidentSolution(incidentSolutionService.findById(((RiIncidentSolution) selectedNode.getData()).getId()));
            }
            incidentSolutionService.saveOrUpdate(insertIncidentSolution);
            if (!isEdit) {
                String label = (getNodeLevel(selectedNode) < 4 ? PARENT_NODE : CHILD_NODE);
                TreeNode node = new DefaultTreeNode(label, insertIncidentSolution, selectedNode);
                selectedNode.getChildren().add(node);
            }
            RequestContext.getCurrentInstance().execute("PF('dlgAddIncidentSolution').hide()");
            MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            cleanRiSgRcPlan();
        } catch (Exception e) {

            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
        }
    }

    private Integer getNodeLevel(TreeNode node) {
        int level = 1;
        while (node != null) {
            node = node.getParent();
            if (node != null) {
                level++;
            }
        }
        return level;
    }

    /**
     * Ham xoa du lieu node con
     */
    public void deleteIncidentSolutionNode() {
        if (selectedNode != null) {
            try {
                deleteNodeOfTree(selectedNode);
                MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            } catch (Exception e) {
                MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
                logger.error(e.getMessage(), e);
            } finally {
                cleanRiSgRcPlan();
            }
        }
    }

    public void deleteNodeOfTree(TreeNode selectedNode) {
        if (selectedNode != null) {
            try {
                selectedNode.getChildren().clear();
                selectedNode.getParent().getChildren().remove(selectedNode);

                List<TreeNode> lstNodeDelete = selectedNode.getChildren();
                if (lstNodeDelete != null && !lstNodeDelete.isEmpty()) {
                    for (TreeNode node : lstNodeDelete) {
                        try {
                            // goi de quy 
                            deleteNodeOfTree(node);
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    } // end loop for
                }

                incidentSolutionService.delete((RiIncidentSolution) selectedNode.getData());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    public String getNameIncidentSolutionSelected() {
        if (selectedNode != null) {
            return ((RiIncidentSolution) selectedNode.getData()).getName();
        }
        return "";
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Insert_Update_Rescue">
    public void prepareEditRiSgRcPlan(RiSgRcPlan nodeData) {
        riSgRcPlan = nodeData;
        try {
            isEdit = true;
            Map<String, Object> filters = new HashMap<>();
            filters.put("nodeCode", nodeData.getNodeRnc().getNodeCode());

            List<Node> lstNode = new NodeServiceImpl().findListExac(filters, null);
            if (lstNode != null && !lstNode.isEmpty()) {
                nodeRnc = lstNode.get(0);
            }
            filters.clear();
            // get node nodeGgsnCurrent
            filters.put("nodeCode", nodeData.getNodeSgsnCurrent().getNodeCode());
            lstNode = new NodeServiceImpl().findListExac(filters, null);
            if (lstNode != null && !lstNode.isEmpty()) {
                nodeSgsnCurrent = lstNode.get(0);
            }
            filters.clear();
            // get node nodeGgsnBackup
            filters.put("nodeCode", nodeData.getNodeSgsnBackup().getNodeCode());
            lstNode = new NodeServiceImpl().findListExac(filters, null);
            if (lstNode != null && !lstNode.isEmpty()) {
                nodeSgsnBackup = lstNode.get(0);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void clearRiSgRcPlan() {
        nodeRnc = null;
        nodeSgsnCurrent = null;
        nodeSgsnBackup = null;
        if (nodeSgsnDown != null) {
            nodeSgsnDown.clear();
        }
        isEdit = false;
    }

    public void saveRiSgRcPlan() {
        try {
            RiSgRcController riSgRcController = new RiSgRcController();
            riSgRcController.savePlan(nodeRnc, nodeSgsnBackup, nodeSgsnCurrent, isEdit, riSgRcPlan);
            RequestContext.getCurrentInstance().execute("PF('dlgAddRiSgRcPlanNode').hide()");
            List<String> lstCheck = new ArrayList<>();
            lstCheck.add(nodeSgsnCurrent.getNodeCode());
            riSgRcController.crossCheck(lstCheck);
            clearRiSgRcPlan();

        } catch (Exception e) {

            logger.error(e.getMessage(), e);
        }
    }

    public boolean checkAddIncidentSolution() {
        boolean check = false;
        try {
            if (selectedNode != null) {
                if (selectedNode.getChildCount() > 0) {
                    check = true;
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return check;
    }

    public void cleanRiSgRcPlan() {
        isEdit = false;
        selectedNode = null;
        insertIncidentSolution = new RiIncidentSolution();
        selectedIncidentSolution = new RiIncidentSolution();
        selectedNodeType = new NodeType();
        isRiSgRcImport = false;
        lstRiSgRcImportResult.clear();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete_Rescue">
    public void preDeleteRiSgRcPlan() {
        if (selectedRiSgRcPlan != null && selectedRiSgRcPlan.size() > 0) {
            riSgRcPlan = selectedRiSgRcPlan.get(0);
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteRiSgRcPlan').show()");
        } else {
            MessageUtil.setInfoMessageFromRes("label.form.choose.one");
            riSgRcPlan = null;
        }
    }

    public void deleteRiSgRcPlan() {
        try {
            if (selectedRiSgRcPlan != null && selectedRiSgRcPlan.size() > 0) {
                riSgRcPlanService.delete(selectedRiSgRcPlan);
            }
            MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteRiSgRcPlan').hide()");
            if (selectedRiSgRcPlan != null) {
                selectedRiSgRcPlan.clear();
            }
        } catch (Exception ex) {
            MessageUtil.setInfoMessageFromRes("label.action.deleteFail");
            logger.error(ex.getMessage(), ex);
        }
    }

    public void clearSelect() {
        selectedRiSgRcPlan.clear();
    }

    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="RNC_MSC">
    //<editor-fold defaultstate="collapsed" desc="Common">
    public List<Node> autoCompleNodeMSC(String nodeCode) {
        List<Node> lstNode = new ArrayList<Node>();
        Map<String, Object> filters = new HashMap<>();

        if (nodeCode != null) {
            filters.put("nodeCode", nodeCode);
            filters.put("nodeType.typeName-EXAC", "MSC");
        }
        try {
            lstNode = new NodeServiceImpl().findList(0, 100, filters, null);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return lstNode;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CrossCheck">
    public void preCrossCheckRiMsRcPlan() {
        try {
            listNodeMscDown = new ArrayList<>();
            List<RiMsRcPlan> listRescuePlan;
            Map<String, Object> filters;
            Map<String, String> orders;
            filters = new HashMap<>();
            orders = new LinkedHashMap<>();
            orders.put("nodeMscCurrent.nodeCode", "ASC");
            orders.put("nodeMscBackup.nodeCode", "ASC");
            orders.put("nodeMscBackup.province.areaCode", "ASC");
            orders.put("nodeRnc.province.areaCode", "ASC");
            orders.put("nodeRnc.nodeCode", "ASC");
            listRescuePlan = riMsRcPlanService.findList(filters, orders);
            Map<String, String> mapString = new HashMap<>();
            for (RiMsRcPlan bo : listRescuePlan) {
                if (mapString.containsKey(bo.getNodeMscCurrent().getNodeCode())) {

                } else {
                    mapString.put(bo.getNodeMscCurrent().getNodeCode(), bo.getNodeMscCurrent().getNodeCode());
                    listNodeMscDown.add(bo.getNodeMscCurrent());
                }
            }
            if (nodeMscDown != null) {
                nodeMscDown.clear();
            }
//            Node nodeAll = new Node();
//            nodeAll.setNodeCode("All");
//            nodeAll.setNodeId(null);
//            listNodeMscDown.add(0, nodeAll);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void onClickAutoCheckRiMsRcPlan() {
        try {
            RiMsRcController RiMsRcController = new RiMsRcController();
            List<String> listMscDown = new ArrayList<>();
            List<String> listMscDownAll = new ArrayList<>();
            for (Node boAll : listNodeMscDown) {
                listMscDownAll.add(boAll.getNodeCode());
            }
            for (Node bo : nodeMscDown) {
                if ("all".equals(bo.getNodeCode().toLowerCase())) {
                    listMscDown = new ArrayList<>(listMscDownAll);
                    break;
                } else {
                    listMscDown.add(bo.getNodeCode());
                }
            }
            if (RiMsRcController.crossCheck(listMscDown)) {
                MessageUtil.setInfoMessageFromRes("label.form.add.rescue.crosscheck.success");
                RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcCrossCheck').hide()");
            } else {
                MessageUtil.setInfoMessageFromRes("label.form.add.rescue.crosscheck.fail");
            }

        } catch (Exception ex) {
            MessageUtil.setInfoMessageFromRes("label.form.add.rescue.crosscheck.fail");
            logger.error(ex.getMessage(), ex);
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="CreatMop">
    public void preRiMsRcCreateMop() throws Exception {
        if (selectedRiMsRcPlan != null && selectedRiMsRcPlan.size() > 0) {
            riMsRcMopType = 0L;
            RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcCreateMop').show()");
//            HashMap<String, Object> filters = new HashMap<>();
//            filters.put("nodeCode", selectedRiMsRcPlan.get(0).getNodeRnc().getNodeCode());
//            filters.put("mscName", selectedRiMsRcPlan.get(0).getNodeMscCurrent().getNodeCode());
//            switch (selectedRiMsRcPlan.get(0).getNodeRnc().getVendor().getVendorName().toUpperCase()) {
//                case Constants.vendorType.HUAWEI:
//                    List<RiMsRcParamRncHuawei> lstSgsnRnc = new RiMsRcParamRncHuaweiServiceImpl().findList(filters);
//                    if (lstSgsnRnc.isEmpty()) {
//                        MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.mscCurrent");
//
//                    } else {
//                        RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcCreateMop').show()");
//                    }
//                    break;
//                case Constants.vendorType.NOKIA:
//                    List<RiMsRcParamRncNokia> lstSgsnRnc1 = new RiMsRcParamRncNokiaServiceImpl().findList(filters);
//                    if (lstSgsnRnc1.isEmpty()) {
//                        MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.mscCurrent");
//
//                    } else {
//                        RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcCreateMop').show()");
//                    }
//                    break;
//                case Constants.vendorType.ERICSSON:
//                    List<RiMsRcParamRncEricsson> lstSgsnRnc2 = new RiMsRcParamRncEricssonServiceImpl().findList(filters);
//                    if (lstSgsnRnc2.isEmpty()) {
//                        MessageUtil.setErrorMessageFromRes("label.err.not.existed.rnc.mscCurrent");
//                    } else {
//                        RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcCreateMop').show()");
//                    }
//                    break;
//            }
        } else {
            MessageUtil.setInfoMessageFromRes("label.form.choose.one");
        }
    }

    public void onClickRiMsRcCreateMop() {
        List<RiMsRcPlan> listRescuePlan;
        if (selectedRiMsRcPlan != null) {
            listRescuePlan = new ArrayList<>(selectedRiMsRcPlan);
        } else {
            listRescuePlan = new ArrayList<>();
        }
        RiMsRcController riMsRcController = new RiMsRcController();
        try {

            FlowTemplates flowTemplates = new FlowTemplatesServiceImpl().findById(12241L);
            if (flowTemplates == null) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
            } else {
                if (flowTemplates.getStatus() != 9) {
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                }
            }
            HashMap<String, HashMap<String, RiMsRcParam>> mapParamList = riMsRcController.getMapParamList();
            List<RiMsRcResult> listRiResult = new ArrayList<>();
            boolean checkCreateMop = true;
            //Gan trang thai dang thuc hien sinh mop de cac user khac khong chay lay tham so
            for (RiMsRcPlan plan : selectedRiMsRcPlan) {
                plan.setIsCreateMop(1L);
                plan.setUsersCreateMop(SessionWrapper.getCurrentUsername());
            }
            riMsRcPlanService.saveOrUpdate(selectedRiMsRcPlan);
            for (RiMsRcPlan plan : listRescuePlan) {
                RiMsRcResult riResult = new RiMsRcResult();
                checkCreateMop = riMsRcController.createFlowRunAction(flowTemplates, plan, mapParamList, riResult);
                listRiResult.add(riResult);
                plan.setIsCreateMop(0L);
                riMsRcPlanService.saveOrUpdate(plan);
            }
            if (checkCreateMop) {
//                    MessageUtil.setInfoMessageFromRes("Sinh Mop ứng cứu thành công");
                RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcCreateMop').hide()");
            } else {
                MessageUtil.getResourceBundleMessage("label.err.create.mop");
            }
            if (selectedRiMsRcPlan != null) {
                selectedRiMsRcPlan.clear();
            }

        } catch (Exception ex) {
            if (ex instanceof MessageException) {
                MessageUtil.setErrorMessage(ex.getMessage());
            } else {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    public void preRiMsRcRefreshParam(RiMsRcPlan nodeData) {
        List<RiMsRcPlan> lstRiMsRcPlanDb = new ArrayList<>();
        List<String> lstNodeMsc = new ArrayList<>();
        String nodeUse = "";
        String userCreateMop = "";
        boolean check = true;
        try {
            riMsRcPlan = nodeData;
            lstNodeMsc.add(riMsRcPlan.getNodeMscBackup().getNodeCode());
            lstNodeMsc.add(riMsRcPlan.getNodeMscCurrent().getNodeCode());
            Map<String, Object> filters = new HashMap<>();
            Map<String, Object> filters1 = new HashMap<>();
            Map<String, Object> filters2 = new HashMap<>();
            filters.put("isCreateMop", 1L);
            filters.put("nodeRnc.nodeCode", riMsRcPlan.getNodeRnc().getNodeCode());

            filters1.put("isCreateMop", 1L);
            filters1.put("nodeMscBackup.nodeCode-EXAC", lstNodeMsc);

            filters2.put("isCreateMop", 1L);
            filters2.put("nodeMscCurrent.nodeCode-EXAC", lstNodeMsc);

            lstRiMsRcPlanDb = riMsRcPlanService.findList(filters);
            if (!lstRiMsRcPlanDb.isEmpty()) {
                check = false;
                nodeUse = nodeUse + "," + riMsRcPlan.getNodeRnc().getNodeCode();
                for (RiMsRcPlan plan : lstRiMsRcPlanDb) {
                    if (plan.getUsersCreateMop() != null) {
                        userCreateMop = userCreateMop + "," + plan.getUsersCreateMop();
                    }
                }
            }

            lstRiMsRcPlanDb = riMsRcPlanService.findList(filters1);
            if (!lstRiMsRcPlanDb.isEmpty()) {
                check = false;
                nodeUse = nodeUse + "," + riMsRcPlan.getNodeMscBackup().getNodeCode();
                for (RiMsRcPlan plan : lstRiMsRcPlanDb) {
                    if (plan.getUsersCreateMop() != null) {
                        userCreateMop = userCreateMop + "," + plan.getUsersCreateMop();
                    }
                }
            }

            lstRiMsRcPlanDb = riMsRcPlanService.findList(filters2);
            if (!lstRiMsRcPlanDb.isEmpty()) {
                check = false;
                nodeUse = nodeUse + "," + riMsRcPlan.getNodeMscCurrent().getNodeCode();
                for (RiMsRcPlan plan : lstRiMsRcPlanDb) {
                    if (plan.getUsersCreateMop() != null) {
                        userCreateMop = userCreateMop + "," + plan.getUsersCreateMop();
                    }
                }
            }

            if (check) {

                account = null;
                password = null;
                useAccountSystem = true;
                oneAccount = true;
                mapAccount = new HashMap<>();
                mapAccount.put(riMsRcPlan.getNodeRnc().getNodeCode(), new AccountObj());
                mapAccount.put(riMsRcPlan.getNodeMscBackup().getNodeCode(), new AccountObj());
                mapAccount.put(riMsRcPlan.getNodeMscCurrent().getNodeCode(), new AccountObj());

                RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcRefreshParam').show()");
            } else {
                if (userCreateMop.startsWith(",")) {
                    userCreateMop = userCreateMop.substring(1);
                }
                if (nodeUse.startsWith(",")) {
                    nodeUse = nodeUse.substring(1);
                }
                String mess = "Tài khoản: " + userCreateMop + " đang tạo mop với các node mạng: " + nodeUse + " của phương án ứng cứu này! \n Vui lòng thực hiện lại trong 30s sau ";
                MessageUtil.setWarnMessageFromRes(mess);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void onClickRiMsRcRefreshParam() {
        HashMap<String, String> mapAcc = new HashMap<>();
        HashMap<String, String> mapPass = new HashMap<>();
        HashMap<String, String> mapNodeCode = new HashMap<>();
        HashMap<String, String> mapRescueId = new HashMap<>();
        RiMsRcPlan plan;
        try {
            boolean check = true;
            if (!useAccountSystem) {
                if (oneAccount) {
                    if (account == null || password == null) {
                        check = false;
                    } else {
                        mapAcc.put(riMsRcPlan.getNodeMscBackup().getNodeCode().trim(), account);
                        mapAcc.put(riMsRcPlan.getNodeMscCurrent().getNodeCode().trim(), account);
                        mapAcc.put(riMsRcPlan.getNodeRnc().getNodeCode().trim(), account);

                        mapPass.put(riMsRcPlan.getNodeMscBackup().getNodeCode().trim(), password);
                        mapPass.put(riMsRcPlan.getNodeMscCurrent().getNodeCode().trim(), password);
                        mapPass.put(riMsRcPlan.getNodeRnc().getNodeCode().trim(), password);
                    }
                } else {
                    for (String key : mapAccount.keySet()) {
                        if (mapAccount.get(key).getAccount() == null || mapAccount.get(key).getPassword() == null) {
                            check = false;
                            break;
                        }
                    }
                    if (check) {
                        for (String key : mapAccount.keySet()) {
                            mapAcc.put(key, mapAccount.get(key).getAccount());
                            mapPass.put(key, mapAccount.get(key).getPassword());
                        }
                    }
                }
            }
            if (check) {
                String serverIp = MessageUtil.getResourceBundleConfig("process_socket_riMsRc_ip");
                int serverPort = Integer.parseInt(MessageUtil.getResourceBundleConfig("process_socket_riMsRc_port"));
                MessageObjectRescue mesObj = new MessageObjectRescue();
                mapRescueId.put(riMsRcPlan.getId().toString(), riMsRcPlan.getId().toString());
                mesObj.setMapRescueId(mapRescueId);
                mapNodeCode.put(riMsRcPlan.getNodeMscBackup().getNodeCode().trim(), riMsRcPlan.getNodeMscBackup().getNodeCode().trim());
                mapNodeCode.put(riMsRcPlan.getNodeMscCurrent().getNodeCode().trim(), riMsRcPlan.getNodeMscCurrent().getNodeCode().trim());
                mapNodeCode.put(riMsRcPlan.getNodeRnc().getNodeCode().trim(), riMsRcPlan.getNodeRnc().getNodeCode().trim());
                mesObj.setMapNodeCode(mapNodeCode);

                if (!useAccountSystem) {
                    mesObj.setMapUserName(mapAcc);
                    mesObj.setMapPass(mapPass);
                }

                //Check type use account
                String encrytedMess = new String(Base64.encodeBase64((new Gson()).toJson(mesObj).getBytes("UTF-8")), "UTF-8");

                SocketClient client = new SocketClient(serverIp, serverPort);
                client.sendMsg(encrytedMess);

                String socketResult = client.receiveResult();
                if (socketResult != null && socketResult.contains("NOK")) {
                    MessageUtil.getResourceBundleMessage("label.form.add.rescue.refreshParam.error");
                    throw new Exception(socketResult);
                } else {
                    //Gan trang thai cho resfreshParam
                    riMsRcPlan.setIsRefreshParam(1L);
                    riMsRcPlan.setUsersRefreshParam(SessionWrapper.getCurrentUsername());
                    riMsRcPlanService.saveOrUpdate(riMsRcPlan);
                    for (int i = 0; i < 10; i++) {
                        plan = riMsRcPlanService.findById(riMsRcPlan.getId());
                        if (plan.getIsRefreshParam().equals(0L)) {
                            MessageUtil.getResourceBundleMessage("label.form.add.rescue.refreshParam.success");
                            RequestContext.getCurrentInstance().execute("PF('dlgRiMsRcRefreshParam').hide()");
                            check = false;
                            break;
                        }
                        Thread.sleep(10000);
                    }
                    if (check) {
                        MessageUtil.getResourceBundleMessage("label.form.add.rescue.refreshParam.error");
                        throw new Exception(socketResult);
                    }
                }
            } else {
                MessageUtil.getResourceBundleMessage("label.form.rescue.refreshParam.inputAccPass.fail");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Export_Rescue">
    public StreamedContent onExportRiMsRcPlan() {
        try {
            RiMsRcController riMsRcController = new RiMsRcController();
            File fileExport = riMsRcController.exportFile();
            return new DefaultStreamedContent(new FileInputStream(fileExport), ".xlsx", fileExport.getName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("button.export")));
        }
        return null;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Import_Rescue">
    public StreamedContent onDownloadTemplateRiMsRcPlan() {
        try {
            InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/templates/import/Template_import_rescue_infomation_Msc_Rnc.xlsx");
            return new DefaultStreamedContent(stream, "application/xls", "Template_import_rescue_infomation_Msc_Rnc.xlsx");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
//                    MessageUtil.getResourceBundleMessage("button.import")));
        }
        return null;
    }

    public void onImportRiMsRcPlan(FileUploadEvent event) {
        try {
            isRiMsRcImport = true;
            RiMsRcController riMsRcController = new RiMsRcController();
            HashMap<String, Node> mapNode = riMsRcController.mapNode();
            List<RiMsRcPlan> lstRescueImportSave = new ArrayList<>();

            riMsRcController.onImportPlan(event, nodeRnc, nodeMscBackup, nodeMscCurrent, lstRiMsRcImportResult, lstRescueImportSave, mapNode);
            if (!lstRescueImportSave.isEmpty()) {
                MessageUtil.setInfoMessageFromRes("label.action.updateOk");
            } else {
                MessageUtil.setErrorMessageFromRes("label.noData.save");
            }
            List<String> lstCheck = new ArrayList<>();
            lstRescueImportSave = riMsRcPlanService.findList();
            HashMap<String, String> mapCheck = new HashMap<>();
            for (RiMsRcPlan bo : lstRescueImportSave) {
                if (!mapCheck.containsKey(bo.getNodeMscCurrent().getNodeCode())) {
                    lstCheck.add(bo.getNodeMscCurrent().getNodeCode());
                    mapCheck.put(bo.getNodeMscCurrent().getNodeCode(), bo.getNodeMscCurrent().getNodeCode());
                }
            }
            riMsRcController.crossCheck(lstCheck);

//            RequestContext.getCurrentInstance().execute("PF('dlgUploadRescueInfo').hide()");
        } catch (Exception e) {
            MessageUtil.setErrorMessageFromRes("label.action.updateFail");
            logger.error(e.getMessage(), e);
        }
    }

    public StreamedContent onRiMsRcResultImport() {
        try {
            RiMsRcController riMsRcController = new RiMsRcController();
            File fileExport = riMsRcController.exportFileResultImport(lstRiMsRcImportResult);
            return new DefaultStreamedContent(new FileInputStream(fileExport), ".xlsx", fileExport.getName());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
//                    MessageUtil.getResourceBundleMessage("button.import")));
        }
        return null;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Insert_Update_Rescue">
    public void prepareEditRiMsRcPlan(RiMsRcPlan nodeData) {
        riMsRcPlan = nodeData;
        try {
            isEdit = true;
            Map<String, Object> filters = new HashMap<>();
            filters.put("nodeCode", nodeData.getNodeRnc().getNodeCode());

            List<Node> lstNode = new NodeServiceImpl().findListExac(filters, null);
            if (lstNode != null && !lstNode.isEmpty()) {
                nodeRnc = lstNode.get(0);
            }
            filters.clear();
            // get node nodeGgsnCurrent
            filters.put("nodeCode", nodeData.getNodeMscCurrent().getNodeCode());
            lstNode = new NodeServiceImpl().findListExac(filters, null);
            if (lstNode != null && !lstNode.isEmpty()) {
                nodeMscCurrent = lstNode.get(0);
            }
            filters.clear();
            // get node nodeGgsnBackup
            filters.put("nodeCode", nodeData.getNodeMscBackup().getNodeCode());
            lstNode = new NodeServiceImpl().findListExac(filters, null);
            if (lstNode != null && !lstNode.isEmpty()) {
                nodeMscBackup = lstNode.get(0);
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void clearRiMsRcPlan() {
        nodeRnc = null;
        nodeMscCurrent = null;
        nodeMscBackup = null;
        if (nodeMscDown != null) {
            nodeMscDown.clear();
        }
        isEdit = false;
    }

    public void saveRiMsRcPlan() {
        try {
            RiMsRcController riMsRcController = new RiMsRcController();
            riMsRcController.savePlan(nodeRnc, nodeMscBackup, nodeMscCurrent, isEdit, riMsRcPlan);
            RequestContext.getCurrentInstance().execute("PF('dlgAddRiMsRcPlanNode').hide()");
            List<String> lstCheck = new ArrayList<>();
            lstCheck.add(nodeMscCurrent.getNodeCode());
            riMsRcController.crossCheck(lstCheck);
            clearRiMsRcPlan();

        } catch (Exception e) {

            logger.error(e.getMessage(), e);
        }
    }

    public void cleanRiMsRcPlan() {
        isEdit = false;
        selectedNode = null;
        insertIncidentSolution = new RiIncidentSolution();
        selectedIncidentSolution = new RiIncidentSolution();
        selectedNodeType = new NodeType();
        isRiMsRcImport = false;
        lstRiMsRcImportResult.clear();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete_Rescue">
    public void preDeleteRiMsRcPlan() {
        if (selectedRiMsRcPlan != null && selectedRiMsRcPlan.size() > 0) {
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteRiMsRcPlan').show()");
        } else {
            MessageUtil.setInfoMessageFromRes("label.form.choose.one");
        }
    }

    public void deleteRiMsRcPlan() {
        try {
            if (selectedRiMsRcPlan != null && selectedRiMsRcPlan.size() > 0) {
                riMsRcPlanService.delete(selectedRiMsRcPlan);
            }
            MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteRiMsRcPlan').hide()");
            if (selectedRiMsRcPlan != null) {
                selectedRiMsRcPlan.clear();
            }
        } catch (Exception ex) {
            MessageUtil.setInfoMessageFromRes("label.action.deleteFail");
            logger.error(ex.getMessage(), ex);
        }
    }
    //</editor-fold>
    //</editor-fold>
}
