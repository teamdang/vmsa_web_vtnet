package com.viettel.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.viettel.model.*;
import com.viettel.persistence.*;
import com.viettel.util.*;
import com.viettel.webservice.object.DataCellUpdateNimsForGnoc;
import com.viettel.webservice.object.ResultDTO;
import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.jsoup.Jsoup;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.lazy.LazyDataModelSearchNode;
import com.viettel.object.GroupAction;
import com.viettel.object.InfoNode;
import com.viettel.object.ObjectExport;
import com.viettel.object.ObjectExportCommand;
import com.viettel.object.ObjectImportDt;
import com.viettel.object.VendorNodeType;
import com.viettel.persistence.common.ConditionQuery;
import com.viettel.persistence.common.OrderBy;
import com.viettel.util.Config.NODE_TYPE;
import com.viettel.util.ParamUtil.PARAMCODE;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.UnselectEvent;

/**
 * @author huynx6
 * @version 1.0.0
 * @since Sep 7, 2016
 */
@SuppressWarnings("serial")
@ViewScoped
@ManagedBean
public class GenerateFlowRunController implements Serializable {

    protected static final Logger logger = LoggerFactory.getLogger(GenerateFlowRunController.class);

    private List<SelectItem> crs = new ArrayList<SelectItem>();
    private List<FlowTemplates> flowTemplates;
    private FlowTemplates selectedFlowTemplates;
    private List<Node> nodes;
    private Node selectedNode;
    //20170420_HaNV15_Add_Start
    private CatCountryBO selectedCountry;
    @ManagedProperty(value = "#{mapUserCountryService}")
    private MapUserCountryServiceImpl mapUserCountryService;
    //20170420_HaNV15_Add_End

    @ManagedProperty("#{flowTemplatesService}")
    private FlowTemplatesServiceImpl flowTemplatesService;

    @ManagedProperty("#{nodeService}")
    private NodeServiceImpl nodeService;

    Map<String, List<ParamInput>> mapParam = new HashMap<String, List<ParamInput>>();
    Map<String, List<ParamValue>> mapParamValue = new HashMap<String, List<ParamValue>>();
    Map<String, List<ParamValue>> mapParamInputsDistinct = new HashMap<String, List<ParamValue>>();
    private Map<String, List<GroupAction>> mapGroupAction = new HashMap<String, List<GroupAction>>();

    private FlowRunAction flowRunAction = new FlowRunAction();

    private LinkedListMultimap<String, ActionOfFlow> groupActions = LinkedListMultimap.create();

    private LazyDataModel<FlowRunAction> lazyFlowRunAction;

    private LazyDataModel<Node> lazyNode;
    private List<Node> preNodes = new ArrayList<Node>();
    private List<Node> tmpNodes = new ArrayList<Node>();
    private List<Node> nodeSeachs = new ArrayList<Node>();
    private boolean rerender = true;

    private List<NodeType> nodeType4Searchs = new ArrayList<NodeType>();
    private List<Vendor> vendor4Searchs = new ArrayList<Vendor>();
    private List<Version> version4Searchs = new ArrayList<Version>();

    @ManagedProperty("#{flowRunActionService}")
    private GenericDaoServiceNewV2<FlowRunAction, Long> flowRunActionService;

    private boolean isTabExecute = false;

    private Integer searchNodeLab = 0;

    private boolean loadRingSrt;
    private List<FlowRunAction> selectedFlowRunActions = new ArrayList<>(0);
    private List<?> paramNodes;
    //Quytv7 Edit core merger Mop
    private List<String> subFlowRuns;
    private String selectSubflowRun;
    private Map<String, List<Node>> mapSubFlowRunNodes = new LinkedHashMap<String, List<Node>>();
    private Map<String, Long> mapSubFlowRunNum = new LinkedHashMap<>();
    private TabView tabview = new TabView();
    private boolean isExecute;
    //Quytv7 ghi log action
    private String logAction = "";
    private String className = GenerateFlowRunController.class.getName();
    private boolean isSaveSucess;
    //11102017_Quytv7 them tham so noi suy node mang start
    private List<ParamValue1> listParamInterPolateNode = new ArrayList<>();
    //11102017_Quytv7 them tham so noi suy node mang end
//20172610_quytv7_getCommandParam in node. Nghiep vu GetParam from NCMS start
    Map<String, Multimap<CommandDetail, ParamInput>> mapCommandParamInput = new HashMap<>();
    Map<String, List<CommandDetail>> mapNodeCommandDetail = new HashMap<>();
    //20172610_quytv7_getCommandParam in node. Nghiep vu GetParam from NCMS end
    private String pathOut;
    private String pathTemplate;

    @PostConstruct
    public void onStart() {
        flowRunAction = new FlowRunAction();
        logAction = LogUtils.addContent("", "Login Function");
        nodes = new ArrayList<Node>();
        subFlowRuns = new ArrayList<>();
//        subFlowRuns.add("test");
        Map<String, Object> filters = new HashMap<String, Object>();
        filters.put("parentId", null);

        ResourceBundle bundle = ResourceBundle.getBundle("cas");
        if (!bundle.getString("service").contains("10.61.127.190")) {
            filters.put("status", 9);
        }

        LinkedHashMap<String, String> orders = new LinkedHashMap<String, String>();
        orders.put("flowTemplateName", "ASC");
        Map<String, Object> _filters = new HashMap<String, Object>();
        //Quytv7 Tam thoi lay cac gia tri nodeRun co subFlowRun != null
//        _filters.put("nodeRuns", null);
        try {
            //20170420_HaNV15_Add_Start
//            List<String> lstCountry = mapUserCountryService.getListCountryForUser();
//            if (lstCountry != null && lstCountry.size() > 0) {
//                _filters.put("countryCode.countryCode-EXAC", lstCountry);
//            }
            //20170420_HaNV15_Add_End
            flowTemplates = flowTemplatesService.findList(filters, orders);
            LinkedHashMap<String, String> _orders = new LinkedHashMap<String, String>();
            _orders.put("createDate", "DESC");
            lazyFlowRunAction = new LazyDataModelBaseNew<FlowRunAction, Long>(flowRunActionService, _filters, _orders);
            lazyNode = new LazyDataModelSearchNode<>(new NodeServiceImpl());
//			ActionDetail actionDetail = flowTemplates.get(0).getActionOfFlows().get(0).getAction().getActionDetails().get(0);
//			List<ActionCommand> ls = actionDetail.getActionCommands();
//			String cmd = ls.get(0).getCommandDetail().getCommandTelnetParser().getCmd();
//			List<ParamInput> as = ls.get(0).getCommandDetail().getParamInputs();
//			System.err.println(ls);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.error(e.getMessage(), e);
        }

        loadCR();
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        if (context.getRequestMap().get("javax.servlet.forward.request_uri").toString().endsWith("/execute")) {
            isTabExecute = true;
            _filters.put("status-NEQ", 0L);
        } else {
            isTabExecute = false;
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    private void loadCR() {
        crs.clear();
        //crs =  CrUtil.getListCrNumber();
        //crs.add(0,new SelectItem(null, MessageUtil.getResourceBundleMessage("view.choose.cr"),null, false, true, true));
        crs.add(new SelectItem(Config.CR_DEFAULT, Config.CR_DEFAULT));

    }

    public boolean isEditMop() {
        return flowRunAction.getStatus() == 0L;// || flowRunAction.getStatus()==1L; 
    }

    public boolean isApproveMop() {
        return flowRunAction.getStatus() == 0L;
    }

    public void preAddFlowRunAction() {

        if (nodes == null) {
            nodes = new ArrayList<>();
        }
        this.isSaveSucess = false;
        isExecute = false;
        nodes.clear();
        flowRunAction = new FlowRunAction();
        flowRunAction.setStatus(0L);
        flowRunAction.setTimeRun(new Date());
        flowRunAction.setCreateDate(new Date());
        flowRunAction.setCrNumber(Config.CR_DEFAULT);
        mapParamValueGroup.clear();
        mapParamValue.clear();
        selectedFlowTemplates = null;
        nodeSeachs.clear();
        tmpNodes.clear();
        preNodes.clear();
        if (subFlowRuns == null) {
            subFlowRuns = new ArrayList<>();
        }
        subFlowRuns.clear();
        subFlowRuns.add(Config.SUB_FLOW_RUN_DEFAULT);
        loadCR();
    }

    Map<String, Multimap<Long, ParamValue>> mapParamValueGroup = new HashMap<>();
    Map<String, Object> caches = new HashMap<>();

    public List<ParamValue> getParamInputs(String subFlowRun, Node node) {
        if (subFlowRun == null || subFlowRun.isEmpty()) {
            subFlowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        if (!mapParamValue.containsKey(subFlowRun + "#" + node.getNodeCode())) {
//            logger.info("------getParamInputs------");
            Set<ParamInput> inputs = new LinkedHashSet<>();
            Map<Long, Long> mapParamInputGroupCode = new HashMap<>();
            Multimap<Long, ParamValue> _mapParamValueGroup = ArrayListMultimap.create();
            Boolean checkRefenderParam = false;
            HashMap<Long, Boolean> mapGroupActionDeclare = new HashMap<>();
            //20172610_quytv7_getCommandParam in node. Nghiep vu GetParam from NCMS start
            Multimap<CommandDetail, ParamInput> _mapCommandParamInput = ArrayListMultimap.create();
            List<CommandDetail> listCommandDetailInNode = new ArrayList<>();
            //20172610_quytv7_getCommandParam in node. Nghiep vu GetParam from NCMS end
            List<GroupAction> lstGroupAction = mapGroupAction.get(subFlowRun + "#" + node.getNodeCode());
            if (lstGroupAction != null) {
                for (GroupAction groupAction : lstGroupAction) {
                    for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                        mapGroupActionDeclare.put(actionOfFlow.getStepNum(), groupAction.isDeclare());
                    }
                }
            }
            Map<ParamInput, Integer> mapTotalRef = new HashMap<>();
            Map<ParamInput, Integer> mapTotal = new HashMap<>();
            Map<ParamInput, List<Long>> mapRefActionFlowId = new HashMap<>();
            if (selectedFlowTemplates != null) {
                for (ActionOfFlow actionOfFlow : selectedFlowTemplates.getActionOfFlows()) {
                    if (mapGroupActionDeclare.containsKey(actionOfFlow.getStepNum())) {
                        checkRefenderParam = mapGroupActionDeclare.get(actionOfFlow.getStepNum());
                    }
                    List<ActionDetail> actionDetails = (List<ActionDetail>) caches.get("ActionDetail#" + actionOfFlow.getAction().getActionId() + "#" + node.getVendor().getVendorId() + "#"
                            + node.getVersion().getVersionId() + "#" + node.getNodeType().getTypeId());
                    if (actionDetails == null) {
                        actionDetails = new ActionDetailServiceImpl().findList("from ActionDetail where action.actionId =? and vendor.vendorId = ? "
                                        + "and version.versionId =? and nodeType.typeId = ?", -1, -1, actionOfFlow.getAction().getActionId(), node.getVendor().getVendorId(),
                                node.getVersion().getVersionId(), node.getNodeType().getTypeId());
                        caches.put("ActionDetail#" + actionOfFlow.getAction().getActionId() + "#" + node.getVendor().getVendorId() + "#"
                                + node.getVersion().getVersionId() + "#" + node.getNodeType().getTypeId(), actionDetails);
                    }
                    for (ActionDetail actionDetail : actionDetails) {
//						if(actionDetail.getVendor().equals(node.getVendor()) &&
//								actionDetail.getVersion().equals(node.getVersion()) &&
//								actionDetail.getNodeType().equals(node.getNodeType()))
                        {
                            List<ActionCommand> actionCommands = (List<ActionCommand>) caches.get("ActionCommand#" + actionDetail.getDetailId() + "#" + node.getVendor().getVendorId() + "#"
                                    + node.getVersion().getVersionId() + "#" + node.getNodeType().getTypeId());
                            if (actionCommands == null) {
                                actionCommands = new ActionCommandServiceImpl().findList("from ActionCommand where actionDetail.detailId = ? and "
                                                + "commandDetail.vendor.vendorId =? and commandDetail.version.versionId =? ", -1, -1, actionDetail.getDetailId(), node.getVendor().getVendorId(),
                                        node.getVersion().getVersionId());
                                caches.put("ActionCommand#" + actionDetail.getDetailId() + "#" + node.getVendor().getVendorId() + "#"
                                        + node.getVersion().getVersionId() + "#" + node.getNodeType().getTypeId(), actionCommands);
                            }
                            List<Long> commandDetailIds = new ArrayList<>();
                            String commandDetailIdString = "";
                            for (ActionCommand actionCommand : actionCommands) {
                                CommandDetail commandDetail = actionCommand.getCommandDetail();
                                //20172610_quytv7_getCommandParam in node. Nghiep vu GetParam from NCMS start
                                listCommandDetailInNode.add(commandDetail);
                                //20172610_quytv7_getCommandParam in node. Nghiep vu GetParam from NCMS end
                                commandDetailIds.add(commandDetail.getCommandDetailId());
                                commandDetailIdString += "#" + commandDetail.getCommandDetailId();
                            }
                            Map<String, Collection<?>> map = new HashMap<>();
                            map.put("commandDetailIds", commandDetailIds);
                            List<ParamInput> paramInputs = (List<ParamInput>) caches.get("ParamInput#-" + commandDetailIdString);
                            if (paramInputs == null) {
                                if (commandDetailIds.size() > 0) {
                                    paramInputs = new ParamInputServiceImpl().findListWithIn("from ParamInput where commandDetail.commandDetailId in (:commandDetailIds)", -1, -1, map);
                                } else {
                                    paramInputs = new ArrayList<>();
                                }
                                caches.put("ParamInput#-" + commandDetailIdString, paramInputs);
                            }
                            Multimap<CommandDetail, ParamInput> mapParamInputs = ArrayListMultimap.create();
                            for (ParamInput paramInput2 : paramInputs) {
                                mapParamInputs.put(paramInput2.getCommandDetail(), paramInput2);
                            }
                            _mapCommandParamInput.putAll(mapParamInputs);
                            if (mapParamInputs.size() > 0) {
                                for (ActionCommand actionCommand : actionCommands) {
                                    CommandDetail commandDetail = actionCommand.getCommandDetail();
//								if(//commandDetail.getNodeType().getTypeId().equals(node.getNodeType().getTypeId()) &&
//										commandDetail.getVendor().getVendorId().equals(node.getVendor().getVendorId()) &&
//										commandDetail.getVersion().equals(node.getVersion()))
                                    Collection<ParamInput> collection = mapParamInputs.get(commandDetail);
                                    if (collection.size() > 0) {
                                        for (ParamInput paramInputTemp : collection) {
                                            try {
                                                ParamInput paramInput = (ParamInput) BeanUtils.cloneBean(paramInputTemp);
                                                if (paramInput.getParamGroups().size() > 0) {
                                                    for (ParamGroup paramGroup : paramInput.getParamGroups()) {
                                                        if (paramGroup.getFlowTemplates().equals(selectedFlowTemplates)) {
                                                            paramInput.setParamDefault(paramGroup.getParamDefault());
                                                            if (paramGroup.getGroupCode() != null) {
                                                                paramInput.setColor(Config.getCOLORS()[Math.min(paramGroup.getGroupCode().intValue(), Config.getCOLORS().length - 1)]);
                                                            }

                                                            if (paramGroup.getGroupCode() != null) {
                                                                mapParamInputGroupCode.put(paramInput.getParamInputId(), paramGroup.getGroupCode());
                                                            }
                                                        }
                                                    }
                                                }
                                                boolean isInOutCmd = false;
                                                String inOutCmd = null;
                                                for (ParamInOut paramInOut : paramInput.getParamInOuts()) {
                                                    if (paramInOut.getActionCommandByActionCommandInputId().equals(actionCommand)
                                                            && paramInOut.getActionOfFlowByActionFlowInId().equals(actionOfFlow)) {
                                                        isInOutCmd = true;
                                                        inOutCmd = paramInOut.getActionCommandByActionCommandOutputId().getCommandDetail().getCommandTelnetParser().getCmd()
                                                                + " (" + paramInOut.getActionOfFlowByActionFlowOutId().getAction().getName() + ")";
                                                        break;
                                                    }

                                                    /*
                                                     for (ActionCommand actionCommand2 : actionDetail.getActionCommands()) {
                                                     if(paramInOut.getActionCommandByActionCommandInputId().equals(actionCommand2)){
                                                     isInOutCmd = true;
                                                     inOutCmd = actionCommand2.getCommandDetail().getCommandTelnetParser().getCmd();
                                                     break;
                                                     }
                                                     }
                                                     */
                                                    if (isInOutCmd) {
                                                        break;
                                                    }
                                                }

                                                paramInput.setInOut(isInOutCmd);
                                                paramInput.setCmdInOut(inOutCmd);
                                                paramInput.setIsDeclare(checkRefenderParam);
                                                if (paramInput.getActionOfFolowId() != null) {
                                                    paramInput.setActionOfFolowId(paramInput.getActionOfFolowId() + "#" + actionOfFlow.getStepNum().toString());
                                                } else {
                                                    paramInput.setActionOfFolowId(actionOfFlow.getStepNum().toString());
                                                }

                                                inputs.add(paramInput);
                                                //hienhv4_20170628_add_start
                                                if (isInOutCmd) {
                                                    if (mapTotalRef.containsKey(paramInput)) {
                                                        mapTotalRef.put(paramInput, mapTotalRef.get(paramInput) + 1);
                                                    } else {
                                                        mapTotalRef.put(paramInput, 1);
                                                    }

                                                    if (!mapRefActionFlowId.containsKey(paramInput)) {
                                                        mapRefActionFlowId.put(paramInput, new ArrayList<Long>());
                                                    }
                                                    mapRefActionFlowId.get(paramInput).add(actionOfFlow.getStepNum());
                                                }

                                                if (mapTotal.containsKey(paramInput)) {
                                                    mapTotal.put(paramInput, mapTotal.get(paramInput) + 1);
                                                } else {
                                                    mapTotal.put(paramInput, 1);
                                                }
                                                //hienhv4_20170628_add_end
                                            } catch (Exception ex) {
                                                logger.error(ex.getMessage(), ex);
                                            }
                                        }

                                    }

                                }
                            }
                        }
                    }
                }
            }
            List<ParamValue> paramValues = new LinkedList<>();
            int colorIndex = 0;
            Map<String, String> mapColor = new HashMap<>();
            for (ParamInput paramInput : inputs) {
//                if (paramInput.getIsDeclare()) {
//                    System.out.println("True_" + node.getNodeCode() + "_" + paramInput.getActionOfFolowId() + "_" + paramInput.getParamCode());
//                } else {
//                    System.out.println("False_" + node.getNodeCode() + "_" + paramInput.getActionOfFolowId() + "_" + paramInput.getParamCode());
//                }
                try {
                    ParamValue paramValue = new ParamValue();
                    paramValue.setParamInput(paramInput);
                    paramValue.setParamCode(paramInput.getParamCode());
                    //paramValue.setFlowRunAction(flowRunAction);
                    paramValue.setNodeRun(new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), subFlowRun), flowRunAction, node, mapSubFlowRunNum.get(subFlowRun)));
                    paramInput.setRelTotal(Objects.equals(mapTotalRef.get(paramInput), mapTotal.get(paramInput)));
                    paramInput.setRefActionFlowIds(mapRefActionFlowId.get(paramInput));
                    if (mapColor.containsKey(paramInput.getParamCode())) {
                        paramInput.setColor(mapColor.get(paramInput.getParamCode()));
                    } else {
                        String color = Config.getCOLORS()[colorIndex >= Config.getCOLORS().length ? Config.getCOLORS().length - 1 : colorIndex];
                        mapColor.put(paramInput.getParamCode(), color);
                        paramInput.setColor(color);
                        colorIndex++;
                    }

                    paramValue.setParamValue(paramInput.getParamDefault());
                    if (paramValue.getParamCode().equals(PARAMCODE.INTERFACE_UPLINK.value)) {
                        String temp = "";
                        if (node.getLstInterface() != null) {
                            for (String string : node.getLstInterface()) {
                                temp += string + ";";
                            }
                        }
                        temp = temp.replaceAll(";$", "");
                        paramValue.setParamValue(temp);
                    }
                    Long groupCode = mapParamInputGroupCode.get(paramInput.getParamInputId());
                    paramValue.setGroupCode(groupCode);
                    if (groupCode != null) {
                        _mapParamValueGroup.put(groupCode, paramValue);
                    }
                    paramValues.add(paramValue);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            for (ParamValue paramValue2 : paramValues) {
                loadParam(paramValue2, paramValues);
            }
//			createActionWithMultiParam(paramValues);

            mapParamValue.put(subFlowRun + "#" + node.getNodeCode(), paramValues);
            mapParamValueGroup.put(subFlowRun + "#" + node.getNodeCode(), _mapParamValueGroup);
            mapCommandParamInput.put(subFlowRun + "#" + node.getNodeCode(), _mapCommandParamInput);
            mapNodeCommandDetail.put(subFlowRun + "#" + node.getNodeCode(), listCommandDetailInNode);
        } else {
//            logger.info("------getParamInputs in map------");
        }
        return mapParamValue.get(subFlowRun + "#" + node.getNodeCode());
    }

    public List<ParamValue> distinctParamValueSameParamCode(String subFlowRun, Node node, Map<String, List<ParamValue>> mapParamValue) {
        return distinctParamValueSameParamCode(subFlowRun, node, mapParamValue.get(subFlowRun + "#" + node.getNodeCode()));
    }

    public List<ParamValue> distinctParamValueSameParamCode(String subFlowRun, Node node, List<ParamValue> paramValues) {
        if (subFlowRun == null || subFlowRun.isEmpty()) {
            subFlowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
//        boolean checkIsRun = false;
//        if (!mapParamInputsDistinct.containsKey(subFlowRun + "#" + node.getNodeCode())) {
        List<ParamValue> _paramValues = new ArrayList<>();
//            logger.info("distinctParamValueSameParamCode paramValues size: " + paramValues.size());
        try {
            Set<String> _tmp = new HashSet<>();
            Map<String, String> _tmpFormula = new HashMap<>();
            Map<String, String> _tmpDescription = new HashMap<>();

            Map<CommandDetail, Boolean> mapCommand = new HashMap<>();
            for (ParamValue paramValue : paramValues) {
                if (!mapCommand.containsKey(paramValue.getParamInput().getCommandDetail())) {
                    mapCommand.put(paramValue.getParamInput().getCommandDetail(), Boolean.TRUE);
                }
            }

            for (CommandDetail command : mapCommand.keySet()) {
                command.calculateParam(paramValues, logger);
            }

            for (ParamValue paramValue : paramValues) {
                String paramCode = paramValue.getParamInput().getParamCode();
                if (!_tmp.contains(paramCode)) {
                    paramValue.setIsDeclare(paramValue.getParamInput().getIsDeclare());
                    paramValue.setDisableByInOut(paramValue.getParamInput().isRelTotal());
                    _paramValues.add(paramValue);
                    _tmp.add(paramCode);
                } else {
                    if (paramValue.getParamInput().getInOut()) {
                        for (ParamValue paramValue2 : _paramValues) {
                            if (paramValue.getParamInput().equals(paramValue2.getParamInput())) {
                                paramValue2.getParamInput().setInOut(true);
                            }
                        }
                    }

                    if (!paramValue.getParamInput().isRelTotal()) {
                        for (ParamValue paramValue2 : _paramValues) {
                            if (paramValue.getParamInput().getParamCode().equals(paramValue2.getParamInput().getParamCode())) {
                                paramValue2.setDisableByInOut(false);
                            }
                        }
                    }

                    if (paramValue.getParamInput().getIsDeclare()) {
                        for (ParamValue paramValue2 : _paramValues) {
                            if (paramValue.getParamInput().getParamCode().equals(paramValue2.getParamInput().getParamCode())) {
                                paramValue2.setIsDeclare(true);
                            }
                        }
                    }
                }
                if (paramValue.getParamInput().getIsFormula()) {
                    _tmpFormula.put(paramCode, paramValue.getParamInput().getParamFormula());
                }
                if (paramValue.getParamInput().getDescription() != null
                        && !paramValue.getParamInput().getDescription().trim().isEmpty()) {
                    _tmpDescription.put(paramCode, paramValue.getParamInput().getDescription());
                }
            }

            for (ParamValue paramValue : _paramValues) {
                if (_tmpFormula.containsKey(paramValue.getParamInput().getParamCode())) {
                    if (paramValue.getParamValue() == null || paramValue.getParamValue().trim().isEmpty()) {
                        paramValue.setFormula(_tmpFormula.get(paramValue.getParamInput().getParamCode()));
                    }
                }
                if (_tmpDescription.containsKey(paramValue.getParamInput().getParamCode())) {
                    paramValue.setDescription(_tmpDescription.get(paramValue.getParamInput().getParamCode()));
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        mapParamInputsDistinct.put(subFlowRun + "#" + node.getNodeCode(), _paramValues);
//        } else {
//            logger.info("distinctParamValueSameParamCode has in map");
//        }
        return mapParamInputsDistinct.get(subFlowRun + "#" + node.getNodeCode());
    }

    //20171018_hienhv4_clone dau viec_start
    public void checkOffParam(String subFlowRun, Node node, GroupAction currGroup) {
        //20171018_hienhv4_clone dau viec_end
        HashMap<String, Boolean> mapGroupActionDeclare = new HashMap<>();
        List<GroupAction> lstGroupAction = mapGroupAction.get(subFlowRun + "#" + node.getNodeCode());

        //20171018_hienhv4_clone dau viec_start
        for (GroupAction groupAction : lstGroupAction) {
            if (groupAction.getGroupActionName().equals(currGroup.getGroupActionName())) {
                groupAction.setDeclare(currGroup.isDeclare());
            }
        }
        //20171018_hienhv4_clone dau viec_end

        for (GroupAction groupAction : lstGroupAction) {
            for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                mapGroupActionDeclare.put(actionOfFlow.getStepNum().toString(), groupAction.isDeclare());
            }
        }
        String[] actionOfFolowIds;
        for (ParamValue paramValue : mapParamValue.get(subFlowRun + "#" + node.getNodeCode())) {
            if (paramValue.getParamInput().getActionOfFolowId() != null) {
                actionOfFolowIds = paramValue.getParamInput().getActionOfFolowId().split("#");
                for (String te : actionOfFolowIds) {
                    if (mapGroupActionDeclare.containsKey(te) && mapGroupActionDeclare.get(te)) {
                        paramValue.getParamInput().setIsDeclare(true);
                        break;
                    } else {
                        paramValue.getParamInput().setIsDeclare(false);
                    }
                }
            }
        }
    }

    public void copyValue(ParamValue paramValue, List<ParamValue> paramValues, List<ParamValue> paramValueHasSetups) {
        if (paramValueHasSetups.size() == paramValues.size()) {
            return;
        }
        if (paramValue != null) {
            for (ParamValue paramValue3 : paramValues) {
                if (paramValue.getParamInput().getParamCode().equals(paramValue3.getParamInput().getParamCode())) {
                    paramValue3.setParamValue(paramValue.getParamValue());
                    paramValueHasSetups.add(paramValue3);
                }
            }
        }
    }

    public void onKeyUpValueParam(ParamValue paramValue, String subFlowRun, Node node, Map<String, List<ParamValue>> mapParamValue) {
        paramValue.setParamValue(Jsoup.parse(paramValue.getParamValue().trim().replaceAll("(?i)<(/?script[^>]*)>", "")).text());
        onKeyUpValueParam(paramValue, mapParamValue.get(subFlowRun + "#" + node.getNodeCode()));
    }

    public void onKeyUpValueParam(ParamValue paramValue, List<ParamValue> paramValues) {
        if (selectSubflowRun == null || "".equals(selectSubflowRun)) {
            selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        loadParam(paramValue, paramValues);
//        long startTime = System.currentTimeMillis();
        createActionWithMultiParam(paramValues);
//        logger.info("time: " + (System.currentTimeMillis() - startTime) / 1000);
    }

    public void loadParamOnce() {
//        System.err.println("aaa");
        List<ParamValue> paramValues = getParamInputs(selectSubflowRun, selectedNode);
        for (ParamValue paramValue : paramValues) {
            loadParam(paramValue, paramValues);
        }
        createActionWithMultiParam(paramValues);
        RequestContext.getCurrentInstance().execute("PF('loadparam').stop()");
    }

    //Quytv7 Them moi dung cho phan create mop with muilti param save on file put OS
    public void loadMultiParam() {
        List<ParamValue> paramValues = getParamInputs(selectSubflowRun, selectedNode);
        for (ParamValue paramValue : paramValues) {
            loadParam(paramValue, paramValues);
        }
        createActionWithMultiParam(paramValues);

    }

    private void loadParam(ParamValue paramValue, List<ParamValue> paramValues) {
        if (paramValue != null) {
            for (ParamValue paramValue3 : paramValues) {
                if (paramValue.getParamInput().getParamCode().equals(paramValue3.getParamInput().getParamCode())) {
                    if (!paramValue3.getParamInput().getInOut()) {
                        paramValue3.getParamInput().setInOut(paramValue.getParamInput().getInOut());
                    }
                    paramValue3.setParamValue(paramValue.getParamValue());

                    if (paramValue3.getGroupCode() != null) {
                        for (ParamValue paramValue5 : paramValues) {
                            if (paramValue5.getGroupCode() != null && paramValue5.getGroupCode().equals(paramValue3.getGroupCode())) {
                                paramValue5.setParamValue(paramValue.getParamValue());
                                if (!paramValue5.getParamInput().getInOut()) {
                                    paramValue5.getParamInput().setInOut(paramValue.getParamInput().getInOut());
                                }
                                for (ParamValue paramValue4 : paramValues) {
                                    if (paramValue5.getParamInput().getParamCode().equals(paramValue4.getParamInput().getParamCode())) {
                                        paramValue4.setParamValue(paramValue5.getParamValue());
                                        if (!paramValue4.getParamInput().getInOut()) {
                                            paramValue4.getParamInput().setInOut(paramValue.getParamInput().getInOut());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (paramValue.getGroupCode() != null) {
                for (ParamValue paramValue5 : paramValues) {
                    if (paramValue5.getGroupCode() != null && paramValue5.getGroupCode().equals(paramValue.getGroupCode())) {
                        paramValue5.setParamValue(paramValue.getParamValue());
                        for (ParamValue paramValue4 : paramValues) {
                            if (paramValue5.getParamInput().getParamCode().equals(paramValue4.getParamInput().getParamCode())) {
                                paramValue4.setParamValue(paramValue5.getParamValue());
                            }
                        }
                    }
                }
            }

        }
    }

    private void createActionWithMultiParam(List<ParamValue> paramValues) {
        createActionWithMultiParamNew(paramValues, selectedNode);
    }

    private void createActionWithMultiParamNew(List<ParamValue> paramValues, Node selectedNode) {
        if (selectSubflowRun == null || selectSubflowRun.isEmpty()) {
            selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        if (selectedNode == null) {
            return;
        }
        List<GroupAction> lstGroupAction = selectedNode.getMapGroupActions().get(selectSubflowRun + "#" + selectedNode.getNodeCode());
        if (lstGroupAction != null) {
            for (Iterator<GroupAction> iterator = selectedNode.getMapGroupActions().get(selectSubflowRun + "#" + selectedNode.getNodeCode()).iterator(); iterator.hasNext(); ) {
                GroupAction groupAction = iterator.next();
                if (groupAction.getCloneNumber() != null && groupAction.getCloneNumber() != 0) {
                    iterator.remove();
                }
                for (Iterator<ActionOfFlow> iterator1 = groupAction.getActionOfFlows().iterator(); iterator1.hasNext(); ) {
                    ActionOfFlow actionOfFlow = iterator1.next();
                    if (actionOfFlow.getIndexParamValue() != null && actionOfFlow.getIndexParamValue() != 0) {
                        iterator1.remove();
                    }
                }
            }
        } else {
            return;
        }

        //20171018_hienhv4_clone dau viec_start
        Map<String, Boolean> mapGroupClone = new HashMap<>();
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("flowTemplateId", flowRunAction.getFlowTemplates().getFlowTemplatesId());
            List<GroupActionClone> groupClones = new GroupActionCloneServiceImpl().findList(filters);

            if (groupClones != null) {
                for (GroupActionClone group : groupClones) {
                    mapGroupClone.put(group.getGroupName(), group.getClone());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        //20171018_hienhv4_clone dau viec_end

        List<Long> acfIdAlls = new ArrayList<>();
        for (GroupAction groupAction : lstGroupAction) {
            //20171018_hienhv4_clone dau viec_start
            if (mapGroupClone.containsKey(groupAction.getGroupActionName())) {
                groupAction.setClone(mapGroupClone.get(groupAction.getGroupActionName()));
            } else {
                groupAction.setClone(false);
            }
            //20171018_hienhv4_clone dau viec_end
            for (ActionOfFlow actionOfFlow2 : groupAction.getActionOfFlows()) {
                acfIdAlls.add(actionOfFlow2.getAction().getActionId());
            }
        }
        int SIZE = 950;
        int countPr = acfIdAlls.size() / SIZE + 1;
        String where = "";
        Map<String, Object> paramlist = new HashMap<>();
        for (int i = 0; i < countPr; i++) {
            if (i == 0) {
                where += " a.ACTION_ID in (:actionIds" + i + ")";
            } else {
                where += " or a.ACTION_ID in (:actionIds" + i + ")";
            }
            int max = (i + 1) * SIZE - 1;
            paramlist.put("actionIds" + i, acfIdAlls.subList(i * SIZE, max >= acfIdAlls.size() ? acfIdAlls.size() : max));
        }
        String sqlAll = "SELECT DISTINCT a.action_id,pi.PARAM_INPUT_ID "
                + "FROM Action a "
                + "JOIN Action_Detail ad "
                + "ON a.ACTION_ID=ad.ACTION_ID "
                + "JOIN Action_Command ac "
                + "ON ad.DETAIL_ID = ac.ACTION_DETAIL_ID "
                + "JOIN Command_Detail cd "
                + "ON cd.COMMAND_DETAIL_ID = ac.COMMAND_DETAIL_ID "
                + "JOIN Param_Input pi "
                + "ON pi.CMD_DETAIL_ID = cd.COMMAND_DETAIL_ID ";
                
        if (!where.trim().isEmpty()) {
            sqlAll += "WHERE" + where;
        }
        
        List<?> paramInputAlls = new ArrayList<>();
        if (!acfIdAlls.isEmpty()) {
            paramInputAlls = new ParamInputServiceImpl().findListSQLWithMapParameters(null, sqlAll, -1, -1, paramlist);
        }
        
        Multimap<String, Object> mulTemp = ArrayListMultimap.create();
        for (Object object : paramInputAlls) {
            Object[] cols = (Object[]) object;
            mulTemp.put("ParamInput##" + cols[0], cols[1]);
        }

        int i = 0;
        while (i < lstGroupAction.size()) {
            GroupAction groupAction = lstGroupAction.get(i);
            List<ActionOfFlow> actionOfFlows = groupAction.getActionOfFlows();
            if (actionOfFlows.isEmpty()) {
                i++;
                continue;
            }
            List<Long> acfIds = new ArrayList<>();
            for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
                acfIds.add(actionOfFlow2.getAction().getActionId());
            }
            for (Long acfId : acfIds) {
                String key = "ParamInput##" + acfId;
                if (mulTemp.containsKey(key)) {
                    caches.put(key, mulTemp.get(key));
                } else {
                    caches.put(key, new ArrayList<>());
                }
            }

            if (groupAction.isClone()) {
                List<Object> paramInputsOfGroup = new ArrayList<>();
                int maxValue = 1;
                for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                    String sql = "SELECT DISTINCT pi.PARAM_INPUT_ID "
                            + "FROM Action a "
                            + "JOIN Action_Detail ad "
                            + "ON a.ACTION_ID=ad.ACTION_ID "
                            + "JOIN Action_Command ac "
                            + "ON ad.DETAIL_ID = ac.ACTION_DETAIL_ID "
                            + "JOIN Command_Detail cd "
                            + "ON cd.COMMAND_DETAIL_ID = ac.COMMAND_DETAIL_ID "
                            + "JOIN Param_Input pi "
                            + "ON pi.CMD_DETAIL_ID = cd.COMMAND_DETAIL_ID "
                            + "JOIN Param_Value pv "
                            + "ON pv.PARAM_INPUT_ID = pi.PARAM_INPUT_ID "
                            + "WHERE a.ACTION_ID    = ?";
                    List<?> paramInputs = (List<?>) caches.get("ParamInput##" + actionOfFlow.getAction().getActionId());
                    if (paramInputs == null) {
                        paramInputs = new ParamInputServiceImpl().findListSQLAll(sql, actionOfFlow.getAction().getActionId());
                        caches.put("ParamInput##" + actionOfFlow.getAction().getActionId(), paramInputs);
                    }
                    paramInputsOfGroup.addAll(paramInputs);
                }

                for (Object paramInput : paramInputsOfGroup) {
                    for (ParamValue paramValue2 : paramValues) {
                        if (paramInput.toString().equals(paramValue2.getParamInput().getParamInputId().toString())) {
                            if (paramValue2.getParamValue() != null) {
                                maxValue = Math.max(maxValue, paramValue2.getParamValue().split(Config.SPLITTER_VALUE, -1).length);
                            }
                        }
                    }
                }

                if (maxValue == 1 || !groupAction.isClone()) {
                    i++;
                    continue;
                }

                int realClone = 1;
                List<GroupAction> groupActionList = new ArrayList<>();
                groupActionList.add(groupAction);
                for (int j = 1; j < maxValue; j++) {
                    try {
                        GroupAction groupActionClone = (GroupAction) BeanUtils.cloneBean(groupAction);
                        groupActionClone.setCloneNumber(j);

                        lstGroupAction.add(Math.min(i + realClone, lstGroupAction.size()), groupActionClone);
                        groupActionList.add(groupActionClone);
                        realClone++;
                    } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                }
                for (GroupAction a : groupActionList) {
                    a.setCloneTotal(realClone);
                }
                i = i + realClone;
            } else {
                int k = 0;
                while (k < actionOfFlows.size()) {
                    ActionOfFlow actionOfFlow = actionOfFlows.get(k);
                    
//                    logger.info("---------------log nua ne: " + actionOfFlow.getAction().getName());
                    Map<String, Boolean> mapcloneCommand = new HashMap<>();
                    //20170824_hienhv4_add_start
                    if (actionOfFlow.getRemoveDuplicate() > 0) {
                        mapcloneCommand.put(actionOfFlow.buildCommand(paramValues, false, selectedNode, (this.flowRunAction == null ? 0L : (flowRunAction.getTimeLoadParam() == null ? 0L : 1L))), Boolean.TRUE);
                    }
                    //20170824_hienhv4_add_end
                    int maxValue = 1;

                    String sql = "SELECT DISTINCT pi.PARAM_INPUT_ID "
                            + "FROM Action a "
                            + "JOIN Action_Detail ad "
                            + "ON a.ACTION_ID=ad.ACTION_ID "
                            + "JOIN Action_Command ac "
                            + "ON ad.DETAIL_ID = ac.ACTION_DETAIL_ID "
                            + "JOIN Command_Detail cd "
                            + "ON cd.COMMAND_DETAIL_ID = ac.COMMAND_DETAIL_ID "
                            + "JOIN Param_Input pi "
                            + "ON pi.CMD_DETAIL_ID = cd.COMMAND_DETAIL_ID "
                            + "JOIN Param_Value pv "
                            + "ON pv.PARAM_INPUT_ID = pi.PARAM_INPUT_ID "
                            + "WHERE a.ACTION_ID    = ?";
                    List<?> paramInputs = (List<?>) caches.get("ParamInput##" + actionOfFlow.getAction().getActionId());
                    if (paramInputs == null) {
                        paramInputs = new ParamInputServiceImpl().findListSQLAll(sql, actionOfFlow.getAction().getActionId());
                        caches.put("ParamInput##" + actionOfFlow.getAction().getActionId(), paramInputs);
                    }

                    for (Object paramInput : paramInputs) {
                        for (ParamValue paramValue2 : paramValues) {
                            if (paramInput.toString().equals(paramValue2.getParamInput().getParamInputId().toString())) {
                                if (paramValue2.getParamValue() != null) {
                                    maxValue = Math.max(maxValue, paramValue2.getParamValue().split(Config.SPLITTER_VALUE, -1).length);
                                }
                            }
                        }
                    }
                    if (maxValue == 1) {
                        k++;
                        continue;
                    }
                    int realClone = 1;
                    List<ActionOfFlow> actionFlows = new ArrayList<>();
                    actionFlows.add(actionOfFlow);
                    for (int j = 1; j < maxValue; j++) {
                        try {
                            ActionOfFlow actionOfFlowClone = (ActionOfFlow) BeanUtils.cloneBean(actionOfFlow);//actionOfFlow.deepClone();
                            actionOfFlowClone.setIndexParamValue(j);

                            String commandSet = actionOfFlowClone.buildCommand(paramValues, false, selectedNode, (this.flowRunAction == null ? 0L : (flowRunAction.getTimeLoadParam() == null ? 0L : 1L)));
                            //20170824_hienhv4_add_start
                            if (actionOfFlow.getRemoveDuplicate() > 0) {
                                if (mapcloneCommand.containsKey(commandSet)) {
                                    continue;
                                }
                                mapcloneCommand.put(commandSet, Boolean.TRUE);
                            }
                            //20170824_hienhv4_add_end
                            actionOfFlows.add(Math.min(k + realClone, actionOfFlows.size()), actionOfFlowClone);
                            actionFlows.add(actionOfFlowClone);
                            realClone++;
                        } catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException ex) {
                            logger.error(ex.getMessage(), ex);
                        }
                    }
                    for (ActionOfFlow a : actionFlows) {
                        a.setTotalIndexParamvalue(realClone);
                    }
                    k = k + realClone;
//                    logger.info("---------------finish log nua ne: " + actionOfFlow.getAction().getName());
                }
                i++;
            }
        }
    }

    private void createActionWithMultiParam(List<ParamValue> paramValues, Node selectedNode) {
        if (selectSubflowRun == null || selectSubflowRun.isEmpty()) {
            selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        if (selectedNode != null && selectedNode.getMapGroupActions().get(selectSubflowRun + "#" + selectedNode.getNodeCode()) != null) {
            for (GroupAction groupAction : selectedNode.getMapGroupActions().get(selectSubflowRun + "#" + selectedNode.getNodeCode())) {
                for (Iterator<ActionOfFlow> iterator = groupAction.getActionOfFlows().iterator(); iterator.hasNext(); ) {
                    ActionOfFlow actionOfFlow = iterator.next();
                    if (actionOfFlow.getIndexParamValue() != null && actionOfFlow.getIndexParamValue() != 0) {
                        iterator.remove();
                    }
                }
            }

            List<Long> acfIdAlls = new ArrayList<>();
            for (GroupAction groupAction : selectedNode.getMapGroupActions().get(selectSubflowRun + "#" + selectedNode.getNodeCode())) {
                for (ActionOfFlow actionOfFlow2 : groupAction.getActionOfFlows()) {
                    acfIdAlls.add(actionOfFlow2.getAction().getActionId());
                }
            }
            int SIZE = 950;
            int countPr = acfIdAlls.size() / SIZE + 1;
            String where = "";
            Map<String, Object> paramlist = new HashMap<>();
            for (int i = 0; i < countPr; i++) {
                if (i == 0) {
                    where += " a.ACTION_ID in (:actionIds" + i + ")";
                } else {
                    where += " or a.ACTION_ID in (:actionIds" + i + ")";
                }
                int max = (i + 1) * SIZE - 1;
                paramlist.put("actionIds" + i, acfIdAlls.subList(i * SIZE, max >= acfIdAlls.size() ? acfIdAlls.size() : max));
            }
            String sqlAll = "SELECT DISTINCT a.action_id,pi.PARAM_INPUT_ID "
                    + "FROM Action a "
                    + "JOIN Action_Detail ad "
                    + "ON a.ACTION_ID=ad.ACTION_ID "
                    + "JOIN Action_Command ac "
                    + "ON ad.DETAIL_ID = ac.ACTION_DETAIL_ID "
                    + "JOIN Command_Detail cd "
                    + "ON cd.COMMAND_DETAIL_ID = ac.COMMAND_DETAIL_ID "
                    + "JOIN Param_Input pi "
                    + "ON pi.CMD_DETAIL_ID = cd.COMMAND_DETAIL_ID "
                    + "WHERE" + where;
            List<?> paramInputAlls = new ParamInputServiceImpl().findListSQLWithMapParameters(null, sqlAll, -1, -1, paramlist);
            Multimap<String, Object> mulTemp = ArrayListMultimap.create();
            for (Object object : paramInputAlls) {
                Object[] cols = (Object[]) object;
                mulTemp.put("ParamInput##" + cols[0], cols[1]);
            }

            for (GroupAction groupAction : selectedNode.getMapGroupActions().get(selectSubflowRun + "#" + selectedNode.getNodeCode())) {
//                logger.info("log ne: " + groupAction.getGroupActionName());
                List<ActionOfFlow> actionOfFlows = groupAction.getActionOfFlows();
                if (actionOfFlows.isEmpty()) {
                    continue;
                }
                List<Long> acfIds = new ArrayList<>();
//                Multimap<String, Object> mulTemp = ArrayListMultimap.create();
                for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
                    acfIds.add(actionOfFlow2.getAction().getActionId());
                }
//
//                String sqlAll = "SELECT DISTINCT a.action_id,pi.PARAM_INPUT_ID "
//                        + "FROM Action a "
//                        + "JOIN Action_Detail ad "
//                        + "ON a.ACTION_ID=ad.ACTION_ID "
//                        + "JOIN Action_Command ac "
//                        + "ON ad.DETAIL_ID = ac.ACTION_DETAIL_ID "
//                        + "JOIN Command_Detail cd "
//                        + "ON cd.COMMAND_DETAIL_ID = ac.COMMAND_DETAIL_ID "
//                        + "JOIN Param_Input pi "
//                        + "ON pi.CMD_DETAIL_ID = cd.COMMAND_DETAIL_ID "
//                        //+ "JOIN Param_Value pv "
//                        //+ "ON pv.PARAM_INPUT_ID = pi.PARAM_INPUT_ID "
//                        + "WHERE a.ACTION_ID  in (:actionIds)";
//                Map<String, Object> paramlist = new HashMap<>();
//                paramlist.put("actionIds", acfIds);
//                List<?> paramInputAlls = new ParamInputServiceImpl().findListSQLWithMapParameters(null, sqlAll, -1, -1, paramlist);
//
//                for (Object object : paramInputAlls) {
//                    Object[] cols = (Object[]) object;
//                    mulTemp.put("ParamInput##" + cols[0], cols[1]);
//                }
                for (Long acfId : acfIds) {
                    String key = "ParamInput##" + acfId;
                    if (mulTemp.containsKey(key)) {
                        caches.put(key, mulTemp.get(key));
                    } else {
                        caches.put(key, new ArrayList<>());
                    }
                }

                int i = 0;
                while (i < actionOfFlows.size()) {
                    ActionOfFlow actionOfFlow = actionOfFlows.get(i);
//                    logger.info("---------------log nua ne: " + actionOfFlow.getAction().getName());
                    Map<String, Boolean> mapcloneCommand = new HashMap<>();
                    //20170824_hienhv4_add_start
                    if (actionOfFlow.getRemoveDuplicate() > 0) {
                        mapcloneCommand.put(actionOfFlow.buildCommand(paramValues, false, selectedNode, (this.flowRunAction == null ? 0L : (flowRunAction.getTimeLoadParam() == null ? 0L : 1L))), Boolean.TRUE);
                    }
                    //20170824_hienhv4_add_end
                    int maxValue = 1;

                    String sql = "SELECT DISTINCT pi.PARAM_INPUT_ID "
                            + "FROM Action a "
                            + "JOIN Action_Detail ad "
                            + "ON a.ACTION_ID=ad.ACTION_ID "
                            + "JOIN Action_Command ac "
                            + "ON ad.DETAIL_ID = ac.ACTION_DETAIL_ID "
                            + "JOIN Command_Detail cd "
                            + "ON cd.COMMAND_DETAIL_ID = ac.COMMAND_DETAIL_ID "
                            + "JOIN Param_Input pi "
                            + "ON pi.CMD_DETAIL_ID = cd.COMMAND_DETAIL_ID "
                            + "JOIN Param_Value pv "
                            + "ON pv.PARAM_INPUT_ID = pi.PARAM_INPUT_ID "
                            + "WHERE a.ACTION_ID    = ?";
                    List<?> paramInputs = (List<?>) caches.get("ParamInput##" + actionOfFlow.getAction().getActionId());
                    if (paramInputs == null) {
                        paramInputs = new ParamInputServiceImpl().findListSQLAll(sql, actionOfFlow.getAction().getActionId());
                        caches.put("ParamInput##" + actionOfFlow.getAction().getActionId(), paramInputs);
                    }

                    for (Object paramInput : paramInputs) {
                        for (ParamValue paramValue2 : paramValues) {
                            if (paramInput.toString().equals(paramValue2.getParamInput().getParamInputId().toString())) {
                                if (paramValue2.getParamValue() != null) {
                                    maxValue = Math.max(maxValue, paramValue2.getParamValue().split(Config.SPLITTER_VALUE, -1).length);
                                }
                            }
                        }
                    }
                    if (maxValue == 1) {
                        i++;
                        continue;
                    }
                    int realClone = 1;
                    List<ActionOfFlow> actionFlows = new ArrayList<>();
                    actionFlows.add(actionOfFlow);
                    for (int j = 1; j < maxValue; j++) {
                        try {
                            ActionOfFlow actionOfFlowClone = (ActionOfFlow) BeanUtils.cloneBean(actionOfFlow);//actionOfFlow.deepClone();
                            actionOfFlowClone.setIndexParamValue(j);

                            String commandSet = actionOfFlowClone.buildCommand(paramValues, false, selectedNode, (this.flowRunAction == null ? 0L : (flowRunAction.getTimeLoadParam() == null ? 0L : 1L)));
                            //20170824_hienhv4_add_start
                            if (actionOfFlow.getRemoveDuplicate() > 0) {
                                if (mapcloneCommand.containsKey(commandSet)) {
                                    continue;
                                }
                                mapcloneCommand.put(commandSet, Boolean.TRUE);
                            }
                            //20170824_hienhv4_add_end
                            actionOfFlows.add(Math.min(i + realClone, actionOfFlows.size()), actionOfFlowClone);
                            actionFlows.add(actionOfFlowClone);
                            realClone++;
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                        }
                    }
                    for (ActionOfFlow a : actionFlows) {
                        a.setTotalIndexParamvalue(realClone);
                    }
                    i = i + realClone;
//                    logger.info("---------------finish log nua ne: " + actionOfFlow.getAction().getName());
                }
            }
        }
    }

    //    private void createActionWithMultiParamOld(List<ParamValue> paramValues) {
//        if (selectedNode != null && selectedNode.getGroupActions() != null) {
//            for (GroupAction groupAction : selectedNode.getGroupActions()) {
//                for (Iterator<ActionOfFlow> iterator = groupAction.getActionOfFlows().iterator(); iterator.hasNext();) {
//                    ActionOfFlow actionOfFlow = iterator.next();
//                    if (actionOfFlow.getIndexParamValue() != null && actionOfFlow.getIndexParamValue() != 0) {
//                        iterator.remove();
//                    }
//                }
//            }
//            for (GroupAction groupAction : selectedNode.getGroupActions()) {
//                List<ActionOfFlow> actionOfFlows = groupAction.getActionOfFlows();
//
//                int i = 0;
//                while (i < actionOfFlows.size()) {
//                    ActionOfFlow actionOfFlow = actionOfFlows.get(i);
//                    int maxValue = 1;
//                    for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
//                        for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
//                            for (ParamInput paramInput : actionCommand.getCommandDetail().getParamInputs()) {
//                                for (ParamValue paramValue2 : paramValues) {
//                                    if (paramInput.equals(paramValue2.getParamInput())) {
//                                        if (paramValue2.getParamValue() != null) {
//                                            maxValue = Math.max(maxValue, paramValue2.getParamValue().split(Config.SPLITTER_VALUE, -1).length);
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    //				System.out.println(actionOfFlow);
//                    if (maxValue == 1) {
//                        i++;
//                        continue;
//                    }
//                    actionOfFlow.setTotalIndexParamvalue(maxValue);
//                    for (int j = 1; j < maxValue; j++) {
//                        //ActionOfFlow actionOfFlowClone = new Cloner().deepCloneDontCloneInstances(actionOfFlow, actionOfFlow.getFlowTemplates());
//                        ActionOfFlow actionOfFlowClone = actionOfFlow.deepClone();
//                        actionOfFlowClone.setIndexParamValue(new Integer(j));
//                        actionOfFlowClone.setTotalIndexParamvalue(maxValue);
//                        actionOfFlows.add(Math.min(i + j, actionOfFlows.size()), actionOfFlowClone);
//                        //					System.out.println(actionOfFlowClone);
//                    }
//                    i = i + maxValue;
//                }
//            }
////			for (GroupAction groupAction : selectedNode.getGroupActions()) {
////				for (Iterator<ActionOfFlow> iterator = groupAction.getActionOfFlows().iterator(); iterator.hasNext();) {
////					ActionOfFlow actionOfFlow = iterator.next();
////					System.out.println(actionOfFlow.buildCommand(paramValues, false, selectedNode));;
////				}
////			}
//        }
//
//    }
//    public void handleUnSelectNode() {
//        if (mapSubFlowRunNodes == null) {
//            mapSubFlowRunNodes = new HashMap<>();
//        }
//        if (mapSubFlowRunNodes.get(selectSubflowRun) == null) {
//            mapSubFlowRunNodes.put(selectSubflowRun, new ArrayList<Node>());
//        }
//        mapSubFlowRunNodes.put(selectSubflowRun, new ArrayList<Node>());
//        for (Node node : nodes) {
//            if (!mapSubFlowRunNodes.get(selectSubflowRun).contains(node)) {
//                mapSubFlowRunNodes.get(selectSubflowRun).add(node);
//            }
//        }
//    }
    public void handleSelectNode(SelectEvent event) {
        try {
            Node node = (Node) event.getObject();
            if (loadRingSrt && node.getNodeType().getTypeId().equals(NODE_TYPE.SRT.value)) {

                RelationNodeUtil relationNodeUtil = new RelationNodeUtil();
                String srtNodecode = node.getNodeCode();
                List<Node> nodes = relationNodeUtil.getRingSrt(srtNodecode);
                if (nodes != null && nodes.size() > 0) {
                    if (this.nodes == null) {
                        this.nodes = new ArrayList<Node>();
                    }
                    this.nodes.clear();
                    for (Node node2 : nodes) {
                        this.nodes.add(node2);
                        if (selectSubflowRun == null) {
                            loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node2);
                        } else {
                            loadGroupAction(selectSubflowRun, node2);
                        }
                    }
                } else {
                    if (selectSubflowRun == null) {
                        handleSelectNode(Config.SUB_FLOW_RUN_DEFAULT, node);
                    } else {
                        handleSelectNode(selectSubflowRun, node);
                    }
                }
                RequestContext.getCurrentInstance().execute("PF('tabInputParamWg').select(0)");
            } else {
                if (selectSubflowRun == null) {
                    handleSelectNode(Config.SUB_FLOW_RUN_DEFAULT, node);
                } else {
                    handleSelectNode(selectSubflowRun, node);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void handleUnSelectNode(UnselectEvent event) {
        Node node = (Node) event.getObject();
        if (selectSubflowRun == null || selectSubflowRun.isEmpty()) {
            selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        if (mapSubFlowRunNodes == null) {
            mapSubFlowRunNodes = new HashMap<>();
        }
        if (mapSubFlowRunNodes.get(selectSubflowRun) == null) {
            mapSubFlowRunNodes.put(selectSubflowRun, new ArrayList<Node>());
        }
        if (mapSubFlowRunNodes.get(selectSubflowRun).contains(node)) {
            mapSubFlowRunNodes.get(selectSubflowRun).remove(node);
        }
        if (mapSubFlowRunNodes.get(selectSubflowRun) != null && mapSubFlowRunNodes.get(selectSubflowRun) .size() > 0 && mapSubFlowRunNodes.get(selectSubflowRun).get(0) != null) {
            //20171018_hienhv4_clone dau viec (fix loi load thieu action)_start
            selectedNode = mapSubFlowRunNodes.get(selectSubflowRun).get(mapSubFlowRunNodes.get(selectSubflowRun).size() - 1);
            //20171018_hienhv4_clone dau viec (fix loi load thieu action)_end
            onChangeNodeAfterRemove();
        }

        mapParamValue.remove(selectSubflowRun + "#" + node.getNodeCode());
        mapParamValueGroup.remove(selectSubflowRun + "#" + node.getNodeCode());
        mapGroupAction.remove(selectSubflowRun + "#" + node.getNodeCode());
        tabview.setActiveIndex(mapSubFlowRunNodes.get(selectSubflowRun).size() - 1);
    }

    public void handleSelectNode(String subFlowRun, Node obj) {
        try {
            if (subFlowRun == null || subFlowRun.isEmpty()) {
                subFlowRun = Config.SUB_FLOW_RUN_DEFAULT;
            }
            Node _node = null;
            if (mapSubFlowRunNodes == null) {
                mapSubFlowRunNodes = new HashMap<>();
            }
            if (mapSubFlowRunNodes.get(subFlowRun) == null) {
                mapSubFlowRunNodes.put(subFlowRun, new ArrayList<Node>());
            }

            for (Node node : nodes) {
                if (node.equals(obj)) {
                    _node = node;
                }
            }
            if (_node != null) {
                loadGroupAction(subFlowRun, _node);
                selectedNode = _node;
                if (!mapSubFlowRunNodes.get(subFlowRun).contains(_node)) {
                    mapSubFlowRunNodes.get(subFlowRun).add(_node);
                }
            }
            tabview.setActiveIndex(mapSubFlowRunNodes.get(subFlowRun).size() - 1);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    List<NodeActionOff2> nodeActionOffs = null;

    public void loadGroupAction(String subFlowRun, Node _node) {
        if (subFlowRun == null || subFlowRun.isEmpty()) {
            subFlowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        loadGroupAction(subFlowRun, _node, null);
    }

    public void loadGroupAction(String subFlowRun, Node _node, Long flowRunId) {

        if (subFlowRun == null || subFlowRun.isEmpty()) {
            subFlowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        List<GroupAction> listGroupActions = new ArrayList<>();
//        _node.getGroupActions().clear();
        GroupAction groupAction;
//        _node.setGroupActions(new ArrayList<GroupAction>());
//        logger.info("--------------Get nodeRunGroupActions loadGroupAction--------------");
        List<NodeRunGroupAction> nodeRunGroupActions = new NodeRunGroupActionServiceImpl()
                .findList("from NodeRunGroupAction where id.flowRunId =? and id.nodeId=?", -1, -1,
                        flowRunId, _node.getNodeId());
        for (String groupName : groupActions.keySet()) {
            LinkedList<ActionOfFlow> actionOfFlows = new LinkedList<>(groupActions.get(groupName));

            boolean declare = false;
            if (flowRunId != null) {
                List<Long> actionOfFlowIds = new ArrayList<>();
                for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
                    actionOfFlowIds.add(actionOfFlow2.getStepNum());
                }
//                logger.info("--------------Get nodeRunGroupActions loadGroupAction--------------");
//                List<NodeRunGroupAction> nodeRunGroupActions = new NodeRunGroupActionServiceImpl()
//                        .findList("from NodeRunGroupAction where id.flowRunId =? and id.nodeId=?", -1, -1,
//                                flowRunId, _node.getNodeId());

                for (NodeRunGroupAction nodeRunGroupAction : nodeRunGroupActions) {
                    if (actionOfFlowIds.contains(nodeRunGroupAction.getId().getStepNum())) {
                        declare = true;
                    }
                }
            } else {
                declare = true;
            }

            if (declare) {
                try {
                    if (nodeActionOffs != null) {
                        for (NodeActionOff2 nodeActionOff : nodeActionOffs) {
                            boolean isBreak = false;
                            for (ActionOfFlow actionOfFlow : actionOfFlows) {
                                Long flag = _node.getFlag();
                                if ((_node.getNodeType().getTypeId() == NODE_TYPE.RNC.value.longValue())) {
                                    if (flag == null) {
                                        flag = 0L;
                                    }
                                    if (flag != 2) {
                                        flag = 0L;
                                    } else if (flag == 2) {
                                        flag = 1L;
                                    }
                                }

                                if (Objects.equals(_node.getNodeType().getTypeId(), Config.NODE_TYPE.SGSN.value) || Objects.equals(_node.getNodeType().getTypeId(), Config.NODE_TYPE.MSC.value)) {
                                    if (actionOfFlow.getStepNum().equals(nodeActionOff.getActionOfFlowId())
                                            && _node.getNodeType().equals(nodeActionOff.getNodeType())
                                            && flag != null
                                            && Objects.equals(flag, nodeActionOff.getIsMaster())) {
                                        declare = false;
                                        isBreak = true;
                                        break;
                                    }
                                } else {
                                    if (actionOfFlow.getStepNum().equals(nodeActionOff.getActionOfFlowId())
                                            && _node.getNodeType().equals(nodeActionOff.getNodeType()) //                                            && flag != null
                                            ) {
                                        declare = false;
                                        isBreak = true;
                                        break;
                                    }
                                }
                            }
                            if (isBreak) {
                                break;
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
            groupAction = new GroupAction(groupName, actionOfFlows, declare);
            listGroupActions.add(groupAction);
        }
        _node.getMapGroupActions().put(subFlowRun + "#" + _node.getNodeCode(), listGroupActions);
        mapGroupAction.put(subFlowRun + "#" + _node.getNodeCode(), listGroupActions);
    }

    public void onChangeNode(TabChangeEvent changeEvent) {
        try {
            logAction = LogUtils.addContent("", "Change Node");
            String id = changeEvent.getTab().getClientId();
            id = id.split("-", -1)[2];

            if (nodes != null) {
                for (Node node : nodes) {
                    if (node.getNodeId().toString().equalsIgnoreCase(id)) {
                        selectedNode = node;
                        logAction = LogUtils.addContent(logAction, "Node code" + node.getNodeCode());
                        List<ParamValue> paramValues = getParamInputs(selectSubflowRun, selectedNode);
                        if (paramValues != null && paramValues.size() > 0) {
                            onKeyUpValueParam(paramValues.get(0), paramValues);
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            logAction = LogUtils.addContent(logAction, "Node code" + e.getMessage());
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void onChangeNodeAfterRemove() {
        try {
            logAction = LogUtils.addContent("", "Change Node after remove");
            List<ParamValue> paramValues = getParamInputs(selectSubflowRun, selectedNode);
            if (paramValues != null && paramValues.size() > 0) {
                onKeyUpValueParam(paramValues.get(0), paramValues);
            }
        } catch (Exception e) {
            logAction = LogUtils.addContent(logAction, "Node code" + e.getMessage());
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void onChangeSubFlowRuns(TabChangeEvent changeEvent) {
        try {
            logAction = LogUtils.addContent("", "Change sub flow run");
            String id = changeEvent.getTab().getClientId();
            id = id.split("-", -1)[1];
            if (mapSubFlowRunNodes != null) {
                for (String subFlowRun : mapSubFlowRunNodes.keySet()) {
                    if (subFlowRun.equalsIgnoreCase(id)) {
                        selectSubflowRun = subFlowRun;
                        logAction = LogUtils.addContent(logAction, "selectSubflowRun: " + selectSubflowRun);
                        nodes = mapSubFlowRunNodes.get(selectSubflowRun);
                        sortNode(nodes);
                        selectedNode = nodes.get(0);
                        List<ParamValue> paramValues = getParamInputs(selectSubflowRun, selectedNode);
                        if (paramValues != null && paramValues.size() > 0) {
                            onKeyUpValueParam(paramValues.get(0), paramValues);
                        }
                        break;
                    }
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }

    public void onChangeActionDeclare(String actionGroup) {

//		Boolean valueChange = selectedNode.getMapGroupActionDeclare().get("get");
//		selectedNode.getMapGroupActionDeclare().put(actionGroup, valueChange);
        for (ActionOfFlow actionOfFlow : groupActions.get(actionGroup)) {

        }
//		System.err.println(actionGroup);
    }

    /**
     * Xu ly nhap param tren nhieu dong
     *
     * @return
     * @author huynx6
     */
    private Multimap<String, BasicDynaBean> joinParamsByNodeCode() {
        Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
        for (int i = 0; i < objectImports.size(); i++) {
            BasicDynaBean basicDynaBean = (BasicDynaBean) objectImports.get(i);
            try {
                //20170817_hienhv4_fix import param to mop_start
                Object nodeCode = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("key.nodeCode").toLowerCase());
                //20170817_hienhv4_fix import param to mop_end
                if (nodeCode != null) {
                    multimapParam.put(nodeCode.toString(), basicDynaBean);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return multimapParam;
    }

    public void pushParamToForm() {
        if (this.nodes == null) {
            this.nodes = new ArrayList<Node>();
        }
        int countNodeAdded = 0;
        Multimap<String, BasicDynaBean> multimapParam = joinParamsByNodeCode();
        for (String nodeCode : multimapParam.keySet()) {

            List<Node> nodes = new ArrayList<Node>();
            try {
                nodes = completeNodeRun(nodeCode, true, true);
                if (nodes.size() != 1) {
                    continue;
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }
            try {
                Node node = nodes.get(0);
                if (selectSubflowRun == null) {
                    selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
                    loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                } else {
                    loadGroupAction(selectSubflowRun, node);
                }
                if (mapSubFlowRunNodes == null) {
                    mapSubFlowRunNodes = new HashMap<>();
                }
                if (mapSubFlowRunNodes.get(selectSubflowRun) == null) {
                    mapSubFlowRunNodes.put(selectSubflowRun, new ArrayList<Node>());
                }
                for (Node _node : nodes) {
                    if (!mapSubFlowRunNodes.get(selectSubflowRun).contains(_node)) {
                        mapSubFlowRunNodes.get(selectSubflowRun).add(_node);
                    }
                }
                List<ParamValue> paramValues = getParamInputs(selectSubflowRun, node);
                Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeCode);
                for (ParamValue paramValue : paramValues) {
                    if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().isRelTotal()) {
                        continue;
                    }
                    String value = "";
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Object object = null;
                            try {
                                //20170817_hienhv4_fix import param to mop_start
                                object = basicDynaBean.getMap().get(Util.normalizeParamCode(paramValue.getParamCode()).toLowerCase());
                                //20170817_hienhv4_fix import param to mop_end
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                            if (object != null) {
                                value += object.toString() + Config.SPLITTER_VALUE;
                            }
                        }
                        value = value.replaceAll(Config.SPLITTER_VALUE + "$", "");

                    } catch (Exception e) {
                        throw e;
                    }
                    if (!value.isEmpty()) {
                        paramValue.setParamValue(value);//.substring(0, Math.min(3950, value.length())));
                    }
                }
                if (!this.nodes.contains(node)) {
                    this.nodes.add(node);
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }
            countNodeAdded++;
        }

        if (nodes != null && nodes.size() > 0) {
            selectedNode = nodes.get(0);
            List<ParamValue> paramValues = getParamInputs(selectSubflowRun, selectedNode);
            for (ParamValue paramValue : paramValues) {
                if (selectSubflowRun == null || "".equals(selectSubflowRun)) {
                    selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
                }
                loadParam(paramValue, paramValues);
                //onKeyUpValueParam(paramValue, paramValues);
            }
//            long startTime = System.currentTimeMillis();
            createActionWithMultiParam(paramValues);
//            logger.info("time: " + (System.currentTimeMillis() - startTime) / 1000);
        }

        MessageUtil.setInfoMessage(MessageUtil.getResourceBundleMessage("info.number.of.node.imported") + ": " + countNodeAdded);


        rerender = true;
    }

    List<?> objectImports = new LinkedList<>();

    @SuppressWarnings("unchecked")
    public void handleImportParams(FileUploadEvent event) {
        objectImports.clear();
        Workbook workbook = null;
        try {
            InputStream inputstream = event.getFile().getInputstream();

            if (inputstream == null) {
                throw new NullPointerException("inputstream is null");
            }
            //Get the workbook instance for XLS/xlsx file 
            try {
                workbook = WorkbookFactory.create(inputstream);
//                if (workbook == null) {
//                    throw new NullPointerException("workbook is null");
//                }
            } catch (InvalidFormatException e2) {
                logger.error(e2.getMessage(), e2);
                throw new AppException("File import phải là Excel 97-2012 (xls, xlsx)!");
            } finally {
                if (workbook != null) {
                    try {
                        workbook.close();
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("meassage.import.fail2");
            return;
        }
        List<ObjectImportDt> params = new LinkedList<ObjectImportDt>();
        List<String> sheetNames = new LinkedList<>();
        getContextVar(params, sheetNames);

        try {
            for (int i = 0; i < sheetNames.size(); i++) {
                String sheetName = sheetNames.get(i);

                Importer<Serializable> importer = new Importer<Serializable>() {

                    @Override
                    protected Map<Integer, String> getIndexMapFieldClass() {
                        return null;
                    }

                    @Override
                    protected String getDateFormat() {
                        return null;
                    }
                };

                Map<Integer, String> indexMapFieldClass = new HashMap<Integer, String>();
                Integer key = 2;
                //20170817_hienhv4_fix import param to mop_start
                indexMapFieldClass.put(1, MessageUtil.getResourceBundleMessage("key.nodeCode").toLowerCase());
                //20170817_hienhv4_fix import param to mop_end
                for (String string : params.get(i).getParamNames()) {
                    indexMapFieldClass.put(key++, string);
                }

                importer.setIndexMapFieldClass(indexMapFieldClass);
                Map<Integer, String> mapHeader = new HashMap<Integer, String>();
                //20170817_hienhv4_fix import param to mop_start
                mapHeader.put(1, MessageUtil.getResourceBundleMessage("datatable.header.stt").toLowerCase());
                importer.setMapHeader(mapHeader);
                importer.setRowHeaderNumber(6);
                importer.setIsReplaceSpace(false);
                //20170817_hienhv4_fix import param to mop_end
                List<Serializable> objects = importer.getDatas(workbook, sheetName, "1-");
                if (objects != null) {
                    ((List<Object>) objectImports).addAll(objects);
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.import.param.fail");
        }

    }


    public void exportTemplateImportParam() {

        HttpServletResponse servletResponse = preHeader();
        String file = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.file.import.param"));
        logAction = LogUtils.addContent("", "Export template import param: " + file);
        try (InputStream is = new FileInputStream(file)) {
            try (OutputStream os = servletResponse.getOutputStream()) {

                Context context = new Context();
                List<ObjectImportDt> params = new LinkedList<>();
                List<String> sheetNames = new LinkedList<>();
                getContextVar(params, sheetNames);
                context.putVar("params", params);
                context.putVar("sheetNames", sheetNames);
//                logger.info("params:" + params);
//                logger.info("sheetNames:" + sheetNames);
                JxlsHelper.getInstance().setDeleteTemplateSheet(true).processTemplateAtCell(is, os, context, "Sheet2!A1");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        FacesContext.getCurrentInstance().responseComplete();
    }

    private HttpServletResponse preHeader() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse servletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        servletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        servletResponse.setHeader("Expires", "0");
        servletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        servletResponse.setHeader("Pragma", "public");
        try {
            servletResponse.setHeader("Content-disposition", "attachment;filename="
                    + URLEncoder.encode(MessageUtil.getResourceBundleMessage("key.template.file.import.param").replace(".xlsx", "") + "_" + selectedFlowTemplates.getFlowTemplateName() + ".xlsx", "UTF-8"));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return servletResponse;
    }

    public StreamedContent exportOldTemp() {
        try {
            File file = new File(CommonExport.getFolderSave() + File.separator + flowRunAction.getFileNameImportDt());
            if (!file.exists()) {
                return null;
            }
            InputStream input = new FileInputStream(file);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            StreamedContent fileDispatch = new DefaultStreamedContent(input, externalContext.getMimeType(flowRunAction.getFileNameImportDt()), flowRunAction.getFileNameImportDt());
            return fileDispatch;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;

    }

    public void handleImportTempFile(FileUploadEvent fileUploadEvent) {
        this.flowRunAction.setFileImportDT(fileUploadEvent.getFile());
    }

    private void getContextVar(List<ObjectImportDt> params, List<String> sheetNames) {
        Set<InfoNode> infoNodes = new HashSet<>();
        for (ActionOfFlow actionOfFlow : selectedFlowTemplates.getActionOfFlows()) {
            for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                infoNodes.add(new InfoNode(actionDetail.getVendor(), actionDetail.getVersion(), actionDetail.getNodeType()));
            }
        }
        long i = 0;
        if (selectSubflowRun == null || selectSubflowRun.isEmpty()) {
            selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }
        for (InfoNode infoNode : infoNodes) {

            ObjectImportDt objectImportDt = new ObjectImportDt();
            Node node = new Node();
            node.setNodeId(i--);
            node.setNodeCode(node.getNodeId().toString());
//            logger.info("infoNode: " + node.getNodeCode());
            node.setVendor(infoNode.getVendor());
            node.setVersion(infoNode.getVersion());
            node.setNodeType(infoNode.getNodeType());
            objectImportDt.setVendor(infoNode.getVendor());
            objectImportDt.setVersion(infoNode.getVersion());
            objectImportDt.setNodeType(infoNode.getNodeType());

            List<ParamValue> _paramValues = getParamInputs(selectSubflowRun, node);
            if (_paramValues.isEmpty()) {
                sheetNames.add(infoNode.getNodeType().getTypeName() + "-" + infoNode.getVendor().getVendorName() + "-" + infoNode.getVersion().getVersionName());
                params.add(objectImportDt);
                continue;
            }
//            logger.info("getContextVar _paramValues size: " + _paramValues.size());
            List<ParamValue> paramValues = distinctParamValueSameParamCode(selectSubflowRun, node, _paramValues);
            List<String> paramNames = new LinkedList<>();
            List<List<Object>> paramValueDefaults2 = new ArrayList<>();
            List<Object> paramValueDefaults = new LinkedList<>();
            for (ParamValue paramValue : paramValues) {

                if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().isRelTotal()
                        || paramValue.getParamInput().getIsFormula()) {
                    continue;
                }
                paramNames.add(paramValue.getParamInput().getParamCode());
                paramValueDefaults.add(paramValue.getParamValue());
            }
            paramValueDefaults2.add(paramValueDefaults);
            objectImportDt.setParamNames(paramNames);
            objectImportDt.setParamValues(paramValueDefaults2);
            sheetNames.add(infoNode.getNodeType().getTypeName() + "-" + infoNode.getVendor().getVendorName() + "-" + infoNode.getVersion().getVersionName());
            params.add(objectImportDt);
        }

    }

    public void exportDt() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            logAction = LogUtils.addContent("", "Export Dt");
            HttpServletResponse servletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            servletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            servletResponse.setHeader("Expires", "0");
            servletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            servletResponse.setHeader("Pragma", "public");
            servletResponse.setHeader("Content-disposition", "attachment;filename=" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + "-exportDt.xlsx");
            String file = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
            //Save temp file
            File fileOut = new File(Util.getTEMP_DIR() + File.separator + "tmp" + new Date().getTime() + ".xlsx");
            exportToFile(file, fileOut, servletResponse.getOutputStream());
            logAction = LogUtils.addContent(logAction, "file: " + file);
            logAction = LogUtils.addContent(logAction, "flowRunAction Id: " + flowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "flowRunAction Name: " + flowRunAction.getFlowRunName());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        facesContext.responseComplete();
    }

    public void exportToFile(String file, File fileOut, OutputStream outStream) {

        logger.info("Bat dau xuat file");
        try (InputStream is = new FileInputStream(file)) {
            //servletResponse.getOutputStream()
            try (OutputStream os = new FileOutputStream(fileOut)) {

                Context context = new Context();

                List<ObjectExport> objectExports = new LinkedList<ObjectExport>();
                Map<String, List<ParamValue>> mapParamValues = new HashMap<>();
                for (Node node : nodes) {
                    if (selectSubflowRun == null) {
                        loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node, this.flowRunAction.getFlowRunId());
                    } else {
                        loadGroupAction(selectSubflowRun, node, this.flowRunAction.getFlowRunId());
                    }
                    List<ParamValue> paramValues = getParamInputs(null, node);
                    mapParamValues.put(node.getNodeCode(), paramValues);
                    if (paramValues != null && paramValues.size() > 0) {
                        loadParam(paramValues.get(0), paramValues);
                        createActionWithMultiParamNew(paramValues, node);
                    }
                }

                for (Iterator<String> iterator = groupActions.keySet().iterator(); iterator.hasNext(); ) {
                    String groupName = iterator.next();
                    objectExports.add(new ObjectExport(groupName, new LinkedList<Node>()));
                }

                long countActOfFlow = 0;
                for (ObjectExport objectExport : objectExports) {
//                    logger.info("objectExport: " + objectExport.getGroupName());
                    if (this.nodes == null) {
                        return;
                    }
                    LinkedList<Node> nodes = new LinkedList<>(this.nodes);
                    for (Node node : nodes) {
//                        logger.info("objectExport node: " + node.getNodeCode());
                        List<ActionOfFlow> actionOfFlows = new ArrayList<>(groupActions.get(objectExport.getGroupName()));
                        boolean contNode = false;
                        if (mapGroupAction.get(selectSubflowRun + "#" + node.getNodeCode()) != null) {
                            for (GroupAction groupAction : mapGroupAction.get(selectSubflowRun + "#" + node.getNodeCode())) {
                                if (groupAction.isNoCommand(node)) {
                                    continue;

                                }
                                if (!groupAction.isDeclare() && actionOfFlows.containsAll(groupAction.getActionOfFlows())) {
                                    contNode = true;
                                    break;
                                }
                                if (objectExport.getGroupName().equals(groupAction.getGroupActionName())) {
                                    actionOfFlows = groupAction.getActionOfFlows();
                                    break;
                                }
                            }
                        }
                        if (contNode) {
                            continue;
                        }

                        List<ParamValue> paramValues = mapParamValues.get(node.getNodeCode());
                        Map<Long, Long> mapStepNumber = new HashMap<>();
                        for (ActionOfFlow actionOfFlow : actionOfFlows) {
                            List<String> commandExecutes = new MyLinkedList<>();
                            List<ObjectExportCommand> objectExportCommandExecutes = new MyLinkedList<>();
                            for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                                if (node.getVendor().equals(actionDetail.getVendor())
                                        && node.getNodeType().equals(actionDetail.getNodeType())
                                        && node.getVersion().equals(actionDetail.getVersion())) {
                                    for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                                        List<String> cmds = actionCommand.getCommandDetail().buildCommand(paramValues, false, actionOfFlow, actionCommand, (this.flowRunAction == null ? 0L : (flowRunAction.getTimeLoadParam() == null ? 0L : 1L)));

                                        for (String cmd : cmds) {
                                            ObjectExportCommand objectExportCommand = new ObjectExportCommand();
                                            objectExportCommand.setCommandName(cmd);
                                            objectExportCommand.setOperator(actionCommand.getCommandDetail().getOperator() == null ? "" : actionCommand.getCommandDetail().getOperator());
                                            objectExportCommand.setStandarValue(actionCommand.getCommandDetail().buildStandardValue(paramValues));
                                            objectExportCommand.setActionName(actionOfFlow.getAction().getName());
                                            if (!"NO CHECK".equalsIgnoreCase(objectExportCommand.getOperator())) {
                                                if (objectExportCommand.getStandarValue() == null) {
                                                    objectExportCommand.setResult(objectExportCommand.getOperator());
                                                } else {
                                                    objectExportCommand.setResult(objectExportCommand.getOperator() + "(" + objectExportCommand.getStandarValue() + ")");
                                                }
                                            } else {
                                                objectExportCommand.setResult("");
                                            }
                                            if (cmd != null && !cmd.isEmpty()) {
                                                if (actionCommand.getType() == Config.EXECUTE_CMD.longValue()) {
                                                    commandExecutes.add(cmd);
                                                    if (!"".equals(cmd)) {
                                                        objectExportCommandExecutes.add(objectExportCommand);
                                                    }
                                                }
                                                //                                            else if (actionCommand.getType() == Config.ROLLBACK_CMD.longValue()) {
                                                //                                                commandRollbacks.add(cmd);
                                                //                                                if (!"".equals(cmd)) {
                                                //                                                    objectExportCommandRollbacks.add(objectExportCommand);
                                                //                                                }
                                                //                                            }
                                            }
                                        }
                                    }
                                }
                            }

                            actionOfFlow.getAction().setCommandExecutes(commandExecutes);
                            actionOfFlow.getAction().setCommandRollbacks(new LinkedList<String>());
                            actionOfFlow.getAction().setObjectExportCommandExecutes(objectExportCommandExecutes);
                            actionOfFlow.getAction().setObjectExportCommandRollbacks(new LinkedList<ObjectExportCommand>());
                            //actionOfFlow.setAction(new Cloner().deepClone(actionOfFlow.getAction()));
                            actionOfFlow.setAction((actionOfFlow.getAction().cloneToExport()));

                            //hienhv4_20170831_hienhv4_fix loi export_start
                            if (!mapStepNumber.containsKey(actionOfFlow.getStepNum())) {
                                mapStepNumber.put(actionOfFlow.getStepNum(), 0l);
                            }
                            mapStepNumber.put(actionOfFlow.getStepNum(), mapStepNumber.get(actionOfFlow.getStepNum()) + 1);
                            //hienhv4_20170831_hienhv4_fix loi export_end
                        }

                        long currActionFlowId = -1;
                        List<Long> listOfRollbackId = new ArrayList<>();
                        for (Iterator<ActionOfFlow> iterator = actionOfFlows.iterator(); iterator.hasNext(); ) {
                            ActionOfFlow actionOfFlow = iterator.next();
                            if (actionOfFlow.getStepNum() != currActionFlowId) {
                                currActionFlowId = actionOfFlow.getStepNum();
                                listOfRollbackId.clear();
                            }
                            for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
                                if (actionOfFlow.getStepNumberLabel().equals(actionOfFlow2.getPreviousStep())
                                        && (actionOfFlow.getIndexParamValue() == null || actionOfFlow2.getIndexParamValue() == null
                                        || (actionOfFlow.getIndexParamValue() != null && actionOfFlow2.getIndexParamValue() != null
                                        && actionOfFlow.getIndexParamValue().equals(actionOfFlow2.getIndexParamValue()))
                                        //hienhv4 edited 2017/03/30 (fix truong hop action rollback clone nhieu, action tac dong chi co 1)
                                        || actionOfFlow.getTotalIndexParamvalue() == 1)
                                        && //huynx6 edited Oct 31, 2016
                                        //actionOfFlow2.getIfValue().equalsIgnoreCase("0")
                                        actionOfFlow2.getIsRollback().equals(1L)) {
                                    if (actionOfFlow.getTotalIndexParamvalue() > 1) {
                                        actionOfFlow.getAction().getCommandRollbacks().clear();
                                        actionOfFlow.getAction().getObjectExportCommandRollbacks().clear();
                                    }
                                    listOfRollbackId.add(actionOfFlow2.getStepNumberLabel());
                                    actionOfFlow.getAction().getCommandRollbacks().addAll(actionOfFlow2.getAction().getCommandExecutes());
                                    actionOfFlow.getAction().getObjectExportCommandRollbacks().addAll(actionOfFlow2.getAction().getObjectExportCommandExecutes());
                                    actionOfFlow.getAction().setNameRollback(actionOfFlow2.getAction().getName());
//                                    if (actionOfFlow.getTotalIndexParamvalue() > 1) {
//                                        break;
//                                    }
                                }

                                // check cac action rollback noi duoi
                                //hienhv4_20170831_hienhv4_fix loi export_start
                                if (!listOfRollbackId.isEmpty() && (actionOfFlow.getIndexParamValue() == mapStepNumber.get(actionOfFlow.getStepNum()) - 1)) {
                                    //hienhv4_20170831_hienhv4_fix loi export_end
                                    if (listOfRollbackId.contains(actionOfFlow2.getPreviousStep()) && actionOfFlow2.getIsRollback().equals(1L)) {
                                        listOfRollbackId.add(actionOfFlow2.getStepNumberLabel());
                                        actionOfFlow.getAction().getCommandRollbacks().addAll(actionOfFlow2.getAction().getCommandExecutes());
                                        actionOfFlow.getAction().getObjectExportCommandRollbacks().addAll(actionOfFlow2.getAction().getObjectExportCommandExecutes());
                                    }
                                }
                            }

                            if (actionOfFlow.getAction().getCommandExecutes().isEmpty()
                                    && actionOfFlow.getAction().getCommandRollbacks().isEmpty()) {
                                iterator.remove();
                                continue;
                            }

                            //Set size rollback equa execute commands 
                            int tmp = actionOfFlow.getAction().getCommandRollbacks().size() - actionOfFlow.getAction().getCommandExecutes().size();
                            if (tmp > 0) {
                                for (int i = 0; i < Math.abs(tmp); i++) {
                                    actionOfFlow.getAction().getCommandExecutes().add(null);
                                    actionOfFlow.getAction().getObjectExportCommandExecutes().add(null);
                                }
                            } else if (tmp < 0) {
                                for (int i = 0; i < Math.abs(tmp); i++) {
                                    actionOfFlow.getAction().getCommandRollbacks().add(null);
                                }
                            } else {
                                if (actionOfFlow.getAction().getCommandRollbacks().isEmpty()) {
                                    actionOfFlow.getAction().getCommandExecutes().add(null);
                                    actionOfFlow.getAction().getCommandRollbacks().add(null);
                                    actionOfFlow.getAction().getObjectExportCommandExecutes().add(null);
                                    actionOfFlow.getAction().getObjectExportCommandRollbacks().add(null);
                                }
                            }
                        }

                        if (actionOfFlows.isEmpty()) {
                            continue;
                        } else {
                            for (Iterator<ActionOfFlow> iterator = actionOfFlows.iterator(); iterator.hasNext(); ) {
                                ActionOfFlow actionOfFlow = iterator.next();
                                if (actionOfFlow.getIsRollback().equals(1L)) {
                                    iterator.remove();
                                } else {
                                    countActOfFlow += actionOfFlow.getAction().getCommandExecutes() == null ? 0 : actionOfFlow.getAction().getCommandExecutes().size();
                                }
                            }
                        }

                        node.setActionOfFlows(actionOfFlows);

                        //20170727_hienhv4_fix_export_start
//                        logger.info("bat dau clone actionOfFlows");
                        Node newNode = new Node();
                        newNode.setActionOfFlows(new ArrayList<ActionOfFlow>());
                        for (ActionOfFlow aofw : actionOfFlows) {
                            newNode.getActionOfFlows().add(aofw.cloneToExport());
                        }
                        newNode.setNodeCode(node.getNodeCode());
//                        newNode.setActionOfFlows(actionOfFlows);
//                        Node deepClone = new Cloner().deepClone(newNode);
//                        Node deepClone = (newNode).deepClone();
                        objectExport.getNodes().add(newNode);
                        //20170727_hienhv4_fix_export_end
//                        logger.info("ket thuc clone actionOfFlows");
                    }

                }

                System.out.println("prepare check before/after");
                //Get Check before/After Group action
                List<Node> nodeCheckBefores = new ArrayList<>();
                List<Node> nodeCheckAfters = new ArrayList<>();

                //Remove group action no node
                for (Iterator<ObjectExport> iterator2 = objectExports.iterator(); iterator2.hasNext(); ) {
                    ObjectExport objectExport2 = iterator2.next();
                    if (objectExport2.getNodes().isEmpty()) {
                        iterator2.remove();
                    } else {
                        for (Node node : objectExport2.getNodes()) {
                            for (Iterator<ActionOfFlow> iterator = node.getActionOfFlows().iterator(); iterator.hasNext(); ) {
                                ActionOfFlow actionOfFlow = iterator.next();
                                //huynx6 edited Oct 31, 2016
                                //if(actionOfFlow.getIfValue().equals("0"))
                                if (actionOfFlow.getIsRollback() == 1L) {
                                    iterator.remove();
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < objectExports.size(); i++) {
                    if (i == 0 || i == objectExports.size() - 1) {
                        ObjectExport objectExport = objectExports.get(i);
                        for (Node node : objectExport.getNodes()) {
                            if (node.getActionOfFlows().size() > 0) {
                                if (i == 0) {
                                    //Node nodeClone = new Cloner().deepClone(node);
                                    //20170727_hienhv4_fix_export_start
                                    Node nodeClone = new Node();//(node).deepClone();
                                    nodeClone.setNodeCode(node.getNodeCode());
                                    //20170727_hienhv4_fix_export_end
                                    nodeClone.getActionOfFlows().clear();
                                    for (ActionOfFlow actionOfFlow : node.getActionOfFlows()) {
                                        ActionOfFlow bak = actionOfFlow;
                                        nodeClone.getActionOfFlows().add(bak);
                                        //node.getActionOfFlows().remove(bak);
                                    }
                                    nodeCheckBefores.add(nodeClone);
                                }
                                if (i == objectExports.size() - 1) {
                                    //Node nodeClone = new Cloner().deepClone(node);
                                    //20170727_hienhv4_fix_export_start
                                    Node nodeClone = new Node();//(node).deepClone();
                                    nodeClone.setNodeCode(node.getNodeCode());
                                    //20170727_hienhv4_fix_export_end
                                    nodeClone.getActionOfFlows().clear();
                                    for (ActionOfFlow actionOfFlow : node.getActionOfFlows()) {
                                        ActionOfFlow bak = actionOfFlow;
                                        nodeClone.getActionOfFlows().add(bak);
                                        //node.getActionOfFlows().remove(bak);
                                    }
                                    nodeCheckAfters.add(nodeClone);
                                }
                            }
                        }
                    }
                }
                //Remove 2 groups action check
                if (objectExports.size() > 0) {
                    objectExports.remove(0);
                }
                if (objectExports.size() > 0) {
                    objectExports.remove(objectExports.size() - 1);
                }

                context.putVar("objectExports", objectExports);
                context.putVar("nodeCheckBefores", nodeCheckBefores);
                context.putVar("nodeCheckAfters", nodeCheckAfters);
                //huynx6 added Nov 5, 2016

                context.putVar("nodes", this.nodes);

//				EmployeeBean manageOfEmployee = new VHRService().getManageOfEmployee();
//				flowRunAction.setApproveBy(manageOfEmployee.getEmail()+" ("+manageOfEmployee.getFullName()+")");
                context.putVar("flowRunAction", this.flowRunAction);
                context.putVar("employees", new ArrayList<>());
                //new VHRService().getEmployeesOfDepartment(BAN_DVCD.KV1.getValue())
                long startTime = System.currentTimeMillis();
                logger.info("bat dau xuat file excel");
//                Workbook workbook = WorkbookFactory.create(is);
//                JxlsHelper.getInstance().processTemplate(context, PoiTransformer.createSxssfTransformer(workbook));
//                workbook.write(os);
                JxlsHelper.getInstance().processTemplate(is, os, context);
                long endTime = System.currentTimeMillis();
                logger.info("ket thuc xuat file excel: " + (endTime - startTime) / 1000);

                //Merge, Style Cell
                logger.info("bat dau meger file excel (size): " + countActOfFlow);
                if (countActOfFlow <= 20000) {
                    postProcessTemplate(fileOut, outStream);
                } else {
                    Workbook workbook = WorkbookFactory.create(fileOut);
                    workbook.write(outStream);
                    //delete tmp file

                    if(workbook != null){
                        workbook.close();
                    }
                    fileOut.delete();
                }
                logger.info("ket thuc meger file excel: " + (System.currentTimeMillis() - endTime) / 1000);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * merge cell, style cell
     *
     * @param fileOut
     * @param outStream
     * @throws IOException
     * @author huynx6
     */
    private void postProcessTemplate(File fileOut, OutputStream outStream) throws IOException {
        // TODO Auto-generated method stub

//        logger.info("start meger");
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(fileOut);

            CellStyle cellStyle = workbook.createCellStyle();
            Font createFont = workbook.createFont();
            createFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
            cellStyle.setFont(createFont);
            cellStyle.setWrapText(true);
            cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            for (int iSheet = 0; iSheet < 3; iSheet++) {
                Sheet sheet = workbook.getSheetAt(iSheet);
                int iRow = 0;
                List<Integer> firstRows = new LinkedList<>();
                for (Iterator<Row> rowIterator = sheet.iterator(); rowIterator
                        .hasNext(); ) {
                    Row row = (Row) rowIterator.next();
                    if (iRow++ < 8) {
                        continue;
                    }
                    Cell cell = row.getCell(3);
                    if (cell != null && !cell.getStringCellValue().isEmpty()) {
                        firstRows.add(iRow);
                    }
                    if (cell != null && cellStyle != null) {
                        if (rowIterator.hasNext()) {
                            cell.setCellStyle(cellStyle);
                        }
                    }
                }
                firstRows.add(iRow + 1);
//				System.err.println(firstRows);
                for (int i = 0; i < firstRows.size() - 1; i++) {
                    Integer integer = firstRows.get(i);
                    int firstRow = integer;
                    int lastRow = firstRows.get(i + 1);
                    if (firstRow < lastRow - 1) {
                        int _firstRow = firstRow - 1;
                        int _lastRow = lastRow - 2;

                        for (CellRangeAddress cellRangeAddress2 : sheet.getMergedRegions()) {
                            if (_firstRow == cellRangeAddress2.getFirstRow()) {
                                _firstRow++;
                            }
                            if (_lastRow == cellRangeAddress2.getLastRow()) {
                                _lastRow--;
                            }
                        }
                        CellRangeAddress cellRangeAddress = new CellRangeAddress(_firstRow, _lastRow, 3, 3);
                        if (_firstRow < _lastRow) {
                            sheet.addMergedRegion(cellRangeAddress);
                            RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                            RegionUtil.setBorderRight(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                            RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                        }

                        if (iSheet == 1) {
                            if (_firstRow < _lastRow) {
                                CellRangeAddress cellRangeAddress2 = new CellRangeAddress(_firstRow, _lastRow, 10, 10);
                                sheet.addMergedRegion(cellRangeAddress2);
                                RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, cellRangeAddress2, sheet, workbook);
                                RegionUtil.setBorderRight(CellStyle.BORDER_THIN, cellRangeAddress2, sheet, workbook);
                            }
                        }
                    }
                }
                for (int i = firstRows.get(0); i <= sheet.getLastRowNum(); i++) {
                    for (int c = 0; c < 3; c++) {
                        CellRangeAddress cellRangeAddress = new CellRangeAddress(i, i, c, c);
                        RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                        RegionUtil.setBorderRight(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                        RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                        RegionUtil.setBorderTop(CellStyle.BORDER_THIN, cellRangeAddress, sheet, workbook);
                    }
                }
            }
            //huynx6 added Nov 9, 2016
            //Add template import
//            logger.info("end meger");
            if (flowRunAction.getFileNameImportDt() != null) {
                SXSSFWorkbook workbookTemp = null;

                try {
//                    logger.info("start save");
                    File file = new File(CommonExport.getFolderSave() + File.separator + flowRunAction.getFileNameImportDt());

                    workbookTemp = new SXSSFWorkbook((XSSFWorkbook) WorkbookFactory.create(file));
                    int ind = 10;
                    for (Iterator<Sheet> iterator = workbookTemp.iterator(); iterator.hasNext(); ) {
                        Sheet sheetTemp = iterator.next();
                        Sheet sheet = workbook.createSheet();
                        ExcelUtil.copySheets(sheet, sheetTemp);
                        workbook.setSheetOrder(sheetTemp.getSheetName(), ind++);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                } finally {
                    if (workbookTemp != null) {
                        workbookTemp.write(outStream);
                        //delete tmp file

                        workbookTemp.close();
                        fileOut.delete();
                    }
                }
//                logger.info("end save");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (workbook != null) {
                workbook.write(outStream);
                //delete tmp file

            }
            if(workbook != null){
                workbook.close();
                fileOut.delete();
            }
            if(fileOut.exists()){
                fileOut.delete();
            }
        }

    }

    public void deleteFlowRunAction(FlowRunAction flowRunAction) {
        try {
            logAction = LogUtils.addContent("", "Delete Flow run action");
            logAction = LogUtils.addContent("", "FlowRun Id: " + flowRunAction.getFlowRunId());
            logAction = LogUtils.addContent("", "FlowRun Name: " + flowRunAction.getFlowRunName());
            //#{!sessionUtil.createMop || flowRunAction.status &gt; 1 || (flowRunAction.crStatus &gt; 0)}
            if (!new SessionUtil().isCreateMop() || flowRunAction.getStatus() > 1 || (flowRunAction.getCrStatus() != null && flowRunAction.getCrStatus() > 0)) {
                MessageUtil.setWarnMessageFromRes("error.no.permission.flow");
                return;
            }
            String currentUsername = SessionUtil.getCurrentUsername();
            if (flowRunAction.getCreateBy() != null && !currentUsername.equalsIgnoreCase(flowRunAction.getCreateBy())
                    && !new SessionUtil().isActionAdmin()) {
                MessageUtil.setWarnMessageFromRes("error.no.permission.flow");
                return;
            }

            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            new ParamValueServiceImpl().execteBulk2("delete from ParamValue where nodeRun.id.flowRunId = ?", session, tx, false, flowRunAction.getFlowRunId());
            new NodeRunGroupActionServiceImpl().execteBulk2("delete from NodeRunGroupAction where id.flowRunId = ? ", session, tx, false, flowRunAction.getFlowRunId());
            new NodeRunServiceImpl().execteBulk2("delete from NodeRun where id.flowRunId = ?", session, tx, false, flowRunAction.getFlowRunId());
            new RiSgRcParamUniqueServiceImpl().execteBulk2("delete RiSgRcParamUnique where flowRunActionId = ? ", session, tx, false, flowRunAction.getFlowRunId());
            new FlowRunActionServiceImpl().execteBulk2("delete from FlowRunAction where flowRunId = ? ", session, tx, true, flowRunAction.getFlowRunId());
            MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            logAction = LogUtils.addContent("", "Result success");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
            logAction = LogUtils.addContent("", "Result fail");
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.DELETE.name(), logAction);
    }

    public void approveDT() {
        try {
            logAction = LogUtils.addContent("", "Approve DT" + flowRunAction.getFlowRunId());

//            if (flowRunAction.getFlowRunId() == null) {
            //Quytv7 sua neu khong save khi phe duyet thi save luon
            saveDT();
//                MessageUtil.setErrorMessageFromRes("error.dt.not.save");
//                return;
//            }
            flowRunAction.setTimeLoadParam(new Date());
            if (isApproveMop()) {
                flowRunAction.setStatus(Config.EXECUTE_AVAILABLE);
            } else {
                flowRunAction.setStatus(Config.SAVE_DRAFT);
            }
            logAction = LogUtils.addContent(logAction, "flowRunAction Id" + flowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "flowRunAction name" + flowRunAction.getFlowRunName());
            logAction = LogUtils.addContent(logAction, "Status: " + flowRunAction.getStatus());
            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
            MessageUtil.setInfoMessageFromRes("info.dt.approve.success");
            logAction = LogUtils.addContent(logAction, "Approve success");
        } catch (SysException | AppException e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.dt.approve.unsuccess");
            logAction = LogUtils.addContent(logAction, "Approve fail");
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);

    }

    //    public boolean saveDT() {
//        //flowRunAction;
//        String currentUsername = SessionUtil.getCurrentUsername();
//        if (flowRunAction.getCreateBy() != null && !currentUsername.equalsIgnoreCase(flowRunAction.getCreateBy())
//                && !new SessionUtil().isActionAdmin()) {
//            MessageUtil.setWarnMessageFromRes("error.no.permission.flow");
//            return false;
//        }
//
//        List<NodeRun> nodeRuns = new ArrayList<NodeRun>();
//        flowRunAction.setCreateDate(new Date());
//        if (flowRunAction.getFlowRunName() != null) {
//            flowRunAction.setFlowRunName(flowRunAction.getFlowRunName().trim());
//        }
//        flowRunAction.setStatus(0L);
//
//        flowRunAction.setCreateBy(currentUsername);
//        //huynx6 added Nov 7, 2016
//        if (flowRunAction.getFileImportDT() != null) {
//            try {
//                String path = CommonExport.getPathSaveFileExport(flowRunAction.getFileImportDT().getFileName());
//                String fileName = path.substring(path.lastIndexOf(File.separator));
//                FileOutputStream out = new FileOutputStream(path);
//                out.write(flowRunAction.getFileImportDT().getContents());
//                out.close();
//                flowRunAction.setFileNameImportDt(fileName);
//            } catch (Exception e) {
//                logger.error(e.getMessage(), e);
//            }
//        }
//
//        Object[] objs = new FlowRunActionServiceImpl().openTransaction();
//        Session session = (Session) objs[0];
//        Transaction tx = (Transaction) objs[1];
//        try {
//
//            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
//            new ParamValueServiceImpl().execteBulk2("delete from ParamValue where nodeRun.id.flowRunId = ?", session, tx, false, flowRunAction.getFlowRunId());
//            new NodeRunGroupActionServiceImpl().execteBulk2("delete from NodeRunGroupAction where id.flowRunId = ? ", session, tx, false, flowRunAction.getFlowRunId());
//            new NodeRunServiceImpl().execteBulk2("delete from NodeRun where id.flowRunId = ?", session, tx, nodes != null && nodes.size() > 0 ? false : true, flowRunAction.getFlowRunId());
//            List<ParamValue> paramValues = new ArrayList<ParamValue>();
//
//            List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<NodeRunGroupAction>();
//            if (nodes != null && nodes.size() > 0) {
//                for (Node node : nodes) {
//                    logger.info("chay vao node :" + node.getNodeCode());
//                    NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), null), flowRunAction, node, null);
//                    nodeRuns.add(nodeRun);
//                    List<ParamValue> _paramValueOfNode = mapParamValue.get(node);
//                    if (_paramValueOfNode != null) {
//                        for (ParamValue paramValue : _paramValueOfNode) {
//                            logger.info("paramValue Code:" + paramValue.getParamCode());
//                            logger.info("paramValue Value:" + paramValue.getParamValue());
//                            //paramValue.setFlowRunAction(flowRunAction);
//                            paramValue.setNodeRun(nodeRun);
//                            paramValue.setCreateTime(new Date());
//                            paramValue.setParamValueId(null);
//                        }
//                        paramValues.addAll(_paramValueOfNode);
//                    }
//                    if (mapGroupAction.get(node) != null) {
//                        logger.info(" co vao mapGroupAction size = " + mapGroupAction.get(node));
//                        for (GroupAction groupAction : mapGroupAction.get(node)) {
//                            if (groupAction.isDeclare() && groupAction.getActionOfFlows().size() > 0 && !groupAction.isNoCommand(node)) {
//                                NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
//                                        new NodeRunGroupActionId(node.getNodeId(),
//                                                flowRunAction.getFlowRunId(),
//                                                groupAction.getActionOfFlows().get(0).getStepNum(), Config.SUB_FLOW_RUN_DEFAULT), groupAction.getActionOfFlows().get(0), nodeRun);
//                                nodeRunGroupActions.add(nodeRunGroupAction);
//                            }
//                        }
//                        logger.info(" thoai khoi mapGroupAction");
//                    }
//                }
//                logger.info(" xoa session");
//                session.clear();
//                logger.info(" insert NodeRunServiceImpl ");
//                sortNodeRun(nodeRuns);
//                new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
//                logger.info(" insert ParamValueServiceImpl ");
//                new ParamValueServiceImpl().saveOrUpdate(paramValues, session, tx, false);
//
//                session.flush();
//                session.clear();
////				for (NodeRunGroupAction nodeRunGroupAction2 : nodeRunGroupActions) {
////		 			//session.merge(nodeRunGroupAction2.getNodeRun());
////					new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupAction2);
////		 			session.merge(nodeRunGroupAction2);
////				}
//                new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
////				session.flush();
////		 		tx.commit();
//
//            } else {
//
//            }
//            //huynx6 added Nov 21, 2016
//            //Save File to database
//            String file2 = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
//            logger.info(" luu file thanh cong ");
//            File fileTemp2 = new File("tmp" + new Date().getTime() + ".xlsx");
//            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
//            exportToFile(file2, fileTemp2, outStream);
//            flowRunAction.setFileContent(outStream.toByteArray());
//            IOUtils.closeQuietly(outStream);
//            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
//
//            MessageUtil.setInfoMessageFromRes("info.save.dt.success");
//
//            // Cap nhat file DT sang GNOC
//            try {
//                if (flowRunAction.getCrNumber() != null
//                        && !flowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
//                        && !flowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
//                    if (GNOCService.isCanUpdateDT(flowRunAction.getCrNumber())) {
//                        List<String> nodeIPs = new ArrayList<>();
//                        if (nodes != null) {
//                            for (Node node : nodes) {
//                                if (!nodeIPs.contains(node.getNodeIp())) {
//                                    nodeIPs.add(node.getNodeIp());
//                                }
//                            }
//                        }
//
//                        Map<String, Object> filters = new HashMap<>();
//                        filters.put("crNumber-" + FlowRunActionServiceImpl.EXAC, flowRunAction.getCrNumber());
//
//                        List<FlowRunAction> flowRuns = new FlowRunActionServiceImpl().findList(filters);
//
//                        if (flowRuns != null) {
//                            for (FlowRunAction flow : flowRuns) {
//                                if (flow.getNodeRuns() != null) {
//                                    for (NodeRun nodeRun : flow.getNodeRuns()) {
//                                        if (!nodeIPs.contains(nodeRun.getNode().getNodeIp())) {
//                                            nodeIPs.add(nodeRun.getNode().getNodeIp());
//                                        }
//                                    }
//                                }
//                            }
//                        }
//
//                        boolean result = GNOCService.updateDTInfo(currentUsername, flowRunAction.getCrNumber(),
//                                nodeIPs.toArray(new String[nodeIPs.size()]), flowRunAction.getFlowRunId() + "", flowRunAction.getFileContent(),
//                                flowRunAction.getFlowRunId() + "_" + ZipUtils.clearHornUnicode(flowRunAction.getFlowRunName()) + ".xlsx");
//
//                        logger.info(result ? "UPDATE DT GNOC SUCCESS EXCELL" : "UPDATE DT GNOC FAIL EXCELL");
////                        }
//                    }
//                }
//            } catch (Exception ex) {
//                logger.error(ex.getMessage(), ex);
//            } finally {
////                if (os != null) {
////                    try {
////                        os.close();
////                    } catch (Exception ex) {
////                        logger.error(ex.getMessage(), ex);
////                    }
////                }
////                if (fileOut1 != null) {
////                    fileOut1.delete();
////                }
////                if (oldFile != null) {
////                    oldFile.delete();
////                }
////                if (newFile != null) {
////                    newFile.delete();
////                }
//            }
////            File zipFile = null;
////            try {
////                if (flowRunAction.getCrNumber() != null 
////                        && !flowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
////                        && !flowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
////                    if (GNOCService.isCanUpdateDT(flowRunAction.getCrNumber())) {
////                        String oldFilePath = flowRunAction.getDtFilePath();
////                        
////                        if (oldFilePath != null && !oldFilePath.trim().isEmpty()) {
////                            File oldFile = new File(oldFilePath);
////                            
////                            File fileTemp = new File("tmp" + new Date().getTime() + ".xlsx");
////                            File fileOut = new File(oldFile.getParentFile().getCanonicalPath() + File.separator + flowRunAction.getFlowRunId() 
////                                    + "_" + ZipUtils.clearHornUnicode(flowRunAction.getFlowRunName()) + ".xlsx");
////                            String file = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
////
////                            os = new FileOutputStream(fileOut);
////                            exportToFile(file, fileTemp, os);
////                            os.flush();
////                            
////                            flowRunAction.setDtFilePath(fileOut.getCanonicalPath());
////                            new FlowRunActionServiceImpl().execteBulk("update FlowRunAction a set a.dtFilePath = ? where a.flowRunId = ?", fileOut.getCanonicalPath(), flowRunAction.getFlowRunId());
////                            oldFile.delete();
////
////                            // Bat dau nen file
////                            zipFile = new File(Config.PATH_OUT + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "-exportDt.zip");
////                            ZipUtils.zipFolder(fileOut.getParentFile(), zipFile);
////                            
////                            String[] nodeIPs = null;
////                            if (nodes != null) {
////                                nodeIPs = new String[nodes.size()];
////                                for (int i = 0; i < nodes.size(); i++) {
////                                    nodeIPs[i] = nodes.get(i).getNodeIp();
////                                }
////                            }
////                            boolean result = GNOCService.updateDTInfo(currentUsername, flowRunAction.getCrNumber(),
////                                    nodeIPs, flowRunAction.getFlowRunName(), zipFile);
////
////                            logger.info(flowRunAction.getCrNumber()+": "+ (result ? "UPDATE DT GNOC SUCCESS" : "UPDATE DT GNOC FAIL"));
////                        } else {
////                            File fileTemp = new File("tmp" + new Date().getTime() + ".xlsx");
////                            fileOut1 = new File("tmp_out" + new Date().getTime() + ".xlsx");
////                            String file = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
////
////                            os = new FileOutputStream(fileOut1);
////                            exportToFile(file, fileTemp, os);
////                            os.flush();
////
////                            String[] nodeIPs = null;
////                            if (nodes != null) {
////                                nodeIPs = new String[nodes.size()];
////                                for (int i = 0; i < nodes.size(); i++) {
////                                    nodeIPs[i] = nodes.get(i).getNodeIp();
////                                }
////                            }
////                            boolean result = GNOCService.updateDTInfo(currentUsername, flowRunAction.getCrNumber(),
////                                    nodeIPs, flowRunAction.getFlowRunName(), fileOut1);
////
////                            logger.info(result ? "UPDATE DT GNOC SUCCESS" : "UPDATE DT GNOC FAIL");
////                        }
////                    }
////                }
////            } catch (Exception ex) {
////                logger.error(ex.getMessage(), ex);
////            } finally {
////                if (os != null) {
////                    try {
////                        os.close();
////                    } catch (Exception ex) {
////                        logger.error(ex.getMessage(), ex);
////                    }
////                }
////                if (zipFile != null) {
////                    zipFile.delete();
////                }
////                if (fileOut1 != null) {
////                    fileOut1.delete();
////                }
////            }
//            return true;
//        } catch (Exception e) {
//            if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
//                tx.rollback();
//            }
//            logger.error(e.getMessage(), e);
//            MessageUtil.setErrorMessageFromRes("error.save.dt.fail");
//            return false;
//        } finally {
//            if (session.isOpen()) {
//                session.close();
//            }
//        }
//
//    }
    public boolean saveDT() {
        //flowRunAction;
        logAction = LogUtils.addContent("", "Save DT");
        try {
            String currentUsername = SessionUtil.getCurrentUsername();
            if (flowRunAction.getCreateBy() != null && !currentUsername.equalsIgnoreCase(flowRunAction.getCreateBy())
                    && !new SessionUtil().isActionAdmin()) {
                MessageUtil.setWarnMessageFromRes("error.no.permission.flow");
                return false;
            }
            flowRunAction.setCreateBy(currentUsername);
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
        }
//        //20170510_HaNV15_Add_Start: validate country code
//        CatCountryBO catCountryBO = new CatCountryBO();
//        catCountryBO.setCountryCode(new SessionUtil().getCountryCodeCurrent());
//        flowRunAction.setCountryCode(catCountryBO);
//        if (flowRunAction.getCountryCode().getCountryCode() == null
//                || "".equals(flowRunAction.getCountryCode().getCountryCode().trim())
//                || !new SessionUtil().checkCountryCode(flowRunAction.getCountryCode().getCountryCode())) {
//            MessageUtil.setWarnMessageFromRes("common.country.code.not.exist");
//            return false;
//        }
//        //20170510_HaNV15_Add_End
        List<NodeRun> nodeRuns = new ArrayList<NodeRun>();
        flowRunAction.setCreateDate(new Date());
        if (flowRunAction.getFlowRunName() != null) {
            flowRunAction.setFlowRunName(Jsoup.parse(flowRunAction.getFlowRunName().replaceAll("(?i)<(/?script[^>]*)>", "").trim()).text());
        }
        if (flowRunAction.getDescription() != null) {
            flowRunAction.setDescription(Jsoup.parse(flowRunAction.getDescription().replaceAll("(?i)<(/?script[^>]*)>", "").trim()).text());
            flowRunAction.setDescription(StringEscapeUtils.escapeEcmaScript(flowRunAction.getDescription()));
        }
        flowRunAction.setStatus(0L);


        logAction = LogUtils.addContent(logAction, "Flowrun Id: " + flowRunAction.getFlowRunId());
        logAction = LogUtils.addContent(logAction, "Flowrun Name: " + flowRunAction.getFlowRunName());
        //huynx6 added Nov 7, 2016
        if (flowRunAction.getFileImportDT() != null) {
            try {
                String path = CommonExport.getPathSaveFileExport(flowRunAction.getFileImportDT().getFileName());
                String fileName = path.substring(path.lastIndexOf(File.separator));
                FileOutputStream out = new FileOutputStream(path);
                out.write(flowRunAction.getFileImportDT().getContents());
                out.close();
                flowRunAction.setFileNameImportDt(fileName);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        Object[] objs = new FlowRunActionServiceImpl().openTransaction();
        Session session = (Session) objs[0];
        Transaction tx = (Transaction) objs[1];
        try {
            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
            new ParamValueServiceImpl().execteBulk2("delete from ParamValue where nodeRun.id.flowRunId = ?", session, tx, false, flowRunAction.getFlowRunId());
            new NodeRunGroupActionServiceImpl().execteBulk2("delete from NodeRunGroupAction where id.flowRunId = ? ", session, tx, false, flowRunAction.getFlowRunId());
            new NodeRunServiceImpl().execteBulk2("delete from NodeRun where id.flowRunId = ?", session, tx, false, flowRunAction.getFlowRunId());
            List<ParamValue> paramValues = new ArrayList<ParamValue>();

            List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<NodeRunGroupAction>();
            if (mapSubFlowRunNodes.isEmpty()) {
                mapSubFlowRunNodes.put(Config.SUB_FLOW_RUN_DEFAULT, nodes);
            }
            Long subFlowRunNum = 0L;
            for (String subFlowRun : mapSubFlowRunNodes.keySet()) {
                subFlowRunNum++;
                nodes = mapSubFlowRunNodes.get(subFlowRun);
                sortNode(nodes);
                if (nodes != null && nodes.size() > 0) {
                    for (Node node : nodes) {
                        logger.info("chay vao node :" + node.getNodeCode());
                        NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), subFlowRun), flowRunAction, node, subFlowRunNum);
                        //20180109_quytv7_add them user/pass cho node run start
                        try {
                            nodeRun.setAccount(node.getAccount());
                            nodeRun.setPassword(node.getPassword());
                        }catch(Exception ex){
                            logger.error(ex.getMessage(), ex);
                        }
                        //20180109_quytv7_add them user/pass cho node run end

                        nodeRuns.add(nodeRun);
                        List<ParamValue> _paramValueOfNode = mapParamValue.get(subFlowRun + "#" + node.getNodeCode());
                        if (_paramValueOfNode != null) {
                            for (ParamValue paramValue : _paramValueOfNode) {
//                                logger.info("paramValue Code:" + paramValue.getParamCode());
//                                logger.info("paramValue Value:" + paramValue.getParamValue());
                                //paramValue.setFlowRunAction(flowRunAction);
                                paramValue.setNodeRun(nodeRun);
                                paramValue.setCreateTime(new Date());
                                paramValue.setParamValueId(null);
                            }
                            paramValues.addAll(_paramValueOfNode);
                        }
                        if (mapGroupAction.get(subFlowRun + "#" + node.getNodeCode()) != null) {
//                            logger.info(" co vao mapGroupAction size = " + mapGroupAction.get(subFlowRun + "#" + node.getNodeCode()));
                            for (GroupAction groupAction : mapGroupAction.get(subFlowRun + "#" + node.getNodeCode())) {
                                if (groupAction.isDeclare() && groupAction.getActionOfFlows().size() > 0 && !groupAction.isNoCommand(node)) {
                                    NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                                            new NodeRunGroupActionId(node.getNodeId(),
                                                    flowRunAction.getFlowRunId(),
                                                    groupAction.getActionOfFlows().get(0).getStepNum(), subFlowRun), groupAction.getActionOfFlows().get(0), nodeRun);
                                    //20171018_hienhv4_clone dau viec_start
                                    if (!nodeRunGroupActions.contains(nodeRunGroupAction)) {
                                        nodeRunGroupActions.add(nodeRunGroupAction);
                                    }
                                    //20171018_hienhv4_clone dau viec_end
                                }
                            }
//                            logger.info(" thoai khoi mapGroupAction");
                        }
                    }

                } else {

                }
            }

//            logger.info(" xoa session");
//            session.flush();
            session.clear();
            if (!nodeRuns.isEmpty()) {
                flowRunAction.setNodeRuns(nodeRuns);
//                logger.info(" insert NodeRunServiceImpl ");
                sortNodeRun(nodeRuns);
                new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
//                logger.info(" insert ParamValueServiceImpl ");
                new ParamValueServiceImpl().saveOrUpdate(paramValues, session, tx, false);
                session.flush();
                session.clear();
//				for (NodeRunGroupAction nodeRunGroupAction2 : nodeRunGroupActions) {
//		 			//session.merge(nodeRunGroupAction2.getNodeRun());
//					new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupAction2);
//		 			session.merge(nodeRunGroupAction2);
//				}
                new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
//				session.flush();
//		 		tx.commit();
            } else {
                tx.commit();
            }

            //huynx6 added Nov 21, 2016
            //Save File to database
            String file2;
            try {
                file2 = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
            } catch (Exception ex) {
                logger.info(" tao file temp theo WS");
                logger.debug(ex.getMessage(), ex);
                file2 = pathTemplate;
                logger.info("file :" + file2 == null ? "null" : file2);
            }
            logger.info(" luu file thanh cong ");
            File fileTemp2 = new File("tmp" + new Date().getTime() + ".xlsx");
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            exportToFile(file2, fileTemp2, outStream);
            flowRunAction.setFileContent(outStream.toByteArray());
//            if (new SessionUtil().isRescue()) {
//                flowRunAction.setStatus(Config.WAITTING_FLAG);
//            }
            IOUtils.closeQuietly(outStream);
            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
            isExecute = true;
            this.isSaveSucess = true;
            MessageUtil.setInfoMessageFromRes("info.save.dt.success");
            logAction = LogUtils.addContent(logAction, "Result: Success");

            // Cap nhat file DT sang GNOC
            try {
                if (flowRunAction.getCrNumber() != null
                        && !flowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
                        && !flowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
                    if (GNOCService.isCanUpdateDT(flowRunAction.getCrNumber())) {
                        List<String> nodeIPs = new ArrayList<>();
                        if (nodes != null) {
                            for (Node node : nodes) {
                                if (!nodeIPs.contains(node.getNodeIp())) {
                                    nodeIPs.add(node.getNodeIp());
                                }
                            }
                        }

                        Map<String, Object> filters = new HashMap<>();
                        filters.put("crNumber-" + FlowRunActionServiceImpl.EXAC, flowRunAction.getCrNumber());

                        List<FlowRunAction> flowRuns = new FlowRunActionServiceImpl().findList(filters);

                        if (flowRuns != null) {
                            for (FlowRunAction flow : flowRuns) {
                                if (flow.getNodeRuns() != null) {
                                    for (NodeRun nodeRun : flow.getNodeRuns()) {
                                        if (!nodeIPs.contains(nodeRun.getNode().getNodeIp())) {
                                            nodeIPs.add(nodeRun.getNode().getNodeIp());
                                        }
                                    }
                                }
                            }
                        }
                        String filePutForGnoc = getSafeFileName(flowRunAction.getFlowRunId() + "_" + ZipUtils.clearHornUnicode(flowRunAction.getFlowRunName()) + ".xlsx");
                        boolean result = GNOCService.updateDTInfo(flowRunAction.getCreateBy(), flowRunAction.getCrNumber(),
                                nodeIPs.toArray(new String[nodeIPs.size()]), flowRunAction.getFlowRunId() + "", flowRunAction.getFileContent(),
                                filePutForGnoc);

                        logger.info(result ? "UPDATE DT GNOC SUCCESS EXCELL" : "UPDATE DT GNOC FAIL EXCELL");
//                        logger.info("file put for GNOC:" + filePutForGnoc);
//                        }
                        logAction = LogUtils.addContent(logAction, result ? "UPDATE DT GNOC SUCCESS EXCELL" : "UPDATE DT GNOC FAIL EXCELL");
                    }
                }

            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            } finally {
//                if (os != null) {
//                    try {
//                        os.close();
//                    } catch (Exception ex) {
//                        logger.error(ex.getMessage(), ex);
//                    }
//                }
//                if (fileOut1 != null) {
//                    fileOut1.delete();
//                }
//                if (oldFile != null) {
//                    oldFile.delete();
//                }
//                if (newFile != null) {
//                    newFile.delete();
//                }
            }
//            File zipFile = null;
//            try {
//                if (flowRunAction.getCrNumber() != null 
//                        && !flowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
//                        && !flowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
//                    if (GNOCService.isCanUpdateDT(flowRunAction.getCrNumber())) {
//                        String oldFilePath = flowRunAction.getDtFilePath();
//                        
//                        if (oldFilePath != null && !oldFilePath.trim().isEmpty()) {
//                            File oldFile = new File(oldFilePath);
//                            
//                            File fileTemp = new File("tmp" + new Date().getTime() + ".xlsx");
//                            File fileOut = new File(oldFile.getParentFile().getCanonicalPath() + File.separator + flowRunAction.getFlowRunId() 
//                                    + "_" + ZipUtils.clearHornUnicode(flowRunAction.getFlowRunName()) + ".xlsx");
//                            String file = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
//
//                            os = new FileOutputStream(fileOut);
//                            exportToFile(file, fileTemp, os);
//                            os.flush();
//                            
//                            flowRunAction.setDtFilePath(fileOut.getCanonicalPath());
//                            new FlowRunActionServiceImpl().execteBulk("update FlowRunAction a set a.dtFilePath = ? where a.flowRunId = ?", fileOut.getCanonicalPath(), flowRunAction.getFlowRunId());
//                            oldFile.delete();
//
//                            // Bat dau nen file
//                            zipFile = new File(Config.PATH_OUT + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "-exportDt.zip");
//                            ZipUtils.zipFolder(fileOut.getParentFile(), zipFile);
//                            
//                            String[] nodeIPs = null;
//                            if (nodes != null) {
//                                nodeIPs = new String[nodes.size()];
//                                for (int i = 0; i < nodes.size(); i++) {
//                                    nodeIPs[i] = nodes.get(i).getNodeIp();
//                                }
//                            }
//                            boolean result = GNOCService.updateDTInfo(currentUsername, flowRunAction.getCrNumber(),
//                                    nodeIPs, flowRunAction.getFlowRunName(), zipFile);
//
//                            logger.info(flowRunAction.getCrNumber()+": "+ (result ? "UPDATE DT GNOC SUCCESS" : "UPDATE DT GNOC FAIL"));
//                        } else {
//                            File fileTemp = new File("tmp" + new Date().getTime() + ".xlsx");
//                            fileOut1 = new File("tmp_out" + new Date().getTime() + ".xlsx");
//                            String file = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
//
//                            os = new FileOutputStream(fileOut1);
//                            exportToFile(file, fileTemp, os);
//                            os.flush();
//
//                            String[] nodeIPs = null;
//                            if (nodes != null) {
//                                nodeIPs = new String[nodes.size()];
//                                for (int i = 0; i < nodes.size(); i++) {
//                                    nodeIPs[i] = nodes.get(i).getNodeIp();
//                                }
//                            }
//                            boolean result = GNOCService.updateDTInfo(currentUsername, flowRunAction.getCrNumber(),
//                                    nodeIPs, flowRunAction.getFlowRunName(), fileOut1);
//
//                            logger.info(result ? "UPDATE DT GNOC SUCCESS" : "UPDATE DT GNOC FAIL");
//                        }
//                    }
//                }
//            } catch (Exception ex) {
//                logger.error(ex.getMessage(), ex);
//            } finally {
//                if (os != null) {
//                    try {
//                        os.close();
//                    } catch (Exception ex) {
//                        logger.error(ex.getMessage(), ex);
//                    }
//                }
//                if (zipFile != null) {
//                    zipFile.delete();
//                }
//                if (fileOut1 != null) {
//                    fileOut1.delete();
//                }
//            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
            return true;
        } catch (Exception e) {
            if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                tx.rollback();
            }
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.save.dt.fail");
            logAction = LogUtils.addContent(logAction, "Result: Fail");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);

            return false;
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
    }

    public static String getSafeFileName(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != '/' && c != '\\' && c != 0) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public boolean saveDTFromService() {
        //flowRunAction;
        logAction = LogUtils.addContent("", "Save DT");
        try {
            String currentUsername = SessionUtil.getCurrentUsername();
            if (flowRunAction.getCreateBy() != null && !currentUsername.equalsIgnoreCase(flowRunAction.getCreateBy())
                    && !new SessionUtil().isActionAdmin()) {
                MessageUtil.setWarnMessageFromRes("error.no.permission.flow");
                return false;
            }
            flowRunAction.setCreateBy(currentUsername);
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
        }

        List<NodeRun> nodeRuns = new ArrayList<NodeRun>();
        flowRunAction.setCreateDate(new Date());
        if (flowRunAction.getFlowRunName() != null) {
            flowRunAction.setFlowRunName(Jsoup.parse(flowRunAction.getFlowRunName().replaceAll("(?i)<(/?script[^>]*)>", "").trim()).text());
        }
        if (flowRunAction.getDescription() != null) {
            flowRunAction.setDescription(Jsoup.parse(flowRunAction.getDescription().replaceAll("(?i)<(/?script[^>]*)>", "").trim()).text());
            flowRunAction.setDescription(StringEscapeUtils.escapeEcmaScript(flowRunAction.getDescription()));
        }
        flowRunAction.setStatus(0L);


        logAction = LogUtils.addContent(logAction, "Flowrun Id: " + flowRunAction.getFlowRunId());
        logAction = LogUtils.addContent(logAction, "Flowrun Name: " + flowRunAction.getFlowRunName());
        //huynx6 added Nov 7, 2016
        if (flowRunAction.getFileImportDT() != null) {
            try {
                String path = CommonExport.getPathSaveFileExport(flowRunAction.getFileImportDT().getFileName());
                String fileName = path.substring(path.lastIndexOf(File.separator));
                FileOutputStream out = new FileOutputStream(path);
                out.write(flowRunAction.getFileImportDT().getContents());
                out.close();
                flowRunAction.setFileNameImportDt(fileName);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }

        Object[] objs = new FlowRunActionServiceImpl().openTransaction();
        Session session = (Session) objs[0];
        Transaction tx = (Transaction) objs[1];
        try {
            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, tx, false);
            new ParamValueServiceImpl().execteBulk2("delete from ParamValue where nodeRun.id.flowRunId = ?", session, tx, false, flowRunAction.getFlowRunId());
            new NodeRunGroupActionServiceImpl().execteBulk2("delete from NodeRunGroupAction where id.flowRunId = ? ", session, tx, false, flowRunAction.getFlowRunId());
            new NodeRunServiceImpl().execteBulk2("delete from NodeRun where id.flowRunId = ?", session, tx, false, flowRunAction.getFlowRunId());
            List<ParamValue> paramValues = new ArrayList<ParamValue>();

            List<NodeRunGroupAction> nodeRunGroupActions = new ArrayList<NodeRunGroupAction>();
            if (mapSubFlowRunNodes.isEmpty()) {
                mapSubFlowRunNodes.put(Config.SUB_FLOW_RUN_DEFAULT, nodes);
            }
            Long subFlowRunNum = 0L;
            for (String subFlowRun : mapSubFlowRunNodes.keySet()) {
                subFlowRunNum++;
                nodes = mapSubFlowRunNodes.get(subFlowRun);
                sortNode(nodes);
                if (nodes != null && nodes.size() > 0) {
                    for (Node node : nodes) {
                        logger.info("chay vao node :" + node.getNodeCode());
                        NodeRun nodeRun = new NodeRun(new NodeRunId(node.getNodeId(), flowRunAction.getFlowRunId(), subFlowRun), flowRunAction, node, subFlowRunNum);
                        nodeRuns.add(nodeRun);
                        List<ParamValue> _paramValueOfNode = mapParamValue.get(subFlowRun + "#" + node.getNodeCode());
                        if (_paramValueOfNode != null) {
                            for (ParamValue paramValue : _paramValueOfNode) {
//                                logger.info("paramValue Code:" + paramValue.getParamCode());
//                                logger.info("paramValue Value:" + paramValue.getParamValue());
                                //paramValue.setFlowRunAction(flowRunAction);
                                paramValue.setNodeRun(nodeRun);
                                paramValue.setCreateTime(new Date());
                                paramValue.setParamValueId(null);
                            }
                            paramValues.addAll(_paramValueOfNode);
                        }
                        if (mapGroupAction.get(subFlowRun + "#" + node.getNodeCode()) != null) {
//                            logger.info(" co vao mapGroupAction size = " + mapGroupAction.get(subFlowRun + "#" + node.getNodeCode()));
                            for (GroupAction groupAction : mapGroupAction.get(subFlowRun + "#" + node.getNodeCode())) {
                                if (groupAction.isDeclare() && groupAction.getActionOfFlows().size() > 0 && !groupAction.isNoCommand(node)) {
                                    NodeRunGroupAction nodeRunGroupAction = new NodeRunGroupAction(
                                            new NodeRunGroupActionId(node.getNodeId(),
                                                    flowRunAction.getFlowRunId(),
                                                    groupAction.getActionOfFlows().get(0).getStepNum(), subFlowRun), groupAction.getActionOfFlows().get(0), nodeRun);
                                    //20171018_hienhv4_clone dau viec_start
                                    if (!nodeRunGroupActions.contains(nodeRunGroupAction)) {
                                        nodeRunGroupActions.add(nodeRunGroupAction);
                                    }
                                    //20171018_hienhv4_clone dau viec_end
                                }
                            }
//                            logger.info(" thoai khoi mapGroupAction");
                        }
                    }

                } else {

                }
            }

//            logger.info(" xoa session");
//            session.flush();
            session.clear();
            if (!nodeRuns.isEmpty()) {
                flowRunAction.setNodeRuns(nodeRuns);
//                logger.info(" insert NodeRunServiceImpl ");
                sortNodeRun(nodeRuns);
                new NodeRunServiceImpl().saveOrUpdate(nodeRuns, session, tx, false);
//                logger.info(" insert ParamValueServiceImpl ");
                new ParamValueServiceImpl().saveOrUpdate(paramValues, session, tx, false);
                session.flush();
                session.clear();
//				for (NodeRunGroupAction nodeRunGroupAction2 : nodeRunGroupActions) {
//		 			//session.merge(nodeRunGroupAction2.getNodeRun());
//					new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupAction2);
//		 			session.merge(nodeRunGroupAction2);
//				}
                new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActions, session, tx, true);
//				session.flush();
//		 		tx.commit();
            } else {
                tx.commit();
            }

            //huynx6 added Nov 21, 2016
            //Save File to database
            String file2;
            try {
                file2 = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
            } catch (Exception ex) {
                logger.info(" tao file temp theo WS");
                logger.debug(ex.getMessage(), ex);
                file2 = pathTemplate;
                logger.info("file :" + file2 == null ? "null" : file2);
            }
            logger.info(" luu file thanh cong ");
            File fileTemp2 = new File("tmp" + new Date().getTime() + ".xlsx");
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            exportToFile(file2, fileTemp2, outStream);
            flowRunAction.setFileContent(outStream.toByteArray());
            IOUtils.closeQuietly(outStream);
            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
            isExecute = true;
            this.isSaveSucess = true;
            MessageUtil.setInfoMessageFromRes("info.save.dt.success");
            logAction = LogUtils.addContent(logAction, "Result: Success");

            // Cap nhat file DT sang GNOC
            try {
                if (flowRunAction.getCrNumber() != null
                        && !flowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
                        && !flowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
                    if (GNOCService.isCanUpdateDT(flowRunAction.getCrNumber())) {
                        List<String> nodeIPs = new ArrayList<>();
                        if (nodes != null) {
                            for (Node node : nodes) {
                                if (!nodeIPs.contains(node.getNodeIp())) {
                                    nodeIPs.add(node.getNodeIp());
                                }
                            }
                        }

                        Map<String, Object> filters = new HashMap<>();
                        filters.put("crNumber-" + FlowRunActionServiceImpl.EXAC, flowRunAction.getCrNumber());

                        List<FlowRunAction> flowRuns = new FlowRunActionServiceImpl().findList(filters);

                        if (flowRuns != null) {
                            for (FlowRunAction flow : flowRuns) {
                                if (flow.getNodeRuns() != null) {
                                    for (NodeRun nodeRun : flow.getNodeRuns()) {
                                        if (!nodeIPs.contains(nodeRun.getNode().getNodeIp())) {
                                            nodeIPs.add(nodeRun.getNode().getNodeIp());
                                        }
                                    }
                                }
                            }
                        }
                        String filePutForGnoc = getSafeFileName(flowRunAction.getFlowRunId() + "_" + ZipUtils.clearHornUnicode(flowRunAction.getFlowRunName()) + ".xlsx");
                        boolean result = GNOCService.updateDTInfo(flowRunAction.getCreateBy(), flowRunAction.getCrNumber(),
                                nodeIPs.toArray(new String[nodeIPs.size()]), flowRunAction.getFlowRunId() + "", flowRunAction.getFileContent(),
                                filePutForGnoc);

                        logger.info(result ? "UPDATE DT GNOC SUCCESS EXCELL" : "UPDATE DT GNOC FAIL EXCELL");
//                        }
                        logAction = LogUtils.addContent(logAction, result ? "UPDATE DT GNOC SUCCESS EXCELL" : "UPDATE DT GNOC FAIL EXCELL");
                    }
                }

            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            } finally {
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
            return true;
        } catch (Exception e) {
            if (tx.getStatus() != TransactionStatus.ROLLED_BACK && tx.getStatus() != TransactionStatus.COMMITTED) {
                tx.rollback();
            }
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.save.dt.fail");
            logAction = LogUtils.addContent(logAction, "Result: Fail");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);

            return false;
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }

    }

    /**
     * Lay gia tri tham so tham chieu luc sinh kich ban
     *
     * @param flowRunAction
     * @author huynx6
     */
    private void createParamValueInOut(FlowRunAction flowRunAction) {
        Session session = null;
        Transaction transaction = null;
        try {
            //Check truoc khi socket xuong tien trinh
            String sql = "select pit.PARAM_CODE " +
                    "from PARAM_IN_OUT piot, ACTION_OF_FLOW aofw, FLOW_TEMPLATES fts, PARAM_INPUT pit, ACTION_COMMAND acd,\n" +
                    "ACTION_DETAIL adl, COMMAND_DETAIL cdl, action an, ACTION_COMMAND acd_out, ACTION_DETAIL adl_out, COMMAND_DETAIL cdl_out,\n" +
                    "ACTION an_out, ACTION_OF_FLOW aofw_out, FLOW_RUN_ACTION fran\n" +
                    "where piot.ACTION_FLOW_IN_ID = aofw.STEP_NUM and aofw.FLOW_TEMPLATES_ID = fts.FLOW_TEMPLATES_ID\n" +
                    "and piot.PARAM_INPUT_ID = pit.PARAM_INPUT_ID and piot.ACTION_COMMAND_INPUT_ID = acd.ACTION_COMMAND_ID\n" +
                    "and acd.ACTION_DETAIL_ID = adl.DETAIL_ID and acd.COMMAND_DETAIL_ID = cdl.COMMAND_DETAIL_ID\n" +
                    "and adl.ACTION_ID = an.ACTION_ID\n" +
                    "and piot.ACTION_COMMAND_OUTPUT_ID = acd_out.ACTION_COMMAND_ID and acd_out.ACTION_DETAIL_ID = adl_out.DETAIL_ID\n" +
                    "and acd_out.COMMAND_DETAIL_ID = cdl_out.COMMAND_DETAIL_ID and adl_out.ACTION_ID = an_out.ACTION_ID\n" +
                    "and piot.ACTION_FLOW_OUT_ID = aofw_out.STEP_NUM and fts.FLOW_TEMPLATES_ID = fran.FLOW_TEMPLATES_ID\n" +
                    "and fran.FLOW_RUN_ID = ? ";
            List<?> paramInOuts = new ParamInOutServiceImpl().findListSQLAll(sql, flowRunAction.getFlowRunId());
            if (paramInOuts != null && paramInOuts.size() > 0) {
                Object[] objs = new FlowRunActionServiceImpl().openTransaction();
                session = (Session) objs[0];
                transaction = (Transaction) objs[1];

                flowRunAction.setStatus(Config.LOAD_PARAM_FLAG);
                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction, session, transaction, false);
                try {
                    String encrytedMess = new String(Base64.encodeBase64(flowRunAction.getFlowRunId().toString().getBytes()), "UTF-8");
                    //20171018_hienhv4_clone dau viec_start
                    CatCountryBO country = flowRunAction.getCountryCode();
                    DrawTopoStatusExecController.startExecute(encrytedMess, country == null ? Constants.VNM : country.getCountryCode());
                    //20171018_hienhv4_clone dau viec_end
                    
                    transaction.commit();
                    MessageUtil.setInfoMessageFromRes("label.load.param.system.sucess");
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    if (transaction != null && transaction.getStatus() != TransactionStatus.ROLLED_BACK
                            && transaction.getStatus() != TransactionStatus.COMMITTED) {
                        transaction.rollback();
                    }
                    throw ex;
                } finally {
                    if (session != null && session.isOpen()) {
                        session.close();
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("label.load.param.system.error");
        }
    }

    /**
     * Sao chep Kich ban DT
     *
     * @param flowRunAction
     * @author huynx6
     */
    public void cloneFlowRun(FlowRunAction flowRunAction) {
        try {

            logAction = LogUtils.addContent("", "Clone Flow run");
            logAction = LogUtils.addContent(logAction, "FlowRunAction: " + flowRunAction.toString());
            if (flowTemplatesService.findById(flowRunAction.getFlowTemplates().getFlowTemplatesId()).getStatus() != 9) {
                MessageUtil.setErrorMessageFromRes("error.clone.dt.fail");
                MessageUtil.setErrorMessageFromRes("error.template.not.approved");
                logAction = LogUtils.addContent(logAction, "Result: Clone Flow run fail this template is not approved");
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
                return;
            }

            FlowRunAction flowRunActionClone = flowRunAction.clone();

            flowRunActionClone.setCreateBy(SessionUtil.getCurrentUsername());
            Long flowRunId = flowRunAction.getFlowRunId();
            List<NodeRun> nodeRunClones = new NodeRunServiceImpl().findList("from NodeRun where id.flowRunId = ?", -1, -1, flowRunId);
            List<NodeRunGroupAction> nodeRunGroupActionClones = new NodeRunGroupActionServiceImpl().findList("from NodeRunGroupAction where id.flowRunId = ?", -1, -1, flowRunId);
//            logger.info("--------------Get paramValue2s cloneFlowRun.--------------");
            List<ParamValue> paramValueClones = new ParamValueServiceImpl().findList("from ParamValue where nodeRun.id.flowRunId = ?", -1, -1, flowRunId);

            Object[] objs = new FlowRunActionServiceImpl().openTransaction();
            Session session = (Session) objs[0];
            Transaction tx = (Transaction) objs[1];
            //quytv7 Cap nhat file DT export sau khi clone

            new FlowRunActionServiceImpl().saveOrUpdate(flowRunActionClone, session, tx, false);
            Long flowRunIdClone = flowRunActionClone.getFlowRunId();
            for (NodeRun nodeRun : nodeRunClones) {
                nodeRun.getId().setFlowRunId(flowRunIdClone);
            }

            new NodeRunServiceImpl().saveOrUpdate(nodeRunClones, session, tx, false);

            for (NodeRunGroupAction nodeRunGroupAction : nodeRunGroupActionClones) {
                nodeRunGroupAction.getId().setFlowRunId(flowRunIdClone);
            }
            new NodeRunGroupActionServiceImpl().saveOrUpdate(nodeRunGroupActionClones, session, tx, false);

            for (ParamValue paramValue : paramValueClones) {
                paramValue.getNodeRun().getId().setFlowRunId(flowRunIdClone);
                paramValue.setCreateTime(new Date());
                paramValue.setParamValueId(null);
            }

            logAction = LogUtils.addContent(logAction, "List nodeRunClones: " + nodeRunClones.toString());
            logAction = LogUtils.addContent(logAction, "List nodeRunGroupActionClones: " + nodeRunGroupActionClones.toString());
            logAction = LogUtils.addContent(logAction, "List paramValueClones: " + paramValueClones.toString());
            new ParamValueServiceImpl().saveOrUpdate(paramValueClones, session, tx, true);
            this.flowRunAction = flowRunActionClone;
            mapSubFlowRunNodes.clear();
            Long subFlowRunNum = 0L;
            for (NodeRun nodeRun : nodeRunClones) {
//                if(nodeRun.getSubFlowRun() == null || "".enodeRun.getSubFlowRun())
                if (mapSubFlowRunNodes.containsKey(nodeRun.getId().getSubFlowRun())) {
                    mapSubFlowRunNodes.get(nodeRun.getId().getSubFlowRun()).add(nodeRun.getNode());
                } else {
                    subFlowRunNum++;
                    subFlowRuns.add(nodeRun.getId().getSubFlowRun());
                    List<Node> lstNode = new ArrayList<>();
                    lstNode.add(nodeRun.getNode());
                    mapSubFlowRunNodes.put(nodeRun.getId().getSubFlowRun(), lstNode);
                    mapSubFlowRunNum.put(nodeRun.getId().getSubFlowRun(), subFlowRunNum);
                }
            }
            for (String subFlowRun : mapSubFlowRunNodes.keySet()) {
                selectSubflowRun = subFlowRun;
                nodes = mapSubFlowRunNodes.get(subFlowRun);
                break;
            }
            selectedFlowTemplates = flowRunAction.getFlowTemplates();
            //quytv7 toi uu code
//            logger.info("--------------Get nodeRunGroupActions PreEditAction.--------------");
            List<NodeRunGroupAction1> listNodeRunGroupAction = new NodeRunGroupAction1ServiceImpl().findList("from NodeRunGroupAction1 where id.flowRunId =? ", -1, -1, flowRunAction.getFlowRunId());
            Map<String, List<NodeRunGroupAction1>> mapListNodeRunGroupAction = new HashMap<>();
            for (NodeRunGroupAction1 nrga : listNodeRunGroupAction) {
                if (mapListNodeRunGroupAction.containsKey(nrga.getId().getSubFlowRun() + "#" + nrga.getId().getNodeId())) {
                    mapListNodeRunGroupAction.get(nrga.getId().getSubFlowRun() + "#" + nrga.getId().getNodeId()).add(nrga);
                } else {
                    List<NodeRunGroupAction1> nodeRunGroupActionsTemp = new ArrayList<>();
                    nodeRunGroupActionsTemp.add(nrga);
                    mapListNodeRunGroupAction.put(nrga.getId().getSubFlowRun() + "#" + nrga.getId().getNodeId(), nodeRunGroupActionsTemp);
                }
            }

//            logger.info("--------------Get paramValue2s PreEditAction.--------------");
            List<ParamValue1> listParamValue2 = new ParamValue1ServiceImpl().findList("from ParamValue1 where flowRunId =? ", -1, -1, flowRunAction.getFlowRunId());
            Map<String, List<ParamValue1>> mapListParamValue2s = new HashMap<>();
            for (ParamValue1 pv : listParamValue2) {
                if (mapListParamValue2s.containsKey(pv.getSubFlowRun() + "#" + pv.getNodeId())) {
                    mapListParamValue2s.get(pv.getSubFlowRun() + "#" + pv.getNodeId()).add(pv);
                } else {
                    List<ParamValue1> paramValue2sTemp = new ArrayList<>();
                    paramValue2sTemp.add(pv);
                    mapListParamValue2s.put(pv.getSubFlowRun() + "#" + pv.getNodeId(), paramValue2sTemp);
                }
            }

            for (NodeRun nodeRun : nodeRunClones) {
//                nodes.add(nodeRun.getNode());
//                nodeRun.getNode().getMapGroupActions().clear();
                List<GroupAction> listGroupAction = new ArrayList<>();
//                logger.info("--------------Get nodeRunGroupActions from map PreEditAction.--------------");
                List<NodeRunGroupAction1> nodeRunGroupActions = new ArrayList<>();
                if (mapListNodeRunGroupAction.containsKey(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId())) {
                    nodeRunGroupActions = mapListNodeRunGroupAction.get(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId());
                }
//                logger.info("--------------Get nodeRunGroupActions PreEditAction.--------------");
//                List<NodeRunGroupAction> nodeRunGroupActions = new NodeRunGroupActionServiceImpl().findList("from NodeRunGroupAction where id.flowRunId =? and id.nodeId=? and id.subFlowRun = ?", -1, -1, nodeRun.getId().getFlowRunId(), nodeRun.getId().getNodeId(), nodeRun.getId().getSubFlowRun());
                for (String groupName : groupActions.keySet()) {
                    boolean isDeclare = false;
                    List<ActionOfFlow> actionOfFlows = groupActions.get(groupName);
                    List<Long> actionOfFlowIds = new ArrayList<Long>();

                    for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
                        actionOfFlowIds.add(actionOfFlow2.getStepNum());
                    }
//                    logger.info("--------------Get nodeRunGroupActions PreEditAction.--------------");
//                    List<NodeRunGroupAction> nodeRunGroupActions = new NodeRunGroupActionServiceImpl().findList("from NodeRunGroupAction where id.flowRunId =? and id.nodeId=? and id.subFlowRun = ?", -1, -1, nodeRun.getId().getFlowRunId(), nodeRun.getId().getNodeId(), nodeRun.getId().getSubFlowRun());

                    for (NodeRunGroupAction1 nodeRunGroupAction : nodeRunGroupActions) {
                        if (actionOfFlowIds.contains(nodeRunGroupAction.getId().getStepNum())) {
                            isDeclare = true;
                            //Quytv7 toi uu code
                            break;
                        }
                    }
                    List<ActionOfFlow> _actionOfFlows = new LinkedList<>();
                    for (ActionOfFlow actionOfFlow : actionOfFlows) {
                        for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                            if (actionDetail.getNodeType().equals(nodeRun.getNode().getNodeType())
                                    && actionDetail.getVendor().equals(nodeRun.getNode().getVendor())
                                    && actionDetail.getVersion().equals(nodeRun.getNode().getVersion())) {
                                _actionOfFlows.add(actionOfFlow);
                            }
                        }
                    }
                    listGroupAction.add(new GroupAction(groupName, _actionOfFlows, isDeclare));
                }
                nodeRun.getNode().getMapGroupActions().put(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode(), listGroupAction);
                mapGroupAction.put(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode(), listGroupAction);
                //Quytv7_edit_sua loi clone DT khong load duoc sang GNOC start

                getParamInputs(nodeRun.getId().getSubFlowRun(), nodeRun.getNode());

                List<ParamValue> paramValues = mapParamValue.get(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode());
//                logger.info("--------------Get paramValue2s PreEditAction.--------------");
                List<ParamValue1> paramValue2s = new ArrayList<>();
                if (mapListParamValue2s.containsKey(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId())) {
                    paramValue2s = mapListParamValue2s.get(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId());
                }
                for (ParamValue paramValue : paramValues) {
                    for (ParamValue1 paramValue2 : paramValue2s) {
                        if (paramValue.getParamInput().getParamInputId().equals(paramValue2.getParamInputId())) {
                            paramValue.setParamValueId(paramValue2.getParamValueId());
                            paramValue.setParamValue(paramValue2.getParamValue());
                            //Quytv7 toi uu code
                            break;
                        }
                    }
                }
                //List<ParamValue> paramValues = flowRunAction.getParamValues();
                //mapParamValue.put(nodeRun.getNode(), paramValues);
                //mapParamValue.put(nodeRun.getNode(), flowRunAction.getParamValues());
                for (ParamValue paramValue3 : paramValues) {
                    loadParam(paramValue3, paramValues);
                    //onKeyUpValueParam(paramValue3, paramValues);
                }
                //Quytv7_edit_sua loi clone DT khong load duoc sang GNOC end
            }

            loadGroupAction(0l);
            saveFileCloneFlowRun(flowRunActionClone);
            MessageUtil.setInfoMessageFromRes("info.clone.dt.success");
            logAction = LogUtils.addContent(logAction, "Result: Clone dt success");
            onStart();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.clone.dt.fail");
            logAction = LogUtils.addContent(logAction, "Result: Clone dt fail: " + e.getMessage());
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.CLONE.name(), logAction);
    }

    public void saveFileCloneFlowRun(FlowRunAction flowRunActionClone) throws AppException {

        //huynx6 added Nov 21, 2016
        //Save File to database
        String file2 = CommonExport.getTemplateMultiExport(MessageUtil.getResourceBundleMessage("key.template.export.dt"));
        logger.info(" luu file thanh cong ");
        File fileTemp2 = new File("tmp" + new Date().getTime() + ".xlsx");
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        exportToFile(file2, fileTemp2, outStream);
        flowRunActionClone.setFileContent(outStream.toByteArray());
        IOUtils.closeQuietly(outStream);
        new FlowRunActionServiceImpl().saveOrUpdate(flowRunActionClone);
        /*      // Cap nhat file DT sang GNOC
         try {
         if (flowRunAction.getCrNumber() != null
         && !flowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
         && !flowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
         if (GNOCService.isCanUpdateDT(flowRunAction.getCrNumber())) {
         List<String> nodeIPs = new ArrayList<>();
         if (nodes != null) {
         for (Node node : nodes) {
         if (!nodeIPs.contains(node.getNodeIp())) {
         nodeIPs.add(node.getNodeIp());
         }
         }
         }

         Map<String, Object> filters = new HashMap<>();
         filters.put("crNumber-" + FlowRunActionServiceImpl.EXAC, flowRunAction.getCrNumber());

         List<FlowRunAction> flowRuns = new FlowRunActionServiceImpl().findList(filters);

         if (flowRuns != null) {
         for (FlowRunAction flow : flowRuns) {
         if (flow.getNodeRuns() != null) {
         for (NodeRun nodeRun : flow.getNodeRuns()) {
         if (!nodeIPs.contains(nodeRun.getNode().getNodeIp())) {
         nodeIPs.add(nodeRun.getNode().getNodeIp());
         }
         }
         }
         }
         }
         String currentUsername = SessionUtil.getCurrentUsername();
         String filePutForGnoc = getSafeFileName(flowRunAction.getFlowRunId() + "_" + ZipUtils.clearHornUnicode(flowRunAction.getFlowRunName()) + ".xlsx");
         boolean result = GNOCService.updateDTInfo(currentUsername, flowRunAction.getCrNumber(),
         nodeIPs.toArray(new String[nodeIPs.size()]), flowRunAction.getFlowRunId() + "", flowRunAction.getFileContent(),
         filePutForGnoc);

         logger.info(result ? "UPDATE DT GNOC SUCCESS EXCELL" : "UPDATE DT GNOC FAIL EXCELL");
         logger.info("file put for GNOC:" + filePutForGnoc);
         //                        }
         logAction = LogUtils.addContent(logAction, result ? "UPDATE DT GNOC SUCCESS EXCELL" : "UPDATE DT GNOC FAIL EXCELL");
         }
         }

         } catch (Exception ex) {
         logger.error(ex.getMessage(), ex);
         } finally {
         //                if (os != null) {
         //                    try {
         //                        os.close();
         //                    } catch (Exception ex) {
         //                        logger.error(ex.getMessage(), ex);
         //                    }
         //                }
         //                if (fileOut1 != null) {
         //                    fileOut1.delete();
         //                }
         //                if (oldFile != null) {
         //                    oldFile.delete();
         //                }
         //                if (newFile != null) {
         //                    newFile.delete();
         //                }
         }*/

    }

    public void preEditFlowRunAction(FlowRunAction flowRunAction) {
        try {
            logAction = LogUtils.addContent("", "Prepare edit DT");
            // hanhnv68 add 2017_08_18
            isExecute = false;
            // end hanhnv68 add 2017_08_18
            if (nodeSeachs == null) {
                nodeSeachs = new ArrayList<Node>();
            } else {
                nodeSeachs.clear();
            }
            if (tmpNodes == null) {
                tmpNodes = new ArrayList<Node>();
            } else {
                tmpNodes.clear();
            }
            if (preNodes == null) {
                preNodes = new ArrayList<Node>();
            } else {
                preNodes.clear();
            }
            mapParamInputsDistinct.clear();
            try {
                flowRunAction = new FlowRunActionServiceImpl().findById(flowRunAction.getFlowRunId());

            } catch (SysException | AppException e) {
                logger.error(e.getMessage(), e);
            }
            this.flowRunAction = flowRunAction;
            logAction = LogUtils.addContent(logAction, "FlowRunAction: " + flowRunAction.toString());
            loadCR();
            crs.add(new SelectItem(flowRunAction.getCrNumber(), flowRunAction.getCrNumber()));
            if (flowRunAction.getFlowTemplates().getStatus() == 9) {

                selectedFlowTemplates = flowRunAction.getFlowTemplates();
                logAction = LogUtils.addContent(logAction, "selectedFlowTemplates: " + selectedFlowTemplates.toString());
            } else {
                logAction = LogUtils.addContent(logAction, "selectedFlowTemplates: Null ");
                selectedFlowTemplates = null;
            }
            //quytv7 toi uu code
//            logger.info("--------------Get nodeRunGroupActions PreEditAction.--------------");
            List<NodeRunGroupAction1> listNodeRunGroupAction = new NodeRunGroupAction1ServiceImpl().findList("from NodeRunGroupAction1 where id.flowRunId =? ", -1, -1, flowRunAction.getFlowRunId());
            Map<String, List<NodeRunGroupAction1>> mapListNodeRunGroupAction = new HashMap<>();
            for (NodeRunGroupAction1 nrga : listNodeRunGroupAction) {
                if (mapListNodeRunGroupAction.containsKey(nrga.getId().getSubFlowRun() + "#" + nrga.getId().getNodeId())) {
                    mapListNodeRunGroupAction.get(nrga.getId().getSubFlowRun() + "#" + nrga.getId().getNodeId()).add(nrga);
                } else {
                    List<NodeRunGroupAction1> nodeRunGroupActionsTemp = new ArrayList<>();
                    nodeRunGroupActionsTemp.add(nrga);
                    mapListNodeRunGroupAction.put(nrga.getId().getSubFlowRun() + "#" + nrga.getId().getNodeId(), nodeRunGroupActionsTemp);
                }
            }

//            logger.info("--------------Get paramValue2s PreEditAction.--------------");
            List<ParamValue1> listParamValue2 = new ParamValue1ServiceImpl().findList("from ParamValue1 where flowRunId =? ", -1, -1, flowRunAction.getFlowRunId());
            Map<String, List<ParamValue1>> mapListParamValue2s = new HashMap<>();
            for (ParamValue1 pv : listParamValue2) {
                if (mapListParamValue2s.containsKey(pv.getSubFlowRun() + "#" + pv.getNodeId())) {
                    mapListParamValue2s.get(pv.getSubFlowRun() + "#" + pv.getNodeId()).add(pv);
                } else {
                    List<ParamValue1> paramValue2sTemp = new ArrayList<>();
                    paramValue2sTemp.add(pv);
                    mapListParamValue2s.put(pv.getSubFlowRun() + "#" + pv.getNodeId(), paramValue2sTemp);
                }
            }
            nodes.clear();
            mapParamValue.clear();
            groupActions.clear();
            subFlowRuns.clear();
            mapGroupAction.clear();
            onChangeFlowTemplates();
//			if(selectedFlowTemplates!=null)
//				loadGroupAction();

            //Get Map Sub_flow_run with node id
            mapSubFlowRunNodes.clear();
            Long subFlowRunNum = 0L;
            for (NodeRun nodeRun : flowRunAction.getNodeRuns()) {
//                if(nodeRun.getSubFlowRun() == null || "".enodeRun.getSubFlowRun())
                if (mapSubFlowRunNodes.containsKey(nodeRun.getId().getSubFlowRun())) {
                    mapSubFlowRunNodes.get(nodeRun.getId().getSubFlowRun()).add(nodeRun.getNode());
                } else {
                    subFlowRunNum++;
                    subFlowRuns.add(nodeRun.getId().getSubFlowRun());
                    List<Node> lstNode = new ArrayList<>();
                    lstNode.add(nodeRun.getNode());
                    List<NodeRun> lstNodeRun = new ArrayList<>();
                    lstNodeRun.add(nodeRun);
                    mapSubFlowRunNodes.put(nodeRun.getId().getSubFlowRun(), lstNode);
                    mapSubFlowRunNum.put(nodeRun.getId().getSubFlowRun(), subFlowRunNum);
                }
            }
            for (String subFlowRun : mapSubFlowRunNodes.keySet()) {
                selectSubflowRun = subFlowRun;
                nodes = mapSubFlowRunNodes.get(subFlowRun);
                break;
            }
            for (NodeRun nodeRun : flowRunAction.getNodeRuns()) {
//                nodes.add(nodeRun.getNode());
//                nodeRun.getNode().getMapGroupActions().clear();
                List<GroupAction> listGroupAction = new ArrayList<>();
//                logger.info("--------------Get nodeRunGroupActions from map PreEditAction.--------------");
                List<NodeRunGroupAction1> nodeRunGroupActions = new ArrayList<>();
                if (mapListNodeRunGroupAction.containsKey(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId())) {
                    nodeRunGroupActions = mapListNodeRunGroupAction.get(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId());
                }
//                logger.info("--------------Get nodeRunGroupActions PreEditAction.--------------");
//                List<NodeRunGroupAction> nodeRunGroupActions = new NodeRunGroupActionServiceImpl().findList("from NodeRunGroupAction where id.flowRunId =? and id.nodeId=? and id.subFlowRun = ?", -1, -1, nodeRun.getId().getFlowRunId(), nodeRun.getId().getNodeId(), nodeRun.getId().getSubFlowRun());
                for (String groupName : groupActions.keySet()) {
                    boolean isDeclare = false;
                    List<ActionOfFlow> actionOfFlows = groupActions.get(groupName);
                    List<Long> actionOfFlowIds = new ArrayList<Long>();

                    for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
                        actionOfFlowIds.add(actionOfFlow2.getStepNum());
                    }
//                    logger.info("--------------Get nodeRunGroupActions PreEditAction.--------------");
//                    List<NodeRunGroupAction> nodeRunGroupActions = new NodeRunGroupActionServiceImpl().findList("from NodeRunGroupAction where id.flowRunId =? and id.nodeId=? and id.subFlowRun = ?", -1, -1, nodeRun.getId().getFlowRunId(), nodeRun.getId().getNodeId(), nodeRun.getId().getSubFlowRun());

                    for (NodeRunGroupAction1 nodeRunGroupAction : nodeRunGroupActions) {
                        if (actionOfFlowIds.contains(nodeRunGroupAction.getId().getStepNum())) {
                            isDeclare = true;
                            //Quytv7 toi uu code
                            break;
                        }
                    }
                    List<ActionOfFlow> _actionOfFlows = new LinkedList<>();
                    for (ActionOfFlow actionOfFlow : actionOfFlows) {
                        for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                            if (actionDetail.getNodeType().equals(nodeRun.getNode().getNodeType())
                                    && actionDetail.getVendor().equals(nodeRun.getNode().getVendor())
                                    && actionDetail.getVersion().equals(nodeRun.getNode().getVersion())) {
                                _actionOfFlows.add(actionOfFlow);
                            }
                        }
                    }
                    listGroupAction.add(new GroupAction(groupName, _actionOfFlows, isDeclare));

                }
                logAction = LogUtils.addContent(logAction, "NodeRun: " + nodeRun.toString());
                nodeRun.getNode().getMapGroupActions().put(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode(), listGroupAction);
                mapGroupAction.put(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode(), listGroupAction);
//				if(nodes.size()>0)
//					selectedNode = nodes.get(0);

//			for (String string : nodeRun.getNode().getMapGroupActionDeclare().keySet()) {
//				nodeRun.getNode().getMapGroupActionDeclare().put(string, false);
//			}
                getParamInputs(nodeRun.getId().getSubFlowRun(), nodeRun.getNode());
//			for (NodeRunGroupAction nodeRunGroupAction : nodeRun.getNodeRunGroupActions()) {
//				nodeRun.getNode().getMapGroupActionDeclare().put(nodeRunGroupAction.getActionOfFlow().getGroupActionName(), true);
//			}
                List<ParamValue> paramValues = mapParamValue.get(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode());
//                logger.info("--------------Get paramValue2s PreEditAction.--------------");
                List<ParamValue1> paramValue2s = new ArrayList<>();
                if (mapListParamValue2s.containsKey(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId())) {
                    paramValue2s = mapListParamValue2s.get(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getId().getNodeId());
                }
//                logger.info("--------------Get paramValue2s PreEditAction.--------------");
//                List<ParamValue> paramValue2s = new ParamValueServiceImpl().findList("from ParamValue where nodeRun.id.flowRunId =? and nodeRun.id.nodeId=? and nodeRun.id.subFlowRun = ? ", -1, -1, nodeRun.getId().getFlowRunId(), nodeRun.getId().getNodeId(), nodeRun.getId().getSubFlowRun());
                for (ParamValue paramValue : paramValues) {
                    for (ParamValue1 paramValue2 : paramValue2s) {
                        if (paramValue.getParamInput().getParamInputId().equals(paramValue2.getParamInputId())) {
                            paramValue.setParamValueId(paramValue2.getParamValueId());
                            paramValue.setParamValue(paramValue2.getParamValue());
                            //Quytv7 toi uu code
                            break;
                        }
                    }
                }
                //List<ParamValue> paramValues = flowRunAction.getParamValues();
                //mapParamValue.put(nodeRun.getNode(), paramValues);
                //mapParamValue.put(nodeRun.getNode(), flowRunAction.getParamValues());
                for (ParamValue paramValue3 : paramValues) {
                    loadParam(paramValue3, paramValues);
                    //onKeyUpValueParam(paramValue3, paramValues);
                }

            }

            if (!nodes.isEmpty()) {
                sortNode(nodes);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        //onKeyUpValueParam(paramValue, paramValues);
        if (nodes.size() > 0) {
            //20171018_hienhv4_clone dau viec (fix loi load thieu action)_start
            tabview.setActiveIndex(mapSubFlowRunNodes.get(selectSubflowRun).size() - 1);
            selectedNode = nodes.get(nodes.size() - 1);
            //20171018_hienhv4_clone dau viec (fix loi load thieu action)_end
            List<ParamValue> paramValues = mapParamValue.get(selectSubflowRun + "#" + selectedNode.getNodeCode());
            createActionWithMultiParam(paramValues);
        }

        // hanhnv68 add 2017_08_18
        if (this.flowRunAction.getStatus().intValue() == 1) {
            isExecute = true;
        }
        // end hanhnv68 add 2017_08_18

        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
    }

    //20171031_hienhv4_load tham so truoc khi chay_start
    public void loadParamDt() {
        try {
            logAction = LogUtils.addContent("", "LoadParam DT" + flowRunAction.getFlowRunId());
            //Quytv7 sua neu khong save khi phe duyet thi save luon
            if (flowRunAction.getFlowRunId() == null) {
                saveDT();
            }
            //20171106_Quytv7_Sua_load_param_tham so ghep_start
            try {
                flowRunAction.setTimeLoadParam(new Date());
                new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
            //20171106_Quytv7_Sua_load_param_tham so ghep_end


            boolean isCheckHasGetParamNcms = false;
            try {
                for (String subFlowRun : mapSubFlowRunNodes.keySet()) {
                    List<Node> lstNode = mapSubFlowRunNodes.get(subFlowRun);
                    for (Node node : lstNode) {
//                        String paramSucess = "";
//                        String paramFail = "";
                        boolean isCheck = false;
                        List<ParamValue> paramValues = mapParamValue.get(subFlowRun + "#" + node.getNodeCode());
                        List<ParamValue> paramValuesDistinct = mapParamInputsDistinct.get(subFlowRun + "#" + node.getNodeCode());
                        if (paramValuesDistinct == null) {
                            distinctParamValueSameParamCode(subFlowRun, node, paramValues);
                            paramValuesDistinct = mapParamInputsDistinct.get(subFlowRun + "#" + node.getNodeCode());
                        }
                        HashMap<String, ParamValue> mapParamValueDistinct = new HashMap<>();
                        for (ParamValue param : paramValuesDistinct) {
                            mapParamValueDistinct.put(param.getParamCode(), param);
                        }
                        Multimap<CommandDetail, ParamInput> mapCommandDetailParamInput = mapCommandParamInput.get(subFlowRun + "#" + node.getNodeCode());
                        for (CommandDetail commandDetail : mapNodeCommandDetail.get(subFlowRun + "#" + node.getNodeCode())) {
                            if (commandDetail.getSourceGetParam() != null && commandDetail.getSourceGetParam().equals(Config.CMD_SOURCE_GET_PARAM_NCMS)) {
                                isCheckHasGetParamNcms = true;
                                isCheck = true;
                                List<ParamInput> lstParamInputInCmd = new ArrayList<>(mapCommandDetailParamInput.get(commandDetail));
                                HashMap<String, ParamValue> mapParamValueDistinctTemp = NCMSService.loadParamFromNcms(node.getNodeCode(), lstParamInputInCmd, mapParamValueDistinct, commandDetail.getCommandName());
                                for (String paramCode : mapParamValueDistinctTemp.keySet()) {
                                    if (mapParamValueDistinct.containsKey(paramCode)) {
                                        mapParamValueDistinct.get(paramCode).setParamValue(mapParamValueDistinctTemp.get(paramCode).getParamValue());
                                    }
                                }
                            }
                        }
                        if (isCheck) {
                            for (ParamValue paramValue : paramValues) {
                                if (mapParamValueDistinct.containsKey(paramValue.getParamCode())) {
                                    paramValue.setParamValue(mapParamValueDistinct.get(paramValue.getParamCode()).getParamValue());
                                }
                            }
                        }
                    }
                }
                if (isCheckHasGetParamNcms) {
                    saveDT();
                }

            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
            //20171106_Quytv7_Sua_load_param_tham so NCMS_end

            //20171106_Quytv7_Sua_load_param_tham so he thong_start
            createParamValueInOut(flowRunAction);
            logAction = LogUtils.addContent(logAction, "flowRunAction Id" + flowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "flowRunAction name" + flowRunAction.getFlowRunName());
            logAction = LogUtils.addContent(logAction, "Status: " + flowRunAction.getStatus());
            //20171106_Quytv7_Sua_load_param_tham so he thong_end

            //20171106_Quytv7_Sua_load_param_tham so NCMS_start

            logAction = LogUtils.addContent(logAction, "Save success");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("common.fail", MessageUtil.getResourceBundleMessage("view.button.save")));
            logAction = LogUtils.addContent(logAction, "Save fail");
        } finally {

        }
        //20171031_hienhv4_load tham so truoc khi chay_end
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.UPDATE.name(), logAction);
    }

    public void loadGroupActionAll(FlowRunAction flowRunAction) {
        try {
            try {
                flowRunAction = new FlowRunActionServiceImpl().findById(flowRunAction.getFlowRunId());
            } catch (SysException | AppException e) {
                logger.error(e.getMessage(), e);
            }
            this.flowRunAction = flowRunAction;
            loadCR();
            crs.add(new SelectItem(flowRunAction.getCrNumber(), flowRunAction.getCrNumber()));
            if (flowRunAction.getFlowTemplates().getStatus() == 9) {
                selectedFlowTemplates = flowRunAction.getFlowTemplates();
            } else {
                selectedFlowTemplates = null;
            }
            nodes.clear();
            mapParamValue.clear();
            groupActions.clear();
            onChangeFlowTemplates();
            //Get Map Sub_flow_run with node id
            mapSubFlowRunNodes.clear();
            Long subFlowRunNum = 0L;
            for (NodeRun nodeRun : flowRunAction.getNodeRuns()) {
//                if(nodeRun.getSubFlowRun() == null || "".enodeRun.getSubFlowRun())
                if (mapSubFlowRunNodes.containsKey(nodeRun.getId().getSubFlowRun())) {
                    mapSubFlowRunNodes.get(nodeRun.getId().getSubFlowRun()).add(nodeRun.getNode());
                } else {
                    subFlowRunNum++;
                    subFlowRuns.add(nodeRun.getId().getSubFlowRun());
                    List<Node> lstNode = new ArrayList<>();
                    lstNode.add(nodeRun.getNode());
                    List<NodeRun> lstNodeRun = new ArrayList<>();
                    lstNodeRun.add(nodeRun);
                    mapSubFlowRunNodes.put(nodeRun.getId().getSubFlowRun(), lstNode);
                    mapSubFlowRunNum.put(nodeRun.getId().getSubFlowRun(), subFlowRunNum);
                }
            }
            for (String subFlowRun : mapSubFlowRunNodes.keySet()) {
                selectSubflowRun = subFlowRun;
                nodes = mapSubFlowRunNodes.get(subFlowRun);
                break;
            }

            for (NodeRun nodeRun : flowRunAction.getNodeRuns()) {
                List<GroupAction> listGroupAction = new ArrayList<>();
                List<NodeRunGroupAction> nodeRunGroupActions = new NodeRunGroupActionServiceImpl().findList("from NodeRunGroupAction where id.flowRunId =? and id.nodeId=? and id.subFlowRun = ?", -1, -1, nodeRun.getId().getFlowRunId(), nodeRun.getId().getNodeId(), nodeRun.getId().getSubFlowRun());
                for (String groupName : groupActions.keySet()) {
                    boolean isDeclare = false;
                    List<ActionOfFlow> actionOfFlows = groupActions.get(groupName);
                    List<Long> actionOfFlowIds = new ArrayList<Long>();

                    for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
                        actionOfFlowIds.add(actionOfFlow2.getStepNum());
                    }

                    for (NodeRunGroupAction nodeRunGroupAction : nodeRunGroupActions) {
                        if (actionOfFlowIds.contains(nodeRunGroupAction.getId().getStepNum())) {
                            isDeclare = true;
                        }
                    }
                    List<ActionOfFlow> _actionOfFlows = new LinkedList<>();
                    for (ActionOfFlow actionOfFlow : actionOfFlows) {
                        for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                            if (actionDetail.getNodeType().equals(nodeRun.getNode().getNodeType())
                                    && actionDetail.getVendor().equals(nodeRun.getNode().getVendor())
                                    && actionDetail.getVersion().equals(nodeRun.getNode().getVersion())) {
                                _actionOfFlows.add(actionOfFlow);
                            }
                        }
                    }
                    listGroupAction.add(new GroupAction(groupName, _actionOfFlows, isDeclare));

                }
                nodeRun.getNode().getMapGroupActions().put(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode(), listGroupAction);
                mapGroupAction.put(nodeRun.getId().getSubFlowRun() + "#" + nodeRun.getNode().getNodeCode(), listGroupAction);
            }
            if (!nodes.isEmpty()) {
                sortNode(nodes);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void loadGroupAction() {
        loadGroupAction(0l);
    }

    public void sortNode(List<Node> nodes) {
        Collections.sort(nodes, new Comparator<Node>() {

            @Override
            public int compare(Node o1, Node o2) {
                String preName1 = o1.getNodeCode().substring(0, Math.max(o1.getNodeCode().indexOf(" "), 0));
                String preName2 = o2.getNodeCode().substring(0, Math.max(o2.getNodeCode().indexOf(" "), 0));

                String name1 = o1.getNodeCode().substring(o1.getNodeCode().indexOf(" ") + 1);
                String name2 = o2.getNodeCode().substring(o2.getNodeCode().indexOf(" ") + 1);

                String[] tmpPre1s = preName1.split("[,.]", -1);
                String[] tmpPre2s = preName2.split("[,.]", -1);
                int r = comp(tmpPre1s, tmpPre2s);
                if (r == 0) {
                    return name1.trim().compareTo(name2.trim());
                }
                return r;
            }

            public int comp(String[] as, String[] bs) {
                int min = Math.min(as.length, bs.length);
                if (as.length < bs.length) {
                    return 1;
                }
                if (as.length > bs.length) {
                    return -1;
                }
                if (min == 0) {
                    return 0;
                }
                int i = Math.min(0, min - 1);
                if (as[i].equals(bs[i])) {
                    as = ArrayUtils.remove(as, i);
                    bs = ArrayUtils.remove(bs, i);
                    return comp(as, bs);
                }
                return as[i].compareTo(bs[i]);
            }
        });
    }

    public void sortNodeRun(List<NodeRun> nodeRuns) {
        Collections.sort(nodeRuns, new Comparator<NodeRun>() {

            @Override
            public int compare(NodeRun o1, NodeRun o2) {
                String preName1 = o1.getNode().getNodeCode().substring(0, Math.max(o1.getNode().getNodeCode().indexOf(" "), 0));
                String preName2 = o2.getNode().getNodeCode().substring(0, Math.max(o2.getNode().getNodeCode().indexOf(" "), 0));

                String name1 = o1.getNode().getNodeCode().substring(o1.getNode().getNodeCode().indexOf(" ") + 1);
                String name2 = o2.getNode().getNodeCode().substring(o2.getNode().getNodeCode().indexOf(" ") + 1);

                String[] tmpPre1s = preName1.split("[,.]", -1);
                String[] tmpPre2s = preName2.split("[,.]", -1);
                int r = comp(tmpPre1s, tmpPre2s);
                if (r == 0) {
                    return name1.trim().compareTo(name2.trim());
                }
                return r;
            }

            public int comp(String[] as, String[] bs) {
                int min = Math.min(as.length, bs.length);
                if (as.length < bs.length) {
                    return 1;
                }
                if (as.length > bs.length) {
                    return -1;
                }
                if (min == 0) {
                    return 0;
                }
                int i = Math.min(0, min - 1);
                if (as[i].equals(bs[i])) {
                    as = ArrayUtils.remove(as, i);
                    bs = ArrayUtils.remove(bs, i);
                    return comp(as, bs);
                }
                return as[i].compareTo(bs[i]);
            }
        });
    }

    public void loadGroupAction(Long mopType) {
        for (ActionOfFlow actionOfFlow : selectedFlowTemplates.getActionOfFlows()) {
            if (!isActionDuplicate(groupActions.get(actionOfFlow.getGroupActionName()), actionOfFlow)) {
                groupActions.put(actionOfFlow.getGroupActionName(), actionOfFlow);
            }
        }
        nodeActionOffs = new ArrayList<NodeActionOff2>();
        try {
            Map<String, Object> filters = new HashMap<String, Object>();
            filters.put("flowTemplatesId", selectedFlowTemplates.getFlowTemplatesId());
            filters.put("type", mopType);
            nodeActionOffs = new NodeActionOff2Impl().findList(filters);
        } catch (Exception e1) {
            logger.error(e1.getMessage(), e1);
        }
    }

    Set<VendorNodeType> vendorNodeTypes = new HashSet<VendorNodeType>();

    public void onChangeFlowTemplates() {
        flowRunAction.setFlowTemplates(selectedFlowTemplates);
        groupActions = LinkedListMultimap.create();

        if (selectedFlowTemplates != null) {
            loadGroupAction(0l);
        }
        nodeType4Searchs.clear();
        vendor4Searchs.clear();
        version4Searchs.clear();

        vendorNodeTypes.clear();
        if (selectedFlowTemplates != null) {
            List<?> actionOfFlowIds = new ActionOfFlowServiceImpl().findList("select action.actionId from ActionOfFlow where flowTemplates.flowTemplatesId= ? ", -1, -1, selectedFlowTemplates.getFlowTemplatesId());
            //List<Long> actionOfFlowIds = new ArrayList<Long>();
            //for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
            //actionOfFlowIds.add(actionOfFlow2.getStepNum());
            //}
            Map<String, Collection<?>> paramlist = new HashMap<String, Collection<?>>();
            paramlist.put("actionIds", actionOfFlowIds);
            List<ActionDetail> actionDetails = new ActionDetailServiceImpl().findListWithIn("from ActionDetail where action.actionId in (:actionIds)", -1, -1, paramlist);
            //for (ActionOfFlow actionOfFlow : selectedFlowTemplates.getActionOfFlows()) 
            {
                for (ActionDetail actionDetail : actionDetails) {
                    vendorNodeTypes.add(new VendorNodeType(actionDetail.getVendor().getVendorId(),
                            actionDetail.getNodeType().getTypeId(), actionDetail.getVersion().getVersionId()));
                    if (!version4Searchs.contains(actionDetail.getVersion())) {
                        version4Searchs.add(actionDetail.getVersion());
                    }
                    if (!vendor4Searchs.contains(actionDetail.getVendor())) {
                        vendor4Searchs.add(actionDetail.getVendor());
                    }
                    if (!nodeType4Searchs.contains(actionDetail.getNodeType())) {
                        nodeType4Searchs.add(actionDetail.getNodeType());
                    }
                    /*
                     for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                     CommandDetail commandDetail = actionCommand.getCommandDetail();
                     vendorNodeTypes.add(new VendorNodeType(commandDetail.getVendor().getVendorId(), commandDetail.getNodeType().getTypeId()));
                     }*/
                }
            }
            //11102017_Quytv7 them tham so noi suy node mang start
            listParamInterPolateNode = new ArrayList<>();
            if (selectedFlowTemplates.getFormulaInterpolateNode() != null && !"".equals(selectedFlowTemplates.getFormulaInterpolateNode())) {
                if (selectedFlowTemplates.getFormulaInterpolateNode().contains("-->")) {
                    List<String> paramInterpolateNodes = new ArrayList<>();
                    selectedFlowTemplates.setFormulaInterpolateNode(getParamList(selectedFlowTemplates.getFormulaInterpolateNode(), paramInterpolateNodes));
                    for (String paramInterpolateNode : paramInterpolateNodes) {
                        ParamValue1 paramValueInterpolate = new ParamValue1();
                        paramValueInterpolate.setParamCode(paramInterpolateNode);
                        listParamInterPolateNode.add(paramValueInterpolate);
                    }
                }
            }
            //11102017_Quytv7 them tham so noi suy node mang end
        }
        if (nodes == null) {
            nodes = new ArrayList<Node>();
        }
        nodes.clear();

        initLazySearchNode();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void initLazySearchNode() {
        changeCountry();
        changeTypeNode();

    }
    
    public void changeCountry() {
        if (flowRunAction.getCountryCode() != null) {
            Map<String, Object> filters = new HashMap<>();
            filters.put("countryCode.countryCode-" + NodeServiceImpl.EXAC, flowRunAction.getCountryCode().getCountryCode());

            lazyNode = new LazyDataModelSearchNode<>(new NodeServiceImpl(), filters);
            ((LazyDataModelSearchNode) lazyNode).setVendorNodeTypes(vendorNodeTypes);
        } else {
            lazyNode = new LazyDataModelSearchNode<>(new NodeServiceImpl());
            ((LazyDataModelSearchNode) lazyNode).setVendorNodeTypes(vendorNodeTypes);
        }
    }

    public void onChangeTypeNode(int type) {
        if (preNodes.size() > 0) {
            preNodes.get(0).setIsLab(type);

            try {
                new NodeServiceImpl().saveOrUpdate(preNodes.get(0));
                preNodes.clear();
            } catch (SysException | AppException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    @SuppressWarnings("rawtypes")
    public void changeTypeNode() {
        ((LazyDataModelSearchNode) lazyNode).setSearchNodeLab(searchNodeLab);
    }

    public void preSearch() {
        rerender = false;
        nodeSeachs.clear();
        preNodes.clear();
        tmpNodes.clear();
        if (nodes != null) {
            nodeSeachs.addAll(nodes);
        }
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("searchNodeForm:tableSearchNode");
        dataTable.setFirst(0);
        dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("searchNodeForm:tableSelectedNode");
        dataTable.setFirst(0);
    }

    public void addNodeSearch() {
        if (nodeSeachs == null) {
            nodeSeachs = new ArrayList<Node>();
        }
        if (preNodes != null) {
            for (Node node : preNodes) {
                if (!nodeSeachs.contains(node)) {
                    nodeSeachs.add(node);
                }
            }
        }

    }

    public void removeNodeSearch() {
        if (nodeSeachs == null) {
            nodeSeachs = new ArrayList<Node>();
        }
        if (tmpNodes != null) {
            for (Node node : tmpNodes) {
                nodeSeachs.remove(node);
            }
        }
    }

    public void copyNodeSearch() {
        if (nodes == null) {
            nodes = new ArrayList<Node>();
        }
        //nodes.clear();
        for (Node node2 : nodeSeachs) {
            if (!nodes.contains(node2)) {
                nodes.add(node2);
            }
        }
        if (nodeSeachs.size() == 0) {
            nodes.clear();
        }
        for (Node _node : nodes) {
            if (selectSubflowRun == null) {
                loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, _node);
            } else {
                loadGroupAction(selectSubflowRun, _node);
            }

        }
        if (nodes.size() > 0) {
            if (selectSubflowRun == null) {
                handleSelectNode(Config.SUB_FLOW_RUN_DEFAULT, nodes.get(0));
            } else {
                handleSelectNode(selectSubflowRun, nodes.get(0));
            }
        }
        rerender = true;
    }

    public void onchangeNodeInterpolate() {
        try {
            if (selectedFlowTemplates.getFormulaInterpolateNode() != null && selectedFlowTemplates.getFormulaInterpolateNode().contains("-->") && listParamInterPolateNode.size() > 0) {
                Map<String, String> mapValue = new HashMap<>();
                for (ParamValue1 paramValue1 : listParamInterPolateNode) {
                    if (paramValue1.getParamValue() != null && !"".equals(paramValue1.getParamValue())) {
                        mapValue.put(paramValue1.getParamCode(), paramValue1.getParamValue());
                    }
                }
                List<String> array = new ArrayList<>();
                getParamList(selectedFlowTemplates.getFormulaInterpolateNode(), array);

                Map<String, String> mapVar = new HashMap<>();
                for (String str : array) {
                    if (mapValue.containsKey(str)) {
                        mapVar.put(str, mapValue.get(str) == null ? "" : mapValue.get(str).trim());
                    }
                }
                List<String> lstValues = getIfValue(selectedFlowTemplates.getFormulaInterpolateNode(), mapVar, logger);
                if (!lstValues.isEmpty()) {
                    buildNodeFromParamInterpolate(lstValues);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void buildNodeFromParamInterpolate(List<String> listNode) {
        if (this.nodes == null) {
            this.nodes = new ArrayList<Node>();
        }
        List<Node> nodesTemp = new ArrayList<Node>();
        for (String nodeCode : listNode) {
            List<Node> nodes = new ArrayList<Node>();
            try {
                nodes = completeNodeRun(nodeCode, true, true);
                if (nodes.size() != 1) {
                    continue;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }
            try {
                Node node = nodes.get(0);
                if (selectSubflowRun == null) {
                    selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
                    loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                } else {
                    loadGroupAction(selectSubflowRun, node);
                }
                if (mapSubFlowRunNodes == null) {
                    mapSubFlowRunNodes = new HashMap<>();
                }
                if (mapSubFlowRunNodes.get(selectSubflowRun) == null) {
                    mapSubFlowRunNodes.put(selectSubflowRun, new ArrayList<Node>());
                }
                for (Node _node : nodes) {
                    if (!mapSubFlowRunNodes.get(selectSubflowRun).contains(_node)) {
                        mapSubFlowRunNodes.get(selectSubflowRun).add(_node);
                    }
                }
                if (!nodesTemp.contains(node)) {
                    nodesTemp.add(node);
                }
                if (!this.nodes.contains(node)) {
                    this.nodes.add(node);
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                continue;
            }
        }
        for (Node node : nodes) {
            if (!nodesTemp.contains(node)) {
                nodes.remove(node);
            }
        }
    }

    public List<Node> completeNodeRun(String node) {
        return completeNodeRun(node, false, false);
    }

    public List<Node> completeNodeRun(String node, boolean isExac, boolean isOverided) {
        OrderBy orderBy = new OrderBy();
        orderBy.add(Order.asc("nodeCode"));
        ConditionQuery query = new ConditionQuery();
        List<Criterion> predicates = new ArrayList<Criterion>();
        if (node != null && !node.isEmpty()) {
            if (isExac) {
                predicates.add(Restrictions.eq("nodeCode", node));
            } else {
                predicates.add(Restrictions.ilike("nodeCode", node, MatchMode.ANYWHERE));
            }
        }
        query.add(Restrictions.or(predicates.toArray(new Criterion[predicates.size()])));
        List<Long> _nodes = new ArrayList<Long>();
        if (!isOverided && nodes != null) {
            for (Node node1 : nodes) {
                _nodes.add(node1.getNodeId());
            }
            if (!_nodes.isEmpty()) {
                query.add(Restrictions.not(Restrictions.in("nodeId", _nodes)));
            }
        }
        if (vendorNodeTypes.size() > 0) {
            List<Criterion> predicate2s = new ArrayList<Criterion>();
            for (VendorNodeType vendorNodeType : vendorNodeTypes) {
                predicate2s.add(Restrictions.and(Restrictions.eq("vendor.vendorId", vendorNodeType.getVendorId()),
                        Restrictions.eq("nodeType.typeId", vendorNodeType.getNodeTypeId()),
                        //huynx6 edited Oct 10, 2016
                        Restrictions.eq("version.versionId", vendorNodeType.getVersionId())
                ));
            }
            query.add(Restrictions.or(predicate2s.toArray(new Criterion[predicate2s.size()])));
        } else {
            return new ArrayList<Node>();
        }
        
        String coutryCode = flowRunAction.getCountryCode() == null ? Constants.VNM : flowRunAction.getCountryCode().getCountryCode();
        query.add(Restrictions.and(Restrictions.eq("countryCode.countryCode", coutryCode)));
        
        List<Node> findList = new NodeServiceImpl().findList(query, orderBy, 1, 20);
        return findList;

    }

    private boolean isActionDuplicate(List<ActionOfFlow> actionOfFlows, ActionOfFlow actionOfFlow) {
        if (actionOfFlows == null || actionOfFlows.size() == 0) {
            return false;
        }
        for (ActionOfFlow _actionOfFlow : actionOfFlows) {
            if (_actionOfFlow.getAction().equals(actionOfFlow.getAction())
                    && _actionOfFlow.getStepNumberLabel().equals(actionOfFlow.getStepNumberLabel())) {
                return true;
            }
        }
        return false;
    }

    public void quickViewParam(FlowRunAction flowRunAction, Node node, String subFlowRun) {
        String sql = "SELECT DISTINCT pv.PARAM_CODE, "
                + "  CASE WHEN LENGTH(pv.PARAM_VALUE) > 500 THEN to_char(dbms_lob.substr(pv.PARAM_VALUE, 500, 1)) || '...'"
                + " ELSE to_char(dbms_lob.substr(pv.PARAM_VALUE, LENGTH(pv.PARAM_VALUE), 1))"
                + " END PARAM_VALUE"
                + " FROM PARAM_VALUE pv "
                + " WHERE pv.FLOW_RUN_ID = ? "
                + " and pv.node_id = ? "
                + " and LOWER(pv.sub_flow_run) = ? "
                + " ORDER BY pv.PARAM_CODE";
        paramNodes = flowRunActionService.findListSQLAll(sql, flowRunAction.getFlowRunId(), node.getNodeId(), subFlowRun.toLowerCase());
    }

    public void loadParamInputs() {

    }

    private String getParamList(String commandPattern, List<String> lstParam) {
        if (commandPattern != null && !commandPattern.trim().isEmpty() && commandPattern.contains("@{")) {
            int preIndex = 0;
            while (true) {
                preIndex = commandPattern.indexOf("@{", preIndex);

                if (preIndex < 0) {
                    break;
                } else {
                    preIndex += 2;
                }

                int endIndex = commandPattern.indexOf("}", preIndex);

                if (endIndex < 0) {
                    break;
                } else {
                    String param = commandPattern.substring(preIndex, endIndex);
                    commandPattern = commandPattern.replace("@{" + param + "}", "@{" + param.toLowerCase() + "}");
                    if (!lstParam.contains(commandPattern.substring(preIndex, endIndex).toLowerCase())) {
                        lstParam.add(param.toLowerCase());
                    }
                    preIndex = endIndex;
                }
            }
        }

        return commandPattern;
    }

    private List<String> getIfValue(String expressStr, Map<String, String> mapVar, Logger logger) {
        List<String> lstValue = new ArrayList<>();
        try {
            int maxNumber = 1;
            for (String value : mapVar.values()) {
                maxNumber = maxNumber > value.split(";").length ? maxNumber : value.split(";").length;
            }
            for (int i = 0; i < maxNumber; i++) {
                //try {
                Map<String, String> mapVarD = new HashMap<>();
                for (String code : mapVar.keySet()) {
                    String vl = mapVar.get(code);
                    String[] arr = vl.split(";");
                    if (arr.length > 0) {
                        if (i <= arr.length - 1) {
                            mapVarD.put(code, arr[i]);
                        } else {
                            mapVarD.put(code, arr[arr.length - 1]);
                        }
                    }
                }

                String[] ifConditions = expressStr.split(";");
                String value = null;
                boolean paramEnough = true;
                for (String condition : ifConditions) {
                    //try {
                    if (condition.contains("-->")) {
                        String[] arr = condition.split("-->");
                        if (arr != null && arr.length == 2) {
                            String ifBody = arr[0].trim();
                            String ifThen = arr[1].trim();

                            for (String var : mapVarD.keySet()) {
                                ifBody = ifBody.replace("@{" + var + "}", mapVarD.get(var));
                                ifThen = ifThen.replace("@{" + var + "}", mapVarD.get(var));
                            }

//                            ScriptEngineManager factory = new ScriptEngineManager();
//                            ScriptEngine engine = factory.getEngineByName("JavaScript");
                            if (!ifBody.contains("@{")) {
                                Boolean ifValue = Boolean.parseBoolean((new EvalUtil()).evalScript(ifBody));
                                if (ifValue && ifThen != null && !ifThen.contains("@{")) {
                                    value = (new EvalUtil()).evalScript(ifThen).replaceAll("\\.0+$", "");
                                    break;
                                }
                            } else {
                                paramEnough = false;
                            }
                        }
                    }
//                        } catch (Exception ex) {
//                            logger.error(ex.getMessage(), ex);
//                        }
                }

                if (value == null && paramEnough) {
                    for (String condition : ifConditions) {
                        if (condition.toLowerCase().contains("else") && !condition.contains("-->")) {
                            for (String var : mapVarD.keySet()) {
                                condition = condition.replace("@{" + var + "}", mapVarD.get(var));
                            }
                            value = condition.replace("else", "").trim();
                            break;
                        }
                    }
                }
                if (value != null) {
                    if (value.contains(",")) {
                        Collections.addAll(lstValue, value.split(","));
                    } else {
                        lstValue.add(value);
                    }
                }
//                } catch (Exception ex) {
//                    return lstValue;
//                }
            }
            return lstValue;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new ArrayList<>();
        }
    }

    public String onFlowProcess(FlowEvent event) {

        return null;
    }

    //<editor-fold defaultstate="collapsed" desc="Process_GNOC">
    public void getDataFromFileInput(HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFileInput, InputStream inputstream, String fileName, HashMap<String, List<?>> mapCellObjectImports, ResultDTO resultDTO, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) throws Exception {

        objectImports.clear();
        try {
            for (String sheetName : mapImportFileInput.get(fileName).keySet()) {
                selectedFlowTemplates = new FlowTemplatesServiceImpl().findById(mapImportFileInput.get(fileName).get(sheetName).get(0).getTemplateId());
                if (selectedFlowTemplates == null) {
                    resultDTO.setResultCode(1);
                    resultDTO.setResultMessage("Not found template(mop)");
                    return;
                }
                break;
            }

            Workbook workbook = null;
            try {
                if (inputstream == null) {
                    throw new NullPointerException("inputstream is null");
                }
                //Get the workbook instance for XLS/xlsx file
                try {
                    workbook = WorkbookFactory.create(inputstream);
//                if (workbook == null) {
//                    throw new NullPointerException("workbook is null");
//                }
                } catch (InvalidFormatException e2) {
                    logger.error(e2.getMessage(), e2);
                    throw new AppException("File import phải là Excel 97-2012 (xls, xlsx)!");
                } finally {
                    if (workbook != null) {
                        try {
                            workbook.close();
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                }

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new AppException(MessageUtil.getResourceBundleMessage("meassage.import.fail2"));
            }
            List<ObjectImportDt> params = new LinkedList<ObjectImportDt>();
            List<String> sheetNames = new LinkedList<>();
            getContextVar(params, sheetNames);
            int countSheet = 0;
            int countSheetHasData = 0;
            try {
                for (int i = 0; i < sheetNames.size(); i++) {
                    if (!mapImportFileInput.get(fileName).containsKey(sheetNames.get(i))) {
                        continue;
                    } else {
                        countSheet++;
                    }
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    String sheetName = sheetNames.get(i);

                    Importer<Serializable> importer = new Importer<Serializable>() {

                        @Override
                        protected Map<Integer, String> getIndexMapFieldClass() {
                            return null;
                        }

                        @Override
                        protected String getDateFormat() {
                            return null;
                        }
                    };

                    Map<Integer, String> indexMapFieldClass = new HashMap<Integer, String>();
                    Integer key = 2;
                    //20170817_hienhv4_fix import param to mop_start
                    indexMapFieldClass.put(1, MessageUtil.getResourceBundleMessage("key.nodeCode").toLowerCase());
                    //20170817_hienhv4_fix import param to mop_end
                    for (String string : params.get(i).getParamNames()) {
                        indexMapFieldClass.put(key++, string);
                    }

                    importer.setIndexMapFieldClass(indexMapFieldClass);
                    Map<Integer, String> mapHeader = new HashMap<Integer, String>();
                    //20170817_hienhv4_fix import param to mop_start
                    mapHeader.put(1, MessageUtil.getResourceBundleMessage("datatable.header.stt").toLowerCase());
                    importer.setMapHeader(mapHeader);
                    importer.setRowHeaderNumber(6);
                    importer.setIsReplaceSpace(false);
                    //20170817_hienhv4_fix import param to mop_end
                    List<Serializable> objects = importer.getDatas(workbook, sheetName, "1-");
                    String columnValidate = mapImportFileInput.get(fileName).get(sheetName).get(0).getColumnValidate().toLowerCase();
                    if (objects != null) {
                        mapCellObjectImports.put(sheetName, objects);
                        if (mapImportFileInput.get(fileName).get(sheetName) == null || mapImportFileInput.get(fileName).get(sheetName).get(0).getIsValidate() == null || mapImportFileInput.get(fileName).get(sheetName).get(0).getIsValidate().equals(0L)) {
                            continue;
                        }

                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(0);
                        if (!basicDynaBean.getMap().containsKey(MessageUtil.getResourceBundleMessage("key.nodeCode").toLowerCase())) {
                            resultDTO.setResultMessage((resultDTO.getResultMessage() == null ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("key.nodeCode")));
                            resultDTO.setResultCode(1);
                        }
                        if (!basicDynaBean.getMap().containsKey(columnValidate.toLowerCase())) {
                            resultDTO.setResultMessage((resultDTO.getResultMessage() == null ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), columnValidate));
                            resultDTO.setResultCode(1);
                        }
                        if (resultDTO.getResultMessage() != null && !resultDTO.getResultMessage().isEmpty()) {
                            return;
                        }
                        for (int j = 0; j < mapCellObjectImports.get(sheetName).size(); j++) {
                            String resultCode = "OK";
                            String resultDetail = "";
                            LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                            basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(j);
                            int countCheck = 0;
                            for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                                if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()))) {
//                                    if (dynaProperty.getName().equalsIgnoreCase(columnValidate)) {
//                                        resultCode = "NOK";
//                                        resultDetail = (resultDetail == null ? "" : (resultDetail + "\n;")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), columnValidate);
//                                    }
//                                    if (dynaProperty.getName().equalsIgnoreCase(MessageUtil.getResourceBundleMessage("key.nodeCode").toLowerCase())) {
//                                        resultCode = "NOK";
//                                        resultDetail = (resultDetail == null ? "" : (resultDetail + "\n;")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("key.nodeCode").toLowerCase());
//                                    }
                                    mapParam.put(dynaProperty.getName(), "");
                                } else {
                                    if (dynaProperty.getName().equalsIgnoreCase(columnValidate)) {
                                        countCheck++;
                                    }
                                    if (dynaProperty.getName().equalsIgnoreCase(MessageUtil.getResourceBundleMessage("key.nodeCode").toLowerCase())) {
                                        countCheck++;
                                    }
                                    mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                }

                            }
                            if (countCheck >= 2) {
                                countSheetHasData++;
                            } else {
                                continue;
                            }
                            mapParam.put("vmsa_result_code", resultCode);
                            mapParam.put("vmsa_result_detail", resultDetail);
                            ListRowData.add(mapParam);
                        }
                    }
                    if (ListRowData.size() > 0) {
                        mapData.put(sheetName, ListRowData);
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new AppException(MessageUtil.getResourceBundleMessage("error.import.param.fail"));
            }
            if (countSheet != mapImportFileInput.get(fileName).size()) {
                logger.info("countSheet != mapImportFileInput.get(fileName).size(): " + countSheet + "!=" + mapImportFileInput.get(fileName));
                resultDTO.setResultMessage((resultDTO.getResultMessage() == null ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.dataInput.isNull"));
                resultDTO.setResultCode(1);
                return;
            }

            boolean checkHasData = false;
            for (String sheetName : mapImportFileInput.get(fileName).keySet()) {
                if (mapImportFileInput.get(fileName).get(sheetName).size() > 0 && mapImportFileInput.get(fileName).get(sheetName).get(0).getIsValidate() != null && mapImportFileInput.get(fileName).get(sheetName).get(0).getIsValidate().equals(1L)) {
                    checkHasData = true;
                    break;
                }
            }

            if (checkHasData && countSheetHasData == 0) {
                logger.info("checkHasData && countSheetHasData == 0");
                resultDTO.setResultMessage((resultDTO.getResultMessage() == null ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageUtil.getResourceBundleMessage("Message.gnoc.process.validate.result.dataInput.isNull"));
                resultDTO.setResultCode(1);
                return;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    public void CreateMop(HashMap<String, List<StationConfigImport>> mapImportFile, HashMap<String, List<?>> mapCellObjectImports, String crNumber, String _userCreate, String _pathOut, String _pathTemplate, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData, Long creatDtId) throws Exception {
        try {
            HashMap<String, String> mapDataCell = new HashMap<>();
            StationConfigImport stationConfigImport = new StationConfigImport();
            for (String sheetName : mapData.keySet()) {
                for (int i = 0; i < mapData.get(sheetName).size(); i++) {
                    mapDataCell.put(mapData.get(sheetName).get(i).get(mapImportFile.get(sheetName).get(0).getColumnValidate().toLowerCase()), mapImportFile.get(sheetName).get(0).getNetworkType());
                }
                stationConfigImport = mapImportFile.get(sheetName).get(0);
            }
            DataCellUpdateNimsForGnoc dataCellUpdateNimsForGnoc = new DataCellUpdateNimsForGnoc();
            dataCellUpdateNimsForGnoc.setCrNumber(crNumber);
            dataCellUpdateNimsForGnoc.setMapDataCell(mapDataCell);
            dataCellUpdateNimsForGnoc.setClassNimsWS(stationConfigImport.getClassNimsWs());
            dataCellUpdateNimsForGnoc.setFunctionNimsWs(stationConfigImport.getFunctionNimsWs());

            if (mapDataCell.size() > 0) {
                String encrytedMess = new String((new Gson()).toJson(dataCellUpdateNimsForGnoc).getBytes("UTF-8"), "UTF-8");
                flowRunAction.setDataCell(encrytedMess);
            }

            pathOut = _pathOut;
            pathTemplate = _pathTemplate;
            preAddFlowRunAction();
            for (String sheetName : mapImportFile.keySet()) {
                selectedFlowTemplates = new FlowTemplatesServiceImpl().findById(mapImportFile.get(sheetName).get(0).getTemplateId());
                if (selectedFlowTemplates == null) {
                    throw new AppException("Not found template(mop)");
                }
                break;
            }
            onChangeFlowTemplatesProcesGnoc();
            for (String sheetName : mapCellObjectImports.keySet()) {
                ((List<Object>) objectImports).addAll(mapCellObjectImports.get(sheetName));
            }

            pushParamToForm();
            for (String sheetName : mapData.keySet()) {
                flowRunAction.setFlowRunName(crNumber + "_" + mapImportFile.get(sheetName).get(0).getBusinessCode());
                break;
            }
            flowRunAction.setCreateBy(_userCreate);
            flowRunAction.setCreateDtFromGnocId(creatDtId);
            saveDT();
            logger.info("Save DT Sucess, update crNumber: " +crNumber);
            flowRunAction.setCrNumber(crNumber);
            new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    public void onChangeFlowTemplatesProcesGnoc() {
        flowRunAction.setFlowTemplates(selectedFlowTemplates);
        groupActions = LinkedListMultimap.create();

        if (selectedFlowTemplates != null) {
            loadGroupAction(0l);
        }
        nodeType4Searchs.clear();
        vendor4Searchs.clear();
        version4Searchs.clear();

        vendorNodeTypes.clear();
        if (selectedFlowTemplates != null) {
            List<?> actionOfFlowIds = new ActionOfFlowServiceImpl().findList("select action.actionId from ActionOfFlow where flowTemplates.flowTemplatesId= ? ", -1, -1, selectedFlowTemplates.getFlowTemplatesId());
            //List<Long> actionOfFlowIds = new ArrayList<Long>();
            //for (ActionOfFlow actionOfFlow2 : actionOfFlows) {
            //actionOfFlowIds.add(actionOfFlow2.getStepNum());
            //}
            Map<String, Collection<?>> paramlist = new HashMap<String, Collection<?>>();
            paramlist.put("actionIds", actionOfFlowIds);
            List<ActionDetail> actionDetails = new ActionDetailServiceImpl().findListWithIn("from ActionDetail where action.actionId in (:actionIds)", -1, -1, paramlist);
            //for (ActionOfFlow actionOfFlow : selectedFlowTemplates.getActionOfFlows())
            {
                for (ActionDetail actionDetail : actionDetails) {
                    vendorNodeTypes.add(new VendorNodeType(actionDetail.getVendor().getVendorId(),
                            actionDetail.getNodeType().getTypeId(), actionDetail.getVersion().getVersionId()));
                    if (!version4Searchs.contains(actionDetail.getVersion())) {
                        version4Searchs.add(actionDetail.getVersion());
                    }
                    if (!vendor4Searchs.contains(actionDetail.getVendor())) {
                        vendor4Searchs.add(actionDetail.getVendor());
                    }
                    if (!nodeType4Searchs.contains(actionDetail.getNodeType())) {
                        nodeType4Searchs.add(actionDetail.getNodeType());
                    }
                    /*
                     for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                     CommandDetail commandDetail = actionCommand.getCommandDetail();
                     vendorNodeTypes.add(new VendorNodeType(commandDetail.getVendor().getVendorId(), commandDetail.getNodeType().getTypeId()));
                     }*/
                }
            }
            //11102017_Quytv7 them tham so noi suy node mang start
            listParamInterPolateNode = new ArrayList<>();
            if (selectedFlowTemplates.getFormulaInterpolateNode() != null && !"".equals(selectedFlowTemplates.getFormulaInterpolateNode())) {
                if (selectedFlowTemplates.getFormulaInterpolateNode().contains("-->")) {
                    List<String> paramInterpolateNodes = new ArrayList<>();
                    selectedFlowTemplates.setFormulaInterpolateNode(getParamList(selectedFlowTemplates.getFormulaInterpolateNode(), paramInterpolateNodes));
                    for (String paramInterpolateNode : paramInterpolateNodes) {
                        ParamValue1 paramValueInterpolate = new ParamValue1();
                        paramValueInterpolate.setParamCode(paramInterpolateNode);
                        listParamInterPolateNode.add(paramValueInterpolate);
                    }
                }
            }
            //11102017_Quytv7 them tham so noi suy node mang end
        }
        if (nodes == null) {
            nodes = new ArrayList<Node>();
        }
        nodes.clear();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set&Get">


    public String getPathOut() {
        return pathOut;
    }

    public void setPathOut(String pathOut) {
        this.pathOut = pathOut;
    }

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public List<SelectItem> getCrs() {
        return crs;
    }

    public void setCrs(List<SelectItem> crs) {
        this.crs = crs;
    }

    public List<FlowTemplates> getFlowTemplates() {
        return flowTemplates;
    }

    public void setFlowTemplates(List<FlowTemplates> flowTemplates) {
        this.flowTemplates = flowTemplates;
    }

    public FlowTemplates getSelectedFlowTemplates() {
        return selectedFlowTemplates;
    }

    public void setSelectedFlowTemplates(FlowTemplates selectedFlowTemplates) {
        this.selectedFlowTemplates = selectedFlowTemplates;
    }

    public FlowTemplatesServiceImpl getFlowTemplatesService() {
        return flowTemplatesService;
    }

    public void setFlowTemplatesService(FlowTemplatesServiceImpl flowTemplatesService) {
        this.flowTemplatesService = flowTemplatesService;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public NodeServiceImpl getNodeService() {
        return nodeService;
    }

    public void setNodeService(NodeServiceImpl nodeService) {
        this.nodeService = nodeService;
    }

    public LinkedListMultimap<String, ActionOfFlow> getGroupActions() {
        return groupActions;
    }

    public void setGroupActions(LinkedListMultimap<String, ActionOfFlow> groupActions) {
        this.groupActions = groupActions;
    }

    public GenericDaoServiceNewV2<FlowRunAction, Long> getFlowRunActionService() {
        return flowRunActionService;
    }

    public void setFlowRunActionService(GenericDaoServiceNewV2<FlowRunAction, Long> flowRunActionService) {
        this.flowRunActionService = flowRunActionService;
    }

    public LazyDataModel<FlowRunAction> getLazyFlowRunAction() {
        return lazyFlowRunAction;
    }

    public void setLazyFlowRunAction(LazyDataModel<FlowRunAction> lazyFlowRunAction) {
        this.lazyFlowRunAction = lazyFlowRunAction;
    }

    public FlowRunAction getFlowRunAction() {
        return flowRunAction;
    }

    public void setFlowRunAction(FlowRunAction flowRunAction) {
        this.flowRunAction = flowRunAction;
    }

    public Node getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(Node selectedNode) {
        this.selectedNode = selectedNode;
    }

    public Map<String, List<GroupAction>> getMapGroupAction() {
        return mapGroupAction;
    }

    public void setMapGroupAction(Map<String, List<GroupAction>> mapGroupAction) {
        this.mapGroupAction = mapGroupAction;
    }

    public LazyDataModel<Node> getLazyNode() {
        return lazyNode;
    }

    public void setLazyNode(LazyDataModel<Node> lazyNode) {
        this.lazyNode = lazyNode;
    }

    public List<Node> getPreNodes() {
        return preNodes;
    }

    public void setPreNodes(List<Node> preNodes) {
        this.preNodes = preNodes;
    }

    public List<Node> getTmpNodes() {
        return tmpNodes;
    }

    public void setTmpNodes(List<Node> tmpNodes) {
        this.tmpNodes = tmpNodes;
    }

    public List<Node> getNodeSeachs() {
        return nodeSeachs;
    }

    public void setNodeSeachs(List<Node> nodeSeachs) {
        this.nodeSeachs = nodeSeachs;
    }

    public boolean isRerender() {
        return rerender;
    }

    public void setRerender(boolean rerender) {
        this.rerender = rerender;
    }

    public List<Vendor> getVendor4Searchs() {
        return vendor4Searchs;
    }

    public void setVendor4Searchs(List<Vendor> vendor4Searchs) {
        this.vendor4Searchs = vendor4Searchs;
    }

    public List<NodeType> getNodeType4Searchs() {
        return nodeType4Searchs;
    }

    public void setNodeType4Searchs(List<NodeType> nodeType4Searchs) {
        this.nodeType4Searchs = nodeType4Searchs;
    }

    public List<Version> getVersion4Searchs() {
        return version4Searchs;
    }

    public void setVersion4Searchs(List<Version> version4Searchs) {
        this.version4Searchs = version4Searchs;
    }

    public boolean isTabExecute() {
        return isTabExecute;
    }

    public void setTabExecute(boolean isTabExecute) {
        this.isTabExecute = isTabExecute;
    }

    public Integer getSearchNodeLab() {
        return searchNodeLab;
    }

    public void setSearchNodeLab(Integer searchNodeLab) {
        this.searchNodeLab = searchNodeLab;
    }

    public Map<String, List<ParamValue>> getMapParamValue() {
        return mapParamValue;
    }

    public boolean isLoadRingSrt() {
        return loadRingSrt;
    }

    public void setLoadRingSrt(boolean loadRingSrt) {
        this.loadRingSrt = loadRingSrt;
    }

    public List<FlowRunAction> getSelectedFlowRunActions() {
        return selectedFlowRunActions;
    }

    public void setSelectedFlowRunActions(List<FlowRunAction> selectedFlowRunActions) {
        this.selectedFlowRunActions = selectedFlowRunActions;
    }

    public List<?> getParamNodes() {
        return paramNodes;
    }

    public void setParamNodes(List<?> paramNodes) {
        this.paramNodes = paramNodes;
    }

    public Map<String, List<ParamValue>> getMapParamInputsDistinct() {
        return mapParamInputsDistinct;
    }

    public void setMapParamInputsDistinct(Map<String, List<ParamValue>> mapParamInputsDistinct) {
        this.mapParamInputsDistinct = mapParamInputsDistinct;
    }

    public CatCountryBO getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(CatCountryBO selectedCountry) {
        this.selectedCountry = selectedCountry;
    }

    public MapUserCountryServiceImpl getMapUserCountryService() {
        return mapUserCountryService;
    }

    public void setMapUserCountryService(MapUserCountryServiceImpl mapUserCountryService) {
        this.mapUserCountryService = mapUserCountryService;
    }

    public List<String> getSubFlowRuns() {
        return subFlowRuns;
    }

    public void setSubFlowRuns(List<String> subFlowRuns) {
        this.subFlowRuns = subFlowRuns;
    }

    public String getSelectSubflowRun() {
        return selectSubflowRun;
    }

    public void setSelectSubflowRun(String selectSubflowRun) {
        this.selectSubflowRun = selectSubflowRun;
    }

    public Map<String, List<Node>> getMapSubFlowRunNodes() {
        return mapSubFlowRunNodes;
    }

    public void setMapSubFlowRunNodes(Map<String, List<Node>> mapSubFlowRunNodes) {
        this.mapSubFlowRunNodes = mapSubFlowRunNodes;
    }

    public Map<String, Long> getMapSubFlowRunNum() {
        return mapSubFlowRunNum;
    }

    public void setMapSubFlowRunNum(Map<String, Long> mapSubFlowRunNum) {
        this.mapSubFlowRunNum = mapSubFlowRunNum;
    }

    public TabView getTabview() {
        return tabview;
    }

    public void setTabview(TabView tabview) {
        this.tabview = tabview;
    }

    public boolean isExecute() {
        return isExecute;
    }

    public void setExecute(boolean execute) {
        isExecute = execute;
    }

    public boolean isSaveSucess() {
        return this.isSaveSucess;
    }

    public void setSaveSucess(boolean saveSucess) {
        this.isSaveSucess = saveSucess;
    }

    public List<ParamValue1> getListParamInterPolateNode() {
        return listParamInterPolateNode;
    }

    public void setListParamInterPolateNode(List<ParamValue1> listParamInterPolateNode) {
        this.listParamInterPolateNode = listParamInterPolateNode;
    }
    //</editor-fold>
}
