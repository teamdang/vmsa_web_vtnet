package com.viettel.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.model.ActionDetail;
import com.viettel.model.ActionOfFlow;
import com.viettel.model.FlowTemplates;
import com.viettel.model.NodeActionOff;
import com.viettel.model.NodeType;
import com.viettel.persistence.NodeActionOffImpl;
import com.viettel.util.MessageUtil;
import com.viettel.util.Config.NODE_TYPE;
import java.util.Objects;

/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Oct 19, 2016
 * @version 1.0 
 */
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class NodeLayerController implements Serializable {
	List<NodeActionOff> nodeActionOffs = new LinkedList<NodeActionOff>();
	
	@ManagedProperty(value = "#{buildTemplateFlowController}")
	BuildTemplateFlowController buildTemplateFlowController;
	
	@ManagedProperty(value = "#{actionController}")
	ActionController actionController;
        private Long mopType = 0l;
	
	List<NodeType> nodeTypes = new ArrayList<NodeType>();
	
	private List<List<NodeActionOff>> nodeLayers = new LinkedList<>();
	
	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	public void addNodeLayer(){
		NodeActionOff nodeActionOff = new NodeActionOff();
		nodeActionOff.setActionOfFlows(getGroupAction());
		nodeActionOffs.add(nodeActionOff);
	}
	public void buildNodeLayer(){
		getNodeType();
		loadNodeLayer();
		loadNodeActionOff();
	}
	
	public void removeNodeLayer(NodeActionOff nodeActionOff){
		nodeActionOffs.remove(nodeActionOff);
	}
	public void loadNodeLayer(){
		nodeLayers.clear();
		List<ActionOfFlow> groupActions = getGroupAction();
		for (int j=0; j< groupActions.size();j++) {
			List<NodeActionOff> list = new LinkedList<>();
			for (int i=0; i< nodeActionOffs.size();i++) {
				NodeActionOff actionOff = nodeActionOffs.get(i).clone();
				actionOff.setActionOfFlow(groupActions.get(j).clone());
				list.add(actionOff);
			}
			nodeLayers.add(list);
		}
	}
	public void loadNodeActionOff(){
		//nodeActionOffs.clear();
		FlowTemplates selectedFlowTemplate = buildTemplateFlowController.getSelectedFlowTemplate();
		List<ActionOfFlow> groupActions = getGroupAction();
		if(selectedFlowTemplate!=null){
			Map<String, Object> filters = new HashMap<String, Object>();
			filters.put("flowTemplates.flowTemplatesId", selectedFlowTemplate.getFlowTemplatesId());
			filters.put("type", mopType);
			try {
				List<NodeActionOff> nodeActionOffs = new NodeActionOffImpl().findList(filters);
				for (NodeActionOff nodeActionOff : nodeActionOffs) {
					nodeActionOff.getNodeType();
					for (int i = 0; i < groupActions.size(); i++) {
						for (int j = 0; j < this.nodeActionOffs.size(); j++) {
							if(nodeLayers.get(i).get(j).equals(nodeActionOff))
								nodeLayers.get(i).get(j).setDeclare(false);
//							MutableBoolean
						}
					}
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
	}
	
	public List<ActionOfFlow> getGroupAction(){
		List<ActionOfFlow> actionOfFlows = new LinkedList<ActionOfFlow>();
		Map<String, ActionOfFlow> mapGroupAction = new HashMap<String, ActionOfFlow>();
		if(buildTemplateFlowController.getSelectedFlowTemplate()!=null){
			for (ActionOfFlow actionOfFlow : buildTemplateFlowController.getSelectedFlowTemplate().getActionOfFlows()) {
				if(!mapGroupAction.containsKey(actionOfFlow.getGroupActionName())){
					actionOfFlows.add(actionOfFlow.clone());
					mapGroupAction.put(actionOfFlow.getGroupActionName(), actionOfFlow);
				}
			}
		}
		return actionOfFlows;
	}
	
	public void getNodeType(){
		nodeTypes.clear();
		nodeActionOffs.clear();
		FlowTemplates selectedFlowTemplate = buildTemplateFlowController.getSelectedFlowTemplate();
		if(selectedFlowTemplate!=null){
			for (ActionOfFlow actionOfFlow : selectedFlowTemplate.getActionOfFlows()) {
				for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
					if(!nodeTypes.contains(actionDetail.getNodeType())){
						nodeTypes.add(actionDetail.getNodeType());
						if(Objects.equals(actionDetail.getNodeType().getTypeId(), NODE_TYPE.SGSN.value)
                                                        || Objects.equals(actionDetail.getNodeType().getTypeId(), NODE_TYPE.MSC.value)){ 
							nodeActionOffs.add(new NodeActionOff(actionDetail.getNodeType(),1L,selectedFlowTemplate));
							nodeActionOffs.add(new NodeActionOff(actionDetail.getNodeType(),0L,selectedFlowTemplate));
						}else{
							nodeActionOffs.add(new NodeActionOff(actionDetail.getNodeType(),2L,selectedFlowTemplate));
						}
					}
				}
			}
		}
	}
	
	public void saveNodeLayer(){
		Session _session = null;
		Transaction _tx = null;
		try {
			List<NodeActionOff> actionOffs = new ArrayList<>();
			for (List<NodeActionOff> list : nodeLayers) {
				for (NodeActionOff nodeActionOff : list) {
					if(!nodeActionOff.getDeclare()) {
                                            nodeActionOff.setType(mopType);
                                            actionOffs.add(nodeActionOff);
                                        }
				}
			}
			Long flowTemplatesId = buildTemplateFlowController.getSelectedFlowTemplate().getFlowTemplatesId();
			Object[] trs = new NodeActionOffImpl().openTransaction();
			_session = (Session) trs[0];
			_tx = (Transaction) trs[1];
			new NodeActionOffImpl().execteBulk2("delete NodeActionOff where flowTemplates.flowTemplatesId = ? and type = ?", _session, _tx, false,
						flowTemplatesId, mopType);
			new NodeActionOffImpl().saveOrUpdate(actionOffs, _session, _tx,true);
			MessageUtil.setInfoMessageFromRes("info.save.success");
		} catch (Exception e) {
			if(_tx!=null && _tx.getStatus()!=TransactionStatus.ROLLED_BACK)
				_tx.rollback();
			LOGGER.error(e.getMessage(), e);
			MessageUtil.setErrorMessageFromRes("error.save.unsuccess");
		}finally{
			if(_session!=null && _session.isOpen()){
				_session.close();
			}
		}
	}


	public List<NodeActionOff> getNodeActionOffs() {
		return nodeActionOffs;
	}

	public void setNodeActionOffs(List<NodeActionOff> nodeActionOffs) {
		this.nodeActionOffs = nodeActionOffs;
	}

	public BuildTemplateFlowController getBuildTemplateFlowController() {
		return buildTemplateFlowController;
	}

	public void setBuildTemplateFlowController(BuildTemplateFlowController buildTemplateFlowController) {
		this.buildTemplateFlowController = buildTemplateFlowController;
	}

	public ActionController getActionController() {
		return actionController;
	}

	public void setActionController(ActionController actionController) {
		this.actionController = actionController;
	}

	public List<List<NodeActionOff>> getNodeLayers() {
		return nodeLayers;
	}

	public void setNodeLayers(List<List<NodeActionOff>> nodeLayers) {
		this.nodeLayers = nodeLayers;
	}

    public Long getMopType() {
        return mopType;
    }

    public void setMopType(Long mopType) {
        this.mopType = mopType;
    }
	
}
