/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.controller;

import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.NodeType;
import com.viettel.model.ServiceTemplateMapping;
import com.viettel.model.Vendor;
import com.viettel.model.Version;
import com.viettel.persistence.NodeTypeServiceImpl;
import com.viettel.persistence.ServiceTemplateMappingServiceImpl;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.persistence.VersionServiceImpl;
import com.viettel.util.MessageUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hienhv4
 */
@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class ServiceTemplateMapController implements Serializable {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private ServiceTemplateMapping serviceMapping;
    @ManagedProperty("#{serviceTemplateMapping}")
    private ServiceTemplateMappingServiceImpl serviceTemplateMapping;
    private LazyDataModel<ServiceTemplateMapping> lazyDataService;
    private List<Vendor> vendors;
    private List<Version> versions;
    private List<NodeType> nodeTypes;

    @PostConstruct
    public void onStart() {
        lazyDataService = new LazyDataModelBaseNew<>(serviceTemplateMapping);
        try {
            vendors = new VendorServiceImpl().findList();
            versions = new VersionServiceImpl().findList();
            nodeTypes = new NodeTypeServiceImpl().findList();
        } catch (SysException | AppException e) {
            LOGGER.error(e.getMessage(), e);
            vendors = new ArrayList<>();
            versions = new ArrayList<>();
            nodeTypes = new ArrayList<>();
        }
    }

    public void clean() {
        serviceMapping = new ServiceTemplateMapping();
    }

    public void preEdit(ServiceTemplateMapping serviceMapping) {
        this.serviceMapping = serviceMapping;
    }

    public void saveOrUpdate() {
        try {
            serviceTemplateMapping.saveOrUpdate(serviceMapping);
            MessageUtil.setInfoMessageFromRes("common.message.success");
        } catch (AppException | SysException e) {
            LOGGER.error(e.getMessage(), e);
            MessageUtil.setErrorMessageFromRes("error.save.unsuccess");
        }
    }

    public void delete() {
        try {
            serviceTemplateMapping.delete(serviceMapping);
            MessageUtil.setInfoMessageFromRes("common.message.success");
        } catch (AppException | SysException e) {
            LOGGER.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("common.fail", ""));
        }
    }

    public LazyDataModel<ServiceTemplateMapping> getLazyDataService() {
        return lazyDataService;
    }

    public void setLazyDataService(LazyDataModel<ServiceTemplateMapping> lazyDataService) {
        this.lazyDataService = lazyDataService;
    }

    public ServiceTemplateMapping getServiceMapping() {
        return serviceMapping;
    }

    public void setServiceMapping(ServiceTemplateMapping serviceMapping) {
        this.serviceMapping = serviceMapping;
    }

    public ServiceTemplateMappingServiceImpl getServiceTemplateMapping() {
        return serviceTemplateMapping;
    }

    public void setServiceTemplateMapping(ServiceTemplateMappingServiceImpl serviceTemplateMapping) {
        this.serviceTemplateMapping = serviceTemplateMapping;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public List<Version> getVersions() {
        return versions;
    }

    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    public List<NodeType> getNodeTypes() {
        return nodeTypes;
    }

    public void setNodeTypes(List<NodeType> nodeTypes) {
        this.nodeTypes = nodeTypes;
    }
}
