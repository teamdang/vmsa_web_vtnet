/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.controller;

import com.viettel.lazy.LazyDataModelBaseNewV2;
import com.viettel.exception.AppException;
import com.viettel.exception.SysException;
import com.viettel.model.adm.V2AdmAlarmScheduleBO;
import com.viettel.model.adm.V2AdmFuncBO;
import com.viettel.persistence.adm.V2AdmAlarmScheduleServiceImpl;
import com.viettel.persistence.adm.V2AdmFuncServiceImpl;
import com.viettel.util.MessageUtil;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author vipc
 */
@ViewScoped
@ManagedBean
public class AdmAlarmScheduleController implements Serializable {

    private static final String OK = "OK";
    @ManagedProperty(value = "#{v2AdmFuncService}")
    private V2AdmFuncServiceImpl v2AdmFuncServiceImpl;
    @ManagedProperty(value = "#{v2AdmAlarmScheduleService}")
    private V2AdmAlarmScheduleServiceImpl v2AdmAlarmScheduleServiceImpl;
    private LazyDataModel<V2AdmAlarmScheduleBO> lazyModel;
    private V2AdmAlarmScheduleBO selectedObj;
    private V2AdmAlarmScheduleBO addOrUpdateObj = new V2AdmAlarmScheduleBO();
    private List<V2AdmFuncBO> listAdmFunc;
    private boolean isEdit = false;

    @PostConstruct
    public void onStart() {
        search();
    }

    public void prepareEdit(V2AdmAlarmScheduleBO obj, boolean isEdit) {
        this.isEdit = isEdit;
        if (isEdit) {
            addOrUpdateObj = obj.cloneObject();
        } else {
            try {
                addOrUpdateObj = new V2AdmAlarmScheduleBO();
                listAdmFunc = v2AdmFuncServiceImpl.findList();
            } catch (AppException | SysException ex) {
                Logger.getLogger(AdmAlarmScheduleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void search() {
        Map<String, Object> filters = new HashMap<>();
        LinkedHashMap<String, String> orders = new LinkedHashMap<>();
        orders.put("functionCode", "ASC");
        lazyModel = new LazyDataModelBaseNewV2<>(v2AdmAlarmScheduleServiceImpl, filters, orders);
    }

    public void onDelete(V2AdmAlarmScheduleBO obj) {
        try {
            v2AdmAlarmScheduleServiceImpl.delete(obj);
        } catch (Exception e) {
            Logger.getLogger(AdmAlarmScheduleController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public String validateForm(V2AdmAlarmScheduleBO form) {
        //validate form start
        if (form == null) {
            return "Form is NULL";
        } else {
            if (form.getFunctionCode() == null || "".equals(form.getFunctionCode())) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionCode"));
            } else if (form.getFunctionCode().length() > 64) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.maxLength"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionCode"), 64);
            } else if (!form.getFunctionCode().matches("^([_]*[0-9]*[A-Za-z])+([A-Za-z0-9_]*)$")) {
                return MessageFormat.format(MessageUtil.getResourceBundleMessage("common.invalid"),
                        MessageUtil.getResourceBundleMessage("label.adm.functionCode"));
            }
        }
        //validate form end
        return OK;
    }

    public void saveOrUpdate() {
        try {
            String validateForm = validateForm(addOrUpdateObj);
            if (OK.equalsIgnoreCase(validateForm)) {
                V2AdmAlarmScheduleBO bo = createAdmFuncBO(addOrUpdateObj, isEdit);
                v2AdmAlarmScheduleServiceImpl.saveOrUpdate(bo);
                if (isEdit) {
                    MessageUtil.setInfoMessageFromRes("validator.updateSuccess");
                } else {
                    MessageUtil.setInfoMessageFromRes("validator.addSuccess");
                }
                RequestContext.getCurrentInstance().execute("PF('dlgAdd').hide();");
            } else {
                MessageUtil.setWarnMessage(validateForm);
            }
        } catch (AppException e) {
            if (isEdit) {
                MessageUtil.setErrorMessageFromRes("validator.updateFail");
            } else {
                MessageUtil.setErrorMessageFromRes("validator.addFail");
            }
            Logger.getLogger(AdmFuncController.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    public V2AdmAlarmScheduleBO createAdmFuncBO(V2AdmAlarmScheduleBO form, boolean isEdit) throws AppException {
        V2AdmAlarmScheduleBO bo;
        if (form.getId()!= null && !form.getId().equals(0L) && isEdit) {
            bo = v2AdmAlarmScheduleServiceImpl.findById(form.getId());
        } else {
            bo = new V2AdmAlarmScheduleBO();
        }
        bo.setFunctionCode(form.getFunctionCode());
        bo.setStartDate(form.getStartDate());
        bo.setEndDate(form.getEndDate());
        bo.setRepeatTime(form.getRepeatTime());
        bo.setRepeatNumber(form.getRepeatNumber());
        bo.setStatus(form.getStatus());
        return bo;
    }

    //<editor-fold defaultstate="collapsed" desc="setter & getter">

    public V2AdmFuncServiceImpl getV2AdmFuncServiceImpl() {
        return v2AdmFuncServiceImpl;
    }

    public void setV2AdmFuncServiceImpl(V2AdmFuncServiceImpl v2AdmFuncServiceImpl) {
        this.v2AdmFuncServiceImpl = v2AdmFuncServiceImpl;
    }

    public List<V2AdmFuncBO> getListAdmFunc() {
        return listAdmFunc;
    }

    public void setListAdmFunc(List<V2AdmFuncBO> listAdmFunc) {
        this.listAdmFunc = listAdmFunc;
    }

    public V2AdmAlarmScheduleServiceImpl getV2AdmAlarmScheduleServiceImpl() {
        return v2AdmAlarmScheduleServiceImpl;
    }

    public void setV2AdmAlarmScheduleServiceImpl(V2AdmAlarmScheduleServiceImpl v2AdmAlarmScheduleServiceImpl) {
        this.v2AdmAlarmScheduleServiceImpl = v2AdmAlarmScheduleServiceImpl;
    }

    public LazyDataModel<V2AdmAlarmScheduleBO> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<V2AdmAlarmScheduleBO> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public V2AdmAlarmScheduleBO getSelectedObj() {
        return selectedObj;
    }

    public void setSelectedObj(V2AdmAlarmScheduleBO selectedObj) {
        this.selectedObj = selectedObj;
    }

    public V2AdmAlarmScheduleBO getAddOrUpdateObj() {
        return addOrUpdateObj;
    }

    public void setAddOrUpdateObj(V2AdmAlarmScheduleBO addOrUpdateObj) {
        this.addOrUpdateObj = addOrUpdateObj;
    }

    public boolean isIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }
    //</editor-fold>
}
