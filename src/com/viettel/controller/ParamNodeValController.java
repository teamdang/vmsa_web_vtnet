package com.viettel.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.Node;
import com.viettel.model.ParamNodeVal;
import com.viettel.object.ParamNodeValModelExcel;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.ParamNodeValServiceImpl;
import com.viettel.util.Importer;
import com.viettel.util.MessageUtil;

@ViewScoped
@ManagedBean
public class ParamNodeValController {
	
	protected static final Logger logger = LoggerFactory.getLogger(ParamNodeValController.class);

	@ManagedProperty(value="#{paramNodeValService}")
	private ParamNodeValServiceImpl paramNodeValService; 
	
	public void setParamNodeValService(ParamNodeValServiceImpl paramNodeValService) {
		this.paramNodeValService = paramNodeValService;
	}

	private LazyDataModel<ParamNodeVal> lazyParamVal;
	private ParamNodeVal selectedParamVal;
	
	private Node node;
	private boolean isEdit;
	private StreamedContent file;
	
	@PostConstruct
	public void onStart() {
		try {
			InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/templates/import/Template_import_params_node_value.xlsx");
		    file = new DefaultStreamedContent(stream, "application/xls", "Template_import_params_node_value.xlsx");
		        
		    lazyParamVal = new LazyDataModelBaseNew<>(paramNodeValService);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public List<Node> autoCompleNode(String nodeCode) {
		List<Node> lstNode = new ArrayList<>();
		Map<String, Object> filters = new HashMap<>();
		if (nodeCode != null) {
			filters.put("nodeCode", nodeCode);
		}
		try {
			lstNode = new NodeServiceImpl().findList(0, 100, filters);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return lstNode;
	}
	
	public void handUploadFile(FileUploadEvent event) {
		try {
			
			Importer<ParamNodeValModelExcel> importer = new Importer<ParamNodeValModelExcel>() {

				protected Class<ParamNodeValModelExcel> getDomainClass() {
					// TODO Auto-generated method stub
					return ParamNodeValModelExcel.class;
				}

				@Override
				protected Map<Integer, String> getIndexMapFieldClass() {
					// TODO Auto-generated method stub
					Map<Integer, String> model = new HashMap<Integer, String>();
					
					model.put(1, "nodeCode");
					model.put(2, "interfacePort");
					model.put(3, "paramKey");
					model.put(4, "paramVal");
					
					return model;
				}

				@Override
				protected String getDateFormat() {
					// TODO Auto-generated method stub
					return null;
				}
			};
			
			List<ParamNodeValModelExcel> lstParamVal = importer.getDatas(event, 0, "2-");
		
            if (lstParamVal.isEmpty()) {
            	MessageUtil.setErrorMessageFromRes("datatable.empty");
                return;
            } else {
            	if (valFileImport(lstParamVal)) {
            		
            		// Kiem tra neu cung node code thi thuc hien update du lieu
            		List<ParamNodeVal> lstParamSave = new ArrayList<>();
            		
             		for (ParamNodeValModelExcel paramExcel : lstParamVal) {
             			ParamNodeVal param = new ParamNodeVal();
             			param.setInterfacePort(paramExcel.getInterfacePort());
             			param.setNodeCode(paramExcel.getNodeCode());
             			param.setParamKey(paramExcel.getNodeCode());
             			param.setParamValue(paramExcel.getParamVal());
             			
             			lstParamSave.add(param);
            		}
             		
             		if (!lstParamSave.isEmpty()) {
             			paramNodeValService.saveOrUpdate(lstParamSave);
             			MessageUtil.setInfoMessageFromRes("label.action.updateOk");
             		} else {
             			MessageUtil.setErrorMessageFromRes("label.noData.save");
             		}
            	} 
            }
            RequestContext.getCurrentInstance().execute("PF('dlgUploadParamVal').hide()");
		} catch (Exception e) {
			MessageUtil.setErrorMessageFromRes("label.action.updateFail");
			logger.error(e.getMessage(), e);
		}
	}
	
	public boolean valFileImport(List<ParamNodeValModelExcel> lstParamNodeVal) {
		boolean check = true;
		for(ParamNodeValModelExcel param : lstParamNodeVal) {
			try {
				if (param.getInterfacePort() == null 
						|| param.getInterfacePort().trim().isEmpty()
						|| param.getNodeCode() == null
						|| param.getNodeCode().trim().isEmpty()
						|| param.getParamKey() == null 
						|| param.getParamKey().trim().isEmpty()
						|| param.getParamVal() == null
						|| param.getParamVal().trim().isEmpty()) {
					check = false;
				} 
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		return check;
	}
	
	public void onSelectNode(SelectEvent event) {
	}
	
	public void prepareEdit(ParamNodeVal paramNodeVal) {
		selectedParamVal = paramNodeVal;
		try {
			isEdit = true;
			Map<String, Object> filters = new HashMap<>();
			filters.put("nodeCode", paramNodeVal.getNodeCode());
			
			List<Node> lstNode = new NodeServiceImpl().findListExac(filters, null);
			if (lstNode != null && !lstNode.isEmpty()) {
				node = lstNode.get(0);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	public void saveParamVal() {
		try {
			if (validateData()) {
				
				ParamNodeVal paramSave = new ParamNodeVal();
				if (isEdit) {
					paramSave.setId(selectedParamVal.getId());
				} 
				paramSave.setInterfacePort(selectedParamVal.getInterfacePort());
				paramSave.setParamKey(selectedParamVal.getParamKey());
				paramSave.setParamValue(selectedParamVal.getParamValue());
				paramSave.setNodeCode(node.getNodeCode());
				
				paramNodeValService.saveOrUpdate(paramSave);
				clear();
				
				MessageUtil.setInfoMessageFromRes("label.action.updateOk");
				
				RequestContext.getCurrentInstance().execute("PF('dlgParamNodeInfo').hide()");
			}
		} catch (Exception e) {
			MessageUtil.setErrorMessageFromRes("label.action.updateFail");
			logger.error(e.getMessage(), e);
		} 
	}
	
	private boolean validateData() {
		boolean val = true;
		// Kiem tra xem cac truong da duoc nhap day du du lieu chua
		if (node == null
				|| selectedParamVal.getInterfacePort() == null
				|| selectedParamVal.getInterfacePort().trim().isEmpty()
				|| selectedParamVal.getParamKey() == null
				|| selectedParamVal.getParamKey().trim().isEmpty()
				|| selectedParamVal.getParamValue() == null
				|| selectedParamVal.getParamValue().trim().isEmpty()) {
			MessageUtil.setErrorMessageFromRes("label.error.no.input.value");
			val = false;
		}
		
		return val;
	}
	
	public void prepareDelParamVal(ParamNodeVal paramNodeVal) {
		if (paramNodeVal != null) {
			selectedParamVal = paramNodeVal;
		}
	}
	
	public void delParamNodeVal() {
		if (selectedParamVal != null) {
			try {
				paramNodeValService.delete(selectedParamVal);
				selectedParamVal = new ParamNodeVal();
				MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
			} catch (Exception e) {
				MessageUtil.setErrorMessageFromRes("label.action.deleteFail");
				logger.error(e.getMessage(), e);
			}
		}
	}
	
	public void clear() {
		selectedParamVal = new ParamNodeVal();
		node = null;
		isEdit = false;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public boolean isEdit() {
		return isEdit;
	}

	public void setEdit(boolean isEdit) {
		this.isEdit = isEdit;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public LazyDataModel<ParamNodeVal> getLazyParamVal() {
		return lazyParamVal;
	}

	public void setLazyParamVal(LazyDataModel<ParamNodeVal> lazyParamVal) {
		this.lazyParamVal = lazyParamVal;
	}

	public ParamNodeVal getSelectedParamVal() {
		return selectedParamVal;
	}

	public void setSelectedParamVal(ParamNodeVal selectedParamVal) {
		this.selectedParamVal = selectedParamVal;
	}
	
	
}
