package com.viettel.controller;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.viettel.controller.RescueInfomationController.logger;
import com.viettel.exception.AppException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.model.FlowRunAction;
import com.viettel.model.FlowTemplates;
import com.viettel.model.Node;
import com.viettel.model.ParamValue;
import com.viettel.model.StationConfigImport;
import com.viettel.model.ChangeFlowRunAction;
import com.viettel.model.ChangeParam;
import com.viettel.model.ChangePlan;
import com.viettel.model.Vendor;
import com.viettel.object.GroupAction;
import com.viettel.object.MessageException;
import com.viettel.object.StationResult;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.NodeServiceImpl;
import com.viettel.persistence.StationConfigImportlServiceImpl;
import com.viettel.persistence.ChangeFlowRunActionServiceImpl;
import com.viettel.persistence.ChangeParamServiceImpl;
import com.viettel.persistence.ChangePlanServiceImpl;
import com.viettel.persistence.MapNodeServiceImpl;
import com.viettel.persistence.VendorServiceImpl;
import com.viettel.util.AutoStopThreadPool;
import com.viettel.util.Config;
import com.viettel.util.Constants;
import com.viettel.util.Importer;
import com.viettel.util.MessageUtil;
import com.viettel.util.SessionUtil;
import com.viettel.util.SessionWrapper;
import com.viettel.webservice.ncms.client.NCMSWebServiceClient;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * @author Dunglv
 * @sin Mar 10, 2017
 * @version 1.0
 */
@SuppressWarnings("serial")
@ManagedBean(name = "changeParamController")
@ViewScoped
public class ChangeParamController implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="Param">

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private LazyDataModel<ChangePlan> lazyChangePlan;
    @ManagedProperty("#{changePlanService}")
    private ChangePlanServiceImpl changePlanService;
    @ManagedProperty("#{changeParamService}")
    private ChangeParamServiceImpl changeParamService;
    @ManagedProperty("#{mapNodeService}")
    private MapNodeServiceImpl mapNodeServiceImpl;
    @ManagedProperty("#{changeFlowRunActionService}")
    private ChangeFlowRunActionServiceImpl changeFlowRunActionServiceImpl;
    private ChangePlan currChangePlan;
    private ChangePlan currStationRelationPlan;
    private List<ChangePlan> selectChangePlans;
    private List<Vendor> vendors;
    private LazyDataModel<ChangeFlowRunAction> lazyChangeDT;
    private Map<String, List<StationResult>> mapStationResult;
    private Long selectProficientType = 0L;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set&Get">

    public ChangePlan getCurrStationRelationPlan() {
        return currStationRelationPlan;
    }

    public void setCurrStationRelationPlan(ChangePlan currStationRelationPlan) {
        this.currStationRelationPlan = currStationRelationPlan;
    }

    public Long getSelectProficientType() {
        return selectProficientType;
    }

    public void setSelectProficientType(Long selectProficientType) {
        this.selectProficientType = selectProficientType;
    }

    public Map<String, List<StationResult>> getMapStationResult() {
        return mapStationResult;
    }

    public void setMapStationResult(Map<String, List<StationResult>> mapStationResult) {
        this.mapStationResult = mapStationResult;
    }

    public List<ChangePlan> getSelectChangePlans() {
        return selectChangePlans;
    }

    public void setSelectChangePlans(List<ChangePlan> selectChangePlans) {
        this.selectChangePlans = selectChangePlans;
    }

    public LazyDataModel<ChangePlan> getLazyChangePlan() {
        return lazyChangePlan;
    }

    public void setLazyChangePlan(LazyDataModel<ChangePlan> lazyChangePlan) {
        this.lazyChangePlan = lazyChangePlan;
    }

    public ChangePlanServiceImpl getChangePlanService() {
        return changePlanService;
    }

    public void setChangePlanService(ChangePlanServiceImpl changePlanService) {
        this.changePlanService = changePlanService;
    }

    public MapNodeServiceImpl getMapNodeServiceImpl() {
        return mapNodeServiceImpl;
    }

    public void setMapNodeServiceImpl(MapNodeServiceImpl mapNodeServiceImpl) {
        this.mapNodeServiceImpl = mapNodeServiceImpl;
    }

    public ChangePlan getCurrChangePlan() {
        return currChangePlan;
    }

    public void setCurrChangePlan(ChangePlan currChangePlan) {
        this.currChangePlan = currChangePlan;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public LazyDataModel<ChangeFlowRunAction> getLazyChangeDT() {
        return lazyChangeDT;
    }

    public void setLazyChangeDT(LazyDataModel<ChangeFlowRunAction> lazyChangeDT) {
        this.lazyChangeDT = lazyChangeDT;
    }

    public ChangeParamServiceImpl getChangeParamService() {
        return changeParamService;
    }

    public void setChangeParamService(ChangeParamServiceImpl changeParamService) {
        this.changeParamService = changeParamService;
    }

    public ChangeFlowRunActionServiceImpl getChangeFlowRunActionServiceImpl() {
        return changeFlowRunActionServiceImpl;
    }

    public void setChangeFlowRunActionServiceImpl(ChangeFlowRunActionServiceImpl changeFlowRunActionServiceImpl) {
        this.changeFlowRunActionServiceImpl = changeFlowRunActionServiceImpl;
    }

    //</editor-fold>
    @PostConstruct
    public void onStart() {
        try {
            Map<String, Object> filters = new HashMap<>();
            Map<String, String> orders = new LinkedHashMap<>();
            Map<String, String> orderVendor = new LinkedHashMap<>();
            filters.put("eventType", selectProficientType);
            orders.put("updateTime", "DESC");
            orders.put("crNumber", "ASC");
            orderVendor.put("vendorName", "ASC");
            currChangePlan = new ChangePlan();
            lazyChangePlan = new LazyDataModelBaseNew<>(changePlanService, filters, orders);
            vendors = new VendorServiceImpl().findList(null, orderVendor);
            mapStationResult = new HashMap<>();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    /**
     * Import plan
     *
     * @author quytv7
     *
     */
    //<editor-fold defaultstate="collapsed" desc="Plan">
    public void handleImportChangePlan(FileUploadEvent event) throws Exception {
        HashMap<String, List<?>> mapCellObjectImports = new HashMap<String, List<?>>();
        try {
            if (!validateInput(currChangePlan)) {
                MessageUtil.setWarnMessageFromRes("error.import.data.input");
                throw new Exception(MessageUtil.getResourceBundleConfig("error.import.data.input"));
            }
            //Lay cau hinh import
            Map<String, Object> filters = new HashMap<String, Object>();
            filters.put("vendorName", currChangePlan.getVendor().getVendorName());
            filters.put("networkType", currChangePlan.getNetworkType());
            if (selectProficientType.equals(Config.CHANGE_PARAM_FESTIVAL)) {
                filters.put("isRule", Config.IS_RULE_CHANGE_FESTIVAL);
            }
            if (selectProficientType.equals(Config.CHANGE_PARAM_NCMS)) {
                filters.put("isRule", Config.IS_RULE_CHANGE_NCMS);
            }
            List<StationConfigImport> stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
            final HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile = new HashMap<>();
            HashMap<String, List<StationConfigImport>> mapImportSheet;
            List<StationConfigImport> lstStationConfigImports;
            if (selectProficientType.equals(Config.CHANGE_PARAM_NCMS)) {
                for (StationConfigImport temp : stationConfigImports) {
                    temp.setFileName(event.getFile().getFileName());
                    stationConfigImports.clear();
                    stationConfigImports.add(temp);
                    break;
                }
            }
            for (StationConfigImport stationConfigImport : stationConfigImports) {
                if (mapImportFile.containsKey(stationConfigImport.getFileName())) {
                    mapImportSheet = mapImportFile.get(stationConfigImport.getFileName());
                    if (mapImportSheet.containsKey(stationConfigImport.getSheetName())) {
                        mapImportSheet.get(stationConfigImport.getSheetName()).add(stationConfigImport);
                    } else {
                        lstStationConfigImports = new ArrayList<>();
                        lstStationConfigImports.add(stationConfigImport);
                        mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                    }
                    mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
                } else {
                    mapImportSheet = new HashMap<>();
                    lstStationConfigImports = new ArrayList<>();
                    lstStationConfigImports.add(stationConfigImport);
                    mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                    mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
                }
            }
            if (mapImportFile.containsKey(event.getFile().getFileName())) {
                Workbook workbook = null;
                try {
                    InputStream inputstream = event.getFile().getInputstream();

                    if (inputstream == null) {
                        throw new NullPointerException("inputstream is null");
                    }
                    //Get the workbook instance for XLS/xlsx file 
                    try {
                        workbook = WorkbookFactory.create(inputstream);
//                        if (workbook == null) {
//                            throw new NullPointerException("workbook is null");
//                        }
                    } catch (InvalidFormatException e2) {
                        LOGGER.debug(e2.getMessage(), e2);
                        throw new AppException("File import phải là Excel 97-2012 (xls, xlsx)!");
                    }

                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    MessageUtil.setErrorMessageFromRes("meassage.import.fail2");
                    return;
                } finally {
                    if (workbook != null) {
                        try {
                            workbook.close();
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                    }
                }

                try {
                    for (String key : mapImportFile.get(event.getFile().getFileName()).keySet()) {
                        List<?> objectImports = new LinkedList<>();
                        Importer<Serializable> importer = new Importer<Serializable>() {

                            @Override
                            protected Map<Integer, String> getIndexMapFieldClass() {
                                return null;
                            }

                            @Override
                            protected String getDateFormat() {
                                return null;
                            }
                        };
                        List<Serializable> objects = null;
                        if (selectProficientType.equals(Config.CHANGE_PARAM_FESTIVAL)) {
                            importer.setRowHeaderNumber(1);
                            importer.setIsReplace(false);
                            objects = importer.getDatas(workbook, key, "2-");
                        }
                        if (selectProficientType.equals(Config.CHANGE_PARAM_NCMS)) {
                            importer.setRowHeaderNumber(6);
                            objects = importer.getDatas(workbook, key, "7-");
                        }


                        if (objects != null) {
                            ((List<Object>) objectImports).addAll(objects);
                            mapCellObjectImports.put(key, objectImports);
                        } else {
                            throw new AppException(MessageUtil.getResourceBundleConfig("error.import.sheetname"));
                        }

                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    MessageUtil.setErrorMessageFromRes("error.import.param.fail");
                    throw e;
                }
                //Lay xong tham so do vao bang mapCellObjectImports
                filters.clear();
                filters.put("vendor.vendorId", currChangePlan.getVendor().getVendorId());
                if (selectProficientType.equals(Config.CHANGE_PARAM_FESTIVAL)) {
                    processImportedParamsFestival(event.getFile().getFileName(), mapImportFile, mapCellObjectImports);
                } else if (selectProficientType.equals(Config.CHANGE_PARAM_NCMS)) {
                    processImportedParamsNCMS(event.getFile().getFileName(), mapImportFile, mapCellObjectImports);
                }

            }
            RequestContext.getCurrentInstance().execute("PF('addChangeDlg').hide()");
        } catch (Exception ex) {
            if (ex instanceof MessageException) {
                MessageUtil.setErrorMessage(ex.getMessage());
            } else {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }

    private void processImportedParamsFestival(String importedFileName, final HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            Map<String, BasicDynaBean> rncParamMap = new HashMap<>();
            Map<String, Map<String, BasicDynaBean>> rncNodeBParamMap = new HashMap<>();
            Map<String, Map<String, BasicDynaBean>> rncCellParamMap = new HashMap<>();
            Map<String, Map<String, String>> rncParamMapForMop = new HashMap<>();
            Map<String, String> rncMopParam;
            final Map<String, String> nodeBIpMap = new HashMap<>();
            List<String> rncInRncSheet = new ArrayList<>();
            List<String> rncInNodeBSheet = new ArrayList<>();
            List<String> rncInCellSheet = new ArrayList<>();

            long now = System.currentTimeMillis();

            // Get data from imported xls file
            for (String sheetName : mapImportFile.get(importedFileName).keySet()) {
                List<BasicDynaBean> rows = (List<BasicDynaBean>) mapCellObjectImports.get(sheetName);
                if (sheetName.toUpperCase().contains("RNC")) {
                    for (BasicDynaBean row : rows) {
                        String rnc = (String) row.get("rnc");
                        rncParamMap.put(rnc, row);

                        if (!rncInRncSheet.contains(rnc)) {
                            rncInRncSheet.add(rnc);
                        }
                    }
                } else if (sheetName.toUpperCase().contains("NODEB")) {
                    Map<String, BasicDynaBean> nodeBParamMap;
                    for (BasicDynaBean row : rows) {
                        String rnc = (String) row.get("rnc");
                        String nodeB = (String) row.get("nodeb_name");
                        if (null == rncNodeBParamMap.get(rnc)) {
                            nodeBParamMap = new HashMap<>();
                            nodeBParamMap.put(nodeB, row);
                            rncNodeBParamMap.put(rnc, nodeBParamMap);
                        } else {
                            nodeBParamMap = rncNodeBParamMap.get(rnc);
                            nodeBParamMap.put(nodeB, row);
                        }
                    }
                    for (String rnc : rncNodeBParamMap.keySet()) {
                        if (null == rncParamMap.get(rnc)) {
                            rncParamMap.put(rnc, null);
                        }
                        if (!rncInNodeBSheet.contains(rnc)) {
                            rncInNodeBSheet.add(rnc);
                        }
                    }
                } else if (sheetName.toUpperCase().contains("CELL")) {
                    Map<String, BasicDynaBean> cellParamMap;
                    for (BasicDynaBean row : rows) {
                        String rnc = (String) row.get("rnc");
                        String cell = (String) row.get("cell");
                        if (null == rncCellParamMap.get(rnc)) {
                            cellParamMap = new HashMap<>();
                            cellParamMap.put(cell, row);
                            rncCellParamMap.put(rnc, cellParamMap);
                        } else {
                            cellParamMap = rncCellParamMap.get(rnc);
                            cellParamMap.put(cell, row);
                        }
                    }
                    for (String rnc : rncCellParamMap.keySet()) {
                        if (null == rncParamMap.get(rnc)) {
                            rncParamMap.put(rnc, null);
                        }
                        if (!rncInCellSheet.contains(rnc)) {
                            rncInCellSheet.add(rnc);
                        }
                    }
                }
            }

            // Insert data to database
            currChangePlan.setUpdateTime(new Date());
            currChangePlan.setUserCreate(SessionWrapper.getCurrentUsername());

            // Process with change detail
            final List<ChangeParam> changeParams = new ArrayList<>();
            ChangeParam changeParam;

            // Add all RNC param
            for (String rnc : rncParamMap.keySet()) {
                rncMopParam = new HashMap<>();
                BasicDynaBean rncRow = rncParamMap.get(rnc);

                if (null != rncRow) {
                    for (Object object : rncRow.getMap().entrySet()) {
                        // Build param for saving to DB
                        Map.Entry entry = (Map.Entry) object;
                        String paramCode = (String) entry.getKey();
                        //Quytv7_Replace lai dung gia tri ("__", " "), ("___",".")
                        paramCode = paramCode.replaceAll("___", ".").replaceAll("__", " ");
                        String paramValue = (String) entry.getValue();
                        if (!"rnc".equals(paramCode)) {
                            changeParam = new ChangeParam();
                            changeParam.setChangePlan(currChangePlan);
                            changeParam.setParamCode(paramCode);
                            changeParam.setParamValueInput(paramValue);
                            changeParam.setParamType(Constants.ParamType.RNC);
                            changeParam.setRnc(rnc);
                            changeParam.setUpdateTime(new Date());
                            changeParams.add(changeParam);

                            // Build param for new mop
                            if (null == rncMopParam.get(paramCode)) {
                                rncMopParam.put(paramCode, paramValue);
                            } else {
                                String value = rncMopParam.get(paramCode) + ";" + paramValue;
                                rncMopParam.put(paramCode, value);
                            }
                        }
                    }
                }

                // Build param for new mop
                if (null == rncMopParam.get("timernc")) {
                    rncMopParam.put("timernc", String.valueOf(now++));
                } else {
                    String value = rncMopParam.get("timernc") + ";" + String.valueOf(now++);
                    rncMopParam.put("timernc", value);
                }

                // Add param map for each Rnc
                rncParamMapForMop.put(rnc, rncMopParam);
            }

            // Add all NODEB param
            for (String rnc : rncNodeBParamMap.keySet()) {
                rncMopParam = rncParamMapForMop.get(rnc);
                if (null == rncMopParam) {
                    rncMopParam = new HashMap<>();
                    rncParamMapForMop.put(rnc, rncMopParam);
                }
                Map<String, BasicDynaBean> nodeBParamMap = rncNodeBParamMap.get(rnc);

                if (null != nodeBParamMap) {
                    for (String nodeB : nodeBParamMap.keySet()) {
                        BasicDynaBean nodeBRow = nodeBParamMap.get(nodeB);
                        for (Object object : nodeBRow.getMap().entrySet()) {
                            Map.Entry entry = (Map.Entry) object;
                            String paramCode = (String) entry.getKey();
                            //Quytv7_Replace lai dung gia tri ("__", " "), ("___",".")
                            paramCode = paramCode.replaceAll("___", ".").replaceAll("__", " ");
                            String paramValue = (String) entry.getValue();
                            if (!"rnc".equals(paramCode) && !"nodeb_name".equals(paramCode)) {
                                changeParam = new ChangeParam();
                                changeParam.setChangePlan(currChangePlan);
                                changeParam.setParamCode(paramCode);
                                changeParam.setParamValueInput(paramValue);
                                if ((Constants.ParamCodeNodeBInRnc.dlHwAdm).equalsIgnoreCase(paramCode) || (Constants.ParamCodeNodeBInRnc.ulHwAdm).equalsIgnoreCase(paramCode)) {
                                    changeParam.setParamType(Constants.ParamType.NODEBInRnc);
                                    changeParam.setCell(nodeB);
                                } else {
                                    changeParam.setParamType(Constants.ParamType.NODEB);
                                }
                                changeParam.setRnc(rnc);
                                changeParam.setNodeb(nodeB);
                                changeParam.setUpdateTime(new Date());
                                changeParams.add(changeParam);

                                // Build param for new mop
                                if (null == rncMopParam.get(paramCode)) {
                                    rncMopParam.put(paramCode, paramValue);
                                } else {
                                    String value = rncMopParam.get(paramCode) + ";" + paramValue;
                                    rncMopParam.put(paramCode, value);
                                }
                            }
                        }

                        // Build param for new mop
                        if (null == rncMopParam.get("nodeb")) {
                            rncMopParam.put("nodeb", nodeB);
                        } else {
                            String value = rncMopParam.get("nodeb") + ";" + nodeB;
                            rncMopParam.put("nodeb", value);
                        }

                        // Build param for new mop
                        if (null == rncMopParam.get("time")) {
                            rncMopParam.put("time", String.valueOf(now++));
                        } else {
                            String value = rncMopParam.get("time") + ";" + String.valueOf(now++);
                            rncMopParam.put("time", value);
                        }
                    }

                    // Get nodeb ip
                    String[] nodebList = rncMopParam.get("nodeb").split(";");
                    for (int i = 0; i < nodebList.length; i++) {
                        String sql = "select distinct NODEB_IP from Map_Node where RNC_BSC = ? and NODEB = ?";
                        List<String> nodeBIps = (List<String>) mapNodeServiceImpl.findListSQLAll(sql, rnc, nodebList[i]);
                        if (null != nodeBIps && nodeBIps.size() > 0) {
                            String nodebIp = nodeBIps.get(0);

                            String[] nodebIpArr = nodebIp.split("\\.");
                            String octet = String.valueOf(Integer.valueOf(nodebIpArr[1]) + 2);
                            nodebIp = nodebIpArr[0] + "." + octet + "." + nodebIpArr[2] + "." + nodebIpArr[3];

                            nodeBIpMap.put(nodebList[i], nodebIp);

                            // Build param for new mop
                            if (null == rncMopParam.get("nodebip")) {
                                rncMopParam.put("nodebip", nodebIp);
                            } else {
                                String value = rncMopParam.get("nodebip") + ";" + nodebIp;
                                rncMopParam.put("nodebip", value);
                            }
                        }
                    }
                }
            }

            // Add all CELL param
            for (String rnc : rncCellParamMap.keySet()) {
                rncMopParam = rncParamMapForMop.get(rnc);
                if (null == rncMopParam) {
                    rncMopParam = new HashMap<>();
                    rncParamMapForMop.put(rnc, rncMopParam);
                }

                Map<String, BasicDynaBean> cellParamMap = rncCellParamMap.get(rnc);

                if (null != cellParamMap) {
                    for (String cell : cellParamMap.keySet()) {
                        BasicDynaBean cellRow = cellParamMap.get(cell);
                        for (Object object : cellRow.getMap().entrySet()) {
                            Map.Entry entry = (Map.Entry) object;
                            String paramCode = (String) entry.getKey();
                            //Quytv7_Replace lai dung gia tri ("__", " "), ("___",".")
                            paramCode = paramCode.replaceAll("___", ".").replaceAll("__", " ");
                            String paramValue = (String) entry.getValue();
                            if (!"rnc".equals(paramCode) && !"cell".equals(paramCode)) {
                                changeParam = new ChangeParam();
                                changeParam.setChangePlan(currChangePlan);
                                changeParam.setParamCode(paramCode);
                                changeParam.setParamValueInput(paramValue);
                                changeParam.setParamType(Constants.ParamType.CELL);
                                changeParam.setRnc(rnc);
                                changeParam.setCell(cell);
                                changeParam.setUpdateTime(new Date());
                                changeParams.add(changeParam);

                                // Build param for new mop
                                if (null == rncMopParam.get(paramCode)) {
                                    rncMopParam.put(paramCode, paramValue);
                                } else {
                                    String value = rncMopParam.get(paramCode) + ";" + paramValue;
                                    rncMopParam.put(paramCode, value);
                                }
                            }
                        }

                        // Build param for new mop
                        if (null == rncMopParam.get("cell")) {
                            rncMopParam.put("cell", cell);
                        } else {
                            String value = rncMopParam.get("cell") + ";" + cell;
                            rncMopParam.put("cell", value);
                        }
                    }
                }
            }

            // Save change plan
            currChangePlan.setEventType((long) Constants.EventType.CHANGE_PARAM);
            changePlanService.saveOrUpdate(currChangePlan);

            // Save param
            changeParamService.saveOrUpdate(changeParams);

            // Sinh mop RNC
            createFlowRunActionChangeParam(rncParamMapForMop, mapImportFile.get(importedFileName).get("Tham so RNC"), rncInRncSheet, rncInNodeBSheet, rncInCellSheet);

            // Call NCMS web service for getting old param values
//            new NCMSWebServiceClient().requestOldParams(changeParams, nodeBIpMap);
            final AutoStopThreadPool autoStopThreadPool = new AutoStopThreadPool("callWSNCMS", 50);

            autoStopThreadPool.addTask(new Runnable() {
                @Override
                public void run() {
                    //Gan them truong nodeType: 1: BSC;2:RNC
                    if (("3G").equalsIgnoreCase(currChangePlan.getNetworkType())) {
                        new NCMSWebServiceClient().requestOldParams(2L, changeParams, nodeBIpMap);
                    } else {
                        new NCMSWebServiceClient().requestOldParams(1L, changeParams, nodeBIpMap);
                    }
                }
            });

            new Thread(new Runnable() {

                @Override
                public void run() {
                    autoStopThreadPool.execute();
                    logger.info("callWSNCMS: Done");
                }
            }).start();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    private void processImportedParamsNCMS(String importedFileName, final HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            List<ChangeParam> changeParams = new ArrayList<>();
            Map<String, Map<String, String>> rncParamMapForMop = new HashMap<>();
            Map<Long, Set<String>> mapParamTypeRnc = new HashMap<>();
            // Get data from imported xls file
            for (String sheetName : mapImportFile.get(importedFileName).keySet()) {
                List<BasicDynaBean> rows = (List<BasicDynaBean>) mapCellObjectImports.get(sheetName);
                //Validate file import
                for (BasicDynaBean row : rows) {
                    try {
                        if (!row.getMap().containsKey(MessageUtil.getResourceBundleMessage("label.changeParam.header.node.name").toLowerCase())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.changeParam.ncms.header.fail"), MessageUtil.getResourceBundleMessage("label.changeParam.header.node.name")));
                        }
                        if (!row.getMap().containsKey(MessageUtil.getResourceBundleMessage("label.changeParam.header.cell.name").toLowerCase())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.changeParam.ncms.header.fail"), MessageUtil.getResourceBundleMessage("label.changeParam.header.cell.name")));
                        }
                        if (!row.getMap().containsKey(MessageUtil.getResourceBundleMessage("label.changeParam.header.station.code").toLowerCase())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.changeParam.ncms.header.fail"), MessageUtil.getResourceBundleMessage("label.changeParam.header.station.code")));
                        }
                        if (!row.getMap().containsKey(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.code").toLowerCase())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.changeParam.ncms.header.fail"), MessageUtil.getResourceBundleMessage("label.changeParam.header.param.code")));
                        }
                        if (!row.getMap().containsKey(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.value").toLowerCase())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.changeParam.ncms.header.fail"), MessageUtil.getResourceBundleMessage("label.changeParam.header.param.value")));
                        }
                        if (!row.getMap().containsKey(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.type").toLowerCase())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.changeParam.ncms.header.fail"), MessageUtil.getResourceBundleMessage("label.changeParam.header.param.type")));
                        }
                        if (!row.getMap().containsKey(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.standard").toLowerCase())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.changeParam.ncms.header.fail"), MessageUtil.getResourceBundleMessage("label.changeParam.header.param.standard")));
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                    break;
                }

                ChangeParam changeParam;
                for (BasicDynaBean row : rows) {
//                    try {
                    changeParam = new ChangeParam();
                    changeParam.setRnc(convertObjectToString(row.getMap().get(MessageUtil.getResourceBundleMessage("label.changeParam.header.node.name").toLowerCase())));
                    changeParam.setCell(convertObjectToString(row.getMap().get(MessageUtil.getResourceBundleMessage("label.changeParam.header.cell.name").toLowerCase())));
                    changeParam.setNodeb(convertObjectToString(row.getMap().get(MessageUtil.getResourceBundleMessage("label.changeParam.header.station.code").toLowerCase())));
                    changeParam.setParamCode(convertObjectToString(row.getMap().get(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.code").toLowerCase())));
                    changeParam.setParamValueOld(convertObjectToString(row.getMap().get(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.value").toLowerCase())));
                    changeParam.setParamType(convertObjectToLong(row.getMap().get(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.type").toLowerCase())));
                    changeParam.setParamValueInput(convertObjectToString(row.getMap().get(MessageUtil.getResourceBundleMessage("label.changeParam.header.param.standard").toLowerCase())));
                    changeParams.add(changeParam);
//                    } catch (Exception ex) {
//                        logger.error(ex.getMessage(), ex);
//                    }
                }
                logger.info("List ChangeParamNcms size = " + changeParams.size());
            }

            // Insert data to database
            currChangePlan.setUpdateTime(new Date());
            currChangePlan.setUserCreate(SessionWrapper.getCurrentUsername());
            // Save change plan
            currChangePlan.setEventType((long) Constants.EventType.CHANGE_PARAM);
            changePlanService.saveOrUpdate(currChangePlan);

            // Save param
            changeParamService.saveOrUpdate(changeParams);

            // Sinh mop RNC
            //Phan tich data
            Map<String, String> mapParam;
            for (ChangeParam changeParam : changeParams) {
                //Gan map Param type, co 3 muc: 0: RNC; 1: NodeB; 2: Cell
                if (mapParamTypeRnc.containsKey(changeParam.getParamType())) {
                    mapParamTypeRnc.get(changeParam.getParamType()).add(changeParam.getRnc());
                } else {
                    Set<String> setTemp = new HashSet<>();
                    setTemp.add(changeParam.getRnc());
                    mapParamTypeRnc.put(changeParam.getParamType(), setTemp);
                }
                // Get Param
                if (rncParamMapForMop.containsKey(changeParam.getRnc())) {
                    mapParam = rncParamMapForMop.get(changeParam.getRnc());
                    if (mapParam.containsKey(changeParam.getParamCode().toLowerCase())) {
                        mapParam.put(changeParam.getParamCode().toLowerCase(), mapParam.get(changeParam.getParamCode().toLowerCase()) + Config.SPLITTER_VALUE + changeParam.getParamValueInput());
                    } else {
                        mapParam.put(changeParam.getParamCode().toLowerCase(), changeParam.getParamValueInput());
                    }
                    if (Config.ParamTypeCell.equals(changeParam.getParamType())) {
                        if (mapParam.containsKey("cell")) {
                            if (!(Config.SPLITTER_VALUE + mapParam.get("cell") + Config.SPLITTER_VALUE).contains((Config.SPLITTER_VALUE + changeParam.getCell() + Config.SPLITTER_VALUE))) {
                                mapParam.put("cell", (mapParam.get("cell") + Config.SPLITTER_VALUE + changeParam.getCell()));
                            }
                        } else {
                            mapParam.put("cell", changeParam.getCell());
                        }
                    }
                    if (Config.ParamTypeNodeB.equals(changeParam.getParamType())) {
                        if (mapParam.containsKey("nodeb")) {
                            if (!(Config.SPLITTER_VALUE + mapParam.get("nodeb") + Config.SPLITTER_VALUE).contains((Config.SPLITTER_VALUE + changeParam.getNodeb() + Config.SPLITTER_VALUE))) {
                                mapParam.put("nodeb", (mapParam.get("nodeb") + Config.SPLITTER_VALUE + changeParam.getNodeb()));
                                mapParam.put("nodebip", (mapParam.get("nodebip") + Config.SPLITTER_VALUE + getIpNodeB(changeParam.getRnc(), changeParam.getCell())));
                            }
                        } else {
                            mapParam.put("nodeb", changeParam.getNodeb());
                            mapParam.put("nodebip", getIpNodeB(changeParam.getRnc(), changeParam.getCell()));
                        }
                    }
                } else {
                    mapParam = new HashMap<>();
                    mapParam.put(changeParam.getParamCode().toLowerCase(), changeParam.getParamValueInput());
                    rncParamMapForMop.put(changeParam.getRnc(), mapParam);
                    if (Config.ParamTypeCell.equals(changeParam.getParamType())) {
                        mapParam.put("cell", changeParam.getCell());
                    }
                    //Get ip node b
                    if (Config.ParamTypeNodeB.equals(changeParam.getParamType())) {
                        mapParam.put("nodeb", changeParam.getNodeb());
                        mapParam.put("nodebip", getIpNodeB(changeParam.getRnc(), changeParam.getCell()));
                    }
                }
            }

            createFlowRunActionChangeParamNcms(rncParamMapForMop, mapImportFile.get(importedFileName).get("Tham so RNC"), mapParamTypeRnc);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    private boolean createFlowRunActionChangeParam(Map<String, Map<String, String>> mapRncParam, List<StationConfigImport> stationConfigImports, List<String> rncInRncSheet, List<String> rncInNodeBSheet, List<String> rncInCellSheet) throws MessageException, Exception {
        GenerateFlowRunController generateFlowRunController;
        List<ChangeFlowRunAction> changeFlowRunActions = new ArrayList<>();
        FlowTemplates flowTemplates = null;

        try {
            for (String rnc : mapRncParam.keySet()) {
                if ("3G".equals(currChangePlan.getNetworkType())) {
                    flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
                } else {
                    flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
                }

                if (flowTemplates == null) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                    continue;
                } else {
                    if (flowTemplates.getStatus() != 9) {
                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                        continue;
                    }
                }

                try {
                    String filePutOSS;
                    if ("3G".equals(currChangePlan.getNetworkType())) {
                        filePutOSS = currChangePlan.getCrNumber().toUpperCase()
                                + "-Change_Param_Festival-" + currChangePlan.getVendor().getVendorName().toUpperCase()
                                + "-" + currChangePlan.getNetworkType().toUpperCase()
                                + "-RNC_" + rnc.toUpperCase()
                                + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    } else {
                        filePutOSS = currChangePlan.getCrNumber().toUpperCase()
                                + "-Change_Param_Festival-" + currChangePlan.getVendor().getVendorName().toUpperCase()
                                + "-" + currChangePlan.getNetworkType().toUpperCase()
                                + "-BSC_" + rnc.toUpperCase()
                                + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    }
                    generateFlowRunController = new GenerateFlowRunController();
                    FlowRunAction flowRunAction = new FlowRunAction();
                    flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
                    flowRunAction.setFlowRunName(filePutOSS);
                    filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                    while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                        flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                    }
                    flowRunAction.setTimeRun(new Date());
                    flowRunAction.setFlowTemplates(flowTemplates);
                    flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());
                    flowRunAction.setRollbackMode(1L);
                    generateFlowRunController.setFlowRunAction(flowRunAction);
                    generateFlowRunController.setSelectedFlowTemplates(flowTemplates);

                    //Lay danh sach param tu bang 
                    generateFlowRunController.setNodes(new ArrayList<Node>());
                    generateFlowRunController.loadGroupAction(0l);
                    List<Node> nodeInPlan;
                    HashMap<String, Object> filters = new HashMap<>();
                    filters.put("nodeCode", rnc);
                    List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                    Node nodeRncBsc;
                    if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                        throw new Exception(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), rnc));
                    } else {
                        nodeRncBsc = nodeRncs.get(0);
                    }

                    Map<String, String> mapParamValues = mapRncParam.get(rnc);
                    for (String key : mapParamValues.keySet()) {
                        LOGGER.info("Pre map key: " + key);
                        LOGGER.info("Pre map value: " + mapParamValues.get(key));
                    }

                    if ("3G".equals(currChangePlan.getNetworkType())) {
                        mapParamValues.put("file_name", filePutOSS);
                        mapParamValues.put("rnc", rnc);
                    }

                    nodeInPlan = new ArrayList<>();
                    generateFlowRunController.getNodes().add(nodeRncBsc);
                    nodeInPlan.add(nodeRncBsc);

                    for (Node node : nodeInPlan) {
                        generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                        List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                        for (ParamValue paramValue : paramValues) {
                            LOGGER.info("Show ParamCode: " + paramValue.getParamCode());

                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }

                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim()));
                                LOGGER.info("Param code: " + paramValue.getParamCode().toLowerCase().trim());
                                LOGGER.info("Param value: " + value);
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }

                            ResourceBundle bundle = ResourceBundle.getBundle("cas");
                            if (bundle.getString("service").contains("10.61.127.190")) {
                                if (value == null || value.toString().isEmpty()) {
                                    value = "TEST_NOT_FOUND";
                                }
                            }
                            if (value != null) {
                                LOGGER.info("Param value set: " + value);
                                paramValue.setParamValue(value.toString());
                            }
                        }
                    }
                    mapParamValues.clear();

                    for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                        if (groupAction.getGroupActionName().equals(MessageUtil.getResourceBundleMessage("label.changeParam.run.dt.nodeB")) && !rncInNodeBSheet.contains(rnc)) {
                            groupAction.setDeclare(false);
                        }
                        if (groupAction.getGroupActionName().equals(MessageUtil.getResourceBundleMessage("label.changeParam.run.dt.rnc")) && !rncInCellSheet.contains(rnc)) {
                            groupAction.setDeclare(false);
                        }
                    }

                    boolean saveDT = generateFlowRunController.saveDT();
                    if (saveDT) {
                        try {
                            // Save flow to database                           
                            ChangeFlowRunAction changeFlowRunAction = new ChangeFlowRunAction();
                            changeFlowRunAction.setFlowRunAction(flowRunAction);
                            changeFlowRunAction.setNode(nodeRncBsc);
                            changeFlowRunAction.setChangePlan(currChangePlan);
                            changeFlowRunAction.setType(1L);
                            changeFlowRunAction.setUpdateTime(new Date());
                            changeFlowRunActions.add(changeFlowRunAction);

                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), rnc));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                    }
                } catch (Exception e) {
                    if (e instanceof MessageException) {
                        MessageUtil.setErrorMessage(e.getMessage());
                    } else {
                        LOGGER.error(e.getMessage(), e);
                    }
                    throw new Exception(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                } finally {
                    //Tam biet
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
        changeFlowRunActionServiceImpl.saveOrUpdate(changeFlowRunActions);
        return true;
    }

    private boolean createFlowRunActionChangeParamNcms(Map<String, Map<String, String>> mapRncParam, List<StationConfigImport> stationConfigImports, Map<Long, Set<String>> mapParamType) throws MessageException, Exception {
        GenerateFlowRunController generateFlowRunController;
        List<ChangeFlowRunAction> changeFlowRunActions = new ArrayList<>();
        FlowTemplates flowTemplates = null;

        try {
            for (String rnc : mapRncParam.keySet()) {
                if ("3G".equals(currChangePlan.getNetworkType())) {
                    flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
                } else {
                    flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
                }

                if (flowTemplates == null) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                    continue;
                } else {
                    if (flowTemplates.getStatus() != 9) {
                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                        continue;
                    }
                }

                try {
                    String filePutOSS;
                    if ("3G".equals(currChangePlan.getNetworkType())) {
                        filePutOSS = "ChangeParamNcms-" + currChangePlan.getCrNumber().toUpperCase()
                                + "-" + currChangePlan.getVendor().getVendorName().toUpperCase()
                                + "-" + currChangePlan.getNetworkType().toUpperCase()
                                + "-RNC_" + rnc.toUpperCase()
                                + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    } else {
                        filePutOSS = "ChangeParamNcms-" + currChangePlan.getCrNumber().toUpperCase()
                                + "-" + currChangePlan.getVendor().getVendorName().toUpperCase()
                                + "-" + currChangePlan.getNetworkType().toUpperCase()
                                + "-BSC_" + rnc.toUpperCase()
                                + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    }
                    generateFlowRunController = new GenerateFlowRunController();
                    FlowRunAction flowRunAction = new FlowRunAction();
                    flowRunAction.setCrNumber(Config.CR_AUTO_DECLARE_CUSTOMER);
                    flowRunAction.setFlowRunName(filePutOSS);
//                    filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                    while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                        flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                    }
                    flowRunAction.setTimeRun(new Date());
                    flowRunAction.setFlowTemplates(flowTemplates);
                    flowRunAction.setExecuteBy(SessionUtil.getCurrentUsername());
                    flowRunAction.setRollbackMode(1L);
                    generateFlowRunController.setFlowRunAction(flowRunAction);
                    generateFlowRunController.setSelectedFlowTemplates(flowTemplates);

                    //Lay danh sach param tu bang 
                    generateFlowRunController.setNodes(new ArrayList<Node>());
                    generateFlowRunController.loadGroupAction(0l);
                    List<Node> nodeInPlan;
                    HashMap<String, Object> filters = new HashMap<>();
                    filters.put("nodeCode", rnc);
                    List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                    Node nodeRncBsc;
                    if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                        throw new Exception(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), rnc));
                    } else {
                        nodeRncBsc = nodeRncs.get(0);
                    }

                    Map<String, String> mapParamValues = mapRncParam.get(rnc);
                    for (String key : mapParamValues.keySet()) {
                        LOGGER.info("Pre map key: " + key);
                        LOGGER.info("Pre map value: " + mapParamValues.get(key));
                    }

                    if ("3G".equals(currChangePlan.getNetworkType())) {
                        mapParamValues.put("rnc", rnc);
                    }

                    nodeInPlan = new ArrayList<>();
                    generateFlowRunController.getNodes().add(nodeRncBsc);
                    nodeInPlan.add(nodeRncBsc);

                    for (Node node : nodeInPlan) {
                        generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                        List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                        for (ParamValue paramValue : paramValues) {
                            LOGGER.info("Show ParamCode: " + paramValue.getParamCode());

                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }

                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim()));
                                LOGGER.info("Param code: " + paramValue.getParamCode().toLowerCase().trim());
                                LOGGER.info("Param value: " + value);
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }

                            ResourceBundle bundle = ResourceBundle.getBundle("cas");
                            if (bundle.getString("service").contains("10.61.127.190")) {
                                if (value == null || value.toString().isEmpty()) {
                                    value = "TEST_NOT_FOUND";
                                }
                            }
                            if (value != null) {
                                LOGGER.info("Param value set: " + value);
                                paramValue.setParamValue(value.toString());
                            }
                        }
                    }
                    mapParamValues.clear();

                    for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                        if (groupAction.getGroupActionName().equals(MessageUtil.getResourceBundleMessage("label.changeParam.run.dt.nodeB")) && (!mapParamType.containsKey(Config.ParamTypeNodeB) || !mapParamType.get(Config.ParamTypeNodeB).contains(rnc))) {
                            groupAction.setDeclare(false);
                        }
                        if (groupAction.getGroupActionName().equals(MessageUtil.getResourceBundleMessage("label.changeParam.run.dt.rnc")) && (!mapParamType.containsKey(Config.ParamTypeRnc) || !mapParamType.get(Config.ParamTypeRnc).contains(rnc)) && (!mapParamType.containsKey(Config.ParamTypeCell) || !mapParamType.get(Config.ParamTypeCell).contains(rnc))) {
                            groupAction.setDeclare(false);
                        }
                    }

                    boolean saveDT = generateFlowRunController.saveDT();
                    if (saveDT) {
                        try {
                            // Save flow to database                           
                            ChangeFlowRunAction changeFlowRunAction = new ChangeFlowRunAction();
                            changeFlowRunAction.setFlowRunAction(flowRunAction);
                            changeFlowRunAction.setNode(nodeRncBsc);
                            changeFlowRunAction.setChangePlan(currChangePlan);
                            changeFlowRunAction.setType(2L);
                            changeFlowRunAction.setUpdateTime(new Date());
                            changeFlowRunActions.add(changeFlowRunAction);

                            MessageUtil.setInfoMessageFromRes(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), rnc));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                    }
                } catch (Exception e) {
                    if (e instanceof MessageException) {
                        MessageUtil.setErrorMessage(e.getMessage());
                    } else {
                        LOGGER.error(e.getMessage(), e);
                    }
                    throw new Exception(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                } finally {
                    //Tam biet
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
        changeFlowRunActionServiceImpl.saveOrUpdate(changeFlowRunActions);
        return true;
    }

    public void preAddChangePlan() {
        currChangePlan = new ChangePlan();
        mapStationResult.clear();
    }

    public void preShowChangeDT(ChangePlan changePlan) throws Exception {
        HashMap<String, Object> filters = new HashMap<>();
        filters.put("changePlan.id", changePlan.getId());
        Map<String, String> orders = new LinkedHashMap<>();

        orders.put("node.nodeCode", "ASC");
        orders.put("flowRunAction.flowRunName", "ASC");
        lazyChangeDT = new LazyDataModelBaseNew<>(new ChangeFlowRunActionServiceImpl(), filters, orders);
    }

    public StreamedContent onDownloadFilePutOSS(ChangeFlowRunAction DT) {
        try {
            if (DT.getFileName() != null && DT.getFilePath() != null) {
                File fileExport = new File(DT.getFilePath());
                if (fileExport.exists()) {
                    return new DefaultStreamedContent(new FileInputStream(fileExport), ".txt", fileExport.getName());
                } else {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                            MessageUtil.getResourceBundleMessage("label.dowload.fileName")));

                }
            } else {
                MessageUtil.setErrorMessage("Khong co file");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public void deleteChangePlan() {
        try {
            HashMap<String, Object> filters = new HashMap<>();
            List<ChangeParam> changeParams = new ArrayList<>();
            if (selectChangePlans != null && selectChangePlans.size() > 0) {

                for (ChangePlan plan : selectChangePlans) {
                    filters.clear();
                    filters.put("changePlan.id", plan.getId());
                    List<ChangeParam> existingChangeParams = changeParamService.findList(filters);
                    changeParams.addAll(existingChangeParams);
                }
                if (!changeParams.isEmpty()) {
                    changeParamService.delete(changeParams);
                }
                changePlanService.delete(selectChangePlans);
            }
            MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteChangePlan').hide()");
            if (selectChangePlans != null) {
                selectChangePlans.clear();
            }
        } catch (Exception ex) {
            MessageUtil.setInfoMessageFromRes("label.action.deleteFail");
            logger.error(ex.getMessage(), ex);
        }
    }

    public void preDeleteChangePlan() {
        if (selectChangePlans != null && selectChangePlans.size() > 0) {
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteChangePlan').show()");
        } else {
            MessageUtil.setInfoMessageFromRes("label.form.choose.one");
        }
    }

    public void exportResult(ChangePlan changePlan) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        XSSFWorkbook wb = null;
        try {
            // Initialize workbook
            wb = new XSSFWorkbook();

            // RNC Sheet
            XSSFSheet sheetRnc;
            if ("3G".equals(currChangePlan.getNetworkType())) {
                sheetRnc = wb.createSheet("Tham so RNC");
            } else {
                sheetRnc = wb.createSheet("Tham so BSC");
            }

            // NodeB Sheet
            XSSFSheet sheetNodeB = wb.createSheet("Tham so NodeB");
            // Cell Sheet
            XSSFSheet sheetCell = wb.createSheet("Tham so cell");

            XSSFCellStyle cellStyleAffected = wb.createCellStyle();
            cellStyleAffected.setFillForegroundColor(new XSSFColor(new java.awt.Color(62, 240, 159)));
            cellStyleAffected.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cellStyleAffected.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyleAffected.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyleAffected.setBorderRight(CellStyle.BORDER_THIN);
            cellStyleAffected.setBorderTop(CellStyle.BORDER_THIN);
            cellStyleAffected.setAlignment(CellStyle.ALIGN_CENTER);

            XSSFCellStyle cellStyleNormal = wb.createCellStyle();
            cellStyleNormal.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyleNormal.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyleNormal.setBorderRight(CellStyle.BORDER_THIN);
            cellStyleNormal.setBorderTop(CellStyle.BORDER_THIN);
            cellStyleNormal.setAlignment(CellStyle.ALIGN_CENTER);

            // Get header of RNC sheet of export file
            String sql = "select distinct PARAM_CODE from CHANGE_PARAM where PLAN_ID = ? and PARAM_TYPE = 0";
            List<String> rncParamCodeList = (List<String>) mapNodeServiceImpl.findListSQLAll(sql, changePlan.getId());
            XSSFRow row = sheetRnc.createRow(0);
            XSSFCell cell = row.createCell(0);
            if ("3G".equals(currChangePlan.getNetworkType())) {
                cell.setCellValue(MessageUtil.getResourceBundleMessage("datatable.header.rnc"));
            } else {
                cell.setCellValue(MessageUtil.getResourceBundleMessage("label.BSC"));
            }
            cell.setCellStyle(cellStyleNormal);
            Map<String, Integer> positionMap = new HashMap<>();
            for (int i = 0; i < rncParamCodeList.size(); i++) {
                int index = i + 1;
                cell = row.createCell(index);
                cell.setCellValue(rncParamCodeList.get(i));
                cell.setCellStyle(cellStyleNormal);
                positionMap.put(rncParamCodeList.get(i), index);
            }

            // Get all rnc param with planId
            HashMap<String, Object> filters = new HashMap<>();
            filters.put("changePlan.id", String.valueOf(changePlan.getId()));
            filters.put("paramType", String.valueOf(0L));
            HashMap<String, String> orders = new LinkedHashMap<>();
            orders.put("id", "ASC");
            List<ChangeParam> changeParams = changeParamService.findList(filters, orders);
            String currentRnc = "";
            int rowIndex = 1;
            for (int i = 0; i < changeParams.size(); i++) {
                ChangeParam changeParam = changeParams.get(i);
                if (!changeParam.getRnc().equals(currentRnc)) {
                    row = sheetRnc.createRow(rowIndex++);
                    cell = row.createCell(0);
                    cell.setCellValue(changeParam.getRnc());
                    cell.setCellStyle(cellStyleNormal);
                }

                cell = row.createCell(positionMap.get(changeParam.getParamCode()));
                cell.setCellValue(changeParam.getParamValueNew());

                if (null != changeParam.getParamValueNew() && !"".equals(changeParam.getParamValueNew()) && !changeParam.getParamValueNew().equals(changeParam.getParamValueOld())) {
                    cell.setCellStyle(cellStyleAffected);
                } else {
                    cell.setCellStyle(cellStyleNormal);
                }

                currentRnc = changeParam.getRnc();
            }

            // Get header of NodeB sheet of export file
            sql = "select distinct PARAM_CODE from CHANGE_PARAM where PLAN_ID = ? and PARAM_TYPE = 1";
            List<String> nodebParamCodeList = (List<String>) mapNodeServiceImpl.findListSQLAll(sql, changePlan.getId());
            row = sheetNodeB.createRow(0);
            cell = row.createCell(0);
            if ("3G".equals(currChangePlan.getNetworkType())) {
                cell.setCellValue(MessageUtil.getResourceBundleMessage("datatable.header.rnc"));
            } else {
                cell.setCellValue(MessageUtil.getResourceBundleMessage("label.BSC"));
            }
            cell.setCellStyle(cellStyleNormal);
            cell = row.createCell(1);
            cell.setCellValue("NodeB Name");
            cell.setCellStyle(cellStyleNormal);
            positionMap.clear();
            for (int i = 0; i < nodebParamCodeList.size(); i++) {
                int index = i + 2;
                cell = row.createCell(index);
                cell.setCellValue(nodebParamCodeList.get(i));
                cell.setCellStyle(cellStyleNormal);
                positionMap.put(nodebParamCodeList.get(i), index);
            }

            // Get all nodeb param with planId
            filters = new HashMap<>();
            filters.put("changePlan.id", String.valueOf(changePlan.getId()));
            filters.put("paramType", String.valueOf(1L));
            orders.clear();
            orders.put("id", "ASC");
            orders.put("nodeb", "ASC");
            changeParams = changeParamService.findList(filters, orders);
            String currentNodeB = "";
            rowIndex = 1;
            for (int i = 0; i < changeParams.size(); i++) {
                ChangeParam changeParam = changeParams.get(i);
                if (!changeParam.getNodeb().equals(currentNodeB)) {
                    row = sheetNodeB.createRow(rowIndex++);
                    cell = row.createCell(0);
                    cell.setCellValue(changeParam.getRnc());
                    cell.setCellStyle(cellStyleNormal);
                    cell = row.createCell(1);
                    cell.setCellValue(changeParam.getNodeb());
                    cell.setCellStyle(cellStyleNormal);
                }
                cell = row.createCell(positionMap.get(changeParam.getParamCode()));
                cell.setCellValue(changeParam.getParamValueNew());

                if (null != changeParam.getParamValueNew() && !"".equals(changeParam.getParamValueNew()) && !changeParam.getParamValueNew().equals(changeParam.getParamValueOld())) {
                    cell.setCellStyle(cellStyleAffected);
                } else {
                    cell.setCellStyle(cellStyleNormal);
                }

                currentNodeB = changeParam.getNodeb();
            }

            // Get header of Cell sheet of export file
            sql = "select distinct PARAM_CODE from CHANGE_PARAM where PLAN_ID = ? and PARAM_TYPE = 2";
            List<String> cellParamCodeList = (List<String>) mapNodeServiceImpl.findListSQLAll(sql, changePlan.getId());
            row = sheetCell.createRow(0);
            cell = row.createCell(0);
            if ("3G".equals(currChangePlan.getNetworkType())) {
                cell.setCellValue(MessageUtil.getResourceBundleMessage("datatable.header.rnc"));
            } else {
                cell.setCellValue(MessageUtil.getResourceBundleMessage("label.BSC"));
            }
            cell.setCellStyle(cellStyleNormal);
            cell = row.createCell(1);
            cell.setCellValue("Cell");
            cell.setCellStyle(cellStyleNormal);
            positionMap.clear();
            for (int i = 0; i < cellParamCodeList.size(); i++) {
                int index = i + 2;
                cell = row.createCell(index);
                cell.setCellValue(cellParamCodeList.get(i));
                cell.setCellStyle(cellStyleNormal);
                positionMap.put(cellParamCodeList.get(i), index);
            }

            // Get all cell param with planId
            filters = new HashMap<>();
            filters.put("changePlan.id", String.valueOf(changePlan.getId()));
            filters.put("paramType", String.valueOf(2L));
            orders.clear();
            orders.put("id", "ASC");
            orders.put("cell", "ASC");
            changeParams = changeParamService.findList(filters, orders);
            String currentCell = "";
            rowIndex = 1;
            for (int i = 0; i < changeParams.size(); i++) {
                ChangeParam changeParam = changeParams.get(i);
                if (!changeParam.getCell().equals(currentCell)) {
                    row = sheetCell.createRow(rowIndex++);
                    cell = row.createCell(0);
                    cell.setCellValue(changeParam.getRnc());
                    cell.setCellStyle(cellStyleNormal);
                    cell = row.createCell(1);
                    cell.setCellValue(changeParam.getCell());
                    cell.setCellStyle(cellStyleNormal);
                }
                cell = row.createCell(positionMap.get(changeParam.getParamCode()));
                cell.setCellValue(changeParam.getParamValueNew());

                if (null != changeParam.getParamValueNew() && !"".equals(changeParam.getParamValueNew()) && !changeParam.getParamValueNew().equals(changeParam.getParamValueOld())) {
                    cell.setCellStyle(cellStyleAffected);
                } else {
                    cell.setCellStyle(cellStyleNormal);
                }

                currentCell = changeParam.getCell();
            }

            HttpServletResponse servletResponse = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            servletResponse.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            servletResponse.setHeader("Expires", "0");
            servletResponse.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            servletResponse.setHeader("Pragma", "public");
            servletResponse.setHeader("Content-disposition", "attachment;filename=" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + "-" + changePlan.getCrNumber() + "-LHSK-Result.xlsx");
            wb.write(servletResponse.getOutputStream());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            if (wb != null) {
                try {

                    wb.close();

                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }
        facesContext.responseComplete();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Common">
    public static String getFolderSave() {
        String pathOut;
        ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                .getExternalContext().getContext();
        pathOut = ctx.getRealPath("/") + Config.PATH_OUT;
        File folderOut = new File(pathOut);
        if (!folderOut.exists()) {
            folderOut.mkdirs();
        }
        return pathOut;
    }

    private static String convertObjectToString(Object object) {
        if (object != null) {
            return object.toString();
        } else {
            return "";
        }
    }

    private static Long convertObjectToLong(Object object) {
        try {
            if (object != null) {
                return Long.valueOf(object.toString());
            } else {
                return -1L;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return -1L;
        }
    }

    public File createFilePutOSS(String filename, StringBuilder stringBuilder) throws Exception {
        BufferedWriter bw = null;
        FileWriter fw = null;
        File file;
        try {
            file = new File(getFolderSave() + "IntegrateStation" + File.separator + "PutFileOSS" + File.separator + filename);
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(stringBuilder.toString());
            bw.close();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return file;
    }

    public boolean validateInput(ChangePlan change) {
        if (change != null
                && change.getVendor() != null && change.getNetworkType() != null) {
            return true;
        } else {
            return false;
        }
    }

    public String getSelectProficientTypeStr() {
        if (selectProficientType == null) {
            return "";
        } else if (selectProficientType.equals(0L)) {
            return MessageUtil.getResourceBundleMessage("label.changeParam.proficient.type0");
        } else if (selectProficientType.equals(1L)) {
            return MessageUtil.getResourceBundleMessage("label.changeParam.proficient.type1");
        } else {
            return "";
        }
    }

    public String getIpNodeB(String rnc, String nodeB) {
        String nodebIp = "";
        String sql = "select distinct NODEB_IP from Map_Node where RNC_BSC = ? and NODEB = ?";
        List<String> nodeBIps = (List<String>) mapNodeServiceImpl.findListSQLAll(sql, rnc, nodeB);
        if (null != nodeBIps && nodeBIps.size() > 0) {
            nodebIp = nodeBIps.get(0);
            String[] nodebIpArr = nodebIp.split("\\.");
            String octet = String.valueOf(Integer.valueOf(nodebIpArr[1]) + 2);
            nodebIp = nodebIpArr[0] + "." + octet + "." + nodebIpArr[2] + "." + nodebIpArr[3];
        }
        return nodebIp;
    }
    //</editor-fold>
}
