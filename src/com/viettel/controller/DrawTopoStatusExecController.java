package com.viettel.controller;

import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.viettel.util.*;
import org.apache.commons.codec.binary.Base64;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.Connector;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.lazy.LazyDataModelSearchNode;

import com.viettel.model.Action;
import com.viettel.model.ActionDetail;
import com.viettel.model.ActionOfFlow;
import com.viettel.model.CatCountryBO;
import com.viettel.model.FlowRunAction;
import com.viettel.model.FlowRunLog;
import com.viettel.model.FlowRunLogAction;
import com.viettel.model.FlowRunLogCommand;
import com.viettel.model.FlowTemplates;
import com.viettel.model.MapProcessCountry;
import com.viettel.model.Node;
import com.viettel.model.NodeRun;
import com.viettel.model.NodeRunGroupAction;
import com.viettel.object.AccountObj;
import com.viettel.object.GroupAction;
import com.viettel.object.LogNodeObj;
import com.viettel.object.LogNodeObj.LogNodeStatus;
import com.viettel.object.MessageException;
import com.viettel.object.MessageObject;
import com.viettel.passprotector.PassProtector;

import com.viettel.persistence.ActionOfFlowServiceImpl;
import com.viettel.persistence.FlowRunActionServiceImpl;
import com.viettel.persistence.FlowRunLogActionServiceImpl;
import com.viettel.persistence.FlowRunLogCommandServiceImpl;
import com.viettel.persistence.FlowRunLogServiceImpl;
import com.viettel.persistence.FlowTemplatesServiceImpl;
import com.viettel.persistence.GenericDaoServiceNewV2;
import com.viettel.persistence.MapProcessCountryServiceImpl;
import com.viettel.persistence.NodeRunServiceImpl;
import com.viettel.persistence.NodeServiceImpl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.primefaces.event.TabChangeEvent;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;

@ViewScoped
@ManagedBean
public class DrawTopoStatusExecController {

    protected static final Logger logger = LoggerFactory.getLogger(BuildTemplateFlowController.class);
    protected static final Logger loggerAction = LoggerFactory.getLogger("log.Action");

    @ManagedProperty(value = "#{flowRunLogCommandService}")
    private FlowRunLogCommandServiceImpl flowRunLogCommandService;
    @ManagedProperty(value = "#{flowRunLogService}")
    private FlowRunLogServiceImpl flowRunLogService;
    @ManagedProperty("#{flowRunActionService}")
    private GenericDaoServiceNewV2<FlowRunAction, Long> flowRunActionService;

    public void setFlowRunLogCommandService(
            FlowRunLogCommandServiceImpl flowRunLogCommandService) {
        this.flowRunLogCommandService = flowRunLogCommandService;
    }

    public static final Long ERROR_CMD = 0l;
    public static final Long SUCCESS_CMD = 1l;

    public static final int ACTION_DETAULT = 0;
    public static final int ACTION_RUNNING_EXEC = 1;
    public static final int ACTION_FAIL_EXEC = 2;
    public static final int ACTION_SUCCESS_EXEC = 3;
    public static final int ACTION_FINISH_EXEC = 4;
    public static final int ACTION_MANUAL = 5;

    public static final Long ACTION_RUN_AUTO = 0L;
    public static final Long ACTION_RUN_MANUAL = 1L;

    public static final Long ELEMENT_START = -1L;
    public static final Long ELEMENT_END = -2L;

    public static final Long FLOW_RUN_ACTION_NOT_RUN = 0L;

    private DefaultDiagramModel topoDiagram;

    private ActionOfFlow selectedActionFlow;
    private FlowRunLogCommand selectedLogCommand;

    private boolean isRunAuto = true;
    private Map<Long, Map<Long, ActionDetail>> mapActionRunInfo = new HashMap<Long, Map<Long, ActionDetail>>();
    private Map<String, LogNodeObj> mapLogCommand = new HashMap<>();
    private Map<String, List<FlowRunLogCommand>> mapLogCommandManual = new HashMap<String, List<FlowRunLogCommand>>();
    private LazyDataModel<FlowRunLog> lstFlowRunLog;

    private FlowRunAction selectedFlowRunAction; // dau viec chay auto
    private FlowRunLog selectedFlowRunLog; // dau viec chay manual

    private String account;
    private String password;
    private Long actionOfFlowId;
    private int activeIndex;
    private int modeRun;
    private int runningType;
    private int runType;
    private boolean oneAccount = false;
    private Map<String, AccountObj> mapAccount;
    private List<String> subFlowRuns;
    private String selectSubflowRun;
    private Map<Long, Boolean> mapActionOfFlowDeclare;

    private boolean isShowDiagramOnly;
    private boolean isActionManual = false;
    //Quytv7 ghi log action
    private String logAction = "";
    private String className = DrawTopoStatusExecController.class.getName();

    @PostConstruct
    public void onStart() {
        try {

            subFlowRuns = new ArrayList<>();
            subFlowRuns.add(Config.SUB_FLOW_RUN_DEFAULT);

            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            if (context.getRequestMap().get("javax.servlet.forward.request_uri").toString().endsWith("/action")) {
                isShowDiagramOnly = true;
            }
            logAction = "Login Function";
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void buildTopoDiagram(FlowTemplates flowTemplate, boolean showDiagramOnly) {
        FlowRunAction flowRunActionFake = new FlowRunAction();
        flowRunActionFake.setFlowTemplates(flowTemplate);
        buildTopoDiagram(flowRunActionFake);
    }

    public void getMapGroupActionDeclare(FlowRunAction flowRunAction) {
        mapActionOfFlowDeclare = new HashMap<>();
        GenerateFlowRunController generateFlowRunController = new GenerateFlowRunController();
        generateFlowRunController.setNodes(new ArrayList<Node>());
        generateFlowRunController.setLazyNode(new LazyDataModelSearchNode<>(new NodeServiceImpl()));
        generateFlowRunController.loadGroupActionAll(flowRunAction);
        for (String node : generateFlowRunController.getMapGroupAction().keySet()) {
            for (GroupAction groupAction : generateFlowRunController.getMapGroupAction().get(node)) {
                for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                    if (mapActionOfFlowDeclare.containsKey(actionOfFlow.getStepNum()) && mapActionOfFlowDeclare.get(actionOfFlow.getStepNum())) {
                        mapActionOfFlowDeclare.put(actionOfFlow.getStepNum(), true);
                    } else {
                        mapActionOfFlowDeclare.put(actionOfFlow.getStepNum(), groupAction.isDeclare());
                    }
                }
            }
        }
    }

    public void buildTopoDiagram(FlowRunAction flowRunAction) {
        topoDiagram = new DefaultDiagramModel();
        topoDiagram.setMaxConnections(-1);

        try {
            if (flowRunAction.getFlowRunId() == null) {
                selectedFlowRunAction = flowRunAction;
            } else {
                selectedFlowRunAction = new FlowRunActionServiceImpl().findById(flowRunAction.getFlowRunId());
            }
            //selectedFlowRunLog = new FlowRunLogServiceImpl().findById(1L);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        if (selectedFlowRunAction != null) {
            try {
                //Set MapSubflowrun
//                GenerateFlowRunController generateFlowRunController = new GenerateFlowRunController();
//                generateFlowRunController.getMapSubFlowRunNodes().clear();
//                generateFlowRunController.setSubFlowRuns(new ArrayList<String>());
//                for (NodeRun nodeRun : flowRunAction.getNodeRuns()) {
////                if(nodeRun.getSubFlowRun() == null || "".enodeRun.getSubFlowRun())
//                    if (generateFlowRunController.getMapSubFlowRunNodes().containsKey(nodeRun.getId().getSubFlowRun())) {
//                        generateFlowRunController.getMapSubFlowRunNodes().get(nodeRun.getId().getSubFlowRun()).add(nodeRun.getNode());
//                    } else {
//                        generateFlowRunController.getSubFlowRuns().add(nodeRun.getId().getSubFlowRun());
//                        List<Node> lstNode = new ArrayList<>();
//                        lstNode.add(nodeRun.getNode());
//                        generateFlowRunController.getMapSubFlowRunNodes().put(nodeRun.getId().getSubFlowRun(), lstNode);
//                    }
//                }
                subFlowRuns = new ArrayList<>();
                for (NodeRun nodeRun : flowRunAction.getNodeRuns()) {
//                if(nodeRun.getSubFlowRun() == null || "".enodeRun.getSubFlowRun())
//                    if (generateFlowRunController.getMapSubFlowRunNodes().containsKey(nodeRun.getId().getSubFlowRun())) {
//                        generateFlowRunController.getMapSubFlowRunNodes().get(nodeRun.getId().getSubFlowRun()).add(nodeRun.getNode());
//                    } else {
//                        generateFlowRunController.getSubFlowRuns().add(nodeRun.getId().getSubFlowRun());
//                        List<Node> lstNode = new ArrayList<>();
//                        lstNode.add(nodeRun.getNode());
//                        generateFlowRunController.getMapSubFlowRunNodes().put(nodeRun.getId().getSubFlowRun(), lstNode);
//                    }

                    if (!subFlowRuns.contains(nodeRun.getId().getSubFlowRun())) {
                        subFlowRuns.add(nodeRun.getId().getSubFlowRun());
                    }
                }
                if (subFlowRuns.isEmpty()) {
                    subFlowRuns.add(Config.SUB_FLOW_RUN_DEFAULT);
                }
                selectSubflowRun = subFlowRuns.get(0);
                List<ActionOfFlow> lstActionFlow = selectedFlowRunAction.getFlowTemplates().getActionOfFlows();
                LinkedHashMap<String, Long> mapActionLastIndexGroup = new LinkedHashMap<>();
                if (lstActionFlow != null) {

                    // lay thong tin cac node thuc hien theo dau viec
                    mapActionLastIndexGroup = buildMapActionRunData(lstActionFlow);
                    int yIndexStartExec = 2;

                    /*
                     * Them phan tu dau tien vao topo
                     */
                    ActionOfFlow start = new ActionOfFlow();
                    start.setAction(new Action());
                    start.getAction().setName(MessageUtil.getResourceBundleMessage("label.diagram.element.start"));
                    start.setStepNum(ELEMENT_START);
                    Element elementStart = new Element(start, "40em", yIndexStartExec + "em");
                    elementStart.setStyleClass("ui-diagram-start");
                    elementStart.setDraggable(false);
                    elementStart.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
                    elementStart.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));
                    elementStart.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
                    elementStart.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT));
                    topoDiagram.addElement(elementStart);
                    yIndexStartExec += 7;

                    Map<String, Element> mapElement = new HashMap<>();
                    Map<String, ActionOfFlow> mapAction = new HashMap<>();
                    Map<Long, Integer> mapCountRollback = new HashMap<>();

                    //20171026_hienhv4_bo sung group order_start
                    List<Long> groupActionOrders = new ArrayList<>();
                    long idex = 0L;
                    for (ActionOfFlow actionOfFlow : lstActionFlow) {
                        if (!groupActionOrders.contains(actionOfFlow.getGroupActionOrder())) {
                            groupActionOrders.add(actionOfFlow.getGroupActionOrder());
                            idex++;
                        }
                        actionOfFlow.setGroupActionOrder2(idex);
                    }
                    //20171026_hienhv4_bo sung group order_end

                    for (ActionOfFlow action : lstActionFlow) {
                        Element element;

                        if (mapElement.containsKey(action.getGroupActionOrder() + "#" + action.getStepNumberLabel())) {
                            ActionOfFlow prevAction = mapAction.get(action.getGroupActionOrder() + "#" + action.getStepNumberLabel());
                            prevAction.getPreStepsNumber().add(action.getPreviousStep() + "");
                            prevAction.getIfValues().add(action.getIfValue() + "");
                            mapAction.put(action.getGroupActionOrder() + "#" + action.getStepNumberLabel(), prevAction);

                            continue;
                        }

                        /*
                         * Hien thi cac action khai bao
                         */
                        if (action.getIsRollback() == 0) {
                            element = new Element(action, "40em", yIndexStartExec + "em");
                            yIndexStartExec += 7;

                            /*
                             * Hien thi cac phan tu rollback sang phia trai cua cac action khai bao
                             */
                        } else {
                            Element previous = mapElement.get(action.getGroupActionOrder() + "#" + action.getPreviousStep());

                            if (previous != null) {

                                if (mapCountRollback.containsKey(((ActionOfFlow) previous.getData()).getStepNum())) {
                                    mapCountRollback.put(((ActionOfFlow) previous.getData()).getStepNum(), mapCountRollback.get(((ActionOfFlow) previous.getData()).getStepNum()) + 1);
                                } else {
                                    mapCountRollback.put(((ActionOfFlow) previous.getData()).getStepNum(), 1);
                                }
                                element = new Element(action, Integer.parseInt(previous.getX().replace("em", "")) - 18 * mapCountRollback.get(((ActionOfFlow) previous.getData()).getStepNum()) + "em",
                                        Integer.parseInt(previous.getY().replace("em", "")) + "em");

                            } else {
                                yIndexStartExec -= 10;
                                element = new Element(action, "0em", yIndexStartExec + "em");
                                yIndexStartExec += 10;
                            }
                        }

                        element.setStyleClass((action.getIsRollback().equals(0l)) ? "ui-diagram-default" : "ui-diagram-rollback-default");
                        if (action.getActionType() != null && action.getActionType().equals(Config.ACTION_TYPE_MANUAL)) {
                            element.setStyleClass("ui-diagram-action-manual");
                        }
                        element.setDraggable(false);
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT));
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_RIGHT));
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT));
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_LEFT));
                        element.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_LEFT));

                        element.setId(action.getStepNum() + "");
                        topoDiagram.addElement(element);

                        List<String> lstPreStep = new ArrayList<>();
                        lstPreStep.add(action.getPreviousStep() + "");
                        action.setPreStepsNumber(lstPreStep);
                        action.setIfValues(new ArrayList<String>());
                        action.getIfValues().add(action.getIfValue());

                        mapElement.put(action.getGroupActionOrder() + "#" + action.getStepNumberLabel(), element);
                        mapAction.put(action.getGroupActionOrder() + "#" + action.getStepNumberLabel(), action);

                    } // end loop for

                    /*
                     * Them phan tu cuoi cung vao topo
                     */
                    ActionOfFlow end = new ActionOfFlow();
                    end.setAction(new Action());
                    end.getAction().setName(MessageUtil.getResourceBundleMessage("label.diagram.element.end"));
                    end.setStepNum(ELEMENT_END);
                    Element elementEnd = new Element(end, "40em", yIndexStartExec + "em");
                    elementEnd.setStyleClass("ui-diagram-end");
                    elementEnd.setDraggable(false);
                    elementEnd.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
                    elementEnd.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));
                    elementEnd.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
                    elementEnd.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT));
                    topoDiagram.addElement(elementEnd);
                }

                // gan cac action link voi nhau
                Connector stateConnectorNotOk = new StateMachineConnector();
                stateConnectorNotOk.setPaintStyle("{strokeStyle:'#e0335e',lineWidth:1}");

                Connector stateConnectorOk = new StateMachineConnector();
                stateConnectorOk.setPaintStyle("{strokeStyle:'#4350e0',lineWidth:1}");

                Connector straightConnector = new StraightConnector();
                straightConnector.setPaintStyle("{strokeStyle:'#4350e0',lineWidth:1}");

                Map<String, Integer> mapElementCheck = new HashMap<>();

                // Danh sach lstActionFlow da duoc xap xep theo thu tu groupOrder va stepLaberOrder
                LinkedHashMap<Long, Long> mapGroupOrderIndex = buildMapGroupOrder(lstActionFlow);
                for (int i = 1; i < topoDiagram.getElements().size() - 1; i++) {
                    topoDiagram.getElements().get(i).setDraggable(false);
                    ActionOfFlow actionBefore = (ActionOfFlow) topoDiagram.getElements().get(i).getData();

                    for (int j = i + 1; j < topoDiagram.getElements().size() - 1; j++) {

                        ActionOfFlow actionAfter = (ActionOfFlow) topoDiagram.getElements().get(j).getData();
                        if (actionAfter.getGroupActionOrder().intValue() > actionBefore.getGroupActionOrder().intValue()) {

                            if (actionAfter.getIsRollback().equals(Config.EXECUTE_ACTION)
                                    && mapActionLastIndexGroup.get(actionAfter.getGroupActionOrder() + "_MIN") != null
                                    && mapActionLastIndexGroup.get(actionAfter.getGroupActionOrder() + "_MIN") == actionAfter.getStepNumberLabel()
                                    && mapElementCheck.get(actionAfter.getGroupActionOrder() + "#" + actionAfter.getPreStepsNumberLabel()) == null) {

                                for (int j2 = j - 1; j2 >= 0; j2--) {
                                    ActionOfFlow actionTmp = null;
                                    try {
                                        actionTmp = (ActionOfFlow) topoDiagram.getElements().get(j2).getData();
                                    } catch (Exception e) {
                                        logger.error(e.getMessage(), e);
                                        actionTmp = null;
                                    }

                                    if (actionTmp == null
                                            || actionTmp.getGroupActionOrder() == null) {
                                        continue;
                                    }

                                    if (actionTmp.getGroupActionOrder().equals(actionBefore.getGroupActionOrder())
                                            && actionTmp.getIsRollback().equals(Config.EXECUTE_ACTION)
                                            && mapGroupOrderIndex.get(actionAfter.getGroupActionOrder()).equals(actionTmp.getGroupActionOrder())) {

                                        // Kiem tra xem action co phai la action cuoi cua dau viec truoc hay khong
                                        if (checkActionEndOfGroup(lstActionFlow, actionTmp.getGroupActionOrder(), actionTmp.getStepNumberLabel())) {

                                            mapElementCheck.put(actionAfter.getGroupActionOrder() + "#" + actionAfter.getPreStepsNumberLabel(), 1);

                                            if (mapActionLastIndexGroup.get(actionTmp.getGroupActionOrder() + "_MAX") != null
                                                    && mapActionLastIndexGroup.get(actionTmp.getGroupActionOrder() + "_MAX") == actionTmp.getStepNumberLabel()) {

                                                topoDiagram.connect(createConnection(topoDiagram.getElements().get(j2).getEndPoints().get(1),
                                                        topoDiagram.getElements().get(j).getEndPoints().get(0), "OK", straightConnector));
                                            } else {
                                                topoDiagram.connect(createConnection(topoDiagram.getElements().get(j2).getEndPoints().get(5),
                                                        topoDiagram.getElements().get(j).getEndPoints().get(4), "OK", stateConnectorOk));
                                            }
                                        }
                                    }

                                } // end loop for j2

                                break;
                            }

                        } else {
                            List<String> preStepsNumber = actionAfter.getPreStepsNumber();
                            for (int k = 0; k < preStepsNumber.size(); k++) {
                                String preStep = preStepsNumber.get(k);
                                if (!preStep.equals(actionBefore.getStepNumberLabel().toString())) {
                                    continue;
                                }
                                /*
                                 * Gan link cho action thuc thi
                                 */
                                if (actionAfter.getIsRollback() == 0) {
                                    String ifValue = actionAfter.getIfValues().get(k);
                                    if ("0".equals(ifValue)) {
                                        topoDiagram.connect(createConnection(topoDiagram.getElements().get(i).getEndPoints().get(5),
                                                topoDiagram.getElements().get(j).getEndPoints().get(4),
                                                "1".equals(ifValue) ? "OK" : "NOK", "1".equals(ifValue) ? straightConnector : stateConnectorNotOk));
                                    } else {
                                        if (!checkTwoPointAdjacent(topoDiagram.getElements().get(i), topoDiagram.getElements().get(j), 10)) {
                                            topoDiagram.connect(createConnection(topoDiagram.getElements().get(i).getEndPoints().get(5),
                                                    topoDiagram.getElements().get(j).getEndPoints().get(4),
                                                    "1".equals(ifValue) ? "OK" : "NOK", "1".equals(ifValue) ? stateConnectorOk : stateConnectorNotOk));
                                        } else {
                                            topoDiagram.connect(createConnection(topoDiagram.getElements().get(i).getEndPoints().get(1),
                                                    topoDiagram.getElements().get(j).getEndPoints().get(0),
                                                    "1".equals(ifValue) ? "OK" : "NOK", straightConnector));
                                        }

                                    }

                                    /*
                                     * Gan link cho action rollback
                                     */
                                } else {
                                    topoDiagram.connect(createConnection(topoDiagram.getElements().get(i).getEndPoints().get(2),
                                            topoDiagram.getElements().get(j).getEndPoints().get(3), "NOK", straightConnector));

                                }
                            }
                        }
                    }
                } // ket thuc vong for cac phan tu topo

                /*
                 * gan ket noi cho phan tu dau tien va phan tu cuoi cung
                 */
                if (topoDiagram.getElements() != null
                        && topoDiagram.getElements().size() > 2) {
                    topoDiagram.connect(createConnection(topoDiagram.getElements().get(0).getEndPoints().get(1),
                            topoDiagram.getElements().get(1).getEndPoints().get(0), "OK", straightConnector));

                    // lay ra index max của group action
                    Long maxGroupIndex = lstActionFlow.get(lstActionFlow.size() - 1).getGroupActionOrder();
                    // lay ra action flow index max
                    Long maxIdxActionOfMaxGroup = mapActionLastIndexGroup.get(maxGroupIndex + "_MAX");
                    if (maxIdxActionOfMaxGroup != null) {

                        for (int i = topoDiagram.getElements().size() - 2; i >= 1; i--) {
                            try {
                                if (((ActionOfFlow) topoDiagram.getElements().get(i).getData()).getStepNumberLabel().equals(maxIdxActionOfMaxGroup)) {
                                    topoDiagram.connect(createConnection(topoDiagram.getElements().get(i).getEndPoints().get(1),
                                            topoDiagram.getElements().get(topoDiagram.getElements().size() - 1).getEndPoints().get(0), "OK", straightConnector));
                                    break;
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    }
                }

                if (selectedFlowRunAction.getStatus() > Config.RUNNING_FLAG) {
                    if (selectedFlowRunAction.getStatus() == Config.LOGIN_FAIL_FLAG.intValue()) {
                    } else {
                        updateStatusAction();
                    }
                }

                if (selectedFlowRunAction.getStatus() == Config.RUNNING_FLAG.intValue() || (selectedFlowRunAction.getStatus() == Config.LOGIN_FAIL_FLAG.intValue() && isActionManual)) {
                    RequestContext.getCurrentInstance().execute("PF('scheduleUpdateTopo').start();");
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    public void onChangeSubFlowRuns(TabChangeEvent changeEvent) {
        try {
            String id = changeEvent.getTab().getClientId();
            id = id.split("-", -1)[1];
            selectSubflowRun = id;
            updateStatusAction();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private boolean checkTwoPointAdjacent(Element beforeElement, Element afterElement, int valCheck) {
        boolean check = true;
        try {
            // lay toa do truc x cua action before
            int yValBefore = Integer.valueOf(beforeElement.getY().replace("em", ""));
            // lay toa do truc y cua action after
            int yValAfter = Integer.valueOf(afterElement.getY().replace("em", ""));

            if (yValAfter - yValBefore > valCheck) {
                check = false;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return check;
    }

    private LinkedHashMap<Long, Long> buildMapGroupOrder(List<ActionOfFlow> lstActionOfFlow) {
        LinkedHashMap<Long, Long> mapGroupOrder = new LinkedHashMap<>();
        if (lstActionOfFlow != null) {
            Map<Long, Integer> mapActionFlow = new HashMap<>();
            List<Long> lstGroupOrder = new ArrayList<>();

            int size = lstActionOfFlow.size();
            for (int i = 0; i < size; i++) {
                if (mapActionFlow.get(lstActionOfFlow.get(i).getGroupActionOrder()) == null) {
                    lstGroupOrder.add(lstActionOfFlow.get(i).getGroupActionOrder());
                    mapActionFlow.put(lstActionOfFlow.get(i).getGroupActionOrder(), 1);
                }
            } // end loop for

            int sizeOrder = lstGroupOrder.size();
            for (int i = 0; i < sizeOrder; i++) {
                mapGroupOrder.put(lstGroupOrder.get(i), (i == 0 ? -1 : lstGroupOrder.get(i - 1)));
            }
        }
        return mapGroupOrder;
    }

    private boolean checkActionEndOfGroup(List<ActionOfFlow> lstActionOfFlow, Long groupOrder, Long preStep) {
        boolean check = true;
        if (lstActionOfFlow != null && !lstActionOfFlow.isEmpty()) {
            int count = 0;
            for (ActionOfFlow action : lstActionOfFlow) {
                if (action.getIsRollback().equals(Config.EXECUTE_ACTION)
                        && action.getGroupActionOrder().equals(groupOrder)
                        && action.getPreviousStep().equals(preStep)) {
                    count++;
                    if (count > 0) {
                        return false;
                    }
                }
            }
        }
        return check;
    }

    private LinkedHashMap<String, Long> buildMapActionRunData(List<ActionOfFlow> actions) {
        LinkedHashMap<String, Long> mapActionLastIndexGroup = new LinkedHashMap<>();
        try {

            mapActionRunInfo = new HashMap<Long, Map<Long, ActionDetail>>();

            for (ActionOfFlow action : actions) {

                if (action.getIsRollback() == 0) {
                    if (mapActionLastIndexGroup.get(action.getGroupActionOrder() + "_MAX") == null) {
                        mapActionLastIndexGroup.put(action.getGroupActionOrder() + "_MAX", action.getStepNumberLabel());
                    } else if (mapActionLastIndexGroup.get(action.getGroupActionOrder() + "_MAX") < action.getStepNumberLabel()) {
                        mapActionLastIndexGroup.put(action.getGroupActionOrder() + "_MAX", action.getStepNumberLabel());
                    }

                    if (mapActionLastIndexGroup.get(action.getGroupActionOrder() + "_MIN") == null) {
                        mapActionLastIndexGroup.put(action.getGroupActionOrder() + "_MIN", action.getStepNumberLabel());
                    } else if (mapActionLastIndexGroup.get(action.getGroupActionOrder() + "_MIN") > action.getStepNumberLabel()) {
                        mapActionLastIndexGroup.put(action.getGroupActionOrder() + "_MIN", action.getStepNumberLabel());
                    }
                }

                Map<Long, ActionDetail> mapNodeActionDetail = new HashMap<Long, ActionDetail>();
                List<ActionDetail> lstActionDetail = action.getAction().getActionDetails();
                List<NodeRunGroupAction> lstNodeRunAction = action.getNodeRunGroupActions();
                if (lstNodeRunAction != null && !lstNodeRunAction.isEmpty()) {

                    for (NodeRunGroupAction nodeRunAction : lstNodeRunAction) {
                        for (ActionDetail detail : lstActionDetail) {
                            if (nodeRunAction.getNodeRun().getNode().getVendor().getVendorId().equals(detail.getVendor().getVendorId())
                                    && nodeRunAction.getNodeRun().getNode().getNodeType().getTypeId().equals(detail.getNodeType().getTypeId())
                                    && nodeRunAction.getNodeRun().getNode().getVersion().getVersionId().equals(detail.getVersion().getVersionId())) {
                                mapNodeActionDetail.put(nodeRunAction.getNodeRun().getNode().getNodeId(), detail);
                                break;
                            }
                        }
                    }
                }

                mapActionRunInfo.put(action.getStepNum(), mapNodeActionDetail);

            } // end loop for

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return mapActionLastIndexGroup;
    }

    //    Map<EndPoint,Element> elemEnpoint = new HashMap<>();
//    
//    private Connection createConnection(Element from, Element to, String label) {
//    	int indexFrom = 1;
//    	int indexTo = 0;
//		while(elemEnpoint.get(from.getEndPoints().get(indexFrom))!=null && indexFrom < from.getEndPoints().size() ){
//			indexFrom ++;
//		}
//		while(elemEnpoint.get(to.getEndPoints().get(indexTo))!=null && indexTo < to.getEndPoints().size() ){
//			indexTo ++;
//		}
//		return createConnection(from.getEndPoints().get(indexFrom), to.getEndPoints().get(indexTo), label);
//    }
    private Connection createConnection(EndPoint from, EndPoint to, String label, Connector connector) {
        Connection conn = new Connection(from, to, connector);

        conn.getOverlays().add(new ArrowOverlay(10, 10, 1, 1));

        if (label != null) {
            if ("ok".equalsIgnoreCase(label)) {
                conn.getOverlays().add(new LabelOverlay(label, "flow-label-ok", 0.5));
            } else {
                conn.getOverlays().add(new LabelOverlay(label, "flow-label-nok", 0.5));
            }
        }

        return conn;
    }

    /*
     * Cap nhat trang thai cua tung action trong topo
     */
    public void updateStatusAction() {
        String nodeRunning = null;
        String nodeFinish = null;
        String nodeFail = null;

        if (topoDiagram != null) {

//			if (isRunAuto) {
            List<ActionOfFlow> lstAction = selectedFlowRunAction.getFlowTemplates().getActionOfFlows();
            for (int i = 1; i < topoDiagram.getElements().size() - 1; i++) {
                try {
                    ActionOfFlow actionFlow = (ActionOfFlow) topoDiagram.getElements().get(i).getData();
                    int actionStatus = getActionRunStatus(actionFlow);
                    switch (actionStatus) {
                        case ACTION_MANUAL:
                            selectedActionFlow = actionFlow;
                            actionOfFlowId = actionFlow.getStepNum();
                            isActionManual = true;
                            showLog(actionFlow.getStepNum());
                            nodeRunning = topoDiagram.getElements().get(i).getId();
                            logger.info("Action Manual mapLogCommand size = " + mapLogCommand.size());
                            if (mapLogCommand.size() > 0) {
                                logger.info("show dlgShowLogAction");
                                RequestContext.getCurrentInstance().execute("PF('dlgShowLogAction').show()");
                            }
                            RequestContext.getCurrentInstance().execute("PF('scheduleUpdateTopo').stop();");
                            break;
                        case ACTION_RUNNING_EXEC:
                            nodeRunning = topoDiagram.getElements().get(i).getId();
                            break;
                        case ACTION_SUCCESS_EXEC:
                            nodeFinish = topoDiagram.getElements().get(i).getId();
                            break;
                        case ACTION_FAIL_EXEC:
                            nodeFail = topoDiagram.getElements().get(i).getId();
                            break;
                        default:
                            for (ActionOfFlow action : lstAction) {
                                if (action.getGroupActionOrder().equals(actionFlow.getGroupActionOrder())
                                        && action.getStepNumberLabel().equals(actionFlow.getStepNumberLabel())
                                        && !action.getStepNum().equals(actionFlow.getStepNum())) {
                                    int actionStatusLocal = getActionRunStatus(action);
                                    if (actionStatus != actionStatusLocal) {
                                        actionStatus = actionStatusLocal;
                                        break;
                                    }
                                }
                            }
                            break;
                    }
                    if (actionStatus == ACTION_MANUAL) {
                        break;
                    }
                    if (actionFlow.getActionType().equals(0L)) {
                        topoDiagram.getElements().get(i).setStyleClass(getElementStyle(actionStatus, actionFlow.getIsRollback() > 0 ? "0" : "1"));
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            } // ket thuc vong lap

            /*
             * Cap nhat action dang thuc hien chay tac dong
             */
//			} else if (selectedFlowRunLog != null){
//				try {
//					ActionOfFlow actionFlow = new ActionOfFlowServiceImpl().findById(selectedFlowRunLog.getActionOfFlowId());
//					if (actionFlow != null) {
//						ActionOfFlow actionFlowCheck = null;
//						for (int i = 1; i < topoDiagram.getElements().size() - 1; i++) {
//							try {
//								actionFlowCheck = (ActionOfFlow) topoDiagram.getElements().get(i).getData();
//								if (actionFlow.equals(actionFlowCheck)) {
//									topoDiagram.getElements().get(i).setStyleClass(getElementStyle(getActionRunStatus(actionFlow), actionFlow.getIfValue()));
//								}
//								
//							} catch (Exception e) {
//								logger.error(e.getMessage(), e);
//							}
//						} // ket thuc vong lap
//					}
//				} catch (Exception e) {
//					logger.error(e.getMessage(), e);
//				}
//				
//			}
        }

        String idElementToScroll = null;
        if (nodeRunning != null) {
            idElementToScroll = nodeRunning;
        } else if (nodeFinish != null) {
            idElementToScroll = nodeFinish;
        } else if (nodeFail != null) {
            idElementToScroll = nodeFail;
        }

        if (idElementToScroll != null) {
            RequestContext.getCurrentInstance().addCallbackParam("idElementToScroll", idElementToScroll);
        }

        /*
         * Kiem tra xem CR da thuc hien xong hay chua
         */
        try {
            selectedFlowRunAction = new FlowRunActionServiceImpl().findById(selectedFlowRunAction.getFlowRunId());
            if (selectedFlowRunAction.getStatus() != null) {

                String finalElementStatusClass = "ui-diagram-end";
                if (selectedFlowRunAction.getStatus().intValue() == Config.FINISH_FLAG) {
                    finalElementStatusClass = "ui-diagram-success";
//                    MessageUtil.setInfoMessageFromRes("note.diagram.success");
                    RequestContext.getCurrentInstance().execute("PF('scheduleUpdateTopo').stop();");
                } else if (selectedFlowRunAction.getStatus().intValue() == Config.FAIL_FLAG
                        || selectedFlowRunAction.getStatus().intValue() == Config.STOP_FLAG
                        || (selectedFlowRunAction.getStatus().intValue() == Config.LOGIN_FAIL_FLAG && !isActionManual)
                        || selectedFlowRunAction.getStatus().intValue() == Config.PAUSE_FLAG) {
                    finalElementStatusClass = "ui-diagram-fail";
                    MessageUtil.setErrorMessageFromRes("note.diagram.fail");
                    RequestContext.getCurrentInstance().execute("PF('scheduleUpdateTopo').stop();");
                }

                topoDiagram.getElements().get(topoDiagram.getElements().size() - 1).setStyleClass(finalElementStatusClass);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    private String getElementStyle(Integer actionStatus, String actionType) {
        String elementStyleClass = "ui-diagram-default";
        try {
            switch (actionType) {

                // action rollback
                case "0":

                    switch (actionStatus) {
                        case ACTION_FAIL_EXEC:
                            elementStyleClass = "ui-diagram-rollback-fail";
                            break;
                        case ACTION_RUNNING_EXEC:
                            elementStyleClass = "ui-diagram-rollback-running";
                            break;
                        case ACTION_SUCCESS_EXEC:
                            elementStyleClass = "ui-diagram-rollback-success";
                            break;
                        default:
                            elementStyleClass = "ui-diagram-rollback-default";
                            break;
                    }
                    break;

                // action thuc thi
                case "1":
                    switch (actionStatus) {
                        case ACTION_FAIL_EXEC:
                            elementStyleClass = "ui-diagram-fail";
                            break;
                        case ACTION_RUNNING_EXEC:
                            elementStyleClass = "ui-diagram-running";
                            break;
                        case ACTION_SUCCESS_EXEC:
                            elementStyleClass = "ui-diagram-success";
                            break;
                        default:
                            elementStyleClass = "ui-diagram-default";
                            break;
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return elementStyleClass;
    }

    private Integer getActionRunStatus(ActionOfFlow action) {
        Integer status = ACTION_DETAULT;
        try {
            Map<String, Object> filters = new HashMap<>();
            filters.put("actionOfFlowId", action.getStepNum());
            filters.put("type", ACTION_RUN_AUTO);
            filters.put("flowRunLogId", selectedFlowRunAction.getFlowRunId());
            if (selectSubflowRun != null) {
                filters.put("subFlowRun", selectSubflowRun == null ? "Default" : selectSubflowRun);
            }
            List<FlowRunLogAction> lstLogAction = new FlowRunLogActionServiceImpl().findList(filters);
            //Lay danh sach action manual
            filters.clear();
            filters.put("actionType", 1L);
            filters.put("flowTemplates.flowTemplatesId", selectedFlowRunAction.getFlowTemplates().getFlowTemplatesId());
            List<ActionOfFlow> lstActionOfFlow = new ActionOfFlowServiceImpl().findList(filters);
            if (selectSubflowRun.equals(Config.SUB_FLOW_RUN_DEFAULT) && lstLogAction.isEmpty()) {
                filters.clear();
                filters.put("actionOfFlowId", action.getStepNum());
                filters.put("type", ACTION_RUN_AUTO);
                filters.put("flowRunLogId", selectedFlowRunAction.getFlowRunId());
                lstLogAction = new FlowRunLogActionServiceImpl().findList(filters);
            }
            HashMap<String, String> mapActionManual = new HashMap<>();
            for (ActionOfFlow aof : lstActionOfFlow) {
                mapActionManual.put(aof.getStepNum().toString(), aof.getStepNum().toString());
            }
            if (lstLogAction != null && !lstLogAction.isEmpty()) {

                // kiem tra trang thai action loi
                for (FlowRunLogAction logAction : lstLogAction) {
                    if (logAction.getResult() != null
                            && logAction.getResult().equals(ERROR_CMD)) {
                        status = ACTION_FAIL_EXEC;
                        break;
                    }
                }

                /*
                 *  Neu action dang thuc thi
                 *  Kiem tra xem action da thuc thi xong chua
                 */
                if (status != ACTION_FAIL_EXEC) {

                    if (selectedFlowRunAction.getStatus().equals(Config.FLOW_RUN_ACTION_FAIL_STATUS)
                            || selectedFlowRunAction.getStatus().equals(Config.FLOW_RUN_ACTION_FINISH_STATUS)
                            || selectedFlowRunAction.getStatus().equals(Config.PAUSE_FLAG)) {
                        status = ACTION_SUCCESS_EXEC;

                    } else {
                        Map<Long, Integer> mapNodeTotalClone = new HashMap<>();
                        Map<Long, Integer> mapNodeCloneFinish = new HashMap<>();

                        for (FlowRunLogAction logAction : lstLogAction) {
                            if (mapActionManual.containsKey(logAction.getActionOfFlowId().toString()) && logAction.getResult() == null) {
                                return ACTION_MANUAL;
                            }
                            if (mapNodeTotalClone.get(logAction.getNodeId()) == null) {
                                if (logAction.getCloneNumber().equals(0L)) {
                                    mapNodeTotalClone.put(logAction.getNodeId(), logAction.getCloneTotal().intValue());
                                }
                            }

                            if (logAction.getFinishTime() != null) {
                                if (mapNodeCloneFinish.get(logAction.getNodeId()) == null) {
                                    mapNodeCloneFinish.put(logAction.getNodeId(), 1);
                                } else {
                                    mapNodeCloneFinish.put(logAction.getNodeId(), mapNodeCloneFinish.get(logAction.getNodeId()) + 1);
                                }
                            }
                        }

                        boolean isFinish = true;
                        for (Map.Entry<Long, Integer> entry : mapNodeTotalClone.entrySet()) {
                            if ((mapNodeCloneFinish.get(entry.getKey()) == null)
                                    || (mapNodeCloneFinish.get(entry.getKey()) < entry.getValue())) {
                                isFinish = false;
                                break;
                            }
                        }
                        status = (isFinish ? ACTION_SUCCESS_EXEC : ACTION_RUNNING_EXEC);
                    }
                }

            } else {
                status = ACTION_DETAULT;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            status = ACTION_DETAULT;
        }
        return status;
    }

    public void onRightClickDiagram() {
        try {
            logAction = LogUtils.addContent("", "Right Click action");
            logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunName());
            logAction = LogUtils.addContent(logAction, "actionOfFlowId: " + actionOfFlowId);
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            actionOfFlowId = Long.parseLong(params.get("action_of_flow_id"));
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logAction = LogUtils.addContent(logAction, "Exception: " + e.getMessage());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        }
    }

    public void onClickRollback() {
        try {
            account = null;
            password = null;
            actionOfFlowId = null;
            logAction = LogUtils.addContent("", "Click rolback DT");
            if (selectedFlowRunAction.getFlowRunId() == null) {
                logAction = LogUtils.addContent(logAction, "FlowRunId: null");
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                return;
            }
            logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "FlowRunName: " + selectedFlowRunAction.getFlowRunName());
            selectedFlowRunAction = new FlowRunActionServiceImpl().findById(selectedFlowRunAction.getFlowRunId());
            if (selectedFlowRunAction != null) {
                if (selectedFlowRunAction.getStatus() == 3
                        || selectedFlowRunAction.getStatus() == 4
                        || selectedFlowRunAction.getStatus().equals(Config.STOP_FLAG)
                        || selectedFlowRunAction.getStatus().equals(Config.LOGIN_FAIL_FLAG)
                        || selectedFlowRunAction.getStatus().equals(Config.PAUSE_FLAG)) {
                    Integer status = new FlowTemplatesServiceImpl().findById(selectedFlowRunAction.getFlowTemplates().getFlowTemplatesId()).getStatus();
                    if (status == null || status != 9) {
                        MessageUtil.setErrorMessageFromRes("error.template.not.approved");
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                        LogUtils.writelog(new Date(), className, new Object() {
                        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);

                        return;
                    }
                    if (new Date().after(selectedFlowRunAction.getTimeRun())) {
                        if (selectedFlowRunAction.getCrNumber() != null) {
                            Boolean canExecute = GNOCService.isCanExecute(selectedFlowRunAction.getCrNumber());
                            if (canExecute == null) {
                                MessageUtil.setErrorMessageFromRes("error.user.cannot.execute");
                                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.user.cannot.execute"));
                            } else {
                                if (canExecute || selectedFlowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
                                        || selectedFlowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
                                    account = null;
                                    password = null;
                                    modeRun = 2;
                                    runType = 2;
                                    runningType = Config.RUNNING_TYPE_DEPENDENT.intValue();
                                    oneAccount = false;
                                    List<NodeRun> listNodeRun = selectedFlowRunAction.getNodeRuns();
                                    mapAccount = new HashMap<>();
                                    if (listNodeRun != null) {
                                        for (NodeRun nodeRun : listNodeRun) {
                                            mapAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj());
                                        }
                                        //20171409_Quytv7_account/pass lay tu DB_Start
                                        for (NodeRun nodeRun : listNodeRun) {
                                            try {
                                                if (mapAccount.containsKey(nodeRun.getNode().getNodeCode())) {
                                                    if (nodeRun.getNode().getAccount() != null && !"".equals(nodeRun.getNode().getAccount())) {
                                                        mapAccount.get(nodeRun.getNode().getNodeCode()).setAccount(PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT));
                                                    }
                                                    if (nodeRun.getNode().getPassword() != null && !"".equals(nodeRun.getNode().getPassword())) {
                                                        mapAccount.get(nodeRun.getNode().getNodeCode()).setPassword(PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT));
                                                    }
                                                }
                                            } catch (Exception ex) {
                                                logger.error(ex.getMessage(), ex);
                                            }
                                            try {
                                                if (oneAccount && account != null && !account.trim().isEmpty() && password != null && !password.trim().isEmpty()) {
                                                    if (nodeRun.getNode().getAccount() != null && nodeRun.getNode().getPassword() != null
                                                            && !nodeRun.getNode().getAccount().isEmpty() && !nodeRun.getNode().getPassword().isEmpty()
                                                            && PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT) != null
                                                            && !"".equals(PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT))
                                                            && PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT) != null
                                                            && !"".equals(PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT))
                                                            )
                                                        account = PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT);
                                                    password = PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT);
                                                }
                                            } catch (Exception ex) {
                                                logger.error(ex.getMessage(), ex);
                                            }
                                        }
                                        //20171409_Quytv7_account/pass lay tu DB_End
                                    }
                                    RequestContext.getCurrentInstance().update("formShowLog");
                                    RequestContext.getCurrentInstance().execute("PF('dlgAcountInfo').show()");
                                    activeIndex = 0;
                                    logAction = LogUtils.addContent(logAction, "Result: Show dialog rollback dt");
                                } else {
                                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.cr.cannot.execute"));
                                    MessageUtil.setErrorMessageFromRes("error.cr.cannot.execute");
                                }
                            }
                        } else {
                            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.cr.notfound"));
                            MessageUtil.setErrorMessageFromRes("label.err.cr.notfound");
                        }
                    } else {
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.dt.cannot.execute.this.time"));
                        MessageUtil.setErrorMessageFromRes("error.dt.cannot.execute.this.time");
                    }
                } else {
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.flow.not.allowrun"));
                    MessageUtil.setErrorMessageFromRes("label.err.flow.not.allowrun");
                }
            } else {
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.flow.not.allowrun"));
                MessageUtil.setErrorMessageFromRes("label.err.flow.not.allowrun");
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Ham xu ly khi goi su kien kich chuot vao action
     */
    public void onClickDiagram() {
        try {
            account = null;
            password = null;
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            String actionFlowId = params.get("action_of_flow_id");
            String actionType = params.get("actionType");

            /*
             * Neu la thuc hien chay auto ca luong
             */
            if (Long.valueOf(actionFlowId).equals(ELEMENT_START)) {
                logAction = LogUtils.addContent("", "Click double ELEMENT_START");
                /*
                 *  Kiem tra xem CR da chay hay chua
                 *  1.Neu chua chay thi hien thi man hinh nhap thong tin ket noi
                 *  2.Neu da chay roi thi dua ra thong bao loi template da chay
                 */
                if (selectedFlowRunAction.getFlowRunId() == null) {
                    logAction = LogUtils.addContent(logAction, "FlowRunId: null");
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                    return;
                }
                logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
                logAction = LogUtils.addContent(logAction, "FlowRunName: " + selectedFlowRunAction.getFlowRunName());
                selectedFlowRunAction = new FlowRunActionServiceImpl().findById(selectedFlowRunAction.getFlowRunId());
                if (selectedFlowRunAction != null) {

                    //1. CR chua thuc hien
                    if (selectedFlowRunAction.getStatus().equals(Config.WAITTING_FLAG)
                            || selectedFlowRunAction.getStatus().equals(Config.STOP_FLAG)
                            || selectedFlowRunAction.getStatus().equals(Config.LOGIN_FAIL_FLAG)
                            || selectedFlowRunAction.getStatus().equals(Config.PAUSE_FLAG)) {

                        Integer status = new FlowTemplatesServiceImpl().findById(selectedFlowRunAction.getFlowTemplates().getFlowTemplatesId()).getStatus();

                        if (status == null || status != 9) {
                            MessageUtil.setErrorMessageFromRes("error.template.not.approved");
                            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                            LogUtils.writelog(new Date(), className, new Object() {
                            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                            return;
                        }
                        if (new Date().after(selectedFlowRunAction.getTimeRun())) {
                            if (selectedFlowRunAction.getCrNumber() != null) {
                                Boolean canExecute = GNOCService.isCanExecute(selectedFlowRunAction.getCrNumber());
                                if (canExecute == null) {
                                    MessageUtil.setErrorMessageFromRes("error.user.cannot.execute");
                                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.user.cannot.execute"));
                                } else {
                                    if (selectedFlowRunAction.getRollbackMode() != null && selectedFlowRunAction.getRollbackMode().equals(1l) && selectedFlowRunAction.getFlowRunRollbackId() == null) {
                                        MessageUtil.setErrorMessageFromRes("error.flow.rollback.isnull");
                                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.flow.rollback.isnull"));
                                    } else {
                                        if (canExecute || selectedFlowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
                                                || selectedFlowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
                                            account = null;
                                            password = null;
                                            modeRun = 1;
                                            runType = 1;
                                            runningType = Config.RUNNING_TYPE_DEPENDENT.intValue();
                                            oneAccount = false;

                                            List<NodeRun> listNodeRun = selectedFlowRunAction.getNodeRuns();
                                            mapAccount = new HashMap<>();
                                            if (listNodeRun != null) {
                                                for (NodeRun nodeRun : listNodeRun) {
                                                    mapAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj());
                                                }
                                                //20171409_Quytv7_account/pass lay tu DB_Start
                                                for (NodeRun nodeRun : listNodeRun) {
                                                    try {
                                                        if (mapAccount.containsKey(nodeRun.getNode().getNodeCode())) {
                                                            if (nodeRun.getNode().getAccount() != null && !"".equals(nodeRun.getNode().getAccount())) {
                                                                mapAccount.get(nodeRun.getNode().getNodeCode()).setAccount(PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT));
                                                            }
                                                            if (nodeRun.getNode().getPassword() != null && !"".equals(nodeRun.getNode().getPassword())) {
                                                                mapAccount.get(nodeRun.getNode().getNodeCode()).setPassword(PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT));
                                                            }
                                                        }
                                                    } catch (Exception ex) {
                                                        logger.error(ex.getMessage(), ex);
                                                    }
                                                    try {
                                                        if (oneAccount && account != null && !account.trim().isEmpty() && password != null && !password.trim().isEmpty()) {
                                                            if (nodeRun.getNode().getAccount() != null && nodeRun.getNode().getPassword() != null
                                                                    && !nodeRun.getNode().getAccount().isEmpty() && !nodeRun.getNode().getPassword().isEmpty()
                                                                    && PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT) != null
                                                                    && !"".equals(PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT))
                                                                    && PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT) != null
                                                                    )                                                                    && !"".equals(PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT))

                                                            account = PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT);
                                                            password = PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT);
                                                        }
                                                    } catch (Exception ex) {
                                                        logger.error(ex.getMessage(), ex);
                                                    }
                                                }
                                                //20171409_Quytv7_account/pass lay tu DB_End
                                            }
                                            RequestContext.getCurrentInstance().update("formShowLog");
                                            RequestContext.getCurrentInstance().execute("PF('dlgAcountInfo').show()");

                                            activeIndex = 0;
                                            logAction = LogUtils.addContent(logAction, "Result: Show dialog run dt");
                                        } else {
                                            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.cr.cannot.execute"));
                                            MessageUtil.setErrorMessageFromRes("error.cr.cannot.execute");
                                        }
                                    }
                                }
                            } else {
                                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.cr.notfound"));
                                MessageUtil.setErrorMessageFromRes("label.err.cr.notfound");
                            }
                        } else {
                            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.dt.cannot.execute.this.time"));
                            MessageUtil.setErrorMessageFromRes("error.dt.cannot.execute.this.time");
                        }
                        //2. CR da thuc hien
                    } else {
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.flow.not.allowrun"));
                        MessageUtil.setErrorMessageFromRes("label.err.flow.not.allowrun");
                    }
                } else {
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.flow.not.allowrun"));
                    MessageUtil.setErrorMessageFromRes("label.err.flow.not.allowrun");
                }

            } else if (!Long.valueOf(actionFlowId).equals(ELEMENT_END)) {
                logAction = LogUtils.addContent("", "Show log action");
                if (actionType.equals(Config.ACTION_TYPE_MANUAL.toString())) {
                    isActionManual = true;
                    showLog(Long.valueOf(actionFlowId));
                    logAction = LogUtils.addContent(logAction, "Action manual id:" + actionFlowId);
                } else {
                    isActionManual = false;
                    showLog(Long.valueOf(actionFlowId));
                    logAction = LogUtils.addContent(logAction, "Action normal id:" + actionFlowId);
                }
                // Hien thi log tac dong cua action
                RequestContext.getCurrentInstance().execute("PF('dlgShowLogAction').show()");
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    //20171031_hienhv4_load tham so truoc khi chay_start
    public void onClickPauseDT() {
        try {
            logAction = LogUtils.addContent("", "Click button pauseDT");
            FlowRunAction flowRun = flowRunActionService.findById(selectedFlowRunAction.getFlowRunId());

            Long portRun = flowRun.getPortRun();
            String ipRun = flowRun.getIpRun();

            logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "FlowRunName: " + selectedFlowRunAction.getFlowRunName());
            logAction = LogUtils.addContent(logAction, "IPRun: " + selectedFlowRunAction.getIpRun());
            logAction = LogUtils.addContent(logAction, "PortRun: " + selectedFlowRunAction.getPortRun());

            String encrytedMess = new String(Base64.encodeBase64(("STOP:flowRunId=" + flowRun.getFlowRunId()).getBytes("UTF-8")), "UTF-8");

            if (portRun != null && ipRun != null) {
                SocketClient client = new SocketClient(ipRun, portRun.intValue());
                client.sendMsg(encrytedMess);
            } else {
                String countryCode = selectedFlowRunAction.getCountryCode() == null ? Constants.VNM : selectedFlowRunAction.getCountryCode().getCountryCode();

                Map<String, Object> filters = new HashMap<>();
                filters.put("countryCode.countryCode-" + MapProcessCountryServiceImpl.EXAC, countryCode);
                filters.put("status", 1l);

                List<MapProcessCountry> maps = new MapProcessCountryServiceImpl().findList(filters);

                if (maps != null && !maps.isEmpty()) {
                    for (MapProcessCountry process : maps) {
                        try {
                            int serverPort = process.getProcessPort();
                            String serverIp = process.getProcessIp();

                            SocketClient client = new SocketClient(serverIp, serverPort);
                            client.sendMsg(encrytedMess);
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                        }
                    }
                }
            }

            MessageUtil.setInfoMessage(MessageUtil.getResourceBundleMessage("message.pause.dt.success"));
            logAction = LogUtils.addContent(logAction, "Result: SUCCESS");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setInfoMessage(MessageUtil.getResourceBundleMessage("common.fail", MessageUtil.getResourceBundleMessage("label.diagram.element.pause")));
            logAction = LogUtils.addContent(logAction, "Result: " + e.getMessage());
        }

        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
    }
    //20171031_hienhv4_load tham so truoc khi chay_end

    public void onClickRunDT() {
        try {

            account = null;
            password = null;

//            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//            String actionFlowId = params.get("action_of_flow_id");

            /*
             * Neu la thuc hien chay auto ca luong
             */

            logAction = LogUtils.addContent("", "Click button start run DT");
            /*
             *  Kiem tra xem CR da chay hay chua
             *  1.Neu chua chay thi hien thi man hinh nhap thong tin ket noi
             *  2.Neu da chay roi thi dua ra thong bao loi template da chay
             */
            if (selectedFlowRunAction.getFlowRunId() == null) {
                logAction = LogUtils.addContent(logAction, "FlowRunId: null");
                LogUtils.writelog(new Date(), className, new Object() {
                }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                return;
            }
            actionOfFlowId = ELEMENT_START;
            logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "FlowRunName: " + selectedFlowRunAction.getFlowRunName());

            selectedFlowRunAction = new FlowRunActionServiceImpl().findById(selectedFlowRunAction.getFlowRunId());
            if (selectedFlowRunAction != null) {

                //1. CR chua thuc hien
                if (selectedFlowRunAction.getStatus().equals(Config.WAITTING_FLAG)
                        || selectedFlowRunAction.getStatus().equals(Config.STOP_FLAG)
                        || selectedFlowRunAction.getStatus().equals(Config.LOGIN_FAIL_FLAG)
                        || selectedFlowRunAction.getStatus().equals(Config.PAUSE_FLAG)) {

                    Integer status = new FlowTemplatesServiceImpl().findById(selectedFlowRunAction.getFlowTemplates().getFlowTemplatesId()).getStatus();

                    if (status == null || status != 9) {
                        MessageUtil.setErrorMessageFromRes("error.template.not.approved");
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                        LogUtils.writelog(new Date(), className, new Object() {
                        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);
                        return;
                    }
                    if (new Date().after(selectedFlowRunAction.getTimeRun())) {
                        if (selectedFlowRunAction.getCrNumber() != null) {
                            Boolean canExecute = GNOCService.isCanExecute(selectedFlowRunAction.getCrNumber());
                            if (canExecute == null) {
                                MessageUtil.setErrorMessageFromRes("error.user.cannot.execute");
                                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.user.cannot.execute"));
                            } else {
                                if (selectedFlowRunAction.getRollbackMode().equals(1l) && selectedFlowRunAction.getFlowRunRollbackId() == null) {
                                    MessageUtil.setErrorMessageFromRes("error.flow.rollback.isnull");
                                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.flow.rollback.isnull"));
                                } else {
                                    if (canExecute || selectedFlowRunAction.getCrNumber().equals(Config.CR_DEFAULT)
                                            || selectedFlowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER)) {
                                        account = null;
                                        password = null;
                                        modeRun = 1;
                                        runType = 1;
                                        runningType = Config.RUNNING_TYPE_DEPENDENT.intValue();
                                        oneAccount = false;

                                        List<NodeRun> listNodeRun = selectedFlowRunAction.getNodeRuns();
                                        mapAccount = new HashMap<>();
                                        if (listNodeRun != null) {
                                            for (NodeRun nodeRun : listNodeRun) {
                                                mapAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj());
                                            }
                                            //20171409_Quytv7_account/pass lay tu DB_Start
                                            for (NodeRun nodeRun : listNodeRun) {
                                                try {
                                                    if (mapAccount.containsKey(nodeRun.getNode().getNodeCode())) {
                                                        if (nodeRun.getNode().getAccount() != null && !"".equals(nodeRun.getNode().getAccount())) {
                                                            mapAccount.get(nodeRun.getNode().getNodeCode()).setAccount(PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT));
                                                        }
                                                        if (nodeRun.getNode().getPassword() != null && !"".equals(nodeRun.getNode().getPassword())) {
                                                            mapAccount.get(nodeRun.getNode().getNodeCode()).setPassword(PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT));
                                                        }
                                                    }
                                                } catch (Exception ex) {
                                                    logger.error(ex.getMessage(), ex);
                                                }
                                                try {
                                                    if (oneAccount && account != null && !account.trim().isEmpty() && password != null && !password.trim().isEmpty()) {
                                                        if (nodeRun.getNode().getAccount() != null && nodeRun.getNode().getPassword() != null
                                                                && !nodeRun.getNode().getAccount().isEmpty() && !nodeRun.getNode().getPassword().isEmpty()
                                                                && PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT) != null
                                                                && !"".equals(PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT))
                                                                && PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT) != null
                                                                && !"".equals(PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT))
                                                                )
                                                            account = PassProtector.decrypt(nodeRun.getNode().getAccount(), Config.SALT);
                                                        password = PassProtector.decrypt(nodeRun.getNode().getPassword(), Config.SALT);
                                                    }
                                                } catch (Exception ex) {
                                                    logger.error(ex.getMessage(), ex);
                                                }
                                            }
                                            //20171409_Quytv7_account/pass lay tu DB_End
                                        }
                                        RequestContext.getCurrentInstance().update("formShowLog");
                                        RequestContext.getCurrentInstance().execute("PF('dlgAcountInfo').show()");

                                        activeIndex = 0;
                                        logAction = LogUtils.addContent(logAction, "Result: Show dialog run dt");
                                    } else {
                                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.cr.cannot.execute"));
                                        MessageUtil.setErrorMessageFromRes("error.cr.cannot.execute");
                                    }
                                }
                            }
                        } else {
                            logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.cr.notfound"));
                            MessageUtil.setErrorMessageFromRes("label.err.cr.notfound");
                        }
                    } else {
                        logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("error.dt.cannot.execute.this.time"));
                        MessageUtil.setErrorMessageFromRes("error.dt.cannot.execute.this.time");
                    }
                    //2. CR da thuc hien
                } else {
                    logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.flow.not.allowrun"));
                    MessageUtil.setErrorMessageFromRes("label.err.flow.not.allowrun");
                }
            } else {
                logAction = LogUtils.addContent(logAction, "Result: " + MessageUtil.getResourceBundleMessage("label.err.flow.not.allowrun"));
                MessageUtil.setErrorMessageFromRes("label.err.flow.not.allowrun");
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.VIEW.name(), logAction);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void showLogActionAuto(Long actionFlowId) {
        mapLogCommand.clear();
        if (selectSubflowRun == null) {
            selectSubflowRun = Config.SUB_FLOW_RUN_DEFAULT;
        }


        List<ActionOfFlow> actionOfFlowsLog = null;
        Set<Long> StepNumActions = new HashSet<>();
        try {
            ActionOfFlow actionOfFlow;
            actionOfFlow = new ActionOfFlowServiceImpl().findById(actionFlowId);

            if (actionOfFlow != null) {
                HashMap<String, Object> filter = new HashMap<>();
                filter.put("stepNumberLabel", actionOfFlow.getStepNumberLabel());
                filter.put("action.actionId", actionOfFlow.getAction().getActionId());
                filter.put("flowTemplates.flowTemplatesId", actionOfFlow.getFlowTemplates().getFlowTemplatesId());

                actionOfFlowsLog = new ActionOfFlowServiceImpl().findList(filter);
                for (ActionOfFlow actionOfFlow1 : actionOfFlowsLog) {
                    //20171113_hienhv4_fix loi hien thi log trung_start
                    if (actionOfFlow1.getGroupActionName().equals(actionOfFlow.getGroupActionName())
                            && actionOfFlow1.getGroupActionOrder().equals(actionOfFlow.getGroupActionOrder())) {
                        StepNumActions.add(actionOfFlow1.getStepNum());
                    }
                    //20171113_hienhv4_fix loi hien thi log trung_end
                }
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        if (StepNumActions.isEmpty()) {
            StepNumActions.add(actionFlowId);
        }
        StringBuilder hql = new StringBuilder();
        hql.append(" select new FlowRunLogAction(flowRunLogAction, actionOfFlow, node) from FlowRunLogAction as flowRunLogAction ");
        hql.append(" left join ActionOfFlow as actionOfFlow on flowRunLogAction.actionOfFlowId = actionOfFlow.stepNum");
        hql.append(" left join Node as node on flowRunLogAction.nodeId = node.nodeId");
        hql.append(" where flowRunLogAction.type = 0 and flowRunLogAction.flowRunLogId = ? and actionOfFlow.stepNum = ?");
        if (selectSubflowRun != null) {
            hql.append(" and flowRunLogAction.subFlowRun = ?");
        }
        hql.append(" order by node.nodeId, flowRunLogAction.startTime, actionOfFlow.stepNumberLabel, flowRunLogAction.cloneNumber");

        List<FlowRunLogAction> lstLogAction = new ArrayList<>();
        try {

            if (isActionManual) {
                HashMap<String, Object> filters = new HashMap<>();
                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                filters.put("flowRunLogId", selectedFlowRunAction.getFlowRunId());

                orders.put("startTime", "DESC");
                orders.put("runLogActionId", "DESC");
                lstLogAction = new FlowRunLogActionServiceImpl().findList(filters, orders);
                FlowRunLogAction frla = new FlowRunLogAction();
                if (!lstLogAction.isEmpty()) {
                    for (FlowRunLogAction flowRunLogAction : lstLogAction) {
                        if (flowRunLogAction.getActionOfFlowId().equals(actionFlowId)) {
                            frla = flowRunLogAction;
                            break;
                        }
                    }
                    for (FlowRunLogAction flowRunLogAction : lstLogAction) {
                        if (flowRunLogAction.getFinishTime() != null && flowRunLogAction.getRunLogActionId() < frla.getRunLogActionId()
                                && flowRunLogAction.getCountCmd() != null && flowRunLogAction.getCountCmd() > 0) {
                            if (selectSubflowRun != null) {
                                lstLogAction = new FlowRunLogActionServiceImpl().findList(hql.toString(), -1, -1, selectedFlowRunAction.getFlowRunId(), flowRunLogAction.getActionOfFlowId(), selectSubflowRun);//.findList(filters, orders);
                            } else {
                                lstLogAction = new FlowRunLogActionServiceImpl().findList(hql.toString(), -1, -1, selectedFlowRunAction.getFlowRunId(), flowRunLogAction.getActionOfFlowId());//.findList(filters, orders);
                            }
                            break;
                        }
                    }
                }
            } else {
                List<FlowRunLogAction> lstLogActionTemp;
                for (Long stepnum : StepNumActions) {
                    if (selectSubflowRun != null) {
                        lstLogActionTemp = new FlowRunLogActionServiceImpl().findList(hql.toString(), -1, -1, selectedFlowRunAction.getFlowRunId(), stepnum, selectSubflowRun);//.findList(filters, orders);
                    } else {
                        lstLogActionTemp = new FlowRunLogActionServiceImpl().findList(hql.toString(), -1, -1, selectedFlowRunAction.getFlowRunId(), stepnum);//.findList(filters, orders);
                    }
                    lstLogAction.addAll(lstLogActionTemp);
                }

                //13102017_Quytv7 sua llay tat ca log action co cung stepNumberLabel_start

                //13102017_Quytv7 sua llay tat ca log action co cung stepNumberLabel_end
                if (lstLogAction.isEmpty()) {
                    List<ActionOfFlow> lstAction = selectedFlowRunAction.getFlowTemplates().getActionOfFlows();
                    for (ActionOfFlow actionFlow : lstAction) {
                        if (actionFlow.getStepNum().equals(actionFlowId)) {
                            for (ActionOfFlow action : lstAction) {
                                if (action.getGroupActionOrder().equals(actionFlow.getGroupActionOrder())
                                        && action.getStepNumberLabel().equals(actionFlow.getStepNumberLabel())
                                        && !action.getStepNum().equals(actionFlowId)) {
                                    if (selectSubflowRun != null) {
                                        lstLogAction = new FlowRunLogActionServiceImpl().findList(hql.toString(), -1, -1,
                                                selectedFlowRunAction.getFlowRunId(), action.getStepNum(), selectSubflowRun);//.findList(filters, orders);
                                    } else {
                                        lstLogAction = new FlowRunLogActionServiceImpl().findList(hql.toString(), -1, -1,
                                                selectedFlowRunAction.getFlowRunId(), action.getStepNum());//.findList(filters, orders);
                                    }
                                    if (lstLogAction != null && !lstLogAction.isEmpty()) {
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }

            if (lstLogAction != null && !lstLogAction.isEmpty()) {
                //20171212_Quytv7 sua da co lenh thi khong can xet truong hop cap nhat cham_start
                boolean isCheckExistCmd = false;
                //20171212_Quytv7 sua da co lenh thi khong can xet truong hop cap nhat cham_end
                LinkedHashMap<String, String> ordersCmd = new LinkedHashMap<>();
                ordersCmd.put("orderRun", "ASC");
                ordersCmd.put("cloneNumber", "ASC");
                //Node node = null;

                for (FlowRunLogAction logActionLocal : lstLogAction) {

                    try {
                        Map<String, Object> filtersCmd = new HashMap<>();
                        filtersCmd.put("runLogActionId", logActionLocal.getRunLogActionId());

                        //node = new NodeServiceImpl().findById(logAction.getNode().getNodeId());
                        if (logActionLocal.getNode() != null) {
                            //Truong hop lenh cap nhat cham, chay them 2 lan nua
                            List<FlowRunLogCommand> lstCmd = new FlowRunLogCommandServiceImpl().findList(filtersCmd, ordersCmd);
                            if (isActionManual && !isCheckExistCmd) {
                                for (int i = 0; i < 3; i++) {
                                    if (lstCmd.isEmpty()) {
                                        Thread.sleep(2000);
                                        logger.info("Truong hop lstcmd rong lan:" + i);
                                        lstCmd = new FlowRunLogCommandServiceImpl().findList(filtersCmd, ordersCmd);
                                    } else {
                                        isCheckExistCmd = true;
                                        break;
                                    }
                                }
                            }

                            //logger.info("Show lenh lstCmd size = " + lstCmd.size());
                            String actionName = ((logActionLocal.getActionOfFlow() != null
                                    && logActionLocal.getActionOfFlow().getAction() != null) ? logActionLocal.getActionOfFlow().getAction().getName() : "")
                                    + "_" + logActionLocal.getCloneNumber();
                            if (lstCmd != null) {
                                for (FlowRunLogCommand cmd : lstCmd) {
                                    cmd.setActionName(actionName);
                                    if (cmd.getCmdRun() == null || "".equals(cmd.getCmdRun())) {
                                        lstCmd.remove(cmd);
                                    }
                                }
                                if (lstCmd.size() > 0) {
//                                logger.info("Show lenh lstCmd size = " + lstCmd.size());
                                    //20171031_hienhv4_them mau sac cho node chay loi tren topo_start
                                    if (!mapLogCommand.containsKey(logActionLocal.getNode().getNodeCode())) {
                                        mapLogCommand.put(logActionLocal.getNode().getNodeCode(), new LogNodeObj(LogNodeStatus.SUCESS.getStatus()));
                                    }
                                    mapLogCommand.get(logActionLocal.getNode().getNodeCode()).getLstLogCmd().addAll(lstCmd);
                                    //20171031_hienhv4_them mau sac cho node chay loi tren topo_end
                                }
                            }
                        }

                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }

                } // ket thuc vong lap log action

                //20171031_hienhv4_them mau sac cho node chay loi tren topo_start
                // Cap nhat ket qua chay thanh cong hay that bai cua action
                for (FlowRunLogAction action : lstLogAction) {
                    if (action.getResult() != null && action.getResult().intValue() == 0) {
                        mapLogCommand.get(action.getNode().getNodeCode()).setStatus(LogNodeStatus.FAIL.getStatus());
                    }
                }
                //20171031_hienhv4_them mau sac cho node chay loi tren topo_end
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void showLog(Long actionFlowId) {
        try {
            selectedActionFlow = null;
            try {
                selectedActionFlow = new ActionOfFlowServiceImpl().findById(actionFlowId);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                selectedActionFlow = null;
            }

            showLogActionAuto(actionFlowId);

            showLogManual();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void showLogManual() {
        try {
            Map<String, Object> filters1 = new HashMap<>();
            filters1.put("actionOfFlowId", selectedActionFlow.getStepNum());
            filters1.put("flowRunId", selectedFlowRunAction.getFlowRunId());

            LinkedHashMap<String, String> orders1 = new LinkedHashMap<>();
            orders1.put("createTime", "DESC");

            lstFlowRunLog = new LazyDataModelBaseNew<>(flowRunLogService, filters1, orders1);

            List<FlowRunLog> lst = lstFlowRunLog.load(0, 1, "createTime", SortOrder.DESCENDING, filters1);
            if (lst != null && !lst.isEmpty()) {
                selectedFlowRunLog = lst.get(0);

                getLogActionManual(selectedFlowRunLog.getFlowRunLogId());
            } else {
                mapLogCommandManual.clear();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void getLogActionManual(SelectEvent event) {
        mapLogCommandManual.clear();
        getLogActionManual(((FlowRunLog) event.getObject()).getFlowRunLogId());
    }

    private void getLogActionManual(Long flowRunLogId) {
        try {
            mapLogCommandManual.clear();
            //Map<String, Object> filters2 = new HashMap<>();
            //filters2.put("actionOfFlowId", selectedActionFlow.getStepNum());
            //filters2.put("type", ACTION_RUN_MANUAL);
            //filters2.put("flowRunLogId", flowRunLogId);

//            LinkedHashMap<String, String> orders2 = new LinkedHashMap<>();
//            orders2.put("node.nodeId", "ASC");
//            orders2.put("actionOfFlow.stepNumberLabel", "ASC");
//            orders2.put("cloneNumber", "ASC");
            String hql = "select new FlowRunLogAction(flowRunLogAction, actionOfFlow, node) from FlowRunLogAction as flowRunLogAction"
                    + " left join ActionOfFlow as actionOfFlow on flowRunLogAction.actionOfFlowId = actionOfFlow.stepNum"
                    + " left join Node as node on flowRunLogAction.nodeId = node.nodeId"
                    + " where flowRunLogAction.type = 1 and flowRunLogAction.flowRunLogId = ?"
                    + " order by node.nodeId, flowRunLogAction.startTime, actionOfFlow.stepNumberLabel, flowRunLogAction.cloneNumber";

            List<FlowRunLogAction> lstLogAction = new FlowRunLogActionServiceImpl().findList(hql, -1, -1, flowRunLogId);//.findList(filters2, orders2);

            if (lstLogAction != null && !lstLogAction.isEmpty()) {

                LinkedHashMap<String, String> ordersCmd = new LinkedHashMap<>();
                ordersCmd.put("orderRun", "ASC");
                ordersCmd.put("cloneNumber", "ASC");

                for (FlowRunLogAction logAction : lstLogAction) {
                    try {
                        Map<String, Object> filtersCmd = new HashMap<>();
                        filtersCmd.put("runLogActionId", logAction.getRunLogActionId());

                        //Node node = new NodeServiceImpl().findById(logAction.getNodeId());
                        if (logAction.getNode() != null) {
                            List<FlowRunLogCommand> lstCmd = new FlowRunLogCommandServiceImpl().findList(filtersCmd, ordersCmd);

                            String actionName = ((logAction.getActionOfFlow() != null
                                    && logAction.getActionOfFlow().getAction() != null) ? logAction.getActionOfFlow().getAction().getName() : "")
                                    + "_" + logAction.getCloneNumber();
                            for (FlowRunLogCommand cmd : lstCmd) {
                                cmd.setActionName(actionName);
                            }

                            if (mapLogCommandManual.containsKey(logAction.getNode().getNodeCode())) {
                                mapLogCommandManual.get(logAction.getNode().getNodeCode()).addAll(lstCmd);
                            } else {
                                mapLogCommandManual.put(logAction.getNode().getNodeCode(), lstCmd);
                            }
                        }

                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }

                } // ket thuc vong lap log action
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void prepareShowCmdLog(FlowRunLogCommand logCmd) {
        if (logCmd != null) {
            selectedLogCommand = logCmd;
        } else {
            selectedLogCommand = new FlowRunLogCommand();
        }
    }

    /**
     * Ham luu thong tin account/password cua node mang nguoi dung nhap vao
     */
    public void onSaveAccountNode() {
        FlowRunLog flowRunLog = null;
        try {
            List<NodeRun> listNodeRun = selectedFlowRunAction.getNodeRuns();
            if (oneAccount) {
                if (account == null || account.trim().isEmpty()) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("prompt.account")));
                    return;
                }
                if (password == null || password.trim().isEmpty()) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                            MessageUtil.getResourceBundleMessage("prompt.pass")));
                    return;
                }
                //Luu du lieu acount pass vao db bang node run

                if (listNodeRun != null) {
                    for (NodeRun nodeRun : listNodeRun) {
                        nodeRun.setAccount(PassProtector.encrypt(account == null ? "" : account, "ipchange"));
                        nodeRun.setPassword(PassProtector.encrypt(password == null ? "" : password, "ipchange"));
                    }
                }

            } else {
                if (mapAccount != null && !mapAccount.isEmpty()) {
                    for (AccountObj accountObj : mapAccount.values()) {
                        if (accountObj.getAccount() == null || accountObj.getAccount().trim().isEmpty()) {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                    MessageUtil.getResourceBundleMessage("prompt.account")));
                            return;
                        }
                        if (accountObj.getPassword() == null || accountObj.getPassword().trim().isEmpty()) {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                    MessageUtil.getResourceBundleMessage("prompt.pass")));
                            return;
                        }
                        accountObj.setAccount(PassProtector.encrypt(accountObj.getAccount() == null ? "" : accountObj.getAccount(), "ipchange"));
                        accountObj.setPassword(PassProtector.encrypt(accountObj.getPassword() == null ? "" : accountObj.getPassword(), "ipchange"));
                    }
                    if (listNodeRun != null) {
                        for (NodeRun nodeRun : listNodeRun) {
                            if (mapAccount.containsKey(nodeRun.getNode().getNodeCode())) {
                                nodeRun.setAccount(mapAccount.get(nodeRun.getNode().getNodeCode()).getAccount());
                                nodeRun.setPassword(mapAccount.get(nodeRun.getNode().getNodeCode()).getPassword());
                            }
                        }
                    }
                }
            }
            //Luu account pass vao db
            new NodeRunServiceImpl().saveOrUpdate(listNodeRun);

//            String serverIp = MessageUtil.getResourceBundleConfig("process_socket_ip");
//            int serverPort = Integer.parseInt(MessageUtil.getResourceBundleConfig("process_socket_port"));
            logAction = LogUtils.addContent("", "Running DT auto");
            logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "FlowRunName: " + selectedFlowRunAction.getFlowRunName());
            logAction = LogUtils.addContent(logAction, "Run type: " + runType);
            logAction = LogUtils.addContent(logAction, "Error mode: " + modeRun);
            logAction = LogUtils.addContent(logAction, "Running type: " + runningType);
            MessageObject mesObj;
            String accountLocal = PassProtector.encrypt(account == null ? "" : account, "ipchange");
            String passLocal = PassProtector.encrypt(password == null ? "" : password, "ipchange");
            if (actionOfFlowId != null && actionOfFlowId > 0) {
                logAction = LogUtils.addContent("", "Running DT manual");
                logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
                logAction = LogUtils.addContent(logAction, "FlowRunName: " + selectedFlowRunAction.getFlowRunName());
                logAction = LogUtils.addContent(logAction, "actionOfFlowId: " + actionOfFlowId);
                logAction = LogUtils.addContent(logAction, "Run type: " + runType);
                logAction = LogUtils.addContent(logAction, "Error mode: " + modeRun);
                logAction = LogUtils.addContent(logAction, "Running type: " + runningType);
                FlowRunAction flowRunAction = (new FlowRunActionServiceImpl()).findById(selectedFlowRunAction.getFlowRunId());
                if (flowRunAction.getStatus().equals(Config.RUNNING_FLAG)) {
                    MessageUtil.setErrorMessageFromRes("message.error.running.auto");
                    logAction = LogUtils.addContent(logAction, "Result: " + "DT is running auto, can not run manual");
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);
                    return;
                }

                Map<String, Object> filters = new HashMap<>();
                filters.put("actionOfFlowId", actionOfFlowId);

                List<FlowRunLog> flowRunLogs = flowRunLogService.findList(filters);

                if (flowRunLogs != null && !flowRunLogs.isEmpty()) {
                    for (FlowRunLog flowRunLog1 : flowRunLogs) {
                        if (flowRunLog1.getFinishTime() == null) {
                            MessageUtil.setErrorMessageFromRes("message.error.running.manual");
                            logAction = LogUtils.addContent(logAction, "Result: " + "Action is running, can not start run manual");
                            LogUtils.writelog(new Date(), className, new Object() {
                            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);
                            return;
                        }
                    }
                }

                ActionOfFlow actionOfFlow = (new ActionOfFlowServiceImpl()).findById(actionOfFlowId);

                flowRunLog = new FlowRunLog();
                flowRunLog.setAccount(accountLocal);
                flowRunLog.setActionOfFlowId(actionOfFlowId);
                if (modeRun == 1) {
                    //truong hop chay 1 action
                    flowRunLog.setActionOfFlowIdEnd(actionOfFlowId);
                }
                flowRunLog.setCreateTime(new Date());
                flowRunLog.setFlowRunId(selectedFlowRunAction.getFlowRunId());
                flowRunLog.setUsername(SessionWrapper.getCurrentUsername());
                flowRunLog.setModeRun((long) modeRun);
                flowRunLog.setRunningType((long) runningType);

                flowRunLogService.save(flowRunLog);

                mesObj = new MessageObject(selectedFlowRunAction.getFlowRunId(),
                        SessionWrapper.getCurrentUsername(), accountLocal, passLocal,
                        flowRunLog.getFlowRunLogId(), selectedFlowRunAction.getFlowRunName(), actionOfFlow.getAction().getName());
                mesObj.setActionOfFlowIdStart(actionOfFlowId);
                mesObj.setRunningType(runningType);
                if (modeRun == 1) {
                    //truong hop chay 1 action
                    mesObj.setActionOfFlowIdEnd(actionOfFlowId);
                }
                if (actionOfFlow.getIsRollback() > 0) {
                    //Truong hop rollback
                    mesObj.setRunType(2);
                } else {
                    mesObj.setRunType(1);
                }
                mesObj.setMapAccount(mapAccount);
            } else {
                mesObj = new MessageObject(selectedFlowRunAction.getFlowRunId(),
                        SessionWrapper.getCurrentUsername(), accountLocal, passLocal, null, selectedFlowRunAction.getFlowRunName(), "");
                mesObj.setRunType(runType);
                mesObj.setErrorMode(modeRun);
                mesObj.setRunningType(runningType);
                mesObj.setMapAccount(mapAccount);
            }

            String encrytedMess = new String(Base64.encodeBase64((new Gson()).toJson(mesObj).getBytes("UTF-8")), "UTF-8");

            //20171018_hienhv4_clone dau viec_start
            CatCountryBO country = selectedFlowRunAction.getCountryCode();
            startExecute(encrytedMess, country == null ? Constants.VNM : country.getCountryCode());
            //20171018_hienhv4_clone dau viec_end

            if (actionOfFlowId == null || actionOfFlowId <= 0) {
                logAction = LogUtils.addContent(logAction, "Result: Running auto success");
                RequestContext.getCurrentInstance().execute("PF('scheduleUpdateTopo').start();");
                MessageUtil.setInfoMessageFromRes("message.execute.success");
                activeIndex = 0;
                RequestContext.getCurrentInstance().execute("PF('dlgAcountInfo').hide()");
            } else {
                logAction = LogUtils.addContent(logAction, "Result: Running manual success");
                MessageUtil.setInfoMessageFromRes("message.execute.success.manual");
                RequestContext.getCurrentInstance().execute("PF('dlgAcountInfo').hide()");

                showLog(actionOfFlowId);

                RequestContext.getCurrentInstance().execute("PF('dlgShowLogAction').show()");
                activeIndex = 1;
            }
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);


        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);

            if (flowRunLog != null) {
                try {
                    flowRunLogService.delete(flowRunLog);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }

            if (ex.getMessage() != null) {
                switch (ex.getMessage()) {
                    case "NOK_MAX_SESSION":
                        MessageUtil.setErrorMessageFromRes("message.error.max.session");
                        logAction = LogUtils.addContent(logAction, "Result: Running fail");
                        logAction = LogUtils.addContent(logAction, "Exception: Over max session telnet please try again");
                        LogUtils.writelog(new Date(), className, new Object() {
                        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);
                        break;
                }
            }

            if (actionOfFlowId == null || actionOfFlowId <= 0) {
                MessageUtil.setErrorMessageFromRes("message.execute.fail");
            } else {
                MessageUtil.setErrorMessageFromRes("message.execute.fail.manual");
            }
            logAction = LogUtils.addContent(logAction, "Result: Running fail");
            logAction = LogUtils.addContent(logAction, "Exception: " + ex.getMessage());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);
        }
    }

    public void ClickContinueActionManual(Long result) {
        try {
            logAction = LogUtils.addContent("", "Click Continue Action Manual");
            logAction = LogUtils.addContent(logAction, "FlowRunId: " + selectedFlowRunAction.getFlowRunId());
            logAction = LogUtils.addContent(logAction, "FlowRunName: " + selectedFlowRunAction.getFlowRunName());
            logAction = LogUtils.addContent(logAction, "Run type: " + runType);
            logAction = LogUtils.addContent(logAction, "Error mode: " + modeRun);
            logAction = LogUtils.addContent(logAction, "Running type: " + runningType);
            List<NodeRun> listNodeRun = selectedFlowRunAction.getNodeRuns();
            mapAccount = new HashMap<>();
            if (listNodeRun != null) {
                for (NodeRun nodeRun : listNodeRun) {
                    mapAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj(nodeRun.getAccount(), nodeRun.getPassword()));
                }
            }
//            String serverIp = MessageUtil.getResourceBundleConfig("process_socket_ip");
//            int serverPort = Integer.parseInt(MessageUtil.getResourceBundleConfig("process_socket_port"));

            MessageObject mesObj;
            String accountLocal = PassProtector.encrypt(account == null ? "" : account, "ipchange");
            String passLocal = PassProtector.encrypt(password == null ? "" : password, "ipchange");
            if (actionOfFlowId != null && actionOfFlowId > 0) {
                //Cap nhat ket qua cho action manual hien tai
                HashMap<String, Object> filters = new HashMap<String, Object>();
                LinkedHashMap<String, String> orders = new LinkedHashMap<>();
                filters.put("flowRunLogId", selectedFlowRunAction.getFlowRunId());
                filters.put("actionOfFlowId", actionOfFlowId);
                List<FlowRunLogAction> lstLogAction = new FlowRunLogActionServiceImpl().findList(filters, orders);
                for (FlowRunLogAction logAction : lstLogAction) {
                    logAction.setFinishTime(new Date());
                    logAction.setResult(result);
                }
                new FlowRunLogActionServiceImpl().saveOrUpdate(lstLogAction);

                mesObj = new MessageObject(selectedFlowRunAction.getFlowRunId(),
                        SessionWrapper.getCurrentUsername(), accountLocal, passLocal, null, selectedFlowRunAction.getFlowRunName(), "");
                mesObj.setRunType(runType);
                mesObj.setErrorMode(modeRun);
                mesObj.setRunningType(runningType);
                mesObj.setMapAccount(mapAccount);
                mesObj.setActionOfFlowIdStart(actionOfFlowId);

                String encrytedMess = new String(Base64.encodeBase64((new Gson()).toJson(mesObj).getBytes("UTF-8")), "UTF-8");

                CatCountryBO country = selectedFlowRunAction.getCountryCode();
                startExecute(encrytedMess, country == null ? Constants.VNM : country.getCountryCode());
            }

            RequestContext.getCurrentInstance().execute("PF('scheduleUpdateTopo').start();");
            MessageUtil.setInfoMessageFromRes("message.execute.success");
            activeIndex = 0;
            RequestContext.getCurrentInstance().execute("PF('dlgShowLogAction').hide()");
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            if (ex.getMessage() != null) {
                switch (ex.getMessage()) {
                    case "NOK_MAX_SESSION":
                        MessageUtil.setErrorMessageFromRes("message.error.max.session");
                        logAction = LogUtils.addContent(logAction, "Result: Running fail");
                        logAction = LogUtils.addContent(logAction, "Exception: Over max session telnet please try again");
                        LogUtils.writelog(new Date(), className, new Object() {
                        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);
                        break;
                }
            }

            if (actionOfFlowId == null || actionOfFlowId <= 0) {
                MessageUtil.setErrorMessageFromRes("message.execute.fail");
            } else {
                MessageUtil.setErrorMessageFromRes("message.execute.fail.manual");
            }
            logAction = LogUtils.addContent(logAction, "Result: Running fail");
            logAction = LogUtils.addContent(logAction, "Exception: " + ex.getMessage());
            LogUtils.writelog(new Date(), className, new Object() {
            }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.IMPACT.name(), logAction);
        }
    }

    /*public static void sendMsg2ThreadExecute(String encrytedMess) throws IOException, Exception, MessageException {
        String[] serverIps = MessageUtil.getResourceBundleConfig("process_socket_ip").split("[,;]", -1);
        String[] ports = MessageUtil.getResourceBundleConfig("process_socket_port").split("[,;]", -1);
        for (int i = 0; i < serverIps.length; i++) {
            int serverPort = Integer.parseInt(ports[i]);
            String serverIp = serverIps[i];
            SocketClient client = new SocketClient(serverIp, serverPort);
            client.sendMsg(encrytedMess);

            String socketResult = client.receiveResult();
            if (socketResult != null && socketResult.contains("NOK")) {
                if (i == serverIps.length - 1) {
                    throw new MessageException(socketResult);
                }
            } else {
                return;
            }
        }
    }
*/
    //20171018_hienhv4_clone dau viec_start
    public static void startExecute(String encrytedMess, String countryCode) throws Exception {
        String usingDbConfig = MessageUtil.getResourceBundleConfig("process_using_db_config");
        if (usingDbConfig != null && "true".equalsIgnoreCase(usingDbConfig.trim())) {
            sendMsg2ThreadExecute(encrytedMess, countryCode);
        } else {
            sendMsg2ThreadExecute(encrytedMess);
        }
    }

    private static void sendMsg2ThreadExecute(String encrytedMess, String countryCode) throws IOException, Exception, MessageException {
        Map<String, Object> filters = new HashMap<>();
        filters.put("countryCode.countryCode-" + MapProcessCountryServiceImpl.EXAC, countryCode);
        filters.put("status", 1l);

        List<MapProcessCountry> maps = new MapProcessCountryServiceImpl().findList(filters);

        if (maps != null && !maps.isEmpty()) {
            //Sap xep lai maps theo thu tu random
            Collections.shuffle(maps);

            int i = 0;
            for (MapProcessCountry process : maps) {
                int serverPort = process.getProcessPort();
                String serverIp = process.getProcessIp();

                SocketClient client = new SocketClient(serverIp, serverPort);
                client.sendMsg(encrytedMess);

                String socketResult = client.receiveResult();
                if (socketResult != null && socketResult.contains("NOK")) {
                    if (i == maps.size() - 1) {
                        throw new MessageException(socketResult);
                    }
                } else {
                    return;
                }
                i++;
            }
        }
    }

    private static void sendMsg2ThreadExecute(String encrytedMess) throws IOException, Exception, MessageException {
        String[] serverIps = MessageUtil.getResourceBundleConfig("process_socket_ip").split("[,;]", -1);
        String[] ports = MessageUtil.getResourceBundleConfig("process_socket_port").split("[,;]", -1);
        for (int i = 0; i < serverIps.length; i++) {
            int serverPort = Integer.parseInt(ports[i]);
            String serverIp = serverIps[i];
            SocketClient client = new SocketClient(serverIp, serverPort);
            client.sendMsg(encrytedMess);

            String socketResult = client.receiveResult();
            if (socketResult != null && socketResult.contains("NOK")) {
                if (i == serverIps.length - 1) {
                    throw new MessageException(socketResult);
                }
            } else {
                return;
            }
        }
    }

    //20171018_hienhv4_clone dau viec_end
    public void prepareStartManual() {
        modeRun = 2;
        runningType = Config.RUNNING_TYPE_DEPENDENT.intValue();
        if (actionOfFlowId < 0) {
            MessageUtil.setErrorMessageFromRes("message.cannot.run.start");
            return;
        }
        Boolean canExecute = GNOCService.isCanExecute(selectedFlowRunAction.getCrNumber());
        if (canExecute == null) {
            MessageUtil.setErrorMessageFromRes("error.user.cannot.execute");
            return;
        } else {
            if (!canExecute
                    //huynx6 added Nov 15, 2016
                    && (!selectedFlowRunAction.getCrNumber().equals(Config.CR_DEFAULT))
                    && (!selectedFlowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER))) {
                MessageUtil.setErrorMessageFromRes("error.cr.cannot.execute");
                return;
            }
        }

        account = null;
        password = null;
        oneAccount = false;
        List<NodeRun> listNodeRun = selectedFlowRunAction.getNodeRuns();
        mapAccount = new HashMap<>();
        if (listNodeRun != null) {
            for (NodeRun nodeRun : listNodeRun) {
                mapAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj());
            }
        }
//        for (ActionOfFlow actionOfFlow : selectedFlowRunAction.getFlowTemplates().getActionOfFlows()) {
//            if (actionOfFlow.getStepNum().equals(actionOfFlowId)) {
//                List<NodeRun> listNodeRun = selectedFlowRunAction.getNodeRuns();
//                List<ActionDetail> lstAction = actionOfFlow.getAction().getActionDetails();
//                if (listNodeRun != null && lstAction != null) {
//
//                    for (ActionDetail action : lstAction) {
//                        for (NodeRun nodeRun : listNodeRun) {
//                            if (nodeRun.getNode().getVendor().getVendorId().equals(action.getVendor().getVendorId())
//                                    && nodeRun.getNode().getVersion().getVersionId().equals(action.getVersion().getVersionId())
//                                    && nodeRun.getNode().getNodeType().getTypeId().equals(action.getNodeType().getTypeId())) {
//                                mapAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj());
//                            }
//                        }
//                    }
//                }
//            }
//        }
        RequestContext.getCurrentInstance().execute("PF('dlgAcountInfo').show()");
    }

    public StreamedContent onExportLog() {
        try {
            logAction = LogUtils.addContent("", "Export Log");


            if (selectedFlowRunAction.getLogFilePath() != null
                    && !selectedFlowRunAction.getLogFilePath().trim().isEmpty()) {
                File fileExport = new File(selectedFlowRunAction.getLogFilePath());

                if (fileExport.exists()) {
                    logAction = LogUtils.addContent(logAction, "Result: Success");
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
                    return new DefaultStreamedContent(new FileInputStream(fileExport), ".zip", fileExport.getName());
                } else {
                    if (selectedFlowRunAction.getLogFileContent() != null) {
                        logAction = LogUtils.addContent(logAction, "Result: Success");
                        LogUtils.writelog(new Date(), className, new Object() {
                        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
                        return new DefaultStreamedContent(new ByteArrayInputStream(selectedFlowRunAction.getLogFileContent()), ".zip", fileExport.getName());
                    }

                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                            MessageUtil.getResourceBundleMessage("button.downoad.log")));
                    logAction = LogUtils.addContent(logAction, "Result: Fail");
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logAction = LogUtils.addContent(logAction, "Result: Fail");
            logAction = LogUtils.addContent(logAction, "Exception: " + e.getMessage());
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("button.downoad.log")));
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
        return new DefaultStreamedContent(new ByteArrayInputStream(new byte[]{}));
    }

    public StreamedContent onExportManualLog(FlowRunLog flowRunLog) {
        try {
            logAction = LogUtils.addContent("", "Export manual Log");
            if (flowRunLog.getLogFilePath() != null
                    && !flowRunLog.getLogFilePath().trim().isEmpty()) {
                File fileExport = new File(flowRunLog.getLogFilePath());

                if (fileExport.exists()) {
                    logAction = LogUtils.addContent(logAction, "Result: Success");
                    LogUtils.writelog(new Date(), className, new Object() {
                    }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
                    return new DefaultStreamedContent(new FileInputStream(fileExport), ".zip", fileExport.getName());
                } else {
                    if (flowRunLog.getLogFileContent() != null) {
                        logAction = LogUtils.addContent(logAction, "Result: Success");
                        LogUtils.writelog(new Date(), className, new Object() {
                        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
                        return new DefaultStreamedContent(new ByteArrayInputStream(flowRunLog.getLogFileContent()), ".zip", fileExport.getName());
                    }
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                            MessageUtil.getResourceBundleMessage("button.downoad.log")));
                    logAction = LogUtils.addContent(logAction, "Result: Fail");
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logAction = LogUtils.addContent(logAction, "Result: Fail");
            logAction = LogUtils.addContent(logAction, "Exception: " + e.getMessage());
            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                    MessageUtil.getResourceBundleMessage("button.downoad.log")));
        }
        LogUtils.writelog(new Date(), className, new Object() {
        }.getClass().getEnclosingMethod().getName(), LogUtils.ActionType.EXPORT.name(), logAction);
        return new DefaultStreamedContent(new ByteArrayInputStream(new byte[]{}));
    }

    public void onExecuteMultiMop(List<FlowRunAction> selectedFlowRunActions) {
        List<Long> flowRunActionIds = new ArrayList<>();
        try {
            if (selectedFlowRunActions.size() > 20) {
                throw new MessageException("MAX_OF_MOP_EXECUTE");
            }
            if (selectedFlowRunActions.isEmpty()) {
                return;
            }
            for (FlowRunAction selectedFlowRunActionLocal : selectedFlowRunActions) {
                //1. CR chua thuc hien
                if (selectedFlowRunActionLocal.getStatus().equals(Config.WAITTING_FLAG)
                        || selectedFlowRunActionLocal.getStatus().equals(Config.STOP_FLAG)
                        || selectedFlowRunActionLocal.getStatus().equals(Config.LOGIN_FAIL_FLAG)
                        || selectedFlowRunActionLocal.getStatus().equals(Config.PAUSE_FLAG)) {

                    if (new FlowTemplatesServiceImpl().findById(selectedFlowRunActionLocal.getFlowTemplates().getFlowTemplatesId()).getStatus() != 9) {
                        throw new MessageException(selectedFlowRunActionLocal.getFlowRunName() + ": " + MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    }
                    if (new Date().after(selectedFlowRunActionLocal.getTimeRun())) {
                        if (selectedFlowRunActionLocal.getCrNumber() != null) {
                            Boolean canExecute = GNOCService.isCanExecute(selectedFlowRunActionLocal.getCrNumber());
                            if (canExecute == null) {
                                throw new MessageException(selectedFlowRunActionLocal.getFlowRunName() + ": " + MessageUtil.getResourceBundleMessage("error.user.cannot.execute"));
                            } else {
                                if (canExecute
                                        || (Config.isTestMerge && selectedFlowRunActionLocal.getCrNumber().equals(Config.CR_DEFAULT))
                                        || (Config.isTestMerge && selectedFlowRunActionLocal.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER))) {
                                    flowRunActionIds.add(selectedFlowRunActionLocal.getFlowRunId());
                                } else {
                                    throw new MessageException(selectedFlowRunActionLocal.getFlowRunName() + ": " + MessageUtil.getResourceBundleMessage("error.cr.cannot.execute"));
                                }
                            }
                        } else {
                            throw new MessageException(selectedFlowRunActionLocal.getFlowRunName() + ": " + MessageUtil.getResourceBundleMessage("label.err.cr.notfound"));
                        }
                    } else {
                        throw new MessageException(selectedFlowRunActionLocal.getFlowRunName() + ": " + MessageUtil.getResourceBundleMessage("error.dt.cannot.execute.this.time"));
                    }
                    //2. CR da thuc hien
                } else {
                    throw new MessageException(selectedFlowRunActionLocal.getFlowRunName() + ": " + MessageUtil.getResourceBundleMessage("label.err.flow.not.allowrun"));
                }
            }
            if (flowRunActionIds.size() == selectedFlowRunActions.size()) {
                String[] ports = MessageUtil.getResourceBundleConfig("process_socket_port").split("[,;]", -1);
                Map<String, Object> params = new HashMap<>();
                params.put("status", 7);
                params.put("error_mode", modeRun);
                params.put("running_type", runningType);
                params.put("execute_by", SessionUtil.getCurrentUsername());
                Session session = null;
                Transaction transaction = null;
                try {
                    Object[] objs = flowRunActionService.openTransaction();
                    session = (Session) objs[0];
                    transaction = (Transaction) objs[1];
                    int result = 0;
                    for (int i = 0; i < flowRunActionIds.size(); i++) {
                        Long flowRunId = flowRunActionIds.get(i);
                        params.put("port_run", Integer.parseInt(ports[i % ports.length]));
                        params.put("flowRunId", flowRunId);
                        result += flowRunActionService
                                .execteNativeBulk2("update Flow_Run_Action set EXECUTE_BY = :execute_by, TIME_RUN = sysdate, status = :status, ERROR_MODE=:error_mode, RUNNING_TYPE=:running_type,PORT_RUN=:port_run "
                                        + "where flow_run_id = :flowRunId", session, transaction, false, params);
                    }
                    transaction.commit();
                    if (result == flowRunActionIds.size()) {
                        MessageUtil.setInfoMessageFromRes("info.sucess.queue.mop");
                    }
                } catch (Exception e) {
                    if (transaction != null && transaction.getStatus() != TransactionStatus.ROLLED_BACK
                            && transaction.getStatus() != TransactionStatus.COMMITTED) {
                        transaction.rollback();
                    }
                    logger.error(e.getMessage(), e);
                } finally {
                    if (session != null && session.isOpen()) {
                        session.close();
                    }
                }
                //Luu thong tin account node mang vao bang node run phuc vu chay nhieu mop
                List<NodeRun> listNodeRun = new ArrayList<>();
                for (FlowRunAction selectedFlowRunActionLocal : selectedFlowRunActions) {
                    listNodeRun.addAll(selectedFlowRunActionLocal.getNodeRuns());
                }
                if (oneAccount) {
                    if (account == null || account.trim().isEmpty()) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                MessageUtil.getResourceBundleMessage("prompt.account")));
                    }
                    if (password == null || password.trim().isEmpty()) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                MessageUtil.getResourceBundleMessage("prompt.pass")));
                    }
                    //Luu du lieu acount pass vao db bang node run

                    if (!listNodeRun.isEmpty()) {
                        for (NodeRun nodeRun : listNodeRun) {
                            nodeRun.setAccount(PassProtector.encrypt(account == null ? "" : account, "ipchange"));
                            nodeRun.setPassword(PassProtector.encrypt(password == null ? "" : password, "ipchange"));
                        }
                    }

                } else {
                    if (mapAccount != null && !mapAccount.isEmpty()) {
                        for (AccountObj accountObj : mapAccount.values()) {
                            if (accountObj.getAccount() == null || accountObj.getAccount().trim().isEmpty()) {
                                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                        MessageUtil.getResourceBundleMessage("prompt.account")));
                                break;
                            }
                            if (accountObj.getPassword() == null || accountObj.getPassword().trim().isEmpty()) {
                                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.required"),
                                        MessageUtil.getResourceBundleMessage("prompt.pass")));
                                break;
                            }
                        }
                        if (!listNodeRun.isEmpty()) {
                            for (NodeRun nodeRun : listNodeRun) {
                                if (mapAccount.containsKey(nodeRun.getNode().getNodeCode())) {
                                    nodeRun.setAccount(PassProtector.encrypt(mapAccount.get(nodeRun.getNode().getNodeCode()).getAccount() == null ? "" : mapAccount.get(nodeRun.getNode().getNodeCode()).getAccount(), "ipchange"));
                                    nodeRun.setPassword(PassProtector.encrypt(mapAccount.get(nodeRun.getNode().getNodeCode()).getPassword() == null ? "" : mapAccount.get(nodeRun.getNode().getNodeCode()).getPassword(), "ipchange"));
                                }
                            }
                        }
                    }
                }
                if (!listNodeRun.isEmpty()) {
                    //Luu account pass vao db
                    new NodeRunServiceImpl().saveOrUpdate(listNodeRun);
                }

            } else {

            }
        } catch (MessageException e) {
            logger.error(e.getMessage(), e);
            MessageUtil.setErrorMessage(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        selectedFlowRunActions.clear();
    }

    public void prepareStartExcuteMultiMop(List<FlowRunAction> selectedFlowRunActions) {
//        runningType = Config.RUNNING_TYPE_DEPENDENT.intValue();
//        if (actionOfFlowId < 0) {
//            MessageUtil.setErrorMessageFromRes("message.cannot.run.start");
//            return;
//        }
//        Boolean canExecute = GNOCService.isCanExecute(selectedFlowRunAction.getCrNumber());
//        if (canExecute == null) {
//            MessageUtil.setErrorMessageFromRes("error.user.cannot.execute");
//            return;
//        } else {
//            if (!canExecute
//                    //huynx6 added Nov 15, 2016
//                    && (!selectedFlowRunAction.getCrNumber().equals(Config.CR_DEFAULT))
//                    && (!selectedFlowRunAction.getCrNumber().equals(Config.CR_AUTO_DECLARE_CUSTOMER))) {
//                MessageUtil.setErrorMessageFromRes("error.cr.cannot.execute");
//                return;
//            }
//        }

        account = null;
        password = null;
        oneAccount = false;
        List<NodeRun> listNodeRun = new ArrayList<>();
        for (FlowRunAction selectedFlowRunActionLocal : selectedFlowRunActions) {
            listNodeRun.addAll(selectedFlowRunActionLocal.getNodeRuns());
        }
        mapAccount = new HashMap<>();
        if (!listNodeRun.isEmpty()) {
            for (NodeRun nodeRun : listNodeRun) {
                mapAccount.put(nodeRun.getNode().getNodeCode(), new AccountObj());
            }
        }
    }

    public DefaultDiagramModel getTopoDiagram() {
        return topoDiagram;
    }

    public void setTopoDiagram(DefaultDiagramModel topoDiagram) {
        this.topoDiagram = topoDiagram;
    }

    public ActionOfFlow getSelectedActionFlow() {
        return selectedActionFlow;
    }

    public void setSelectedActionFlow(ActionOfFlow selectedActionFlow) {
        this.selectedActionFlow = selectedActionFlow;
    }

    public FlowRunAction getSelectedFlowRunAction() {
        return selectedFlowRunAction;
    }

    public void setSelectedFlowRunAction(FlowRunAction selectedFlowRunAction) {
        this.selectedFlowRunAction = selectedFlowRunAction;
    }

    public Map<String, LogNodeObj> getMapLogCommand() {
        return mapLogCommand;
    }

    public void setMapLogCommand(Map<String, LogNodeObj> mapLogCommand) {
        this.mapLogCommand = mapLogCommand;
    }

    public FlowRunLogCommand getSelectedLogCommand() {
        return selectedLogCommand;
    }

    public void setSelectedLogCommand(FlowRunLogCommand selectedLogCommand) {
        this.selectedLogCommand = selectedLogCommand;
    }

    public void check() {
        System.out.println("aaaaaaaaaaaaaaaaaaa");
    }

    public FlowRunLog getSelectedFlowRunLog() {
        return selectedFlowRunLog;
    }

    public void setSelectedFlowRunLog(FlowRunLog selectedFlowRunLog) {
        this.selectedFlowRunLog = selectedFlowRunLog;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, List<FlowRunLogCommand>> getMapLogCommandManual() {
        return mapLogCommandManual;
    }

    public void setMapLogCommandManual(Map<String, List<FlowRunLogCommand>> mapLogCommandManual) {
        this.mapLogCommandManual = mapLogCommandManual;
    }

    public LazyDataModel<FlowRunLog> getLstFlowRunLog() {
        return lstFlowRunLog;
    }

    public void setLstFlowRunLog(LazyDataModel<FlowRunLog> lstFlowRunLog) {
        this.lstFlowRunLog = lstFlowRunLog;
    }

    public FlowRunLogServiceImpl getFlowRunLogService() {
        return flowRunLogService;
    }

    public void setFlowRunLogService(FlowRunLogServiceImpl flowRunLogService) {
        this.flowRunLogService = flowRunLogService;
    }

    public Long getActionOfFlowId() {
        return actionOfFlowId;
    }

    public void setActionOfFlowId(Long actionOfFlowId) {
        this.actionOfFlowId = actionOfFlowId;
    }

    public int getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(int activeIndex) {
        this.activeIndex = activeIndex;
    }

    public int getModeRun() {
        return modeRun;
    }

    public void setModeRun(int modeRun) {
        this.modeRun = modeRun;
    }

    public boolean isShowDiagramOnly() {
        return isShowDiagramOnly;
    }

    public void setShowDiagramOnly(boolean isShowDiagramOnly) {
        this.isShowDiagramOnly = isShowDiagramOnly;
    }

    public boolean isOneAccount() {
        return oneAccount;
    }

    public void setOneAccount(boolean oneAccount) {
        this.oneAccount = oneAccount;
    }

    public Map<String, AccountObj> getMapAccount() {
        return mapAccount;
    }

    public void setMapAccount(Map<String, AccountObj> mapAccount) {
        this.mapAccount = mapAccount;
    }

    public int getRunningType() {
        return runningType;
    }

    public void setRunningType(int runningType) {
        this.runningType = runningType;
    }

    public GenericDaoServiceNewV2<FlowRunAction, Long> getFlowRunActionService() {
        return flowRunActionService;
    }

    public void setFlowRunActionService(GenericDaoServiceNewV2<FlowRunAction, Long> flowRunActionService) {
        this.flowRunActionService = flowRunActionService;
    }

    public int getRunType() {
        return runType;
    }

    public List<String> getSubFlowRuns() {
        return subFlowRuns;
    }

    public void setSubFlowRuns(List<String> subFlowRuns) {
        this.subFlowRuns = subFlowRuns;
    }

    public String getSelectSubflowRun() {
        return selectSubflowRun;
    }

    public void setSelectSubflowRun(String selectSubflowRun) {
        this.selectSubflowRun = selectSubflowRun;
    }

    public boolean isIsActionManual() {
        return isActionManual;
    }

    public void setIsActionManual(boolean isActionManual) {
        this.isActionManual = isActionManual;
    }

}
