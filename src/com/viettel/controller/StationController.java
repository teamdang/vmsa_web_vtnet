package com.viettel.controller;

import java.io.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.google.gson.Gson;
import com.viettel.model.*;
import com.viettel.nims.infra.webservice.CheckCellForCRForm;
import com.viettel.nims.infra.webservice.UpdateInfraWS;
import com.viettel.nims.infra.webservice.UpdateInfraWSService;
import com.viettel.persistence.*;
import com.viettel.util.*;
import com.viettel.webservice.object.DataCellUpdateNimsForGnoc;
import com.viettel.webservice.object.FlowTemplateObj;
import com.viettel.webservice.object.ResultDTO;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import static com.viettel.controller.RescueInfomationController.logger;

import com.viettel.exception.AppException;
import com.viettel.lazy.LazyDataModelBaseNew;
import com.viettel.object.GroupAction;
import com.viettel.object.MessageException;
import com.viettel.object.StationResult;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @version 1.0
 * @sin Mar 10, 2017
 */
@SuppressWarnings("serial")
@ManagedBean(name = "stationController")
@ViewScoped
public class StationController implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="Param">

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private LazyDataModel<StationPlan> lazyStationPlan;
    @ManagedProperty("#{stationPlanService}")
    private StationPlanServiceImpl stationPlanService;
    private StationPlan currStationPlan;
    private StationPlan currStationRelationPlan;
    private List<StationPlan> selectStationPlans;
    private List<StationDetail> stationDetails;
    private List<Vendor> vendors;
    private LazyDataModel<StationFlowRunAction> lazyStationDT;
    private Map<String, List<StationResult>> mapStationResult;
    private Long selectProficientType = 0L;
    private String selectProficientTypeStr;
    private List<FileUploadEvent> fileUploadEvents;
    private Long hotspot1SDRightValue = 200L;
    private Long hotspot2SDLeftValue = 200L;
    private Long hotspot2SDRightValue = 250L;
    private Long hotspot3SDLeftValue = 250L;
    private Long normal1SDRightValue = 80L;
    private Long normal2SDLeftValue = 80L;
    private Long normal2SDRightValue = 120L;
    private Long normal3SDLeftValue = 120L;
    private Long normal3SDRightValue = 150L;
    private Long normal4SDLeftValue = 150L;
    private Long normal1HRRightValue = 90L;
    private Long normal1HRValue = 20L;
    private Long normal2HRLeftValue = 90L;
    private Long normal2HRRightValue = 120L;
    private Long normal2HRValue = 30L;
    private Long normal3HRLeftValue = 120L;
    private Long normal3HRRightValue = 150L;
    private Long normal3HRValue = 50L;
    private Long normal4HRLeftValue = 150L;
    private Long normal4HRRightValue = 180L;
    private Long normal4HRValue = 70L;
    private Long normal5HRLeftValue = 180L;
    private Long normal5HRValue = 90L;

    private String hotspot1SDValue = "2 * NTRx";
    private String hotspot2SDValue = "1 + 2 * NTRx";
    private String hotspot3SDValue = "2 + 2 * NTRx";
    private String normal1SDValue = "NTRx";
    private String normal2SDValue = "NTRx + 1";
    private String normal3SDValue = "1.5 * NTRx";
    private String normal4SDValue = "2 * NTRx";
    private String userCreate;
    private String pathOut;
    private String pathTemplate;
    private File fileOutPut;
    private StreamedContent resultImport;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Set&Get">

    public File getFileOutPut() {
        return fileOutPut;
    }

    public void setFileOutPut(File fileOutPut) {
        this.fileOutPut = fileOutPut;
    }

    public StreamedContent getResultImport() {
        return resultImport;
    }

    public void setResultImport(StreamedContent resultImport) {
        this.resultImport = resultImport;
    }

    public String getPathOut() {
        return pathOut;
    }

    public void setPathOut(String pathOut) {
        this.pathOut = pathOut;
    }

    public String getPathTemplate() {
        return pathTemplate;
    }

    public void setPathTemplate(String pathTemplate) {
        this.pathTemplate = pathTemplate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public List<FileUploadEvent> getFileUploadEvents() {
        return fileUploadEvents;
    }

    public void setFileUploadEvents(List<FileUploadEvent> fileUploadEvents) {
        this.fileUploadEvents = fileUploadEvents;
    }

    public StationPlan getCurrStationRelationPlan() {
        return currStationRelationPlan;
    }

    public void setCurrStationRelationPlan(StationPlan currStationRelationPlan) {
        this.currStationRelationPlan = currStationRelationPlan;
    }

    public Long getSelectProficientType() {
        return selectProficientType;
    }

    public void setSelectProficientType(Long selectProficientType) {
        this.selectProficientType = selectProficientType;
    }

    public Map<String, List<StationResult>> getMapStationResult() {
        return mapStationResult;
    }

    public void setMapStationResult(Map<String, List<StationResult>> mapStationResult) {
        this.mapStationResult = mapStationResult;
    }

    public List<StationPlan> getSelectStationPlans() {
        return selectStationPlans;
    }

    public void setSelectStationPlans(List<StationPlan> selectStationPlans) {
        this.selectStationPlans = selectStationPlans;
    }

    public List<StationDetail> getStationDetails() {
        return stationDetails;
    }

    public void setStationDetails(List<StationDetail> stationDetails) {
        this.stationDetails = stationDetails;
    }

    public LazyDataModel<StationPlan> getLazyStationPlan() {
        return lazyStationPlan;
    }

    public void setLazyStationPlan(LazyDataModel<StationPlan> lazyStationPlan) {
        this.lazyStationPlan = lazyStationPlan;
    }

    public StationPlanServiceImpl getStationPlanService() {
        return stationPlanService;
    }

    public void setStationPlanService(StationPlanServiceImpl stationPlanService) {
        this.stationPlanService = stationPlanService;
    }

    public StationPlan getCurrStationPlan() {
        return currStationPlan;
    }

    public void setCurrStationPlan(StationPlan currStationPlan) {
        this.currStationPlan = currStationPlan;
    }

    public List<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public LazyDataModel<StationFlowRunAction> getLazyStationDT() {
        return lazyStationDT;
    }

    public void setLazyStationDT(LazyDataModel<StationFlowRunAction> lazyStationDT) {
        this.lazyStationDT = lazyStationDT;
    }

    public String getSelectProficientTypeStr() {
        if (selectProficientType == null) {
            return "";
        } else if (selectProficientType.equals(0L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type0");
        } else if (selectProficientType.equals(1L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type1");
        } else if (selectProficientType.equals(2L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type2");
        } else if (selectProficientType.equals(3L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type3");
        } else if (selectProficientType.equals(4L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type4");
        } else if (selectProficientType.equals(5L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type5");
        } else if (selectProficientType.equals(6L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type6");
        } else if (selectProficientType.equals(7L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type7");
        } else if (selectProficientType.equals(8L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type8");
        } else if (selectProficientType.equals(9L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type9");
        } else if (selectProficientType.equals(10L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type10");
        }else if (selectProficientType.equals(11L)) {
            return MessageUtil.getResourceBundleMessage("label.station.proficient.type11");
        }
        else {
            return "";
        }
    }

    public void setSelectProficientTypeStr(String selectProficientTypeStr) {
        this.selectProficientTypeStr = selectProficientTypeStr;
    }

    public Long getHotspot1SDRightValue() {
        return hotspot1SDRightValue;
    }

    public void setHotspot1SDRightValue(Long hotspot1SDRightValue) {
        this.hotspot1SDRightValue = hotspot1SDRightValue;
    }

    public Long getHotspot2SDLeftValue() {
        return hotspot2SDLeftValue;
    }

    public void setHotspot2SDLeftValue(Long hotspot2SDLeftValue) {
        this.hotspot2SDLeftValue = hotspot2SDLeftValue;
    }

    public Long getHotspot2SDRightValue() {
        return hotspot2SDRightValue;
    }

    public void setHotspot2SDRightValue(Long hotspot2SDRightValue) {
        this.hotspot2SDRightValue = hotspot2SDRightValue;
    }

    public Long getHotspot3SDLeftValue() {
        return hotspot3SDLeftValue;
    }

    public void setHotspot3SDLeftValue(Long hotspot3SDLeftValue) {
        this.hotspot3SDLeftValue = hotspot3SDLeftValue;
    }

    public Long getNormal1SDRightValue() {
        return normal1SDRightValue;
    }

    public void setNormal1SDRightValue(Long normal1SDRightValue) {
        this.normal1SDRightValue = normal1SDRightValue;
    }

    public Long getNormal2SDLeftValue() {
        return normal2SDLeftValue;
    }

    public void setNormal2SDLeftValue(Long normal2SDLeftValue) {
        this.normal2SDLeftValue = normal2SDLeftValue;
    }

    public Long getNormal2SDRightValue() {
        return normal2SDRightValue;
    }

    public void setNormal2SDRightValue(Long normal2SDRightValue) {
        this.normal2SDRightValue = normal2SDRightValue;
    }

    public Long getNormal3SDLeftValue() {
        return normal3SDLeftValue;
    }

    public void setNormal3SDLeftValue(Long normal3SDLeftValue) {
        this.normal3SDLeftValue = normal3SDLeftValue;
    }

    public Long getNormal3SDRightValue() {
        return normal3SDRightValue;
    }

    public void setNormal3SDRightValue(Long normal3SDRightValue) {
        this.normal3SDRightValue = normal3SDRightValue;
    }

    public Long getNormal4SDLeftValue() {
        return normal4SDLeftValue;
    }

    public void setNormal4SDLeftValue(Long normal4SDLeftValue) {
        this.normal4SDLeftValue = normal4SDLeftValue;
    }

    public Long getNormal1HRRightValue() {
        return normal1HRRightValue;
    }

    public void setNormal1HRRightValue(Long normal1HRRightValue) {
        this.normal1HRRightValue = normal1HRRightValue;
    }

    public Long getNormal1HRValue() {
        return normal1HRValue;
    }

    public void setNormal1HRValue(Long normal1HRValue) {
        this.normal1HRValue = normal1HRValue;
    }

    public Long getNormal2HRLeftValue() {
        return normal2HRLeftValue;
    }

    public void setNormal2HRLeftValue(Long normal2HRLeftValue) {
        this.normal2HRLeftValue = normal2HRLeftValue;
    }

    public Long getNormal2HRRightValue() {
        return normal2HRRightValue;
    }

    public void setNormal2HRRightValue(Long normal2HRRightValue) {
        this.normal2HRRightValue = normal2HRRightValue;
    }

    public Long getNormal2HRValue() {
        return normal2HRValue;
    }

    public void setNormal2HRValue(Long normal2HRValue) {
        this.normal2HRValue = normal2HRValue;
    }

    public Long getNormal3HRLeftValue() {
        return normal3HRLeftValue;
    }

    public void setNormal3HRLeftValue(Long normal3HRLeftValue) {
        this.normal3HRLeftValue = normal3HRLeftValue;
    }

    public Long getNormal3HRRightValue() {
        return normal3HRRightValue;
    }

    public void setNormal3HRRightValue(Long normal3HRRightValue) {
        this.normal3HRRightValue = normal3HRRightValue;
    }

    public Long getNormal3HRValue() {
        return normal3HRValue;
    }

    public void setNormal3HRValue(Long normal3HRValue) {
        this.normal3HRValue = normal3HRValue;
    }

    public Long getNormal4HRLeftValue() {
        return normal4HRLeftValue;
    }

    public void setNormal4HRLeftValue(Long normal4HRLeftValue) {
        this.normal4HRLeftValue = normal4HRLeftValue;
    }

    public Long getNormal4HRRightValue() {
        return normal4HRRightValue;
    }

    public void setNormal4HRRightValue(Long normal4HRRightValue) {
        this.normal4HRRightValue = normal4HRRightValue;
    }

    public Long getNormal4HRValue() {
        return normal4HRValue;
    }

    public void setNormal4HRValue(Long normal4HRValue) {
        this.normal4HRValue = normal4HRValue;
    }

    public Long getNormal5HRLeftValue() {
        return normal5HRLeftValue;
    }

    public void setNormal5HRLeftValue(Long normal5HRLeftValue) {
        this.normal5HRLeftValue = normal5HRLeftValue;
    }

    public Long getNormal5HRValue() {
        return normal5HRValue;
    }

    public void setNormal5HRValue(Long normal5HRValue) {
        this.normal5HRValue = normal5HRValue;
    }

    public String getHotspot1SDValue() {
        return hotspot1SDValue;
    }

    public void setHotspot1SDValue(String hotspot1SDValue) {
        this.hotspot1SDValue = hotspot1SDValue;
    }

    public String getHotspot2SDValue() {
        return hotspot2SDValue;
    }

    public void setHotspot2SDValue(String hotspot2SDValue) {
        this.hotspot2SDValue = hotspot2SDValue;
    }

    public String getHotspot3SDValue() {
        return hotspot3SDValue;
    }

    public void setHotspot3SDValue(String hotspot3SDValue) {
        this.hotspot3SDValue = hotspot3SDValue;
    }

    public String getNormal1SDValue() {
        return normal1SDValue;
    }

    public void setNormal1SDValue(String normal1SDValue) {
        this.normal1SDValue = normal1SDValue;
    }

    public String getNormal2SDValue() {
        return normal2SDValue;
    }

    public void setNormal2SDValue(String normal2SDValue) {
        this.normal2SDValue = normal2SDValue;
    }

    public String getNormal3SDValue() {
        return normal3SDValue;
    }

    public void setNormal3SDValue(String normal3SDValue) {
        this.normal3SDValue = normal3SDValue;
    }

    public String getNormal4SDValue() {
        return normal4SDValue;
    }

    public void setNormal4SDValue(String normal4SDValue) {
        this.normal4SDValue = normal4SDValue;
    }

    //</editor-fold>
    @PostConstruct
    public void onStart() {
        try {
            Map<String, Object> filters = new HashMap<>();
            Map<String, String> orders = new LinkedHashMap<>();
            Map<String, String> orderVendor = new LinkedHashMap<>();
            orders.put("updateTime", "DESC");
            orders.put("crNumber", "ASC");
            orderVendor.put("vendorName", "ASC");
            currStationPlan = new StationPlan();
            filters.put("proficientType", selectProficientType);
            lazyStationPlan = new LazyDataModelBaseNew<>(stationPlanService, filters, orders);
            vendors = new VendorServiceImpl().findList(null, orderVendor);
            mapStationResult = new HashMap<>();
            fileUploadEvents = new ArrayList<>();
            userCreate = SessionUtil.getCurrentUsername();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    /**
     * Import plan
     *
     * @author quytv7
     */
    //<editor-fold defaultstate="collapsed" desc="Plan">
    public void refreshStationPlan() {
        try {

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void handleImportStationPlan(FileUploadEvent event) {
        try {
            fileUploadEvents.add(event);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void removeFileImport(FileUploadEvent event) {
        try {
            fileUploadEvents.remove(event);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void ImportStationPlan() throws Exception {
        try {
            Map<String, Object> filters = new HashMap<>();
            List<StationConfigImport> stationConfigImports;
            List<File> lstFilePutSystem = null;//List file nguoi dung import len de day len he thong
            HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile;
            if (!validateInput(currStationPlan)) {
                MessageUtil.setWarnMessageFromRes("error.import.data.input");
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.input"));
            }
            if (selectProficientType.equals(Config.IS_RULE_INTEGRATE)) {

                // Lay du lieu file import cua tich hop tram
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                filters.put("isRule", Config.IS_RULE_INTEGRATE);
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                mapImportFile = new HashMap<>();
                getMapImportFile(mapImportFile, stationConfigImports);
                boolean checkFileExits = false;
                for (FileUploadEvent event : fileUploadEvents) {
                    if (mapImportFile.containsKey(event.getFile().getFileName())) {
                        checkFileExits = true;
                        break;
                    }
                }
                if (!checkFileExits) {
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                }
                filters.clear();
                filters.put("vendor.vendorId", currStationPlan.getVendor().getVendorId());
                List<StationParamDefault> lstParamDefault = new StationParamDefaultServiceImpl().findList(filters);
                // Lay du lieu file import cua relation
                filters.clear();
                filters.put("isRule", Config.IS_RULE_RELATION);
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);

                for (FileUploadEvent event : fileUploadEvents) {
                    if (!mapImportFile.containsKey(event.getFile().getFileName())) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                    }
                }

                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_RELATION)) {

                filters.clear();
                filters.put("isRule", Config.IS_RULE_RELATION);
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                mapImportFile = new HashMap<>();
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);
/*                for (FileUploadEvent event : fileUploadEvents) {
                    if (!mapImportFile.containsKey(event.getFile().getFileName())) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                    }
                }*/
                boolean checkFileExits;
                for (FileUploadEvent event : fileUploadEvents) {
                    checkFileExits = false;
                    for (String fileName : mapImportFile.keySet()) {
                        if (event.getFile().getFileName().endsWith(".xlsx") && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".xlsx", "").toLowerCase())) {
                            checkFileExits = true;
                            if (!event.getFile().getFileName().toLowerCase().equals(fileName.toLowerCase())) {
                                mapImportFile.put(event.getFile().getFileName(), mapImportFile.get(fileName));
                                mapImportFile.remove(fileName);
                            }
                            break;
                        }
                    }
                    if (!checkFileExits) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                    }
                }

                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_DELETE)) {

                // Lay du lieu file import cua xoa tram
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                filters.put("isRule", Config.IS_RULE_DELETE);
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                mapImportFile = new HashMap<>();
                getMapImportFile(mapImportFile, stationConfigImports);
                boolean checkFileExits = false;
                for (FileUploadEvent event : fileUploadEvents) {
                    if (mapImportFile.containsKey(event.getFile().getFileName())) {
                        checkFileExits = true;
                        break;
                    }
                }
                if (!checkFileExits) {
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                }
                // Lay du lieu file import cua relation
                filters.clear();
                filters.put("isRule", Config.IS_RULE_RELATION);
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);
                for (FileUploadEvent event : fileUploadEvents) {
                    if (!mapImportFile.containsKey(event.getFile().getFileName())) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                    }
                }
                filters.clear();
                filters.put("vendor.vendorId", currStationPlan.getVendor().getVendorId());
                List<StationParamDefault> lstParamDefault = new StationParamDefaultServiceImpl().findList(filters);
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_UPGRADE_DONWGRADE)) {

                // Lay du lieu file import cua xoa tram
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                filters.put("isRule", Config.IS_RULE_UPGRADE_DONWGRADE);
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                mapImportFile = new HashMap<>();
                getMapImportFile(mapImportFile, stationConfigImports);
                boolean checkFileExits = false;
                for (FileUploadEvent event : fileUploadEvents) {
                    if (mapImportFile.containsKey(event.getFile().getFileName())) {
                        checkFileExits = true;
                        break;
                    }
                }
                if (!checkFileExits) {
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                }

                filters.clear();
                filters.put("vendor.vendorId", currStationPlan.getVendor().getVendorId());
                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_CHANGE_FORCE)) {

                filters.clear();
                filters.put("isRule", Config.IS_RULE_CHANGE_FORCE);
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                mapImportFile = new HashMap<>();
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);

                boolean checkFileExits;
                for (FileUploadEvent event : fileUploadEvents) {
                    checkFileExits = false;
                    for (String fileName : mapImportFile.keySet()) {
                        if (event.getFile().getFileName().endsWith(".xlsx") && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".xlsx", "").toLowerCase())) {
                            checkFileExits = true;
                            if (!event.getFile().getFileName().toLowerCase().equals(fileName.toLowerCase())) {
                                mapImportFile.put(event.getFile().getFileName(), mapImportFile.get(fileName));
                                mapImportFile.remove(fileName);
                            }
                            break;
                        }
                    }
                    if (!checkFileExits) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                    }
                }

                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_LOAD_LICENSE)) {

                filters.clear();
                filters.put("isRule", Config.IS_RULE_LOAD_LICENSE);
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                mapImportFile = new HashMap<>();
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);
                boolean checkFileExits = false;
                boolean checkFileExcelExits = false;
                boolean checkFileRarExits = false;
                for (FileUploadEvent event : fileUploadEvents) {
                    checkFileExits = false;
                    for (String fileName : mapImportFile.keySet()) {
                        if (event.getFile().getFileName().endsWith(".xlsx") && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".xlsx", "").toLowerCase())) {
                            checkFileExits = true;
                            checkFileExcelExits = true;
                            if (!event.getFile().getFileName().toLowerCase().equals(fileName.toLowerCase())) {
                                mapImportFile.put(event.getFile().getFileName(), mapImportFile.get(fileName));
                                mapImportFile.remove(fileName);
                            }
                            break;
                        }
                    }

                    for (String fileName : mapImportFile.keySet()) {
                        if ((event.getFile().getFileName().endsWith(".rar") || event.getFile().getFileName().endsWith(".zip")) && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".rar", "").replace(".zip", "").toLowerCase())) {
                            checkFileExits = true;
                            checkFileRarExits = true;
                            //Quytv7 day file rar len vao giai nen
                            String OutPutFile = getFolderSave() + "IntegrateStation" + File.separator + "LoadLicense" + File.separator + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                            //create output directory is not exists
                            File folder = new File(OutPutFile);
                            if (!folder.exists()) {
                                folder.mkdirs();
                            }
                            File file = new File(OutPutFile + File.separator + fileName.replace(".rar", ".zip"));
                            FileOutputStream fileOuputStream = null;
                            try {
                                fileOuputStream = new FileOutputStream(file.getPath());
                                fileOuputStream.write(event.getFile().getContents());
                            } catch (Exception ex) {
                                logger.error(ex.getMessage(), ex);
                            } finally {
                                if (fileOuputStream != null) {
                                    fileOuputStream.close();
                                }
                            }
                            lstFilePutSystem = new ArrayList<>();
                            unZip(lstFilePutSystem, file, OutPutFile);
                            mapImportFile.remove(fileName);
                            break;
                        }
                    }
                    if (!checkFileExits) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                    }
                }
                if (!checkFileExcelExits) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName.incomplete"),
                            MessageUtil.getResourceBundleMessage("label.station.proficient.type7")));
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName.incomplete"),
                            MessageUtil.getResourceBundleMessage("label.data")));
                }
                if (!checkFileRarExits) {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName.incomplete"),
                            MessageUtil.getResourceBundleMessage("label.station.proficient.type7")));
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName.incomplete"),
                            MessageUtil.getResourceBundleMessage("label.station.proficient.type7")));
                }


                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_CHANGE_PARAM)) {

                filters.clear();
                filters.put("isRule", Config.IS_RULE_CHANGE_PARAM);
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                mapImportFile = new HashMap<>();
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);
/*                for (FileUploadEvent event : fileUploadEvents) {
                    if (!mapImportFile.containsKey(event.getFile().getFileName())) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                    }
                }*/
                boolean checkFileExits;
                for (FileUploadEvent event : fileUploadEvents) {
                    checkFileExits = false;
                    for (String fileName : mapImportFile.keySet()) {
                        if (event.getFile().getFileName().endsWith(".xlsx") && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".xlsx", "").toLowerCase())) {
                            checkFileExits = true;
                            if (!event.getFile().getFileName().toLowerCase().equals(fileName.toLowerCase())) {
                                mapImportFile.put(event.getFile().getFileName(), mapImportFile.get(fileName));
                                mapImportFile.remove(fileName);
                            }
                            break;
                        }
                    }
                    if (!checkFileExits) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                    }
                }

                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_ACTIVE_STANDARD)) {

                filters.clear();
                filters.put("isRule", Config.IS_RULE_ACTIVE_STANDARD);
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                mapImportFile = new HashMap<>();
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);
                boolean checkFileExits;
                for (FileUploadEvent event : fileUploadEvents) {
                    checkFileExits = false;
                    for (String fileName : mapImportFile.keySet()) {
                        if (event.getFile().getFileName().endsWith(".xlsx") && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".xlsx", "").toLowerCase())) {
                            checkFileExits = true;
                            if (!event.getFile().getFileName().toLowerCase().equals(fileName.toLowerCase())) {
                                mapImportFile.put(event.getFile().getFileName(), mapImportFile.get(fileName));
                                mapImportFile.remove(fileName);
                            }
                            break;
                        }
                    }
                    if (!checkFileExits) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                    }
                }

                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            } else if (selectProficientType.equals(Config.IS_RULE_ACTIVE_TEST)) {

                filters.clear();
                filters.put("isRule", Config.IS_RULE_ACTIVE_TEST);
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                mapImportFile = new HashMap<>();
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);
                boolean checkFileExits;
                for (FileUploadEvent event : fileUploadEvents) {
                    checkFileExits = false;
                    for (String fileName : mapImportFile.keySet()) {
                        if (event.getFile().getFileName().endsWith(".xlsx") && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".xlsx", "").toLowerCase())) {
                            checkFileExits = true;
                            if (!event.getFile().getFileName().toLowerCase().equals(fileName.toLowerCase())) {
                                mapImportFile.put(event.getFile().getFileName(), mapImportFile.get(fileName));
                                mapImportFile.remove(fileName);
                            }
                            break;
                        }
                    }
                    if (!checkFileExits) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                    }
                }

                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            }
            else if (selectProficientType.equals(Config.IS_RULE_CHANGE_VLAN)) {

                filters.clear();
                filters.put("isRule", Config.IS_RULE_CHANGE_VLAN);
                filters.put("vendorName", currStationPlan.getVendor().getVendorName());
                filters.put("networkType", currStationPlan.getNetworkType());
                mapImportFile = new HashMap<>();
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                getMapImportFile(mapImportFile, stationConfigImports);
                boolean checkFileExits;
                for (FileUploadEvent event : fileUploadEvents) {
                    checkFileExits = false;
                    for (String fileName : mapImportFile.keySet()) {
                        if (event.getFile().getFileName().endsWith(".xlsx") && event.getFile().getFileName().toLowerCase().contains(fileName.replace(".xlsx", "").toLowerCase())) {
                            checkFileExits = true;
                            if (!event.getFile().getFileName().toLowerCase().equals(fileName.toLowerCase())) {
                                mapImportFile.put(event.getFile().getFileName(), mapImportFile.get(fileName));
                                mapImportFile.remove(fileName);
                            }
                            break;
                        }
                    }
                    if (!checkFileExits) {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.fileName"),
                                event.getFile().getFileName()));
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.import.data.station.fileNotExitsIntegrate"));
                    }
                }

                List<StationParamDefault> lstParamDefault = new ArrayList<>();
                ImportStationPlanDetail(mapImportFile, lstParamDefault, lstFilePutSystem);
            }
            RequestContext.getCurrentInstance().execute("PF('addStationDlg').hide()");
        } catch (Exception e) {
            if (e instanceof MessageException) {
                MessageUtil.setErrorMessage(e.getMessage());
            } else {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    public void getMapImportFile(HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile, List<StationConfigImport> stationConfigImports) {
        HashMap<String, List<StationConfigImport>> mapImportSheet;
        List<StationConfigImport> lstStationConfigImports;
        for (StationConfigImport stationConfigImport : stationConfigImports) {
            if (mapImportFile.containsKey(stationConfigImport.getFileName())) {
                mapImportSheet = mapImportFile.get(stationConfigImport.getFileName());
                if (mapImportSheet.containsKey(stationConfigImport.getSheetName())) {
                    mapImportSheet.get(stationConfigImport.getSheetName()).add(stationConfigImport);
                } else {
                    lstStationConfigImports = new ArrayList<>();
                    lstStationConfigImports.add(stationConfigImport);
                    mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                }
                mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
            } else {
                mapImportSheet = new HashMap<>();
                lstStationConfigImports = new ArrayList<>();
                lstStationConfigImports.add(stationConfigImport);
                mapImportSheet.put(stationConfigImport.getSheetName(), lstStationConfigImports);
                mapImportFile.put(stationConfigImport.getFileName(), mapImportSheet);
            }
        }
    }

    public void ImportStationPlanDetail(HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFileInput, List<StationParamDefault> lstParamDefault, List<File> lstFilePutSystem) throws Exception {

        try {

            //Lay cau hinh import
            final HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile = new HashMap<>(mapImportFileInput);
            for (FileUploadEvent event : fileUploadEvents) {
                InputStream inputstream;
                HashMap<String, List<?>> mapCellObjectImports = new HashMap<String, List<?>>();
                if (mapImportFile.containsKey(event.getFile().getFileName())) {
//                    fileUploadEvents.remove(event);
                    Workbook workbook = null;
                    try {
                        inputstream = event.getFile().getInputstream();

                        if (inputstream == null) {
                            throw new NullPointerException("inputstream is null");
                        }
                        //Get the workbook instance for XLS/xlsx file 
                        try {
//                            ZipSecureFile.setMinInflateRatio(0.0d);
                            workbook = WorkbookFactory.create(inputstream);
                            if (workbook == null) {
                                throw new NullPointerException("workbook is null");
                            }
                        } catch (InvalidFormatException e2) {
                            logger.debug(e2.getMessage(), e2);
                            throw new AppException(MessageUtil.getResourceBundleMessage("label.file.config.type"));
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        MessageUtil.setErrorMessageFromRes("meassage.import.fail2");
                        return;
                    }

                    try {
                        for (String key : mapImportFile.get(event.getFile().getFileName()).keySet()) {
                            List<?> objectImports = new LinkedList<>();
                            Importer<Serializable> importer = new Importer<Serializable>() {

                                @Override
                                protected Map<Integer, String> getIndexMapFieldClass() {
                                    return null;
                                }

                                @Override
                                protected String getDateFormat() {
                                    return null;
                                }
                            };

                            importer.setRowHeaderNumber(1);
                            List<Serializable> objects = importer.getDatas(workbook, key, "2-");
                            if (objects != null) {
                                if (mapImportFile.get(event.getFile().getFileName()).get(key) != null &&
                                        mapImportFile.get(event.getFile().getFileName()).get(key).get(0).getIsOblige() != null &&
                                        mapImportFile.get(event.getFile().getFileName()).get(key).get(0).getIsOblige().equals(1L) && objects.size() == 0) {
                                    throw new AppException((MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.sheetname.empty"), key)));
                                }
                                ((List<Object>) objectImports).addAll(objects);
                                mapCellObjectImports.put(key, objectImports);
                            } else {
                                throw new AppException(MessageUtil.getResourceBundleMessage("error.import.sheetname"));
                            }

                        }
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        MessageUtil.setErrorMessage(e.getMessage());
                        throw e;
                    }
                    //20171412_Quytv7_Validate_Tra ve output_After import _start
                    try {
                        for (String sheetName : mapImportFile.get(event.getFile().getFileName()).keySet()) {
                            ResultDTO resultDTO = new ResultDTO();
                            resultDTO.setResultCode(0);
                            LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData = new LinkedHashMap<>();
                            if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_INTEGRATE)) {
                                ValidateDataIntegrate(mapCellObjectImports, resultDTO, mapImportFile.get(event.getFile().getFileName()), mapData);
                            } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_STANDARD)) {
                                ValidateDataActiveStandard(mapCellObjectImports, resultDTO, mapImportFile.get(event.getFile().getFileName()), mapData);
                                ValidateNims(resultDTO, mapImportFile.get(event.getFile().getFileName()), mapData);
                            } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_TEST)) {
                                ValidateDataActiveTest(mapCellObjectImports, resultDTO, mapImportFile.get(event.getFile().getFileName()), mapData);
                                if (resultDTO.getResultCode() == 0) {
                                    ValidateNims(resultDTO, mapImportFile.get(event.getFile().getFileName()), mapData);
                                }
                            } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_CHANGE_VLAN)) {
                                ValidateDataChangeVlan(mapCellObjectImports, resultDTO, mapImportFile.get(event.getFile().getFileName()), mapData);
                            }
                            fileOutPut = exportFileResult(workbook, mapData, 0, event.getFile().getFileName(), currStationPlan.getCrNumber().trim());
                            resultImport = new DefaultStreamedContent(new FileInputStream(fileOutPut), ".xlsx", fileOutPut.getName());
                            if (resultDTO.getResultCode() == 1) {
                                if(!isNullOrEmpty(resultDTO.getResultMessage())){
                                    MessageUtil.setErrorMessage(resultDTO.getResultMessage());
                                }
                                throw new MessageException(MessageUtil.getResourceBundleMessage("meassage.import.fail"));
                            }
                            break;
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw ex;
                    } finally {
                        if (workbook != null) {
                            workbook.close();
                        }
                        if (inputstream != null) {
                            inputstream.close();
                        }
                    }
                    //20171412_Quytv7_Validate_Tra ve output_After import _end

                    //Lay xong tham so do vao bang mapCellObjectImports
                    for (String sheetName : mapImportFile.get(event.getFile().getFileName()).keySet()) {
                        if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_INTEGRATE)) {
                            analyseDataIntegrate(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_RELATION)) {
                            analyseDataRelationCell(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_DELETE)) {
                            if ("3G".equals(currStationPlan.getNetworkType())) {
                                analyseDataDelete3G(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                            } else {
                                if (currStationPlan.getVendor().getVendorName().toUpperCase().equals(Constants.vendorType.ERICSSON.toUpperCase())) {
                                    analyseDataDelete2G(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                                } else {
                                    analyseDataDelete(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                                }
                            }
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_UPGRADE_DONWGRADE)) {
                            analyseDataUpDowngrade(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_CHANGE_FORCE)) {
                            if (currStationPlan.getVendor().getVendorName().toUpperCase().equals(Constants.vendorType.ERICSSON.toUpperCase())) {
                                analyseDataChangeSdHrForceEricsson(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                            } else {
                                analyseDataChangeSdHrForce(mapImportFile.get(event.getFile().getFileName()), lstParamDefault, mapCellObjectImports);
                            }
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_LOAD_LICENSE)) {
                            analyseDataLoadLicense(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports, lstFilePutSystem);
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_CHANGE_PARAM)) {
                            analyseDataChangeParam(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_STANDARD)) {
                            analyseDataActiveStandard(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_TEST)) {
                            analyseDataActiveTest(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                        } else if (mapImportFile.get(event.getFile().getFileName()).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_CHANGE_VLAN)) {
                            analyseDataChangeVlan(mapImportFile.get(event.getFile().getFileName()).get(sheetName), lstParamDefault, mapCellObjectImports);
                        }
                        break;
                    }
                }
            }

        } catch (Exception ex) {
//            if (ex instanceof MessageException) {
//                MessageUtil.setErrorMessage(ex.getMessage());
//            } else {
//                LOGGER.error(ex.getMessage(), ex);
//            }
            throw ex;
        }

    }

    public void preAddStationPlan() {
        currStationPlan = new StationPlan();
        fileOutPut = null;
        resultImport = null;

        //quytv7 truong hop relation khong can chon network type
        if (selectProficientType.equals(Config.IS_RULE_RELATION)) {
            currStationPlan.setNetworkType("3G");
        } else if (selectProficientType.equals(Config.IS_RULE_CHANGE_FORCE)) {
            currStationPlan.setNetworkType("2G");
        }
        mapStationResult.clear();
        fileUploadEvents.clear();
    }

    public void onChangeSelectVendor() {
        if (currStationPlan != null && currStationPlan.getVendor() != null && currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON) && selectProficientType.equals(Config.IS_RULE_CHANGE_FORCE)) {
            RequestContext.getCurrentInstance().execute("PF('defineRuleDlg').show()");
        }
    }

    public void preShowStationDetail(StationPlan stationPlan) throws Exception {
        HashMap<String, Object> filters = new HashMap<>();
        filters.put("stationPlan.id", stationPlan.getId());
        stationDetails = new ArrayList<>();
        List<StationDetail> lstStationDetails = new StationDetailServiceImpl().findList(filters);
        for (StationDetail stationDetail : lstStationDetails) {
            String[] cellCodes = stationDetail.getCellCode().split(";");
            for (String cell : cellCodes) {
                StationDetail stationDetailTemp = new StationDetail();
                stationDetailTemp.setCellCode(cell);
                stationDetailTemp.setRncBsc(stationDetail.getRncBsc());
                stationDetailTemp.setStationCode(stationDetail.getStationCode());
                stationDetailTemp.setStationResource(stationDetail.getStationResource());
                stationDetailTemp.setUpdateTime(stationDetail.getUpdateTime());
                stationDetails.add(stationDetailTemp);
            }
        }
    }

    public void preShowStationDT(StationPlan stationPlan) throws Exception {
        HashMap<String, Object> filters = new HashMap<>();
        filters.put("stationPlan.id", stationPlan.getId());
        Map<String, String> orders = new LinkedHashMap<>();

        orders.put("node.nodeCode", "ASC");
        orders.put("flowRunAction.flowRunName", "ASC");
        lazyStationDT = new LazyDataModelBaseNew<>(new StationFlowRunActionServiceImpl(), filters, orders);

    }

    public StreamedContent onDownloadFilePutOSS(StationFlowRunAction DT) {
        try {
            if (DT.getFileName() != null && DT.getFilePath() != null) {
                File fileExport = new File(DT.getFilePath());
                if (fileExport.exists()) {
                    return new DefaultStreamedContent(new FileInputStream(fileExport), ".txt", fileExport.getName());
                } else {
                    MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
                            MessageUtil.getResourceBundleMessage("label.dowload.fileName")));

                }
            } else {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("label.no.such.file"));
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public void deleteStationPlan() {
        try {
            HashMap<String, Object> filters = new HashMap<>();
            List<StationDetail> lststationDetailDelete = new ArrayList<>();
            List<StationFlowRunAction> lstStationFlowRunActionesDel = new ArrayList<>();
            if (selectStationPlans != null && selectStationPlans.size() > 0) {

                for (StationPlan plan : selectStationPlans) {
                    filters.clear();
                    filters.put("stationPlan.id", plan.getId());
                    List<StationDetail> lststationDetail = new StationDetailServiceImpl().findList(filters);
                    lststationDetailDelete.addAll(lststationDetail);
                    List<StationFlowRunAction> lstStationFlowRunActiones = new StationFlowRunActionServiceImpl().findList(filters);
                    lstStationFlowRunActionesDel.addAll(lstStationFlowRunActiones);
                }
                if (!lststationDetailDelete.isEmpty()) {
                    new StationDetailServiceImpl().delete(lststationDetailDelete);
                }
                if (!lststationDetailDelete.isEmpty()) {
                    new StationFlowRunActionServiceImpl().delete(lstStationFlowRunActionesDel);
                }
                stationPlanService.delete(selectStationPlans);
            }
            MessageUtil.setInfoMessageFromRes("label.action.delelteOk");
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteStationPlan').hide()");
            if (selectStationPlans != null) {
                selectStationPlans.clear();
            }
        } catch (Exception ex) {
            MessageUtil.setInfoMessageFromRes("label.action.deleteFail");
            logger.error(ex.getMessage(), ex);
        }
    }

    public void preDeleteStationPlan() {
        if (selectStationPlans != null && selectStationPlans.size() > 0) {
            RequestContext.getCurrentInstance().execute("PF('comfirmDeleteStationPlan').show()");
        } else {
            MessageUtil.setInfoMessageFromRes("label.form.choose.one");
        }
    }

    public StreamedContent onDownloadTemplate() {
        try {
            InputStream stream;
            String fileName = "";
            if (selectProficientType.equals(0L)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Integrate.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Integrate.Ericsson_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.NOKIA:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Integrate.Nokia_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Integrate.Nokia_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Integrate.Huawei_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Integrate.Huawei_2G;
                                break;
                        }
                        break;
                }
            } else if (selectProficientType.equals(1L)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Relation.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Relation.Ericsson_2G;
                                break;
                        }
                        break;

                    case Constants.vendorType.NOKIA:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Relation.Nokia_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Relation.Nokia_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Relation.Huawei_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Relation.Huawei_2G;
                                break;
                        }
                        break;
                }
            } else if (selectProficientType.equals(2L)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Delete.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Delete.Ericsson_2G;
                                break;
                        }
                        break;

                    case Constants.vendorType.NOKIA:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Delete.Nokia_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Delete.Nokia_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.Delete.Huawei_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.Delete.Huawei_2G;
                                break;
                        }
                        break;
                }
            } else if (selectProficientType.equals(Config.IS_RULE_CHANGE_FORCE)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.SD_HR.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.SD_HR.Ericsson_2G;
                                break;
                        }
                        break;

                    case Constants.vendorType.NOKIA:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.SD_HR.Nokia_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.SD_HR.Nokia_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.SD_HR.Huawei_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.SD_HR.Huawei_2G;
                                break;
                        }
                        break;
                }
            } else if (selectProficientType.equals(Config.IS_RULE_LOAD_LICENSE)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.LoadLicense.Ericsson_3G;
                                break;
                            case "4G":
                                fileName = Constants.fileNameTemplate.LoadLicense.Ericsson_4G;
                                break;
                        }
                        break;
                }
            } else if (selectProficientType.equals(Config.IS_RULE_CHANGE_PARAM)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ChangeParam.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ChangeParam.Ericsson_2G;
                                break;
                        }
                        break;

                    case Constants.vendorType.NOKIA:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ChangeParam.Nokia_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ChangeParam.Nokia_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ChangeParam.Huawei_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ChangeParam.Huawei_2G;
                                break;
                        }
                        break;
                }
            } else if (selectProficientType.equals(Config.IS_RULE_ACTIVE_STANDARD)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ActiveStandard.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ActiveStandard.Ericsson_2G;
                                break;
                        }
                        break;

                    case Constants.vendorType.NOKIA:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ActiveStandard.Nokia_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ActiveStandard.Nokia_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ActiveStandard.Huawei_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ActiveStandard.Huawei_2G;
                                break;
                        }
                        break;
                }
            } else if (selectProficientType.equals(Config.IS_RULE_ACTIVE_TEST)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ActiveTest.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ActiveTest.Ericsson_2G;
                                break;
                        }
                        break;

                    case Constants.vendorType.NOKIA:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ActiveTest.Nokia_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ActiveTest.Nokia_2G;
                                break;
                        }
                        break;
                    case Constants.vendorType.HUAWEI:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ActiveTest.Huawei_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ActiveTest.Huawei_2G;
                                break;
                        }
                        break;
                }
            }else if (selectProficientType.equals(Config.IS_RULE_CHANGE_VLAN)) {
                switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                    case Constants.vendorType.ERICSSON:
                        switch (currStationPlan.getNetworkType()) {
                            case "3G":
                                fileName = Constants.fileNameTemplate.ChangeVLAN.Ericsson_3G;
                                break;
                            case "2G":
                                fileName = Constants.fileNameTemplate.ChangeVLAN.Ericsson_2G;
                                break;
                        }
                        break;


                }
            }

            if (fileName != null && !"".equals(fileName)) {
                try {
                    stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/templates/import/Auto_VMSA/" + fileName);
                    return new DefaultStreamedContent(stream, "application/xls", fileName);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    return null;
                }
            } else {
                return null;
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
//            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("common.fail"),
//                    MessageUtil.getResourceBundleMessage("button.import")));
        }
        return null;
    }

    public StreamedContent onDownloadFileResult(StationPlan stationPlan) {
        try {
            File fileOutPut_ = new File(stationPlan.getFilePath());
            FileUtils.writeByteArrayToFile(fileOutPut_, stationPlan.getFileContent());
            return new DefaultStreamedContent(new FileInputStream(fileOutPut_), ".xlsx", fileOutPut_.getName());
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return null;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Process_GNOC">
    //Quytv7 Them luong Validate

    public void CreateMop(HashMap<String, List<StationConfigImport>> mapImportFile, HashMap<String, List<?>> mapCellObjectImports, String crNumber, String _userCreate, String _pathOut, String _pathTemplate, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData, Long createDtId, File fileOutPut_) throws Exception {
        try {
            fileOutPut = fileOutPut_;
            resultImport = new DefaultStreamedContent(new FileInputStream(fileOutPut_), ".xlsx", fileOutPut_.getName());
            mapStationResult = new HashMap<>();
            pathOut = _pathOut;
            pathTemplate = _pathTemplate;
            logger.info("Day roi: pathOut: " + pathOut);
            Map<String, Object> filters = new HashMap<>();
            filters.clear();
            this.userCreate = _userCreate;
            List<StationParamDefault> lstParamDefault = new ArrayList<>();
            //Lay xong tham so do vao bang mapCellObjectImports
            for (String sheetName : mapImportFile.keySet()) {
                currStationPlan = new StationPlan();
                currStationPlan.setNetworkType(mapImportFile.get(sheetName).get(0).getNetworkType());
                currStationPlan.setCrNumber(crNumber.trim());
                currStationPlan.setProficientType(mapImportFile.get(sheetName).get(0).getIsRule());
                currStationPlan.setCrNumberGnoc(crNumber.trim());
                currStationPlan.setCreateDtId(createDtId);
                filters.put("vendorName", mapImportFile.get(sheetName).get(0).getVendorName());
                List<Vendor> vendor = new VendorServiceImpl().findList(filters);
                if (vendor != null && !vendor.isEmpty()) {
                    filters.clear();
                    filters.put("vendor.vendorId", vendor.get(0).getVendorId());
                    currStationPlan.setVendor(vendor.get(0));
                    lstParamDefault = new StationParamDefaultServiceImpl().findList(filters);
                }
                if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_INTEGRATE)) {
                    analyseDataIntegrate(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_RELATION)) {
                    analyseDataRelationCell(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_DELETE)) {
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        analyseDataDelete3G(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                    } else {
                        if (currStationPlan.getVendor().getVendorName().toUpperCase().equals(Constants.vendorType.ERICSSON.toUpperCase())) {
                            analyseDataDelete2G(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                        } else {
                            analyseDataDelete(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                        }
                    }
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_UPGRADE_DONWGRADE)) {
                    analyseDataUpDowngrade(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_CHANGE_FORCE)) {
                    if (currStationPlan.getVendor().getVendorName().toUpperCase().equals(Constants.vendorType.ERICSSON.toUpperCase())) {
                        analyseDataChangeSdHrForceEricsson(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                    } else {
                        analyseDataChangeSdHrForce(mapImportFile, lstParamDefault, mapCellObjectImports);
                    }
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_CHANGE_PARAM)) {
                    analyseDataChangeParam(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_LOAD_LICENSE)) {
//                    analyseDataLoadLicense(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports, lstFilePutSystem);
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_CHANGE_PARAM)) {
                    analyseDataChangeParam(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_STANDARD)) {
                    analyseDataActiveStandard(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                } else if (mapImportFile.get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_TEST)) {
                    analyseDataActiveTest(mapImportFile.get(sheetName), lstParamDefault, mapCellObjectImports);
                }
                break;
            }


        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw ex;
        } catch (AppException ex) {
            logger.debug(ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
            throw ex;
        }
    }

    public void getDataFromFileInput(HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFileInput, InputStream inputstream, String fileName, HashMap<String, List<?>> mapCellObjectImports, ResultDTO resultDTO, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) throws Exception {
        try {
            //Lay cau hinh import
            final HashMap<String, HashMap<String, List<StationConfigImport>>> mapImportFile = new HashMap<>(mapImportFileInput);
//                    fileUploadEvents.remove(event);
            Workbook workbook = null;
            try {

                if (inputstream == null) {
                    throw new NullPointerException("inputstream is null");
                }
                //Get the workbook instance for XLS/xlsx file
                try {
                    workbook = WorkbookFactory.create(inputstream);
//                    if (workbook == null) {
//                        throw new NullPointerException("workbook is null");
//                    }
                } catch (InvalidFormatException e2) {
                    logger.debug(e2.getMessage(), e2);
                    throw new AppException(MessageUtil.getResourceBundleMessage("label.file.config.type"));
                } finally {
                    if (workbook != null) {
                        workbook.close();
                    }
                    if (inputstream != null) {
                        inputstream.close();
                    }
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
//                MessageUtil.setErrorMessageFromRes("meassage.import.fail2");
                throw new AppException(MessageUtil.getResourceBundleMessage("meassage.import.fail2"));
            }

            try {
                for (String key : mapImportFile.get(fileName).keySet()) {
                    List<?> objectImports = new LinkedList<>();
                    Importer<Serializable> importer = new Importer<Serializable>() {

                        @Override
                        protected Map<Integer, String> getIndexMapFieldClass() {
                            return null;
                        }

                        @Override
                        protected String getDateFormat() {
                            return null;
                        }
                    };

                    importer.setRowHeaderNumber(1);
                    List<Serializable> objects = importer.getDatas(workbook, key, "2-");
                    if (objects != null && objects.size() > 0) {
                        if (mapImportFile.get(fileName).get(key) != null &&
                                mapImportFile.get(fileName).get(key).get(0).getIsRule() != null &&
                                mapImportFile.get(fileName).get(key).get(0).getIsRule().equals(1L) && objects.size() == 0) {
                            throw new AppException((MessageFormat.format(MessageUtil.getResourceBundleMessage("error.import.sheetname.empty"), key)));
                        }
                        ((List<Object>) objectImports).addAll(objects);
                        mapCellObjectImports.put(key, objectImports);
                    } else {
                        throw new AppException(MessageUtil.getResourceBundleMessage("error.import.sheetname"));
                    }

                }
                try {
                    Map<String, Object> filters = new HashMap<>();
                    for (String sheetName : mapImportFile.get(fileName).keySet()) {
                        currStationPlan = new StationPlan();
                        currStationPlan.setNetworkType(mapImportFile.get(fileName).get(sheetName).get(0).getNetworkType());
                        currStationPlan.setProficientType(mapImportFile.get(fileName).get(sheetName).get(0).getIsRule());
                        filters.put("vendorName", mapImportFile.get(fileName).get(sheetName).get(0).getVendorName());
                        List<Vendor> vendor = new VendorServiceImpl().findList(filters);
                        if (vendor != null && !vendor.isEmpty()) {
                            filters.clear();
                            filters.put("vendor.vendorId", vendor.get(0).getVendorId());
                            currStationPlan.setVendor(vendor.get(0));
                        }

                        if (mapImportFile.get(fileName).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_INTEGRATE)) {
                            ValidateDataIntegrate(mapCellObjectImports, resultDTO, mapImportFile.get(fileName), mapData);
                        } else if (mapImportFile.get(fileName).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_STANDARD)) {
                            ValidateDataActiveStandard(mapCellObjectImports, resultDTO, mapImportFile.get(fileName), mapData);

                        } else if (mapImportFile.get(fileName).get(sheetName).get(0).getIsRule().equals(Config.IS_RULE_ACTIVE_TEST)) {
                            ValidateDataActiveTest(mapCellObjectImports, resultDTO, mapImportFile.get(fileName), mapData);
                        }
                        break;
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                    throw ex;
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
//                MessageUtil.setErrorMessageFromRes("error.import.param.fail");
                throw new AppException(MessageUtil.getResourceBundleMessage("error.import.param.fail"));
            }
        } catch (Exception ex) {
            if (ex instanceof MessageException) {
//                MessageUtil.setErrorMessage(ex.getMessage());
            } else {
                LOGGER.error(ex.getMessage(), ex);
            }
            throw ex;
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Integrate">
    public void ValidateDataIntegrate(HashMap<String, List<?>> mapCellObjectImports, ResultDTO resultDTO, HashMap<String, List<StationConfigImport>> mapImportSheet, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            List<String> lstStringCell;
            List<String> lstStringNodeB;


            lstStringCell = new ArrayList<>();
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.ZONE").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.nodeB").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.userLabel").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.UtrancellID").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.localCellId").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.uarfcnDl").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.uarfcnUl").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.rac").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.URA").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.primaryScramblingCode").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.tCell").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.pwroffset").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.pwradm").toLowerCase());
            lstStringCell.add(MessageUtil.getResourceBundleMessage("label.dlCodeAdm").toLowerCase());

            lstStringNodeB = new ArrayList<>();
            lstStringNodeB.add(MessageUtil.getResourceBundleMessage("label.oam_ip_address").toLowerCase());
            lstStringNodeB.add(MessageUtil.getResourceBundleMessage("label.nodeB").toLowerCase());
            lstStringNodeB.add(MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase());
            for (String sheetName : mapCellObjectImports.keySet()) {
                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                logger.info("---Quytv7 mapCellObjectImports: " + mapCellObjectImports.size());

                BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(0);

                //Check sheet nodeB
                if (sheetName.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.nodeB"))) {
                    if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {

                        for (String str : lstStringNodeB) {
                            if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                                resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                                resultDTO.setResultCode(1);
                            }
                        }
                    }
                } else {
                    //check cell
                    if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                        for (String str : lstStringCell) {
                            if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                                resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                                resultDTO.setResultCode(1);
                            }
                        }
                    } else if (Config.networkType2G.equalsIgnoreCase(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                        lstStringCell = new ArrayList<>();

                        lstStringCell.add(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase());
                        lstStringCell.add(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                        lstStringCell.add(mapImportSheet.get(sheetName).get(0).getColumnValidate());
                        for (String str : lstStringCell) {
                            if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                                resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                                resultDTO.setResultCode(1);
                            }
                        }
                    }
                }
            }
            if (resultDTO.getResultMessage() != null && !resultDTO.getResultMessage().isEmpty()) {
                return;
            }
            HashMap<String, String> mapNodeB = new HashMap<>();
            Map<String, Object> filters = new HashMap<>();
            for (String sheetName : mapCellObjectImports.keySet()) {

                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    if (sheetName.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.nodeB"))) {
                        continue;
                    }
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstStringCell.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }


                        mapNodeB.put(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.nodeB").toLowerCase()).toString(), sheetName);

                        if ("OK".equalsIgnoreCase(resultCode)) {
                            try {
                                if (!Config.validateIntegrate_uarfcnDl_uarfcnUl.equals(Long.valueOf(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.uarfcnDl").toLowerCase()).toString()) - Long.valueOf(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.uarfcnUl").toLowerCase()).toString()))) {
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + " uarfcnDl-uarfcnUl#950";
                                    resultCode = "NOK";
                                }
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                                resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + " Exception: uarfcnDl-uarfcnUl#950";
                                resultCode = "NOK";
                            }

                            try {
                                if ((Long.valueOf(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.primaryScramblingCode").toLowerCase()).toString()) > Config.validateIntegrate_primaryScramblingCode) || (Long.valueOf(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.primaryScramblingCode").toLowerCase()).toString()) < 0)) {
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + "primaryScramblingCode < 0 or primaryScramblingCode >511";
                                    resultCode = "NOK";
                                }
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                                resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + " Exception: primaryScramblingCode < 0 or primaryScramblingCode >511";
                                resultCode = "NOK";
                            }
                            try {
                                if ((Long.valueOf(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.pwroffset").toLowerCase()).toString()) + Long.valueOf(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.pwradm").toLowerCase()).toString())) > Config.validateIntegrate_pwroffset_pwradm) {
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + "pwroffset + pwradm >100";
                                    resultCode = "NOK";
                                }
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                                resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + " Exception: pwroffset + pwradm >100";
                                resultCode = "NOK";
                            }
                            try {
                                if (!basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.UtrancellID").toLowerCase()).toString().equalsIgnoreCase(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.localCellId").toLowerCase()).toString())
                                        || !basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.UtrancellID").toLowerCase()).toString().equalsIgnoreCase(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase()).toString())
                                        || !basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase()).toString().equalsIgnoreCase(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.localCellId").toLowerCase()).toString())
                                        ) {
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + "UtrancellID#localCellId#sac";
                                    resultCode = "NOK";
                                }
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                                resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + " Exception: UtrancellID#localCellId#sac";
                                resultCode = "NOK";
                            }
                        }

                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                } else if (Config.networkType2G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {

                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstStringCell.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }


                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                }

            }
            for (String sheetName : mapCellObjectImports.keySet()) {
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                        continue;
                    }
                    if (!sheetName.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.nodeB"))) {
                        continue;
                    }
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstStringNodeB.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }

                        if (!mapNodeB.containsKey(basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.nodeB").toLowerCase()).toString())) {
                            resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + " Not find nodeB: " + basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.nodeB").toLowerCase()).toString() + " in sheet cell";
                            resultCode = "NOK";
                        }

                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail information in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                    }
                    mapData.put(sheetName, ListRowData);
                }
            }


        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void analyseDataIntegrate(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeB;
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.NodeB").toLowerCase());
                        } else {
                            nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase());
                        }

                        if (nodeB != null) {
                            multimapParam.put(nodeB.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.NodeB")));
                        } else {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BTS")));
                        }
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();

            for (String nodeB : multimapParam.keySet()) {
                try {
//                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, String> mapParam = new HashMap<>();
                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                for (String paramCode : mapInBasicDynaBean.keySet()) {
                                    if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                        mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    } else {
                                        mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    }
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }
                    if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)) {
                        calculateParamEricsson(mapParam, lstParamDefault);
                    } else if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
                        calculateParamNokia(mapParam, lstParamDefault);
                    }

//                    if ("3G".equals(currStationPlan.getNetworkType())) {
//                        if (mapParam.containsKey("oam_ip_address")) {
//                            mapParam.put("oam_ip_address", convertIp(mapParam.get("oam_ip_address"), 1, -2));
//                        } else {
//                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), "oam_ip_address"));
//                        }
//                    }
                    //Luu log import
                    StationResult stationResult;
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        for (String paramCode : mapParam.keySet()) {
                            if (mapParam.get(paramCode) == null
                                    || "".equals(mapParam.get(paramCode))
                                    || mapParam.get(paramCode).startsWith(";")
                                    || mapParam.get(paramCode).contains(";;")) {
                                logger.info("param is null: " + paramCode);
//                                checkCreateMop = false;
                                stationResult = new StationResult();
                                stationResult.setParamCode(paramCode);
                                stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
                                stationResult.setNodeB(nodeB);
                                stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                                stationResult.setType("Integrate");
                                stationResult.setDetailResult(
                                        MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"),
                                                paramCode));
                                lstStationResult.add(stationResult);
                            } else {
                                mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
                            }
                        }
                    } else {
                        for (String paramCode : mapParam.keySet()) {
//                            if (mapParam.get(paramCode) == null
//                                    || mapParam.get(paramCode).equals("")
//                                    || mapParam.get(paramCode).startsWith(";")
//                                    || mapParam.get(paramCode).contains(";;")) {
//                                checkCreateMop = false;
//                                stationResult = new StationResult();
//                                stationResult.setParamCode(paramCode);
//                                stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                                stationResult.setNodeB(nodeB);
//                                stationResult.setResult("NOK, Khong sinh mop");
//                                stationResult.setType("Integrate");
//                                stationResult.setDetailResult("Tham so dien thieu hoac sai dieu kien");
//                                lstStationResult.add(stationResult);
//                            } else {
                            mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
//                            }
                        }
                    }

//                    if (checkCreateMop) {
                    mapNodeBParam.put(nodeB, mapParam);
//                    } else {
//                        logger.info(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
//                                nodeB));
//                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
//                                nodeB));
//                        if (!lstStationResult.isEmpty()) {
//                            if (mapStationResult.containsKey(nodeB)) {
//                                mapStationResult.get(nodeB).addAll(lstStationResult);
//                            } else {
//                                mapStationResult.put(nodeB, lstStationResult);
//                            }
//                        }
//                        throw new AppException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
//                                nodeB));
//                    }

                } catch (Exception e) {
//                    if (e instanceof MessageException) {
//                        MessageUtil.setErrorMessage(e.getMessage());
//                    } else {
////                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
//                        LOGGER.error(e.getMessage(), e);
//                        throw new Exception(e.getMessage());
//                    }
                    throw e;
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeBRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeBMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(userCreate);
            currStationPlan.setProficientType(0L);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> stationDetails = new ArrayList<>();
            StationDetail stationDetail;
            StationResult stationResult;
            Map<String, Object> filters = new HashMap<>();
            String bscType = null;
            String btsType = null;
            for (String nodeB : mapNodeBParam.keySet()) {
                List<StationResult> lstStationResult = new ArrayList<>();
                stationDetail = new StationDetail();
                stationDetail.setStationPlan(currStationPlan);
                stationDetail.setStationCode(nodeB);

                if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
                        || mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {
                    String[] rncBscs;
                    String[] btsTypes;
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase()).split(";");
                    } else {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase()).split(";");
                        if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.station.btsType").trim().toLowerCase())) {
                            btsTypes = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.station.btsType").trim().toLowerCase()).split(";");
                            btsType = btsTypes[0];
                            mapNodeBParam.get(nodeB).put(MessageUtil.getResourceBundleMessage("label.station.btsType").trim().toLowerCase(), btsType);
                        } else {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.btsType")));
                        }
                    }

                    if (rncBscs.length == 0) {

                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    } else {
                        filters.put("rncBsc", rncBscs[0]);
                        List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);

                        if (lstResource.isEmpty()) {
                            stationResult = new StationResult();
                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.station.rncBsc").trim());
                            stationResult.setParamValue(null);
                            stationResult.setNodeB(nodeB);
                            stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                            stationResult.setType("Integrate");
                            stationResult.setDetailResult(
                                    MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"),
                                            MessageUtil.getResourceBundleMessage("label.station.rncBsc")));

                            lstStationResult.add(stationResult);
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                    nodeB));
                            mapNodeBParam.remove(nodeB);
//                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
                        } else {
                            mapNodeBRncBsc.put(nodeB, lstResource.get(0).getRncBsc());
                            mapNodeBMsc.put(nodeB, lstResource.get(0).getMsc());
                            stationDetail.setStationResource(lstResource.get(0));
                            stationDetail.setRncBsc(lstResource.get(0).getRncBsc());
                            if ("3G".equals(currStationPlan.getNetworkType())) {
                                stationDetail.setCellCode(mapNodeBParam.get(nodeB).get("userlabel"));
                            } else {
                                stationDetail.setCellCode(mapNodeBParam.get(nodeB).get("cell"));
                            }

                            stationDetail.setUpdateTime(new Date());
                            if (lstResource.get(0).getBscType() != null) {
                                bscType = lstResource.get(0).getBscType();
                                mapNodeBParam.get(nodeB).put(MessageUtil.getResourceBundleMessage("label.station.bscType").trim().toLowerCase(), bscType);
                            }
                        }
                    }
                } else {
                    throw new AppException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                }
                stationDetails.add(stationDetail);
                if (!lstStationResult.isEmpty()) {
                    if (mapStationResult.containsKey(nodeB)) {
                        mapStationResult.get(nodeB).addAll(lstStationResult);
                    } else {
                        mapStationResult.put(nodeB, lstStationResult);
                    }
                }
            }
            new StationPlanServiceImpl().saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(stationDetails);
            //Sinh mop RNC, BSC
            createMopIntegrate(mapNodeBParam, stationConfigImports, mapNodeBRncBsc, mapNodeBMsc, bscType, btsType);

        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

    }

    public void createMopIntegrate(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, String bscType, String btsType) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        try {
            createFlowRunActionIntegrate(mapNodeBParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    protected boolean createFlowRunActionIntegrate(HashMap<String, HashMap<String, String>> mapNodeBParam, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
        logger.info("vao ham createFlowRunActionIntegrate");
        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        String btsType = null;
        String bscType = null;
        for (String nodeB : mapNodeBParam.keySet()) {
            logger.info("Bat dau tao mop cho node: " + nodeB);
            if ("3G".equals(currStationPlan.getNetworkType())) {
                flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
            } else {
                if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)) {
                    if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.station.bscType").trim().toLowerCase())) {
                        bscType = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.station.bscType").trim().toLowerCase());
                    }
                    if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.station.btsType").trim().toLowerCase())) {
                        btsType = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.station.btsType").trim().toLowerCase());
                    }
                    if (bscType != null && btsType != null) {
                        for (StationConfigImport sci : stationConfigImports) {
                            if (bscType.equalsIgnoreCase(sci.getBscType()) && btsType.equalsIgnoreCase(sci.getBtsType())) {
                                flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
                                break;
                            }
                        }
                    } else {
                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                    }
                } else if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
//                    if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.station.transmission.type").trim().toLowerCase())) {
//                        String bscTypes[] = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.station.transmission.type").trim().toLowerCase()).split(";");
//                        bscType = bscTypes[0];
//                    }

//                    if (bscType != null) {
//                        for (StationConfigImport sci : stationConfigImports) {
//                            if (bscType.equalsIgnoreCase(sci.getBscType())) {
//                                flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
//                                break;
//                            }
//                        }
//                    } else {
//                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//                    }
                    for (StationConfigImport sci : stationConfigImports) {
                        flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
                        break;
                    }
                }
            }
            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//                throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//                break;
                continue;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
//                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Integrate-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-NodeB_" + nodeB.toUpperCase() + "-RNC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Integrate-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BTS_" + nodeB.toUpperCase() + "-BSC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                generateFlowRunController.setPathTemplate(pathTemplate);
                FlowRunAction flowRunAction = new FlowRunAction();

                flowRunAction.setCrNumber(Config.CR_DEFAULT);

                flowRunAction.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
                if (currStationPlan.getCreateDtId() != null) {
                    flowRunAction.setCreateDtFromGnocId(currStationPlan.getCreateDtId());
                }

//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang
                generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", mapNodeBRncBsc.get(nodeB));
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                Node nodeMsc;
                List<Node> lstnodeMsc = new ArrayList<>();
                filters.clear();
                String[] mscStr = mapNodeBMsc.get(nodeB).split(";");
                for (String msc : mscStr) {
                    filters.clear();
                    filters.put("nodeCode-EXAC", msc);
                    List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
                    if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
                    } else {
                        nodeMsc = nodeMscs.get(0);
                        lstnodeMsc.add(nodeMsc);
                    }
                }
                mapParamValues = mapNodeBParam.get(nodeB);
                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_start
                StationConfigImport stationConfigImport = stationConfigImports.get(0);
                if (stationConfigImport != null && !isNullOrEmpty(stationConfigImport.getColumnValidate())) {
                    logger.info("Bat dau cap nhat DataCell cac cell cap nhat sang nims");
                    HashMap<String, String> mapDataCell = new HashMap<>();
                    String cell = mapParamValues.get(stationConfigImport.getColumnValidate().toLowerCase());
                    String[] cells = cell.split(Config.SPLITTER_VALUE);
                    for (String cell1 : cells) {
                        if (!isNullOrEmpty(cell1)) {
                            mapDataCell.put(cell1, stationConfigImport.getNetworkType());
                        }
                    }
                    DataCellUpdateNimsForGnoc dataCellUpdateNimsForGnoc = new DataCellUpdateNimsForGnoc();
                    dataCellUpdateNimsForGnoc.setCrNumber(currStationPlan.getCrNumber().trim());
                    dataCellUpdateNimsForGnoc.setMapDataCell(mapDataCell);
                    dataCellUpdateNimsForGnoc.setClassNimsWS(stationConfigImport.getClassNimsWs());
                    dataCellUpdateNimsForGnoc.setFunctionNimsWs(stationConfigImport.getFunctionNimsWs());
                    dataCellUpdateNimsForGnoc.setIsVaildateNims(stationConfigImport.getIsValidateNims());
                    dataCellUpdateNimsForGnoc.setIsUpdateNims(stationConfigImport.getIsUpdateNims());
                    if (mapDataCell != null && mapDataCell.size() > 0) {
                        String encrytedMess = new String((new Gson()).toJson(dataCellUpdateNimsForGnoc).getBytes("UTF-8"), "UTF-8");
                        generateFlowRunController.getFlowRunAction().setDataCell(encrytedMess);
                    }
                }
                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_end


                if ("3G".equals(currStationPlan.getNetworkType())) {
                    mapParamValues.put("file_name", filePutOSS);
                    mapParamValues.put("nodeb", nodeB);
                    if (mapParamValues.containsKey("oam_ip_address")) {
                        mapParamValues.put("oam_ip_address", convertIp(mapParamValues.get("oam_ip_address"), 1, -2));
                    } else {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), "oam_ip_address"));
                    }
                }

                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                for (Node node : lstnodeMsc) {
                    generateFlowRunController.getNodes().add(node);
                    nodeInPlan.add(node);
                }
                nodeInPlan.add(nodeRncBsc);

//                setParamFromTableOther(plan, mapParamValues, riResult, uniqueParams, mopType);
                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
//                        LOGGER.info("Show ParamCode: " + paramValue.getParamCode());
//                        if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
//                            continue;
//                        }
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        ResourceBundle bundle = ResourceBundle.getBundle("cas");
                        if (bundle.getString("service").contains("10.61.127.190")) {
                            if (value == null || value.toString().isEmpty()) {
                                value = "TEST_NOT_FOUND";
                            }
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
                                        .replace("{1}", paramValue.getParamCode()));
                                return false;
                            }
                        }
                    }
                    //Quytv7 truong hop tich hop tram khong can khai tren msc nay
                    if (Config.MscNotRunIntegrateStation.toUpperCase().contains(node.getNodeCode().toUpperCase() + ";")) {
                        for (GroupAction groupAction : generateFlowRunController.getMapGroupAction().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + node.getNodeCode())) {
                            groupAction.setDeclare(false);
                        }
                    }
                }
                mapParamValues.clear();

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {

                    logger.info("Tao mop thanh cong cho node: " + nodeB);
                    logger.info("Save DT Sucess, update crNumber: " + currStationPlan.getCrNumberGnoc());
                    if (currStationPlan != null && !isNullOrEmpty(currStationPlan.getCrNumberGnoc())) {
                        flowRunAction.setCrNumber(currStationPlan.getCrNumberGnoc());
                        new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
                    }
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file
                        StringBuilder stringBuilder = new StringBuilder();
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            generateFlowRunController.setSelectedNode(nodeRncBsc);
                            generateFlowRunController.loadMultiParam();

                            for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                                if ("Sinh du lieu tich hop tren RNC".equals(groupAction.getGroupActionName())) {
                                    for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                                        for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                                            if (nodeRncBsc.getVendor().equals(actionDetail.getVendor())
                                                    && nodeRncBsc.getNodeType().equals(actionDetail.getNodeType())
                                                    && nodeRncBsc.getVersion().equals(actionDetail.getVersion())) {
                                                for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                                                    String cmd = actionCommand.getCommandDetail().buildCommandFullValue(generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeRncBsc), false, actionOfFlow, actionCommand);
                                                    if (cmd != null && !cmd.isEmpty()) {
                                                        if (actionCommand.getType() == Config.EXECUTE_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        } else if (actionCommand.getType() == Config.ROLLBACK_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

//                                generateFlowRunController.createToFilePutOss(groupAction.getGroupActionName(), stringBuilder);
                                    groupAction.setDeclare(false);
                                    File files = createFilePutOSS(filePutOSS, stringBuilder);
                                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                    stationFlowRunAction.setFileName(files.getName());
                                    stationFlowRunAction.setFileContent(readBytesFromFile(files));
                                    stationFlowRunAction.setFlowRunAction(flowRunAction);
                                    stationFlowRunAction.setNode(nodeRncBsc);
                                    stationFlowRunAction.setFilePath(files.getPath());
                                    stationFlowRunAction.setGroupActionName("Sinh du lieu tich hop tren RNC");
                                    stationFlowRunAction.setStationPlan(currStationPlan);
                                    stationFlowRunAction.setCommandPutFile("Put file OSS for RNC Ericsson");
                                    stationFlowRunAction.setType(1L);
                                    stationFlowRunAction.setUpdateTime(new Date());
                                    stationFlowRunActiones.add(stationFlowRunAction);

                                    break;
                                }
                            }
                        } else {
                            StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                            stationFlowRunAction.setFlowRunAction(flowRunAction);
                            stationFlowRunAction.setNode(nodeRncBsc);
                            stationFlowRunAction.setStationPlan(currStationPlan);
                            stationFlowRunAction.setType(1L);
                            stationFlowRunAction.setUpdateTime(new Date());
                            stationFlowRunActiones.add(stationFlowRunAction);
                        }

                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), mapNodeBRncBsc.get(nodeB)));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao

                } else {
                    logger.info("Tao mop that bai cho node: " + nodeB);
                }
//                return saveDT;
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
//                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }

//                return false;
//            throw e;
            } finally {
                //Tam biet
            }
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Relation">
    public void analyseDataRelationCell(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.rncBsc").toLowerCase());
                        if (nodeB != null) {
                            multimapParam.put(nodeB.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();

            for (String nodeB : multimapParam.keySet()) {
                boolean checkCreateMop = true;
                List<StationResult> lstStationResult = new ArrayList<>();
                try {

                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    HashMap<String, HashMap<String, String>> mapSheetParam = new HashMap<>();
                    HashMap<String, String> mapParamFinal = new HashMap<>();
                    try {
                        HashMap<String, String> mapParam;
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            String sheetName = basicDynaBean.getDynaClass().getName().trim().toLowerCase().replace(" ", "_").replace(".", "_");
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                if (mapSheetParam.containsKey(sheetName)) {
                                    mapParam = mapSheetParam.get(sheetName);

                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                    mapSheetParam.put(sheetName, mapParam);
                                } else {
                                    mapParam = new HashMap<>();
                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                    mapSheetParam.put(sheetName, mapParam);
                                }

                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }
                    for (String nameSheet : mapSheetParam.keySet()) {
//                        calculateParam(mapSheetParam.get(nameSheet), lstParamDefault, LOGGER);
                        StationResult stationResult;
                        for (String paramCode : mapSheetParam.get(nameSheet).keySet()) {
//                        if (mapParam.get(paramCode) == null || mapParam.get(paramCode).equals("") || mapParam.get(paramCode).contains(";;")) {
//                            checkCreateMop = false;
//                            stationResult = new StationResult();
//                            stationResult.setParamCode(paramCode);
//                            stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                            stationResult.setNodeCode(nodeB);
//                            stationResult.setResult("NOK, Khong sinh mop");
//                            stationResult.setType("Relation");
//                            stationResult.setDetailResult("Khong du tham so trong file import");
//                            lstStationResult.add(stationResult);
//                        } else {
                            if (mapParamFinal.containsKey(nameSheet + "-" + paramCode.trim().toLowerCase())) {
                                mapParamFinal.put((nameSheet + "-" + paramCode.trim().toLowerCase()), mapParamFinal.get((nameSheet + "-" + paramCode.trim().toLowerCase())) + Config.SPLITTER_VALUE + (mapSheetParam.get(nameSheet).get(paramCode) == null ? "" : mapSheetParam.get(nameSheet).get(paramCode)));
                            } else {
                                mapParamFinal.put((nameSheet + "-" + paramCode.trim().toLowerCase()), mapSheetParam.get(nameSheet).get(paramCode) == null ? "" : mapSheetParam.get(nameSheet).get(paramCode));
                            }

//                        }
                        }
                    }
//                    for (String paramCode : mapParamFinal.keySet()) {
//                        mapParamFinal.put(paramCode, mapParamFinal.get(paramCode).substring(0, Math.min(3950, mapParamFinal.get(paramCode).length())));
//                    }
                    if (checkCreateMop) {
                        mapNodeBParam.put(nodeB, mapParamFinal);
                    }
                    if (!lstStationResult.isEmpty()) {
                        if (mapStationResult.containsKey(nodeB)) {
                            mapStationResult.get(nodeB).addAll(lstStationResult);
                        } else {
                            mapStationResult.put(nodeB, lstStationResult);
                        }
                    }

                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            currStationRelationPlan = new StationPlan();
            if (currStationPlan != null) {
                currStationRelationPlan.setCrNumber(currStationPlan.getCrNumber().trim());
                currStationRelationPlan.setNetworkType(currStationPlan.getNetworkType() == null ? "" : currStationPlan.getNetworkType());
                currStationRelationPlan.setVendor(currStationPlan.getVendor() == null ? null : currStationPlan.getVendor());
                currStationRelationPlan.setUpdateTime(new Date());
                currStationRelationPlan.setUserCreate(userCreate);
                currStationRelationPlan.setProficientType(1L);
                if (fileOutPut != null) {
                    currStationRelationPlan.setFileName(fileOutPut.getName());
                    currStationRelationPlan.setFileContent(readBytesFromFile(fileOutPut));
                    currStationRelationPlan.setFilePath(fileOutPut.getPath());
                }
            }

            stationPlanService.saveOrUpdate(currStationRelationPlan);
            //Sinh mop RNC, BSC
            createMopRelationCell(mapNodeBParam, stationConfigImports);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void createMopRelationCell(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        FlowTemplates flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
        if (flowTemplates == null) {
            throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
        } else {
            if (flowTemplates.getStatus() != 9) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
            }
        }
//        createFlowRunActionRelationCellGroupNode(mapNodeBParam, flowTemplates);
        createFlowRunActionRelationCellGroupNodeNew(mapNodeBParam, flowTemplates);
    }

    protected boolean createFlowRunActionRelationCell(HashMap<String, HashMap<String, String>> mapNodeParam, FlowTemplates flowTemplates) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        for (String rncBsc : mapNodeParam.keySet()) {
            try {
                String filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                        + "-Relation"
                        + "-RNC_BSC_" + rncBsc.toUpperCase()
                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                generateFlowRunController = new GenerateFlowRunController();
                FlowRunAction flowRunAction = new FlowRunAction();
                flowRunAction.setCrNumber(Config.CR_DEFAULT);
                flowRunAction.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang 
                generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", rncBsc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }

                mapParamValues = mapNodeParam.get(rncBsc);
                mapParamValues.put("file_name", filePutOSS);

                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);

                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }

                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                    if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                .replace("{1}", paramValue.getParamCode()));
//                        return false;
//                    }
                    }
                }
                mapParamValues.clear();
//                if (currStationPlan.getId() == null) {
//                    MessageUtil.setErrorMessage("Khong tim thay du lieu tích hợp trạm");
//                    return false;
//                }
                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file

                        if ("RNC".equals(nodeRncBsc.getNodeType().getTypeName()) && "ERICSSON".equals(nodeRncBsc.getVendor().getVendorName().toUpperCase())) {
                            StringBuilder stringBuilder = new StringBuilder();
//                            
                            generateFlowRunController.setSelectedNode(nodeRncBsc);
                            generateFlowRunController.loadMultiParam();

                            for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                                if ("Sinh dữ liệu Relation trên RNC".equals(groupAction.getGroupActionName())) {
                                    for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                                        for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                                            if (nodeRncBsc.getVendor().equals(actionDetail.getVendor())
                                                    && nodeRncBsc.getNodeType().equals(actionDetail.getNodeType())
                                                    && nodeRncBsc.getVersion().equals(actionDetail.getVersion())) {
                                                for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                                                    String cmd = actionCommand.getCommandDetail().buildCommandFullValue(generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeRncBsc), false, actionOfFlow, actionCommand);
                                                    if (cmd != null && !cmd.isEmpty()) {
                                                        if (actionCommand.getType() == Config.EXECUTE_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        } else if (actionCommand.getType() == Config.ROLLBACK_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    groupAction.setDeclare(false);
                                    File files = createFilePutOSS(filePutOSS, stringBuilder);
                                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                    stationFlowRunAction.setFileName(files.getName());
                                    stationFlowRunAction.setFileContent(readBytesFromFile(files));
                                    stationFlowRunAction.setFlowRunAction(flowRunAction);
                                    stationFlowRunAction.setNode(nodeRncBsc);
                                    stationFlowRunAction.setFilePath(files.getPath());
                                    stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                                    stationFlowRunAction.setStationPlan(currStationRelationPlan);
                                    stationFlowRunAction.setCommandPutFile("Put file OSS for RNC Ericsson");
                                    stationFlowRunAction.setType(2L);
                                    stationFlowRunAction.setUpdateTime(new Date());
                                    stationFlowRunActiones.add(stationFlowRunAction);
                                    break;
                                }
                            }
                        } else {
                            StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                            stationFlowRunAction.setFlowRunAction(flowRunAction);
                            stationFlowRunAction.setNode(nodeRncBsc);
                            stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                            stationFlowRunAction.setStationPlan(currStationRelationPlan);
                            stationFlowRunAction.setType(2L);
                            stationFlowRunAction.setUpdateTime(new Date());
                            stationFlowRunActiones.add(stationFlowRunAction);
                        }
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), rncBsc));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao 

                }
//                return saveDT;
            } catch (Exception e) {
                if (e instanceof MessageException) {
//                    MessageUtil.setErrorMessage(e.getMessage());
                    throw e;
                } else {
//                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }
//                return false;
//            throw e;

            } finally {
                //Tam biet
            }
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    protected boolean createFlowRunActionRelationCellGroupNode(HashMap<String, HashMap<String, String>> mapNodeParam, FlowTemplates flowTemplates) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();

        try {
            String filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                    + "-Relation"
//                        + "-RNC_BSC_" + rncBsc.toUpperCase()
                    + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();
            flowRunAction.setCrNumber(Config.CR_DEFAULT);
            flowRunAction.setFlowRunName(filePutOSS);

            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplates);
            flowRunAction.setExecuteBy(userCreate);
            flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
            //Lay danh sach param tu bang
            generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
            Map<String, String> mapParamValues;
            generateFlowRunController.loadGroupAction(0l);

            List<Node> nodeInPlan;
            List<Node> listNodeRncBsc = new ArrayList<>();
            HashMap<Node, String> mapNodeFileName = new HashMap<>();
            for (String rncBsc : mapNodeParam.keySet()) {
                filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                        + "-Relation"
                        + "-RNC_BSC_" + rncBsc.toUpperCase()
                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", rncBsc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), rncBsc));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                mapParamValues = mapNodeParam.get(rncBsc);


                mapParamValues.put("file_name", filePutOSS);

                nodeInPlan = new ArrayList<>();
                listNodeRncBsc.add(nodeRncBsc);
                mapNodeFileName.put(nodeRncBsc, filePutOSS);
                generateFlowRunController.getNodes().add(nodeRncBsc);
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);

                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }

                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                    if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                .replace("{1}", paramValue.getParamCode()));
//                        return false;
//                    }
                    }
                }
                mapParamValues.clear();
            }
//                if (currStationPlan.getId() == null) {
//                    MessageUtil.setErrorMessage("Khong tim thay du lieu tích hợp trạm");
//                    return false;
//                }
            boolean saveDT = generateFlowRunController.saveDT();
            //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_start
            generateFlowRunController.loadParamDt();
            //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_end
            if (saveDT) {
                try {
                    //Bat dau xu ly day file OSS
                    //Tao file
                    for (Node nodeRncBsc : listNodeRncBsc) {
                        if ("RNC".equals(nodeRncBsc.getNodeType().getTypeName()) && "ERICSSON".equals(nodeRncBsc.getVendor().getVendorName().toUpperCase())) {
                            StringBuilder stringBuilder = new StringBuilder();
//
                            generateFlowRunController.setSelectedNode(nodeRncBsc);
                            generateFlowRunController.loadMultiParam();

                            for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                                if ("Sinh dữ liệu Relation trên RNC".equals(groupAction.getGroupActionName())) {
                                    for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                                        for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                                            if (nodeRncBsc.getVendor().equals(actionDetail.getVendor())
                                                    && nodeRncBsc.getNodeType().equals(actionDetail.getNodeType())
                                                    && nodeRncBsc.getVersion().equals(actionDetail.getVersion())) {
                                                for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                                                    String cmd = actionCommand.getCommandDetail().buildCommandFullValue(generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeRncBsc), false, actionOfFlow, actionCommand);
                                                    if (cmd != null && !cmd.isEmpty()) {
                                                        if (actionCommand.getType() == Config.EXECUTE_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        } else if (actionCommand.getType() == Config.ROLLBACK_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    groupAction.setDeclare(false);
                                    File files = createFilePutOSS(mapNodeFileName.get(nodeRncBsc), stringBuilder);
                                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                    stationFlowRunAction.setFileContent(readBytesFromFile(files));
                                    stationFlowRunAction.setFileName(files.getName());
                                    stationFlowRunAction.setFlowRunAction(flowRunAction);
                                    stationFlowRunAction.setNode(nodeRncBsc);
                                    stationFlowRunAction.setFilePath(files.getPath());
                                    stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                                    stationFlowRunAction.setStationPlan(currStationRelationPlan);
                                    stationFlowRunAction.setCommandPutFile("Put file OSS for RNC Ericsson");
                                    stationFlowRunAction.setType(2L);
                                    stationFlowRunAction.setUpdateTime(new Date());
                                    stationFlowRunActiones.add(stationFlowRunAction);
                                    break;
                                }
                            }
                        } else {
                            StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                            stationFlowRunAction.setFlowRunAction(flowRunAction);
                            stationFlowRunAction.setNode(nodeRncBsc);
                            stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                            stationFlowRunAction.setStationPlan(currStationRelationPlan);
                            stationFlowRunAction.setType(2L);
                            stationFlowRunAction.setUpdateTime(new Date());
                            stationFlowRunActiones.add(stationFlowRunAction);
                        }
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeRncBsc.getNodeCode()));
                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
                //Xu ly day du lieu vao


            }
//                return saveDT;
        } catch (Exception e) {
            if (e instanceof MessageException) {
//                    MessageUtil.setErrorMessage(e.getMessage());
                throw e;
            } else {
//                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                LOGGER.error(e.getMessage(), e);
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
            }
//                return false;
//            throw e;

        } finally {
            //Tam biet
        }

        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    protected boolean createFlowRunActionRelationCellGroupNodeNew(HashMap<String, HashMap<String, String>> mapNodeParam, FlowTemplates flowTemplates) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        int group = 0;
        try {
            String filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                    + "-Relation-Group" + String.valueOf(group)
//                        + "-RNC_BSC_" + rncBsc.toUpperCase()
                    + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            String dtName = currStationPlan.getCrNumber().trim().toUpperCase()
                    + "-Relation-Group-" + String.valueOf(group)
//                        + "-RNC_BSC_" + rncBsc.toUpperCase()
                    + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();
            flowRunAction.setCrNumber(Config.CR_DEFAULT);
            flowRunAction.setFlowRunName(dtName);

            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplates);
            flowRunAction.setExecuteBy(userCreate);
            flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
            //Lay danh sach param tu bang
            generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
            Map<String, String> mapParamValues;
            generateFlowRunController.loadGroupAction(0l);

            List<Node> nodeInPlan;
            List<Node> listNodeRncBsc = new ArrayList<>();
            HashMap<Node, String> mapNodeFileName = new HashMap<>();
            int countNode = 0;
            logger.info("Sap xep lai map theo thu tu RNC");
            TreeMap<String, HashMap<String, String>> treeMap = sortHashMap(mapNodeParam);
            for (String rncBsc : treeMap.keySet()) {
                countNode++;

                filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                        + "-Relation"
                        + "-RNC_BSC_" + rncBsc.toUpperCase()
                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", rncBsc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc_;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), rncBsc));
                } else {
                    nodeRncBsc_ = nodeRncs.get(0);
                }
                mapParamValues = treeMap.get(rncBsc);


                mapParamValues.put("file_name", filePutOSS);

                nodeInPlan = new ArrayList<>();
                listNodeRncBsc.add(nodeRncBsc_);
                mapNodeFileName.put(nodeRncBsc_, filePutOSS);
                generateFlowRunController.getNodes().add(nodeRncBsc_);
                nodeInPlan.add(nodeRncBsc_);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);

                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }

                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
                    }
                }
                mapParamValues.clear();

                if (countNode >= Config.maxNodeInDT) {
                    logger.info("Save DT Reletion group: " + group);
                    boolean saveDT = generateFlowRunController.saveDT();
                    //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_start
                    generateFlowRunController.loadParamDt();
                    //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_end
                    if (saveDT) {
                        try {
                            //Bat dau xu ly day file OSS
                            //Tao file
                            for (Node nodeRncBsc : listNodeRncBsc) {
                                if ("RNC".equals(nodeRncBsc.getNodeType().getTypeName()) && "ERICSSON".equals(nodeRncBsc.getVendor().getVendorName().toUpperCase())) {
                                    StringBuilder stringBuilder = new StringBuilder();
//
                                    generateFlowRunController.setSelectedNode(nodeRncBsc);
                                    generateFlowRunController.loadMultiParam();

                                    for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                                        if ("Sinh dữ liệu Relation trên RNC".equals(groupAction.getGroupActionName())) {
                                            for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                                                for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                                                    if (nodeRncBsc.getVendor().equals(actionDetail.getVendor())
                                                            && nodeRncBsc.getNodeType().equals(actionDetail.getNodeType())
                                                            && nodeRncBsc.getVersion().equals(actionDetail.getVersion())) {
                                                        for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                                                            String cmd = actionCommand.getCommandDetail().buildCommandFullValue(generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeRncBsc), false, actionOfFlow, actionCommand);
                                                            if (cmd != null && !cmd.isEmpty()) {
                                                                if (actionCommand.getType() == Config.EXECUTE_CMD.longValue()) {
                                                                    stringBuilder.append(cmd);
                                                                } else if (actionCommand.getType() == Config.ROLLBACK_CMD.longValue()) {
                                                                    stringBuilder.append(cmd);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            groupAction.setDeclare(false);
                                            File files = createFilePutOSS(mapNodeFileName.get(nodeRncBsc), stringBuilder);
                                            StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                            stationFlowRunAction.setFileContent(readBytesFromFile(files));
                                            stationFlowRunAction.setFileName(files.getName());
                                            stationFlowRunAction.setFlowRunAction(flowRunAction);
                                            stationFlowRunAction.setNode(nodeRncBsc);
                                            stationFlowRunAction.setFilePath(files.getPath());
                                            stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                                            stationFlowRunAction.setStationPlan(currStationRelationPlan);
                                            stationFlowRunAction.setCommandPutFile("Put file OSS for RNC Ericsson");
                                            stationFlowRunAction.setType(2L);
                                            stationFlowRunAction.setUpdateTime(new Date());
                                            stationFlowRunActiones.add(stationFlowRunAction);
                                            break;
                                        }
                                    }
                                } else {
                                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                    stationFlowRunAction.setFlowRunAction(flowRunAction);
                                    stationFlowRunAction.setNode(nodeRncBsc);
                                    stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                                    stationFlowRunAction.setStationPlan(currStationRelationPlan);
                                    stationFlowRunAction.setType(2L);
                                    stationFlowRunAction.setUpdateTime(new Date());
                                    stationFlowRunActiones.add(stationFlowRunAction);
                                }
                                MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeRncBsc.getNodeCode()));
                            }
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        //Xu ly day du lieu vao
                    }
                    group++;
                    dtName = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Relation-Group-" + String.valueOf(group)
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    generateFlowRunController = new GenerateFlowRunController();
                    flowRunAction = new FlowRunAction();
                    flowRunAction.setCrNumber(Config.CR_DEFAULT);
                    flowRunAction.setFlowRunName(dtName);

                    while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                        flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                    }
                    flowRunAction.setTimeRun(new Date());
                    flowRunAction.setFlowTemplates(flowTemplates);
                    flowRunAction.setExecuteBy(userCreate);
                    flowRunAction.setCreateBy(userCreate);

                    generateFlowRunController.setFlowRunAction(flowRunAction);
                    generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                    //Lay danh sach param tu bang
                    generateFlowRunController.setNodes(new ArrayList<Node>());
                    generateFlowRunController.loadGroupAction(0l);

                    listNodeRncBsc = new ArrayList<>();
                    mapNodeFileName = new HashMap<>();

                    countNode = 0;

                }
            }
            if (countNode > 0) {
                boolean saveDT = generateFlowRunController.saveDT();
                //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_start
                generateFlowRunController.loadParamDt();
                //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_end
                if (saveDT) {
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file
                        for (Node nodeRncBsc : listNodeRncBsc) {
                            if ("RNC".equals(nodeRncBsc.getNodeType().getTypeName()) && "ERICSSON".equals(nodeRncBsc.getVendor().getVendorName().toUpperCase())) {
                                StringBuilder stringBuilder = new StringBuilder();
//
                                generateFlowRunController.setSelectedNode(nodeRncBsc);
                                generateFlowRunController.loadMultiParam();

                                for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                                    if ("Sinh dữ liệu Relation trên RNC".equals(groupAction.getGroupActionName())) {
                                        for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                                            for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                                                if (nodeRncBsc.getVendor().equals(actionDetail.getVendor())
                                                        && nodeRncBsc.getNodeType().equals(actionDetail.getNodeType())
                                                        && nodeRncBsc.getVersion().equals(actionDetail.getVersion())) {
                                                    for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                                                        String cmd = actionCommand.getCommandDetail().buildCommandFullValue(generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeRncBsc), false, actionOfFlow, actionCommand);
                                                        if (cmd != null && !cmd.isEmpty()) {
                                                            if (actionCommand.getType() == Config.EXECUTE_CMD.longValue()) {
                                                                stringBuilder.append(cmd);
                                                            } else if (actionCommand.getType() == Config.ROLLBACK_CMD.longValue()) {
                                                                stringBuilder.append(cmd);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        groupAction.setDeclare(false);
                                        File files = createFilePutOSS(mapNodeFileName.get(nodeRncBsc), stringBuilder);
                                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                        stationFlowRunAction.setFileContent(readBytesFromFile(files));
                                        stationFlowRunAction.setFileName(files.getName());
                                        stationFlowRunAction.setFlowRunAction(flowRunAction);
                                        stationFlowRunAction.setNode(nodeRncBsc);
                                        stationFlowRunAction.setFilePath(files.getPath());
                                        stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                                        stationFlowRunAction.setStationPlan(currStationRelationPlan);
                                        stationFlowRunAction.setCommandPutFile("Put file OSS for RNC Ericsson");
                                        stationFlowRunAction.setType(2L);
                                        stationFlowRunAction.setUpdateTime(new Date());
                                        stationFlowRunActiones.add(stationFlowRunAction);
                                        break;
                                    }
                                }
                            } else {
                                StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                stationFlowRunAction.setFlowRunAction(flowRunAction);
                                stationFlowRunAction.setNode(nodeRncBsc);
                                stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                                stationFlowRunAction.setStationPlan(currStationRelationPlan);
                                stationFlowRunAction.setType(2L);
                                stationFlowRunAction.setUpdateTime(new Date());
                                stationFlowRunActiones.add(stationFlowRunAction);
                            }
                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeRncBsc.getNodeCode()));
                        }
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao
                }
            }
        } catch (Exception e) {
            if (e instanceof MessageException) {
                throw e;
            } else {
                LOGGER.error(e.getMessage(), e);
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
            }
        } finally {
            //Tam biet
        }

        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    public void analyseDataRelationCell2G(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.rncBsc").toLowerCase());
                        if (nodeB != null) {
                            multimapParam.put(nodeB.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException("Không có code Node RNC BSC trong file template");
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();

            for (String nodeB : multimapParam.keySet()) {
                boolean checkCreateMop = true;
                List<StationResult> lstStationResult = new ArrayList<>();
                try {
                    HashMap<String, String> mapParam = new HashMap<>();
                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                for (String paramCode : mapInBasicDynaBean.keySet()) {
                                    if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                        mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    } else {
                                        mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    }
                                }
                            } catch (Exception ex) {
                                logger.error(ex.getMessage(), ex);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }

                    calculateParamDetail(mapParam, lstParamDefault, LOGGER);
                    StationResult stationResult;
                    for (String paramCode : mapParam.keySet()) {
//                        if (mapParam.get(paramCode) == null || mapParam.get(paramCode).equals("") || mapParam.get(paramCode).contains(";;")) {
//                            checkCreateMop = false;
//                            stationResult = new StationResult();
//                            stationResult.setParamCode(paramCode);
//                            stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                            stationResult.setNodeCode(nodeB);
//                            stationResult.setResult("NOK, Khong sinh mop");
//                            stationResult.setType("Relation");
//                            stationResult.setDetailResult("Khong du tham so trong file import");
//                            lstStationResult.add(stationResult);
//                        } else {
                        mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
//                        }
                    }
                    if (checkCreateMop) {
                        mapNodeBParam.put(nodeB, mapParam);
                    }
                    if (!lstStationResult.isEmpty()) {
                        if (mapStationResult.containsKey(nodeB)) {
                            mapStationResult.get(nodeB).addAll(lstStationResult);
                        } else {
                            mapStationResult.put(nodeB, lstStationResult);
                        }
                    }

                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database

            //Sinh mop RNC, BSC
            createMopRelationCell(mapNodeBParam, stationConfigImports);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    protected boolean createFlowRunActionRelationCell2G(HashMap<String, HashMap<String, String>> mapNodeParam, FlowTemplates flowTemplates) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        for (String rncBsc : mapNodeParam.keySet()) {
            try {
                String filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                        + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                        + "-" + currStationPlan.getNetworkType().toUpperCase()
                        + "-RNC_BSC_" + rncBsc.toUpperCase()
                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                generateFlowRunController = new GenerateFlowRunController();
                FlowRunAction flowRunAction = new FlowRunAction();
                flowRunAction.setCrNumber(Config.CR_DEFAULT);
                flowRunAction.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang 
                generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", rncBsc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException("Khong tim thay node RNC/BSC hoac tim thay lon hon 1 ban ghi " + rncBsc);
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }

                mapParamValues = mapNodeParam.get(rncBsc);
                mapParamValues.put("file_name", filePutOSS);

                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                nodeInPlan.add(nodeRncBsc);

//                setParamFromTableOther(plan, mapParamValues, riResult, uniqueParams, mopType);
                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }

                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                    if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                .replace("{1}", paramValue.getParamCode()));
//                        return false;
//                    }
                    }
//                    Multimap<Long, ParamValue> mapParamValueGroup = ArrayListMultimap.create();
//                    mapParamValueGroup= generateFlowRunController.mapParamValueGroup.get(node);
//                    for(Long )
                }
                mapParamValues.clear();
//                if (currStationRelationPlan.getId() == null) {
//                    MessageUtil.setErrorMessage("Khong tim thay du lieu tích hợp trạm");
//                    return false;
//                }
                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file

                        if ("RNC".equals(nodeRncBsc.getNodeType().getTypeName()) && "ERICSSON".equals(nodeRncBsc.getVendor().getVendorName().toUpperCase())) {
                            StringBuilder stringBuilder = new StringBuilder();
//                            
                            generateFlowRunController.setSelectedNode(nodeRncBsc);
                            generateFlowRunController.loadMultiParam();
                            for (GroupAction groupAction : nodeRncBsc.getMapGroupActions().get(Config.SUB_FLOW_RUN_DEFAULT + "#" + nodeRncBsc.getNodeCode())) {
                                if ("Sinh dữ liệu Relation trên RNC".equals(groupAction.getGroupActionName())) {
                                    for (ActionOfFlow actionOfFlow : groupAction.getActionOfFlows()) {
                                        for (ActionDetail actionDetail : actionOfFlow.getAction().getActionDetails()) {
                                            if (nodeRncBsc.getVendor().equals(actionDetail.getVendor())
                                                    && nodeRncBsc.getNodeType().equals(actionDetail.getNodeType())
                                                    && nodeRncBsc.getVersion().equals(actionDetail.getVersion())) {
                                                for (ActionCommand actionCommand : actionDetail.getActionCommands()) {
                                                    String cmd = actionCommand.getCommandDetail().buildCommandFullValue(generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeRncBsc), false, actionOfFlow, actionCommand);
                                                    if (cmd != null && !cmd.isEmpty()) {
                                                        if (actionCommand.getType() == Config.EXECUTE_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        } else if (actionCommand.getType() == Config.ROLLBACK_CMD.longValue()) {
                                                            stringBuilder.append(cmd);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    groupAction.setDeclare(false);
                                    File files = createFilePutOSS(filePutOSS, stringBuilder);

                                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                                    stationFlowRunAction.setFileName(files.getName());
                                    stationFlowRunAction.setFileContent(readBytesFromFile(files));
                                    stationFlowRunAction.setFlowRunAction(flowRunAction);
                                    stationFlowRunAction.setNode(nodeRncBsc);
                                    stationFlowRunAction.setFilePath(files.getPath());
                                    stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                                    stationFlowRunAction.setStationPlan(currStationRelationPlan);
                                    stationFlowRunAction.setCommandPutFile("Put file OSS for RNC Ericsson");
                                    stationFlowRunAction.setType(2L);
                                    stationFlowRunAction.setUpdateTime(new Date());
                                    stationFlowRunActiones.add(stationFlowRunAction);
                                    break;
                                }
                            }
                        } else {
                            StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                            stationFlowRunAction.setFlowRunAction(flowRunAction);
                            stationFlowRunAction.setNode(nodeRncBsc);
                            stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                            stationFlowRunAction.setStationPlan(currStationRelationPlan);
                            stationFlowRunAction.setType(2L);
                            stationFlowRunAction.setUpdateTime(new Date());
                            stationFlowRunActiones.add(stationFlowRunAction);
                        }
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), rncBsc));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao 

                }
//                return saveDT;
            } catch (Exception e) {
                if (e instanceof MessageException) {
//                    MessageUtil.setErrorMessage(e.getMessage());
                    throw e;
                } else {
//                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }
//                return false;
//            throw e;

            } finally {
                //Tam biet
            }
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete_3G">
    public void analyseDataDelete3G(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key node
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();

            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object node;
                        Object deleteType;
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").toLowerCase());
                        } else {
                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                        }
                        deleteType = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.delete.type").toLowerCase());
                        if (node != null && deleteType != null) {
                            multimapParam.put((node.toString() + "#" + deleteType.toString()), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeParam = new HashMap<>();
            StationDetail stationDetail;
            HashMap<String, List<StationDetail>> mapBcfStationDetail = new HashMap<>();
            Map<String, String> mapIubBcf = new HashMap<>();
            for (String node : multimapParam.keySet()) {
                try {

                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, String> mapParam = new HashMap<>();
                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(node);
                    Object iub_bcf;

                    Multimap<String, BasicDynaBean> multimapParamTemp = ArrayListMultimap.create();
                    List<StationDetail> stationDetails = new ArrayList<>();
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            //sap xep xoa theo bcf/iub
                            try {
                                if ("3G".equals(currStationPlan.getNetworkType())) {
                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase());
                                } else {
                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
                                }
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BCF")));
                            }
                            if (iub_bcf != null) {
                                multimapParamTemp.put(iub_bcf.toString(), basicDynaBean);
                            }
                        }
                        for (String bcfTemp : multimapParamTemp.keySet()) {
                            if (mapIubBcf.containsKey(bcfTemp)) {
                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.iub_bcf.exits.2.type.delete"), bcfTemp));
                            }
                            mapIubBcf.put(bcfTemp, bcfTemp);
                            Collection<BasicDynaBean> basicDynaBeansBcf = multimapParamTemp.get(bcfTemp);
                            for (BasicDynaBean basicDynaBean : basicDynaBeansBcf) {
                                Map<String, String> mapInBasicDynaBean;
                                try {
                                    mapInBasicDynaBean = basicDynaBean.getMap();
                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            //Luu du lieu vao bang detail
                            stationDetail = new StationDetail();
                            stationDetail.setStationCode(bcfTemp);
                            stationDetails.add(stationDetail);

                        }

                    } catch (Exception e) {
                        throw e;
                    }

                    //Xet lai gia tri bcf
                    Map<String, List<String>> mapBcf = new HashMap<>();
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase())
                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                            String[] bcfs = mapParam.get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase()).split(";");
                            String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                            String bcfFinal = null;
                            String cellAll = null;
                            for (int i = 0; i < bcfs.length; i++) {
                                if (cells[i] != null) {
                                    if (mapBcf.containsKey(bcfs[i])) {
                                        mapBcf.get(bcfs[i]).add(cells[i]);
                                    } else {
                                        List<String> lstCell = new ArrayList<>();
                                        lstCell.add(cells[i]);
                                        mapBcf.put(bcfs[i], lstCell);
                                    }

                                }
                            }
                            for (String bcf : mapBcf.keySet()) {
                                Collections.sort(mapBcf.get(bcf));
                                if (bcfFinal == null) {
                                    bcfFinal = bcf;
                                    cellAll = StringUtils.join(mapBcf.get(bcf), "-");
                                } else {
                                    bcfFinal = bcfFinal + ";" + bcf;
                                    cellAll = cellAll + ";" + StringUtils.join(mapBcf.get(bcf), "-");
                                }
                            }
                            mapParam.put("cell_all", cellAll);
                            mapParam.put("iub", bcfFinal);
                        }
                    } else {
                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase())
                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                            String[] bcfs = mapParam.get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase()).split(";");
                            String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                            String bcfFinal = null;
                            String cellAll = null;
                            for (int i = 0; i < bcfs.length; i++) {
                                if (cells[i] != null) {
                                    if (mapBcf.containsKey(bcfs[i])) {
                                        mapBcf.get(bcfs[i]).add(cells[i]);
                                    } else {
                                        List<String> lstCell = new ArrayList<>();
                                        lstCell.add(cells[i]);
                                        mapBcf.put(bcfs[i], lstCell);
                                    }

                                }
                            }
                            for (String bcf : mapBcf.keySet()) {
                                Collections.sort(mapBcf.get(bcf));
                                if (bcfFinal == null) {
                                    bcfFinal = bcf;
                                    cellAll = StringUtils.join(mapBcf.get(bcf), "-");
                                } else {
                                    bcfFinal = bcfFinal + ";" + bcf;
                                    cellAll = cellAll + ";" + StringUtils.join(mapBcf.get(bcf), "-");
                                }
                            }
                            mapParam.put("cell_all", cellAll);
                            mapParam.put("bcf", bcfFinal);
                        }
                    }
                    if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
                            && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase())) {
                        String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
                        String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase()).split(";");
                        String cell_lac_ci = "";
                        for (int i = 0; i < lacs.length; i++) {
                            if (cis[i] != null) {
                                cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
                            }
                        }

                        mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
                    }
                    mapParam.put("cell_check", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
                    mapParam.put("cell_msc", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
                    //Luu log import
                    StationResult stationResult;
                    for (String paramCode : mapParam.keySet()) {
                        if (mapParam.get(paramCode) == null
                                || "".equals(mapParam.get(paramCode))
                                || mapParam.get(paramCode).startsWith(";")
                                || mapParam.get(paramCode).contains(";;")) {
                            checkCreateMop = false;
                            stationResult = new StationResult();
                            stationResult.setParamCode(paramCode);
                            stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
                            stationResult.setNodeB(node);
                            stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                            stationResult.setType("Integrate");
                            stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), paramCode));
                            lstStationResult.add(stationResult);
                        } else {
                            mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
                        }
                    }

                    if (checkCreateMop) {
                        mapNodeParam.put(node, mapParam);
                        mapBcfStationDetail.put(node, stationDetails);
                    } else {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"), node));
                        if (!lstStationResult.isEmpty()) {
                            if (mapStationResult.containsKey(node)) {
                                mapStationResult.get(node).addAll(lstStationResult);
                            } else {
                                mapStationResult.put(node, lstStationResult);
                            }
                        }
                    }

                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
            currStationPlan.setProficientType(2L);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> lstInsertDetails = new ArrayList<>();
            List<StationDetail> stationDetailsTemp;
            StationResult stationResult;
            Map<String, Object> filters = new HashMap<>();
            for (String node : mapNodeParam.keySet()) {
                stationDetailsTemp = mapBcfStationDetail.get(node);
                String[] node_deleteType = node.split("#");
                String nodeRncBsc = node_deleteType[0];
                mapNodeRncBsc.put(nodeRncBsc, nodeRncBsc);
                List<StationResult> lstStationResult = new ArrayList<>();

                if (mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
                        || mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {

                    filters.put("rncBsc", nodeRncBsc);
                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);

                    if (lstResource.isEmpty()) {
                        stationResult = new StationResult();
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim());
                        } else {
                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.BSC").trim());
                        }

                        stationResult.setParamValue(nodeRncBsc);
                        stationResult.setNodeB(node);
                        stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                        stationResult.setType("Integrate");
                        stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), nodeRncBsc));
                        lstStationResult.add(stationResult);
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                nodeRncBsc));
                        mapNodeParam.remove(node);
//                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
                    } else {
                        mapNodeMsc.put(node, lstResource.get(0).getMsc());
                        for (StationDetail sd : stationDetailsTemp) {
                            sd.setStationResource(lstResource.get(0));
                            sd.setRncBsc(nodeRncBsc);
                            if ("3G".equals(currStationPlan.getNetworkType())) {
                                sd.setCellCode(mapNodeParam.get(node).get("userlabel"));
                            } else {
                                sd.setCellCode(mapNodeParam.get(node).get("cell"));
                            }
                            sd.setUpdateTime(new Date());
                            sd.setStationPlan(currStationPlan);
                        }
                        lstInsertDetails.addAll(stationDetailsTemp);
                    }

                } else {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                }
                if (!lstStationResult.isEmpty()) {
                    if (mapStationResult.containsKey(node)) {
                        mapStationResult.get(node).addAll(lstStationResult);
                    } else {
                        mapStationResult.put(node, lstStationResult);
                    }
                }
            }
            stationPlanService.saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(lstInsertDetails);
            //Sinh mop RNC, BSC
            createMopDelete3G(mapNodeParam, stationConfigImports, mapNodeRncBsc, mapNodeMsc);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void createMopDelete3G(HashMap<String, HashMap<String, String>> mapNodeParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        createFlowRunActionDelete3G(mapNodeParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
    }

    protected boolean createFlowRunActionDelete3G(HashMap<String, HashMap<String, String>> mapNodeParam, HashMap<String, String> mapNodeRncBsc, HashMap<String, String> mapNodeMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        String deleteType;
        String nodeCodeRncBsc;
        for (String node : mapNodeParam.keySet()) {
            String[] node_DeleteType = node.split("#");
            nodeCodeRncBsc = node_DeleteType[0];
            deleteType = node_DeleteType[1];

            if (deleteType != null) {
                for (StationConfigImport sci : stationConfigImports) {
                    if (deleteType.equalsIgnoreCase(sci.getDeleteType())) {
                        flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
                        break;
                    }
                }
            } else {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
            }

            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                continue;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-RNC_" + nodeCodeRncBsc.toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BSC_" + nodeCodeRncBsc.toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                FlowRunAction flowRunAction = new FlowRunAction();
                flowRunAction.setCrNumber(Config.CR_DEFAULT);
                flowRunAction.setFlowRunName(filePutOSS);
//                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang 
                generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", nodeCodeRncBsc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                Node nodeMsc;
                List<Node> lstnodeMsc = new ArrayList<>();
                filters.clear();
                String[] mscStr = mapNodeMsc.get(node).split(";");
                for (String msc : mscStr) {
                    filters.clear();
                    filters.put("nodeCode-EXAC", msc);
                    List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
                    if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
                    } else {
                        nodeMsc = nodeMscs.get(0);
                        lstnodeMsc.add(nodeMsc);
                    }
                }
                mapParamValues = mapNodeParam.get(node);

                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                for (Node nodeTemp : lstnodeMsc) {
                    generateFlowRunController.getNodes().add(nodeTemp);
                    nodeInPlan.add(nodeTemp);
                }
                nodeInPlan.add(nodeRncBsc);
                if (deleteType != null && deleteType.toUpperCase().equals(MessageUtil.getResourceBundleMessage("label.station.delete.type0").toUpperCase())) {
                    mapParamValues.put(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase(), null);
                }
                for (Node nodeTemp : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, nodeTemp);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeTemp);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                        if (currStationPlan.getNetworkType().equals("3G")) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", nodeTemp.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }
                    }
                }
                mapParamValues.clear();

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file

                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                        stationFlowRunAction.setFlowRunAction(flowRunAction);
                        stationFlowRunAction.setNode(nodeRncBsc);
                        stationFlowRunAction.setStationPlan(currStationPlan);
                        stationFlowRunAction.setType(1L);
                        stationFlowRunAction.setUpdateTime(new Date());
                        stationFlowRunActiones.add(stationFlowRunAction);
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeCodeRncBsc));

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao 

                }
//                return saveDT;
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
//                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }

//                return false;
//            throw e;
            } finally {
                //Tam biet
            }
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete_2G">
    public void analyseDataDelete2G(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key node
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();

            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object node;
                        Object deleteType;
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").toLowerCase());
                        } else {
                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                        }
                        deleteType = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.delete.type").toLowerCase());
                        if (node != null && deleteType != null) {
                            multimapParam.put((node.toString() + "#" + deleteType.toString()), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, HashMap<String, String>>> mapNodeParam = new HashMap<>();
            Map<String, String> mapIubBcf = new HashMap<>();
            for (String node : multimapParam.keySet()) {
                try {

                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, HashMap<String, String>> mapSubNodeParam = new HashMap<>();

                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(node);
                    Object iub_bcf;

                    Multimap<String, BasicDynaBean> multimapParamTemp = ArrayListMultimap.create();
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            //sap xep xoa theo bcf/iub
                            try {
                                if ("3G".equals(currStationPlan.getNetworkType())) {
                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase());
                                } else {
                                    if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
                                        iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
                                    } else {
                                        iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.TG_NUMBER").toLowerCase());
                                    }
                                }
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);

                                if ("3G".equals(currStationPlan.getNetworkType())) {
                                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.iub")));
                                } else {
                                    if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
                                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BCF")));
                                    } else {
                                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.TG_NUMBER")));
                                    }
                                }
                            }
                            if (iub_bcf != null) {
                                multimapParamTemp.put(iub_bcf.toString(), basicDynaBean);
                            }
                        }
                        for (String bcfTemp : multimapParamTemp.keySet()) {
                            HashMap<String, String> mapParam = new HashMap<>();
                            if (mapIubBcf.containsKey(bcfTemp)) {
                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.iub_bcf.exits.2.type.delete"), bcfTemp));
                            }
                            mapIubBcf.put(bcfTemp, bcfTemp);
                            Collection<BasicDynaBean> basicDynaBeansBcf = multimapParamTemp.get(bcfTemp);
                            for (BasicDynaBean basicDynaBean : basicDynaBeansBcf) {
                                Map<String, String> mapInBasicDynaBean;
                                try {
                                    mapInBasicDynaBean = basicDynaBean.getMap();
                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            //Luu du lieu vao bang detail
                            mapSubNodeParam.put(bcfTemp, mapParam);
                        }

                    } catch (Exception e) {
                        throw e;
                    }
                    for (String nodeSub : mapSubNodeParam.keySet()) {
                        HashMap<String, String> mapParam = mapSubNodeParam.get(nodeSub);
                        //Xet lai gia tri bcf
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase())
                                    && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                                String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                                String cellAll = "";
                                for (String cell : cells) {
                                    cellAll = cellAll + "-" + cell;
                                }

                                mapParam.put("cell_all", cellAll.substring(1));
                                mapParam.put("iub", nodeSub);
                            }
                        } else {
                            if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
                                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase())
                                        && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                                    String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                                    String cellAll = "";
                                    for (String cell : cells) {
                                        cellAll = cellAll + "-" + cell;
                                    }

                                    mapParam.put("cell_all", cellAll.substring(1));
                                    mapParam.put("bcf", nodeSub);
                                }
                            } else if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)) {
                                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.TG_NUMBER").toLowerCase())
                                        && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                                    String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                                    String cellAll = "";
                                    for (String cell : cells) {
                                        cellAll = cellAll + "-" + cell;
                                    }

                                    mapParam.put("cell_all", cellAll.substring(1));
                                    mapParam.put("tg_number", nodeSub);
                                }
                            }
                        }
                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase())) {
                            String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
                            String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase()).split(";");
                            String cell_lac_ci = "";
                            for (int i = 0; i < lacs.length; i++) {
                                if (cis[i] != null) {
                                    cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
                                }
                            }

                            mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
                        }
                        mapParam.put("cell_check", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
                        mapParam.put("cell_msc", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
                        //Luu log import
                        StationResult stationResult;
                        for (String paramCode : mapParam.keySet()) {
                            if (mapParam.get(paramCode) == null
                                    || "".equals(mapParam.get(paramCode))
                                    || mapParam.get(paramCode).startsWith(";")
                                    || mapParam.get(paramCode).contains(";;")) {
                                checkCreateMop = false;
                                stationResult = new StationResult();
                                stationResult.setParamCode(paramCode);
                                stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
                                stationResult.setNodeB(node);
                                stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                                stationResult.setType("Integrate");
                                stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), paramCode));
                                lstStationResult.add(stationResult);
                            } else {
                                mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
                            }
                        }

                        if (checkCreateMop) {
                            mapNodeParam.put(node, mapSubNodeParam);
                        } else {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"), node));
                            if (!lstStationResult.isEmpty()) {
                                if (mapStationResult.containsKey(node)) {
                                    mapStationResult.get(node).addAll(lstStationResult);
                                } else {
                                    mapStationResult.put(node, lstStationResult);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
            currStationPlan.setProficientType(2L);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> lstInsertDetails = new ArrayList<>();
            StationResult stationResult;
            Map<String, Object> filters = new HashMap<>();
            for (String node : mapNodeParam.keySet()) {
                String[] node_deleteType = node.split("#");
                String nodeRncBsc = node_deleteType[0];
                mapNodeRncBsc.put(nodeRncBsc, nodeRncBsc);

                List<StationResult> lstStationResult = new ArrayList<>();
                for (String subNode : mapNodeParam.get(node).keySet()) {
                    if (mapNodeParam.get(node).get(subNode).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
                            || mapNodeParam.get(node).get(subNode).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {

                        filters.put("rncBsc", nodeRncBsc);
                        List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);

                        if (lstResource.isEmpty()) {
                            stationResult = new StationResult();
                            if ("3G".equals(currStationPlan.getNetworkType())) {
                                stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim());
                            } else {
                                stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.BSC").trim());
                            }

                            stationResult.setParamValue(nodeRncBsc);
                            stationResult.setNodeB(subNode);
                            stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                            stationResult.setType("Integrate");
                            stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), nodeRncBsc));
                            lstStationResult.add(stationResult);
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                    nodeRncBsc));
                            mapNodeParam.get(node).remove(subNode);
//                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
                        } else {
                            mapNodeMsc.put(node, lstResource.get(0).getMsc());
                            StationDetail sd = new StationDetail();
                            sd.setStationResource(lstResource.get(0));
                            sd.setRncBsc(nodeRncBsc);
                            sd.setStationCode(subNode);
                            if ("3G".equals(currStationPlan.getNetworkType())) {
                                sd.setCellCode(mapNodeParam.get(node).get(subNode).get("userlabel"));
                            } else {
                                sd.setCellCode(mapNodeParam.get(node).get(subNode).get("cell"));
                            }
                            sd.setUpdateTime(new Date());
                            sd.setStationPlan(currStationPlan);
                            lstInsertDetails.add(sd);

                        }
                    } else {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    }
                    if (!lstStationResult.isEmpty()) {
                        if (mapStationResult.containsKey(node)) {
                            mapStationResult.get(node).addAll(lstStationResult);
                        } else {
                            mapStationResult.put(node, lstStationResult);
                        }

                    }
                }
            }
            stationPlanService.saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(lstInsertDetails);
            //Sinh mop RNC, BSC
            createMopDelete2G(mapNodeParam, stationConfigImports, mapNodeRncBsc, mapNodeMsc);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void createMopDelete2G(HashMap<String, HashMap<String, HashMap<String, String>>> mapNodeParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        createFlowRunActionDelete2G(mapNodeParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
    }

    protected boolean createFlowRunActionDelete2G(HashMap<String, HashMap<String, HashMap<String, String>>> mapNodeParam, HashMap<String, String> mapNodeRncBsc, HashMap<String, String> mapNodeMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        String deleteType;
        String nodeCodeRncBsc;
        for (String node : mapNodeParam.keySet()) {
            String[] node_DeleteType = node.split("#");
            nodeCodeRncBsc = node_DeleteType[0];
            deleteType = node_DeleteType[1];
            List<Node> nodeInPlan;
            HashMap<String, Object> filters = new HashMap<>();
            filters.put("nodeCode-EXAC", nodeCodeRncBsc);
            List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
            Node nodeRncBsc;
            if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
            } else {
                nodeRncBsc = nodeRncs.get(0);
            }
            Node nodeMsc;
            List<Node> lstnodeMsc = new ArrayList<>();
            filters.clear();
            String[] mscStr = mapNodeMsc.get(node).split(";");
            for (String msc : mscStr) {
                filters.clear();
                filters.put("nodeCode-EXAC", msc);
                List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
                if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
                } else {
                    nodeMsc = nodeMscs.get(0);
                    lstnodeMsc.add(nodeMsc);
                }
            }
            for (String subNode : mapNodeParam.get(node).keySet()) {
                if (deleteType != null) {
                    for (StationConfigImport sci : stationConfigImports) {
                        if (deleteType.equalsIgnoreCase(sci.getDeleteType())) {
                            flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
                            break;
                        }
                    }
                } else {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                }

                if (flowTemplates == null) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                    continue;
                } else {
                    if (flowTemplates.getStatus() != 9) {
                        MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                        continue;
                    }
                }

                try {
                    String filePutOSS;
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                                + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                                + "-" + currStationPlan.getNetworkType().toUpperCase()
                                + "-IUB_" + subNode
                                + "-RNC_" + nodeCodeRncBsc.toUpperCase()
                                + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    } else {
                        filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                                + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                                + "-" + currStationPlan.getNetworkType().toUpperCase()
                                + "-BCF_" + subNode
                                + "-BSC_" + nodeCodeRncBsc.toUpperCase()
                                + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                    }
                    generateFlowRunController = new GenerateFlowRunController();
                    FlowRunAction flowRunAction = new FlowRunAction();
                    flowRunAction.setCrNumber(Config.CR_DEFAULT);
                    flowRunAction.setFlowRunName(filePutOSS);
//                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                    while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                        flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                    }
                    flowRunAction.setTimeRun(new Date());
                    flowRunAction.setFlowTemplates(flowTemplates);
                    flowRunAction.setExecuteBy(userCreate);
                    flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

                    generateFlowRunController.setFlowRunAction(flowRunAction);
                    generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                    //Lay danh sach param tu bang 
                    generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
                    Map<String, String> mapParamValues;
                    generateFlowRunController.loadGroupAction(0l);
                    mapParamValues = mapNodeParam.get(node).get(subNode);
                    //Tham so theo tung subnode
                    nodeInPlan = new ArrayList<>();
                    generateFlowRunController.getNodes().add(nodeRncBsc);
                    for (Node nodeTemp : lstnodeMsc) {
                        generateFlowRunController.getNodes().add(nodeTemp);
                        nodeInPlan.add(nodeTemp);
                    }
                    nodeInPlan.add(nodeRncBsc);
//                    if (deleteType != null && deleteType.toUpperCase().equals(MessageUtil.getResourceBundleMessage("label.station.delete.type0").toUpperCase())) {
//                        mapParamValues.put(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase(), null);
//                    }
                    for (Node nodeTemp : nodeInPlan) {
                        generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, nodeTemp);
                        List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, nodeTemp);
                        for (ParamValue paramValue : paramValues) {
                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }
                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                            if (value != null) {
                                paramValue.setParamValue(value.toString());
                            }
//                        if (currStationPlan.getNetworkType().equals("3G")) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", nodeTemp.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }
                        }
                    }
                    mapParamValues.clear();

                    boolean saveDT = generateFlowRunController.saveDT();
                    if (saveDT) {
                        try {
                            //Bat dau xu ly day file OSS
                            //Tao file

                            StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                            stationFlowRunAction.setFlowRunAction(flowRunAction);
                            stationFlowRunAction.setNode(nodeRncBsc);
                            stationFlowRunAction.setStationPlan(currStationPlan);
                            stationFlowRunAction.setType(1L);
                            stationFlowRunAction.setUpdateTime(new Date());
                            stationFlowRunActiones.add(stationFlowRunAction);
                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeCodeRncBsc));

                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        //Xu ly day du lieu vao 

                    }

//                return saveDT;
                } catch (Exception e) {
                    if (e instanceof MessageException) {
                        throw e;
                    } else {
//                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                    }

//                return false;
//            throw e;
                } finally {
                    //Tam biet
                }
            }
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete">
//    public void analyseDataDelete(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
//        try {
//            //Xy ly du lieu file
//            //Xu ly day tat ca cac du lieu vao map voi key node
//            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
//
//            for (String sheetName : mapCellObjectImports.keySet()) {
//                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
//
//                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
//                    try {
//                        Object node;
//                        Object deleteType;
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").toLowerCase());
//                        } else {
//                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
//                        }
//                        deleteType = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.delete.type").toLowerCase());
//                        if (node != null && deleteType != null) {
//                            multimapParam.put((node.toString() + "#" + deleteType.toString()), basicDynaBean);
//                        }
//
//                    } catch (Exception e) {
//                        LOGGER.error(e.getMessage(), e);
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                    }
//                }
//            }
//            //xu ly map
//            HashMap<String, HashMap<String, String>> mapNodeParam = new HashMap<>();
//            StationDetail stationDetail;
//            HashMap<String, List<StationDetail>> mapBcfStationDetail = new HashMap<>();
//            Map<String, String> mapIubBcf = new HashMap<>();
//            for (String node : multimapParam.keySet()) {
//                try {
//
//                    boolean checkCreateMop = true;
//                    List<StationResult> lstStationResult = new ArrayList<>();
//                    HashMap<String, String> mapParam = new HashMap<>();
//                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(node);
//                    Object iub_bcf;
//
//                    Multimap<String, BasicDynaBean> multimapParamTemp = ArrayListMultimap.create();
//                    List<StationDetail> stationDetails = new ArrayList<>();
//                    try {
//                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
//                            //sap xep xoa theo bcf/iub
//                            try {
//                                if ("3G".equals(currStationPlan.getNetworkType())) {
//                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase());
//                                } else {
//                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
//                                }
//                            } catch (Exception e) {
//                                LOGGER.error(e.getMessage(), e);
//                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BCF")));
//                            }
//                            if (iub_bcf != null) {
//                                multimapParamTemp.put(iub_bcf.toString(), basicDynaBean);
//                            }
//                        }
//                        for (String bcfTemp : multimapParamTemp.keySet()) {
//                            if (mapIubBcf.containsKey(bcfTemp)) {
//                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.iub_bcf.exits.2.type.delete"), bcfTemp));
//                            }
//                            mapIubBcf.put(bcfTemp, bcfTemp);
//                            Collection<BasicDynaBean> basicDynaBeansBcf = multimapParamTemp.get(bcfTemp);
//                            for (BasicDynaBean basicDynaBean : basicDynaBeansBcf) {
//                                Map<String, String> mapInBasicDynaBean;
//                                try {
//                                    mapInBasicDynaBean = basicDynaBean.getMap();
//                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
//                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
//                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                        } else {
//                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                        }
//                                    }
//                                } catch (Exception ex) {
//                                    logger.error(ex.getMessage(), ex);
//                                }
//                            }
//                            //Luu du lieu vao bang detail
//                            stationDetail = new StationDetail();
//                            stationDetail.setStationCode(bcfTemp);
//                            stationDetails.add(stationDetail);
//
//                        }
//
//                    } catch (Exception e) {
//                        throw e;
//                    }
//
//                    //Xet lai gia tri bcf
//                    Map<String, List<String>> mapBcf = new HashMap<>();
//                    if ("3G".equals(currStationPlan.getNetworkType())) {
//                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase())
//                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
//                            String[] bcfs = mapParam.get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase()).split(";");
//                            String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
//                            String bcfFinal = null;
//                            String cellAll = null;
//                            for (int i = 0; i < bcfs.length; i++) {
//                                if (cells[i] != null) {
//                                    if (mapBcf.containsKey(bcfs[i])) {
//                                        mapBcf.get(bcfs[i]).add(cells[i]);
//                                    } else {
//                                        List<String> lstCell = new ArrayList<>();
//                                        lstCell.add(cells[i]);
//                                        mapBcf.put(bcfs[i], lstCell);
//                                    }
//
//                                }
//                            }
//                            for (String bcf : mapBcf.keySet()) {
//                                Collections.sort(mapBcf.get(bcf));
//                                if (bcfFinal == null) {
//                                    bcfFinal = bcf;
//                                    cellAll = StringUtils.join(mapBcf.get(bcf), "-");
//                                } else {
//                                    bcfFinal = bcfFinal + ";" + bcf;
//                                    cellAll = cellAll + ";" + StringUtils.join(mapBcf.get(bcf), "-");
//                                }
//                            }
//                            mapParam.put("cell_all", cellAll);
//                            mapParam.put("iub", bcfFinal);
//                        }
//                    } else {
//                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase())
//                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
//                            String[] bcfs = mapParam.get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase()).split(";");
//                            String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
//                            String bcfFinal = null;
//                            String cellAll = null;
//                            for (int i = 0; i < bcfs.length; i++) {
//                                if (cells[i] != null) {
//                                    if (mapBcf.containsKey(bcfs[i])) {
//                                        mapBcf.get(bcfs[i]).add(cells[i]);
//                                    } else {
//                                        List<String> lstCell = new ArrayList<>();
//                                        lstCell.add(cells[i]);
//                                        mapBcf.put(bcfs[i], lstCell);
//                                    }
//
//                                }
//                            }
//                            for (String bcf : mapBcf.keySet()) {
//                                Collections.sort(mapBcf.get(bcf));
//                                if (bcfFinal == null) {
//                                    bcfFinal = bcf;
//                                    cellAll = StringUtils.join(mapBcf.get(bcf), "-");
//                                } else {
//                                    bcfFinal = bcfFinal + ";" + bcf;
//                                    cellAll = cellAll + ";" + StringUtils.join(mapBcf.get(bcf), "-");
//                                }
//                            }
//                            mapParam.put("cell_all", cellAll);
//                            mapParam.put("bcf", bcfFinal);
//                        }
//                    }
//                    if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
//                            && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase())) {
//                        String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
//                        String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase()).split(";");
//                        String cell_lac_ci = "";
//                        for (int i = 0; i < lacs.length; i++) {
//                            if (cis[i] != null) {
//                                cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
//                            }
//                        }
//
//                        mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
//                    }
//                    mapParam.put("cell_check", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
//                    mapParam.put("cell_msc", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
//                    //Luu log import
//                    StationResult stationResult;
//                    for (String paramCode : mapParam.keySet()) {
//                        if (mapParam.get(paramCode) == null
//                                || "".equals(mapParam.get(paramCode))
//                                || mapParam.get(paramCode).startsWith(";")
//                                || mapParam.get(paramCode).contains(";;")) {
//                            checkCreateMop = false;
//                            stationResult = new StationResult();
//                            stationResult.setParamCode(paramCode);
//                            stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                            stationResult.setNodeB(node);
//                            stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
//                            stationResult.setType("Integrate");
//                            stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), paramCode));
//                            lstStationResult.add(stationResult);
//                        } else {
//                            mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
//                        }
//                    }
//
//                    if (checkCreateMop) {
//                        mapNodeParam.put(node, mapParam);
//                        mapBcfStationDetail.put(node, stationDetails);
//                    } else {
//                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"), node));
//                        if (!lstStationResult.isEmpty()) {
//                            if (mapStationResult.containsKey(node)) {
//                                mapStationResult.get(node).addAll(lstStationResult);
//                            } else {
//                                mapStationResult.put(node, lstStationResult);
//                            }
//                        }
//                    }
//
//                } catch (Exception e) {
//                    throw e;
//                }
//            }
//            //Luu database
//            //Insert vao DB
//            HashMap<String, String> mapNodeRncBsc = new HashMap<>();
//            HashMap<String, String> mapNodeMsc = new HashMap<>();
//            currStationPlan.setUpdateTime(new Date());
//            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
//            currStationPlan.setProficientType(2L);
//
//            //Insert tram vao DB
//            List<StationDetail> lstInsertDetails = new ArrayList<>();
//            List<StationDetail> stationDetailsTemp;
//            StationResult stationResult;
//            Map<String, Object> filters = new HashMap<>();
//            for (String node : mapNodeParam.keySet()) {
//                stationDetailsTemp = mapBcfStationDetail.get(node);
//                String[] node_deleteType = node.split("#");
//                String nodeRncBsc = node_deleteType[0];
//                mapNodeRncBsc.put(nodeRncBsc, nodeRncBsc);
//                List<StationResult> lstStationResult = new ArrayList<>();
//
//                if (mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
//                        || mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {
//
//                    filters.put("rncBsc", nodeRncBsc);
//                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
//
//                    if (lstResource.isEmpty()) {
//                        stationResult = new StationResult();
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim());
//                        } else {
//                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.BSC").trim());
//                        }
//
//                        stationResult.setParamValue(nodeRncBsc);
//                        stationResult.setNodeB(node);
//                        stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
//                        stationResult.setType("Integrate");
//                        stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), nodeRncBsc));
//                        lstStationResult.add(stationResult);
//                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
//                                nodeRncBsc));
//                        mapNodeParam.remove(node);
////                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
//                    } else {
//                        mapNodeMsc.put(node, lstResource.get(0).getMsc());
//                        for (StationDetail sd : stationDetailsTemp) {
//                            sd.setStationResource(lstResource.get(0));
//                            sd.setRncBsc(nodeRncBsc);
//                            if ("3G".equals(currStationPlan.getNetworkType())) {
//                                sd.setCellCode(mapNodeParam.get(node).get("userlabel"));
//                            } else {
//                                sd.setCellCode(mapNodeParam.get(node).get("cell"));
//                            }
//                            sd.setUpdateTime(new Date());
//                            sd.setStationPlan(currStationPlan);
//                        }
//                        lstInsertDetails.addAll(stationDetailsTemp);
//                    }
//
//                } else {
//                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                }
//                if (!lstStationResult.isEmpty()) {
//                    if (mapStationResult.containsKey(node)) {
//                        mapStationResult.get(node).addAll(lstStationResult);
//                    } else {
//                        mapStationResult.put(node, lstStationResult);
//                    }
//                }
//            }
//            stationPlanService.saveOrUpdate(currStationPlan);
//            new StationDetailServiceImpl().saveOrUpdate(lstInsertDetails);
//            //Sinh mop RNC, BSC
//            createMopDelete(mapNodeParam, stationConfigImports, mapNodeRncBsc, mapNodeMsc);
//
//        } catch (Exception ex) {
//
//            LOGGER.error(ex.getMessage());
//            throw ex;
//        }
//
//    }
//
//    public void createMopDelete(HashMap<String, HashMap<String, String>> mapNodeParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc) throws AppException, Exception {
//        //Sinh mop cho RNC, MSC
//        createFlowRunActionDelete(mapNodeParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
//    }
//    protected boolean createFlowRunActionDelete(HashMap<String, HashMap<String, String>> mapNodeParam, HashMap<String, String> mapNodeRncBsc, HashMap<String, String> mapNodeMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
//
//        GenerateFlowRunController generateFlowRunController;
//        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
//        FlowTemplates flowTemplates = null;
//        String deleteType;
//        String nodeCodeRncBsc;
//        for (String node : mapNodeParam.keySet()) {
//            String[] node_DeleteType = node.split("#");
//            nodeCodeRncBsc = node_DeleteType[0];
//            deleteType = node_DeleteType[1];
//
//            if (deleteType != null) {
//                for (StationConfigImport sci : stationConfigImports) {
//                    if (deleteType.equalsIgnoreCase(sci.getDeleteType())) {
//                        flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
//                        break;
//                    }
//                }
//            } else {
//                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//            }
//
//            if (flowTemplates == null) {
//                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//                continue;
//            } else {
//                if (flowTemplates.getStatus() != 9) {
//                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
//                    continue;
//                }
//            }
//
//            try {
//                String filePutOSS;
//                if ("3G".equals(currStationPlan.getNetworkType())) {
//                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
//                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
//                            + "-" + currStationPlan.getNetworkType().toUpperCase()
//                            + "-RNC_" + nodeCodeRncBsc.toUpperCase()
//                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//                } else {
//                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
//                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
//                            + "-" + currStationPlan.getNetworkType().toUpperCase()
//                            + "-BSC_" + nodeCodeRncBsc.toUpperCase()
//                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//                }
//                generateFlowRunController = new GenerateFlowRunController();
//                FlowRunAction flowRunAction = new FlowRunAction();
//                flowRunAction.setCrNumber(Config.CR_DEFAULT);
//                flowRunAction.setFlowRunName(filePutOSS);
////                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
//                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
//                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
//                }
//                flowRunAction.setTimeRun(new Date());
//                flowRunAction.setFlowTemplates(flowTemplates);
//                flowRunAction.setExecuteBy(userCreate);
////                if (mopType != null) {
////                    flowRunAction.setMopType(mopType);
////                } else {
////                    flowRunAction.setMopType(1l);
////                }
//
//                generateFlowRunController.setFlowRunAction(flowRunAction);
//                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
//                //Lay danh sach param tu bang 
//                generateFlowRunController.setNodes(new ArrayList<Node>());
////                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
//                Map<String, String> mapParamValues;
//                generateFlowRunController.loadGroupAction(0l);
//                List<Node> nodeInPlan;
//                HashMap<String, Object> filters = new HashMap<>();
//                filters.put("nodeCode-EXAC", nodeCodeRncBsc);
//                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
//                Node nodeRncBsc;
//                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
//                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                } else {
//                    nodeRncBsc = nodeRncs.get(0);
//                }
//                Node nodeMsc;
//                List<Node> lstnodeMsc = new ArrayList<>();
//                filters.clear();
//                String[] mscStr = mapNodeMsc.get(node).split(";");
//                for (String msc : mscStr) {
//                    filters.clear();
//                    filters.put("nodeCode-EXAC", msc);
//                    List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
//                    if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
//                    } else {
//                        nodeMsc = nodeMscs.get(0);
//                        lstnodeMsc.add(nodeMsc);
//                    }
//                }
//                mapParamValues = mapNodeParam.get(node);
//
//                nodeInPlan = new ArrayList<>();
//                generateFlowRunController.getNodes().add(nodeRncBsc);
//                for (Node nodeTemp : lstnodeMsc) {
//                    generateFlowRunController.getNodes().add(nodeTemp);
//                    nodeInPlan.add(nodeTemp);
//                }
//                nodeInPlan.add(nodeRncBsc);
//                if (deleteType != null && deleteType.toUpperCase().equals(MessageUtil.getResourceBundleMessage("label.station.delete.type0").toUpperCase())) {
//                    mapParamValues.put(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase(), null);
//                }
//                for (Node nodeTemp : nodeInPlan) {
//                    generateFlowRunController.loadGroupAction(nodeTemp);
//                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(nodeTemp);
//                    for (ParamValue paramValue : paramValues) {
//                        if (paramValue.getParamInput().getReadOnly()) {
//                            continue;
//                        }
//                        Object value = null;
//                        try {
//                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
//                        } catch (Exception e) {
//                            LOGGER.error(e.getMessage(), e);
//                        }
//                        if (value != null) {
//                            paramValue.setParamValue(value.toString());
//                        }
////                        if (currStationPlan.getNetworkType().equals("3G")) {
////                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
////                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", nodeTemp.getNodeCode())
////                                        .replace("{1}", paramValue.getParamCode()));
////                                return false;
////                            }
////                        }
//                    }
//                }
//                mapParamValues.clear();
//
//                boolean saveDT = generateFlowRunController.saveDT();
//                if (saveDT) {
//                    try {
//                        //Bat dau xu ly day file OSS
//                        //Tao file
//
//                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
//                        stationFlowRunAction.setFlowRunAction(flowRunAction);
//                        stationFlowRunAction.setNode(nodeRncBsc);
//                        stationFlowRunAction.setStationPlan(currStationPlan);
//                        stationFlowRunAction.setType(1L);
//                        stationFlowRunAction.setUpdateTime(new Date());
//                        stationFlowRunActiones.add(stationFlowRunAction);
//                        MessageUtil.setInfoMessageFromRes(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeCodeRncBsc));
//
//                    } catch (Exception e) {
//                        LOGGER.error(e.getMessage(), e);
//                    }
//                    //Xu ly day du lieu vao 
//
//                }
////                return saveDT;
//            } catch (Exception e) {
//                if (e instanceof MessageException) {
//                    throw e;
//                } else {
////                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
//                    LOGGER.error(e.getMessage(), e);
//                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
//                }
//
////                return false;
////            throw e;
//            } finally {
//                //Tam biet
//            }
//        }
//        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
//        return true;
//    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete">
//    public void analyseDataDelete(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
//        try {
//            //Xy ly du lieu file
//            //Xu ly day tat ca cac du lieu vao map voi key node
//            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
//
//            for (String sheetName : mapCellObjectImports.keySet()) {
//                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
//
//                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
//                    try {
//                        Object node;
//                        Object deleteType;
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").toLowerCase());
//                        } else {
//                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
//                        }
//                        deleteType = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.delete.type").toLowerCase());
//                        if (node != null && deleteType != null) {
//                            multimapParam.put((node.toString() + "#" + deleteType.toString()), basicDynaBean);
//                        }
//
//                    } catch (Exception e) {
//                        LOGGER.error(e.getMessage(), e);
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                    }
//                }
//            }
//            //xu ly map
//            HashMap<String, HashMap<String, String>> mapNodeParam = new HashMap<>();
//            StationDetail stationDetail;
//            HashMap<String, List<StationDetail>> mapBcfStationDetail = new HashMap<>();
//            Map<String, String> mapIubBcf = new HashMap<>();
//            for (String node : multimapParam.keySet()) {
//                try {
//
//                    boolean checkCreateMop = true;
//                    List<StationResult> lstStationResult = new ArrayList<>();
//                    HashMap<String, String> mapParam = new HashMap<>();
//                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(node);
//                    Object iub_bcf;
//
//                    Multimap<String, BasicDynaBean> multimapParamTemp = ArrayListMultimap.create();
//                    List<StationDetail> stationDetails = new ArrayList<>();
//                    try {
//                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
//                            //sap xep xoa theo bcf/iub
//                            try {
//                                if ("3G".equals(currStationPlan.getNetworkType())) {
//                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase());
//                                } else {
//                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
//                                }
//                            } catch (Exception e) {
//                                LOGGER.error(e.getMessage(), e);
//                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BCF")));
//                            }
//                            if (iub_bcf != null) {
//                                multimapParamTemp.put(iub_bcf.toString(), basicDynaBean);
//                            }
//                        }
//                        for (String bcfTemp : multimapParamTemp.keySet()) {
//                            if (mapIubBcf.containsKey(bcfTemp)) {
//                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.iub_bcf.exits.2.type.delete"), bcfTemp));
//                            }
//                            mapIubBcf.put(bcfTemp, bcfTemp);
//                            Collection<BasicDynaBean> basicDynaBeansBcf = multimapParamTemp.get(bcfTemp);
//                            for (BasicDynaBean basicDynaBean : basicDynaBeansBcf) {
//                                Map<String, String> mapInBasicDynaBean;
//                                try {
//                                    mapInBasicDynaBean = basicDynaBean.getMap();
//                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
//                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
//                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                        } else {
//                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                        }
//                                    }
//                                } catch (Exception ex) {
//                                    logger.error(ex.getMessage(), ex);
//                                }
//                            }
//                            //Luu du lieu vao bang detail
//                            stationDetail = new StationDetail();
//                            stationDetail.setStationCode(bcfTemp);
//                            stationDetails.add(stationDetail);
//
//                        }
//
//                    } catch (Exception e) {
//                        throw e;
//                    }
//
//                    //Xet lai gia tri bcf
//                    Map<String, List<String>> mapBcf = new HashMap<>();
//                    if ("3G".equals(currStationPlan.getNetworkType())) {
//                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase())
//                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
//                            String[] bcfs = mapParam.get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase()).split(";");
//                            String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
//                            String bcfFinal = null;
//                            String cellAll = null;
//                            for (int i = 0; i < bcfs.length; i++) {
//                                if (cells[i] != null) {
//                                    if (mapBcf.containsKey(bcfs[i])) {
//                                        mapBcf.get(bcfs[i]).add(cells[i]);
//                                    } else {
//                                        List<String> lstCell = new ArrayList<>();
//                                        lstCell.add(cells[i]);
//                                        mapBcf.put(bcfs[i], lstCell);
//                                    }
//
//                                }
//                            }
//                            for (String bcf : mapBcf.keySet()) {
//                                Collections.sort(mapBcf.get(bcf));
//                                if (bcfFinal == null) {
//                                    bcfFinal = bcf;
//                                    cellAll = StringUtils.join(mapBcf.get(bcf), "-");
//                                } else {
//                                    bcfFinal = bcfFinal + ";" + bcf;
//                                    cellAll = cellAll + ";" + StringUtils.join(mapBcf.get(bcf), "-");
//                                }
//                            }
//                            mapParam.put("cell_all", cellAll);
//                            mapParam.put("iub", bcfFinal);
//                        }
//                    } else {
//                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase())
//                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
//                            String[] bcfs = mapParam.get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase()).split(";");
//                            String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
//                            String bcfFinal = null;
//                            String cellAll = null;
//                            for (int i = 0; i < bcfs.length; i++) {
//                                if (cells[i] != null) {
//                                    if (mapBcf.containsKey(bcfs[i])) {
//                                        mapBcf.get(bcfs[i]).add(cells[i]);
//                                    } else {
//                                        List<String> lstCell = new ArrayList<>();
//                                        lstCell.add(cells[i]);
//                                        mapBcf.put(bcfs[i], lstCell);
//                                    }
//
//                                }
//                            }
//                            for (String bcf : mapBcf.keySet()) {
//                                Collections.sort(mapBcf.get(bcf));
//                                if (bcfFinal == null) {
//                                    bcfFinal = bcf;
//                                    cellAll = StringUtils.join(mapBcf.get(bcf), "-");
//                                } else {
//                                    bcfFinal = bcfFinal + ";" + bcf;
//                                    cellAll = cellAll + ";" + StringUtils.join(mapBcf.get(bcf), "-");
//                                }
//                            }
//                            mapParam.put("cell_all", cellAll);
//                            mapParam.put("bcf", bcfFinal);
//                        }
//                    }
//                    if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
//                            && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase())) {
//                        String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
//                        String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase()).split(";");
//                        String cell_lac_ci = "";
//                        for (int i = 0; i < lacs.length; i++) {
//                            if (cis[i] != null) {
//                                cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
//                            }
//                        }
//
//                        mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
//                    }
//                    mapParam.put("cell_check", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
//                    mapParam.put("cell_msc", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
//                    //Luu log import
//                    StationResult stationResult;
//                    for (String paramCode : mapParam.keySet()) {
//                        if (mapParam.get(paramCode) == null
//                                || "".equals(mapParam.get(paramCode))
//                                || mapParam.get(paramCode).startsWith(";")
//                                || mapParam.get(paramCode).contains(";;")) {
//                            checkCreateMop = false;
//                            stationResult = new StationResult();
//                            stationResult.setParamCode(paramCode);
//                            stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                            stationResult.setNodeB(node);
//                            stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
//                            stationResult.setType("Integrate");
//                            stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), paramCode));
//                            lstStationResult.add(stationResult);
//                        } else {
//                            mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
//                        }
//                    }
//
//                    if (checkCreateMop) {
//                        mapNodeParam.put(node, mapParam);
//                        mapBcfStationDetail.put(node, stationDetails);
//                    } else {
//                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"), node));
//                        if (!lstStationResult.isEmpty()) {
//                            if (mapStationResult.containsKey(node)) {
//                                mapStationResult.get(node).addAll(lstStationResult);
//                            } else {
//                                mapStationResult.put(node, lstStationResult);
//                            }
//                        }
//                    }
//
//                } catch (Exception e) {
//                    throw e;
//                }
//            }
//            //Luu database
//            //Insert vao DB
//            HashMap<String, String> mapNodeRncBsc = new HashMap<>();
//            HashMap<String, String> mapNodeMsc = new HashMap<>();
//            currStationPlan.setUpdateTime(new Date());
//            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
//            currStationPlan.setProficientType(2L);
//
//            //Insert tram vao DB
//            List<StationDetail> lstInsertDetails = new ArrayList<>();
//            List<StationDetail> stationDetailsTemp;
//            StationResult stationResult;
//            Map<String, Object> filters = new HashMap<>();
//            for (String node : mapNodeParam.keySet()) {
//                stationDetailsTemp = mapBcfStationDetail.get(node);
//                String[] node_deleteType = node.split("#");
//                String nodeRncBsc = node_deleteType[0];
//                mapNodeRncBsc.put(nodeRncBsc, nodeRncBsc);
//                List<StationResult> lstStationResult = new ArrayList<>();
//
//                if (mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
//                        || mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {
//
//                    filters.put("rncBsc", nodeRncBsc);
//                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
//
//                    if (lstResource.isEmpty()) {
//                        stationResult = new StationResult();
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim());
//                        } else {
//                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.BSC").trim());
//                        }
//
//                        stationResult.setParamValue(nodeRncBsc);
//                        stationResult.setNodeB(node);
//                        stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
//                        stationResult.setType("Integrate");
//                        stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), nodeRncBsc));
//                        lstStationResult.add(stationResult);
//                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
//                                nodeRncBsc));
//                        mapNodeParam.remove(node);
////                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
//                    } else {
//                        mapNodeMsc.put(node, lstResource.get(0).getMsc());
//                        for (StationDetail sd : stationDetailsTemp) {
//                            sd.setStationResource(lstResource.get(0));
//                            sd.setRncBsc(nodeRncBsc);
//                            if ("3G".equals(currStationPlan.getNetworkType())) {
//                                sd.setCellCode(mapNodeParam.get(node).get("userlabel"));
//                            } else {
//                                sd.setCellCode(mapNodeParam.get(node).get("cell"));
//                            }
//                            sd.setUpdateTime(new Date());
//                            sd.setStationPlan(currStationPlan);
//                        }
//                        lstInsertDetails.addAll(stationDetailsTemp);
//                    }
//
//                } else {
//                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                }
//                if (!lstStationResult.isEmpty()) {
//                    if (mapStationResult.containsKey(node)) {
//                        mapStationResult.get(node).addAll(lstStationResult);
//                    } else {
//                        mapStationResult.put(node, lstStationResult);
//                    }
//                }
//            }
//            stationPlanService.saveOrUpdate(currStationPlan);
//            new StationDetailServiceImpl().saveOrUpdate(lstInsertDetails);
//            //Sinh mop RNC, BSC
//            createMopDelete(mapNodeParam, stationConfigImports, mapNodeRncBsc, mapNodeMsc);
//
//        } catch (Exception ex) {
//
//            LOGGER.error(ex.getMessage());
//            throw ex;
//        }
//
//    }
//
//    public void createMopDelete(HashMap<String, HashMap<String, String>> mapNodeParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc) throws AppException, Exception {
//        //Sinh mop cho RNC, MSC
//        createFlowRunActionDelete(mapNodeParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
//    }
//    protected boolean createFlowRunActionDelete(HashMap<String, HashMap<String, String>> mapNodeParam, HashMap<String, String> mapNodeRncBsc, HashMap<String, String> mapNodeMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
//
//        GenerateFlowRunController generateFlowRunController;
//        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
//        FlowTemplates flowTemplates = null;
//        String deleteType;
//        String nodeCodeRncBsc;
//        for (String node : mapNodeParam.keySet()) {
//            String[] node_DeleteType = node.split("#");
//            nodeCodeRncBsc = node_DeleteType[0];
//            deleteType = node_DeleteType[1];
//
//            if (deleteType != null) {
//                for (StationConfigImport sci : stationConfigImports) {
//                    if (deleteType.equalsIgnoreCase(sci.getDeleteType())) {
//                        flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
//                        break;
//                    }
//                }
//            } else {
//                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//            }
//
//            if (flowTemplates == null) {
//                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//                continue;
//            } else {
//                if (flowTemplates.getStatus() != 9) {
//                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
//                    continue;
//                }
//            }
//
//            try {
//                String filePutOSS;
//                if ("3G".equals(currStationPlan.getNetworkType())) {
//                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
//                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
//                            + "-" + currStationPlan.getNetworkType().toUpperCase()
//                            + "-RNC_" + nodeCodeRncBsc.toUpperCase()
//                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//                } else {
//                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
//                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
//                            + "-" + currStationPlan.getNetworkType().toUpperCase()
//                            + "-BSC_" + nodeCodeRncBsc.toUpperCase()
//                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//                }
//                generateFlowRunController = new GenerateFlowRunController();
//                FlowRunAction flowRunAction = new FlowRunAction();
//                flowRunAction.setCrNumber(Config.CR_DEFAULT);
//                flowRunAction.setFlowRunName(filePutOSS);
////                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
//                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
//                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
//                }
//                flowRunAction.setTimeRun(new Date());
//                flowRunAction.setFlowTemplates(flowTemplates);
//                flowRunAction.setExecuteBy(userCreate);
////                if (mopType != null) {
////                    flowRunAction.setMopType(mopType);
////                } else {
////                    flowRunAction.setMopType(1l);
////                }
//
//                generateFlowRunController.setFlowRunAction(flowRunAction);
//                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
//                //Lay danh sach param tu bang
//                generateFlowRunController.setNodes(new ArrayList<Node>());
////                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
//                Map<String, String> mapParamValues;
//                generateFlowRunController.loadGroupAction(0l);
//                List<Node> nodeInPlan;
//                HashMap<String, Object> filters = new HashMap<>();
//                filters.put("nodeCode-EXAC", nodeCodeRncBsc);
//                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
//                Node nodeRncBsc;
//                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
//                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                } else {
//                    nodeRncBsc = nodeRncs.get(0);
//                }
//                Node nodeMsc;
//                List<Node> lstnodeMsc = new ArrayList<>();
//                filters.clear();
//                String[] mscStr = mapNodeMsc.get(node).split(";");
//                for (String msc : mscStr) {
//                    filters.clear();
//                    filters.put("nodeCode-EXAC", msc);
//                    List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
//                    if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
//                    } else {
//                        nodeMsc = nodeMscs.get(0);
//                        lstnodeMsc.add(nodeMsc);
//                    }
//                }
//                mapParamValues = mapNodeParam.get(node);
//
//                nodeInPlan = new ArrayList<>();
//                generateFlowRunController.getNodes().add(nodeRncBsc);
//                for (Node nodeTemp : lstnodeMsc) {
//                    generateFlowRunController.getNodes().add(nodeTemp);
//                    nodeInPlan.add(nodeTemp);
//                }
//                nodeInPlan.add(nodeRncBsc);
//                if (deleteType != null && deleteType.toUpperCase().equals(MessageUtil.getResourceBundleMessage("label.station.delete.type0").toUpperCase())) {
//                    mapParamValues.put(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase(), null);
//                }
//                for (Node nodeTemp : nodeInPlan) {
//                    generateFlowRunController.loadGroupAction(nodeTemp);
//                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(nodeTemp);
//                    for (ParamValue paramValue : paramValues) {
//                        if (paramValue.getParamInput().getReadOnly()) {
//                            continue;
//                        }
//                        Object value = null;
//                        try {
//                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
//                        } catch (Exception e) {
//                            LOGGER.error(e.getMessage(), e);
//                        }
//                        if (value != null) {
//                            paramValue.setParamValue(value.toString());
//                        }
////                        if (currStationPlan.getNetworkType().equals("3G")) {
////                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
////                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", nodeTemp.getNodeCode())
////                                        .replace("{1}", paramValue.getParamCode()));
////                                return false;
////                            }
////                        }
//                    }
//                }
//                mapParamValues.clear();
//
//                boolean saveDT = generateFlowRunController.saveDT();
//                if (saveDT) {
//                    try {
//                        //Bat dau xu ly day file OSS
//                        //Tao file
//
//                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
//                        stationFlowRunAction.setFlowRunAction(flowRunAction);
//                        stationFlowRunAction.setNode(nodeRncBsc);
//                        stationFlowRunAction.setStationPlan(currStationPlan);
//                        stationFlowRunAction.setType(1L);
//                        stationFlowRunAction.setUpdateTime(new Date());
//                        stationFlowRunActiones.add(stationFlowRunAction);
//                        MessageUtil.setInfoMessageFromRes(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeCodeRncBsc));
//
//                    } catch (Exception e) {
//                        LOGGER.error(e.getMessage(), e);
//                    }
//                    //Xu ly day du lieu vao
//
//                }
////                return saveDT;
//            } catch (Exception e) {
//                if (e instanceof MessageException) {
//                    throw e;
//                } else {
////                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
//                    LOGGER.error(e.getMessage(), e);
//                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
//                }
//
////                return false;
////            throw e;
//            } finally {
//                //Tam biet
//            }
//        }
//        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
//        return true;
//    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Delete SubMop">
//    public void analyseDataDeleteSub(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
//        try {
//            //Xy ly du lieu file
//            //Xu ly day tat ca cac du lieu vao map voi key node
//            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
//
//            for (String sheetName : mapCellObjectImports.keySet()) {
//                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
//
//                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
//                    try {
//                        Object node;
//                        Object deleteType;
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").toLowerCase());
//                        } else {
//                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
//                        }
//                        deleteType = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.delete.type").toLowerCase());
//                        if (node != null && deleteType != null) {
//                            multimapParam.put((node.toString() + "#" + deleteType.toString()), basicDynaBean);
//                        }
//
//                    } catch (Exception e) {
//                        LOGGER.error(e.getMessage(), e);
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                    }
//                }
//            }
//            //xu ly map
//            HashMap<String, HashMap<String, HashMap<String, String>>> mapNodeParam = new HashMap<>();
//            Map<String, String> mapIubBcf = new HashMap<>();
//            for (String node : multimapParam.keySet()) {
//                try {
//
//                    boolean checkCreateMop = true;
//                    List<StationResult> lstStationResult = new ArrayList<>();
//                    HashMap<String, HashMap<String, String>> mapSubNodeParam = new HashMap<>();
//
//                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(node);
//                    Object iub_bcf;
//
//                    Multimap<String, BasicDynaBean> multimapParamTemp = ArrayListMultimap.create();
//                    try {
//                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
//                            //sap xep xoa theo bcf/iub
//                            try {
//                                if ("3G".equals(currStationPlan.getNetworkType())) {
//                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase());
//                                } else {
//                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
//                                }
//                            } catch (Exception e) {
//                                LOGGER.error(e.getMessage(), e);
//                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BCF")));
//                            }
//                            if (iub_bcf != null) {
//                                multimapParamTemp.put(iub_bcf.toString(), basicDynaBean);
//                            }
//                        }
//                        for (String bcfTemp : multimapParamTemp.keySet()) {
//                            HashMap<String, String> mapParam = new HashMap<>();
//                            if (mapIubBcf.containsKey(bcfTemp)) {
//                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.iub_bcf.exits.2.type.delete"), bcfTemp));
//                            }
//                            mapIubBcf.put(bcfTemp, bcfTemp);
//                            Collection<BasicDynaBean> basicDynaBeansBcf = multimapParamTemp.get(bcfTemp);
//                            for (BasicDynaBean basicDynaBean : basicDynaBeansBcf) {
//                                Map<String, String> mapInBasicDynaBean;
//                                try {
//                                    mapInBasicDynaBean = basicDynaBean.getMap();
//                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
//                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
//                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                        } else {
//                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                        }
//                                    }
//                                } catch (Exception ex) {
//                                    logger.error(ex.getMessage(), ex);
//                                }
//                            }
//                            //Luu du lieu vao bang detail
//                            mapSubNodeParam.put(bcfTemp, mapParam);
//                        }
//
//                    } catch (Exception e) {
//                        throw e;
//                    }
//                    for (String nodeSub : mapSubNodeParam.keySet()) {
//                        HashMap<String, String> mapParam = mapSubNodeParam.get(nodeSub);
//                        //Xet lai gia tri bcf
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase())
//                                    && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
//                                String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
//                                String cellAll = "";
//                                for (String cell : cells) {
//                                    cellAll = cellAll + "-" + cell;
//                                }
//
//                                mapParam.put("cell_all", cellAll.substring(1));
//                                mapParam.put("iub", nodeSub);
//                            }
//                        } else {
//                            if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase())
//                                    && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
//                                String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
//                                String cellAll = "";
//                                for (String cell : cells) {
//                                    cellAll = cellAll + "-" + cell;
//                                }
//
//                                mapParam.put("cell_all", cellAll.substring(1));
//                                mapParam.put("bcf", nodeSub);
//                            }
//                        }
//                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
//                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase())) {
//                            String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
//                            String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase()).split(";");
//                            String cell_lac_ci = "";
//                            for (int i = 0; i < lacs.length; i++) {
//                                if (cis[i] != null) {
//                                    cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
//                                }
//                            }
//
//                            mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
//                        }
//                        mapParam.put("cell_check", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
//                        mapParam.put("cell_msc", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
//                        //Luu log import
//                        StationResult stationResult;
//                        for (String paramCode : mapParam.keySet()) {
//                            if (mapParam.get(paramCode) == null
//                                    || "".equals(mapParam.get(paramCode))
//                                    || mapParam.get(paramCode).startsWith(";")
//                                    || mapParam.get(paramCode).contains(";;")) {
//                                checkCreateMop = false;
//                                stationResult = new StationResult();
//                                stationResult.setParamCode(paramCode);
//                                stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                                stationResult.setNodeB(node);
//                                stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
//                                stationResult.setType("Integrate");
//                                stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), paramCode));
//                                lstStationResult.add(stationResult);
//                            } else {
//                                mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
//                            }
//                        }
//
//                        if (checkCreateMop) {
//                            mapNodeParam.put(node, mapSubNodeParam);
//                        } else {
//                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"), node));
//                            if (!lstStationResult.isEmpty()) {
//                                if (mapStationResult.containsKey(node)) {
//                                    mapStationResult.get(node).addAll(lstStationResult);
//                                } else {
//                                    mapStationResult.put(node, lstStationResult);
//                                }
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    throw e;
//                }
//            }
//            //Luu database
//            //Insert vao DB
//            HashMap<String, String> mapNodeRncBsc = new HashMap<>();
//            HashMap<String, String> mapNodeMsc = new HashMap<>();
//            currStationPlan.setUpdateTime(new Date());
//            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
//            currStationPlan.setProficientType(2L);
//
//            //Insert tram vao DB
//            List<StationDetail> lstInsertDetails = new ArrayList<>();
//            StationResult stationResult;
//            Map<String, Object> filters = new HashMap<>();
//            for (String node : mapNodeParam.keySet()) {
//                String[] node_deleteType = node.split("#");
//                String nodeRncBsc = node_deleteType[0];
//                mapNodeRncBsc.put(nodeRncBsc, nodeRncBsc);
//
//                List<StationResult> lstStationResult = new ArrayList<>();
//
//                if (mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
//                        || mapNodeParam.get(node).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {
//
//                    filters.put("rncBsc", nodeRncBsc);
//                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
//
//                    if (lstResource.isEmpty()) {
//                        stationResult = new StationResult();
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim());
//                        } else {
//                            stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.BSC").trim());
//                        }
//
//                        stationResult.setParamValue(nodeRncBsc);
//                        stationResult.setNodeB(node);
//                        stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
//                        stationResult.setType("Integrate");
//                        stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), nodeRncBsc));
//                        lstStationResult.add(stationResult);
//                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
//                                nodeRncBsc));
//                        mapNodeParam.remove(node);
////                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
//                    } else {
//                        mapNodeMsc.put(node, lstResource.get(0).getMsc());
//                        for (String subNode : mapNodeParam.get(node).keySet()) {
//                            StationDetail sd = new StationDetail();
//                            sd.setStationResource(lstResource.get(0));
//                            sd.setRncBsc(nodeRncBsc);
//                            sd.setStationCode(subNode);
//                            if ("3G".equals(currStationPlan.getNetworkType())) {
//                                sd.setCellCode(mapNodeParam.get(node).get(subNode).get("userlabel"));
//                            } else {
//                                sd.setCellCode(mapNodeParam.get(node).get(subNode).get("cell"));
//                            }
//                            sd.setUpdateTime(new Date());
//                            sd.setStationPlan(currStationPlan);
//                            lstInsertDetails.add(sd);
//                        }
//                    }
//                } else {
//                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//                }
//                if (!lstStationResult.isEmpty()) {
//                    if (mapStationResult.containsKey(node)) {
//                        mapStationResult.get(node).addAll(lstStationResult);
//                    } else {
//                        mapStationResult.put(node, lstStationResult);
//                    }
//
//                }
//            }
//            stationPlanService.saveOrUpdate(currStationPlan);
//            new StationDetailServiceImpl().saveOrUpdate(lstInsertDetails);
//            //Sinh mop RNC, BSC
//            createMopDelete(mapNodeParam, stationConfigImports, mapNodeRncBsc, mapNodeMsc);
//
//        } catch (Exception ex) {
//
//            LOGGER.error(ex.getMessage());
//            throw ex;
//        }
//
//    }
    public void analyseDataDelete(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key node
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();

            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object node;
                        Object deleteType;
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").toLowerCase());
                        } else {
                            node = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                        }
                        deleteType = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.delete.type").toLowerCase());
                        if (node != null && deleteType != null) {
                            multimapParam.put((node.toString() + "#" + deleteType.toString()), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, HashMap<String, String>>> mapNodeParam = new HashMap<>();
            Map<String, String> mapIubBcf = new HashMap<>();
            for (String node : multimapParam.keySet()) {
                try {

                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, HashMap<String, String>> mapSubNodeParam = new HashMap<>();

                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(node);
                    Object iub_bcf;

                    Multimap<String, BasicDynaBean> multimapParamTemp = ArrayListMultimap.create();
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            //sap xep xoa theo bcf/iub
                            try {
                                if ("3G".equals(currStationPlan.getNetworkType())) {
                                    iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase());
                                } else {
                                    if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
                                        iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
                                    } else {
                                        iub_bcf = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.TG_NUMBER").toLowerCase());
                                    }
                                }
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);

                                if ("3G".equals(currStationPlan.getNetworkType())) {
                                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.iub")));
                                } else {
                                    if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
                                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BCF")));
                                    } else {
                                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.TG_NUMBER")));
                                    }
                                }
                            }
                            if (iub_bcf != null) {
                                multimapParamTemp.put(iub_bcf.toString(), basicDynaBean);
                            }
                        }
                        for (String bcfTemp : multimapParamTemp.keySet()) {
                            HashMap<String, String> mapParam = new HashMap<>();
                            if (mapIubBcf.containsKey(bcfTemp)) {
                                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.iub_bcf.exits.2.type.delete"), bcfTemp));
                            }
                            mapIubBcf.put(bcfTemp, bcfTemp);
                            Collection<BasicDynaBean> basicDynaBeansBcf = multimapParamTemp.get(bcfTemp);
                            for (BasicDynaBean basicDynaBean : basicDynaBeansBcf) {
                                Map<String, String> mapInBasicDynaBean;
                                try {
                                    mapInBasicDynaBean = basicDynaBean.getMap();
                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                            }
                            //Luu du lieu vao bang detail
                            mapSubNodeParam.put(bcfTemp, mapParam);
                        }

                    } catch (Exception e) {
                        throw e;
                    }
                    for (String nodeSub : mapSubNodeParam.keySet()) {
                        HashMap<String, String> mapParam = mapSubNodeParam.get(nodeSub);
                        //Xet lai gia tri bcf
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.iub").toLowerCase())
                                    && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                                String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                                String cellAll = "";
                                for (String cell : cells) {
                                    cellAll = cellAll + "-" + cell;
                                }

                                mapParam.put("cell_all", cellAll.substring(1));
                                mapParam.put("iub", nodeSub);
                            }
                        } else {
                            if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.NOKIA)) {
                                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase())
                                        && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                                    String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                                    String cellAll = "";
                                    for (String cell : cells) {
                                        cellAll = cellAll + "-" + cell;
                                    }

                                    mapParam.put("cell_all", cellAll.substring(1));
                                    mapParam.put("bcf", nodeSub);
                                }
                            } else if (currStationPlan.getVendor().getVendorName().equalsIgnoreCase(Constants.vendorType.ERICSSON)) {
                                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.TG_NUMBER").toLowerCase())
                                        && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase())) {
                                    String[] cells = mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()).split(";");
                                    String cellAll = "";
                                    for (String cell : cells) {
                                        cellAll = cellAll + "-" + cell;
                                    }

                                    mapParam.put("cell_all", cellAll.substring(1));
                                    mapParam.put("tg_number", nodeSub);
                                }
                            }
                        }
                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
                                && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase())) {
                            String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
                            String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.ci").toLowerCase()).split(";");
                            String cell_lac_ci = "";
                            for (int i = 0; i < lacs.length; i++) {
                                if (cis[i] != null) {
                                    cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
                                }
                            }

                            mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
                        }
                        mapParam.put("cell_check", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
                        mapParam.put("cell_msc", mapParam.get(MessageUtil.getResourceBundleMessage("label.station.cellCode").toLowerCase()));
                        //Luu log import
                        StationResult stationResult;
                        for (String paramCode : mapParam.keySet()) {
                            if (mapParam.get(paramCode) == null
                                    || "".equals(mapParam.get(paramCode))
                                    || mapParam.get(paramCode).startsWith(";")
                                    || mapParam.get(paramCode).contains(";;")) {
                                checkCreateMop = false;
                                stationResult = new StationResult();
                                stationResult.setParamCode(paramCode);
                                stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
                                stationResult.setNodeB(node);
                                stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                                stationResult.setType("Integrate");
                                stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), paramCode));
                                lstStationResult.add(stationResult);
                            } else {
                                mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
                            }
                        }

                        if (checkCreateMop) {
                            mapNodeParam.put(node, mapSubNodeParam);
                        } else {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"), node));
                            if (!lstStationResult.isEmpty()) {
                                if (mapStationResult.containsKey(node)) {
                                    mapStationResult.get(node).addAll(lstStationResult);
                                } else {
                                    mapStationResult.put(node, lstStationResult);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
            currStationPlan.setProficientType(2L);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> lstInsertDetails = new ArrayList<>();
            StationResult stationResult;
            Map<String, Object> filters = new HashMap<>();
            for (String node : mapNodeParam.keySet()) {
                String[] node_deleteType = node.split("#");
                String nodeRncBsc = node_deleteType[0];
                mapNodeRncBsc.put(nodeRncBsc, nodeRncBsc);

                List<StationResult> lstStationResult = new ArrayList<>();
                for (String subNode : mapNodeParam.get(node).keySet()) {
                    if (mapNodeParam.get(node).get(subNode).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
                            || mapNodeParam.get(node).get(subNode).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {

                        filters.put("rncBsc", nodeRncBsc);
                        List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);

                        if (lstResource.isEmpty()) {
                            stationResult = new StationResult();
                            if ("3G".equals(currStationPlan.getNetworkType())) {
                                stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim());
                            } else {
                                stationResult.setParamCode(MessageUtil.getResourceBundleMessage("label.BSC").trim());
                            }

                            stationResult.setParamValue(nodeRncBsc);
                            stationResult.setNodeB(subNode);
                            stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                            stationResult.setType("Integrate");
                            stationResult.setDetailResult(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"), nodeRncBsc));
                            lstStationResult.add(stationResult);
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                    nodeRncBsc));
                            mapNodeParam.get(node).remove(subNode);
//                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
                        } else {
                            mapNodeMsc.put(node, lstResource.get(0).getMsc());
                            StationDetail sd = new StationDetail();
                            sd.setStationResource(lstResource.get(0));
                            sd.setRncBsc(nodeRncBsc);
                            sd.setStationCode(subNode);
                            if ("3G".equals(currStationPlan.getNetworkType())) {
                                sd.setCellCode(mapNodeParam.get(node).get(subNode).get("userlabel"));
                            } else {
                                sd.setCellCode(mapNodeParam.get(node).get(subNode).get("cell"));
                            }
                            sd.setUpdateTime(new Date());
                            sd.setStationPlan(currStationPlan);
                            lstInsertDetails.add(sd);

                        }
                    } else {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    }
                    if (!lstStationResult.isEmpty()) {
                        if (mapStationResult.containsKey(node)) {
                            mapStationResult.get(node).addAll(lstStationResult);
                        } else {
                            mapStationResult.put(node, lstStationResult);
                        }

                    }
                }
            }
            stationPlanService.saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(lstInsertDetails);
            //Sinh mop RNC, BSC
            createMopDelete(mapNodeParam, stationConfigImports, mapNodeRncBsc, mapNodeMsc);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void createMopDelete(HashMap<String, HashMap<String, HashMap<String, String>>> mapNodeParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        createFlowRunActionDelete(mapNodeParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
    }

    protected boolean createFlowRunActionDelete(HashMap<String, HashMap<String, HashMap<String, String>>> mapNodeParam, HashMap<String, String> mapNodeRncBsc, HashMap<String, String> mapNodeMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        String deleteType;
        String nodeCodeRncBsc;
        for (String node : mapNodeParam.keySet()) {
            String[] node_DeleteType = node.split("#");
            nodeCodeRncBsc = node_DeleteType[0];
            deleteType = node_DeleteType[1];

            if (deleteType != null) {
                for (StationConfigImport sci : stationConfigImports) {
                    if (deleteType.equalsIgnoreCase(sci.getDeleteType())) {
                        flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
                        break;
                    }
                }
            } else {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
            }

            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                continue;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-RNC_" + nodeCodeRncBsc.toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-" + deleteType + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BSC_" + nodeCodeRncBsc.toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                FlowRunAction flowRunAction = new FlowRunAction();
                flowRunAction.setCrNumber(Config.CR_DEFAULT);
                flowRunAction.setFlowRunName(filePutOSS);
//                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang 
                generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", nodeCodeRncBsc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                Node nodeMsc;
                List<Node> lstnodeMsc = new ArrayList<>();
                filters.clear();
                String[] mscStr = mapNodeMsc.get(node).split(";");
                for (String msc : mscStr) {
                    filters.clear();
                    filters.put("nodeCode-EXAC", msc);
                    List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
                    if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
                    } else {
                        nodeMsc = nodeMscs.get(0);
                        lstnodeMsc.add(nodeMsc);
                    }
                }
                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                for (Node nodeTemp : lstnodeMsc) {
                    generateFlowRunController.getNodes().add(nodeTemp);
                    nodeInPlan.add(nodeTemp);
                }
                nodeInPlan.add(nodeRncBsc);
                //Tham so theo tung subnode
                for (String subFlowRun : mapNodeParam.get(node).keySet()) {
                    generateFlowRunController.getMapSubFlowRunNodes().put(subFlowRun, nodeInPlan);
                    for (Node nodeTemp : nodeInPlan) {
                        generateFlowRunController.loadGroupAction(subFlowRun, nodeTemp);
                        List<ParamValue> paramValues = generateFlowRunController.getParamInputs(subFlowRun, nodeTemp);
                        mapParamValues = mapNodeParam.get(node).get(subFlowRun);
                        for (ParamValue paramValue : paramValues) {
                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }
                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                            if (value != null) {
                                paramValue.setParamValue(value.toString());
                            }
//                        if (currStationPlan.getNetworkType().equals("3G")) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", nodeTemp.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }
                        }

                    }
                }

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file

                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                        stationFlowRunAction.setFlowRunAction(flowRunAction);
                        stationFlowRunAction.setNode(nodeRncBsc);
                        stationFlowRunAction.setStationPlan(currStationPlan);
                        stationFlowRunAction.setType(1L);
                        stationFlowRunAction.setUpdateTime(new Date());
                        stationFlowRunActiones.add(stationFlowRunAction);
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeCodeRncBsc));

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao 

                }
//                return saveDT;
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
//                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }

//                return false;
//            throw e;
            } finally {
                //Tam biet
            }
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //    //<editor-fold defaultstate="collapsed" desc="Upgrade Dowgrade SubFlowRun">
//    public void analyseDataUpDowngrade(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
//        try {
//            //Xy ly du lieu file
//            //Xu ly day tat ca cac du lieu vao map voi key nodeB
//            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
//            for (String sheetName : mapCellObjectImports.keySet()) {
//                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
//
//                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
//                    try {
//                        Object nodeB;
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.NodeB").toLowerCase());
//                        } else {
//                            nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase());
//                        }
//
//                        if (nodeB != null) {
//                            multimapParam.put(nodeB.toString(), basicDynaBean);
//                        }
//
//                    } catch (Exception e) {
//                        LOGGER.error(e.getMessage(), e);
//                        throw new MessageException(MessageUtil.getResourceBundleMessage("label.station.nodeB.not.exits"));
//                    }
//                }
//            }
//            //xu ly map
//            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();
//
//            for (String nodeB : multimapParam.keySet()) {
//                try {
//                    boolean checkCreateMop = true;
//                    List<StationResult> lstStationResult = new ArrayList<>();
//                    HashMap<String, String> mapParam = new HashMap<>();
//                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
//                    try {
//                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
//                            Map<String, String> mapInBasicDynaBean;
//                            try {
//                                mapInBasicDynaBean = basicDynaBean.getMap();
//                                for (String paramCode : mapInBasicDynaBean.keySet()) {
//                                    if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
//                                        mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                    } else {
//                                        mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
//                                    }
//                                }
//                            } catch (Exception e) {
//                                logger.error(e.getMessage(), e);
//                            }
//                        }
//                    } catch (Exception e) {
//                        throw e;
//                    }
//                    String ipNodeB = null;
//                    Map<String, String> mapIpNodeB = new HashMap<String, String>();
//                    if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ip"))) {
//                        ipNodeB = mapParam.get(MessageUtil.getResourceBundleMessage("label.ip"));
//                        String[] ipNodeBs = ipNodeB.split(";");
//                        if (ipNodeBs.length == 0) {
//                            mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), null);
//                        } else {
//                            for (String ip : ipNodeBs) {
//                                mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), ip);
//
//                                if (mapIpNodeB.isEmpty()) {
//                                    mapIpNodeB.put(ip, ip);
//                                } else {
//                                    if (!mapIpNodeB.containsKey(ip)) {
//                                        mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), null);
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//
//                    } else {
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
//                    }
//                    mapParam.put(MessageUtil.getResourceBundleMessage("date"), new SimpleDateFormat("HH_dd_MM_yyyy").format(new Date()));
//                    calculateParamDetail(mapParam, lstParamDefault, LOGGER);
//                    //Luu log import
//                    StationResult stationResult;
//                    if ("3G".equals(currStationPlan.getNetworkType())) {
//                        for (String paramCode : mapParam.keySet()) {
//                            if (mapParam.get(paramCode) == null
//                                    || "".equals(mapParam.get(paramCode))
//                                    || mapParam.get(paramCode).startsWith(";")
//                                    || mapParam.get(paramCode).contains(";;")) {
//                                checkCreateMop = false;
//                                stationResult = new StationResult();
//                                stationResult.setParamCode(paramCode);
//                                stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                                stationResult.setNodeB(nodeB);
//                                stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
//                                stationResult.setType("UpDowngrade");
//                                stationResult.setDetailResult(
//                                        MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"),
//                                                paramCode));
//                                lstStationResult.add(stationResult);
//                            } else {
//                                mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
//                            }
//                        }
//                    } else {
//                        for (String paramCode : mapParam.keySet()) {
//                            mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
//                        }
//                    }
//
//                    if (checkCreateMop) {
//                        mapNodeBParam.put(nodeB, mapParam);
//                    } else {
//                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
//                                nodeB));
//                        if (!lstStationResult.isEmpty()) {
//                            if (mapStationResult.containsKey(nodeB)) {
//                                mapStationResult.get(nodeB).addAll(lstStationResult);
//                            } else {
//                                mapStationResult.put(nodeB, lstStationResult);
//                            }
//                        }
//                    }
//
//                } catch (Exception e) {
//                    if (e instanceof MessageException) {
//                        MessageUtil.setErrorMessage(e.getMessage());
//                    } else {
//                        LOGGER.error(e.getMessage(), e);
//                        throw new Exception(e.getMessage());
//                    }
//                }
//            }
//            //Luu database
//            //Insert vao DB
//            HashMap<String, String> mapNodeBRncBsc = new HashMap<>();
//            HashMap<String, String> mapNodeBMsc = new HashMap<>();
//            currStationPlan.setUpdateTime(new Date());
//            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
//            currStationPlan.setProficientType(3L);
//
//            //Insert tram vao DB
//            List<StationDetail> stationDetails = new ArrayList<>();
//            StationDetail stationDetail;
//            StationResult stationResult;
//            for (String nodeB : mapNodeBParam.keySet()) {
//                List<StationResult> lstStationResult = new ArrayList<>();
//                stationDetail = new StationDetail();
//                stationDetail.setStationPlan(currStationPlan);
//                stationDetail.setStationCode(nodeB);
//
//                stationDetail.setCellCode(nodeB);
//                stationDetail.setUpdateTime(new Date());
//
//                stationDetails.add(stationDetail);
//                if (!lstStationResult.isEmpty()) {
//                    if (mapStationResult.containsKey(nodeB)) {
//                        mapStationResult.get(nodeB).addAll(lstStationResult);
//                    } else {
//                        mapStationResult.put(nodeB, lstStationResult);
//                    }
//                }
//            }
//            stationPlanService.saveOrUpdate(currStationPlan);
//            new StationDetailServiceImpl().saveOrUpdate(stationDetails);
//            //Sinh mop RNC, BSC
//            createMopUpDowngrade(mapNodeBParam, stationConfigImports, mapNodeBRncBsc, mapNodeBMsc);
//
//        } catch (Exception ex) {
//
//            LOGGER.error(ex.getMessage());
//            throw ex;
//        }
//
//    }
//
//    public void createMopUpDowngrade(HashMap<String, HashMap<String, String>> mapNodeParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc) throws AppException, Exception {
//        //Sinh mop cho RNC, MSC
//        createFlowRunActionUpDowngrade(mapNodeParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
//    }
//
//    protected boolean createFlowRunActionUpDowngrade(HashMap<String, HashMap<String, String>> mapNodeParam, HashMap<String, String> mapNodeRncBsc, HashMap<String, String> mapNodeMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
//
//        GenerateFlowRunController generateFlowRunController;
//        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
//        FlowTemplates flowTemplates = null;
//        String nodeCodeRncBsc = "NODEB_RUN";
//        if (!stationConfigImports.isEmpty()) {
//            for (StationConfigImport sci : stationConfigImports) {
//                flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
//                break;
//            }
//        } else {
//            throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//        }
//
//        if (flowTemplates == null) {
//            throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
//        } else {
//            if (flowTemplates.getStatus() != 9) {
//                throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
//            }
//        }
//
//        try {
//            String filePutOSS;
//            if ("3G".equals(currStationPlan.getNetworkType())) {
//                filePutOSS = currStationPlan.getCrNumber().toUpperCase()
//                        + "-" + "UPGRADE_DOWNGRADE" + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
//                        + "-" + currStationPlan.getNetworkType().toUpperCase()
//                        + "-NODEB_" + nodeCodeRncBsc.toUpperCase()
//                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//            } else {
//                filePutOSS = currStationPlan.getCrNumber().toUpperCase()
//                        + "-" + "UPGRADE_DOWNGRADE" + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
//                        + "-" + currStationPlan.getNetworkType().toUpperCase()
//                        + "-BTS_" + nodeCodeRncBsc.toUpperCase()
//                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//            }
//            generateFlowRunController = new GenerateFlowRunController();
//            FlowRunAction flowRunAction = new FlowRunAction();
//            flowRunAction.setCrNumber(Config.CR_DEFAULT);
//            flowRunAction.setFlowRunName(filePutOSS);
////                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
//            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
//                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
//            }
//            flowRunAction.setTimeRun(new Date());
//            flowRunAction.setFlowTemplates(flowTemplates);
//            flowRunAction.setExecuteBy(userCreate);
////                if (mopType != null) {
////                    flowRunAction.setMopType(mopType);
////                } else {
////                    flowRunAction.setMopType(1l);
////                }
//
//            generateFlowRunController.setFlowRunAction(flowRunAction);
//            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
//            //Lay danh sach param tu bang 
//            generateFlowRunController.setNodes(new ArrayList<Node>());
////                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
//            Map<String, String> mapParamValues;
//            generateFlowRunController.loadGroupAction(0l);
//            List<Node> nodeInPlan;
//            HashMap<String, Object> filters = new HashMap<>();
//            filters.put("nodeCode-EXAC", nodeCodeRncBsc);
//            List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
//            Node nodeRncBsc;
//            if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
//                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
//            } else {
//                nodeRncBsc = nodeRncs.get(0);
//            }
//
//            nodeInPlan = new ArrayList<>();
//            generateFlowRunController.getNodes().add(nodeRncBsc);
//            nodeInPlan.add(nodeRncBsc);
//            //Tham so theo tung subnode
//            String ipNodeBAll = null;
//            for (String subFlowRun : mapNodeParam.keySet()) {
//                mapParamValues = mapNodeParam.get(subFlowRun);
//                if (mapParamValues.containsKey(MessageUtil.getResourceBundleMessage("label.ip"))) {
//                    if (mapParamValues.get(MessageUtil.getResourceBundleMessage("label.ip")) == null) {
//                        MessageUtil.getResourceBundleMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
//                        continue;
//                    }
//                    if (ipNodeBAll != null) {
//                        ipNodeBAll = ipNodeBAll + mapParamValues.get(MessageUtil.getResourceBundleMessage("label.ip"));
//                    } else {
//                        ipNodeBAll = mapParamValues.get(MessageUtil.getResourceBundleMessage("label.ip"));
//                    }
//                } else {
//                    MessageUtil.getResourceBundleMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
//                    continue;
//                }
//                generateFlowRunController.getMapSubFlowRunNodes().put(subFlowRun, nodeInPlan);
//                for (Node nodeTemp : nodeInPlan) {
//
//                    generateFlowRunController.loadGroupAction(subFlowRun, nodeTemp);
//                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(subFlowRun, nodeTemp);
//
//                    for (ParamValue paramValue : paramValues) {
//                        if (paramValue.getParamInput().getReadOnly()) {
//                            continue;
//                        }
//                        Object value = null;
//                        try {
//                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
//                        } catch (Exception e) {
//                            LOGGER.error(e.getMessage(), e);
//                        }
//                        if (value != null) {
//                            paramValue.setParamValue(value.toString());
//                        }
////                        if (currStationPlan.getNetworkType().equals("3G")) {
////                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
////                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", nodeTemp.getNodeCode())
////                                        .replace("{1}", paramValue.getParamCode()));
////                                return false;
////                            }
////                        }
//
//                    }
//                    for (GroupAction groupAction : generateFlowRunController.getMapGroupAction().get(subFlowRun + "#" + nodeTemp.getNodeCode())) {
//                        if (groupAction.getGroupActionName().equals(Constants.StationUpDowngradeGroupActionName.CheckAfterExecuting)) {
//                            groupAction.setDeclare(false);
//                        }
//                    }
//
//                }
//
//            }
//            String subFlowRun = "CheckAfterRun";
//            generateFlowRunController.getMapSubFlowRunNodes().put(subFlowRun, nodeInPlan);
//            mapParamValues = new HashMap<>();
//            mapParamValues.put("ip", ipNodeBAll);
//            for (Node nodeTemp : nodeInPlan) {
//                generateFlowRunController.loadGroupAction(subFlowRun, nodeTemp);
//                List<ParamValue> paramValues = generateFlowRunController.getParamInputs(subFlowRun, nodeTemp);
//                for (ParamValue paramValue : paramValues) {
//                    if (paramValue.getParamInput().getReadOnly()) {
//                        continue;
//                    }
//                    Object value = null;
//                    try {
//                        value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
//                    } catch (Exception e) {
//                        LOGGER.error(e.getMessage(), e);
//                    }
//                    if (value != null) {
//                        paramValue.setParamValue(value.toString());
//                    }
//                }
//                for (GroupAction groupAction : generateFlowRunController.getMapGroupAction().get(subFlowRun + "#" + nodeTemp.getNodeCode())) {
//                    if (groupAction.getGroupActionName().equals(Constants.StationUpDowngradeGroupActionName.Executing)) {
//                        groupAction.setDeclare(false);
//                    }
//                }
//
//            }
//            boolean saveDT = generateFlowRunController.saveDT();
//            if (saveDT) {
//                try {
//                    //Bat dau xu ly day file OSS
//                    //Tao file
//
//                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
//                    stationFlowRunAction.setFlowRunAction(flowRunAction);
//                    stationFlowRunAction.setNode(nodeRncBsc);
//                    stationFlowRunAction.setStationPlan(currStationPlan);
//                    stationFlowRunAction.setType(1L);
//                    stationFlowRunAction.setUpdateTime(new Date());
//                    stationFlowRunActiones.add(stationFlowRunAction);
//                    MessageUtil.setInfoMessageFromRes(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeCodeRncBsc));
//
//                } catch (Exception e) {
//                    LOGGER.error(e.getMessage(), e);
//                }
//                //Xu ly day du lieu vao 
//
//            }
////                return saveDT;
//        } catch (Exception e) {
//            if (e instanceof MessageException) {
//                throw e;
//            } else {
//                LOGGER.error(e.getMessage(), e);
//                throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
//            }
//        } finally {
//            //Tam biet
//        }
//
//        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
//        return true;
//    }
//
//    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Upgrade Dowgrade SubFlowRun New">
    public void analyseDataUpDowngrade(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Config type Updowngrade 1: Chi Upgrade, 2: Chi Downgrade, 0: cả 2
            Long typeAction = -1L;
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeSub;
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            nodeSub = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.NodeB").toLowerCase());
                        } else {
                            nodeSub = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase());
                        }

                        if (nodeSub != null) {
                            multimapParam.put(nodeSub.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.NodeB")));
                        } else {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BTS")));
                        }
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();
            String date = new SimpleDateFormat("HH_dd_MM_yyyy").format(new Date());
            for (String nodeB : multimapParam.keySet()) {
                try {
                    typeAction = -1L;
                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, String> mapParam = new HashMap<>();
                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            try {
                                Object type;
                                type = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.updowngrade.action.type").toLowerCase());
                                if (type != null) {
                                    if (!typeAction.equals(0L)) {
                                        if (type.toString().trim().toLowerCase().equals(MessageUtil.getResourceBundleMessage("label.station.updowngrade.action.type.up").toLowerCase())) {
                                            if (typeAction.equals(-1L)) {
                                                typeAction = 1L;
                                            } else if (typeAction.equals(2L)) {
                                                typeAction = 0L;
                                            }
                                        } else if (type.toString().trim().toLowerCase().equals(MessageUtil.getResourceBundleMessage("label.station.updowngrade.action.type.down").toLowerCase())) {
                                            if (typeAction.equals(-1L)) {
                                                typeAction = 2L;
                                            } else if (typeAction.equals(1L)) {
                                                typeAction = 0L;
                                            }
                                        }
                                    }
                                    Map<String, String> mapInBasicDynaBean;
                                    try {
                                        mapInBasicDynaBean = basicDynaBean.getMap();
                                        for (String paramCode : mapInBasicDynaBean.keySet()) {
                                            if (mapParam.containsKey(paramCode.trim().toLowerCase() + "_" + type.toString().trim().toLowerCase())) {
                                                mapParam.put(paramCode.trim().toLowerCase() + "_" + type.toString().trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase() + "_" + type.toString().trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                            } else {
                                                mapParam.put(paramCode.trim().toLowerCase() + "_" + type.toString().trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                            }
                                        }
                                    } catch (Exception e) {
                                        logger.error(e.getMessage(), e);
                                    }
                                } else {
                                    //Dua ra canh bao nhe
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }

                        }
                    } catch (Exception e) {
                        throw e;
                    }
                    String ipNodeBAdd;
                    Map<String, String> mapIpNodeB = new HashMap<String, String>();
                    if (typeAction.equals(1L) || typeAction.equals(0L)) {
                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ip.add"))) {
                            ipNodeBAdd = mapParam.get(MessageUtil.getResourceBundleMessage("label.ip.add"));
                            String[] ipNodeBs = ipNodeBAdd.split(";");
                            if (ipNodeBs.length == 0) {
                                mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), null);
                            } else {
                                for (String ip : ipNodeBs) {
                                    mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), ip);

                                    if (mapIpNodeB.isEmpty()) {
                                        mapIpNodeB.put(ip, ip);
                                    } else {
                                        if (!mapIpNodeB.containsKey(ip)) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), null);
                                            break;
                                        }
                                    }
                                }
                            }

                        } else {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
                        }
                    }
                    if (typeAction.equals(2L) || typeAction.equals(0L)) {
                        String ipNodeBRemove;
                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ip.remove"))) {
                            ipNodeBRemove = mapParam.get(MessageUtil.getResourceBundleMessage("label.ip.remove"));
                            String[] ipNodeBs = ipNodeBRemove.split(";");
                            if (ipNodeBs.length == 0) {
                                mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), null);
                            } else {
                                for (String ip : ipNodeBs) {
                                    mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), ip);

                                    if (mapIpNodeB.isEmpty()) {
                                        mapIpNodeB.put(ip, ip);
                                    } else {
                                        if (!mapIpNodeB.containsKey(ip)) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.ip"), null);
                                            break;
                                        }
                                    }
                                }
                            }

                        } else {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
                        }
                    }
                    mapParam.put(MessageUtil.getResourceBundleMessage("label.date").toLowerCase(), date);
                    calculateParamDetail(mapParam, lstParamDefault, LOGGER);
                    //Luu log import
                    StationResult stationResult;
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        for (String paramCode : mapParam.keySet()) {
                            if (mapParam.get(paramCode) == null
                                    || "".equals(mapParam.get(paramCode))
                                    || mapParam.get(paramCode).startsWith(";")
                                    || mapParam.get(paramCode).contains(";;")) {
                                checkCreateMop = false;
                                stationResult = new StationResult();
                                stationResult.setParamCode(paramCode);
                                stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
                                stationResult.setNodeB(nodeB);
                                stationResult.setResult(MessageUtil.getResourceBundleMessage("label.NOK.not.Create.DT"));
                                stationResult.setType("UpDowngrade");
                                stationResult.setDetailResult(
                                        MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid"),
                                                paramCode));
                                lstStationResult.add(stationResult);
                            } else {
                                mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
                            }
                        }
                    } else {
                        for (String paramCode : mapParam.keySet()) {
                            mapParam.put(paramCode, mapParam.get(paramCode).substring(0, Math.min(3950, mapParam.get(paramCode).length())));
                        }
                    }

                    if (checkCreateMop) {
                        mapNodeBParam.put(nodeB, mapParam);
                    } else {
                        MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                nodeB));
                        if (!lstStationResult.isEmpty()) {
                            if (mapStationResult.containsKey(nodeB)) {
                                mapStationResult.get(nodeB).addAll(lstStationResult);
                            } else {
                                mapStationResult.put(nodeB, lstStationResult);
                            }
                        }
                    }

                } catch (Exception e) {
                    if (e instanceof MessageException) {
                        MessageUtil.setErrorMessage(e.getMessage());
                    } else {
                        LOGGER.error(e.getMessage(), e);
                        throw new Exception(e.getMessage());
                    }
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeBRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeBMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
            currStationPlan.setProficientType(3L);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> stationDetails = new ArrayList<>();
            StationDetail stationDetail;
            StationResult stationResult;
            for (String nodeB : mapNodeBParam.keySet()) {
                List<StationResult> lstStationResult = new ArrayList<>();
                stationDetail = new StationDetail();
                stationDetail.setStationPlan(currStationPlan);
                stationDetail.setStationCode(nodeB);
                stationDetail.setRncBsc(MessageUtil.getResourceBundleMessage("label.nodeB.run"));
                stationDetail.setCellCode(nodeB);
                stationDetail.setUpdateTime(new Date());

                stationDetails.add(stationDetail);
                if (!lstStationResult.isEmpty()) {
                    if (mapStationResult.containsKey(nodeB)) {
                        mapStationResult.get(nodeB).addAll(lstStationResult);
                    } else {
                        mapStationResult.put(nodeB, lstStationResult);
                    }
                }
            }
            stationPlanService.saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(stationDetails);
            //Sinh mop RNC, BSC
            createMopUpDowngrade(mapNodeBParam, stationConfigImports, mapNodeBRncBsc, mapNodeBMsc, typeAction);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void createMopUpDowngrade(HashMap<String, HashMap<String, String>> mapNodeParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, Long type) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        createFlowRunActionUpDowngrade(mapNodeParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports, type);
    }

    protected boolean createFlowRunActionUpDowngrade(HashMap<String, HashMap<String, String>> mapNodeParam, HashMap<String, String> mapNodeRncBsc, HashMap<String, String> mapNodeMsc, List<StationConfigImport> stationConfigImports, Long type) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        String nodeCodeRncBsc = "NODEB_RUN";
        if (!stationConfigImports.isEmpty()) {
            for (StationConfigImport sci : stationConfigImports) {
                flowTemplates = new FlowTemplatesServiceImpl().findById(sci.getTemplateId());
                break;
            }
        } else {
            throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
        }

        if (flowTemplates == null) {
            throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
        } else {
            if (flowTemplates.getStatus() != 9) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
            }
        }

        try {
            String filePutOSS;
            if ("3G".equals(currStationPlan.getNetworkType())) {
                filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                        + "-" + "UPGRADE_DOWNGRADE" + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                        + "-" + currStationPlan.getNetworkType().toUpperCase()
                        + "-NODEB_" + nodeCodeRncBsc.toUpperCase()
                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            } else {
                filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                        + "-" + "UPGRADE_DOWNGRADE" + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                        + "-" + currStationPlan.getNetworkType().toUpperCase()
                        + "-BTS_" + nodeCodeRncBsc.toUpperCase()
                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            }
            generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();
            flowRunAction.setCrNumber(Config.CR_DEFAULT);
            flowRunAction.setFlowRunName(filePutOSS);
//                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplates);
            flowRunAction.setExecuteBy(userCreate);
            flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
            //Lay danh sach param tu bang 
            generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
            Map<String, String> mapParamValues;
            generateFlowRunController.loadGroupAction(0l);
            List<Node> nodeInPlan;
            HashMap<String, Object> filters = new HashMap<>();
            filters.put("nodeCode-EXAC", nodeCodeRncBsc);
            List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
            Node nodeRncBsc;
            if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
            } else {
                nodeRncBsc = nodeRncs.get(0);
            }

            nodeInPlan = new ArrayList<>();
            generateFlowRunController.getNodes().add(nodeRncBsc);
            nodeInPlan.add(nodeRncBsc);
            //Tham so theo tung subnode
            String ipNodeBAll = null;
            for (String subFlowRun : mapNodeParam.keySet()) {
                mapParamValues = mapNodeParam.get(subFlowRun);
                if (mapParamValues.containsKey(MessageUtil.getResourceBundleMessage("label.ip"))) {
                    if (mapParamValues.get(MessageUtil.getResourceBundleMessage("label.ip")) == null) {
                        MessageUtil.getResourceBundleMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
                        continue;
                    }
                    if (ipNodeBAll != null) {
                        ipNodeBAll = ipNodeBAll + ";" + mapParamValues.get(MessageUtil.getResourceBundleMessage("label.ip"));
                    } else {
                        ipNodeBAll = mapParamValues.get(MessageUtil.getResourceBundleMessage("label.ip"));
                    }
                } else {
                    MessageUtil.getResourceBundleMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
                    continue;
                }
                generateFlowRunController.getMapSubFlowRunNodes().put(subFlowRun, nodeInPlan);
                for (Node nodeTemp : nodeInPlan) {

                    generateFlowRunController.loadGroupAction(subFlowRun, nodeTemp);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(subFlowRun, nodeTemp);

                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                        if (currStationPlan.getNetworkType().equals("3G")) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", nodeTemp.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }

                    }
                    for (GroupAction groupAction : generateFlowRunController.getMapGroupAction().get(subFlowRun + "#" + nodeTemp.getNodeCode())) {
                        if (groupAction.getGroupActionName().equals(Constants.StationUpDowngradeGroupActionName.CheckAfterExecuting)) {
                            groupAction.setDeclare(false);
                        }
                        //Truong hop chi co upgrade
                        if (type.equals(1L)) {
                            if (groupAction.getGroupActionName().equals(Constants.StationUpDowngradeGroupActionName.Executing_Downgrade)) {
                                groupAction.setDeclare(false);
                            }
                        } else if (type.equals(2L)) {
                            if (groupAction.getGroupActionName().equals(Constants.StationUpDowngradeGroupActionName.Executing_Upgrade)) {
                                groupAction.setDeclare(false);
                            }
                        }
                    }

                }

            }
            String subFlowRun = "CheckAfterRun";
            generateFlowRunController.getMapSubFlowRunNodes().put(subFlowRun, nodeInPlan);
            mapParamValues = new HashMap<>();
            mapParamValues.put("ip_all", ipNodeBAll);
            for (Node nodeTemp : nodeInPlan) {
                generateFlowRunController.loadGroupAction(subFlowRun, nodeTemp);
                List<ParamValue> paramValues = generateFlowRunController.getParamInputs(subFlowRun, nodeTemp);
                for (ParamValue paramValue : paramValues) {
                    if (paramValue.getParamInput().getReadOnly()) {
                        continue;
                    }
                    Object value = null;
                    try {
                        value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    if (value != null) {
                        paramValue.setParamValue(value.toString());
                    }
                }
                for (GroupAction groupAction : generateFlowRunController.getMapGroupAction().get(subFlowRun + "#" + nodeTemp.getNodeCode())) {
                    if (!groupAction.getGroupActionName().equals(Constants.StationUpDowngradeGroupActionName.CheckAfterExecuting)) {
                        groupAction.setDeclare(false);
                    }
                }

            }
            boolean saveDT = generateFlowRunController.saveDT();
            if (saveDT) {
                try {
                    //Bat dau xu ly day file OSS
                    //Tao file

                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                    stationFlowRunAction.setFlowRunAction(flowRunAction);
                    stationFlowRunAction.setNode(nodeRncBsc);
                    stationFlowRunAction.setStationPlan(currStationPlan);
                    stationFlowRunAction.setType(1L);
                    stationFlowRunAction.setUpdateTime(new Date());
                    stationFlowRunActiones.add(stationFlowRunAction);
                    MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeCodeRncBsc));

                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
                //Xu ly day du lieu vao 

            }
//                return saveDT;
        } catch (Exception e) {
            if (e instanceof MessageException) {
                throw e;
            } else {
                LOGGER.error(e.getMessage(), e);
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
            }
        } finally {
            //Tam biet
        }

        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ChangeSD_HR Force">
    private void analyseDataChangeSdHrForceEricsson(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            HashMap<String, HashMap<String, String>> bscParamMapForMopHR = new HashMap<>();
            HashMap<String, HashMap<String, String>> bscParamMapForMopSD = new HashMap<>();
            HashMap<String, String> bscMopParam;
            ChangeHrSdResource changeHrSdResource;
            Long nTRx;
            Double offerTraffic;

            // Get data from imported xls file
            for (String sheetName : mapCellObjectImports.keySet()) {
                List<BasicDynaBean> rows = (List<BasicDynaBean>) mapCellObjectImports.get(sheetName);
                for (BasicDynaBean row : rows) {
                    String bsc = (String) row.get("bsc");
                    String cell = (String) row.get("cell");
                    String estimateTraffic = (String) row.get("estimate_traffic");
                    String zone = (String) row.get("zone");
                    String hr = (String) row.get("hr");
                    String sd = (String) row.get("sd");

                    // Get traffic offer from DB
                    Map<String, Object> filters = new HashMap<>();
                    filters.put("rncBsc", bsc);
                    filters.put("cellCode", cell);
                    filters.put("type", 0L);
                    List<ChangeHrSdResource> changeHrSdResources = new ChangeHrSdResourceServiceImpl().findList(filters);
                    if (changeHrSdResources.size() > 0) {
                        changeHrSdResource = changeHrSdResources.get(0);
                        nTRx = changeHrSdResource.getTrx();
                        offerTraffic = changeHrSdResource.getTrafficOffer();
                    } else {
                        MessageUtil.setErrorMessage("Cell data is not exited. Cell code: " + cell);
                        return;
                    }

                    if (nTRx == null || offerTraffic == null) {
                        MessageUtil.setErrorMessage("Cell data is not exited. Cell code: " + cell);
                        continue;
                    }
                    Double tu = null;
                    if (estimateTraffic != null && !"".equals(estimateTraffic)) {
                        tu = (Double.parseDouble(estimateTraffic) / offerTraffic) * 100;
                    }

                    Long hrval = null;
                    Long sdval = null;
                    try {
                        if (null == hr || "".equals(hr)) {
                            if (tu != null) {
                                hrval = calculateHR(tu);
                            }
                        } else {
                            hrval = Long.valueOf(hr);
                        }
                        if (null == sd || "".equals(sd)) {
                            if (tu != null) {
                                sdval = calculateSD(zone, tu, nTRx);
                            }
                        } else {
                            sdval = Long.valueOf(sd);
                        }
                    } catch (Exception ex) {
                        MessageUtil.setErrorMessage("Rule Error!");
                        throw ex;
                    }
                    if (hrval != null) {
                        if (null == bscParamMapForMopHR.get(bsc)) {
                            bscMopParam = new HashMap<>();
                            bscMopParam.put("cell", cell);
                            bscMopParam.put("hrval", String.valueOf(hrval));
                            bscMopParam.put("sdval", String.valueOf(sdval));
                            bscParamMapForMopHR.put(bsc, bscMopParam);
                        } else {
                            bscMopParam = bscParamMapForMopHR.get(bsc);
                            bscMopParam.put("cell", bscMopParam.get("cell") + ";" + cell);
                            bscMopParam.put("hrval", bscMopParam.get("hrval") + ";" + String.valueOf(hrval));
                            bscMopParam.put("sdval", bscMopParam.get("sdval") + ";" + String.valueOf(sdval));
                        }
                    }

                    if (sdval != null) {
                        if (null == bscParamMapForMopSD.get(bsc)) {
                            bscMopParam = new HashMap<>();
                            bscMopParam.put("cell", cell);
                            bscMopParam.put("hrval", String.valueOf(hrval));
                            bscMopParam.put("sdval", String.valueOf(sdval));
                            bscParamMapForMopSD.put(bsc, bscMopParam);
                        } else {
                            bscMopParam = bscParamMapForMopSD.get(bsc);
                            bscMopParam.put("cell", bscMopParam.get("cell") + ";" + cell);
                            bscMopParam.put("hrval", bscMopParam.get("hrval") + ";" + hrval);
                            bscMopParam.put("sdval", bscMopParam.get("sdval") + ";" + sdval);
                        }
                    }

                }
            }

            // Insert data to database
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
            // Save change plan
            currStationPlan.setProficientType(Config.IS_RULE_CHANGE_FORCE);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            Map<String, Object> filters = new HashMap<String, Object>();
            if (bscParamMapForMopHR.size() > 0) {
                //Insert tram vao DB
                List<StationDetail> stationDetails = new ArrayList<>();
                StationDetail stationDetail;
                for (String bsc : bscParamMapForMopHR.keySet()) {
//                    List<StationResult> lstStationResult = new ArrayList<>();
                    stationDetail = new StationDetail();
                    stationDetail.setStationPlan(currStationPlan);
                    stationDetail.setStationCode("HR");
                    stationDetail.setRncBsc(bsc);
                    stationDetail.setCellCode(bscParamMapForMopHR.get(bsc).get("cell"));
                    stationDetails.add(stationDetail);
                }

                // Sinh mop BSC cho HR
                filters.put("fileName", Constants.fileNameTemplate.SD_HR.Ericsson_2G);
                filters.put("bscType", "HR");
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                createFlowRunActionForceChangeSdHr("HR", bscParamMapForMopHR, stationConfigImports.get(0).getTemplateId(), stationDetails);
            }
            if (bscParamMapForMopSD.size() > 0) {
                //Insert tram vao DB
                List<StationDetail> stationDetails = new ArrayList<>();
                StationDetail stationDetail;
                for (String bsc : bscParamMapForMopHR.keySet()) {
//                    List<StationResult> lstStationResult = new ArrayList<>();
                    stationDetail = new StationDetail();
                    stationDetail.setStationPlan(currStationPlan);
                    stationDetail.setStationCode("SD");
                    stationDetail.setRncBsc(bsc);
                    stationDetail.setCellCode(bscParamMapForMopHR.get(bsc).get("cell"));
                    stationDetails.add(stationDetail);
                }
                // Sinh mop BSC cho SD
                filters.put("fileName", Constants.fileNameTemplate.SD_HR.Ericsson_2G);
                filters.put("bscType", "SD");
                stationConfigImports = new StationConfigImportlServiceImpl().findList(filters);
                createFlowRunActionForceChangeSdHr("SD", bscParamMapForMopSD, stationConfigImports.get(0).getTemplateId(), stationDetails);
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    public void analyseDataChangeSdHrForce(HashMap<String, List<StationConfigImport>> mapImportFileSheet, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB

            for (String sheetName : mapCellObjectImports.keySet()) {
                Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object bscRnc;
                        bscRnc = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                        if (bscRnc != null) {
                            multimapParam.put(bscRnc.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.BSC")));
                    }
                }

                //xu ly map
                HashMap<String, HashMap<String, String>> mapNodeParam = new HashMap<>();

                for (String nodeB : multimapParam.keySet()) {
                    try {
                        boolean checkCreateMop = true;
//                        List<StationResult> lstStationResult = new ArrayList<>();
                        HashMap<String, String> mapParam = new HashMap<>();
                        Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                        try {
                            for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                                Map<String, String> mapInBasicDynaBean;
                                try {
                                    mapInBasicDynaBean = basicDynaBean.getMap();
                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                } catch (Exception e) {
                                    logger.error(e.getMessage(), e);
                                }
                            }
                        } catch (Exception e) {
                            throw e;
                        }

                        if (checkCreateMop) {
                            mapNodeParam.put(nodeB, mapParam);
                        }

                    } catch (Exception e) {
//                    if (e instanceof MessageException) {
//                        MessageUtil.setErrorMessage(e.getMessage());
//                    } else {
////                    MessageUtil.setErrorMessageFromRes("error.cannot.create.mop");
//                        LOGGER.error(e.getMessage(), e);
//                        throw new Exception(e.getMessage());
//                    }
                        throw e;
                    }
                }

                currStationPlan.setUpdateTime(new Date());
                currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
                currStationPlan.setProficientType(Config.IS_RULE_CHANGE_FORCE);
                if (fileOutPut != null) {
                    currStationPlan.setFileName(fileOutPut.getName());
                    currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                    currStationPlan.setFilePath(fileOutPut.getPath());
                }

                //Insert tram vao DB
                List<StationDetail> stationDetails = new ArrayList<>();
                StationDetail stationDetail;
                StationResult stationResult;
//                Map<String, Object> filters = new HashMap<>();
//                String bscType = null;
//                String btsType = null;
                for (String bsc : mapNodeParam.keySet()) {
//                    List<StationResult> lstStationResult = new ArrayList<>();
                    stationDetail = new StationDetail();
                    stationDetail.setStationPlan(currStationPlan);
                    stationDetail.setStationCode(sheetName);
                    stationDetail.setRncBsc(bsc);
                    stationDetail.setCellCode(mapNodeParam.get(bsc).get("cellname"));
                    stationDetails.add(stationDetail);
                }
                createMopChangeSdHrForce(mapNodeParam, mapImportFileSheet.get(sheetName).get(0).getTemplateId(), sheetName, stationDetails);
            }
            //Sinh mop RNC, BSC
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    private boolean createFlowRunActionForceChangeSdHr(String dtName, HashMap<String, HashMap<String, String>> bscParamMapForMop, Long templateId, List<StationDetail> stationDetails) throws MessageException, Exception {
        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActions = new ArrayList<>();
        FlowTemplates flowTemplates = null;

        try {

            flowTemplates = new FlowTemplatesServiceImpl().findById(templateId);

            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                return false;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    return false;
                }
            }

            try {
                String filePutOSS;
                filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase() + "- CHANGE_FORCE_  " + dtName
                        + "-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                        + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

                generateFlowRunController = new GenerateFlowRunController();
                FlowRunAction flowRunAction = new FlowRunAction();
                flowRunAction.setCrNumber(Config.CR_DEFAULT);
                flowRunAction.setFlowRunName(filePutOSS);
//                    filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);

                //Lay danh sach param tu bang
                generateFlowRunController.setNodes(new ArrayList<Node>());
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                List<Node> listNodeRncBsc = new ArrayList<>();
                if (bscParamMapForMop.size() == 0) {
//                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.import.param.fail"));
                    return false;
                }
                for (String bsc : bscParamMapForMop.keySet()) {
                    HashMap<String, Object> filters = new HashMap<>();
                    filters.put("nodeCode-EXAC", bsc);
                    List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                    Node nodeRncBsc;
                    if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                        throw new Exception("Khong tim thay node RNC hoac tim thay lon hon 1 ban ghi");
                    } else {
                        nodeRncBsc = nodeRncs.get(0);
                    }

                    mapParamValues = bscParamMapForMop.get(bsc);

                    nodeInPlan = new ArrayList<>();

                    generateFlowRunController.getNodes().add(nodeRncBsc);
                    listNodeRncBsc.add(nodeRncBsc);
                    nodeInPlan.add(nodeRncBsc);

                    for (Node node : nodeInPlan) {
                        generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                        List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                        for (ParamValue paramValue : paramValues) {
                            LOGGER.info("Show ParamCode: " + paramValue.getParamCode());

                            if (paramValue.getParamInput().getReadOnly()) {
                                continue;
                            }

                            Object value = null;
                            try {
                                value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }

                            ResourceBundle bundle = ResourceBundle.getBundle("cas");
                            if (bundle.getString("service").contains("10.61.127.190")) {
                                if (value == null || value.toString().isEmpty()) {
                                    value = "TEST_NOT_FOUND";
                                }
                            }
                            if (value != null) {
                                paramValue.setParamValue(value.toString());
                            }
                        }
                    }
                }

                boolean saveDT = generateFlowRunController.saveDT();
                //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_start
                generateFlowRunController.loadParamDt();
                //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_end
                if (saveDT) {
                    try {
                        stationPlanService.saveOrUpdate(currStationPlan);
                        new StationDetailServiceImpl().saveOrUpdate(stationDetails);
                        // Save flow to database
                        StationFlowRunAction stationFlowRunAction;
                        for (Node nodeRncBsc : listNodeRncBsc) {
                            stationFlowRunAction = new StationFlowRunAction();
                            stationFlowRunAction.setFlowRunAction(flowRunAction);
                            stationFlowRunAction.setNode(nodeRncBsc);
                            stationFlowRunAction.setType(Config.IS_RULE_CHANGE_FORCE);
                            stationFlowRunAction.setUpdateTime(new Date());
                            stationFlowRunAction.setStationPlan(currStationPlan);
                            stationFlowRunActions.add(stationFlowRunAction);
                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeRncBsc.getNodeCode()));
                        }
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                }

            } catch (Exception e) {
                if (e instanceof MessageException) {
                    MessageUtil.setErrorMessage(e.getMessage());
                } else {
                    LOGGER.error(e.getMessage(), e);
                }
                throw new Exception(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
            } finally {
                //Tam biet
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActions);
        return true;
    }

    private Long calculateSD(String zone, Double tu, Long nTRx) throws Exception {
        Long sd;
        String clause;
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        if (zone.equals(Constants.sdZoneType.HOTSPOT)) {
            if (tu < hotspot1SDRightValue) {
                clause = hotspot1SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else if (tu >= hotspot2SDLeftValue && tu <= hotspot2SDRightValue) {
                clause = hotspot2SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else {
                clause = hotspot3SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            }
        } else {
            if (tu < normal1SDRightValue) {
                clause = normal1SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else if (tu > normal2SDLeftValue && tu <= normal2SDRightValue) {
                clause = normal2SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else if (tu > normal3SDLeftValue && tu <= normal3SDRightValue) {
                clause = normal3SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            } else {
                clause = normal4SDValue.toLowerCase().replace("ntrx", String.valueOf(nTRx));
            }
        }
        Object result = engine.eval(clause);
        sd = ((Number) result).longValue();
        return sd;
    }

    private Long calculateHR(Double tu) {
        Long hr;
        if (tu < normal1HRRightValue) {
            hr = normal1HRValue;
        } else if (tu >= normal2HRLeftValue && tu < normal2HRRightValue) {
            hr = normal2HRValue;
        } else if (tu >= normal3HRLeftValue && tu <= normal3HRRightValue) {
            hr = normal3HRValue;
        } else if (tu > normal4HRLeftValue && tu <= normal4HRRightValue) {
            hr = normal4HRValue;
        } else {
            hr = normal5HRValue;
        }
        return hr;
    }

    public void createMopChangeSdHrForce(HashMap<String, HashMap<String, String>> mapNodeParam, Long templateId, String dtName, List<StationDetail> stationDetails) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        createFlowRunActionForceChangeSdHr(dtName, mapNodeParam, templateId, stationDetails);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="LoadLicense">
    public void analyseDataLoadLicense(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports, List<File> lstFilePutSystem) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            HashMap<String, String> mapFileLicense = new HashMap<>();
            for (File file : lstFilePutSystem) {
                mapFileLicense.put(file.getName(), file.getPath());
            }
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        multimapParam.put(sheetName, basicDynaBean);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();
            HashMap<String, HashMap<String, String>> mapNodeBParamTemp = new HashMap<>();
            HashMap<String, String> mapParamTemp = new HashMap<>();
            for (String key : multimapParam.keySet()) {
                try {

                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(key);
                    if (key.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.station.sheet.loadLicense"))) {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            HashMap<String, String> mapParam = new HashMap<>();
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                String fileName = "";
                                String pathFile = "";
                                String pathFileServer = "";
                                String fileNameLicense;
                                Object nodeB;
                                try {
                                    nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                                } catch (Exception e) {
                                    LOGGER.error(e.getMessage(), e);
                                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.site")));
                                }
                                if (nodeB == null || "".equals(nodeB.toString())) {
                                    continue;
                                }
                                String ip;
                                try {
                                    ip = mapInBasicDynaBean.get(MessageUtil.getResourceBundleMessage("label.ip").toLowerCase());
                                    convertIp(ip, 1, -2);
                                } catch (Exception e) {
                                    LOGGER.error(e.getMessage(), e);
                                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.ip")));
                                }
                                if (ip == null || "".equals(ip.toString())) {
                                    continue;
                                }
                                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.ip").toLowerCase())) {
                                    mapParam.put(MessageUtil.getResourceBundleMessage("label.ip").toLowerCase(), mapParam.get(MessageUtil.getResourceBundleMessage("label.ip").toLowerCase()) + Config.SPLITTER_VALUE + ip);
                                } else {
                                    mapParam.put(MessageUtil.getResourceBundleMessage("label.ip").toLowerCase(), ip);
                                }
                                //Get file load license
                                boolean checkExitsFileLicense = false;
                                try {
                                    if ("3G".equals(currStationPlan.getNetworkType()) && mapInBasicDynaBean.containsKey(MessageUtil.getResourceBundleMessage("label.station.fp"))) {
                                        fileNameLicense = mapInBasicDynaBean.get(MessageUtil.getResourceBundleMessage("label.station.fp"));
                                        if ("".equals(fileNameLicense)) {
                                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.not.exits.fileLoadLicense"), nodeB.toString()));
                                            continue;
                                        }
                                        for (String file : mapFileLicense.keySet()) {
                                            if (file.startsWith(fileNameLicense)) {
                                                fileName = file;
                                                pathFile = mapFileLicense.get(file);
                                                pathFileServer = File.separator + MessageUtil.getResourceBundleMessage("lable.cmd.ftp.pathFileServie_3G") + File.separator + date + File.separator + fileName;
                                                checkExitsFileLicense = true;
                                                break;
                                            }
                                        }
                                        if (!checkExitsFileLicense) {
                                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.not.exits.fileLoadLicense"), nodeB.toString()));
                                            continue;
                                        }
                                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName").toLowerCase())) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName").toLowerCase(), mapParam.get(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName")) + Config.SPLITTER_VALUE + fileName);
                                        } else {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName").toLowerCase(), fileName);
                                        }
                                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile").toLowerCase())) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile").toLowerCase(), mapParam.get(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile")) + Config.SPLITTER_VALUE + pathFile);
                                        } else {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile").toLowerCase(), pathFile);
                                        }
                                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFileServer").toLowerCase())) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFileServer").toLowerCase(), mapParam.get(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile")) + Config.SPLITTER_VALUE + pathFileServer);
                                        } else {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFileServer").toLowerCase(), pathFileServer);
                                        }
                                    } else if ("4G".equals(currStationPlan.getNetworkType()) && mapInBasicDynaBean.containsKey(MessageUtil.getResourceBundleMessage("label.station.enodeb_name"))) {
                                        fileNameLicense = mapInBasicDynaBean.get(MessageUtil.getResourceBundleMessage("label.station.enodeb_name"));
                                        if ("".equals(fileNameLicense)) {
                                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.not.exits.fileLoadLicense"), nodeB.toString()));
                                            continue;
                                        }
                                        for (String file : mapFileLicense.keySet()) {
                                            if (file.startsWith(fileNameLicense) && !file.endsWith("_info.xml")) {
                                                fileName = file;
                                                pathFile = mapFileLicense.get(file);
                                                pathFileServer = File.separator + MessageUtil.getResourceBundleMessage("lable.cmd.ftp.pathFileServie_3G") + File.separator + date + File.separator + fileName;
                                                break;
                                            }
                                        }
                                        if (!checkExitsFileLicense) {
                                            MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.not.exits.fileLoadLicense"), nodeB.toString()));
                                            continue;
                                        }
                                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName").toLowerCase())) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName").toLowerCase(), mapParam.get(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName")) + Config.SPLITTER_VALUE + fileName);
                                        } else {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpFileName").toLowerCase(), fileName);
                                        }
                                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile").toLowerCase())) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile").toLowerCase(), mapParam.get(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile")) + Config.SPLITTER_VALUE + pathFile);
                                        } else {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile").toLowerCase(), pathFile);
                                        }
                                        if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFileServer").toLowerCase())) {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFileServer").toLowerCase(), mapParam.get(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFile")) + Config.SPLITTER_VALUE + pathFileServer);
                                        } else {
                                            mapParam.put(MessageUtil.getResourceBundleMessage("label.cmd.ftp.ftpPathFileServer").toLowerCase(), pathFileServer);
                                        }
                                    }


                                } catch (Exception ex) {
                                    logger.error(ex.getMessage(), ex);
                                }
                                for (String paramCode : mapInBasicDynaBean.keySet()) {
                                    if ("3G".equals(currStationPlan.getNetworkType()) && !paramCode.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.ip"))) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    } else if ("4G".equals(currStationPlan.getNetworkType()) && !paramCode.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.ip"))) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                }
                                mapNodeBParam.put(nodeB.toString(), mapParam);
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                        if (mapNodeBParam.size() <= 0) {
                            throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.site")));
                        }
                    } else {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            HashMap<String, String> mapParam = new HashMap<>();
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                Object nodeB;
                                try {
                                    nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                                } catch (Exception e) {
                                    LOGGER.error(e.getMessage(), e);
                                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.site")));
                                }
                                if (nodeB == null || "".equals(nodeB.toString())) {
                                    continue;
                                }

                                for (String paramCode : mapInBasicDynaBean.keySet()) {
                                    if (!paramCode.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.ip")) && !paramCode.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.station.site"))) {
                                        if (mapParamTemp != null) {
                                            if (!mapParamTemp.containsKey(paramCode)) {
                                                mapParamTemp.put(paramCode, "");
                                            }
                                        }
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                }
                                mapNodeBParamTemp.put(nodeB.toString(), mapParam);
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }

                    }
                    //Map sang sheet load license

                } catch (Exception e) {
                    throw e;
                }
            }
            if (mapNodeBParamTemp.size() > 0) {
                for (String nodeB : mapNodeBParam.keySet()) {
                    if (mapNodeBParamTemp.containsKey(nodeB)) {
                        mapNodeBParam.get(nodeB).putAll(mapNodeBParamTemp.get(nodeB));
                    } else {
                        mapNodeBParam.get(nodeB).putAll(mapParamTemp);
                    }
                }
            }

            //Luu database
            //Insert vao DB
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
            currStationPlan.setProficientType(Config.IS_RULE_LOAD_LICENSE);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> stationDetails = new ArrayList<>();
            StationDetail stationDetail;
            StationResult stationResult;
//            Map<String, Object> filters = new HashMap<>();
//            String bscType = null;
//            String btsType = null;
            for (String nodeB : mapNodeBParam.keySet()) {
                List<StationResult> lstStationResult = new ArrayList<>();
                stationDetail = new StationDetail();
                stationDetail.setStationPlan(currStationPlan);
                stationDetail.setStationCode(nodeB);


                stationDetails.add(stationDetail);
                if (!lstStationResult.isEmpty()) {
                    if (mapStationResult.containsKey(nodeB)) {
                        mapStationResult.get(nodeB).addAll(lstStationResult);
                    } else {
                        mapStationResult.put(nodeB, lstStationResult);
                    }
                }
            }
            stationPlanService.saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(stationDetails);
            //Sinh mop RNC, BSC
            createMopLoadLicense(mapNodeBParam, stationConfigImports);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void createMopLoadLicense(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports) throws Exception {
        //Sinh mop cho RNC, MSC
        createFlowRunActionLoadLicense(mapNodeBParam, stationConfigImports);
    }

    protected boolean createFlowRunActionLoadLicense(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports) throws Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates;
        String nodeRun = "NODEB_RUN";
        if ("3G".equals(currStationPlan.getNetworkType())) {
            nodeRun = "NODEB_RUN";
        }
        Map<String, String> mapParamValuesFinal = new HashMap<>();
        for (String node : mapNodeBParam.keySet()) {
            for (String paramCode : mapNodeBParam.get(node).keySet()) {
                if (mapParamValuesFinal.containsKey(paramCode)) {
                    mapParamValuesFinal.put(paramCode, mapParamValuesFinal.get(paramCode) + ";" + mapNodeBParam.get(node).get(paramCode));
                } else {
                    mapParamValuesFinal.put(paramCode, mapNodeBParam.get(node).get(paramCode));
                }
            }
        }


        String date = new SimpleDateFormat("HH_dd_MM_yyyy").format(new Date());
        mapParamValuesFinal.put(MessageUtil.getResourceBundleMessage("label.date").toLowerCase(), date);

        flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
        if (flowTemplates == null) {
            MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
        } else {
            if (flowTemplates.getStatus() != 9) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
            }
        }

        try {
            String flowRunName;
            flowRunName = currStationPlan.getCrNumber().trim().toUpperCase()
                    + "-Load_License-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                    + "-" + currStationPlan.getNetworkType().toUpperCase()
                    + "-Site_" + mapParamValuesFinal.get("site").replaceAll(";", "_").toUpperCase()
                    + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();
            flowRunAction.setCrNumber(Config.CR_DEFAULT);
            flowRunAction.setFlowRunName(flowRunName);
            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplates);
            flowRunAction.setExecuteBy(userCreate);
            flowRunAction.setCreateBy(userCreate);
            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
            //Lay danh sach param tu bang
            generateFlowRunController.setNodes(new ArrayList<Node>());

            generateFlowRunController.loadGroupAction(0l);
            List<Node> nodeInPlan;
            HashMap<String, Object> filters = new HashMap<>();
            filters.put("nodeCode-EXAC", nodeRun);
            List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
            Node nodeRncBsc;
            if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
            } else {
                nodeRncBsc = nodeRncs.get(0);
            }

            nodeInPlan = new ArrayList<>();
            generateFlowRunController.getNodes().add(nodeRncBsc);

            nodeInPlan.add(nodeRncBsc);

            for (Node node : nodeInPlan) {
                generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                for (ParamValue paramValue : paramValues) {
                    LOGGER.info("Show ParamCode: " + paramValue.getParamCode());
                    if (paramValue.getParamInput().getReadOnly()) {
                        continue;
                    }
                    Object value = null;
                    try {
                        value = mapParamValuesFinal.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
//                    ResourceBundle bundle = ResourceBundle.getBundle("cas");
                    if (value != null) {
                        paramValue.setParamValue(value.toString());
                    }
//                    if ("3G".equals(currStationPlan.getNetworkType())) {
//                        if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                            MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                    .replace("{1}", paramValue.getParamCode()));
//                            return false;
//                        }
//                    }
                }
            }

            boolean saveDT = generateFlowRunController.saveDT();
            if (saveDT) {
                try {
                    //Bat dau xu ly day file OSS
                    //Tao file
//                    StringBuilder stringBuilder = new StringBuilder();
                    StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                    stationFlowRunAction.setFlowRunAction(flowRunAction);
                    stationFlowRunAction.setNode(nodeRncBsc);
                    stationFlowRunAction.setStationPlan(currStationPlan);
                    stationFlowRunAction.setType(Config.IS_RULE_CHANGE_FORCE);
                    stationFlowRunAction.setUpdateTime(new Date());
                    stationFlowRunActiones.add(stationFlowRunAction);

                    MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeRun));
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            if (e instanceof MessageException) {
                throw e;
            } else {
                LOGGER.error(e.getMessage(), e);
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
            }
        } finally {
            //Tam biet
        }

        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ChangeParam">
    public void analyseDataChangeParam(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.rncBsc").toLowerCase());
                        if (nodeB != null) {
                            multimapParam.put(nodeB.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();

            for (String nodeB : multimapParam.keySet()) {
                boolean checkCreateMop = true;
                List<StationResult> lstStationResult = new ArrayList<>();
                try {

                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    HashMap<String, HashMap<String, String>> mapSheetParam = new HashMap<>();
                    HashMap<String, String> mapParamFinal = new HashMap<>();
                    try {
                        HashMap<String, String> mapParam;
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            String sheetName = basicDynaBean.getDynaClass().getName().trim().toLowerCase().replace(" ", "_").replace(".", "_");
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                if (mapSheetParam.containsKey(sheetName)) {
                                    mapParam = mapSheetParam.get(sheetName);

                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                    mapSheetParam.put(sheetName, mapParam);
                                } else {
                                    mapParam = new HashMap<>();
                                    for (String paramCode : mapInBasicDynaBean.keySet()) {
                                        if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                            mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        } else {
                                            mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                        }
                                    }
                                    mapSheetParam.put(sheetName, mapParam);
                                }

                            } catch (Exception e) {
                                LOGGER.error(e.getMessage(), e);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }
                    for (String nameSheet : mapSheetParam.keySet()) {
//                        calculateParam(mapSheetParam.get(nameSheet), lstParamDefault, LOGGER);
                        StationResult stationResult;
                        for (String paramCode : mapSheetParam.get(nameSheet).keySet()) {
//                        if (mapParam.get(paramCode) == null || mapParam.get(paramCode).equals("") || mapParam.get(paramCode).contains(";;")) {
//                            checkCreateMop = false;
//                            stationResult = new StationResult();
//                            stationResult.setParamCode(paramCode);
//                            stationResult.setParamValue(mapParam.get(paramCode) == null ? "" : mapParam.get(paramCode));
//                            stationResult.setNodeCode(nodeB);
//                            stationResult.setResult("NOK, Khong sinh mop");
//                            stationResult.setType("Relation");
//                            stationResult.setDetailResult("Khong du tham so trong file import");
//                            lstStationResult.add(stationResult);
//                        } else {
                            if (mapParamFinal.containsKey(nameSheet + "-" + paramCode.trim().toLowerCase())) {
                                mapParamFinal.put((nameSheet + "-" + paramCode.trim().toLowerCase()), mapParamFinal.get((nameSheet + "-" + paramCode.trim().toLowerCase())) + Config.SPLITTER_VALUE + (mapSheetParam.get(nameSheet).get(paramCode) == null ? "" : mapSheetParam.get(nameSheet).get(paramCode)));
                            } else {
                                mapParamFinal.put((nameSheet + "-" + paramCode.trim().toLowerCase()), mapSheetParam.get(nameSheet).get(paramCode) == null ? "" : mapSheetParam.get(nameSheet).get(paramCode));
                            }

//                        }
                        }
                    }
//                    for (String paramCode : mapParamFinal.keySet()) {
//                        mapParamFinal.put(paramCode, mapParamFinal.get(paramCode).substring(0, Math.min(3950, mapParamFinal.get(paramCode).length())));
//                    }
                    if (checkCreateMop) {
                        mapNodeBParam.put(nodeB, mapParamFinal);
                    }
                    if (!lstStationResult.isEmpty()) {
                        if (mapStationResult.containsKey(nodeB)) {
                            mapStationResult.get(nodeB).addAll(lstStationResult);
                        } else {
                            mapStationResult.put(nodeB, lstStationResult);
                        }
                    }

                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            currStationRelationPlan = new StationPlan();
            if (currStationPlan != null) {
                currStationRelationPlan.setCrNumber(currStationPlan.getCrNumber().trim());
                currStationRelationPlan.setNetworkType(currStationPlan.getNetworkType() == null ? "" : currStationPlan.getNetworkType());
                currStationRelationPlan.setVendor(currStationPlan.getVendor() == null ? null : currStationPlan.getVendor());
                currStationRelationPlan.setUpdateTime(new Date());
                currStationRelationPlan.setUserCreate(SessionWrapper.getCurrentUsername());
                currStationRelationPlan.setProficientType(Config.IS_RULE_CHANGE_PARAM);
                if (fileOutPut != null) {
                    currStationRelationPlan.setFileName(fileOutPut.getName());
                    currStationRelationPlan.setFileContent(readBytesFromFile(fileOutPut));
                    currStationRelationPlan.setFilePath(fileOutPut.getPath());
                }
            }

            stationPlanService.saveOrUpdate(currStationRelationPlan);
            //Sinh mop RNC, BSC
            createMopChangeParam(mapNodeBParam, stationConfigImports);

        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void createMopChangeParam(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        FlowTemplates flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
        if (flowTemplates == null) {
            throw new MessageException(MessageUtil.getResourceBundleMessage("error.not.found.template"));
        } else {
            if (flowTemplates.getStatus() != 9) {
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
            }
        }
        createFlowRunActionChangeParamGroupNode(mapNodeBParam, flowTemplates);
    }

    protected boolean createFlowRunActionChangeParamGroupNode(HashMap<String, HashMap<String, String>> mapNodeParam, FlowTemplates flowTemplates) throws MessageException, Exception {

        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();

        try {
            String filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                    + "-ChangeParam-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                    + "-" + currStationPlan.getNetworkType().toUpperCase()
                    + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            generateFlowRunController = new GenerateFlowRunController();
            FlowRunAction flowRunAction = new FlowRunAction();
            flowRunAction.setCrNumber(Config.CR_DEFAULT);
            flowRunAction.setFlowRunName(filePutOSS);

            while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
            }
            flowRunAction.setTimeRun(new Date());
            flowRunAction.setFlowTemplates(flowTemplates);
            flowRunAction.setExecuteBy(userCreate);
            flowRunAction.setCreateBy(userCreate);
//                if (mopType != null) {
//                    flowRunAction.setMopType(mopType);
//                } else {
//                    flowRunAction.setMopType(1l);
//                }

            generateFlowRunController.setFlowRunAction(flowRunAction);
            generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
            //Lay danh sach param tu bang
            generateFlowRunController.setNodes(new ArrayList<Node>());
//                Map<String, RiSgRcParam> mapTableParam = new HashMap<>();
            Map<String, String> mapParamValues;
            generateFlowRunController.loadGroupAction(0l);

            List<Node> nodeInPlan;
            List<Node> listNodeRncBsc = new ArrayList<>();
            HashMap<Node, String> mapNodeFileName = new HashMap<>();
            for (String rncBsc : mapNodeParam.keySet()) {

                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", rncBsc);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), rncBsc));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                mapParamValues = mapNodeParam.get(rncBsc);

                nodeInPlan = new ArrayList<>();
                listNodeRncBsc.add(nodeRncBsc);
                mapNodeFileName.put(nodeRncBsc, filePutOSS);
                generateFlowRunController.getNodes().add(nodeRncBsc);
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);

                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly() || paramValue.getParamInput().getInOut()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }

                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
                    }
                }
                mapParamValues.clear();
            }
            boolean saveDT = generateFlowRunController.saveDT();
            //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_start
            generateFlowRunController.loadParamDt();
            //Quytv7 them load param sau khi save DT de chuan hoa tham so ghep_end
            if (saveDT) {
                try {
                    //Bat dau xu ly day file OSS
                    //Tao file
                    for (Node nodeRncBsc : listNodeRncBsc) {
                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                        stationFlowRunAction.setFlowRunAction(flowRunAction);
                        stationFlowRunAction.setNode(nodeRncBsc);
                        stationFlowRunAction.setGroupActionName("Sinh dữ liệu Relation trên RNC");
                        stationFlowRunAction.setStationPlan(currStationRelationPlan);
                        stationFlowRunAction.setType(Config.IS_RULE_CHANGE_PARAM);
                        stationFlowRunAction.setUpdateTime(new Date());
                        stationFlowRunActiones.add(stationFlowRunAction);

                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), nodeRncBsc.getNodeCode()));
                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
                //Xu ly day du lieu vao
            }
        } catch (Exception e) {
            if (e instanceof MessageException) {
                throw e;
            } else {
                LOGGER.error(e.getMessage(), e);
                throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
            }
        } finally {
            //Tam biet
        }

        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Active_Standard">
    public void ValidateNims(ResultDTO resultDTO, HashMap<String, List<StationConfigImport>> mapConfigFile, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) {
        //Validate neu VMSA khong loi thi goi sang NIMS de validate
        //Truong hop nghiep vụ co can check ton tai cell ben NIMS(IS_VALIDATE_NIMS = 1)
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("config");
            URL url = new URL(bundle.getString("ws_nims"));
            for (String sheetName : mapData.keySet()) {
                if (!"cell".equalsIgnoreCase(sheetName)) {
                    continue;
                }
                if (mapConfigFile.get(sheetName).get(0) == null ||
                        mapConfigFile.get(sheetName).get(0).getIsValidateNims() == null ||
                        mapConfigFile.get(sheetName).get(0).getIsValidateNims().equals(0L)) {
                    logger.info("---Truong hop khong can validate ben nims---");
                    continue;
                }
                List<CheckCellForCRForm> listCellCheck = new ArrayList<>();
                CheckCellForCRForm checkCellForCRForm;
                for (int i = 0; i < mapData.get(sheetName).size(); i++) {
                    checkCellForCRForm = new CheckCellForCRForm();
                    checkCellForCRForm.setCellCode(mapData.get(sheetName).get(i).get(mapConfigFile.get(sheetName).get(0).getColumnValidate().toLowerCase()).toString());
                    checkCellForCRForm.setId(String.valueOf(i));
                    checkCellForCRForm.setType(mapConfigFile.get(sheetName).get(0).getNetworkType());
                    listCellCheck.add(checkCellForCRForm);
                }
                if (listCellCheck.size() > 0) {
                    UpdateInfraWSService locator = new UpdateInfraWSService(url);
                    UpdateInfraWS service = locator.getUpdateInfraWSPort();
                    listCellCheck = service.checkCellForCR(listCellCheck);
                }

                for (int i = 0; i < mapData.get(sheetName).size(); i++) {
                    if (!isNullOrEmpty(listCellCheck.get(i).getResult())) {
                        resultDTO.setResultCode(1);
                    }

                    mapData.get(sheetName).get(i).put("vmsa_result_code", (listCellCheck.get(i).getResult() == null || listCellCheck.get(i).getResult().isEmpty()) ? "OK" : "NOK");
                    mapData.get(sheetName).get(i).put("vmsa_result_detail", listCellCheck.get(i).getResult());
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void ValidateDataActiveStandard(HashMap<String, List<?>> mapCellObjectImports, ResultDTO resultDTO, HashMap<String, List<StationConfigImport>> mapImportSheet, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            List<String> lstString3G = new ArrayList<>();
            List<String> lstString2G = new ArrayList<>();
            switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                case Constants.vendorType.ERICSSON:

                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase());

                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.TG_NUMBER").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.CGI").toLowerCase());
                    break;
                case Constants.vendorType.NOKIA:
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.CGI").toLowerCase());
                    break;
                default:
                    break;
            }
            for (String sheetName : mapCellObjectImports.keySet()) {
                lstString3G.add(mapImportSheet.get(sheetName).get(0).getColumnValidate());
                lstString2G.add(mapImportSheet.get(sheetName).get(0).getColumnValidate());
                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                logger.info("---Quytv7 mapCellObjectImports: " + mapCellObjectImports.size());

                BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(0);

                //check cell
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    for (String str : lstString3G) {
                        if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                            resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                            resultDTO.setResultCode(1);
                        }
                    }
                } else if (Config.networkType2G.equalsIgnoreCase(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    for (String str : lstString2G) {
                        if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                            resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                            resultDTO.setResultCode(1);
                        }
                    }
                }

            }
            if (resultDTO.getResultMessage() != null && !resultDTO.getResultMessage().isEmpty()) {
                return;
            }
            Map<String, Object> filters = new HashMap<>();
            for (String sheetName : mapCellObjectImports.keySet()) {

                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    if (sheetName.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.nodeB"))) {
                        continue;
                    }
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstString3G.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }

                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                } else if (Config.networkType2G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstString2G.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }
                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                }

            }
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void analyseDataActiveStandard(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeB;
                        nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                        if (nodeB != null) {
                            multimapParam.put(nodeB.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.site")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();

            for (String nodeB : multimapParam.keySet()) {
                try {
//                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, String> mapParam = new HashMap<>();
                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                for (String paramCode : mapInBasicDynaBean.keySet()) {
                                    if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                        mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    } else {
                                        mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    }
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }

                    for (String paramCode : mapParam.keySet()) {
                        mapParam.put(paramCode, mapParam.get(paramCode));
                    }
                    if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
                            && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase())) {
                        String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
                        String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase()).split(";");
                        String cell_lac_ci = "";
                        for (int i = 0; i < lacs.length; i++) {
                            if (cis[i] != null) {
                                cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
                            }
                        }

                        mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
                    }

                    mapNodeBParam.put(nodeB, mapParam);
                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeBRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeBMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(userCreate);
            currStationPlan.setProficientType(Config.IS_RULE_ACTIVE_STANDARD);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> stationDetails = new ArrayList<>();
            StationDetail stationDetail;
            Map<String, Object> filters = new HashMap<>();
            String bscType = null;
            String btsType = null;
            for (String nodeB : mapNodeBParam.keySet()) {
                stationDetail = new StationDetail();
                stationDetail.setStationPlan(currStationPlan);
                stationDetail.setStationCode(nodeB);

                if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
                        || mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {
                    String[] rncBscs;
                    String[] btsTypes;
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase()).split(";");
                    } else {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase()).split(";");
                    }
                    if (rncBscs.length == 0) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    } else {
                        filters.put("rncBsc", rncBscs[0]);
                        List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);

                        if (lstResource.isEmpty()) {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                    nodeB));
                            mapNodeBParam.remove(nodeB);
//                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
                        } else {
                            mapNodeBRncBsc.put(nodeB, lstResource.get(0).getRncBsc());
                            mapNodeBMsc.put(nodeB, lstResource.get(0).getMsc());
                            stationDetail.setStationResource(lstResource.get(0));
                            stationDetail.setRncBsc(lstResource.get(0).getRncBsc());
                            stationDetail.setCellCode(mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("datatable.header.cellCode")));
                            stationDetail.setUpdateTime(new Date());
                        }
                    }
                } else {
                    throw new AppException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                }
                stationDetails.add(stationDetail);
            }
            new StationPlanServiceImpl().saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(stationDetails);
            //Sinh mop RNC, BSC
            createMopActiveStandard(mapNodeBParam, stationConfigImports, mapNodeBRncBsc, mapNodeBMsc, bscType, btsType);

        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

    }

    public void createMopActiveStandard(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, String bscType, String btsType) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        try {
            createFlowRunActionActiveStandard(mapNodeBParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    protected boolean createFlowRunActionActiveStandard(HashMap<String, HashMap<String, String>> mapNodeBParam, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
        logger.info("vao ham createFlowRunActionActiveStandard");
        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        for (String nodeB : mapNodeBParam.keySet()) {
            logger.info("Bat dau tao mop cho node: " + nodeB);
            flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                continue;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Active_Standard-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-NodeB_" + nodeB.toUpperCase() + "-RNC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Active_Standard-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BTS_" + nodeB.toUpperCase() + "-BSC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                generateFlowRunController.setPathTemplate(pathTemplate);
                FlowRunAction flowRunAction = new FlowRunAction();

                flowRunAction.setCrNumber(Config.CR_DEFAULT);

                flowRunAction.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
                if (currStationPlan.getCreateDtId() != null) {
                    flowRunAction.setCreateDtFromGnocId(currStationPlan.getCreateDtId());
                }
                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang
                generateFlowRunController.setNodes(new ArrayList<Node>());
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", mapNodeBRncBsc.get(nodeB));
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                Node nodeMsc;
                List<Node> lstnodeMsc = new ArrayList<>();
                filters.clear();
                String[] mscStr = mapNodeBMsc.get(nodeB).split(";");
                for (String msc : mscStr) {
                    filters.clear();
                    filters.put("nodeCode-EXAC", msc);
                    List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
                    if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
                    } else {
                        nodeMsc = nodeMscs.get(0);
                        lstnodeMsc.add(nodeMsc);
                    }
                }
                mapParamValues = mapNodeBParam.get(nodeB);
                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_start
                StationConfigImport stationConfigImport = stationConfigImports.get(0);
                if (stationConfigImport != null && !isNullOrEmpty(stationConfigImport.getColumnValidate())) {
                    logger.info("Bat dau cap nhat DataCell cac cell cap nhat sang nims");
                    HashMap<String, String> mapDataCell = new HashMap<>();
                    String cell = mapParamValues.get(stationConfigImport.getColumnValidate().toLowerCase());
                    String[] cells = cell.split(Config.SPLITTER_VALUE);
                    for (String cell1 : cells) {
                        if (!isNullOrEmpty(cell1)) {
                            mapDataCell.put(cell1, stationConfigImport.getNetworkType());
                        }
                    }
                    DataCellUpdateNimsForGnoc dataCellUpdateNimsForGnoc = new DataCellUpdateNimsForGnoc();
                    dataCellUpdateNimsForGnoc.setCrNumber(currStationPlan.getCrNumber().trim());
                    dataCellUpdateNimsForGnoc.setMapDataCell(mapDataCell);
                    dataCellUpdateNimsForGnoc.setClassNimsWS(stationConfigImport.getClassNimsWs());
                    dataCellUpdateNimsForGnoc.setFunctionNimsWs(stationConfigImport.getFunctionNimsWs());
                    if (mapDataCell != null && mapDataCell.size() > 0) {
                        String encrytedMess = new String((new Gson()).toJson(dataCellUpdateNimsForGnoc).getBytes("UTF-8"), "UTF-8");
                        generateFlowRunController.getFlowRunAction().setDataCell(encrytedMess);
                    }
                }
                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_end
//                if ("3G".equals(currStationPlan.getNetworkType())) {
//                    mapParamValues.put("file_name", filePutOSS);
//                    mapParamValues.put("nodeb", nodeB);
//                    if (mapParamValues.containsKey("oam_ip_address")) {
//                        mapParamValues.put("oam_ip_address", convertIp(mapParamValues.get("oam_ip_address"), 1, -2));
//                    } else {
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), "oam_ip_address"));
//                    }
//                }
                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                for (Node node : lstnodeMsc) {
                    generateFlowRunController.getNodes().add(node);
                    nodeInPlan.add(node);
                }
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
                        if ("3G".equals(currStationPlan.getNetworkType())) {
                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
                                        .replace("{1}", paramValue.getParamCode()));
                                return false;
                            }
                        }
                    }
                }
                mapParamValues.clear();

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {

                    logger.info("Tao mop thanh cong cho node: " + nodeB);
                    logger.info("Save DT Sucess, update crNumber: " + currStationPlan.getCrNumberGnoc());
                    if (currStationPlan != null && !isNullOrEmpty(currStationPlan.getCrNumberGnoc())) {
                        flowRunAction.setCrNumber(currStationPlan.getCrNumberGnoc());
                        new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
                    }
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file
                        StringBuilder stringBuilder = new StringBuilder();

                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                        stationFlowRunAction.setFlowRunAction(flowRunAction);
                        stationFlowRunAction.setNode(nodeRncBsc);
                        stationFlowRunAction.setStationPlan(currStationPlan);
                        stationFlowRunAction.setType(Config.IS_RULE_ACTIVE_STANDARD);
                        stationFlowRunAction.setUpdateTime(new Date());
                        stationFlowRunActiones.add(stationFlowRunAction);
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), mapNodeBRncBsc.get(nodeB)));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao
                } else {
                    logger.info("Tao mop that bai cho node: " + nodeB);
                }
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }
            } finally {
                //Tam biet
            }
        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Active_Test">
    public void ValidateDataActiveTest(HashMap<String, List<?>> mapCellObjectImports, ResultDTO resultDTO, HashMap<String, List<StationConfigImport>> mapImportSheet, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            List<String> lstString3G = new ArrayList<>();
            List<String> lstString2G = new ArrayList<>();

            switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                case Constants.vendorType.ERICSSON:

                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase());

                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.TG_NUMBER").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.CGI").toLowerCase());
                    break;
                case Constants.vendorType.NOKIA:
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.BCF").toLowerCase());
                    lstString2G.add(MessageUtil.getResourceBundleMessage("label.CGI").toLowerCase());
                    break;
                default:
                    break;
            }
            for (String sheetName : mapCellObjectImports.keySet()) {
                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                logger.info("---Quytv7 mapCellObjectImports: " + mapCellObjectImports.size());

                BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(0);

                //check cell
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    for (String str : lstString3G) {
                        if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                            resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                            resultDTO.setResultCode(1);
                        }
                    }
                } else if (Config.networkType2G.equalsIgnoreCase(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    for (String str : lstString2G) {
                        if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                            resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                            resultDTO.setResultCode(1);
                        }
                    }
                }

            }
            if (resultDTO.getResultMessage() != null && !resultDTO.getResultMessage().isEmpty()) {
                return;
            }
            Map<String, Object> filters = new HashMap<>();
            for (String sheetName : mapCellObjectImports.keySet()) {

                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    if (sheetName.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.nodeB"))) {
                        continue;
                    }
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstString3G.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }

                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                } else if (Config.networkType2G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstString2G.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }
                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                }

            }
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void analyseDataActiveTest(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeB;
                        nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.station.site").toLowerCase());
                        if (nodeB != null) {
                            multimapParam.put(nodeB.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.site")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();

            for (String nodeB : multimapParam.keySet()) {
                try {
//                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, String> mapParam = new HashMap<>();
                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                for (String paramCode : mapInBasicDynaBean.keySet()) {
                                    if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                        mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    } else {
                                        mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    }
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }

                    for (String paramCode : mapParam.keySet()) {
                        mapParam.put(paramCode, mapParam.get(paramCode));
                    }
                    if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase())
                            && mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase())) {
                        String[] lacs = mapParam.get(MessageUtil.getResourceBundleMessage("label.lac").toLowerCase()).split(";");
                        String[] cis = mapParam.get(MessageUtil.getResourceBundleMessage("label.sac").toLowerCase()).split(";");
                        String cell_lac_ci = "";
                        for (int i = 0; i < lacs.length; i++) {
                            if (cis[i] != null) {
                                cell_lac_ci = cell_lac_ci + ";" + ("LocationArea=" + lacs[i] + ",ServiceArea=" + cis[i]);
                            }
                        }

                        mapParam.put("cell_lac_ci", cell_lac_ci.substring(1));
                    }

                    mapNodeBParam.put(nodeB, mapParam);
                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeBRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeBMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(userCreate);
            currStationPlan.setProficientType(Config.IS_RULE_ACTIVE_TEST);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> stationDetails = new ArrayList<>();
            StationDetail stationDetail;
            Map<String, Object> filters = new HashMap<>();
            String bscType = null;
            String btsType = null;
            for (String nodeB : mapNodeBParam.keySet()) {
                stationDetail = new StationDetail();
                stationDetail.setStationPlan(currStationPlan);
                stationDetail.setStationCode(nodeB);

                if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
                        || mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {
                    String[] rncBscs;
                    String[] btsTypes;
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase()).split(";");
                    } else {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase()).split(";");
                    }
                    if (rncBscs.length == 0) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    } else {
                        filters.put("rncBsc", rncBscs[0]);
                        List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);

                        if (lstResource.isEmpty()) {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                    nodeB));
                            mapNodeBParam.remove(nodeB);
//                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
                        } else {
                            mapNodeBRncBsc.put(nodeB, lstResource.get(0).getRncBsc());
                            mapNodeBMsc.put(nodeB, lstResource.get(0).getMsc());
                            stationDetail.setStationResource(lstResource.get(0));
                            stationDetail.setRncBsc(lstResource.get(0).getRncBsc());
                            stationDetail.setCellCode(mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("datatable.header.cellCode")));
                            stationDetail.setUpdateTime(new Date());
                        }
                    }
                } else {
                    throw new AppException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                }
                stationDetails.add(stationDetail);
            }
            new StationPlanServiceImpl().saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(stationDetails);
            //Sinh mop RNC, BSC
            createMopActiveTest(mapNodeBParam, stationConfigImports, mapNodeBRncBsc, mapNodeBMsc, bscType, btsType);

        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

    }

    public void createMopActiveTest(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, String bscType, String btsType) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        try {
            createFlowRunActionActiveTest(mapNodeBParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    protected boolean createFlowRunActionActiveTest(HashMap<String, HashMap<String, String>> mapNodeBParam, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
        logger.info("vao ham createFlowRunActionActiveStandard");
        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        FlowTemplates flowTemplatesRollback = null;
        FlowRunAction flowRunAction;
        FlowRunAction flowRunActionRollBack;
        for (String nodeB : mapNodeBParam.keySet()) {
            flowRunAction = new FlowRunAction();
            flowRunActionRollBack = new FlowRunAction();
            //20170102_SinhDT Rollback(Halt) cho active Test_start
            logger.info("Bat dau tao mop rollback cho node: " + nodeB);
            flowTemplatesRollback = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateRollbackId());
            if (flowTemplatesRollback == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                continue;
            } else {
                if (flowTemplatesRollback.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Active_Test_Rollback(halt)-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-NodeB_" + nodeB.toUpperCase() + "-RNC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Active_Test_Rollback(halt)-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BTS_" + nodeB.toUpperCase() + "-BSC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                generateFlowRunController.setPathTemplate(pathTemplate);

                flowRunActionRollBack.setCrNumber(Config.CR_DEFAULT);

                flowRunActionRollBack.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunActionRollBack.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunActionRollBack.getFlowRunName()));
                }
                flowRunActionRollBack.setTimeRun(new Date());
                flowRunActionRollBack.setFlowTemplates(flowTemplatesRollback);
                flowRunActionRollBack.setExecuteBy(userCreate);
                flowRunActionRollBack.setCreateBy(userCreate);
                if (currStationPlan.getCreateDtId() != null) {
                    flowRunActionRollBack.setCreateDtFromGnocId(currStationPlan.getCreateDtId());
                }
                generateFlowRunController.setFlowRunAction(flowRunActionRollBack);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplatesRollback);
                //Lay danh sach param tu bang
                generateFlowRunController.setNodes(new ArrayList<Node>());
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", mapNodeBRncBsc.get(nodeB));
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                Node nodeMsc;
                List<Node> lstnodeMsc = new ArrayList<>();
                filters.clear();

                mapParamValues = mapNodeBParam.get(nodeB);

                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_end
                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                for (Node node : lstnodeMsc) {
                    generateFlowRunController.getNodes().add(node);
                    nodeInPlan.add(node);
                }
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }
                    }
                }

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {

                    logger.info("Tao mop thanh cong cho node: " + nodeB);
                    logger.info("Save DT Sucess, update crNumber: " + currStationPlan.getCrNumberGnoc());
                    if (currStationPlan != null && !isNullOrEmpty(currStationPlan.getCrNumberGnoc())) {
                        flowRunActionRollBack.setCrNumber(currStationPlan.getCrNumberGnoc());
                        new FlowRunActionServiceImpl().saveOrUpdate(flowRunActionRollBack);
                    }

                    //Xu ly day du lieu vao
                } else {
                    logger.info("Tao mop rollback that bai cho node: " + nodeB);
                }
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }
            } finally {
                //Tam biet
            }
            //20170102_SinhDT Rollback(Halt) cho active Test_end

            logger.info("Bat dau tao mop cho node: " + nodeB);
            flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                continue;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Active_Test-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-NodeB_" + nodeB.toUpperCase() + "-RNC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Active_Test-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BTS_" + nodeB.toUpperCase() + "-BSC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                generateFlowRunController.setPathTemplate(pathTemplate);


                flowRunAction.setCrNumber(Config.CR_DEFAULT);

                flowRunAction.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
                if (currStationPlan.getCreateDtId() != null) {
                    flowRunAction.setCreateDtFromGnocId(currStationPlan.getCreateDtId());
                }
                flowRunAction.setRollbackMode(2L);
                flowRunAction.setFlowRunRollbackId(flowRunActionRollBack.getFlowRunId());
                CatCountryBO catCountryBO = new CatCountryServiceImpl().findById("VNM_SECURITY");
                if (catCountryBO != null) {
                    flowRunAction.setCountryCode(catCountryBO);
                }

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang
                generateFlowRunController.setNodes(new ArrayList<Node>());
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", mapNodeBRncBsc.get(nodeB));
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                Node nodeMsc;
                List<Node> lstnodeMsc = new ArrayList<>();
                filters.clear();
                String[] mscStr = mapNodeBMsc.get(nodeB).split(";");
                for (String msc : mscStr) {
                    filters.clear();
                    filters.put("nodeCode-EXAC", msc);
                    List<Node> nodeMscs = new NodeServiceImpl().findList(filters);
                    if (nodeMscs.isEmpty() || nodeRncs.size() > 1) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.msc")));
                    } else {
                        nodeMsc = nodeMscs.get(0);
                        lstnodeMsc.add(nodeMsc);
                    }
                }
                mapParamValues = mapNodeBParam.get(nodeB);
                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_start
                StationConfigImport stationConfigImport = stationConfigImports.get(0);
                if (stationConfigImport != null && !isNullOrEmpty(stationConfigImport.getColumnValidate()) && stationConfigImport.getIsUpdateNims().equals(1L)) {
                    logger.info("Bat dau cap nhat DataCell cac cell cap nhat sang nims");
                    HashMap<String, String> mapDataCell = new HashMap<>();
                    String cell = mapParamValues.get(stationConfigImport.getColumnValidate().toLowerCase());
                    String[] cells = cell.split(Config.SPLITTER_VALUE);
                    for (String cell1 : cells) {
                        if (!isNullOrEmpty(cell1)) {
                            mapDataCell.put(cell1, stationConfigImport.getNetworkType());
                        }
                    }
                    DataCellUpdateNimsForGnoc dataCellUpdateNimsForGnoc = new DataCellUpdateNimsForGnoc();
                    dataCellUpdateNimsForGnoc.setCrNumber(currStationPlan.getCrNumber().trim());
                    dataCellUpdateNimsForGnoc.setMapDataCell(mapDataCell);
                    dataCellUpdateNimsForGnoc.setClassNimsWS(stationConfigImport.getClassNimsWs());
                    dataCellUpdateNimsForGnoc.setFunctionNimsWs(stationConfigImport.getFunctionNimsWs());
                    if (mapDataCell != null && mapDataCell.size() > 0) {
                        String encrytedMess = new String((new Gson()).toJson(dataCellUpdateNimsForGnoc).getBytes("UTF-8"), "UTF-8");
                        generateFlowRunController.getFlowRunAction().setDataCell(encrytedMess);
                    }
                }
                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_end
//                if ("3G".equals(currStationPlan.getNetworkType())) {
//                    mapParamValues.put("file_name", filePutOSS);
//                    mapParamValues.put("nodeb", nodeB);
//                    if (mapParamValues.containsKey("oam_ip_address")) {
//                        mapParamValues.put("oam_ip_address", convertIp(mapParamValues.get("oam_ip_address"), 1, -2));
//                    } else {
//                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), "oam_ip_address"));
//                    }
//                }
                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                for (Node node : lstnodeMsc) {
                    generateFlowRunController.getNodes().add(node);
                    nodeInPlan.add(node);
                }
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }
                    }
                }
                mapParamValues.clear();

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {

                    logger.info("Tao mop thanh cong cho node: " + nodeB);
                    logger.info("Save DT Sucess, update crNumber: " + currStationPlan.getCrNumberGnoc());
                    if (currStationPlan != null && !isNullOrEmpty(currStationPlan.getCrNumberGnoc())) {
                        flowRunAction.setCrNumber(currStationPlan.getCrNumberGnoc());
                        new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
                    }
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file

                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                        stationFlowRunAction.setFlowRunAction(flowRunAction);
                        stationFlowRunAction.setNode(nodeRncBsc);
                        stationFlowRunAction.setStationPlan(currStationPlan);
                        stationFlowRunAction.setType(Config.IS_RULE_ACTIVE_TEST);
                        stationFlowRunAction.setUpdateTime(new Date());
                        stationFlowRunAction.setFlowRunActionRollback(flowRunActionRollBack);
                        stationFlowRunActiones.add(stationFlowRunAction);
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), mapNodeBRncBsc.get(nodeB)));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao
                } else {
                    logger.info("Tao mop that bai cho node: " + nodeB);
                }
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }
            } finally {
                //Tam biet
            }


        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    public void runDTRollback(FlowRunAction flowRunActionRb) {
        try {
            flowRunActionRb.setTimeRun(new Date());
            flowRunActionRb.setStatus(Config.SCHEDULE_FLAG);
            Map<String, Object> _filters = new HashMap<>();
            _filters.put("countryCode.countryCode-" + MapProcessCountryServiceImpl.EXAC, "VNM_SECURITY");
            _filters.put("status", 1l);

            List<MapProcessCountry> maps = new MapProcessCountryServiceImpl().findList(_filters);

            if (maps != null && !maps.isEmpty()) {
                //Sap xep lai maps theo thu tu random
                Collections.shuffle(maps);

                MapProcessCountry obj = maps.get(0);
                flowRunActionRb.setPortRun(obj.getProcessPort().longValue());
                flowRunActionRb.setIpRun(obj.getProcessIp());
                flowRunActionRb.setStatus(7L);
            } else {
                flowRunActionRb.setStatus(0L);
            }
            new FlowRunActionServiceImpl().saveOrUpdate(flowRunActionRb);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Change_Vlan">
    public void ValidateDataChangeVlan(HashMap<String, List<?>> mapCellObjectImports, ResultDTO resultDTO, HashMap<String, List<StationConfigImport>> mapImportSheet, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            List<String> lstString3G = new ArrayList<>();
            List<String> lstString2G = new ArrayList<>();

            switch (currStationPlan.getVendor().getVendorName().toUpperCase()) {
                case Constants.vendorType.ERICSSON:
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.nodeB").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.RBS_type").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.oam_ip_address").toLowerCase());

                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.VLAN_OAM_old").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.VLAN_Service_old").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.VLAN_OAM_New").toLowerCase());
                    lstString3G.add(MessageUtil.getResourceBundleMessage("label.VLAN_Service_New").toLowerCase());
                    break;
                default:
                    break;
            }
            for (String sheetName : mapCellObjectImports.keySet()) {
                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                logger.info("---Quytv7 mapCellObjectImports: " + mapCellObjectImports.size());

                BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(0);

                //check cell
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    for (String str : lstString3G) {
                        if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                            resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                            resultDTO.setResultCode(1);
                        }
                    }
                } else if (Config.networkType2G.equalsIgnoreCase(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    for (String str : lstString2G) {
                        if (!basicDynaBean.getMap().containsKey(str.toLowerCase())) {
                            resultDTO.setResultMessage((isNullOrEmpty(resultDTO.getResultMessage()) ? "" : (resultDTO.getResultMessage() + ";\n")) + MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), str));
                            resultDTO.setResultCode(1);
                        }
                    }
                }

            }
            if (resultDTO.getResultMessage() != null && !resultDTO.getResultMessage().isEmpty()) {
                return;
            }
            Map<String, Object> filters = new HashMap<>();
            for (String sheetName : mapCellObjectImports.keySet()) {

                if (mapImportSheet.get(sheetName) == null || mapImportSheet.get(sheetName).get(0).getIsValidate() == null || mapImportSheet.get(sheetName).get(0).getIsValidate().equals(0L)) {
                    continue;
                }
                if (Config.networkType3G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    if (sheetName.equalsIgnoreCase(MessageUtil.getResourceBundleMessage("label.nodeB"))) {
                        continue;
                    }
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstString3G.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.RNC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }

                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                } else if (Config.networkType2G.equals(mapImportSheet.get(sheetName).get(0).getNetworkType())) {
                    List<LinkedHashMap<String, String>> ListRowData = new ArrayList<>();
                    for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {
                        String resultCode = "OK";
                        String resultDetail = "";
                        LinkedHashMap<String, String> mapParam = new LinkedHashMap<>();
                        BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                        for (DynaProperty dynaProperty : basicDynaBean.getDynaClass().getDynaProperties()) {
                            if (basicDynaBean.getMap().get(dynaProperty.getName()) == null || "".equals(basicDynaBean.getMap().get(dynaProperty.getName()).toString())) {
                                if (lstString2G.contains(dynaProperty.getName().toLowerCase())) {
                                    resultCode = "NOK";
                                    resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is null");
                                }
                                mapParam.put(dynaProperty.getName(), "");
                            } else {
                                if (MessageUtil.getResourceBundleMessage("label.BSC").toLowerCase().equalsIgnoreCase(dynaProperty.getName().toLowerCase())) {
                                    filters.clear();
                                    filters.put("rncBsc", basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                                    List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);
                                    if (lstResource.isEmpty()) {
                                        resultCode = "NOK";
                                        resultDetail = (isNullOrEmpty(resultDetail) ? "" : (resultDetail + ";\n")) + (dynaProperty.getName() + " is not exist in table resource");
                                    }
                                }
                                mapParam.put(dynaProperty.getName(), basicDynaBean.getMap().get(dynaProperty.getName()).toString());
                            }
                        }
                        if ("NOK".equalsIgnoreCase(resultCode)) {
                            resultDTO.setResultCode(1);
                            resultDTO.setResultMessage("Data validate VMSA fail, detail info in file result");
                        }
                        mapParam.put("vmsa_result_code", resultCode);
                        mapParam.put("vmsa_result_detail", resultDetail);
                        ListRowData.add(mapParam);
                        mapData.put(sheetName, ListRowData);
                    }
                }

            }
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }

    }

    public void analyseDataChangeVlan(List<StationConfigImport> stationConfigImports, List<StationParamDefault> lstParamDefault, HashMap<String, List<?>> mapCellObjectImports) throws Exception {
        try {
            //Xy ly du lieu file
            //Xu ly day tat ca cac du lieu vao map voi key nodeB
            Multimap<String, BasicDynaBean> multimapParam = ArrayListMultimap.create();
            for (String sheetName : mapCellObjectImports.keySet()) {
                for (int i = 0; i < mapCellObjectImports.get(sheetName).size(); i++) {

                    BasicDynaBean basicDynaBean = (BasicDynaBean) mapCellObjectImports.get(sheetName).get(i);
                    try {
                        Object nodeB;
                        nodeB = basicDynaBean.getMap().get(MessageUtil.getResourceBundleMessage("label.nodeB").toLowerCase());
                        if (nodeB != null) {
                            multimapParam.put(nodeB.toString(), basicDynaBean);
                        }

                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.nodeB")));
                    }
                }
            }
            //xu ly map
            HashMap<String, HashMap<String, String>> mapNodeBParam = new HashMap<>();

            for (String nodeB : multimapParam.keySet()) {
                try {
//                    boolean checkCreateMop = true;
                    List<StationResult> lstStationResult = new ArrayList<>();
                    HashMap<String, String> mapParam = new HashMap<>();
                    Collection<BasicDynaBean> basicDynaBeans = multimapParam.get(nodeB);
                    try {
                        for (BasicDynaBean basicDynaBean : basicDynaBeans) {
                            Map<String, String> mapInBasicDynaBean;
                            try {
                                mapInBasicDynaBean = basicDynaBean.getMap();
                                for (String paramCode : mapInBasicDynaBean.keySet()) {
                                    if (mapParam.containsKey(paramCode.trim().toLowerCase())) {
                                        mapParam.put(paramCode.trim().toLowerCase(), mapParam.get(paramCode.trim().toLowerCase()) + Config.SPLITTER_VALUE + (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    } else {
                                        mapParam.put(paramCode.trim().toLowerCase(), (mapInBasicDynaBean.get(paramCode.trim().toLowerCase()) == null ? "" : mapInBasicDynaBean.get(paramCode.trim().toLowerCase())));
                                    }
                                }
                            } catch (Exception e) {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }

                    for (String paramCode : mapParam.keySet()) {
                        mapParam.put(paramCode, mapParam.get(paramCode));
                    }
                    String date = new SimpleDateFormat("HH_dd_MM_yyyy").format(new Date());
                    mapParam.put(MessageUtil.getResourceBundleMessage("label.date").toLowerCase(), date);

                    mapNodeBParam.put(nodeB, mapParam);
                } catch (Exception e) {
                    throw e;
                }
            }
            //Luu database
            //Insert vao DB
            HashMap<String, String> mapNodeBRncBsc = new HashMap<>();
            HashMap<String, String> mapNodeBMsc = new HashMap<>();
            currStationPlan.setUpdateTime(new Date());
            currStationPlan.setUserCreate(userCreate);
            currStationPlan.setProficientType(Config.IS_RULE_CHANGE_VLAN);
            if (fileOutPut != null) {
                currStationPlan.setFileName(fileOutPut.getName());
                currStationPlan.setFileContent(readBytesFromFile(fileOutPut));
                currStationPlan.setFilePath(fileOutPut.getPath());
            }

            //Insert tram vao DB
            List<StationDetail> stationDetails = new ArrayList<>();
            StationDetail stationDetail;
            Map<String, Object> filters = new HashMap<>();
            String bscType = null;
            String btsType = null;
            for (String nodeB : mapNodeBParam.keySet()) {
                stationDetail = new StationDetail();
                stationDetail.setStationPlan(currStationPlan);
                stationDetail.setStationCode(nodeB);

                if (mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase())
                        || mapNodeBParam.get(nodeB).containsKey(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase())) {
                    String[] rncBscs;
                    String[] btsTypes;
                    if ("3G".equals(currStationPlan.getNetworkType())) {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.rescue.RNC").trim().toLowerCase()).split(";");
                    } else {
                        rncBscs = mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("label.BSC").trim().toLowerCase()).split(";");
                    }
                    if (rncBscs.length == 0) {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                    } else {
                        filters.put("rncBsc", rncBscs[0]);
                        List<StationResource> lstResource = new StationResourceServiceImpl().findList(filters);

                        if (lstResource.isEmpty()) {
                            MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.valid.not.create.DT"),
                                    nodeB));
                            mapNodeBParam.remove(nodeB);
//                            throw new AppException("Khong tim thay tham so RNC trong bang resource");
                        } else {
                            mapNodeBRncBsc.put(nodeB, lstResource.get(0).getRncBsc());
                            mapNodeBMsc.put(nodeB, lstResource.get(0).getMsc());
                            stationDetail.setStationResource(lstResource.get(0));
                            stationDetail.setRncBsc(lstResource.get(0).getRncBsc());
                            stationDetail.setCellCode(mapNodeBParam.get(nodeB).get(MessageUtil.getResourceBundleMessage("datatable.header.cellCode")));
                            stationDetail.setUpdateTime(new Date());
                        }
                    }
                } else {
                    throw new AppException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                }
                stationDetails.add(stationDetail);
            }
            new StationPlanServiceImpl().saveOrUpdate(currStationPlan);
            new StationDetailServiceImpl().saveOrUpdate(stationDetails);
            //Sinh mop RNC, BSC
            createMopChangeVlan(mapNodeBParam, stationConfigImports, mapNodeBRncBsc, mapNodeBMsc, bscType, btsType);

        } catch (SQLException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw ex;
        }

    }

    public void createMopChangeVlan(HashMap<String, HashMap<String, String>> mapNodeBParam, List<StationConfigImport> stationConfigImports, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, String bscType, String btsType) throws AppException, Exception {
        //Sinh mop cho RNC, MSC
        try {
            createFlowRunActionChangeVlan(mapNodeBParam, mapNodeBRncBsc, mapNodeBMsc, stationConfigImports);
        } catch (Exception ex) {

            LOGGER.error(ex.getMessage());
            throw ex;
        }
    }

    protected boolean createFlowRunActionChangeVlan(HashMap<String, HashMap<String, String>> mapNodeBParam, HashMap<String, String> mapNodeBRncBsc, HashMap<String, String> mapNodeBMsc, List<StationConfigImport> stationConfigImports) throws MessageException, Exception {
        logger.info("vao ham createFlowRunActionChangeVLAN");
        GenerateFlowRunController generateFlowRunController;
        List<StationFlowRunAction> stationFlowRunActiones = new ArrayList<>();
        FlowTemplates flowTemplates = null;
        FlowTemplates flowTemplatesRollback = null;
        FlowRunAction flowRunAction;
        FlowRunAction flowRunActionRollBack;
        for (String nodeB : mapNodeBParam.keySet()) {
            flowRunAction = new FlowRunAction();
            flowRunActionRollBack = new FlowRunAction();
            //20170102_SinhDT Rollback(Halt) cho active Test_start
            logger.info("Bat dau tao mop rollback cho node: " + nodeB);
            flowTemplatesRollback = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateRollbackId());
            if (flowTemplatesRollback == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                continue;
            } else {
                if (flowTemplatesRollback.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }
            String nodeRun = "NODEB_RUN";
            if ("3G".equals(currStationPlan.getNetworkType())) {
                nodeRun = "NODEB_RUN";
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Change_VLAN_Rollback-(save cv)-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-NodeB_" + nodeB.toUpperCase() + "-RNC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Change_VLAN_Rollback-(save cv)--" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BTS_" + nodeB.toUpperCase() + "-BSC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                generateFlowRunController.setPathTemplate(pathTemplate);

                flowRunActionRollBack.setCrNumber(Config.CR_DEFAULT);

                flowRunActionRollBack.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunActionRollBack.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunActionRollBack.getFlowRunName()));
                }
                flowRunActionRollBack.setTimeRun(new Date());
                flowRunActionRollBack.setFlowTemplates(flowTemplatesRollback);
                flowRunActionRollBack.setExecuteBy(userCreate);
                flowRunActionRollBack.setCreateBy(userCreate);
                if (currStationPlan.getCreateDtId() != null) {
                    flowRunActionRollBack.setCreateDtFromGnocId(currStationPlan.getCreateDtId());
                }
                generateFlowRunController.setFlowRunAction(flowRunActionRollBack);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplatesRollback);
                //Lay danh sach param tu bang
                generateFlowRunController.setNodes(new ArrayList<Node>());
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;

                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", nodeRun);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }


                filters.clear();

                mapParamValues = mapNodeBParam.get(nodeB);
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    mapParamValues.put("nodeb", nodeB);
                    if (mapParamValues.containsKey(MessageUtil.getResourceBundleMessage("label.oam_ip_address"))) {
                        mapParamValues.put(MessageUtil.getResourceBundleMessage("label.oam_ip_address"), convertIp(mapParamValues.get(MessageUtil.getResourceBundleMessage("label.oam_ip_address")), 1, -2));
                    } else {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), "oam_ip_address"));
                    }
                }

                //Quytv7_cap nhat cell gui sang nims sau khi chay xong dt_end
                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);
                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }
                    }
                }

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {

                    logger.info("Tao mop thanh cong cho node: " + nodeB);
                    logger.info("Save DT Sucess, update crNumber: " + currStationPlan.getCrNumberGnoc());
                    if (currStationPlan != null && !isNullOrEmpty(currStationPlan.getCrNumberGnoc())) {
                        flowRunActionRollBack.setCrNumber(currStationPlan.getCrNumberGnoc());
                        new FlowRunActionServiceImpl().saveOrUpdate(flowRunActionRollBack);
                    }

                    //Xu ly day du lieu vao
                } else {
                    logger.info("Tao mop rollback that bai cho node: " + nodeB);
                }
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }
            } finally {
                //Tam biet
            }
            //20170102_SinhDT Rollback(Halt) cho active Test_end

            logger.info("Bat dau tao mop cho node: " + nodeB);
            flowTemplates = new FlowTemplatesServiceImpl().findById(stationConfigImports.get(0).getTemplateId());
            if (flowTemplates == null) {
                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.found.template"));
                continue;
            } else {
                if (flowTemplates.getStatus() != 9) {
                    MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.template.not.approved"));
                    continue;
                }
            }

            try {
                String filePutOSS;
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Change_VLAN-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-NodeB_" + nodeB.toUpperCase() + "-RNC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                } else {
                    filePutOSS = currStationPlan.getCrNumber().trim().toUpperCase()
                            + "-Change_VLAN-" + currStationPlan.getVendor().getVendorName().toUpperCase()
                            + "-" + currStationPlan.getNetworkType().toUpperCase()
                            + "-BTS_" + nodeB.toUpperCase() + "-BSC_" + mapNodeBRncBsc.get(nodeB).toUpperCase()
                            + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                }
                generateFlowRunController = new GenerateFlowRunController();
                generateFlowRunController.setPathTemplate(pathTemplate);


                flowRunAction.setCrNumber(Config.CR_DEFAULT);

                flowRunAction.setFlowRunName(filePutOSS);
                filePutOSS = (filePutOSS.replace("-", "_").replace(" ", "")) + ".txt";
                while (FlowRunAction.isExistFlowName(flowRunAction.getFlowRunName())) {
                    flowRunAction.setFlowRunName(FlowRunAction.createFlowRunName(flowRunAction.getFlowRunName()));
                }
                flowRunAction.setTimeRun(new Date());
                flowRunAction.setFlowTemplates(flowTemplates);
                flowRunAction.setExecuteBy(userCreate);
                flowRunAction.setCreateBy(userCreate);
                if (currStationPlan.getCreateDtId() != null) {
                    flowRunAction.setCreateDtFromGnocId(currStationPlan.getCreateDtId());
                }
                flowRunAction.setRollbackMode(3L);
                flowRunAction.setFlowRunRollbackId(flowRunActionRollBack.getFlowRunId());
                CatCountryBO catCountryBO = new CatCountryServiceImpl().findById("VNM");
                if (catCountryBO != null) {
                    flowRunAction.setCountryCode(catCountryBO);
                }

                generateFlowRunController.setFlowRunAction(flowRunAction);
                generateFlowRunController.setSelectedFlowTemplates(flowTemplates);
                //Lay danh sach param tu bang
                generateFlowRunController.setNodes(new ArrayList<Node>());
                Map<String, String> mapParamValues;
                generateFlowRunController.loadGroupAction(0l);
                List<Node> nodeInPlan;
                HashMap<String, Object> filters = new HashMap<>();
                filters.put("nodeCode-EXAC", nodeRun);
                List<Node> nodeRncs = new NodeServiceImpl().findList(filters);
                Node nodeRncBsc;
                if (nodeRncs.isEmpty() || nodeRncs.size() > 1) {
                    throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.node.not.exits"), MessageUtil.getResourceBundleMessage("label.station.rncBsc")));
                } else {
                    nodeRncBsc = nodeRncs.get(0);
                }
                filters.clear();

                mapParamValues = mapNodeBParam.get(nodeB);
                if ("3G".equals(currStationPlan.getNetworkType())) {
                    mapParamValues.put("nodeb", nodeB);
                    if (mapParamValues.containsKey(MessageUtil.getResourceBundleMessage("label.oam_ip_address"))) {
                        mapParamValues.put(MessageUtil.getResourceBundleMessage("label.oam_ip_address"), convertIp(mapParamValues.get(MessageUtil.getResourceBundleMessage("label.oam_ip_address")), 1, -2));
                    } else {
                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), "oam_ip_address"));
                    }
                }
                nodeInPlan = new ArrayList<>();
                generateFlowRunController.getNodes().add(nodeRncBsc);

                nodeInPlan.add(nodeRncBsc);

                for (Node node : nodeInPlan) {
                    generateFlowRunController.loadGroupAction(Config.SUB_FLOW_RUN_DEFAULT, node);
                    List<ParamValue> paramValues = generateFlowRunController.getParamInputs(Config.SUB_FLOW_RUN_DEFAULT, node);
                    for (ParamValue paramValue : paramValues) {
                        if (paramValue.getParamInput().getReadOnly()) {
                            continue;
                        }
                        Object value = null;
                        try {
                            value = mapParamValues.get((paramValue.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_")));
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        if (value != null) {
                            paramValue.setParamValue(value.toString());
                        }
//                        if ("3G".equals(currStationPlan.getNetworkType())) {
//                            if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
//                                MessageUtil.setErrorMessage(MessageUtil.getResourceBundleMessage("error.not.need.param.value").replace("{0}", node.getNodeCode())
//                                        .replace("{1}", paramValue.getParamCode()));
//                                return false;
//                            }
//                        }
                    }
                }
                mapParamValues.clear();

                boolean saveDT = generateFlowRunController.saveDT();
                if (saveDT) {

                    logger.info("Tao mop thanh cong cho node: " + nodeB);
                    logger.info("Save DT Sucess, update crNumber: " + currStationPlan.getCrNumberGnoc());
                    if (currStationPlan != null && !isNullOrEmpty(currStationPlan.getCrNumberGnoc())) {
                        flowRunAction.setCrNumber(currStationPlan.getCrNumberGnoc());
                        new FlowRunActionServiceImpl().saveOrUpdate(flowRunAction);
                    }
                    try {
                        //Bat dau xu ly day file OSS
                        //Tao file

                        StationFlowRunAction stationFlowRunAction = new StationFlowRunAction();
                        stationFlowRunAction.setFlowRunAction(flowRunAction);
                        stationFlowRunAction.setNode(nodeRncBsc);
                        stationFlowRunAction.setStationPlan(currStationPlan);
                        stationFlowRunAction.setType(Config.IS_RULE_CHANGE_VLAN);
                        stationFlowRunAction.setUpdateTime(new Date());
                        stationFlowRunAction.setFlowRunActionRollback(flowRunActionRollBack);
                        stationFlowRunActiones.add(stationFlowRunAction);
                        MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.create.DT.success"), mapNodeBRncBsc.get(nodeB)));
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                    //Xu ly day du lieu vao
                } else {
                    logger.info("Tao mop that bai cho node: " + nodeB);
                }
            } catch (Exception e) {
                if (e instanceof MessageException) {
                    throw e;
                } else {
                    LOGGER.error(e.getMessage(), e);
                    throw new MessageException(MessageUtil.getResourceBundleMessage("error.cannot.create.mop"));
                }
            } finally {
                //Tam biet
            }


        }
        new StationFlowRunActionServiceImpl().saveOrUpdate(stationFlowRunActiones);
        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Common">
    public static String getFolderSave() {
        String pathOut;
        ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                .getExternalContext().getContext();
        pathOut = ctx.getRealPath("/") + Config.PATH_OUT;
        File folderOut = new File(pathOut);
        if (!folderOut.exists()) {
            folderOut.mkdirs();
        }
        return pathOut;
    }

    public File createFilePutOSS(String filename, StringBuilder stringBuilder) throws Exception {
        BufferedWriter bw = null;
        FileWriter fw = null;
        File file;
        try {
            try {
                file = new File(getFolderSave() + "IntegrateStation" + File.separator + "PutFileOSS" + File.separator + filename);
            } catch (Exception ex) {
                logger.debug(ex.getMessage(), ex);
                file = new File(pathOut + "IntegrateStation" + File.separator + "PutFileOSS" + File.separator + filename);
            }
            logger.info("file :" + file == null ? "null" : file.getName());
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(stringBuilder.toString());
            bw.close();

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            throw ex;
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
        return file;
    }

    public void calculateParamEricsson(HashMap<String, String> mapParam, List<StationParamDefault> lstParamDefault) {

        try {
            LinkedHashMap<String, LinkedHashMap<String, Long>> mapCellMctri = new LinkedHashMap<>();

            if ("2G".equals(currStationPlan.getNetworkType())) {
                if (mapParam.containsKey("trx_number")) {
                    String[] trx_numbers = mapParam.get("trx_number").split(";");
                    String[] cells = null;
                    if (mapParam.containsKey("cell")) {
                        cells = mapParam.get("cell").split(";");
                    }
                    String cellFinal = "";
                    int ALL_CONFIG_TRX = 0;
                    String TRX_NUMBER = "";
                    String MCTRI = "";

                    if (cells != null && cells.length > 0) {
                        for (int i = 0; i < cells.length; i++) {
                            int ch = 0;
                            String cell = "";
                            String[] trx_numberss = trx_numbers[i].split("-");
                            LinkedHashMap<String, Long> mapMctri = new LinkedHashMap<>();
                            if (trx_numberss.length > 0) {
                                for (String trx : trx_numberss) {
                                    cell = cell + ";" + cells[i];
                                    TRX_NUMBER = TRX_NUMBER + ";" + trx;
                                    ALL_CONFIG_TRX++;
                                    ch++;
                                    String mctriTemp;
                                    if ("0".equals(trx) || "1".equals(trx) || "2".equals(trx) || "3".equals(trx)) {
                                        MCTRI = MCTRI + ";" + "0";
                                        mctriTemp = "0";
                                    } else if ("4".equals(trx) || "5".equals(trx) || "6".equals(trx) || "7".equals(trx)) {
                                        MCTRI = MCTRI + ";" + "1";
                                        mctriTemp = "1";
                                    } else if ("8".equals(trx) || "9".equals(trx) || "10".equals(trx) || "11".equals(trx)) {
                                        MCTRI = MCTRI + ";" + "2";
                                        mctriTemp = "2";
                                    } else {
                                        MCTRI = MCTRI + ";" + "3";
                                        mctriTemp = "3";
                                    }
                                    if (mapMctri.containsKey(mctriTemp)) {
                                        mapMctri.put(mctriTemp, (mapMctri.get(mctriTemp) + 1));
                                    } else {
                                        mapMctri.put(mctriTemp, 1L);
                                    }

                                }
                                mapParam.put("ch", String.valueOf(ch));
                            }
                            cellFinal = cellFinal + cell;
                            mapCellMctri.put(cell, mapMctri);
                        }

                    }
                    if (cellFinal != null && !"".equals(cellFinal)) {
                        cellFinal = cellFinal.substring(1);
                    }
                    if (TRX_NUMBER != null && !"".equals(TRX_NUMBER)) {
                        TRX_NUMBER = TRX_NUMBER.substring(1);
                    }
                    mapParam.put("cell_trx", cellFinal);
                    mapParam.put("trx_number", TRX_NUMBER);
                    mapParam.put("all_config_trx", String.valueOf(ALL_CONFIG_TRX));

                    // Phan tich lay cong suat
                    String MAXTRX = "";
                    for (String cell : mapCellMctri.keySet()) {
                        for (String mctri : mapCellMctri.get(cell).keySet()) {
                            for (int i = 0; i < mapCellMctri.get(cell).get(mctri); i++) {
                                MAXTRX = MAXTRX + ";" + mapCellMctri.get(cell).get(mctri).toString();
                            }
                        }
                    }
                    MAXTRX = MAXTRX.substring(1);
                    mapParam.put("maxtrx", MAXTRX);
                }

                //Bo sung them province
                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase())) {
                    String provinceCode = "";
                    String[] btss;
                    btss = mapParam.get(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase()).split(";");

                    if (btss != null && btss.length > 0) {
                        for (String bts : btss) {
                            provinceCode = provinceCode + ";" + bts.substring(0, 3);
                        }
                    }
                    mapParam.put("province_code", provinceCode.substring(1));
                }
            }

            calculateParamDetail(mapParam, lstParamDefault, LOGGER);

            if ("2G".equals(currStationPlan.getNetworkType())) {
                if (mapParam.containsKey("mpwr")) {
                    String[] mpwrs = mapParam.get("mpwr").split(";");
                    String BSPWRB = "";
                    Integer count = 0;
                    for (String cell : mapCellMctri.keySet()) {

                        Long bspwrbLong = 1000L;
                        for (String mctri : mapCellMctri.get(cell).keySet()) {

                            for (int i = 0; i < mapCellMctri.get(cell).get(mctri); i++) {

                                if (mpwrs[i] != null) {
                                    try {
                                        if (Long.valueOf(mpwrs[i + count]) < bspwrbLong) {
                                            bspwrbLong = Long.valueOf(mpwrs[i + count]);
                                        }
                                    } catch (Exception ex) {
                                        logger.debug(ex.getMessage(), ex);
                                        throw new MessageException(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.station.param.not.exits"), "BSPWRB"));
                                    }
                                }
                            }
                            count = count + Integer.parseInt(mapCellMctri.get(cell).get(mctri).toString());
                        }
                        BSPWRB = BSPWRB + ";" + bspwrbLong.toString();
                    }
                    BSPWRB = BSPWRB.substring(1);
                    mapParam.put("bspwrb", BSPWRB);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void calculateParamNokia(HashMap<String, String> mapParam, List<StationParamDefault> lstParamDefault) {

        try {
            String[] btss;
            String compareBcf = "0";
            String bcf = "";
            String lapdoam = "";
            String lapdtrx = "";
            String provinceCode = "";
            String TU_A_B_C = "A";
            if ("2G".equals(currStationPlan.getNetworkType())) {
                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase())) {
                    btss = mapParam.get(MessageUtil.getResourceBundleMessage("label.BTS").toLowerCase()).split(";");

                    if (btss != null && btss.length > 0) {
                        for (String bts : btss) {
                            String bcfTemp;
                            if (bts.length() >= 6) {
                                if (StringUtils.isNumericSpace(bts.substring(2, 3))) {
                                    compareBcf = bts.substring(2, 6);
                                    bcf = bcf + ";";

                                } else {

                                    bcfTemp = bts.substring(3, bts.length());
                                    if (bcfTemp.endsWith("B")) {
                                        try {
                                            TU_A_B_C = "B";
                                            if ((Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1))) >= 500) {
                                                compareBcf = String.valueOf((Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1))));
                                                bcfTemp = "";
                                            } else {
                                                lapdoam = lapdoam + ";" + ("BO" + String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 500));
                                                lapdtrx = lapdtrx + ";" + ("Y" + String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 500));
                                                bcfTemp = String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 500);
                                            }

                                        } catch (Exception ex) {
                                            logger.debug(ex.getMessage(), ex);
                                            lapdoam = lapdoam + ";";
                                            lapdtrx = lapdtrx + ";";
                                            bcfTemp = "";
                                        }
                                    } else if (bcfTemp.endsWith("C")) {
                                        try {
                                            TU_A_B_C = "C";
                                            if ((Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1))) >= 500) {
                                                compareBcf = String.valueOf((Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1))));
                                                bcfTemp = "";
                                            } else {
                                                lapdoam = lapdoam + ";" + ("AO" + String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 1000));
                                                lapdtrx = lapdtrx + ";" + ("Z" + String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 1000));
                                                bcfTemp = String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 1000);
                                            }

                                        } catch (Exception ex) {
                                            logger.debug(ex.getMessage(), ex);
                                            lapdoam = lapdoam + ";";
                                            lapdtrx = lapdtrx + ";";
                                            bcfTemp = "";
                                        }
                                    } else if (bcfTemp.endsWith("D")) {
                                        try {
                                            TU_A_B_C = "D";
                                            if ((Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1))) >= 500) {
                                                compareBcf = String.valueOf((Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1))));
                                                bcfTemp = "";
                                            } else {
                                                lapdoam = lapdoam + ";" + ("AO" + String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 1500));
                                                lapdtrx = lapdtrx + ";" + ("Z" + String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 1500));
                                                bcfTemp = String.valueOf(Long.valueOf(bcfTemp.substring(0, bcfTemp.length() - 1)) + 1500);
                                            }

                                        } catch (Exception ex) {
                                            logger.debug(ex.getMessage(), ex);
                                            lapdoam = lapdoam + ";";
                                            lapdtrx = lapdtrx + ";";
                                            bcfTemp = "";
                                        }
                                    } else {
                                        try {
                                            if ((Long.valueOf(bcfTemp.substring(0, bcfTemp.length()))) >= 500) {
                                                compareBcf = String.valueOf((Long.valueOf(bcfTemp.substring(0, bcfTemp.length()))));
                                                bcfTemp = "";
                                            } else {
                                                lapdoam = lapdoam + ";" + ("BO" + String.valueOf(bcfTemp.substring(0, bcfTemp.length())));
                                                lapdtrx = lapdtrx + ";" + ("Y" + String.valueOf(bcfTemp.substring(0, bcfTemp.length())));
                                                bcfTemp = String.valueOf(bcfTemp.substring(0, bcfTemp.length()));
                                            }

                                        } catch (Exception ex) {
                                            logger.debug(ex.getMessage(), ex);
                                            lapdoam = lapdoam + ";";
                                            lapdtrx = lapdtrx + ";";
                                            bcfTemp = "";
                                        }
                                    }
                                    bcf = bcf + ";" + bcfTemp;
                                }
                            }
                            provinceCode = provinceCode + ";" + bts.substring(0, 3);
                        }
                    }
                    mapParam.put("bcf_compare", compareBcf);
                    mapParam.put("province_code", provinceCode.substring(1));
                    mapParam.put("tu_a_b_c", TU_A_B_C);

                    if ("0".equals(compareBcf)) {
                        mapParam.put("bcf", bcf.substring(1));
                        mapParam.put("lapdoam", lapdoam.substring(1));
                        mapParam.put("lapd", lapdtrx.substring(1));
                    }
                }
                String bcch = "";
                String config = "";
                String cellFinal = "";
                String tscFinal = "";
                if (mapParam.containsKey(MessageUtil.getResourceBundleMessage("bcch"))
                        && mapParam.containsKey(MessageUtil.getResourceBundleMessage("trx"))
                        && mapParam.containsKey(MessageUtil.getResourceBundleMessage("tch"))) {
                    String bcchs[] = mapParam.get(MessageUtil.getResourceBundleMessage("bcch")).split(";");
                    String configs[] = mapParam.get(MessageUtil.getResourceBundleMessage("trx")).split(";");
                    String freLists[] = mapParam.get(MessageUtil.getResourceBundleMessage("tch")).split(";");
                    String[] cells = null;
                    String[] tscs = null;
                    if (mapParam.containsKey("cell")) {
                        cells = mapParam.get("cell").split(";");
                    }
                    if (mapParam.containsKey("tsc")) {
                        tscs = mapParam.get("tsc").split(";");
                    }
                    if (bcchs.length > 0) {

                        for (int i = 0; i < bcchs.length; i++) {
                            String bcchTemp = "";
                            String configTemp = "";
                            String configTemps[] = configs[i].split("-");
                            String freListTemps[] = freLists[i].split("-");
                            for (int j = 0; j < configTemps.length; j++) {
                                configTemp = configTemp + ";" + configTemps[j].trim();
                                if (j > 0) {
                                    bcchTemp = bcchTemp + ";" + freListTemps[j - 1].trim();
                                }
                                if (cells != null && cells[i] != null) {
                                    cellFinal = cellFinal + ";" + cells[i];
                                }
                                if (tscs != null && tscs[i] != null) {
                                    tscFinal = tscFinal + ";" + tscs[i];
                                }

                            }
                            bcchTemp = bcchs[i] + bcchTemp;
                            bcch = bcch + ";" + bcchTemp;
                            config = config + configTemp;
                        }
                    }
                    mapParam.put("cell_trx", cellFinal.substring(1));
                    mapParam.put("tsc", tscFinal.substring(1));
                    mapParam.put("tch", bcch.substring(1));
                    mapParam.put("trx_number", config.substring(1));
                }

            }

            calculateParamDetail(mapParam, lstParamDefault, LOGGER);

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    public void calculateParamDetail(HashMap<String, String> mapValue, List<StationParamDefault> lstParamDefault, Logger logger) {
        // Tinh toan cac tham so cua lenh theo cong thuc duoc cau hinh

        for (StationParamDefault paramInput : lstParamDefault) {
            if (paramInput.getParamFormula() != null && !paramInput.getParamFormula().trim().isEmpty()) {
                if (paramInput.getParamFormula().contains("-->")) {
                    List<String> array = getParamList(paramInput.getParamFormula());

                    Map<String, String> mapVar = new HashMap<>();
                    for (String str : array) {
                        if (mapValue.containsKey(str)) {
                            mapVar.put(str, mapValue.get(str) == null ? "" : mapValue.get(str).trim());
                        }
                    }
                    if (mapVar.isEmpty()) {
                        continue;
                    }
                    List<String> lstValues = getIfValue(paramInput.getParamFormula(), mapVar, logger);
                    if (!lstValues.isEmpty()) {
                        String vl = StringUtils.join(lstValues, ";");
                        mapValue.put(paramInput.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_"), vl);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(vl);
//                            }
//                        }
                    } else {
//                        mapValue.put(paramInput.getParamCode(), null);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(null);
//                            }
//                        }
                    }
                } else {
                    List<String> array = getParamList(paramInput.getParamFormula());

                    Map<String, String> mapVar = new HashMap<>();
                    for (String str : array) {
                        if (mapValue.containsKey(str)) {
                            mapVar.put(str, mapValue.get(str) == null ? "" : mapValue.get(str).trim());
                        }
                    }
                    List<Long> lstValues = getExpressValue(paramInput.getParamFormula(), mapVar);
                    if (!lstValues.isEmpty()) {
                        String vl = StringUtils.join(lstValues, ";");
                        mapValue.put(paramInput.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_"), vl);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(vl);
//                            }
//                        }
                    } else {
//                        mapValue.put(paramInput.getParamCode(), null);

//                        for (ParamValue paramValue : paramValues) {
//                            if (((paramValue.getParamInput().getParamFormula() == null || paramValue.getParamInput().getParamFormula().trim().isEmpty())
//                                    && paramValue.getParamInput().getParamCode().equals(paramInput.getParamCode()))
//                                    || paramValue.getParamInput().getParamInputId().equals(paramInput.getParamInputId())) {
//                                paramValue.setParamValue(null);
//                            }
//                        }
                    }
                }
            } else {
                mapValue.put(paramInput.getParamCode().toLowerCase().trim().replace(" ", "_").replace(".", "_"), paramInput.getParamDefault() == null ? "" : paramInput.getParamDefault());
            }
        }
    }

    private List<String> getParamList(String commandPattern) {
        List<String> lstParam = new ArrayList<>();

        if (commandPattern != null && !commandPattern.trim().isEmpty() && commandPattern.contains("@{")) {
            int preIndex = 0, endIndex;
            while (true) {
                preIndex = commandPattern.indexOf("@{", preIndex);

                if (preIndex < 0) {
                    break;
                } else {
                    preIndex += 2;
                }

                endIndex = commandPattern.indexOf("}", preIndex);

                if (endIndex < 0) {
                    break;
                } else {
                    lstParam.add(commandPattern.substring(preIndex, endIndex));
                    preIndex = endIndex;
                }
            }
        }

        return lstParam;
    }

    private List<String> getIfValue(String expressStr, Map<String, String> mapVar, Logger logger) {
        List<String> lstValue = new ArrayList<>();
        try {
            int maxNumber = 1;
            for (String value : mapVar.values()) {
                maxNumber = maxNumber > value.split(";").length ? maxNumber : value.split(";").length;
            }
            for (int i = 0; i < maxNumber; i++) {
                //try {
                Map<String, String> mapVarD = new HashMap<>();
                for (String code : mapVar.keySet()) {
                    String vl = mapVar.get(code);
                    String[] arr = vl.split(";", -1);
                    if (arr.length > 0) {
                        if (i <= arr.length - 1) {
                            mapVarD.put(code, arr[i]);
                        } else {
                            mapVarD.put(code, arr[arr.length - 1]);
                        }
                    }
                }

                String[] ifConditions = expressStr.split(";");
                String value = null;
                boolean paramEnough = true;
                for (String condition : ifConditions) {
                    //try {
                    if (condition.contains("-->")) {
                        String[] arr = condition.split("-->");
                        if (arr != null && arr.length == 2) {
                            String ifBody = arr[0].trim();
                            String ifThen = arr[1].trim();

                            for (String var : mapVarD.keySet()) {
                                ifBody = ifBody.replace("@{" + var + "}", mapVarD.get(var));
                                ifThen = ifThen.replace("@{" + var + "}", mapVarD.get(var));
                            }

//                            ScriptEngineManager factory = new ScriptEngineManager();
//                            ScriptEngine engine = factory.getEngineByName("JavaScript");
                            if (!ifBody.contains("@{")) {
                                Boolean ifValue = Boolean.parseBoolean((new EvalUtil()).evalScript(ifBody));
                                if (ifValue && ifThen != null && !ifThen.contains("@{")) {
                                    value = (new EvalUtil()).evalScript(ifThen).replaceAll("\\.0+$", "");
                                    break;
                                }
                            } else {
                                paramEnough = false;
                            }
                        }
                    }
//                        } catch (Exception ex) {
//                            logger.error(ex.getMessage(), ex);
//                        }
                }

                if (value == null && paramEnough) {
                    for (String condition : ifConditions) {
                        if (condition.toLowerCase().contains("else") && !condition.contains("-->")) {
                            for (String var : mapVarD.keySet()) {
                                condition = condition.replace("@{" + var + "}", mapVarD.get(var));
                            }
                            value = condition.replace("else", "").trim();
                            break;
                        }
                    }
                }
                if (value != null) {
                    if (value.contains(",")) {
                        Collections.addAll(lstValue, value.split(","));
                    } else {
                        lstValue.add(value);
                    }
                }
//                } catch (Exception ex) {
//                    return lstValue;
//                }
            }
            return lstValue;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new ArrayList<>();
        }
    }

    private List<Long> getExpressValue(final String expressStr, Map<String, String> mapVar) {
        List<Long> lstValue = new ArrayList<>();
        try {
            int maxNumber = 1;
            for (String value : mapVar.values()) {
                maxNumber = maxNumber > value.split(";").length ? maxNumber : value.split(";").length;
            }
            for (int i = 0; i < maxNumber; i++) {
                String expressStrLocal = expressStr;
                //try {
                Map<String, Double> mapVarD = new HashMap<>();
                for (String code : mapVar.keySet()) {
                    String vl = mapVar.get(code);
                    String[] arr = vl.split(";");
                    if (i <= arr.length - 1) {
                        mapVarD.put(code, Double.parseDouble(arr[i]));
                    } else {
                        mapVarD.put(code, Double.parseDouble(arr[arr.length - 1]));
                    }
                }

                for (String var : mapVarD.keySet()) {
                    expressStrLocal = expressStrLocal.replace("@{" + var + "}", String.valueOf(mapVarD.get(var)));
                }

                Expression e = new ExpressionBuilder(expressStrLocal).build();

                lstValue.add((long) e.evaluate());
//                } catch (Exception ex) {
//                    return lstValue;
//                }
            }
            return lstValue;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new ArrayList<>();
        }
    }

    public boolean validateInput(StationPlan station) {
        if (selectProficientType.equals(0L)) {
            if (station != null && station.getCrNumber() != null && !station.getCrNumber().isEmpty()
                    && station.getVendor() != null && station.getNetworkType() != null && !station.getNetworkType().isEmpty()) {
                return true;
            }
        } else if (selectProficientType.equals(1L)) {
            if (station != null && station.getCrNumber() != null && !station.getCrNumber().isEmpty()) {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    public String convertIp(String ip, int type, int i) {
        String result;
        try {
            if (ip != null && !"".equals(ip)) {
                String[] str = ip.split("\\.");
                if (type == 0) {
                    result = str[0] + "." + str[1] + "." + str[2] + "." + "0";
                } else if (type == 1) {
                    result = str[0] + "." + String.valueOf(Long.valueOf(str[1]) + i) + "." + str[2] + "." + str[3];
                } else {
                    result = ip;
                }
            } else {
                return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex;
        }
        return result;
    }

    public static void unZip(List<File> files, File zipFile, String outputFolder) {

        byte[] buffer = new byte[1024];

        try {

            //create output directory is not exists
            File folder = new File(outputFolder);
            if (!folder.exists()) {
                folder.mkdir();
            }

            try (ZipFile zipFile1 = new ZipFile(zipFile.getPath())) {
                Enumeration zipEntries = zipFile1.entries();
                while (zipEntries.hasMoreElements()) {

                    ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
                    String fileName = zipEntry.getName();
                    System.out.println(fileName);
                    if (!zipEntry.isDirectory()) {
                        File newFile = new File(outputFolder + File.separator + fileName);
                        new File(newFile.getParent()).mkdirs();
                        InputStream stream = zipFile1.getInputStream(zipEntry);
                        FileOutputStream fileOuputStream = null;

                        try {
                            fileOuputStream = new FileOutputStream(newFile.getPath());
                            int len;
                            while ((len = stream.read(buffer)) > 0) {
                                fileOuputStream.write(buffer, 0, len);
                            }
                            files.add(newFile);
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                        } finally {
                            if (fileOuputStream != null) {
                                fileOuputStream.close();
                            }
                            stream.close();
                        }
                    }
                }
            }

            System.out.println("Done");
            //Delete zip file
            zipFile.delete();
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public static boolean isNullOrEmpty(String obj1) {
        return (obj1 == null || "".equals(obj1.trim()));
    }

    private static byte[] readBytesFromFile(File file) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }

        }

        return bytesArray;

    }

    private static File exportFileResult(Workbook workbook, LinkedHashMap<String, List<LinkedHashMap<String, String>>> mapData, int rowStart, String fileName, String crNumber) throws IOException, AppException {


//        String fileOut = "E:\\PROJECT\\VTN_QT06_17001_GSTD_FINAL\\VMSA\\VMSA_Web\\out\\artifacts\\vmsa\\templates\\import\\Auto_VMSA" + File.separator + "tmpResult" + new Date().getTime() + ".xlsx";
        fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_" + fileName;
        String fileOut = getFolderSave() + "IntegrateStation" + File.separator + "ResultFileImport" + File.separator + fileName;
        File fileOutResult = new File(fileOut);
        if (!fileOutResult.getParentFile().exists()) {
            fileOutResult.getParentFile().mkdirs();
        }
        fileOut = fileOutResult.getPath();
        try {
            if (workbook == null) {
                throw new NullPointerException();
            }
            CellStyle cellStyle = workbook.createCellStyle();
            Font createFont = workbook.createFont();
            createFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

            cellStyle.setFont(createFont);
            cellStyle.setWrapText(true);
            cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            cellStyle.setBorderTop(CellStyle.BORDER_THIN);
            cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyle.setBorderRight(CellStyle.BORDER_THIN);
            cellStyle.setFillBackgroundColor(HSSFColor.YELLOW.index);
            Sheet sheet = null;

            for (String sheetName : mapData.keySet()) {
                System.out.println("sheetName: " + sheetName);
                sheet = workbook.getSheet(sheetName);
                int lastColumn = mapData.get(sheetName).get(0).size();
                Cell cellTitleCode = sheet.getRow(rowStart).createCell(lastColumn - 2);
                if (cellTitleCode != null) {
                    cellTitleCode.setCellStyle(cellStyle);
                    cellTitleCode.setCellValue(Config.vmsa_result_code);
                }


                Cell cellTitleDetail = sheet.getRow(rowStart).createCell(lastColumn - 1);
                if (cellTitleDetail != null) {
                    cellTitleDetail.setCellStyle(cellStyle);
                    cellTitleDetail.setCellValue(Config.vmsa_result_detail);
                }

                for (int i = rowStart; i < mapData.get(sheetName).size(); i++) {
                    logger.info("CellCode: " + mapData.get(sheetName).get(i).get(Config.vmsa_result_code));
                }

                logger.info("mapData.get(sheetName).size() " + mapData.get(sheetName).size());
                logger.info("sheet.getLastRowNum(): " + sheet.getLastRowNum());
                for (int i = rowStart; i < mapData.get(sheetName).size(); i++) {
                    Cell cellCode = sheet.getRow(i + 1).createCell(lastColumn - 2);
                    if (cellCode != null) {
                        cellCode.setCellStyle(cellStyle);
                        cellCode.setCellValue(mapData.get(sheetName).get(i).get(Config.vmsa_result_code) == null ? "" : mapData.get(sheetName).get(i).get(Config.vmsa_result_code));
                    }
                    Cell cellDetail = sheet.getRow(i + 1).createCell(lastColumn - 1);
                    if (cellDetail != null && cellStyle != null) {
                        cellDetail.setCellStyle(cellStyle);
                        cellDetail.setCellValue(mapData.get(sheetName).get(i).get(Config.vmsa_result_detail) == null ? "" : mapData.get(sheetName).get(i).get(Config.vmsa_result_detail));
                    }
                }
            }
            try {
                FileOutputStream outputStream = new FileOutputStream(fileOut);
                workbook.write(outputStream);
                workbook.close();
            } catch (FileNotFoundException e) {
                logger.error(e.getMessage(), e);
                throw e;
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
                throw e;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
//                    throw e;
                }
            }
        }
        return fileOutResult;
    }

    public TreeMap<String, HashMap<String, String>> sortHashMap(HashMap<String, HashMap<String, String>> map) {
        try {
            TreeMap<String, HashMap<String, String>> treeMap = new TreeMap<>(map);
            Set set = map.entrySet();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                Map.Entry me = (Map.Entry) iterator.next();
            }
            return treeMap;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return new TreeMap<>(map);
    }
    //</editor-fold>
}
