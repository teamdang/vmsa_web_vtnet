package com.viettel.rest;

import com.viettel.util.*;
import static com.viettel.util.HibernateUtil.getSessionFactory;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.sql.Timestamp;
import java.util.*;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BlobType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

/**
 * Created by hanv15 on 10/04/2017.
 */
public class DbTask {

    static Logger logger = Logger.getLogger(DbTask.class);

    public static void main(String[] args) {

        try {
//            DbTask dbTask = new DbTask();
            /*
             */
//            Session session = null;
//
////            session = HibernateUtil.getSessionFactory().openSession();
////            Long flowRunId = null;
////            flowRunId = 882L;
//            String sql = " select \n"
//                    + " tbl1.STEP_NUM, tbl1.GROUP_ACTION_ORDER, tbl1.GROUP_ACTION_NAME, tbl1.NODE_ID, tbl1.NODE_CODE, tbl1.NODE_IP, tbl1.area_code,\n"
//                    + " tbl1.VENDOR_NAME, tbl1.TYPE_NAME, tbl2.detail_id, tbl1.action_id, tbl1.IF_VALUE, tbl1.NAME, tbl1.PREVIOUS_STEP, \n"
//                    + " tbl1.STEP_NUMBER_LABEL, tbl1.IS_ROLLBACK, tbl1.IS_STOP_HERE, tbl1.DELAY_TIME, tbl2.COMMAND_DETAIL_ID, tbl2.OPERATOR_val,\n"
//                    + " tbl2.PROTOCOL, tbl2.STANDARD_VALUE, tbl2.CMD, tbl2.CMD_END, tbl2.CMD_TIMEOUT, tbl2.COLUMN_OUTPUT, tbl2.COUNT_ROW,\n"
//                    + " tbl2.PAGING_CMD, tbl2.PAGING_REGEX, tbl2.REGEX_OUTPUT, tbl2.ROW_END, tbl2.ROW_START, tbl2.SPLIT_COLUMN_REGEX,\n"
//                    + " tbl2.ROW_OUTPUT, tbl2.COMMAND_NAME, tbl2.ACTION_COMMAND_ID, tbl2.ORDER_RUN, tbl2.ROW_START_OPERATOR, tbl2.ROW_END_OPERATOR,\n"
//                    + " tbl2.IS_IMPORTANT, tbl2.CONFIRM_CMD, tbl2.CONFIRM_REGEX, tbl2.CONFIRM_CMD_TYPE, tbl2.DIRECTION_TYPE\n"
//                    + " from \n"
//                    + " (\n"
//                    + "         select fran.FLOW_RUN_NAME, fts.FLOW_TEMPLATE_NAME, aofw.ACTION_ID, aofw.GROUP_ACTION_NAME, aofw.GROUP_ACTION_ORDER,\n"
//                    + "         aofw.IF_VALUE, aofw.PREVIOUS_STEP, aofw.STEP_NUM, aofw.STEP_NUMBER_LABEL, aofw.IS_STOP_HERE,\n"
//                    + "         nrn.NODE_ID, ne.NODE_IP, ne.JDBC_URL, ne.NODE_TYPE_ID, ne.VENDOR_ID, ne.VERSION_ID, ne.NODE_CODE, an.NAME,\n"
//                    + "         vr.VENDOR_NAME, aofw.IS_ROLLBACK, aofw.DELAY_TIME, vn.VERSION_NAME, nte.TYPE_NAME, p.area_code\n"
//                    + "         from FLOW_RUN_ACTION fran, FLOW_TEMPLATES fts, ACTION_OF_FLOW aofw, VENDOR vr,\n"
//                    + "         ACTION an, NODE_RUN nrn, node ne, version vn, NODE_TYPE nte, province p,\n"
//                    + "         (select nrgan.FLOW_RUN_ID, nrgan.NODE_ID, aofw.GROUP_ACTION_NAME, aofw.GROUP_ACTION_ORDER\n"
//                    + "         from NODE_RUN_GROUP_ACTION nrgan, FLOW_RUN_ACTION fran, ACTION_OF_FLOW aofw\n"
//                    + "         where nrgan.FLOW_RUN_ID = fran.FLOW_RUN_ID and fran.FLOW_TEMPLATES_ID = aofw.FLOW_TEMPLATES_ID\n"
//                    + "         and nrgan.STEP_NUM = aofw.STEP_NUM) nrg\n"
//                    + "         where fran.FLOW_TEMPLATES_ID = fts.FLOW_TEMPLATES_ID and fts.FLOW_TEMPLATES_ID = aofw.FLOW_TEMPLATES_ID\n"
//                    + "         and aofw.ACTION_ID = an.ACTION_ID and nrn.FLOW_RUN_ID = fran.FLOW_RUN_ID and nrn.NODE_ID = ne.NODE_ID\n"
//                    + "         and fran.FLOW_RUN_ID = nrg.FLOW_RUN_ID and ne.NODE_ID = nrg.NODE_ID\n"
//                    + "         and aofw.GROUP_ACTION_ORDER = nrg.GROUP_ACTION_ORDER and aofw.GROUP_ACTION_NAME = nrg.GROUP_ACTION_NAME\n"
//                    + "         and ne.version_id = vn.version_id and ne.node_type_id = nte.type_id and ne.province_code = p.bccs_code(+)\n"
//                    + "         and ne.VENDOR_ID = vr.VENDOR_ID and fran.FLOW_RUN_ID = 1006\n"
//                    + ") tbl1 left join\n"
//                    + "(\n"
//                    + "		 select cdl.COMMAND_DETAIL_ID, cdl.OPERATOR OPERATOR_val, cdl.PROTOCOL, cdl.STANDARD_VALUE, ctpr.CMD,\n"
//                    + "         ctpr.CMD_END, ctpr.CMD_TIMEOUT, ctpr.COLUMN_OUTPUT, ctpr.COUNT_ROW, ctpr.PAGING_CMD, ctpr.PAGING_REGEX,\n"
//                    + "         ctpr.REGEX_OUTPUT, ctpr.ROW_END, ctpr.ROW_OUTPUT, ctpr.ROW_START, ctpr.SPLIT_COLUMN_REGEX, cdl.COMMAND_NAME,\n"
//                    + "         ctpr.ROW_START_OPERATOR, ctpr.ROW_END_OPERATOR, ctpr.confirm_regex, ctpr.confirm_cmd, ctpr.confirm_cmd_type, ctpr.direction_type,\n"
//                    + "         acd.ACTION_DETAIL_ID, cdl.VERSION_ID, adl.DETAIL_ID,\n"
//                    + "         cdl.NODE_TYPE_ID, cdl.VENDOR_ID, acd.ACTION_COMMAND_ID, acd.ORDER_RUN, adl.action_id, acd.IS_IMPORTANT\n"
//                    + "         from COMMAND_DETAIL cdl, ACTION_COMMAND acd, COMMAND_TELNET_PARSER ctpr, ACTION_DETAIL adl\n"
//                    + "         where acd.COMMAND_DETAIL_ID = cdl.COMMAND_DETAIL_ID\n"
//                    + "         and cdl.PARSER_ID = ctpr.TELNET_PARSER_ID and acd.action_detail_id = adl.detail_id\n"
//                    + ") tbl2\n"
//                    + "         on tbl1.VENDOR_ID = tbl2.VENDOR_ID \n"
//                    + "         and tbl1.VERSION_ID = tbl2.VERSION_ID \n"
//                    + "		 and tbl1.action_id = tbl2.action_id\n"
//                    + "         order by tbl1.GROUP_ACTION_ORDER, tbl1.GROUP_ACTION_NAME, tbl1.NODE_ID, tbl1.STEP_NUMBER_LABEL, tbl2.ORDER_RUN";
//
//            SQLQuery query = session.createSQLQuery(sql);
////            query.setLong(0, flowRunId);
//
//            List r = query.list();
//
//            if (r != null && r.size() > 0) {
//                for (Object obj : r) {
//                    System.out.println(obj.toString());
//                    break;
//                }
//            }

            //return json
            System.out.println("Main test");

        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }

    public Map<String, String> getSQLByCode(String code) {

        String sql = "SELECT SQL_TEXT, SQL_TYPE FROM CFG_WEB_SERVICE_SQL WHERE STATUS = 1 AND CODE = :code";

        Session session = null;
        Map<String, String> mapResult = new HashMap<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            SQLQuery sqlCuery = session.createSQLQuery(sql);
            sqlCuery.setString("code", code);

            List<Object[]> result = sqlCuery.list();

            if (result != null && !result.isEmpty()) {

                if (result.get(0)[0] != null) {
                    mapResult.put("SQL_TEXT", result.get(0)[0].toString());
                }
                if (result.get(0)[1] != null) {
                    mapResult.put("SQL_TYPE", result.get(0)[1].toString());
                }
                if (mapResult.get("SQL_TEXT") != null && mapResult.get("SQL_TYPE") != null) {
                    return mapResult;
                }
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return mapResult;
    }

    public static String getServiceFromFlowRun(Long flowRunId, Logger logger) {

        String sql2 = "select s.SERVICE_CODE from FLOW_RUN_ACTION f join SERVICE_TEMPLATE s on s.FLOW_TEMPLATE_ID = f.FLOW_TEMPLATES_ID where f.FLOW_RUN_ID = ?";

        Long obj = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            SQLQuery query = session.createSQLQuery(sql2);
            query.setLong(0, flowRunId);

            List r = query.list();

            if (r != null && !r.isEmpty()) {
                obj = Long.parseLong(r.get(r.size() - 1).toString());
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        //IPTABLE(14), FIREWALL(15),
        if (obj != null) {
            if (obj.equals(14L)) {
                return "POLICY_IPTABLE";
            }
            if (obj.equals(15L)) {
                return "POLICY_FIREWALL";
            }
        }
        return "";
    }

    public Long getSequence(String seqName, Logger logger) {

        Session session = null;
        Long seqId = null;
        String sql2 = "select " + seqName + ".nextval from dual ";

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            SQLQuery query = session.createSQLQuery(sql2);
            List r = query.list();
            if (r != null && !r.isEmpty()) {
                for (Object o : r) {
                    seqId = Long.parseLong(o.toString());
                    break;
                }
            }
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return seqId;
    }

    public Long insertFlowRunLogAction(Long flowRunId, String username, Long actionOfFlowId, Long cloneNumber,
            Long cloneTotal, Long flowRunLogId, Long nodeId, Logger logger) {
        Session session = null;
        Transaction tx = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            Long flowRunManualId = getSequence("FLOW_RUN_LOG_ACTION_SEQ", logger);

            String sql = "insert into FLOW_RUN_LOG_ACTION a (a.RUN_LOG_ACTION_ID, a.FLOW_RUN_LOG_ID, a.ACTION_OF_FLOW_ID, a.CLONE_NUMBER,"
                    + "a.START_TIME, a.USERNAME, a.TYPE, a.NODE_ID, a.CLONE_TOTAL) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

            SQLQuery query = session.createSQLQuery(sql);
            int i = 0;
            query.setLong(i++, flowRunManualId);
            query.setLong(i++, flowRunLogId == null ? flowRunId : flowRunLogId);
            query.setLong(i++, actionOfFlowId);
            query.setLong(i++, cloneNumber);
            query.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
            query.setString(i++, username);
            query.setLong(i++, flowRunLogId == null ? 0 : 1);
            query.setLong(i++, nodeId);
            query.setLong(i++, cloneTotal);

            if (query.executeUpdate() > 0) {
                return flowRunManualId;
            }
            tx.commit();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error(ex.getMessage(), ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    public FlowRunObject getFlowRunStatus(Long flowRunId, Date currDate, Logger logger) throws Exception {

        String sql2 = "select a.STATUS status, a.LOG_FILE_PATH logFilePath, "
                + " a.FLOW_RUN_NAME flowRunName, a.CR_NUMBER crNumber, a.LOG_FILE_CONTENT strLogFileContent"//
                + " from flow_run_action a where a.FLOW_RUN_ID = ?";

//        FileOutputStream fout = null;
        Session session = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            SQLQuery query = session.createSQLQuery(sql2);
            query.setLong(0, flowRunId);
            query
                    .addScalar("status", IntegerType.INSTANCE)
                    .addScalar("logFilePath", StringType.INSTANCE)
                    .addScalar("flowRunName", StringType.INSTANCE)
                    .addScalar("crNumber", StringType.INSTANCE)
                    .addScalar("strLogFileContent", BlobType.INSTANCE)
                    .setResultTransformer(Transformers.aliasToBean(FlowRunObject.class));

//            List<Object[]> r = query.list();
            List<FlowRunObject> r = query.list();
            if (r != null && !r.isEmpty()) {
//                for (Object[] o : r) {
//                    FlowRunObject obj = new FlowRunObject();
//                    obj.setStatus((Integer) o[0]);
//                }
                return r.get(r.size() - 1);
            }

//            if (rs.next()) {
//                FlowRunObject obj = new FlowRunObject();
//
//                obj.setFlowRunName(rs.getString("FLOW_RUN_NAME"));
//                obj.setLogFilePath(rs.getString("LOG_FILE_PATH"));
//                obj.setStatus(Integer.parseInt(rs.getString("STATUS")));
//                obj.setCrNumber(rs.getString("CR_NUMBER"));
//
//                Blob blob = rs.getBlob("LOG_FILE_CONTENT");
//                if (blob != null && blob.length() > 0) {
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//                    File file = new File(Start.pathSaveLog + File.separator + sdf.format(currDate)
//                            + File.separator + Utils.clearHornUnicode(obj.getFlowRunName()) + ".zip");
//                    fout = new FileOutputStream(file);
//                    IOUtils.copy(blob.getBinaryStream(), fout);
//                    logger.info("LOG_FILE_CONTENT not null");
//                } else {
//                    logger.info("LOG_FILE_CONTENT null");
//                }
//
//                return obj;
//            }
        } catch (Exception ex) {
            logger.error(ex);
        } finally {

//            try {
//                if (fout != null) {
//                    fout.close();
//                }
//            } catch (Exception ex) {
//                logger.error(ex.getMessage(), ex);
//            }
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    public List<Map<String, Object>> getDataFromSQL(Logger logger, String sql, Map<String, Object> params) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            SQLQuery query = session.createSQLQuery(sql);
            query.setResultTransformer(MyResultTransformer.INSTANCE);
            if (params != null && !params.isEmpty()) {
                for (Map.Entry<String, Object> entry : params.entrySet()) {
                    Object value = entry.getValue();
                    if (value instanceof List) {
                        query.setParameterList(entry.getKey(), (List) entry.getValue());
                    } else {
                        query.setParameter(entry.getKey(), entry.getValue());
                    }
                }
            }
            query.setFetchSize(5000);
            List<Map<String, Object>> result = query.list();
            return result;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<Map<String, Object>> executeUpdate(Logger logger, String sql, Map<String, Object> params) throws Exception {
        Session session = null;
        Transaction tx = null;
        List<Map<String, Object>> result = new ArrayList<>();
        try {

            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            SQLQuery query = session.createSQLQuery(sql);
            if (params != null && !params.isEmpty()) {
                for (Map.Entry<String, Object> entry : params.entrySet()) {
                    Object value = entry.getValue();
                    if (value instanceof List) {
                        query.setParameterList(entry.getKey(), (List) entry.getValue());
                    } else {
//                        if (value instanceof byte[]) {
//                            query.setBinary(entry.getKey(), value);
//                        } else {
                        query.setParameter(entry.getKey(), entry.getValue());
//                        }
                    }
                }
            }
            query.setFetchSize(5000);
            int count = query.executeUpdate();
            Map<String, Object> mapResult = new HashMap<>();
            mapResult.put("STATUS", "Successful");
            mapResult.put("COUNT", count);
            result.add(mapResult);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error(e.getMessage(), e);
            Map<String, Object> mapResult = new HashMap<>();
            mapResult.put("STATUS", "Failed");
            mapResult.put("COUNT", null);
            result.add(mapResult);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return result;
    }

    public void updateCRLogFile(String crNumber, byte[] logFile, Logger logger) throws Exception {

        Session session = null;
        Transaction t = null;

        try {

            session = HibernateUtil.getSessionFactory().openSession();
            t = session.beginTransaction();

            String sql = "update CR_LOG_FILE a set a.LOG_FILE = :logFile where a.CR_NUMBER = :crNumber";

            SQLQuery query = session.createSQLQuery(sql);

            query.setBinary("logFile", logFile);//sua lai phan nay thanh luu file chu ko luu noi dung file
            query.setString("crNumber", crNumber);

            query.executeUpdate();

            t.commit();
        } catch (Exception ex) {
            if (t != null) {
                t.rollback();
            }
            logger.error(ex.getMessage(), ex);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<Map<String, Object>> updateParamValue(final List<ParamRun> params, Logger logger) {

        Session session = null;
        Transaction tx = null;
        List<Map<String, Object>> result = new ArrayList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.doWork(new Work() {
                @Override
                public void execute(Connection conn) throws SQLException {
                    PreparedStatement pstmt = null;
                    try {
                        StringBuilder sql = new StringBuilder();
                        sql.append(" update PARAM_VALUE a set a.PARAM_VALUE = ? where a.FLOW_RUN_ID = ? and a.NODE_ID = ? and a.PARAM_CODE = ?");
                        pstmt = conn.prepareStatement(sql.toString());
                        int count = 0;
                        for (ParamRun param : params) {
                            pstmt.setString(1, param.getParamValue());
                            pstmt.setLong(2, param.getFlowRunId());
                            pstmt.setLong(3, param.getNodeId());
                            pstmt.setString(4, param.getParamCode());
                            pstmt.addBatch();
                            count++;
                            if (count >= 500) {
                                pstmt.executeBatch();
                                pstmt.clearBatch();
                                count = 0;
                            }
                        }
                        if (count > 0) {
                            pstmt.executeBatch();
//                            pstmt.clearBatch();
                        }
                        pstmt.executeBatch();
                    } finally {
                        if (pstmt != null) {
                            pstmt.close();
                        }
                    }
                }
            });

            tx.commit();
            Map<String, Object> mapResult = new HashMap<>();
            mapResult.put("STATUS", "Successful");
            mapResult.put("COUNT", 0);
            result.add(mapResult);
        } catch (Exception e) {
            logger.error(e);
            if (tx != null) {
                tx.rollback();
            }
            Map<String, Object> mapResult = new HashMap<>();
            mapResult.put("STATUS", "Failed");
            mapResult.put("COUNT", null);
            result.add(mapResult);
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return result;
    }
}
