/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.rest;

/**
 *
 * @author hienhv4
 */
public class Constants {

    //20170427_HaNV15_Add_Start
    public final static String DEFAULT_SALT = "default_$AL7";
    //20170427_HaNV15_Add_End

    //20170510_HaNV15_Add_Start: country code on session
    public final static String COUNTRY_CODE_CURRENT = "countryCodeCurrent";
    public final static String VNM = "VNM";
    public final static String VTP = "VTP";
    public final static String VTZ = "VTZ";
    //20170510_HaNV15_Add_End

    public static final Long CMD_TIMEOUT_DEFAULT = 30000l; //Thoi gian timeout lenh default (30s)

    public static final String SPLITTER_PARAM = ";"; // Ky tu phan tach khi nhap nhieu gia tri cho tham so

    public static final int SAVE_FLAG = 0;
    public static final int WAITTING_FLAG = 1;
    public static final int RUNNING_FLAG = 2;
    public static final int FINISH_FLAG = 3;
    public static final int FAIL_FLAG = 4;
    public static final int STOP_FLAG = 6;
    public static final int SCHEDULE_FLAG = 7;
    public static final int STOP_TEMP_FLAG = 8;

    public static final String LOGIN_PROMPT = "(>|#)";

    public static final String LOGIN_FAIL = "LOGIN_FAIL";
    public static final String CONN_TIMEOUT = "CONN_TIMEOUT";
    public static final String AUTHEN_FAIL = "AUTHEN_FAIL";
    public static final String CONN_LOST = "CONN_LOST";

    public static final int RUN_TYPE_IMPACT = 1;
    public static final int RUN_TYPE_ROLLBACK = 2;

    public static final int ERROR_MODE_ROLLBACK = 1;
    public static final int ERROR_MODE_STOP = 2;

    public static final int RUNNING_TYPE_INDEPENDECE = 1; // Cac node chay doc lap
    public static final int RUNNING_TYPE_DEPENDENT = 2; // Cac node chay song song

    public static final String CR_AUTO_DECLARE_CUSTOMER = "CR_AUTO";
    public static final String CR_DEFAULT = "CR_DEFAULT";
}
