package com.viettel.rest;

import java.util.List;

/**
 * Created by hanv15 on 19/04/2017.
 */
public class DataParamRun {

    private List<ParamRun> lstParamRun;

    public DataParamRun() {
    }

    public DataParamRun(List<ParamRun> lstParamRun) {
        this.lstParamRun = lstParamRun;
    }

    public List<ParamRun> getLstParamRun() {
        return lstParamRun;
    }

    public void setLstParamRun(List<ParamRun> lstParamRun) {
        this.lstParamRun = lstParamRun;
    }
}
