/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.rest;

/**
 *
 * @author hienhv4
 */
public class ParamRun {

    private String paramCode;
    private String paramValue;
    private Long paramValueId;

    private Long actionFlowOutId;
    private Long actionDetailOutId;
    private Long commandDetailOutId;

    private Long nodeId;

    //20170419_HaNV15_Add_Start
    private Long flowRunId;
    //20170419_HaNV15_Add_End

    public ParamRun(String paramCode, String paramValue) {
        this.paramCode = paramCode;
        this.paramValue = paramValue;
    }

    public ParamRun(String paramCode, String paramValue, Long paramValueId) {
        this.paramCode = paramCode;
        this.paramValue = paramValue;
        this.paramValueId = paramValueId;
    }

    public ParamRun(String paramCode, Long actionFlowOutId, Long actionDetailOutId, Long commandDetailOutId) {
        this.paramCode = paramCode;
        this.actionFlowOutId = actionFlowOutId;
        this.actionDetailOutId = actionDetailOutId;
        this.commandDetailOutId = commandDetailOutId;
    }

    public String getParamCode() {
        return paramCode;
    }

    public void setParamCode(String paramCode) {
        this.paramCode = paramCode;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public Long getParamValueId() {
        return paramValueId;
    }

    public void setParamValueId(Long paramValueId) {
        this.paramValueId = paramValueId;
    }

    public Long getActionFlowOutId() {
        return actionFlowOutId;
    }

    public void setActionFlowOutId(Long actionFlowOutId) {
        this.actionFlowOutId = actionFlowOutId;
    }

    public Long getActionDetailOutId() {
        return actionDetailOutId;
    }

    public void setActionDetailOutId(Long actionDetailOutId) {
        this.actionDetailOutId = actionDetailOutId;
    }

    public Long getCommandDetailOutId() {
        return commandDetailOutId;
    }

    public void setCommandDetailOutId(Long commandDetailOutId) {
        this.commandDetailOutId = commandDetailOutId;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getFlowRunId() {
        return flowRunId;
    }

    public void setFlowRunId(Long flowRunId) {
        this.flowRunId = flowRunId;
    }
}
