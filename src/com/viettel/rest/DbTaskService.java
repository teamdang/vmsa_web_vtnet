package com.viettel.rest;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.google.gson.Gson;
import com.viettel.object.ParameterBO;
import com.viettel.object.RequestInputBO;
import com.viettel.passprotector.PassProtector;
import com.viettel.persistence.DaoSimpleService;
import com.viettel.util.DateTimeUtils;
import com.viettel.util.MessageUtil;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import java.io.StringWriter;
import java.sql.Blob;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import org.primefaces.util.Base64;
import org.apache.commons.io.IOUtils;

/**
 * Created by hanv15 on 08/04/2017.
 */
@Path("DbTaskService")
public class DbTaskService {

    Logger logger = Logger.getLogger(DbTaskService.class);

    @Context
    HttpServletRequest servletRequest;

    @GET
    @Path("/check")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response get() {
        return Response.status(200).entity("NO_DATA").build();
    }

    @POST
    @Path("/query")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDataJson(@FormParam("userService") String userService, @FormParam("passService") String passService,
            RequestInputBO requestInputBO) {
        if (!checkWhiteListIp()) {
            return Response.status(403).entity("AUTHEN_FAIL").build();
        }
        Response isAuthen = authentication(userService, passService);
        if (isAuthen != null) {
            return isAuthen;
        }
        String r = "";
        String defaultTimezone = "Asia/Bangkok";
        //login successful to service

        //get query
        DbTask dbTask = new DbTask();
        try {
            TimeZone timeZone = TimeZone.getTimeZone(defaultTimezone);
            Map<String, String> mapSql = dbTask.getSQLByCode(requestInputBO.getCode());

            if (validString(requestInputBO.getQuery())) {//get data use query

            } else {//get data by query where code
                if (validString(mapSql.get("SQL_TEXT"))) {
                    String sql = mapSql.get("SQL_TEXT");
                    if ("1".equals(mapSql.get("SQL_TYPE"))) {//select
                        r = createDataJson(dbTask, requestInputBO, sql, timeZone, null);
                    } else if ("2".equals(mapSql.get("SQL_TYPE"))) {//insert, update, delete
                        //convert to map param
                        r = createDataJson(dbTask, requestInputBO, sql, timeZone, null);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return Response.status(Response.Status.OK).entity(r).build();
    }

    @POST
    @Path("/query2")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getData(@FormParam("userService") String userService, @FormParam("passService") String passService,
            @FormParam("input") String input) {
        if (!checkWhiteListIp()) {
            return Response.status(403).entity("AUTHEN_FAIL").build();
        }
        Response isAuthen = authentication(userService, passService);
        if (isAuthen != null) {
            return isAuthen;
        }
        String r = "";

        //convert string input json to Object
        String defaultTimezone = "Asia/Bangkok";
        //login successful to service

        //get query
        DbTask dbTask = new DbTask();
        try {
            Gson g = new Gson();

            RequestInputBO requestInputBO = g.fromJson(input, RequestInputBO.class);

//            JsonObject jsonObjectServer = g.fromJson(input, JsonObject.class);
//            JsonElement jsonArrayServer = jsonObjectServer.get("data");
//            Type listTypeServer = new TypeToken<List<ServerBO>>() {
//            }.getType();
//            List<ServerBO> listServerBO;
//            listServerBO = g.fromJson(jsonArrayServer, listTypeServer);
            TimeZone timeZone = TimeZone.getTimeZone(defaultTimezone);
            Map<String, String> mapSql = dbTask.getSQLByCode(requestInputBO.getCode());

            if (validString(requestInputBO.getQuery())) {//get data use query
                String sql = requestInputBO.getQuery();
                r = createDataJson(dbTask, requestInputBO, sql, timeZone, null);
            } else {//get data by query where code
                if (mapSql != null && validString(mapSql.get("SQL_TEXT"))) {
                    String sql = mapSql.get("SQL_TEXT");
//                    Map<String, Object> mapParam = getMapParam(requestInputBO.getParams(), timeZone);
                    if ("1".equals(mapSql.get("SQL_TYPE"))) {//select
                        r = createDataJson(dbTask, requestInputBO, sql, timeZone, null);
                    } else if ("2".equals(mapSql.get("SQL_TYPE"))) {//insert, update, delete
                        //convert to map param
                        r = executeUpdate(dbTask, requestInputBO, sql, timeZone, null);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("ERROR:", e);
        }
        return Response.status(Response.Status.OK).entity(r).build();
    }

    @POST
    @Path("/getFile")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response doTest(@FormParam("userService") String userService, @FormParam("passService") String passService,
            @FormParam("input") String input) throws Exception {
        if (!checkWhiteListIp()) {
            return Response.status(403).entity("AUTHEN_FAIL").build();
        }
        Response isAuthen = authentication(userService, passService);
        if (isAuthen != null) {
            return isAuthen;
        }
        DbTask dbTask = new DbTask();
        String defaultTimezone = "Asia/Bangkok";

        try {
            Gson g = new Gson();

            RequestInputBO requestInputBO = g.fromJson(input, RequestInputBO.class);

            TimeZone timeZone = TimeZone.getTimeZone(defaultTimezone);
            Map<String, String> mapSql = dbTask.getSQLByCode(requestInputBO.getCode());

            if (mapSql != null && validString(mapSql.get("SQL_TEXT"))) {
                String sql = mapSql.get("SQL_TEXT");
                InputStream is = getInputStream(dbTask, requestInputBO, sql, timeZone);

                return Response.ok((InputStream) is).build();
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return null;

    }

    @POST
    @Path("/updateCRLogFile")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateCRLogFile(@FormParam("userService") String userService, @FormParam("passService") String passService,
            @FormParam("input") String input) {
        if (!checkWhiteListIp()) {
            return Response.status(403).entity("AUTHEN_FAIL").build();
        }
        Response isAuthen = authentication(userService, passService);
        if (isAuthen != null) {
            return isAuthen;
        }
        String r = "{\"data\":\"\"}";
        try {
            Gson g = new Gson();

            RequestInputBO requestInputBO = g.fromJson(input, RequestInputBO.class);
            DbTask dbTask = new DbTask();
            dbTask.updateCRLogFile(requestInputBO.getParams().get(0).getValue(), requestInputBO.getParams().get(1).getValue().getBytes(), logger);
        } catch (Exception ex) {
            r = "FAILED";
            logger.error(ex);
        }
        return Response.status(Response.Status.OK).entity(r).build();
    }

    @POST
    @Path("/updateParamValue")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateParamValue(@FormParam("userService") String userService, @FormParam("passService") String passService,
            @FormParam("input") String input) {
        if (!checkWhiteListIp()) {
            return Response.status(403).entity("AUTHEN_FAIL").build();
        }
        Response isAuthen = authentication(userService, passService);
        if (isAuthen != null) {
            return isAuthen;
        }
        String r = "";

        try {

            Gson g = new Gson();

            DataParamRun data = g.fromJson(input, DataParamRun.class);
            List<Map<String, Object>> result = new ArrayList<>();
            if (data != null && data.getLstParamRun() != null && data.getLstParamRun().size() > 0) {
                System.out.println(data.getLstParamRun().get(0).getParamCode());
                DbTask dbTask = new DbTask();
                result = dbTask.updateParamValue(data.getLstParamRun(), logger);
            }
            r = buildResponse(result, "dd/MM/yyyy HH:mm:ss");
        } catch (Exception ex) {
            logger.error(ex);
        }
        return Response.status(Response.Status.OK).entity(r).build();
    }

    public Map<String, Object> getMapParam(List<ParameterBO> params, TimeZone timeZone) {
        Map<String, Object> mapParam = new HashMap<String, Object>();
        try {
            if (params != null && !params.isEmpty()) {
                for (ParameterBO bo : params) {
                    if (validString(bo.getValue())) {
                        if (validString(bo.getSeparator())) {
                            String[] tmps = bo.getValue().split(bo.getSeparator());
                            if (tmps.length > 0) {
                                List<Object> lst = new ArrayList<Object>();
                                for (String value : tmps) {
                                    if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                                        if (value.matches("-?\\d+\\.?\\d*")) {
                                            if (value.contains(".")) {
                                                lst.add(Double.parseDouble(value.trim()));
                                            } else {
                                                lst.add(Long.parseLong(value.trim()));
                                            }
                                        }
                                    } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                                        if (validString(bo.getFormat())) {
                                            lst.add(DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                                        } else {
                                            lst.add(new Date(Long.valueOf(value.trim())));
                                        }
                                    } else {
                                        lst.add(value.trim());
                                    }
                                }
                                mapParam.put(bo.getName(), lst);
                            }
                        } else {
                            String value = bo.getValue();
                            if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                                if (value.matches("-?\\d+\\.?\\d*")) {
                                    if (value.contains(".")) {
                                        mapParam.put(bo.getName(), Double.parseDouble(value.trim()));
                                    } else {
                                        mapParam.put(bo.getName(), Long.parseLong(value.trim()));
                                    }
                                }
                            } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                                if (validString(bo.getFormat())) {
                                    mapParam.put(bo.getName(), DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                                } else {
                                    mapParam.put(bo.getName(), new Date(Long.valueOf(value.trim())));
                                }
                            } else {
                                mapParam.put(bo.getName(), value.trim());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return mapParam;
    }

    private String createDataJson(DbTask dbTask, RequestInputBO requestInputBO, String sqlByCode, TimeZone timeZone, String dateFormat) throws Exception {
        //Param
        Map<String, Object> mapParam = new HashMap<String, Object>();
        if (requestInputBO.getParams() != null && !requestInputBO.getParams().isEmpty()) {
            for (ParameterBO bo : requestInputBO.getParams()) {
                if (validString(bo.getValue())) {
                    if (validString(bo.getSeparator())) {
                        String[] tmps = bo.getValue().split(bo.getSeparator());
                        if (tmps.length > 0) {
                            List<Object> lst = new ArrayList<Object>();
                            for (String value : tmps) {
                                if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                                    if (value.matches("-?\\d+\\.?\\d*")) {
                                        if (value.contains(".")) {
                                            lst.add(Double.parseDouble(value.trim()));
                                        } else {
                                            lst.add(Long.parseLong(value.trim()));
                                        }
                                    }
                                } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                                    if (validString(bo.getFormat())) {
                                        lst.add(DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                                    } else {
                                        lst.add(new Date(Long.valueOf(value.trim())));
                                    }
                                } else {
                                    lst.add(value.trim());
                                }
                            }
                            mapParam.put(bo.getName(), lst);
                        }
                    } else {
                        String value = bo.getValue();
                        if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                            if (value.matches("-?\\d+\\.?\\d*")) {
                                if (value.contains(".")) {
                                    mapParam.put(bo.getName(), Double.parseDouble(value.trim()));
                                } else {
                                    mapParam.put(bo.getName(), Long.parseLong(value.trim()));
                                }
                            }
                        } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                            if (validString(bo.getFormat())) {
                                mapParam.put(bo.getName(), DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                            } else {
                                mapParam.put(bo.getName(), new Date(Long.valueOf(value.trim())));
                            }
                        } else {
                            mapParam.put(bo.getName(), value.trim());
                        }
                    }
                }
            }
        }
        logger.info("[" + requestInputBO.getCode() + "] mapParam: " + mapParam);
        logger.info("[" + requestInputBO.getCode() + "] execute query ...");
        List<Map<String, Object>> lst = dbTask.getDataFromSQL(logger, sqlByCode, mapParam);
        logger.info("[" + requestInputBO.getCode() + "] output size: " + lst.size());
//        response.setTotalDataJson(lst.size());
        logger.info("[" + requestInputBO.getCode() + "] Conver json ...");
        SimpleDateFormat formatDateJson = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        if (dateFormat != null && !dateFormat.trim().isEmpty()) {
            formatDateJson = new SimpleDateFormat(dateFormat);
        }
        formatDateJson.setTimeZone(timeZone);
        StringWriter sw = new StringWriter();
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(sw);
        jsonGenerator.setPrettyPrinter(new DefaultPrettyPrinter());
        jsonGenerator.writeStartObject();
        jsonGenerator.writeArrayFieldStart("data");
        for (Map<String, Object> row : lst) {
            jsonGenerator.writeStartObject();
            for (String key : row.keySet()) {
                String column = key.toLowerCase();
                if (row.get(key) != null) {
                    Object value = row.get(key);
                    try {
                        if (value instanceof java.sql.Blob) {
                            Blob val = (java.sql.Blob) value;
                            if ("getFlowRunStatus".equals(requestInputBO.getCode()) && "log_file_content".equals(key)) {
                                String a = IOUtils.toString(val.getBinaryStream());
                                jsonGenerator.writeStringField(column, a);
                            } else {
                                jsonGenerator.writeStringField(column, new String(val.getBytes(1l, 2048)));
                            }
                        } else if (value instanceof String) {
                            jsonGenerator.writeStringField(column, value.toString());
                        } else if (value instanceof Timestamp) {
                            Timestamp time = (Timestamp) value;
                            java.util.Date date = new Date(time.getTime());
                            jsonGenerator.writeStringField(column, formatDateJson.format(date));
                        } else if (value instanceof java.sql.Date) {
                            java.util.Date date = new Date(((java.sql.Date) value).getTime());
                            jsonGenerator.writeStringField(column, formatDateJson.format(date));
                        } else {
                            if ((null != value) && value.toString().matches("-?\\d+\\.?\\d*")) {
                                if (value.toString().contains(".")) {
                                    jsonGenerator.writeNumberField(column, Double.parseDouble(value.toString()));
                                } else {
                                    jsonGenerator.writeNumberField(column, Long.parseLong(value.toString()));
                                }
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw new Exception("Error json write value: " + value + ". " + ex.toString());
                    }
                }
            }
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
        jsonGenerator.flush();
        jsonGenerator.close();

        String content = sw.getBuffer().toString();
        sw.close();
        logger.info("[" + requestInputBO.getCode() + "] Convert json success: OK");
        return content;
    }

    private InputStream getInputStream(DbTask dbTask, RequestInputBO requestInputBO, String sqlByCode, TimeZone timeZone) throws Exception {
        //Param
        Map<String, Object> mapParam = new HashMap<String, Object>();
        if (requestInputBO.getParams() != null && !requestInputBO.getParams().isEmpty()) {
            for (ParameterBO bo : requestInputBO.getParams()) {
                if (validString(bo.getValue())) {
                    if (validString(bo.getSeparator())) {
                        String[] tmps = bo.getValue().split(bo.getSeparator());
                        if (tmps.length > 0) {
                            List<Object> lst = new ArrayList<Object>();
                            for (String value : tmps) {
                                if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                                    if (value.matches("-?\\d+\\.?\\d*")) {
                                        if (value.contains(".")) {
                                            lst.add(Double.parseDouble(value.trim()));
                                        } else {
                                            lst.add(Long.parseLong(value.trim()));
                                        }
                                    }
                                } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                                    if (validString(bo.getFormat())) {
                                        lst.add(DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                                    } else {
                                        lst.add(new Date(Long.valueOf(value.trim())));
                                    }
                                } else {
                                    lst.add(value.trim());
                                }
                            }
                            mapParam.put(bo.getName(), lst);
                        }
                    } else {
                        String value = bo.getValue();
                        if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                            if (value.matches("-?\\d+\\.?\\d*")) {
                                if (value.contains(".")) {
                                    mapParam.put(bo.getName(), Double.parseDouble(value.trim()));
                                } else {
                                    mapParam.put(bo.getName(), Long.parseLong(value.trim()));
                                }
                            }
                        } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                            if (validString(bo.getFormat())) {
                                mapParam.put(bo.getName(), DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                            } else {
                                mapParam.put(bo.getName(), new Date(Long.valueOf(value.trim())));
                            }
                        } else {
                            mapParam.put(bo.getName(), value.trim());
                        }
                    }
                }
            }
        }
        logger.info("[" + requestInputBO.getCode() + "] mapParam: " + mapParam);
        logger.info("[" + requestInputBO.getCode() + "] execute query ...");
        List<Map<String, Object>> lst = dbTask.getDataFromSQL(logger, sqlByCode, mapParam);
        logger.info("[" + requestInputBO.getCode() + "] output size: " + lst.size());

        logger.info("[" + requestInputBO.getCode() + "] Conver json ...");
        InputStream is = null;
        for (Map<String, Object> row : lst) {
            for (String key : row.keySet()) {
                if (row.get(key) != null) {
                    Object value = row.get(key);
                    try {
                        if (value instanceof java.sql.Blob) {
                            Blob val = (java.sql.Blob) value;
                            is = val.getBinaryStream();
                            break;
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw new Exception("Error get input stream: " + value + ". " + ex.toString());
                    } finally {
                    }
                }
            }
        }

        return is;
    }

    private String executeUpdate(DbTask dbTask, RequestInputBO requestInputBO, String sqlByCode, TimeZone timeZone, String dateFormat) throws Exception {
        //Param
        Map<String, Object> mapParam = new HashMap<String, Object>();
        if (requestInputBO.getParams() != null && !requestInputBO.getParams().isEmpty()) {
            for (ParameterBO bo : requestInputBO.getParams()) {
                if (validString(bo.getValue())) {
                    if (validString(bo.getSeparator())) {
                        String[] tmps = bo.getValue().split(bo.getSeparator());
                        if (tmps.length > 0) {
                            List<Object> lst = new ArrayList<Object>();
                            for (String value : tmps) {
                                if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                                    if (value.matches("-?\\d+\\.?\\d*")) {
                                        if (value.contains(".")) {
                                            lst.add(Double.parseDouble(value.trim()));
                                        } else {
                                            lst.add(Long.parseLong(value.trim()));
                                        }
                                    }
                                } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                                    if (validString(bo.getFormat())) {
                                        lst.add(DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                                    } else {
                                        lst.add(new Date(Long.valueOf(value.trim())));
                                    }
                                } else {
                                    lst.add(value.trim());
                                }
                            }
                            mapParam.put(bo.getName(), lst);
                        }
                    } else {
                        String value = bo.getValue();
                        if (validString(bo.getType()) && "NUMBER".equalsIgnoreCase(bo.getType())) {
                            if (value.matches("-?\\d+\\.?\\d*")) {
                                if (value.contains(".")) {
                                    mapParam.put(bo.getName(), Double.parseDouble(value.trim()));
                                } else {
                                    mapParam.put(bo.getName(), Long.parseLong(value.trim()));
                                }
                            }
                        } else if (validString(bo.getType()) && "DATE".equalsIgnoreCase(bo.getType())) {
                            if (validString(bo.getFormat())) {
                                mapParam.put(bo.getName(), DateTimeUtils.convertStringToDate(value.trim(), timeZone, bo.getFormat()));
                            } else {
                                mapParam.put(bo.getName(), new Date(Long.valueOf(value.trim())));
                            }
                        } else if (validString(bo.getType()) && "FILE".equalsIgnoreCase(bo.getType())) {
                            mapParam.put(bo.getName(), org.apache.commons.net.util.Base64.decodeBase64(value));
                        } else {
                            if ("insertLogCommandToDB".equalsIgnoreCase(requestInputBO.getCode())
                                    && ("result".equalsIgnoreCase(bo.getName()) || "resultDetail".equalsIgnoreCase(bo.getName()))) {
                                value = new String(Base64.decode(value.trim()));
                            }
                            mapParam.put(bo.getName(), value.trim());
                        }
                    }
                }
            }
        }
        logger.info("[" + requestInputBO.getCode() + "] mapParam: " + mapParam);
        logger.info("[" + requestInputBO.getCode() + "] execute query ...");
        List<Map<String, Object>> lst = dbTask.executeUpdate(logger, sqlByCode, mapParam);
        logger.info("[" + requestInputBO.getCode() + "] output size: " + lst.size());
//        response.setTotalDataJson(lst.size());
        logger.info("[" + requestInputBO.getCode() + "] Conver json ...");
        SimpleDateFormat formatDateJson = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        if (dateFormat != null && !dateFormat.trim().isEmpty()) {
            formatDateJson = new SimpleDateFormat(dateFormat);
        }
        formatDateJson.setTimeZone(timeZone);
        StringWriter sw = new StringWriter();
        JsonGenerator jsonGenerator = new JsonFactory().createGenerator(sw);
        jsonGenerator.setPrettyPrinter(new DefaultPrettyPrinter());
        jsonGenerator.writeStartObject();
        jsonGenerator.writeArrayFieldStart("data");
        for (Map<String, Object> row : lst) {
            jsonGenerator.writeStartObject();
            for (String key : row.keySet()) {
                String column = key.toLowerCase();
                if (row.get(key) != null) {
                    Object value = row.get(key);
                    try {
                        if (value instanceof String) {
                            jsonGenerator.writeStringField(column, value.toString());
                        } else if (value instanceof Timestamp) {
                            Timestamp time = (Timestamp) value;
                            java.util.Date date = new Date(time.getTime());
                            jsonGenerator.writeStringField(column, formatDateJson.format(date));
                        } else if (value instanceof java.sql.Date) {
                            java.util.Date date = new Date(((java.sql.Date) value).getTime());
                            jsonGenerator.writeStringField(column, formatDateJson.format(date));
                        } else {
                            if ((null != value) && value.toString().matches("-?\\d+\\.?\\d*")) {
                                if (value.toString().contains(".")) {
                                    jsonGenerator.writeNumberField(column, Double.parseDouble(value.toString()));
                                } else {
                                    jsonGenerator.writeNumberField(column, Long.parseLong(value.toString()));
                                }
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw new Exception("Error json write value: " + value + ". " + ex.toString());
                    }
                }
            }
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
        jsonGenerator.flush();
        jsonGenerator.close();

        String content = sw.getBuffer().toString();
        sw.close();
        logger.info("[" + requestInputBO.getCode() + "] Convert json success: OK");
        return content;
    }

    //build response
    public String buildResponse(List<Map<String, Object>> lstDataResponse, String formatDate) {

        StringWriter sw = new StringWriter();
        String content = "";
        try {

            SimpleDateFormat formatDateJson = new SimpleDateFormat(formatDate);
            JsonGenerator jsonGenerator = new JsonFactory().createGenerator(sw);
            jsonGenerator.setPrettyPrinter(new DefaultPrettyPrinter());
            jsonGenerator.writeStartObject();
            jsonGenerator.writeArrayFieldStart("data");
            for (Map<String, Object> row : lstDataResponse) {
                jsonGenerator.writeStartObject();
                for (String key : row.keySet()) {
                    String column = key.toLowerCase();
                    if (row.get(key) != null) {
                        Object value = row.get(key);
                        try {
                            if (value instanceof String) {
                                jsonGenerator.writeStringField(column, value.toString());
                            } else if (value instanceof Timestamp) {
                                Timestamp time = (Timestamp) value;
                                java.util.Date date = new Date(time.getTime());
                                jsonGenerator.writeStringField(column, formatDateJson.format(date));
                            } else if (value instanceof java.sql.Date) {
                                java.util.Date date = new Date(((java.sql.Date) value).getTime());
                                jsonGenerator.writeStringField(column, formatDateJson.format(date));
                            } else {
                                if ((null != value) && value.toString().matches("-?\\d+\\.?\\d*")) {
                                    if (value.toString().contains(".")) {
                                        jsonGenerator.writeNumberField(column, Double.parseDouble(value.toString()));
                                    } else {
                                        jsonGenerator.writeNumberField(column, Long.parseLong(value.toString()));
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            logger.error(ex.getMessage(), ex);
                            throw new Exception("Error json write value: " + value + ". " + ex.toString());
                        }
                    }
                }
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
            jsonGenerator.flush();
            jsonGenerator.close();

            content = sw.getBuffer().toString();
            sw.close();
            logger.info("[" + "" + "] Convert json success: OK");
        } catch (Exception e) {
            logger.error("[" + "" + "] Convert json failed: NOK");
            logger.error("ERROR:", e);
        }
        return content;
    }

    public static boolean validString(String temp) {
        if (temp == null || temp.trim().length() == 0) {
            return false;
        }
        return true;
    }

    private boolean checkWhiteListIp() {
        try {
            String remoteHost = servletRequest.getRemoteHost();
            List<?> ips = new DaoSimpleService().findListSQLAll("SELECT IP from WHITELIST_WS WHERE SYSTEM_NAME='WS_FOR_VIPA'");
            logger.info("IP remote: " + remoteHost);
            if (ips == null || ips.isEmpty()) {
                return true;
            }
            for (Object ip : ips) {
                if (ip != null && ip.equals(remoteHost)) {
                    return true;
                }
            }
            logger.info("Forbidden access: " + remoteHost);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return false;
    }

    private Response authentication(String userService, String passService) {
        try {
            String vipaUser = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_user_service"), "ipchange");
            String vipaPass = PassProtector.decrypt(MessageUtil.getResourceBundleConfig("vipa_pass_service"), "ipchange");

            if (!vipaUser.equals(userService) || !vipaPass.equals(passService)) {
                return Response.status(200).entity("AUTHEN_FAIL").build();
            }
        } catch (final Exception ex) {
            logger.error(ex.getMessage(), ex);
            return Response.status(200).entity("ERROR").build();
        }
        return null;
    }
}
