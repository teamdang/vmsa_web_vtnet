/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.rest;

//import com.viettel.ipchange.autodt.util.Constants;
import java.io.File;
import java.sql.Blob;

/**
 *
 * @author hienhv4
 */
public class FlowRunObject {

    private int status = Constants.WAITTING_FLAG;
    private String logFilePath;
    private String flowRunName;
    private String crNumber;
    private File oldLogFile;
    private Blob strLogFileContent;
    private byte[] logFileContent;//LOG_FILE_CONTENT

    public Blob getStrLogFileContent() {
        return strLogFileContent;
    }

    public void setStrLogFileContent(Blob strLogFileContent) {
        this.strLogFileContent = strLogFileContent;
    }

    public byte[] getLogFileContent() {
        return logFileContent;
    }

    public void setLogFileContent(byte[] logFileContent) {
        this.logFileContent = logFileContent;
    }

    public File getOldLogFile() {
        return oldLogFile;
    }

    public void setOldLogFile(File oldLogFile) {
        this.oldLogFile = oldLogFile;
    }

    public String getCrNumber() {
        return crNumber;
    }

    public void setCrNumber(String crNumber) {
        this.crNumber = crNumber;
    }

    public String getFlowRunName() {
        return flowRunName;
    }

    public void setFlowRunName(String flowRunName) {
        this.flowRunName = flowRunName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }
}
