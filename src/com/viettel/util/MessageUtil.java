package com.viettel.util;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.viettel.resource.AppMessages;

@SuppressWarnings("serial")
public class MessageUtil {

    private volatile static ResourceBundle bundle;
    protected static final Logger LOGGER = LoggerFactory.getLogger(MessageUtil.class);
    private static Locale local;

    public static ResourceBundle getResourceBundle() {
        FacesContext context = FacesContext.getCurrentInstance();
        //if (bundle == null)
        {
            try {
                bundle = context.getApplication()
                        .getResourceBundle(context, "msg");
            } catch (Exception ex) {
                //quytv7 truong hop webservice
                bundle = ResourceBundle.getBundle("com.viettel.resource.messages", local == null ? new Locale("en", "EN") : local, new AppMessages.UTF8Control());
                LOGGER.debug(ex.getMessage(), ex);
            }
        }
        return bundle;
    }

    public static void setResourceBundle() {
        FacesContext context = FacesContext.getCurrentInstance();
        bundle = context.getApplication()
                .getResourceBundle(context, "msg");
    }

    public static void setErrorMessage(String message) {
        try {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error", message);

            FacesContext.getCurrentInstance().addMessage("mainMessage", msg);
        } catch (Exception ex) {
            LOGGER.debug(ex.getMessage(), ex);
        }
    }

    public static void setInfoMessage(String message) {
        try {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info",
                    message);

            FacesContext.getCurrentInstance().addMessage("mainMessage", msg);
        } catch (Exception ex) {
            LOGGER.debug(ex.getMessage(), ex);
        }
    }

    public static void setWarnMessage(String message) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warn",
                message);

        FacesContext.getCurrentInstance().addMessage("mainMessage", msg);
    }

    public static void setFatalMessage(String message) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL,
                "Fatal", message);

        FacesContext.getCurrentInstance().addMessage("mainMessage", msg);
    }

    public static ResourceBundle getResourceBundle(String name) {
        FacesContext context = FacesContext.getCurrentInstance();
//		if (bundle == null) 
        {
            if (context == null) {
                bundle = ResourceBundle.getBundle("com.viettel.resource.messages", local == null ? new Locale("vi", "VN") : local, new AppMessages.UTF8Control());
            } else {
                bundle = context.getApplication().getResourceBundle(context, name);
            }
        }
        return bundle;
    }

    public static String getResourceBundleMessage(String key) {
        if (key == null) {
            return key;
        }
        if ("".equals(key)) {
            return "";
        }
        try {
            bundle = getResourceBundle();
            return bundle.getString(key);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return key;
    }

    public static void setInfoMessageFromRes(String key) {
        setInfoMessage(getResourceBundleMessage(key));
    }

    public static void setErrorMessageFromRes(String key) {
        setErrorMessage(getResourceBundleMessage(key));
    }

    public static void setWarnMessageFromRes(String key) {
        setWarnMessage(getResourceBundleMessage(key));
    }

    public static String getResourceBundleMessage(String key, Object... params) {
        return getResourceBundleMessageByVar(key, null, params);
    }

    public static ResourceBundle getResourceBundleByVar(String name) {
        FacesContext context = FacesContext.getCurrentInstance();
        ResourceBundle bundle;
        if (context == null) {
            Locale local = (Locale) LanguageBean.getLocales().get(FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage());
            bundle = ResourceBundle.getBundle("com.viettel.resource.messages", local == null ? new Locale("vi", "VN") : local, new AppMessages.UTF8Control());
        } else {
            bundle = context.getApplication().getResourceBundle(context, name);
        }

        return bundle;
    }

    public static String getResourceBundleMessageByVar(String key, String var, Object... params) {
        String msg;
        if (var != null) {
            msg = getResourceBundleByVar(var).getString(key);
        } else {
            msg = getResourceBundleMessage(key);
        }
        try {
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    msg = msg.replace("{" + i + "}", params[i].toString());
                }
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage(), e);
            System.err.println(e.getMessage());
        }
        return msg;
    }

    public static String getResourceBundleConfig(String key) {
        if (key == null) {
            return key;
        }
        if ("".equals(key)) {
            return "";
        }
        try {
            ResourceBundle configBundle = ResourceBundle.getBundle("config");
            return configBundle.getString(key);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return key;
    }

    public static Locale getLocal() {
        return local;
    }

    public static void setLocal(Locale local) {
        MessageUtil.local = local;
    }
}
