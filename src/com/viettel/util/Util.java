/*
 * Created on Jun 7, 2013
 *
 * Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.primefaces.model.UploadedFile;

/**
 *
 * Class chua cac ham tien ich dung chung cua ca project
 *
 * @author Nguyen Hai Ha (hanh45@viettel.com.vn)
 * @since Jun 7, 2013
 * @version 1.0.0
 *
 */
public class Util {

    private static ExternalContext externalContext;
    private static File TOMCAT_DIR;
    private static File TEMP_DIR;
    private static File RESOURCES_DIR;
    protected static final Logger LOGGER = LoggerFactory.getLogger(Util.class);

    static {
        try {
            setExternalContext(FacesContext.getCurrentInstance().getExternalContext());
            setTOMCAT_DIR(new File(((ServletContext) getExternalContext().getContext())
                    .getRealPath("")).getParentFile().getParentFile());	//...../tomcat
            setTEMP_DIR(new File(getTOMCAT_DIR().getPath() + File.separator + "temp")); //...../tomcat/temp
            getTEMP_DIR().mkdirs();
            setRESOURCES_DIR(new File(getTOMCAT_DIR().getPath() + File.separator + "temp")); //...../tomcat/temp
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            setRESOURCES_DIR(new File(""));
        }
//        try {
//            RESOURCES_DIR = new File(((ServletContext) externalContext.getContext())
//                    .getRealPath("resources"));	//...../resources
//        } catch (Exception ex) {
//            RESOURCES_DIR = new File("");
//        }
    }

    /**
     * Lay gia tri ip cua client.
     */
    public static String getClientIp() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest req = (HttpServletRequest) context.getRequest();

        return req.getRemoteHost();
    }

    public static String getUploadFolder(String handleFolder) {
        String dir = RESOURCES_DIR + File.separator + Config.ROOT_FOLDER_DATA + File.separator + handleFolder;
        new File(dir).mkdirs();
        return dir;
    }

    public static boolean storeFile(String handleFoder, UploadedFile fileUpload) {
        File file = new File(getUploadFolder(handleFoder) + File.separator + fileUpload.getFileName());
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            out.write(fileUpload.getContents());
            return true;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return false;
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }
    }

    public static String normalizeParamCode(String paramCode) {
        if (paramCode != null) {
            return paramCode.replace("(", "").replace(")", "").replace("[", "").replace("]", "");
        }
        return null;
    }

    public static ExternalContext getExternalContext() {
        return externalContext;
    }

    public static void setExternalContext(ExternalContext externalContext) {
        Util.externalContext = externalContext;
    }

    public static File getTOMCAT_DIR() {
        return TOMCAT_DIR;
    }

    public static void setTOMCAT_DIR(File TOMCAT_DIR) {
        Util.TOMCAT_DIR = TOMCAT_DIR;
    }

    public static File getTEMP_DIR() {
        return TEMP_DIR;
    }

    public static void setTEMP_DIR(File TEMP_DIR) {
        Util.TEMP_DIR = TEMP_DIR;
    }

    public static File getRESOURCES_DIR() {
        return RESOURCES_DIR;
    }

    public static void setRESOURCES_DIR(File RESOURCES_DIR) {
        Util.RESOURCES_DIR = RESOURCES_DIR;
    }

}
