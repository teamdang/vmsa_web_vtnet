package com.viettel.util;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.vhr.service.EmployeeBean;
import com.viettel.vhr.service.ServiceResponse;
import com.viettel.vhr.service.VHRWebService_PortType;
import com.viettel.vhr.service.VHRWebService_ServiceLocator;
import com.viettel.vhr.service.VhrActor;
import com.viettel.vsa.token.UserToken;

public class VHRService {

    static VHRWebService_PortType vhrWebServicePort;
    static String ipLan = null;
    static String ipWan = null;
    static String password;
    static String userName;
    protected static final Logger LOGGER = LoggerFactory.getLogger(VHRService.class);
    static VhrActor actor;

    static {
        //VHR
        try {

            ResourceBundle bundle = ResourceBundle.getBundle("config");
            VHRWebService_ServiceLocator vhrWebService_ServiceLocator = new VHRWebService_ServiceLocator();
            vhrWebService_ServiceLocator.setVHRWebServicePortEndpointAddress(bundle.getString("VHR_WS"));
            vhrWebServicePort = vhrWebService_ServiceLocator.getVHRWebServicePort();

            try {
                ipLan = bundle.getString("ACTOR_IP_LAN");
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
            try {
                ipWan = bundle.getString("ACTOR_IP_WAN");
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
            password = bundle.getString("ACTOR_PASS");
            userName = bundle.getString("ACTOR_USER");
            actor = new VhrActor(ipLan, ipWan, password, userName);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public EmployeeBean getManageOfEmployee() {

        try {
            String employeeCode = "122215";//SessionUtil.getEmployeeCode();
            ServiceResponse employeeBean = vhrWebServicePort.getManageOfEmployee(actor, employeeCode, new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
            if (employeeBean.isIsSuccess()) {
                EmployeeBean employee = (EmployeeBean) employeeBean.getResponse()[0];
                employee = vhrWebServicePort.getListEmployees(actor, employee.getEmployeeCode(), null, null)[0];
                return employee;
            }
        } catch (RemoteException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public EmployeeBean[] getEmployeesOfDepartment(Long deptId) {
        try {
            return vhrWebServicePort.getListEmployees(actor, null, deptId, null);
        } catch (RemoteException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    public static void main(String[] args) {
        new VHRService().getManageOfEmployee();

    }
}
