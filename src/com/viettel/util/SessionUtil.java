/*
 * Created on Jun 7, 2013
 *
 * Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.util;

import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.viettel.vsa.token.ObjectToken;
import com.viettel.vsa.token.RoleToken;
import com.viettel.vsa.token.UserToken;

/**
 * Dinh nghia cac ham thao tac voi session cua ca he thong.
 *
 * @author Nguyen Hai Ha (hanh45@viettel.com.vn)
 * @since Jun 7, 2013
 * @version 1.0.0
 */
@ManagedBean
@RequestScoped
public class SessionUtil extends SessionWrapper {

    static String ACTION_ADMIN = "IP_MOP_ACTION_ADMIN";
    static String ACTION_TEMP = "IP_MOP_ACTION_TEMP";
    static String ACTION_MOP = "IP_MOP_ACTION_MOP";
    static String ACTION_COMMAND = "IP_MOP_ACTION_COMMAND";
    static String ACTION_EXECUTE = "IP_MOP_ACTION_EXECUTE";
    static String APPROVE_TEMP = "IP_MOP_APPROVE_TEMP";
    static String APPROVE_COMMAND = "IP_MOP_APPROVE_COMMAND";

    static String EXECUTE_MOP = "EXECUTE_MOP";
    static String APPROVE_MOP = "APPROVE_MOP";
    static String CREATE_MOP = "CREATE_MOP";
    static String APPROVE_TEMP_COMPONENT = "MS_APPROVE_TEMP";
    static String PRE_APPROVE_TEMP_COMPONENT = "MS_PRE_APPROVE_TEMP";
    static String RESCUE_MOP = "MS_RESCUE_MOP";

    static {
        ACTION_ADMIN = "MS_MOP_ACTION_ADMIN";
        ACTION_TEMP = "MS_MOP_ACTION_TEMP";
        ACTION_MOP = "MS_MOP_ACTION_MOP";
        ACTION_COMMAND = "MS_MOP_ACTION_COMMAND";
        ACTION_EXECUTE = "MS_MOP_ACTION_EXECUTE";
        APPROVE_TEMP = "MS_MOP_APPROVE_TEMP";
        APPROVE_COMMAND = "MS_MOP_APPROVE_COMMAND";

        EXECUTE_MOP = "MS_EXECUTE_MOP";
        APPROVE_MOP = "MS_APPROVE_MOP";
        CREATE_MOP = "MS_CREATE_MOP";
        RESCUE_MOP = "MS_RESCUE_MOP";
    }
    private static final long serialVersionUID = -7313741895804416337L;

    /**
     * Lay gia tri menu default.
     *
     * @return
     */
    public static String getMenuDefault() {
        HttpSession session = getCurrentSession();
        UserToken userToken = (UserToken) session.getAttribute("vsaUserToken");
        if (userToken != null && userToken.getRolesList() != null) {
            for (RoleToken ot : userToken.getRolesList()) {
                if (ot.getRoleCode().equalsIgnoreCase(ACTION_ADMIN)
                        || ot.getRoleCode().equalsIgnoreCase(ACTION_EXECUTE)
                        || ot.getRoleCode().equalsIgnoreCase(ACTION_MOP)
                        || ot.getRoleCode().equalsIgnoreCase(APPROVE_COMMAND)
                        || ot.getRoleCode().equalsIgnoreCase(APPROVE_TEMP)) {
                    return Config._DEFAULT_URL;
                } else if (ot.getRoleCode().equalsIgnoreCase(ACTION_TEMP)
                        || ot.getRoleCode().equalsIgnoreCase(ACTION_COMMAND)) {
                    return Config._DEFAULT_URL2;
                } else {
                    for (ObjectToken ot1 : userToken.getObjectTokens()) {
                        return ot1.getObjectUrl();
                    }
                }
            }
        }
        // 	Nguoi dung khong co url nao trong he thong
        // Tra ve trang bao loi.
        return Config._ERROR_PAGE;
    }
    Map<String, RoleToken> mapRoleCode = new HashMap<>();
    Map<String, ObjectToken> mapComponentCode = new HashMap<>();

    public SessionUtil() {
        mapRoleCode.clear();
        HttpSession session = getCurrentSession();
        UserToken userToken = (UserToken) session.getAttribute("vsaUserToken");
        if (userToken != null && userToken.getRolesList() != null) {
            for (RoleToken roleToken : userToken.getRolesList()) {
                mapRoleCode.put(roleToken.getRoleCode(), roleToken);
            }
        }
        mapComponentCode.clear();
        if (userToken != null && userToken.getComponentList() != null) {
            for (ObjectToken component : userToken.getComponentList()) {
                mapComponentCode.put(component.getObjectCode(), component);
            }
        }

    }

    public boolean checkRole(String roleCode) {
        if (mapRoleCode.get(roleCode) != null) {
            return true;
        }
        return false;
    }

    public boolean checkComponent(String componentCode) {
        if (mapComponentCode.get(componentCode) != null) {
            return true;
        }
        return false;
//        return true;
    }

    public boolean isActionAdmin() {
        return checkRole(ACTION_ADMIN);
    }

    public boolean isActionTemp() {
        return checkRole(ACTION_TEMP);
    }

    public boolean isActionMop() {
        return checkRole(ACTION_MOP) && !checkRole(ACTION_ADMIN);
    }

    public boolean isActionExecute() {
        return checkRole(ACTION_EXECUTE) && !checkRole(ACTION_MOP) && !checkRole(ACTION_ADMIN);
    }

    public boolean isOnlyViewCommand() {
        return checkRole(ACTION_MOP) && !checkRole(ACTION_ADMIN) && !checkRole(ACTION_COMMAND)
                && !checkRole(ACTION_EXECUTE) && !checkRole(ACTION_TEMP)
                && !checkRole(APPROVE_COMMAND) && !checkRole(APPROVE_TEMP);
    }

    public boolean isCreateMop() {
        return checkComponent(CREATE_MOP) || checkComponent(APPROVE_MOP);

    }

    public boolean isApproveMop() {
        return checkComponent(APPROVE_MOP);
    }
    public boolean isRescueMop() {
        return checkComponent(RESCUE_MOP);
    }

    public boolean isExecute() {
        return checkComponent(EXECUTE_MOP);
    }

    public boolean isApproveTemplate() {
        return checkComponent(APPROVE_TEMP_COMPONENT)
                || checkRole(APPROVE_TEMP) || checkRole(ACTION_ADMIN);
    }

    public boolean isPreApproveTemplate() {
        return checkComponent(PRE_APPROVE_TEMP_COMPONENT)
                || checkRole(APPROVE_TEMP) || checkRole(ACTION_ADMIN);
    }

    public boolean isShowNodeType() {
        try {
            return Boolean.parseBoolean(MessageUtil.getResourceBundleConfig("SHOW_NODE_TYPE"));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return true;
    }

    public boolean isCreateCustomer() {
        return isCreateMop() || checkRole("IP_MOP_CREATE_CUSTOMER");
    }

    public boolean isCreateEnodeB() {
        return isCreateMop() || checkRole("IP_MOP_CREATE_4G");
    }

    //20170510_HaNV15_Add_Start
    public boolean checkCountryCode(String countryCode) {
        try {

            if (Constants.VNM.equals(countryCode) || Constants.VTP.equals(countryCode)
                    || Constants.VTZ.equals(countryCode)) {
                return true;
            }
        } catch (Exception e) {
            logger.error("ERROR:", e);
            return false;
        }

        return false;
    }

    public String getCountryCodeCurrent() {
        try {
            HttpSession session = getCurrentSession();
            return (String) session.getAttribute(Constants.COUNTRY_CODE_CURRENT);
        } catch (Exception e) {
            logger.error("ERROR:", e);
            return null;
        }
    }
    //20170510_HaNV15_Add_End

    // HANHNV68 ADD 2017_08_03
    public boolean isRescue() {
        return checkComponent(RESCUE_MOP);

    }
    // END HANHNV68 ADD 2017_08_03
}// End class
