/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

/**
 * @author hienhv4
 */
public class Constants {

    //20170427_HaNV15_Add_Start
    public final static String DEFAULT_SALT = "default_$AL7";
    //20170427_HaNV15_Add_End
    //20170510_HaNV15_Add_Start: country code on session
    public final static String COUNTRY_CODE_CURRENT = "countryCodeCurrent";
    public final static String VNM = "VNM";
    public final static String VTP = "VTP";
    public final static String VTZ = "VTZ";
    //20170510_HaNV15_Add_End

    public static final Long CMD_TIMEOUT_DEFAULT = 30000l; //Thoi gian timeout lenh default (30s)

    public static final String SPLITTER_PARAM = ";"; // Ky tu phan tach khi nhap nhieu gia tri cho tham so

    public static final int SAVE_FLAG = 0;
    public static final int WAITTING_FLAG = 1;
    public static final int RUNNING_FLAG = 2;
    public static final int FINISH_FLAG = 3;
    public static final int FAIL_FLAG = 4;

    public static final String LOGIN_PROMPT = "(>|#)";

    public static final String LOGIN_FAIL = "LOGIN_FAIL";
    public static final String CONN_TIMEOUT = "CONN_TIMEOUT";
    public static final int NCMS_REQUEST_TIMEOUT = 50;

    public class vendorType {

        public static final String HUAWEI = "HUAWEI";
        public static final String NOKIA = "NOKIA";
        public static final String TEKELEC = "TEKELEC";
        public static final String ERICSSON = "ERICSSON";
        public static final String ZTE = "ZTE";

    }


    public class fileNameTemplate {
        public class Integrate {
            public static final String Ericsson_3G = "Ericsson_3G_Integrate.xlsx";
            public static final String Ericsson_2G = "Ericsson_2G_Integrate.xlsx";
            public static final String Nokia_3G = "Nokia_3G_Integrate.xlsx";
            public static final String Nokia_2G = "Nokia_2G_Integrate.xlsx";
            public static final String Huawei_3G = "Huawei_2G_Integrate.xlsx";
            public static final String Huawei_2G = "Huawei_2G_Integrate.xlsx";
        }

        public class Delete {
            public static final String Ericsson_3G = "Ericsson_3G_Delete.xlsx";
            public static final String Ericsson_2G = "Ericsson_2G_Delete.xlsx";
            public static final String Nokia_3G = "Nokia_3G_Delete.xlsx";
            public static final String Nokia_2G = "Nokia_2G_Delete.xlsx";
            public static final String Huawei_3G = "Huawei_2G_Delete.xlsx";
            public static final String Huawei_2G = "Huawei_2G_Delete.xlsx";
        }

        public class Relation {
            public static final String Ericsson_3G = "Add_Remove_Relation_2G_3G_vendor_Ericsson.xlsx";
            public static final String Ericsson_2G = "Add_Remove_Relation_2G_3G_vendor_Ericsson.xlsx";
            public static final String Nokia_3G = "Add_Remove_Relation_2G_3G_vendor_Nokia.xlsx";
            public static final String Nokia_2G = "Add_Remove_Relation_2G_3G_vendor_Nokia.xlsx";
            public static final String Huawei_3G = "Add_Remove_Relation_2G_3G_vendor_Huawei.xlsx";
            public static final String Huawei_2G = "Add_Remove_Relation_2G_3G_vendor_Huawei.xlsx";
        }

        public class SD_HR {
            public static final String Ericsson_3G = "Change_HR_SD_2G_vendor_Ericsson.xlsx";
            public static final String Ericsson_2G = "Change_HR_SD_2G_vendor_Ericsson.xlsx";
            public static final String Nokia_3G = "Change_HR_SD_2G_vendor_Nokia.xlsx";
            public static final String Nokia_2G = "Change_HR_SD_2G_vendor_Nokia.xlsx";
            public static final String Huawei_3G = "Change_HR_SD_2G_vendor_Huawei.xlsx";
            public static final String Huawei_2G = "Change_HR_SD_2G_vendor_Huawei.xlsx";
        }

        public class LoadLicense {
            public static final String Ericsson_3G = "Load_License_3G_vendor_Ericsson.xlsx";
            public static final String Ericsson_4G = "Load_License_4G_vendor_Ericsson.xlsx";
        }

        public class ChangeParam {
            public static final String Ericsson_3G = "Change_Param_3G_vendor_Ericsson.xlsx";
            public static final String Ericsson_2G = "Change_Param_2G_vendor_Ericsson.xlsx";
            public static final String Nokia_3G = "Change_Param_3G_vendor_Nokia.xlsx";
            public static final String Nokia_2G = "Change_Param_2G_vendor_Nokia.xlsx";
            public static final String Huawei_3G = "Change_Param_3G_vendor_Huawei.xlsx";
            public static final String Huawei_2G = "Change_Param_2G_vendor_Huawei.xlsx";
        }

        public class ActiveStandard {
            public static final String Ericsson_3G = "Active_Standard_Cell_3G_vendor_Ericsson.xlsx";
            public static final String Ericsson_2G = "Active_Standard_Cell_2G_vendor_Ericsson.xlsx";
            public static final String Nokia_3G = "Active_Standard_Cell_3G_vendor_Nokia.xlsx";
            public static final String Nokia_2G = "Active_Standard_Cell_2G_vendor_Nokia.xlsx";
            public static final String Huawei_3G = "Active_Standard_Cell_3G_vendor_Huawei.xlsx";
            public static final String Huawei_2G = "Active_Standard_Cell_2G_vendor_Huawei.xlsx";
        }

        public class ActiveTest {
            public static final String Ericsson_3G = "Active_Test_Cell_3G_vendor_Ericsson.xlsx";
            public static final String Ericsson_2G = "Active_Test_Cell_2G_vendor_Ericsson.xlsx";
            public static final String Nokia_3G = "Active_Test_Cell_3G_vendor_Nokia.xlsx";
            public static final String Nokia_2G = "Active_Test_Cell_2G_vendor_Nokia.xlsx";
            public static final String Huawei_3G = "Active_Test_Cell_3G_vendor_Huawei.xlsx";
            public static final String Huawei_2G = "Active_Test_Cell_2G_vendor_Huawei.xlsx";
        }
        public class ChangeVLAN {
            public static final String Ericsson_3G = "Change_VLAN_3G_vendor_Ericsson.xlsx";
            public static final String Ericsson_2G = "Change_VLAN_2G_vendor_Ericsson.xlsx";
            public static final String Nokia_3G = "Change_VLAN_3G_vendor_Nokia.xlsx";
            public static final String Nokia_2G = "Change_VLAN_2G_vendor_Nokia.xlsx";
            public static final String Huawei_3G = "Change_VLAN_3G_vendor_Huawei.xlsx";
            public static final String Huawei_2G = "Change_VLAN_2G_vendor_Huawei.xlsx";
        }

    }

    public class versionType {

        public static final String IPA2600 = "IPA2600";
        public static final String mcRNC = "MCRNC";
        public static final String HUAWEI_6900 = "RNC6900";
        public static final String HUAWEI_6910 = "RNC6910";
    }

    public class sdZoneType {

        public static final String HOTSPOT = "Hotspot";
        public static final String NORMAL = "Normal";
    }

    public class nodeTypeName {

        public static final String RNC = "RNC";
        public static final String SGSN = "SGSN";
    }

    public class protocol {

        public static final String SSH = "SSH";
        public static final String TELNET = "TELNET";
        public static final String SQL = "SQL";
    }

    public class ProcessPhase {

        public static final int CHANGE_ROLLBACK = 0;
        public static final int CHANGE_CHECK_RESULT = 1;
    }

    public class ResultType {

        public static final int PLAN_NOT_FOUND = 5;
        public static final int CREATE_DT_ERROR = 6;
        public static final int SUCCESS = 0;
    }

    public class ChangeDTType {
        public static final String CHANGE_HR_SD = "CHANGE_HR_SD";
        public static final String CHANGE_HR_DAILY = "HR";
        public static final String CHANGE_HR_WEEKLY = "HR_WEEKLY";
        public static final String CHANGE_SD_DAILY = "SD";
        public static final String CHANGE_SD_WEEKLY = "SD_WEEKLY";
        public static final String CHANGE_HR_FORCE = "CHANGE_HR_FORCE";
        public static final String CHANGE_SD_FORCE = "CHANGE_SD_FORCE";
    }

    public class EventType {

        public static final int CHANGE_PARAM = 0;
        public static final int CHANGE_HR_DAILY = 1;
        public static final int CHANGE_HR_WEEKLY = 2;
        public static final int CHANGE_SD_DAILY = 3;
        public static final int CHANGE_SD_WEEKLY = 4;
        public static final int CHANGE_FORCE_HR_SD = 5;
    }

    public class ParamType {

        public static final long RNC = 0;
        public static final long NODEB = 1;
        public static final long CELL = 2;
        public static final long NODEBInRnc = 3;
    }

    public class ParamCodeNodeBInRnc {

        public static final String dlHwAdm = "dlHwAdm";
        public static final String ulHwAdm = "ulHwAdm";
    }

    public class StationUpDowngradeGroupActionName {

        public static final String Executing_Upgrade = "Excutive Upgrade";
        public static final String Executing_Downgrade = "Excutive Downgrade";
        public static final String CheckAfterExecuting = "Check After Executive";
    }

    public class CommandSgsnEricson {

        public static final String command_get_ne = "gsh get_ne";
        public static final String command_list_capacity = "gsh list_capacity -par '*'";
        public static final String list_sctp_end_point = "gsh list_sctp_end_point all";
        public static final String list_ip_service_address = "gsh list_ip_service_address";
        public static final String list_rnc = "gsh list_rnc all";
        public static final String list_ss7_m3ua_destination_spc = "gsh list_ss7_m3ua_destination_spc all";
        public static final String list_rnc_ra = "gsh list_rnc_ra all";
    }

    public class CommandSgsnHeaderEricson {

        public static final String gt = "isdn (IsdnNumber)";
        public static final String currentSau = "sauw (NbrActAttachedSubW)";
        public static final String currentPdp = "pdpw (NbrActPdpContextW)";
        public static final String licenseSau = "A  capacity   -name  attach_limit";
        public static final String licensePDP = "A  capacity   -name  context_limit";
        public static final String licenseThroughput = "A  capacity   -name  payload_limit";
        public static final String SGSNSPC = "A  ss7_local_signaling_point   -net  RAN  -nid";
        public static final String RANnet = "A  ss7_sccp_local_sap   -net  RAN  -nid";
        public static final String SGSNSctpProfile1 = "A  sctp_end_point   -no";
        public static final String SGSNSctpProfile2 = "Iu-SS7";
        public static final String SGSNIuSS7 = "A  ip_service_address   -sn  Iu-SS7";
        public static final String rnc = "A  rnc   -rn";
        public static final String rnc_active = "in-service";
        public static final String RNCLasid = "A  ss7_m3ua_destination_spc";
        public static final String RNCRasid = "A  ss7_m3ua_local_as   -lasid";
        public static final String racLac = "A  rnc_ra";

    }

    public class paramCodeSgsn {

        public static final String gt = "GT";
        public static final String currentSau = "CurrentSAU";
        public static final String currentPdp = "CurrentPDP";
        public static final String licenseSau = "LicenseSAU";
        public static final String licensePDP = "LicensePDP";
        public static final String licenseThroughput = "LicenseThroughput";
        public static final String SGSNSPC = "SGSNSPC";
        public static final String RANnet = "RANnet";
        public static final String RANnid = "RANnid";
        public static final String RANssn = "RANssn";
        public static final String SGSNSctpProfile = "@SGSNSctpProfile";
        public static final String RNCName = "RNCName";
        public static final String RNCID = "RNCID";
        public static final String RNCgrc = "RNCgrc";
        public static final String RNCgrn = "RNCgrn";
        public static final String RNCgcc = "RNCgcc";
        public static final String RNCgcn = "RNCgcn";
        public static final String RNCSPC = "RNCSPC";
        public static final String RNCSPCh = "RNCSPCh";
        public static final String SGSNSPCh = "SGSNSPCh";
    }

    public class CommandRncNokia {

        public static final String DestinationEntity = "ZNSI:NA1:P:;";
        public static final String DestinationEntityIndentify = "ZOYI:::;";
        public static final String SignallingLinkID1 = "ZOYI:NAME=";
        public static final String SignallingLinkID2 = ":A:;";
        public static final String getwayIp1 = "ZQKB::";
        public static final String getwayIp2 = "::;";
        public static final String listLinkId = "ZNCI:;";

    }

    public class CommandRncHeaderNokia {

        public static final String DestinationEntity = "IUPS";
        public static final String DestinationEntityStatus = "AV";
        public static final String SignallingLinkID = "ASSOC.         ASSOC ID  PARAMETER SET";
        public static final String assoc = "IETF             ASP-ACTIVE";
        public static final String RncIp1 = "SOURCE ADDRESS 1";
        public static final String RncIp2 = "SOURCE ADDRESS 2";
        public static final String RncPort = "SOURCE PORT";
        public static final String sgsnIp1 = "PRIMARY DEST. ADDRESS";
        public static final String sgsnIp2 = "SECONDARY DEST. ADDRESS";
        public static final String sgsnPort = "DESTINATION PORT";
        public static final String ICSU = "ICSU";
        public static final String listLinkId = "----";
        public static final String listLinkIdEnd = "COMMAND EXECUTED";

    }

    public class paramCodeRnc {

        public static final String RNCSAU = "RNCSAU";
        public static final String RNCPDP = "RNCPDP";
        public static final String RNCAssocIndex = "RNCAssocIndex";
    }

    public class paramCodeMsc {

        public static final String MSCSAU = "MSCSAU";
        public static final String currentPdp = "CurrentPDP";
        public static final String MSCSAULicense = "MSCSAULicense";
        public static final String licensePDP = "LicensePDP";
        public static final String licenseThroughput = "LicenseThroughput";
        public static final String SGSNSPC = "SGSNSPC";
        public static final String RANnet = "RANnet";
        public static final String RANnid = "RANnid";
        public static final String RANssn = "RANssn";
        public static final String SGSNSctpProfile = "@SGSNSctpProfile";
        public static final String RNCName = "RNCName";
        public static final String RNCID = "RNCID";
        public static final String RNCgrc = "RNCgrc";
        public static final String RNCgrn = "RNCgrn";
        public static final String RNCgcc = "RNCgcc";
        public static final String RNCgcn = "RNCgcn";
        public static final String RNCSPC = "RNCSPC";
        public static final String NRNCID = "NRNCID";
        public static final String NMSCGT = "NMSCGT";
        public static final String NRNCID_NMSCGT = "NRNCID_NMSCGT";
    }

    public class keyValueColumn {

        public static final  primsn = "primsn";
        public static final StStringring secsn = "secsn";
        public static final String portno = "portno";
    }

    public class paramCodeCommon {

        public static final double paramCrossCheck = 85D;
    }

    public class paramCode {

        //Khai tham so cho RNC Nokia
        public static final String SgsnSctpProfile = "SgsnSctpProfile";
        public static final String RNCPort = "RNCPort";
        public static final String RNCIuIP1 = "RNCIuIP1";
        public static final String RNCIuIP2 = "RNCIuIP2";
        public static final String AssocciationSetName = "AssocciationSetName";
        public static final String icsu = "icsu";
        public static final String SGSNIuIP1 = "SGSNIuIP1";
        public static final String SGSNIuIP2 = "SGSNIuIP2";
        public static final String SGSNPort = "SGSNPort";
        public static final String SignallingLink = "SignallingLink";
        public static final String SgsnIuIP = "SgsnIuIP";
        public static final String GatewayCPNPGEP = "GatewayCPNPGEP";
        public static final String NPGEPIndex = "NPGEPIndex";
        public static final String UPNPGEPIndex = "UPNPGEPIndex";
        public static final String RNCLac = "RNCLac";
        public static final String RNCRac = "RNCRac";
        public static final String RNCLac1 = "RNCLac1";
        public static final String RNCRac1 = "RNCRac1";
        public static final String CPNPGEP1 = "CPNPGEP1";
        public static final String CPNPGEP2 = "CPNPGEP2";
        public static final String AssocID = "AssocID";
        public static final String SGSNName = "SGSNName";
        public static final String RNCName = "RNCName";
        public static final String RNCNameNokia = "RNCNameN";
        public static final String VRFName = "VRFName";
        //Khai tham so cho RNC Ericsson
        public static final String sctpid = "sctpid";
        public static final String IpAccessHostPool = "IpAccessHostPool";
        public static final String SccpApLocal = "SccpApLocal";
        public static final String RNCLasid = "RNCLasid";
        public static final String RncSPC = "RncSPC";
        public static final String RNCLspid = "RNCLspid";
        public static final String RNCRspid = "RNCRspid";
        public static final String RNCRasid = "RNCRasid";
        public static final String SignallingLinkName = "SignallingLinkName";
        public static final String index = "index";
        public static final String number = "number";
        public static final String dscp = "dscp";

        public static final String RemoteAsID = "RemoteAsID";
        public static final String RemoteAsName = "RemoteAsName";
        public static final String local_as_name = "local-as-name";
        public static final String local_client_port = "local-client-port";
        public static final String local_server_port = "local-server-port";
        public static final String NodeAssoc = "node";
        public static final String PointCodeId = "PointCodeId";
        public static final String PointCodeName = "PointCodeName";
        public static final String SubSystemID = "SubSystemID";
        public static final String SubSystemName = "SubSystemName";
        public static final String ConcernSSNName = "ConcernSSNName";
        public static final String AffectSSNName = "AffectSSNName";
        public static final String ConcernPointCodeName = "ConcernPointCodeName";
        public static final String gateway1 = "gateway1";
        public static final String gateway2 = "gateway2";
        public static final String ipbr_id = "ipbr-id";
        public static final String ipbr_name = "ipbr-name";

        //Khai tham so cho RNC Huawei
        public static final String RNCIP1 = "RNCIuIP1";
        public static final String RNCIP2 = "RNCIuIP2";
        public static final String Subrack = "Subrack";
        public static final String Slot = "Slot";
        public static final String Sctplink = "Sctplink";
        public static final String SignallingLinkSetID = "SignallingLinkSetID";
        public static final String SignallingLinkID = "SignallingLinkID";
        public static final String DSPIndex = "DSPIndex";
        public static final String CNOperatorID = "CNOperatorID";
        public static final String CNNodeID = "CNNodeID";
        public static final String DestinationEntityNo = "DestinationEntityNo";
        public static final String AdjacentNodeID = "AdjacentNodeID";
        public static final String pathid = "pathid";
        public static final String RNCUserIP = "RNCUserIP";
        public static final String GOUSubrack = "GOUSubrack";
        public static final String GOUSlot = "GOUSlot";
        public static final String GOUSubrackDetail = "GOUSubrackDetail";
        public static final String GOUSlotDetail = "GOUSlotDetail";
        public static final String NexthopIP = "NexthopIP";
        public static final String PRIORITY = "PRIORITY";

        //Khai tham so cho Msc Huawei
        public static final String MSCIP1 = "MSCIP1";
        public static final String MSCIP2 = "MSCIP2";
        public static final String MSCIP = "MSCIP";
        public static final String MSCPort = "MSCPort";
        public static final String RNCBillOFN = "RNCBillOFN";
        public static final String RNCLAI = "RNCLAI";
        public static final String RNCSAI = "RNCSAI";
        public static final String RNCLAC = "RNCLAC";
        public static final String RNCProvinceLai = "RNCProvinceLai";
        public static final String RNCProvinceSai = "RNCProvinceSai";
        public static final String RNCSAIName = "RNCSAIName";
        public static final String MSCPORTALL = "MSCPORTALL";

        public static final String RNCSCCP = "RNCSCCP";
        public static final String RNCSSN = "RNCSSN";
        public static final String RNCDSP = "RNCDSP";
        public static final String RNCOFC = "RNCOFC";
        public static final String RNCLinkName = "RNCLinkName";
        public static final String RNCRT = "RNCRT";
        public static final String RNCLinkSet = "RNCLinkSet";
        public static final String RNCDE = "RNCDE";
        public static final String MSCName = "MSCName";
        public static final String LocationName = "LocationName";
        public static final String RNCParameterSetName = "RNCParameterSetName";
        public static final String AssocLinkIndex = "AssocLinkIndex";
        public static final String PGI = "pgi";
        public static final String RNCPAPU = "RNCPAPU";
        public static final String SGSNSPC = "SGSNSPC";
        public static final String SgsnUserIP = "SgsnUserIP";
        public static final String RNCDEX = "RNCDEX";
        public static final String RNCLinkSetIndex = "RNCLinkSetIndex";
        public static final String RNCRouteIndex = "RNCRouteIndex";
        public static final String RNCDPCIndex = "RNCDPCIndex";
        public static final String RNCSCMGID = "RNCSCMGID";
        public static final String RNCRnapID = "RNCRnapID";
        public static final String RNCIndex = "RNCIndex";
        public static final String ECUSubrack = "ECUSubrack";
        public static final String ECUSlot = "ECUSlot";
    }
}
