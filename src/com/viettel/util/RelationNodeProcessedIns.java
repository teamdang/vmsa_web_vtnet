package com.viettel.util;
import java.util.concurrent.ConcurrentHashMap;

import com.viettel.model.Node;
import com.viettel.model.RelationNode;

public class RelationNodeProcessedIns {

	private static RelationNodeProcessedIns instance;
	private ConcurrentHashMap<Long, RelationNode> mapNodeRelationProcessed = new ConcurrentHashMap<Long, RelationNode>();
	private ConcurrentHashMap<String, Node> mapNodeProcessed = new ConcurrentHashMap<String, Node>();
	
	public synchronized static RelationNodeProcessedIns getInstance() {
		if (instance == null) {
			instance = new RelationNodeProcessedIns();
		}
		return instance;
	}

	public ConcurrentHashMap<Long, RelationNode> getMapNodeRelationProcessed() {
		return mapNodeRelationProcessed;
	}

	public void setMapNodeRelationProcessed(
			ConcurrentHashMap<Long, RelationNode> mapNodeRelationProcessed) {
		this.mapNodeRelationProcessed = mapNodeRelationProcessed;
	}

	public ConcurrentHashMap<String, Node> getMapNodeProcessed() {
		return mapNodeProcessed;
	}

	public void setMapNodeProcessed(ConcurrentHashMap<String, Node> mapNodeProcessed) {
		this.mapNodeProcessed = mapNodeProcessed;
	}
	
}
