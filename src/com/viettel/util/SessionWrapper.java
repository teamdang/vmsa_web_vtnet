/*
 * Created on Jun 7, 2013
 *
 * Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.viettel.model.ManageProcess;
import com.viettel.model.ManageUserAdmin;
import com.viettel.persistence.GenericDaoImplNewV2;
import com.viettel.persistence.GenericDaoServiceNewV2;
import com.viettel.vsa.token.ObjectToken;
import com.viettel.vsa.token.UserToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Các hàm thao tác với session cơ bản của cả hệ thống.
 *
 * @author Nguyen Hai Ha (hanh45@viettel.com.vn)
 * @version 1.0.0
 * @since Jun 7, 2013
 */
public class SessionWrapper implements Serializable {

    private static final long serialVersionUID = -8318262775763386620L;
    public static final String _VSA_USER_TOKEN = "vsaUserToken";
    protected static final Logger logger = LoggerFactory.getLogger(SessionWrapper.class);
    private static final String _VSA_USER_ID = "netID";
    private GenericDaoServiceNewV2<ManageUserAdmin, Long> manageUserAdminService = new GenericDaoImplNewV2<ManageUserAdmin, Long>() {
    };

    /**
     * Get current session cua he thong.
     *
     * @return current session
     */
    public static HttpSession getCurrentSession() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        return request.getSession();
    }

    /**
     * Lay gia tri session attribute.
     *
     * @param attributeName
     * @return
     */
    public String getSessionAttribute(String attributeName) {
        return (String) getCurrentSession().getAttribute(attributeName);
    }

    /**
     * Lay thong tin cua user hien tai dang login.
     *
     * @return
     */
    public static String getCurrentUsername() {
        try {
            return ((UserToken) getCurrentSession().getAttribute(_VSA_USER_TOKEN)).getUserName();
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
            return "system";
        }
    }

    public boolean checkUserAdmin() throws Exception {
        String user = getCurrentUsername();
        if (user == null || "".equals(user)) {
            return false;
        }

        List<String> listUser = new ArrayList<>();
        List<ManageUserAdmin> manageUserAdmins = manageUserAdminService.findList(null);
        if(manageUserAdmins != null){
            for(ManageUserAdmin manageUserAdmin : manageUserAdmins){
                listUser.add(manageUserAdmin.getUserAdmin().toLowerCase());
            }
        }
//        listUser.add("quytv7");
//        listUser.add("hienhv4");
//        listUser.add("hieunt9");
//        listUser.add("huynx6");
//        listUser.add("phuongdtt2");
//        listUser.add("hanhnv68");
        if (listUser.contains(user.toLowerCase())) {
            return true;
        }
        return false;

    }

    /**
     * Kiem tra xem URL nay co duoc truy cap khong.
     *
     * @param urlCode
     * @return
     */
    public boolean getUrlDisplay(String urlCode) {
        HttpSession session = getCurrentSession();
//		boolean result = false;
        boolean result = true;
        String objToken;
        UserToken userToken = (UserToken) session.getAttribute(_VSA_USER_TOKEN);
        if (userToken != null) {
            for (ObjectToken ot : userToken.getObjectTokens()) {
                objToken = ot.getObjectUrl();
                if (objToken.equalsIgnoreCase(urlCode)) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    protected static Boolean getUrlByKey(String key) {
        HttpSession session = getCurrentSession();
        String url;
        UserToken userToken = (UserToken) session.getAttribute(_VSA_USER_TOKEN);
        if (userToken != null) {
            for (ObjectToken ot : userToken.getObjectTokens()) {
                url = ot.getObjectUrl();
                if (key.equalsIgnoreCase(url)) {
                    return true;
                }
            }
        } else {
            return false;
        }

        return false;
    }

    public static String getEmployeeCode() {
        UserToken vsaValidate = ((UserToken) SessionUtil.getCurrentSession().getAttribute(SessionUtil._VSA_USER_TOKEN));
        String employeeCode = vsaValidate.getStaffCode();
        return employeeCode;
    }
}// End class
