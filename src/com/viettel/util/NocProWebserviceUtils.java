package com.viettel.util;

import com.viettel.nms.nocpro.service.NocproWebservice;
import com.viettel.nms.nocpro.service.NocproWebservice_Service;
import org.apache.log4j.Logger;

/**
 * Created by hanhnv68 on 8/9/2017.
 */
public class NocProWebserviceUtils {

    private static final Logger logger = Logger.getLogger(WOService.class);
    private static NocproWebservice service;

    static {
        try {
            service = new NocproWebservice_Service().getNocproWebservicePort();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public static NocproWebservice getService() {
        return service;
    }

    public static void setService(NocproWebservice service) {
        NocProWebserviceUtils.service = service;
    }
    
}
