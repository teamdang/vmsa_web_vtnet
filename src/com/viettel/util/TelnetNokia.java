/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

/**
 *
 * @author hienhv4
 */
public class TelnetNokia extends TelnetClientUtil {

    public TelnetNokia(String hostName, Integer port, String vendor) {
        super(hostName, port, vendor);
        cmdExit = "logout";
    }

    @Override
    public String connect(String username, String password) throws Exception {
        setShellPromt("\n<");
        String login = connect("<", username, "<", password, false);
        if (getCurrentOutStr().contains("USER AUTHORIZATION FAILURE")) {
            throw new Exception("LOGIN_FAIL");
        }
        setShellPromt("\n<");
        return login;
    }

    @Override
    public void write(String value) throws Exception {
        try {
            out.write((value + "\r\n").getBytes());
            out.flush();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void writeMore(String value) throws Exception {
        try {
            out.write(value.getBytes());
            out.flush();
        } catch (Exception e) {
            throw e;
        }
    }

    /*@Override
    public String readUntilBelongRegex(String cmd, String pattern, int timeOut) throws Exception {
        log.info("---Quytv7 readUntilBelongRegex Nokia");
        StringBuilder sb = new StringBuilder();
        System.out.println("pattern: " + pattern);
        String patternCtrF = "(\\/\\*\\*\\*(.*\\r\\n.*)+[=,:])";
        try {
            if (checkInput()) {
                long startTime = System.currentTimeMillis();
                char[] chs = new char[defaultCharBufferSize];
                int t = buf.read(chs);

                while (t != -1) {
                    if (isDebug) {
                        System.out.print(chs);
                    }
                    sb.append(chs, 0, t);
                    if (checkRegex(sb.toString(), pattern)) {
                        return sb.toString();
                    }
                    //Quytv7 xet truong hop cho vendor nokia, khi lenh loi gui them lenh ctr + F
                    if (checkRegex(sb.toString(), patternCtrF)) {
                        log.info("---Quytv7 check vao truong hop lenh fail can gui cmd Ctr X");
                        sendNoWait("\u0018");
                        log.info("---Quytv7 Da gui Ctr X");
                    }
                    log.info("---Quytv7 t:" + t);
                    t = buf.read(chs);

                    if (System.currentTimeMillis() - startTime > timeOut) {
                        log.info("---Quytv7 timeout gui lenh Ctr C");
                        sendNoWait("\u0003");
                        throw new Exception("Time out for waitting promt: " + pattern);
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.currentOutStr = sb.toString();
        }

        return sb.toString().replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
    }*/
    @Override
    public String readUntilBelongRegex(String cmd, String pattern, int timeOut) throws Exception {
        StringBuilder sb = new StringBuilder();
//        log.info("pattern: " + pattern + ", timeOut: " + timeOut);
        String patternCtrF = "(\\/\\*\\*\\*(.*\\r\\n.*)+[=,:])";
        try {
            if (checkInput()) {
                long startTime = System.currentTimeMillis();
                char[] chs = new char[defaultCharBufferSize];
                int t = buf.read(chs);
//                log.info("---t: " + t);
                while (t != -1) {
                    if (isDebug) {
                        System.out.print(chs);
                    }
                    sb.append(chs, 0, t);
                    if (checkRegex(sb.toString(), pattern)) {
                        if (cmd != null) {
                            String content = sb.toString().replace(" [1D", "");

                            if (content.equals(cmd)) {
                                sb = new StringBuilder();
                            } else {
                                return sb.toString();
                            }
                        } else {
                            return sb.toString();
                        }
                    }
                    //Quytv7 xet truong hop cho vendor nokia, khi lenh loi gui them lenh ctr + F
                    if (checkRegex(sb.toString(), patternCtrF)) {
                        log.info("---Quytv7 check vao truong hop lenh fail can gui cmd Ctr X");
                        sendNoWait("\u0018");
                        log.info("---Quytv7 Da gui Ctr X");
                    }


                    t = buf.read(chs);

                    if (System.currentTimeMillis() - startTime > timeOut) {
                        System.out.println("noi dung lay duoc: " + sb.toString());
                        sendNoWait("\u0003");
                        throw new Exception("Time out for waitting promt: " + pattern);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("hehe loi roi: " + sb.toString());
            throw e;
        } finally {
            this.currentOutStr = sb.toString();
        }

        return sb.toString();//.replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
    }
}
