package com.viettel.util;

//import com.sun.xml.internal.ws.client.BindingProviderProperties;

import com.ctc.wstx.util.StringUtil;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.sun.tools.ws.wsdl.document.Message;
import com.viettel.model.ParamInput;
import com.viettel.model.ParamValue;
import com.viettel.ncms.webservice.*;
import com.viettel.gnoc.cr.service.CrForOtherSystemServiceImplServiceLocator;
import com.viettel.gnoc.cr.service.CrOutputForQLTNDTO;
import com.viettel.gnoc.cr.service.ResultDTO;
import com.viettel.passprotector.PassProtector;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.*;
import org.kohsuke.rngom.digested.DDataPattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.rpc.ServiceException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
//import javax.xml.ws.BindingProvider;

public class NCMSService {

    protected final static Logger LOGGER = LoggerFactory.getLogger(NCMSService.class);
    private static NcmsWebService_Service service;
    private static URL url;
    static ResourceBundle bundle = ResourceBundle.getBundle("config");
    static final int TIME_OUT = 30000;

    static {

        try {
            url = new URL(bundle.getString("ws_ncms_new"));
            service = new NcmsWebService_Service(url);
            setService(new NcmsWebService_Service(url));
        } catch (MalformedURLException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }


    public static HashMap<String, ParamValue> loadParamFromNcms(String nodeCode, List<ParamInput> paramInputs, HashMap<String, ParamValue> mapParamValueDistinct, String commandName) {
        try {
            //Phan tich du lieu dau vao
            HashMap<String, ParamValue> mapParamValueFinal = new HashMap<>();
            Multimap<String, ParamInput> mapConfigCodeParamInput = ArrayListMultimap.create();
            for (ParamInput paramInput : paramInputs) {
                mapConfigCodeParamInput.put(paramInput.getConfigCode(), paramInput);
            }
            List<HashMap<String, ParamValue>> listMapParmCondition;
            listMapParmCondition = getListMapParamCondition(paramInputs, mapParamValueDistinct);
            HashMap<CoreMobileParamInput, CoreMobileParamOutput> mapResult = new HashMap<>();
            String paramSucess = "";
            String paramFail = "";
            for (String configCode : mapConfigCodeParamInput.keySet()) {
                for (HashMap<String, ParamValue> mapParamDistinct : listMapParmCondition) {
                    CoreMobileParamCondition coreMobileParamCondition = new CoreMobileParamCondition();
                    coreMobileParamCondition.setConfigCode(configCode);
                    coreMobileParamCondition.setNodeCode(nodeCode);
                    List<String> listParamInConfig = coreMobileParamCondition.getListParams();
                    List<CoreMobileParamForm> coreMobileParamForms = coreMobileParamCondition.getListConditions();
                    List<ParamInput> paramInputsInConfig = new ArrayList<>(mapConfigCodeParamInput.get(configCode));
                    for (ParamInput paramInput : paramInputsInConfig) {
                        if (paramInput.getIsParamKey()) {
                            CoreMobileParamForm coreMobileParamForm = new CoreMobileParamForm();
                            coreMobileParamForm.setParamCode(paramInput.getParamCode());
                            if (mapParamDistinct.containsKey(paramInput.getParamCode())) {
                                coreMobileParamForm.setParamValue(mapParamDistinct.get(paramInput.getParamCode()).getParamValue());
                            }

                            coreMobileParamForms.add(coreMobileParamForm);
                        } else {
                            listParamInConfig.add(paramInput.getParamCode());
                        }
                    }
                    CoreMobileParamInput coreMobileParamInput = new CoreMobileParamInput();
                    coreMobileParamInput.setUserName(bundle.getString("ws_ncms_new_user"));
                    coreMobileParamInput.setPassword(bundle.getString("ws_ncms_new_pass"));
                    coreMobileParamInput.setCondition(coreMobileParamCondition);
                    CoreMobileParamOutput coreMobileParamOutput;
                    if (mapResult.containsKey(coreMobileParamInput)) {
                        coreMobileParamOutput = mapResult.get(coreMobileParamInput);
                    } else {
                        coreMobileParamOutput = service.getNcmsWebServicePort().getCoreMobileParamValue(coreMobileParamInput);
                    }

                    if ("OK".equalsIgnoreCase(coreMobileParamOutput.getResult()) && coreMobileParamOutput.getListData().size() > 0) {

                        LinkedHashMap<Long, HashMap<String, CoreMobileParamForm>> mapResultParamFormGroupId = new LinkedHashMap<>();
                        sortResult(coreMobileParamOutput.getListData());
                        for (CoreMobileParamForm coreMobileParamForm : coreMobileParamOutput.getListData()) {
                            if (mapResultParamFormGroupId.containsKey(coreMobileParamForm.getGroupId())) {
                                mapResultParamFormGroupId.get(coreMobileParamForm.getGroupId()).put(coreMobileParamForm.getParamCode(), coreMobileParamForm);

                            } else {
                                HashMap<String, CoreMobileParamForm> maptemp = new HashMap<>();
                                maptemp.put(coreMobileParamForm.getParamCode(), coreMobileParamForm);
                                mapResultParamFormGroupId.put(coreMobileParamForm.getGroupId(), maptemp);
                            }
                        }

                        for (Long key : mapResultParamFormGroupId.keySet()) {
                            HashMap<String, CoreMobileParamForm> coreMobileParamForms1 = mapResultParamFormGroupId.get(key);
                            for (String paramCode : listParamInConfig) {
                                if (!mapParamValueFinal.containsKey(paramCode)) {
                                    if (!paramSucess.contains(paramCode)) {
                                        paramSucess = paramSucess + Config.SPLITTER_VALUE + paramCode;
                                    }
                                    ParamValue paramValue = new ParamValue();
                                    paramValue.setParamCode(paramCode);
                                    paramValue.setParamValue("");
                                    mapParamValueFinal.put(paramCode, paramValue);
                                    if (coreMobileParamForms1.containsKey(paramCode)) {
                                        mapParamValueFinal.get(paramCode).setParamValue((coreMobileParamForms1.get(paramCode).getParamValue() == null ? "" : coreMobileParamForms1.get(paramCode).getParamValue()));
                                    } else {
                                        mapParamValueFinal.get(paramCode).setParamValue("");
                                        paramFail = paramFail + Config.SPLITTER_VALUE + paramCode;
                                    }

                                    continue;
                                }
                                if (coreMobileParamForms1.containsKey(paramCode)) {
                                    mapParamValueFinal.get(paramCode).setParamValue(mapParamValueFinal.get(paramCode).getParamValue() + Config.SPLITTER_VALUE + (coreMobileParamForms1.get(paramCode).getParamValue() == null ? "" : coreMobileParamForms1.get(paramCode).getParamValue()));
                                } else {
                                    mapParamValueFinal.get(paramCode).setParamValue(mapParamValueFinal.get(paramCode).getParamValue() + Config.SPLITTER_VALUE + "");
                                }
                            }
                            for (CoreMobileParamForm coreMobileParamFormTemp : coreMobileParamForms) {
                                if (!mapParamValueFinal.containsKey(coreMobileParamFormTemp.getParamCode())) {

                                    ParamValue paramValue = new ParamValue();
                                    paramValue.setParamCode(coreMobileParamFormTemp.getParamCode());
                                    paramValue.setParamValue(coreMobileParamFormTemp.getParamValue());
                                    mapParamValueFinal.put(paramValue.getParamCode(), paramValue);
                                } else {
                                    mapParamValueFinal.get(coreMobileParamFormTemp.getParamCode()).setParamValue(mapParamValueFinal.get(coreMobileParamFormTemp.getParamCode()).getParamValue() + Config.SPLITTER_VALUE + coreMobileParamFormTemp.getParamValue());
                                }
                            }
                        }
                    } else {
                        if (!paramFail.contains(org.apache.commons.lang3.StringUtils.join(listParamInConfig, Config.SPLITTER_VALUE))) {
                            paramFail = paramFail + Config.SPLITTER_VALUE + org.apache.commons.lang3.StringUtils.join(listParamInConfig, Config.SPLITTER_VALUE);
                        }
                    }
                }
            }
            paramSucess = paramSucess.startsWith(Config.SPLITTER_VALUE) ? paramSucess.substring(1) : paramSucess;
            paramFail = paramFail.startsWith(Config.SPLITTER_VALUE) ? paramFail.substring(1) : paramFail;
            if (paramSucess != null && !"".equals(paramSucess)) {
                MessageUtil.setInfoMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.load.param.ncms.sucess"), paramSucess));
            }
            if (paramFail != null && !"".equals(paramFail)) {
                MessageUtil.setErrorMessage(MessageFormat.format(MessageUtil.getResourceBundleMessage("label.load.param.ncms.error"), paramFail));
            }
            return mapParamValueFinal;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new HashMap<>();
        }

    }

    public static List<HashMap<String, ParamValue>> getListMapParamCondition(List<ParamInput> paramInputs, HashMap<String, ParamValue> mapParamValue) {
        Map<String, String> mapParamValueInCmd = new HashMap<>();
        List<HashMap<String, ParamValue>> listMapParmCondition = new ArrayList<>();
        int maxIndex = 1;
        for (ParamInput paramInput : paramInputs) {
            ParamValue paramValue = mapParamValue.get(paramInput.getParamCode());
            if (paramValue == null || paramInput.getIsParamKey() == null || !paramInput.getIsParamKey()) {
                continue;
            }
            String paramValue2 = paramValue.getParamValue();
            if (paramValue2 != null && !paramValue2.isEmpty()) {
                mapParamValueInCmd.put(paramInput.getParamCode(), paramValue2);
                maxIndex = paramValue2.split(";", -1).length > maxIndex ? paramValue2.split(";", -1).length : maxIndex;
            }
        }
        for (int i = 0; i < maxIndex; i++) {
            HashMap<String, ParamValue> mapParamCondition = new HashMap<>();
            for (ParamInput paramInput : paramInputs) {
                ParamValue paramValue = mapParamValue.get(paramInput.getParamCode());
                if (paramValue == null) {
                    continue;
                }
                if (paramInput.getIsParamKey() == null || !paramInput.getIsParamKey()) {
                    mapParamCondition.put(paramInput.getParamCode(), mapParamValue.get(paramInput.getParamCode()));
                    continue;
                }
                if (paramValue.getParamValue() == null || paramValue.getParamValue().isEmpty()) {
                    paramValue.setParamValue("");
                    mapParamCondition.put(paramInput.getParamCode(), paramValue);
                    continue;
                }
                String[] arr = paramValue.getParamValue().split(";", -1);
                int index = arr.length - 1;
                index = index < i ? index : i;
                ParamValue paramValue1 = new ParamValue();
                paramValue1.setParamCode(paramInput.getParamCode());
                paramValue1.setParamValue(arr[index]);
                mapParamCondition.put(paramInput.getParamCode(), paramValue1);
            }
            listMapParmCondition.add(mapParamCondition);
        }
        return listMapParmCondition;
    }

    public static void setService(NcmsWebService_Service service) {
        NCMSService.service = service;
    }

    public static void sortResult(List<CoreMobileParamForm> coreMobileParamForms) {
        Collections.sort(coreMobileParamForms, new Comparator<CoreMobileParamForm>() {

            @Override
            public int compare(CoreMobileParamForm o1, CoreMobileParamForm o2) {
                int r = comp(o1.getGroupId(), o2.getGroupId());
                return r;
            }

            public int comp(Long a, Long b) {
                if (a > b) {
                    return 1;
                } else if (a < b) {
                    return -1;
                } else {
                    return 0;
                }

            }
        });
    }
}
