package com.viettel.util;

import org.apache.log4j.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by quytv7 on 8/28/2017.
 */
public class LogUtils {

    private static final Logger logger = Logger.getLogger("log.Action");
    public static final String appCode = "VMSA";

    public enum ResultCode {

        SUCCESS,
        FAIL
    }

    public enum ErrorLevel {

        LOW,
        NORMAL,
        HIGHT
    }

    public enum ActionType {

        IMPACT,
        CREATE,
        UPDATE,
        DELETE,
        VIEW,
        EXPORT,
        CLONE,
        IMPORT

    }

    /*
     * Log đăng nhập (thành công và không thành công)
     1. Thời gian
     2. Nguồn (ID/IP) đăng nhập
     3. Nội dung
     4. Kết quả
     */
    public static void log1(Date date, String ip, String content, ResultCode result) {
        try {
            if (logger != null) {
                StringBuilder info = new StringBuilder();

                info.append(date).append("||");
                info.append(ip).append("||");
                info.append(content).append("||");
                info.append(result);
                if (result.equals(ResultCode.SUCCESS)) {
                    logger.info(info.toString().replace("null", "N/A"));
                } else {
                    logger.error(info.toString().replace("null", "N/A"));
                }
            }
        } catch (Exception e) {
            if (logger != null) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    /*
     * Log lỗi của ứng dụng (trong quá trình thực thi ứng dụng để để hỗ trợ sửa lỗi)
     1. StartAction
     2. AppCode
     3. StartTime
     4. EndTime
     5. User
     6. ipClient
     7. linkWeb
     8. Function
     9. ActionMethod
     10. ActionType
     11. Content
     start_action|App_code|start_time|end_time|user|ip_client|link_web|class|action_method|action_type|content
     */
    public static void writelog(Date StartTime,
            String className, String actionMethod, String actionType,
            String content) {
        try {
            if (logger != null) {
                StringBuilder info = new StringBuilder();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                info.append(appCode).append("|");
                info.append(dateFormat.format(StartTime)).append("|");
                info.append(dateFormat.format(new Date())).append("|");
                info.append(SessionUtil.getCurrentUsername()).append("|");
                info.append(getRemoteIpClient()).append("|");
                info.append(getUrl()).append("|");
                info.append(className).append("|");
                info.append(actionMethod).append("|");
                info.append(actionType).append("|");
                info.append(content).append("|");
                try {
                    info.append(SessionUtil.getCurrentSession().getId()).append("|");
                } catch (Exception e) {
                    if (logger != null) {
                        logger.debug(e.getMessage(), e);
                    }
                }
                if (logger != null) {
                    logger.info(info.toString().replace("null", "N/A").replace("Null", "N/A"));
                }
            }
        } catch (Exception e) {
            if (logger != null) {
                logger.debug(e.getMessage(), e);
            }
        }
    }

    /**
     * Ham lay thong tin IP nguoi dung
     *
     * @return
     */
    public static String getRemoteIpClient() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            if (request != null) {
                return request.getRemoteAddr();
            } else {
                return "N/A";
            }
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
            return "N/A";
        }
    }

    /**
     * Ham lay thong tin LinkWeb
     *
     * @return
     */
    public static String getUrl() {
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            if (request != null) {
//            _REQUEST_PATH = req.getServletPath();
//            int pos = _REQUEST_PATH.indexOf("/", _FACES.length());
//            String SUB_REQUEST_PATH = _REQUEST_PATH.substring(0, pos);
//            _REQUEST_PATH = _REQUEST_PATH.substring(0, _REQUEST_PATH.indexOf(_XHTML) + _XHTML.length());
                return request.getServerName() + // "myhost"
                        ":" + // ":"
                        request.getServerPort() + // "8080"
                        request.getRequestURI();      // "lastname=Fox&age=30"
            } else {
                return "N/A";
            }
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
            return "N/A";
        }
    }

    public static String addContent(String content, String addCont) {
        try {
            if ("".equals(content)) {
                return addCont;
            } else {
                return content + ", " + (addCont == null ? "null" : addCont);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return content + ", " + ex.getMessage();
        }
    }
}
