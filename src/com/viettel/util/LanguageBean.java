package com.viettel.util;

import com.viettel.model.CatCountryBO;
import com.viettel.persistence.CatCountryServiceImpl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.LinkedHashMap;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@ManagedBean(name = "language")
@SessionScoped
public class LanguageBean implements Serializable {

    protected static Map<String, Object> locales;
    //20170511_HaNV15_Add_Start
    static Map<String, String> countryCodes;
    String timeZoneForCalendar;
    static Map<String, CatCountryBO> timeZones;
    private CatCountryBO timeZone;
    protected static final Logger logger = LoggerFactory.getLogger(LanguageBean.class);

    static {
        countryCodes = new HashMap<>();
        countryCodes.put(Constants.VNM, Constants.VNM);
        countryCodes.put(Constants.VTP, Constants.VTP);
        countryCodes.put(Constants.VTZ, Constants.VTZ);
    }
    //20170511_HaNV15_Add_End

    static {
        locales = new HashMap<String, Object>();
        Locale vn = new Locale("vi", "VN");
        locales.put(vn.getLanguage(), vn);
        Locale us = new Locale("en", "US");
        locales.put(us.getLanguage(), us);
    }

    static {
        LinkedHashMap<String, String> order = new LinkedHashMap<>();
        order.put("timeZone", "ASC");
        try {
            timeZones = new HashMap<>();
            countryCodes = new HashMap<>();
            HashMap<String, Object> _filter = new HashMap<>();
            _filter.put("status", 1l);
            List<CatCountryBO> _timeZones = new CatCountryServiceImpl().findList(_filter, order);
            for (CatCountryBO timeZone : _timeZones) {
                timeZones.put(timeZone.getCountryCode(), timeZone);
                countryCodes.put(timeZone.getCountryCode(), timeZone.getCountryCode());
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    //20160917_hienhv4_fix ngon ngu mac dinh tieng viet_start
    private String localeCode = new Locale("vi", "VN").getLanguage();

    //20160917_hienhv4_fix ngon ngu mac dinh tieng viet_end
    public List<String> getCountries() {
        List<String> countries = new ArrayList<>();
        for (Iterator<String> iterator = locales.keySet().iterator(); iterator.hasNext(); ) {
            String string = (String) iterator.next();
            countries.add(string);
        }
        return countries;
    }

    public List<Locale> getCountrie2s() {
        List<Locale> countries = new ArrayList<>();
        for (Iterator<String> iterator = locales.keySet().iterator(); iterator.hasNext(); ) {
            String string = (String) iterator.next();
            countries.add((Locale) locales.get(string));
        }
        return countries;
    }

    public void countryLocaleCodeChanged(ValueChangeEvent e) {

        String newLocaleCode = e.getNewValue().toString();
        for (Iterator<String> iterator = locales.keySet().iterator(); iterator.hasNext(); ) {
            String localeCode = (String) iterator.next();
            if (localeCode.equals(newLocaleCode)) {
                FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) locales.get(localeCode));
                // hienhv4_20160910_fix loi message_start
                MessageUtil.setResourceBundle();
                // hienhv4_20160910_fix loi message_end
            }
        }
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    public static Map<String, Object> getLocales() {
        return locales;
    }

    //20170510_HaNV15_Add_Start
    public void countryCodeChanged(ValueChangeEvent e) {
        String valSelected = e.getNewValue().toString();
        for (Iterator<String> iterator = countryCodes.keySet().iterator(); iterator.hasNext(); ) {
            String countryCode = iterator.next();
            if (countryCode.equals(valSelected)) {
                HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                HttpSession session = request.getSession();
                session.setAttribute(Constants.COUNTRY_CODE_CURRENT, countryCode);
            }
        }
    }

    public List<String> getListCountryCode() {
        List<String> countryCode = new ArrayList<>();
        for (Iterator<String> iterator = countryCodes.keySet().iterator(); iterator.hasNext(); ) {
            String string = iterator.next();
            countryCode.add(string);
        }
        return countryCode;
    }

    public String getCountryName(String countryCode) {
        switch (countryCode) {
            case Constants.VNM:
                return "Vietnam";
            case Constants.VTP:
                return "Peru";
            case Constants.VTZ:
                return "Tanzanian";
            default:
                break;
        }
        return "";
    }

    private String countryCode = Constants.VNM;

    public String getCountryCode() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession();
        countryCode = (String) session.getAttribute(Constants.COUNTRY_CODE_CURRENT);
        return countryCode;
    }

    public String getTimeZoneDisplay(String countryCode) {
        if (timeZones.get(countryCode) != null) {
            return timeZones.get(countryCode).getTimeZone();
        }
        return "";
    }

    public void loadTimeZoneForCalendar() {
        String timezone = getTimeZoneDisplay(getCountryCode());
        if (timezone != null) {
            String[] split = timezone.split("\\)\\s", -1);
            if (split.length == 2) {
                setTimeZoneForCalendar(split[1]);
            }
        }
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public static Map<String, String> getCountryCodes() {
        return countryCodes;
    }

    public static void setCountryCodes(Map<String, String> countryCodes) {
        LanguageBean.countryCodes = countryCodes;
    }

    public String getTimeZoneForCalendar() {
        if (timeZoneForCalendar == null) {
            loadTimeZoneForCalendar();
        }
        return timeZoneForCalendar;
    }

    public void setTimeZoneForCalendar(String timeZoneForCalendar) {
        this.timeZoneForCalendar = timeZoneForCalendar;
    }
    //20170510_HaNV15_Add_End

    public CatCountryBO getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(CatCountryBO timeZone) {
        this.timeZone = timeZone;
    }

    public String getLocaleName(String localeCode) {
        switch (localeCode) {
            case "vi":
                return "Tiếng Việt";
            case "en":
                return "English";
            default:
                break;
        }
        return "";
    }
}
