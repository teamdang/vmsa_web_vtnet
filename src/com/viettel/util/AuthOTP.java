package com.viettel.util;

import com.viettel.sendsms.client.SmsServer;
import com.viettel.sendsms.client.SmsServerImplService;
import com.viettel.sendsms.client.SmsServerImplServiceLocator;
import com.viettel.vsa.token.UserToken;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class AuthOTP {

    protected static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(AuthOTP.class);
    private final ResourceBundle config;

    public AuthOTP() {
        // TODO Auto-generated constructor stub
        config = ResourceBundle.getBundle("config");
    }

    public boolean createOTP(String timezone, List<Integer> typeSends) {
        Random random = new Random();
        int otpNumber = random.nextInt(999999 - 100000) + 100000;
        //Keep in session
        Date date = new Date();
        SessionUtil.getCurrentSession().setAttribute("dateCreateOtp", date);
        SessionUtil.getCurrentSession().setAttribute("otpNumberCode", otpNumber);
        SessionUtil.getCurrentSession().setAttribute("isAuthOtp", false);
        //Send SMS
        SmsServerImplService SmsService;
        try {
            SmsService = new SmsServerImplServiceLocator();
            System.out.println(config.keySet());
            SmsServer smsServer = SmsService.getSmsServerImplPort(new URL(config.getString("SMS_SERVER_ADDRESS")));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MINUTE, 5);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss Z");
            if (timezone == null) {
                timezone = "Asia/Ho_Chi_Minh";
            }
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timezone));

            String contentMsg = MessageUtil.getResourceBundleMessage("view.otp.msg.content", otpNumber,
                    simpleDateFormat.format(calendar.getTime()));

            UserToken user = (UserToken) SessionUtil.getCurrentSession().getAttribute("vsaUserToken");
            if (user != null) {
                if (user.getCellphone() != null) {
                    String phones = user.getCellphone();
                    for (String phone : phones.split("[,;]", -1)) {
                        phone = phone.trim().replaceAll("^0", "84");
                        if (phone.startsWith("9")) {
                            phone = "84" + phone;
                        }
                        if (typeSends.contains(0)) {
                            smsServer.sendSingleSms("VMSA", contentMsg, phone, 12L, 50);
                        }
                        if (typeSends.contains(1)) {
                            smsServer.sendFlashSms("VMSA", contentMsg, phone, 12L, 50);
                        }
                        if ((typeSends.contains(0)) || (typeSends.contains(1))) {
                            LOGGER.info("Sent OTP to " + phone);
                        }
                    }
                }
                if (typeSends.contains(2) && user.getEmail() != null) {
                    String subject = MessageUtil.getResourceBundleMessage("view.otp.msg.subject");
                    new MailServiceUtil().sendMail(new String[]{user.getEmail()}, null, null, subject, contentMsg, (byte[][]) null, null);
                    LOGGER.info("Sent OTP to " + user.getEmail());
                }
                return true;
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;

    }

    public Object[] validateOTP(String otp) {
        Date dateCreateOtp = (Date) SessionUtil.getCurrentSession().getAttribute("dateCreateOtp");
        Integer otpNumberCode = (Integer) SessionUtil.getCurrentSession().getAttribute("otpNumberCode");
        Object[] results = new Object[]{false, MessageUtil.getResourceBundleMessage("error.otp.wrong.default")};
        if (dateCreateOtp == null) {
            results[1] = MessageUtil.getResourceBundleMessage("error.otp.wrong.no.otp");
            return results;
        }
        if ((boolean) SessionUtil.getCurrentSession().getAttribute("isAuthOtp")) {
            if ((new Date().getTime() - dateCreateOtp.getTime()) / (60 * 1000) <= Integer.valueOf(config.getString("MAX_TTL_OTP"))) {
                results[0] = true;
                results[1] = "OK";
                SessionUtil.getCurrentSession().setAttribute("dateCreateOtp", new Date());
                return results;
            } else {
                results[1] = MessageUtil.getResourceBundleMessage("error.otp.wrong.expired");
            }
        } else {
            if ((new Date().getTime() - dateCreateOtp.getTime()) / (60 * 1000) <= 5) {
                if (otp != null && otpNumberCode != null) {
                    if (otp.equalsIgnoreCase(otpNumberCode + "")) {
                        results[0] = true;
                        results[1] = "OK";
                        SessionUtil.getCurrentSession().setAttribute("dateCreateOtp", new Date());
                        SessionUtil.getCurrentSession().setAttribute("isAuthOtp", true);
                    } else {
                        results[0] = false;
                        results[1] = MessageUtil.getResourceBundleMessage("error.otp.wrong");
                    }
                } else {
                    results[0] = false;
                    if (otp == null) {
                        results[1] = "Chưa nhập mã xác thực";
                    } else {
                        results[1] = MessageUtil.getResourceBundleMessage("error.otp.wrong.no.otp");
                    }
                }
            } else {
                if (otp != null && otpNumberCode != null) {
                    if (otp.equalsIgnoreCase(otpNumberCode + "")) {
                        results[0] = false;
                        results[1] = MessageUtil.getResourceBundleMessage("error.otp.expired");
                    }
                }
            }
        }

        return results;
    }

}
