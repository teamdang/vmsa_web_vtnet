package com.viettel.util;

import java.io.File;
import java.util.ResourceBundle;

public class Config {

    // Trang error.
    public static final String _ERROR_PAGE = "/error";
    // Doi tuong UserToken trong du lieu VSA tra ve.
    public static final String _VSA_USER_TOKEN = "vsaUserToken";
    // Config default url cho ung dung.
    // Neu khong dung default thi gan gia tri ve ""
    public static final String _DEFAULT_URL = "/execute";
    public static final String _DEFAULT_URL2 = "/action";
    public static final String _DEFAULT_URL3 = "/command";
    public static final String _DEFAULT_URL4 = "/mop";

    public static final String ROOT_FOLDER_DATA = "datas";
    public static final String MEDIA_FOLDER = "medias";
    public static final String IMAGE_FOLDER = "images";
    public static final String AUDIO_FOLDER = "audios";

    public static final String SALT = "ipchange";

    public static final String PATH_OUT = File.separator + ".." + File.separator + ".." + File.separator + "report_out" + File.separator;
    public static final String XLSX_FILE_EXTENTION = ".xlsx";
    public static final Long EXECUTE_CMD = 1L;
    public static final Long ROLLBACK_CMD = 2L;
    public static final String SPLITTER_VALUE = ";";
    public static final Long EXECUTE_AVAILABLE = 1L;
    public static final Long SAVE_DRAFT = 0L;
    protected static String[] COLORS = {"#7FFF00", "#32CCFE", "#FF8C00", "#007780", "#FF4500", "#DDA0DD", "#D2B48C", "#FF6347", "#00FFFF", "#FFDD66", "#33CC33",
            "#0099CC", "#CD853F", "#97CDCC", "#A2CD5A", "#FFD800", "#7F8800", "#00FF00", "#32FFFE", "#997780", "#DDA084"};

    public static final Long SAVE_FLAG = 0l;
    public static final Long WAITTING_FLAG = 1l;
    public static final Long RUNNING_FLAG = 2l;
    public static final Long FINISH_FLAG = 3l;
    public static final Long FAIL_FLAG = 4l;
    public static final Long NODE_TYPE_ID_DEFAULT = -1L;
    public static final Long CMD_TYPE_IMPACT = 0l;
    public static final Long CMD_TYPE_VIEW = 1l;
    //20171031_hienhv4_load tham so truoc khi chay_start
    public static final Long LOAD_PARAM_FLAG = 11l;
    //20171031_hienhv4_load tham so truoc khi chay_end
    public static final Long CMD_TYPE_GET_PARAM = 2l;

    public static final Long CMD_SOURCE_GET_PARAM_SYSTEM = 0l;
    public static final Long CMD_SOURCE_GET_PARAM_NCMS = 1l;


    public static final Integer APPROVAL_STATUS_DEFAULT = 0;
    public static final Integer APPROVALED_STATUS_LEVEL1 = 1;
    public static final Integer APPROVALED_STATUS_LEVEL2 = 9;
    public static final Long STOP_FLAG = 6l;
    //20170824_hienhv4_add_start
    public static final Long SCHEDULE_FLAG = 7l;
    //20170824_hienhv4_add_end
    //20171103_hienhv4_Them trang thai tam dung_start
    public static final Long PAUSE_FLAG = 8l;
    public static final Long LOGIN_FAIL_FLAG = 10l;
    //20171103_hienhv4_Them trang thai tam dung_end

    public static final Long AGG_NODE_ODD = 1L;
    public static final Long AGG_NODE_EVEN = 0L;
    public static final Long SGSN_ACTIVE = 1L;
    public static final Long SGSN_BACKUP = 0L;

    public static final String PROVINCE_CORE_NODE_TYPE = "CORE_PROVINCE";
    //	public static final String PROVINCE_CORE_NODE_TYPE = "CV,CORE_AREA";
    public static final String AGG_NODE_TYPE = "AGG METRO";
    public static final String SRT_NODE_TYPE = "SRT";
    public static final String SWITCH_NODE_TYPE = "SWITCH_NODE_TYPE";
    public static final String AGG_NODE_CLIENT_CONNECT_DIRECTOR = "AGG_NODE_CLIENT_CONNECT_DIRECTOR";
    public static final Long SRT_NODE_CLIENT_CONNECT_DIRECTOR = 2L;
    public static final Long FLOW_RUN_ACTION_FINISH_STATUS = 3l;
    public static final Long FLOW_RUN_ACTION_FAIL_STATUS = 4L;

    public static final Long ROLLBACK_ACTION = 1l;
    public static final Long EXECUTE_ACTION = 0L;

    public static final Long ACTION_TYPE_NORMAL = 0L;
    public static final Long ACTION_TYPE_MANUAL = 1L;

    public static final String PROTOCOL_TELNET = "TELNET";
    public static final String PROTOCOL_SSH = "SSH";
    public static final String PROTOCOL_SQL = "SQL";
    public static final String PROTOCOL_EXCHANGE = "EXCHANGE";
    public static final String PROTOCOL_FTP = "FTP";
    public static final String CR_AUTO_DECLARE_CUSTOMER = "CR_AUTO";
    public static final String CR_DEFAULT = "CR_DEFAULT";
    public static final String SUB_FLOW_RUN_DEFAULT = "Default";
    public static final Long SUB_FLOW_RUN_DEFAULT_NUM = 1L;
    public static final String MscNotRunIntegrateStation = "MSHL05;";//Truong hop tich hop tram khong can khai tren MSC nay

    public static final Long RUNNING_TYPE_INDEPENDECE = 1l; // Cac node chay doc lap
    public static final Long RUNNING_TYPE_DEPENDENT = 2L; // Cac node chay song song

    public static final boolean isTestMerge = true;
    //Quytv7_ gan loại nghiep vu. Trong ban statin_config_import
    public static final Long IS_RULE_INTEGRATE = 0L;
    public static final Long IS_RULE_RELATION = 1L;
    public static final Long IS_RULE_DELETE = 2L;
    public static final Long IS_RULE_UPGRADE_DONWGRADE = 3L;
    public static final Long IS_RULE_CHANGE_FESTIVAL = 4L;
    public static final Long IS_RULE_CHANGE_FORCE = 5L;
    public static final Long IS_RULE_CHANGE_NCMS = 6L;
    public static final Long IS_RULE_LOAD_LICENSE = 7L;
    public static final Long IS_RULE_CHANGE_PARAM = 8L;
    public static final Long IS_RULE_ACTIVE_STANDARD = 9L;
    public static final Long IS_RULE_ACTIVE_TEST = 10L;
    public static final Long IS_RULE_CHANGE_VLAN = 11L;

    public static final Long ParamTypeRnc = 0L;
    public static final Long ParamTypeNodeB = 1L;
    public static final Long ParamTypeCell = 2L;

    public static final Long CHANGE_PARAM_FESTIVAL = 0L;
    public static final Long CHANGE_PARAM_NCMS = 1L;

    public static final String CmdProtocolFtp_cmd = "Ftp pathFile = @{ftpPathFile}, filename = @{ftpFileName}";
    public static final String CmdProtocolFtp_ftpPathFile = "ftpPathFile";
    public static final String CmdProtocolFtp_ftpFileName = "ftpFileName";
    public static final String CmdProtocolFtp_destination = "destination";

    public static final String networkType2G = "2G";
    public static final String networkType3G = "3G";

    public static final Long validateIntegrate_uarfcnDl_uarfcnUl = 950L;
    public static final Long validateIntegrate_primaryScramblingCode = 511L;
    public static final Long validateIntegrate_pwroffset_pwradm = 100L;
    public static final String vmsa_result_code = "vmsa_result_code";
    public static final String vmsa_result_detail = "vmsa_result_detail";
    public static final String vmsa_code = "VMSA";
    public static final int maxNodeInDT = 15;


    public static String[] getCOLORS() {
        return COLORS;
    }

    public static enum ACTION {

        NEW(1), UPDATE(2), DELETE(3), SUSPEND(4);
        public Long value;

        ACTION(long v) {
            value = v;
        }
    }

    ;

    public static enum SERVICE {

        L2VPN(2), L3VPN(3), ENODEB_L2(4), ENODEB_L3(5);
        public long value;

        SERVICE(int v) {
            value = v;
        }
    }

    ;

    public static enum NODE_TYPE {

        AGG_METRO(6),
        ASW_DCN(103),
        BB_INTRANET(114),
        BRAS(109),
        CKV(2),
        CORESW_LAYER(113),
        CORE_LAYER(105),
        CORE_PROVINCE(111),
        CV(3),
        DGW(5),
        DSLAM(100),
        GATEWAY_LAYER(112),
        GPON_AMP(102),
        GPON_OLT(8),
        IGW_PEERING(4),
        NODE_TYPE_DEFAULT(-1),
        PECD(1),
        PE_2G(110),
        SRT(7),
        RNC(8),
        STP(115),
        SWITCH(101),
        SGSN(11),
        MSC(13);
        public Long value;

        NODE_TYPE(long value) {
            this.value = value;
        }
    }

    public static enum CONNECT_TYPE {

        L2_SW(2), DIRECT(1);
        public Long value;

        CONNECT_TYPE(long value) {
            this.value = value;
        }
    }

    public static enum VERSION {

        CRS_X16_MC(4),
        ASR9010(5),
        Cisco_ASR9922(6),
        ASR9000(7),
        CX600_X16A(8),
        CRS_X16_SC(9),
        _5928E(100),
        IS2828F(101),
        FSAP9800(102),
        MA5300(103),
        IES5000(104),
        MA5605(105),
        S3100_28FC(106),
        MES3500_24F(107),
        SM3100_28TC_AC(108),
        ED5229_64(109),
        OVSxE64190(110),
        H89(111),
        ED5229_32(112),
        H88(113),
        CX600_8(114),
        MA5103(115),
        IES1000(116),
        CX600_X16(117),
        S3400(118),
        S2309(119),
        DCS_3950(120),
        ASR901_6CZ_FT(121),
        MEDFA_FHx64(122),
        MEDFA_AT5200x64(123),
        S3300(124),
        MEDFA_IPGx64(125),
        ES_3124F(126),
        CiscoCRS16(127),
        CX600_16(128),
        SF300_24(129),
        Cisco_ARS901(16),
        V45(254),
        CRS3(1),
        CX600_X8(2),
        S5328(3),
        C300(10),
        C320(11),
        MA5603T(12),
        MA5608(13),
        OLT_ALU(14),
        V8240(15),
        ATN910B(17),
        ZXCTN9008(130),
        MA5303(131),
        M41(132),
        MA5608T(133),
        IPNODEB(134),
        MA5105(135),
        MEDFA_FHx32(136),
        SAM960(137),
        Firewall_F800C(138),
        _3928A(139),
        NSH_5632(140),
        BRAS_MX960(141),
        R2126EA_MA(142),
        FG800C(143),
        ASR9922(144),
        UA5000(145),
        _24S(146),
        S5352(147),
        Fortigate800C(148),
        ES1248(149),
        S6509(150),
        NE80(151),
        MA5100(152),
        T160G(153),
        C2950(154),
        C3550(155),
        R7206(156),
        S5300(157),
        S3900(158),
        C3750(159),
        S5648(160),
        Q9300(161),
        C2850(162),
        MA5200G4(163),
        SE800(164),
        M6000(165),
        T64G(166),
        Q5648(167),
        SE400(168),
        Q3900(169),
        S3500(170),
        R7613(171),
        PC6248(172),
        R3845(173),
        Q5328(174),
        SCE8000(175),
        T640(176),
        SGSE_14(177),
        NS500(178),
        ASA5540(179),
        Q5352(180),
        SummitX440(181),
        IPS3500(182),
        Firewall_ISG1000(183),
        C2911(184),
        C3560(185),
        S3928(186),
        C4900(187),
        SSG550(188),
        S3528(189),
        HPA5500_48G_EI(190),
        C2900(191),
        E200(192),
        GS748T(193),
        AX1000(194),
        C2620(195),
        C1900(196),
        C2600(197),
        SSG350(198),
        BIGIP3600(199),
        BIGIP1600(200),
        FWS624(201),
        HSM(202),
        ZXR10T64G(203),
        AX2100(204),
        FG200B(205),
        MPX9500(206),
        AR46(207),
        S5600(208),
        TLSG1024(209),
        ZXR108902(210),
        ZXR105952(211),
        PCT6248(212),
        PCT5448(213),
        S3526(214),
        PCT6224(215),
        PTC6224(216),
        C880(217),
        cisco2960(218),
        FG620(219),
        HPA5500_24GEI(220),
        C2800(221),
        C870(222),
        C1800(223),
        PIX515(224),
        Z5952(225),
        FG110C(226),
        MCU_RMX2000(227),
        SRX5800(228),
        ASR903(229),
        TPLink24_Ge(230),
        FA200D(231),
        CTHtest(232),
        CP12407(233),
        Fortinet_1500D(234),
        MAG4610(235),
        C2960(236),
        FG310B(237),
        AR4640(238),
        HP2910(239),
        GGC(240),
        R3745(241),
        S3550(242),
        R2811(243),
        S3560(244),
        S3750(245),
        Q9309(246),
        Q3500(247),
        Q7810(248),
        _7810(249),
        R1741(250),
        R3850(251),
        S5352C_EI(252),
        OVSxE32190(253);

        public Long value;

        VERSION(long value) {
            this.value = value;
        }
    }

    public static enum BAN_DVCD {

        KV1(ResourceBundle.getBundle("config").getString("BAN_DVCD_KV1")),
        KV2(ResourceBundle.getBundle("config").getString("BAN_DVCD_KV2")),
        KV3(ResourceBundle.getBundle("config").getString("BAN_DVCD_KV3"));
        private long value;

        BAN_DVCD(String v) {
            this.value = Long.parseLong(v);
        }

        public long getValue() {
            return value;
        }

        public void setValue(long value) {
            this.value = value;
        }

        public void setValue(String value) {
            this.value = Long.parseLong(value);
        }
    }
}
