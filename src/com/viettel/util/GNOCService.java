package com.viettel.util;

//import com.sun.xml.internal.ws.client.BindingProviderProperties;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

import javax.xml.rpc.ServiceException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.gnoc.cr.service.CrForOtherSystemService;
import com.viettel.gnoc.cr.service.CrForOtherSystemServiceImplServiceLocator;
import com.viettel.gnoc.cr.service.CrOutputForQLTNDTO;
import com.viettel.gnoc.cr.service.ResultDTO;
import com.viettel.passprotector.PassProtector;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Level;
//import javax.xml.ws.BindingProvider;

public class GNOCService {

    protected final static Logger LOGGER = LoggerFactory.getLogger(GNOCService.class);
    private static CrForOtherSystemService service;
    static ResourceBundle bundle = ResourceBundle.getBundle("config");
    static final int TIME_OUT = 30000;

    enum StateGnoc {
        DRAFT(0), OPEN(1), QUEUE(2), COORDINATED(3), EVALUATED(4), APPROVED(5), ACCEPTED(6), RESOLVED(7), INCOMPLETED(8), CLOSED(9);
        public Integer value;

        private StateGnoc(Integer value) {
            this.value = value;
        }

    }

    ;

    static {

        try {
            setService(new CrForOtherSystemServiceImplServiceLocator().getCrForOtherSystemServiceImplPort(new URL(bundle.getString("ws_gnoc_new"))));

//                        ((BindingProvider) service).getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, TIME_OUT);
//                        ((BindingProvider) service).getRequestContext().put(BindingProviderProperties.CONNECT_TIMEOUT, TIME_OUT);
//
//                        ((BindingProvider) service).getRequestContext().put("com.sun.xml.internal.ws.connect.timeout", TIME_OUT);
//                        ((BindingProvider) service).getRequestContext().put("com.sun.xml.internal.ws.request.timeout", TIME_OUT);
        } catch (MalformedURLException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public static Boolean isCanUpdateDT(String crNumber) {
        try {
            String user = PassProtector.decrypt(bundle.getString("ws_gnoc_user"), "ipchange");
            String pass = PassProtecto
            Integer state = Integer.valr.decrypt(bundle.getString("ws_gnoc_pass"), "ipchange");
            CrOutputForQLTNDTO cr = service.getCrForQLTN(user, pass, crNumber);
            if (cr.getState() == null)
                return false;ueOf(cr.getState());
            return state < StateGnoc.EVALUATED.value || Objects.equals(state, StateGnoc.INCOMPLETED.value);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;

    }

    public static Boolean isCanExecute(String crNumber) {
        try {
            String user = PassProtector.decrypt(bundle.getString("ws_gnoc_user"), "ipchange");
            String pass = PassProtector.decrypt(bundle.getString("ws_gnoc_pass"), "ipchange");
            CrOutputForQLTNDTO cr = service.getCrForQLTN(user, pass, crNumber);
            if (cr.getState() == null)
                return false;
            Integer state = Integer.valueOf(cr.getState());
            if (!cr.getUserExecute().equalsIgnoreCase(SessionUtil.getCurrentUsername())) {
                LOGGER.info("Khong duoc quyen tac dong");
                LOGGER.info("Nguoi duoc phep tac dong: "+ cr.getUserExecute());
                LOGGER.info("Nguoi tac dong: "+ SessionUtil.getCurrentUsername());
                return null;
            }
            if (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(cr.getImpactStartTime()).after(new Date())) {
                LOGGER.info("Thoi gian bat dau lon hon thoi gian hien tai "+ new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(cr.getImpactStartTime()));
                return false;
            }
            if (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(cr.getImpactEndTime()).before(new Date())) {
                LOGGER.info("Thoi gian ket thuc nho hon thoi gian hien tai "+ new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(cr.getImpactEndTime()));
                return false;
            }
            return state.equals(StateGnoc.ACCEPTED.value) && cr.getUserExecute().equalsIgnoreCase(SessionUtil.getCurrentUsername());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;

    }

    public static void insertFile(String crNumber, File codeFile, String fileName) {
        try {
            String user = PassProtector.decrypt(bundle.getString("ws_gnoc_user"), "ipchange");
            String pass = PassProtector.decrypt(bundle.getString("ws_gnoc_pass"), "ipchange");
            ResultDTO result = service.insertFile(user, pass,
                    "gnoc_admin", crNumber, "100", fileName, Base64.encodeBase64String(FileUtils.readFileToByteArray(codeFile)));
            result.getMessage();

        } catch (RemoteException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public static boolean updateDTInfo(String username, String crNumber, String[] lstIpNode, String dtName, File mopFile) {
        try {
            String fileContent = Base64.encodeBase64String(FileUtils.readFileToByteArray(mopFile));
            String user = PassProtector.decrypt(bundle.getString("ws_gnoc_user"), "ipchange");
            String pass = PassProtector.decrypt(bundle.getString("ws_gnoc_pass"), "ipchange");

            ResultDTO result = service.updateDtInfo(user, pass,
                    username, crNumber, dtName, lstIpNode,
                    null, mopFile.getName(), fileContent, mopFile.getName(), fileContent, null);

            return "OK".equals(result.getKey());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;
    }

    public static boolean updateDTInfo(String username, String crNumber, String[] lstIpNode,
                                       String mopId, byte[] fileContentByte, String fileName) {
        try {
            String fileContent = Base64.encodeBase64String(fileContentByte);
            String user = PassProtector.decrypt(bundle.getString("ws_gnoc_user"), "ipchange");
            String pass = PassProtector.decrypt(bundle.getString("ws_gnoc_pass"), "ipchange");

            ResultDTO result = service.updateDtInfo(user, pass,
                    username, crNumber, mopId, lstIpNode,
                    new String[0], fileName, fileContent, fileName, fileContent, null);

            LOGGER.info(result.getMessage());

            return "OK".equals(result.getKey());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return false;
    }

    void getCrs() {

    }

    public static void setService(CrForOtherSystemService service) {
        GNOCService.service = service;
    }
}
