/*
 * Created on Jun 7, 2013
 *
 * Copyright (C) 2013 by Viettel Network Company. All rights reserved
 */
package com.viettel.util;

import com.viettel.exception.SysException;
import com.viettel.model.adm.V2AdmDatabase;
import com.viettel.security.PassTranformer;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServiceUtil {

    public static Long getSequence(String sequence) {
        Session session = HibernateUtil.openSession();
        Transaction tx = session.beginTransaction();
        try {
            String sql = "select " + sequence.trim() + ".nextval as nextval from dual ";
            SQLQuery query = (SQLQuery) session
                    .createSQLQuery(sql);
            BigDecimal a = (BigDecimal) query.uniqueResult();
            return a.longValue();
        } catch (HibernateException e) {
            tx.rollback();
            Logger.getLogger(ServiceUtil.class.getName()).log(Level.SEVERE, null, e);
        } catch (Exception e) {
            tx.rollback();
            Logger.getLogger(ServiceUtil.class.getName()).log(Level.SEVERE, null, e);
            throw new SysException(e.getMessage());
        } finally {
            session.close();
        }
        return null;
    }

    public Connection connect(V2AdmDatabase database) {
        Connection conn = null;
        String user = database.getUsername();
        String pass = database.getPassword();
        String url = database.getUrl();
        String driver = database.getDriver();
        String vietelSecKey = MessageUtil.getResourceBundleConfig("viettel_secure_key");
        PassTranformer.setInputKey(vietelSecKey);
        String passDecrypt = null;
        if (pass != null) {
            passDecrypt = PassTranformer.decrypt(pass);
        }
        try {
            Class.forName(driver);
            Properties connectionProperties = new Properties();
            if (user != null) {
                connectionProperties.put("user", user);
            }
            if (passDecrypt != null) {
                connectionProperties.put("password", passDecrypt);
            }
            conn = DriverManager.getConnection(url, connectionProperties);
        } catch (ClassNotFoundException | SQLException e) {
            Logger.getLogger(ServiceUtil.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return conn;
    }
}
