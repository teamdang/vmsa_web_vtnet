/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

import org.apache.log4j.Logger;

/**
 * @author hienhv4
 */
public class TelnetHuawei extends TelnetClientUtil {

    private final Logger logger = Logger.getLogger(TelnetHuawei.class);
    private final String version;
    private final String nodeType;
    private final Long formRun;
    private final String nodeIp;

    public TelnetHuawei(String hostName, Integer port, String vendor, String version, String nodeType, Long formRun, String nodeIp) {
        super(hostName, port, vendor);
        cmdExit = "logout";
        this.version = version;
        this.nodeType = nodeType;
        this.formRun = formRun;
        this.nodeIp = nodeIp;
    }

    @Override
    public String connect(String username, String password) throws Exception {
        if (this.version != null && "V200R005".equals(this.version.toUpperCase().trim())) {
            setShellPromt("(>|#)");
            return super.connect(username, password);
        } else if (this.nodeType != null && "CRBT-MNODE".equals(this.nodeType.toUpperCase().trim())) {
            setShellPromt("(>|#)");
            return super.connect(username, password);
        }
        connect(false);
        String slogin;
        if ("10.60.253.200".equals(hostName)) {
            slogin = "LGI: OPNAME = \":username\", PWD = \":password\", DECODEMODE=GB2312;";
        } else {
            slogin = "LGI:OP=\":username\", pwd=\":password\";";
        }

        slogin = slogin.replace(":username", username);
        slogin = slogin.replace(":password", password);
        setShellPromt("  END");
        String sLog;
        if (formRun != null && formRun.equals(1L)) {
            sLog = sendWait(slogin);
        } else {
            sLog = sendWait(slogin);
        }
        if (!sLog.contains("RETCODE = 0")) {
            throw new Exception("LOGIN_FAIL");
        }
//quytv7 Xet truong hop chay qua OSS
        if (formRun != null && formRun.equals(1L)) {
            String cmd = "REG NE:IP=\":nodeIp\";";
            logger.info("Cmd : " + cmd);
            cmd = cmd.replace(":nodeIp", nodeIp);
            sLog = sLog + "\n" + sendWait(cmd);
        }
        return sLog;
    }

    @Override
    public String sendWait(String command, String promt, boolean isRegex, Integer timeOut) throws Exception {
        return super.sendWait(command, promt, isRegex, timeOut);
    }

    @Override
    protected String readUntilBelongRegex(String cmd, String pattern, int timeOut) throws Exception {
        if (this.version != null && "V200R005".equals(this.version.toUpperCase().trim())) {
            return super.readUntilBelongRegex(cmd, pattern, timeOut);
        } else if (this.nodeType != null && "CRBT-MNODE".equals(this.nodeType.toUpperCase().trim())) {
            return super.readUntilBelongRegex(cmd, pattern, timeOut);
        } else {
            StringBuilder sb = new StringBuilder();
            System.out.println("pattern: " + pattern);
            int startIndex = -1;
            try {
                if (checkInput()) {
                    long startTime = System.currentTimeMillis();
                    char[] chs = new char[defaultCharBufferSize];
                    int t = buf.read(chs);

                    while (t != -1) {
                        if (isDebug) {
                            System.out.print(chs);
                        }
                        sb.append(chs, 0, t);

                        if (cmd == null) {
                            startIndex = 0;
                        } else {
                            if (startIndex < 0) {
                                startIndex = sb.toString().indexOf(cmd);
                                
                                if (startIndex < 0) {
                                    String cmdLocal = cmd;
                                    if (!cmdLocal.contains(": ")) {
                                        cmdLocal = cmd.replace(":", ": ");
                                    }
                                    startIndex = sb.toString().indexOf(cmdLocal);
                                    
                                    if (startIndex < 0) {
                                        cmdLocal = cmdLocal.replaceAll(",\\s+", ",");
                                        
                                        startIndex = sb.toString().indexOf(cmdLocal);
                                    }
                                }
                                
                                if (startIndex >= 0) {
                                    sb = new StringBuilder(sb.toString().substring(startIndex));
                                }
                            }
                        }
                        if (checkRegex(sb.toString(), pattern) && startIndex >= 0) {
                            if (cmd != null) {
                                String content = sb.toString().replace(" [1D", "");

                                if (content.equals(cmd)) {
                                    sb = new StringBuilder();
                                } else {
                                    return sb.toString();
                                }
                            } else {
                                return sb.toString();
                            }
                        }
                        t = buf.read(chs);

                        if (System.currentTimeMillis() - startTime > timeOut) {
                            System.out.println("noi dung lay duoc: " + sb.toString());
                            sendNoWait("\u0003");
                            throw new Exception("Time out for waitting promt: " + pattern);
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("startIndex: " + startIndex);
                logger.error(sb.toString());
                if (!checkRegex(sb.toString(), pattern)) {
                    throw e;
                }
            } finally {
                this.currentOutStr = sb.toString();
            }

            return this.currentOutStr;//.replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
        }
    }

    public boolean enterMode(String meid) {
        try {
            String content = sendWait("USE ME:MEID=" + meid + ";");

            if (content.contains("RETCODE = 0")) {
                return true;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return false;
    }
    @Override
    public void disConnect(String username) throws Exception {
        String slogout;
        if (formRun != null && formRun.equals(1L)) {
            slogout = "LGO:OP=\":username\";";
            slogout = slogout.replace(":username", username);
        }else{
            slogout = "LGO:;\";";
        }
        logger.info("Quytv7 cmdExits:" + slogout);
        if (cmdExit != null) {
            sendNoWait(slogout);
        }
        logger.info("Quytv7 thoat thanh cong");

        try {
            if (out != null) {
                out.close();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        try {
            if (buf != null) {
                buf.close();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        try {
            telnetClient.disconnect();
        } catch (Exception e) {
            throw e;
        }
    }
}
