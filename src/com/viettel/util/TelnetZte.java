/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import org.apache.commons.net.telnet.EchoOptionHandler;
import org.apache.commons.net.telnet.InvalidTelnetOptionException;
import org.apache.commons.net.telnet.SuppressGAOptionHandler;
import org.apache.commons.net.telnet.TerminalTypeOptionHandler;
import org.apache.commons.net.telnet.WindowSizeOptionHandler;

/**
 *
 * @author hienhv4
 */
public class TelnetZte extends TelnetClientUtil {

    public TelnetZte(String hostName, Integer port, String vendor) {
        super(hostName, port, vendor);
        cmdExit = "logout";
    }
    
    @Override
    public String connect(String userPromt, String username, String passwordPromt, String password, boolean isRegex)
            throws InvalidTelnetOptionException, IOException, Exception {
        TerminalTypeOptionHandler terminalHandler = new TerminalTypeOptionHandler(this.terminalType, false, false, true, false);
        EchoOptionHandler echoopt = new EchoOptionHandler(true, false, true, false);
        SuppressGAOptionHandler gaopt = new SuppressGAOptionHandler(true, true, true, true);
        WindowSizeOptionHandler wsopt = new WindowSizeOptionHandler(4096, 24, true, true, true, true);
        telnetClient.setDefaultTimeout(timeout);
        telnetClient.setConnectTimeout(timeout);
        telnetClient.addOptionHandler(terminalHandler);
        telnetClient.addOptionHandler(echoopt);
        telnetClient.addOptionHandler(gaopt);
        telnetClient.addOptionHandler(wsopt);
        //set buffer
        try {
            telnetClient.connect(hostName, port);
            telnetClient.setSoTimeout(timeout);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new Exception("CONN_TIMEOUT");
        }

        telnetClient.setReceiveBufferSize(512);
        telnetClient.setSendBufferSize(512);
        buf = new BufferedReader(new InputStreamReader(telnetClient.getInputStream()), 512);
        out = new PrintStream(telnetClient.getOutputStream());

        //login
        if (isRegex) {
            try {
                readUntilBelongRegex(null, "Current Bureau Code", timeout);
                write("\r\n");
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
            //log.info("Connecting...waiting for userPrompt : " + userPromt);
            readUntilBelongRegex(null, userPromt, timeout);
            write(username);

            //log.info("Connecting...waiting for passPrompt : " + passwordPromt);
            readUntilBelongRegex(null, passwordPromt, timeout);
            write(password);

            Thread.sleep(3000);
            return readUntilBelongRegex(prompt, "[Y/N]:", "N", timeout);
        } else {
            readUntil(userPromt, timeout);
            write(username);
            
            readUntil(passwordPromt, timeout);
            write(password);
            
            return readUntil(prompt, timeout);
        }
    }
    
    @Override
    public void write(String value) throws Exception {
        try {
            out.write((value + "\r\n").getBytes());
            out.flush();
        } catch (Exception e) {
            throw e;
        }
    }
}
