/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

/**
 * @author hienhv4
 */
public class TelnetEricsson extends TelnetClientUtil {

    private final String nodeType;
    private final String nodeCode;
    private boolean isSendCtrlD = false;

    public TelnetEricsson(String hostName, Integer port, String vendor, String osType, String nodeType,
            String nodeCode) {
        super(hostName, port, vendor);
        this.setOs(osType);
        cmdExit = "exit";
        this.nodeType = nodeType;
        this.nodeCode = nodeCode;
    }

    @Override
    public String connect(String username, String password) throws Exception {
        log.info("Ericsson, port = " + port + ", os_type = " + getOs() + ", isDebug = " + isDebug
                + ", ip = " + hostName);
        if (port == 5000 || port == 8010) {
            return connectErissonPort5000(username, password);
        }
        if (getOs() != null && !getOs().trim().isEmpty() && getOs().toLowerCase().contains("linux")) {
            return connectErissonLinux(username, password);
        }

        log.info("LOGIN Window");
        setShellPromt("Windows NT Domain:");
        connect("(.*)login(.*):\\s*$", username, "(.*)ssword:\\s*$", password, true);
        setShellPromt(">");
        sendWait("", ">", false, timeout);
        setShellPromt("<");
        if (getCurrentOutStr().contains("Logon failure")) {
            throw new Exception("LOGIN_FAIL");
        }
        sendWait("mml", null, false, timeout);
        return getCurrentOutStr();
    }

    private String connectErissonPort5000(String username, String password) throws Exception {
        setShellPromt("DOMAIN:");
        connect("USERCODE:\\s*$", username, "PASSWORD:\\s*$", password, true);
        sendWait("\r", "<", false, timeout);
        setShellPromt("<");
        if (getCurrentOutStr().contains("NOT ACCEPTED")) {
            throw new Exception("LOGIN_FAIL");
        }
        return getCurrentOutStr();
    }

    private String connectErissonLinux(String username, String password) throws Exception {
        setShellPromt("(>|#)");
        connect("(.*)login(.*):\\s*$", username, "(.*)ssword:\\s*$", password, true);
        setShellPromt("(<|#|>)");

//        if (strLogin != null && strLogin.contains("\n")) {
//            String loginPrompt = strLogin.substring(strLogin.lastIndexOf("\n"));
//            this.setShellPromt(loginPrompt.trim());
//        }
        if (getCurrentOutStr().contains("Login incorrect")) {
            throw new Exception("LOGIN_FAIL");
        }

        if (nodeType == null) {
            sendWait("mml", null, false, timeout);
        } else {
            switch (nodeType.toUpperCase().trim()) {
                case "SGSN":
                    break;
                case "RNC":
                case "MGW":
                    sendWait("amos " + nodeCode, null, false, timeout);
                    String content = sendWait("lt all", null, false, timeout);
                    if (content != null) {
                        if (content.contains("\n")) {
                            setShellPromt(formatRegex(content.substring(content.lastIndexOf("\n")).trim()));
                        } else {
                            setShellPromt(formatRegex(content.trim()));
                        }
                    }
                    break;
                default:
                    sendWait("mml", null, false, timeout);
                    break;
            }
        }
        return getCurrentOutStr();
    }

    private String connectErissonNoChangeMode(String username, String password) throws Exception {
        setShellPromt("Windows NT Domain:");
        connect("(.*)login(.*):\\s*$", username, "(.*)ssword:\\s*$", password, true);
        setShellPromt(">");
        sendWait("\r\n", ">", false, timeout);
        if (getCurrentOutStr().contains("Logon failure")) {
            throw new Exception("LOGIN_FAIL");
        }
        return getCurrentOutStr();
    }

    private String connectErissonLinuxNoChangeMode(String username, String password) throws Exception {
        setShellPromt(">");
        connect("(.*)login(.*):\\s*$", username + "\r\n", "(.*)ssword:\\s*$", password + "\r\n", true);
        if (getCurrentOutStr().contains("Login incorrect")) {
            throw new Exception("LOGIN_FAIL");
        }
        return getCurrentOutStr();
    }

    @Override
    public String sendWait(String command, String promt, boolean isRegex, Integer timeOut) throws Exception {
        //if (port == 5000 || (getOs() != null && getOs().toLowerCase().contains("linux"))) {
//        if (!"RNC".equals(nodeType.toUpperCase().trim())
//                && !"MGW".equals(nodeType.toUpperCase().trim())
//                && !"PG".equals(nodeType.toUpperCase().trim())) {
//            command = (command + "\r\n");
//        }
        //}
        return super.sendWait(command, promt, isRegex, timeOut);
    }

    @Override
    public String sendWaitHasConfirm(String command, String promt, String confirmPromt, String commandForConfirm, String morePromt, String commandForMore, Integer timeOut) throws Exception {
//        if (!"RNC".equals(nodeType.toUpperCase().trim())
//                && !"MGW".equals(nodeType.toUpperCase().trim())
//                && !"PG".equals(nodeType.toUpperCase().trim())) {
//            return super.sendWaitHasConfirm(command + "\r\n", promt, confirmPromt,
//                    commandForConfirm == null ? null : commandForConfirm + "\r\n", morePromt,
//                    commandForMore == null ? null : commandForMore + "\r\n", timeOut); //To change body of generated methods, choose Tools | Templates.
//        } else {
            return super.sendWaitHasConfirm(command, promt, confirmPromt,
                    commandForConfirm == null ? null : commandForConfirm, morePromt,
                    commandForMore == null ? null : commandForMore, timeOut);
//        }
    }

    @Override
    public String sendWaitHasMore(String command, String promt, String morePromt, String commandForMore, Integer timeOut) throws Exception {
//        if (!"RNC".equals(nodeType.toUpperCase().trim())
//                && !"MGW".equals(nodeType.toUpperCase().trim())
//                && !"PG".equals(nodeType.toUpperCase().trim())) {
//            return super.sendWaitHasMore(command + "\r\n", promt, morePromt, commandForMore == null ? null : commandForMore + "\r\n", timeOut); //To change body of generated methods, choose Tools | Templates.
//        } else {
            return super.sendWaitHasMore(command, promt, morePromt, commandForMore == null ? null : commandForMore, timeOut);
//        }
    }

    @Override
    protected String readUntilBelongRegex(String cmd, String pattern, int timeOut) throws Exception {
        StringBuilder sb = new StringBuilder();
        int startIndex = -1;
        long startTime = System.currentTimeMillis();
        log.info("pattern: " + pattern);
        boolean isThrow = false;
        try {
            if (checkInput()) {
                char[] chs = new char[defaultCharBufferSize];
                int t = buf.read(chs);

                while (t != -1) {
                    if (isDebug) {
                        System.out.print(chs);
                    }
                    sb.append(chs, 0, t);
//                    if ((sb.indexOf("ORDERED") >= 0) && !isSendCtrlD) {
//                        log.info("Lenh Order is break");
//                        break;
//                    }
                    if (cmd == null || cmd.trim().isEmpty() || "PG".equalsIgnoreCase(nodeType)
                            || (sb.indexOf("ORDERED") >= 0)) {
                        startIndex = 0;

                    } else {
                        if (startIndex < 0) {
                            startIndex = sb.toString().toUpperCase().indexOf(cmd.toUpperCase());
                            if (startIndex >= 0) {
                                sb = new StringBuilder(sb.toString().substring(startIndex));
                            }
                        }
                    }
                    if (checkRegex(sb.toString(), pattern) && startIndex >= 0) {
                        if (cmd != null) {
                            String content = sb.toString().replace(" [1D", "");

                            if (content.equals(cmd)) {
                                sb = new StringBuilder();
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    t = buf.read(chs);
                    if (System.currentTimeMillis() - startTime > timeOut) {
                        log.info("System.currentTimeMillis() - startTime" + (System.currentTimeMillis() - startTime));
                        log.info("timeOut" + timeOut);
                        sendNoWait("\u0003");
                        throw new Exception("Time out for waitting promt: " + pattern);
                    }
                }
            }
        } catch (Exception e) {
            log.error("startIndex: " + startIndex);
            log.error(sb.toString());
            if (!checkRegex(sb.toString(), pattern)) {
                throw e;
            }
        } finally {
            log.info("finally sb: " + sb);
            if (checkSendCtrlD(sb.toString()) || isSendCtrlD) {
                log.info("SEND CTRL + D");
                if ("HLR".equalsIgnoreCase(this.nodeType) || "MSC".equalsIgnoreCase(this.nodeType)) {
                    isSendCtrlD = true;
                }
                if (isSendCtrlD && checkRegex(sb.toString(), getPatternOrderCmd())) {
                    log.info("Gap ket thuc lenh roi");
                    isSendCtrlD = false;
                    this.currentOutStr = sb.toString();
                } else {
                    log.info("Sleep in 3s");
                    Thread.sleep(3000);
                    long remainTime = timeOut - (System.currentTimeMillis() - startTime);

                    if (remainTime < 0) {
                        log.info("Time out for waitting promt: " + pattern);
                        isThrow = true;
                    }
                    if (!isThrow) {
                        if (!"RNC".equals(nodeType.toUpperCase().trim()) && !"MGW".equals(nodeType.toUpperCase().trim())) {
                            sendNoWait("\u0004\r\n");
                        } else {
                            sendNoWait("\u0004");
                        }
                        this.currentOutStr = readUntilBelongRegex("\r\n", pattern, (int) remainTime);
                    }
                }
            } else {
                if ("PG".equalsIgnoreCase(nodeType) && cmd != null) {
                    this.currentOutStr = cmd + "\n" + sb.toString();
                } else {
                    this.currentOutStr = sb.toString();
                }
            }

        }
        if (isThrow) {
            throw new Exception("Time out for waitting promt: " + pattern);
        }

        return this.currentOutStr;
    }

    private boolean checkSendCtrlD(String content) {
        if (content == null) {
            return false;
        }
        if (!content.contains("\n")) {
            return false;
        }
        content = content.substring(content.indexOf("\n"));
        if (content.lastIndexOf("\n") > 0) {
            content = content.substring(0, content.lastIndexOf("\n"));
        }
        return "ORDERED".equals(content.trim());
    }

    @Override
    public void write(String value) throws Exception {
        try {
            out.println(value);
            out.flush();

//            if (value != null && value.trim().equals(";")) {
//                if (!"RNC".equals(nodeType.toUpperCase().trim()) && !"MGW".equals(nodeType.toUpperCase().trim())) {
//                    out.println("\u0004" + "\r\n");
//                } else {
//                    out.println("\u0004");
//                }
//                log.info("sleep in 3s");
//                Thread.sleep(3000l);
//            }
            if (isDebug) {
                System.out.println(value);
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
