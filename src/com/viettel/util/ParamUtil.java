/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

import com.viettel.model.Customer;
import com.viettel.model.CustomerNode;
import com.viettel.model.Node;
import com.viettel.passprotector.PassProtector;
import com.viettel.persistence.CustomerServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author hienhv4
 */
public class ParamUtil {

    private static final Logger logger = LogManager.getLogger(ParamUtil.class);
    private static final int TIME_OUT = 15000;
    public static final int TYPE_NORMAL = 1;
    public static final int TYPE_CASCADE = 2;
    public static final int SUB_RING = 3;

    public static enum PARAMCODE {

        CRC("CRC"),
        DESCRIPTION_FOR_THIS_INTERFACE("Description for this interface"),
        DUPLEX("Duplex"),
        GIAN("GIANT"),
        INTERFACE("Interface ID"),
        INTERFACE_DOWNLINK_AGG_BACKUP("Interface downlink AGG Backup"),//ok
        INTERFACE_DOWNLINK_AGG_MASTER("Interface downlink AGG Master"),//ok
        IP_CONNECT_ENODEB_AGG_BACKUP_MAIN("IPv4 address eNodeB in VE AGG Backup Main"),
        IP_CONNECT_ENODEB_AGG_BACKUP_PROTECT("IPv4 address eNodeB in VE AGG Backup Protect"),
        IP_CONNECT_ENODEB_AGG_MASTER_MAIN("IPv4 address eNodeB in VE AGG Master Main"),
        IP_CONNECT_ENODEB_AGG_MASTER_PROTECT("IPv4 address eNodeB in VE AGG Master Protect"),
        IP_CONNECT_SRT_MAIN("IPv4 address eNodeB in SRT Main"),
        IP_CONNECT_SRT_PROTECT("IPv4 address eNodeB in SRT Protect"),
        IP_OAM("Network of OAM"),
        IP_OAM_AGG_BACKUP("IP OAM AGG Backup"),//ok
        IP_OAM_AGG_MASTER("IP OAM AGG Master"), //ok"
        IP_OAM_ENODEB("IPv4 address of OAM"),
        IP_OAM_GATEWAY("IP OAM gateway"),
        IP_SERVICE("Network of Service"),
        IP_SERVICE_AGG_BACKUP("IP Service AGG Backup"),
        IP_SERVICE_AGG_MASTER("IP Service AGG Master"),
        IP_SERVICE_ENODEB("IPv4 address of Service"),
        IP_SERVICE_GATEWAY("IP Service gateway"),
        IP_VIRTUAL_CONNECT_ENODEB_MAIN("IP Service gateway Main"),
        IP_VIRTUAL_CONNECT_ENODEB_PROTECT("IP Service gateway Protect"),
        LOOPBACK_AGG_BACKUP("Loopback AGG Backup"),///ok
        LOOPBACK_AGG_MASTER("Loopback AGG Master"),//ok
        MAC("MAC"),
        NEGOTIATION("Negotiation"),
        ROUTE_DISTINGUISHER("Route distinguisher"),
        RX("Rx Power"),
        SPEED("Speed"),
        STATE("State"),
        TX("Tx Power"),
        VE_ID("VE ID"),
        VE_ID_OAM("VE ID OAM"),//ok
        VE_ID_OAM_BACKUP("VE ID OAM BK"),
        VE_ID_SERVICE("VE ID Service"),
        VE_ID_SERVICE_BACKUP("VE ID Service BK"),
        VENDOR_NAME("Vendor Name"),
        VENDOR_PN("Vendor PN"),
        VLAN_CONTROL("VLAN ID of RRPP domain 1"),
        VLAN_ID("VLAN ID"),
        VLAN_OAM("VLAN ID OAM"),
        VLAN_OAM_BACKUP("VLAN OAM backup"),
        VLAN_OAM_SUBRING("VLAN OAM subring"),
        VLAN_OAM_SUBRING_BACKUP("VLAN OAM subring backup"),
        VLAN_SERVICE("VLAN ID Service"),
        VLAN_SERVICE_BACKUP("VLAN Service backup"),
        VLAN_SERVICE_SUBRING("VLAN Service subring"),
        VLAN_SERVICE_SUBRING_BACKUP("VLAN Service subring backup"),
        VRF_NAME("VRF name"),
        VRRP_ID("Virtual router identifier"),
        VSI_ID("VSI ID"),
        VSI_ID_OAM("VSI ID OAM"),
        VSI_ID_OAM_BACKUP("VSI ID OAM BK"),
        VSI_ID_SERVICE("VSI ID Service"),
        VSI_ID_SERVICE_BACKUP("VSI ID Service BK"),
        VSI_NAME("VSI Name"),
        VSI_NAME_OAM_BACKUP("VSI Name Backup OAM"),
        VSI_NAME_SERVICE_BACKUP("VSI Name Service BK"),
        VSI_NAME_OAM("VSI Name OAM"),//ok
        VSI_NAME_SERVICE("VSI Name OAM BK"),//ok

        INTERFACE_UPLINK("Interface uplink"),
        COMMUNITY_PROVINCE("community province"),
        INTERFACE_UPLINK_1_AGG("interface uplink 1 AGG"),
        INTERFACE_UPLINK_2_AGG("interface uplink 2 AGG"),
        INTERFACE_UPLINK_1_SRT("Interface uplink 1 SRT"),
        INTERFACE_UPLINK_2_SRT("Interface uplink 2 SRT"),;
        public String value;

        PARAMCODE(String v) {
            setValue(v);
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static enum PARAMCODE_ENODEB {

        INTERFACE("Interface ID"),
        DESCRIPTION_FOR_THIS_INTERFACE("Description for this interface"),
        IP_SERVICE_ENODEB("IPv4 address of Service"),
        IP_OAM_ENODEB("IPv4 address of OAM"),
        IP_SERVICE("Network of Service"),
        IP_OAM("Network of OAM"),
        VLAN_SERVICE("VLAN ID Service"),
        VLAN_OAM("VLAN ID OAM"),
        IP_CONNECT_ENODEB_AGG_MASTER_MAIN("IP d?u n?i eNodeB tr�n Agg Master hu?ng Main"),
        IP_CONNECT_ENODEB_AGG_MASTER_PROTECT("IP d?u n?i eNodeB tr�n Agg Master hu?ng Protect"),
        IP_VIRTUAL_CONNECT_ENODEB_MAIN("IP Virtual d?u n?i eNodeB hu?ng Main"),
        IP_VIRTUAL_CONNECT_ENODEB_PROTECT("IP Virtual d?u n?i eNodeB hu?ng Protect"),
        IP_CONNECT_ENODEB_AGG_BACKUP_MAIN("IP d?u n?i eNodeB tr�n Agg Backup hu?ng Main"),
        IP_CONNECT_ENODEB_AGG_BACKUP_PROTECT("IP d?u n?i eNodeB tr�n Agg Backup hu?ng Protect"),
        IP_CONNECT_SRT_MAIN("IP d?u n?i tr�n SRT hu?ng Main"),
        IP_CONNECT_SRT_PROTECT("IP d?u n?i tr�n SRT hu?ng Protect"),
        RX("Rx Power"),
        TX("Tx Power"),;
        private String value;

        PARAMCODE_ENODEB(String v) {
            setValue(v);
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    private static String getValue(String content, String start) {
        int index = content.indexOf(start);
        if (index > -1) {
            if (index == content.length() - 1) {
                return null;
            }
            return content.substring(index + start.length(), content.length()).trim();
        }
        return null;
    }

    private static String getValue(String content, String start, String end) {
        int index = content.indexOf(start);
        if (index > -1) {
            if (index == content.length() - 1) {
                return null;
            }

            String value = content.substring(index + start.length(), content.length()).trim();
            int indexEnd = value.indexOf(end);

            if (indexEnd < 0) {
                return value;
            } else {
                return value.substring(0, indexEnd).trim();
            }
        }
        return null;
    }

    private static boolean checkRegex(String content, String pattern) {
        Pattern p = Pattern.compile(pattern, Pattern.MULTILINE);
        Matcher m;
        m = p.matcher(content);
        if (m.find()) {
            return true;
        }

        return false;
    }

    private static String getVlanCtrlHuaweiS5328(String content) {
        BufferedReader bf = null;
        StringReader sr = null;
        try {
            sr = new StringReader(content);
            bf = new BufferedReader(sr);

            String line;
            while ((line = bf.readLine()) != null) {
                if (line.contains("Control VLAN")) {
                    return getValue(line, "major", " ");
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (sr != null) {
                sr.close();
            }

            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
        return "";
    }

    private static String getVlanCtrlZte5928E(String content, String protectInst) {
        BufferedReader bf = null;
        StringReader sr = null;
        try {
            sr = new StringReader(content);
            bf = new BufferedReader(sr);

            String line;
            while ((line = bf.readLine()) != null) {
                if (line.contains("ctrl-vlan:")) {
                    String protectInstLocal = getValue(line, "protect-instance:", " ");
                    if (protectInst.equals(protectInstLocal)) {
                        return getValue(line, "ctrl-vlan:", " ");
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (sr != null) {
                sr.close();
            }

            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
        return "";
    }

    private static String getValueTelnet(TelnetClientUtil telnetClient, String command,
            int timeout, String cmdEnd, String pagingCmd, String pagingRegex) throws Exception {

        try {
            String content;
            if (pagingRegex == null || pagingRegex.trim().isEmpty()) {
                content = telnetClient.sendWait(command, cmdEnd, true, timeout);
            } else {
                if (pagingCmd == null) {
                    pagingCmd = " ";
                }
                content = telnetClient.sendWaitHasMore(command, cmdEnd, pagingRegex, pagingCmd, timeout);
            }

            if (content == null) {
                return null;
            }

            String firstLine = content.contains("\n") ? content.substring(0, content.indexOf("\n")) : null;
            if (firstLine != null) {
                content = content.substring(content.indexOf("\n"));
            }
            System.out.println("firstLine: " + firstLine);

            if (content.lastIndexOf("\n") > 0) {
                content = content.substring(0, content.lastIndexOf("\n"));
            } else if (checkRegex(content, isNullOrEmpty(cmdEnd) ? telnetClient.getPrompt() : cmdEnd)) {
                content = "";
            }

            return content.replaceAll("\\b", "")
                    .replaceAll("\u001B7+", "").replaceAll("[\\x00\\x08\\x0B\\x0C\\x0E-\\x1F]", "")
                    .replace("[42D", "")
                    .trim();
        } catch (Exception ex) {
            throw ex;
        }
    }

    private static TelnetClientUtil login(String username, String pass, String ip, int port, String prompt,
            String terminator, String cmdExit, Long totalTimeout) throws Exception {
        TelnetClientUtil telnetClient = new TelnetClientUtil(ip, port, null);
        telnetClient.setShellPromt(prompt);
        telnetClient.setCmdExit(cmdExit);
        telnetClient.setTerminalType(terminator);
        telnetClient.setTimeoutDefault(totalTimeout.intValue());

        try {
            telnetClient.connect(username, pass);
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
            if (!"CONN_TIMEOUT".equals(ex.getMessage())) {
                throw new Exception("LOGIN_FAIL");
            } else {
                throw new Exception("CONN_TIMEOUT");
            }
        }

        return telnetClient;
    }

    private static boolean checkVrrpId(String content) {
        if (content == null) {
            return false;
        }

        if (content.contains("The VRRP does not exist")) {
            return true;
        }

        BufferedReader bf = null;
        StringReader sr = null;
        try {
            sr = new StringReader(content);
            bf = new BufferedReader(sr);

            String line;
            boolean isStart = false;
            int count = 0;
            while ((line = bf.readLine()) != null) {
                if (line.contains("------------")) {
                    isStart = true;
                    continue;
                }

                if (isStart) {
                    count++;
                }
            }
            return count < 16;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (sr != null) {
                sr.close();
            }

            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
        return false;
    }

    private static boolean checkVlanId(String content) {
        if (content == null || content.contains("Wrong parameter found at")) {
            return true;
        }

        return false;
    }

    private static boolean checkVsiId(String content) {
        if (isNullOrEmpty(content)) {
            return true;
        }

        return false;
    }

    private static boolean checkVlanIdL3CiscoAsr901(String content) {
        if (isNullOrEmpty(content)) {
            return true;
        }

        return false;
    }

    private static boolean checkVlanIdL3HuaweiATN910B(String content) {
        if (isNullOrEmpty(content)) {
            return true;
        }

        if (content.trim().contains("\n")) {
            return true;
        }

        return false;
    }

    private static boolean checkVsiName(String content) {
        if (isNullOrEmpty(content)) {
            return true;
        }

        return false;
    }

    private static boolean checkVeId(String content) {
        if (content == null || content.contains("Wrong parameter found at")) {
            return true;
        }

        return false;
    }

    private static boolean checkRdRt(String content) {
        if (isNullOrEmpty(content)) {
            return true;
        }

        return false;
    }

    private static String getIpLoopback(String ipAGG) {
        try {
            if (isNullOrEmpty(ipAGG)) {
                return "";
            }

            String[] arr = ipAGG.trim().split("\\.");
            if (arr.length == 4) {
                int octet3 = Integer.parseInt(arr[2]);

                if (octet3 >= 0) {
                    return arr[0] + "." + arr[1] + "." + (octet3 - 32) + "." + arr[3];
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getVlanControl(TelnetClientUtil telnetClient, String vendor, String version, String domain) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {
                String content = getValueTelnet(telnetClient, "display rrpp verbose domain " + domain, TIME_OUT,
                        null, null, "---- More ----");

                return getVlanCtrlHuaweiS5328(content);
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {
                String content = getValueTelnet(telnetClient, "show zesr brief", TIME_OUT,
                        null, null, "--More--");

                return getVlanCtrlZte5928E(content, domain);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getVrrpId(TelnetClientUtil telnetClientMT, TelnetClientUtil telnetClientBK,
            int start, Integer end) {
        try {
            do {
                String cmdRun = "display vrrp " + start + " brief";

                //Check AGG Master
                String content = getValueTelnet(telnetClientMT, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                if (checkVrrpId(content)) {
                    //Check AGG Backup

                    content = getValueTelnet(telnetClientBK, cmdRun, TIME_OUT,
                            null, null, "---- More ----");
                    if (checkVrrpId(content)) {
                        return start + "";
                    }
                }

                start++;
                if (end != null && start > end) {
                    break;
                }
            } while (true);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getVlanIdL2(TelnetClientUtil telnetClientMT, TelnetClientUtil telnetClientBK, String interfaceMT,
            String interfaceBK) {
        try {
            int start = 2451;

            do {
                String cmdRun = "display current-configuration interface " + interfaceMT + "." + start;
                //Check AGG Master
                String content = getValueTelnet(telnetClientMT, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                if (checkVlanId(content)) {
                    //Check AGG Backup
                    cmdRun = "display current-configuration interface " + interfaceBK + "." + start;

                    content = getValueTelnet(telnetClientBK, cmdRun, TIME_OUT,
                            null, null, "---- More ----");
                    if (checkVlanId(content)) {
                        return start + "";
                    } else {
                        start++;
                    }
                } else {
                    start++;
                }
                if (start > 2500) {
                    return "";
                }
            } while (true);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getVlanIdL3(TelnetClientUtil telnetClient, String interfaceSrtDown,
            String vendor, String version) {
        try {
            if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                int start = 3501, end = 4000;

                do {
                    String cmdRun = "show interfaces description | include Vl" + start;

                    //Check SRT
                    String content = getValueTelnet(telnetClient, cmdRun, TIME_OUT,
                            null, null, "--More--");

                    if (checkVlanIdL3CiscoAsr901(content)) {
                        return start + "";
                    }

                    start++;
                } while (start <= end);
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                int start = 3501, end = 4000;

                do {
                    String cmdRun = "display interface description | include " + interfaceSrtDown + "." + start;

                    //Check SRT
                    String content = getValueTelnet(telnetClient, cmdRun, TIME_OUT,
                            null, null, "---- More ----");

                    if (checkVlanIdL3HuaweiATN910B(content)) {
                        return start + "";
                    }

                    start++;
                } while (start <= end);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static int getVsiId(TelnetClientUtil telnetClientMT, TelnetClientUtil telnetClientBK,
            int start, int end, int incNum) {
        try {
            do {
                String cmdRun = "display vsi verbose | include " + start;

                //Check AGG Master
                String content = getValueTelnet(telnetClientMT, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                if (checkVsiId(content)) {
                    //Check AGG Backup

                    content = getValueTelnet(telnetClientBK, cmdRun, TIME_OUT,
                            null, null, "---- More ----");
                    if (checkVsiId(content)) {
                        return start;
                    }
                }

                start += incNum;
            } while (start <= end);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return -1;
    }

    private static int getVeId(TelnetClientUtil telnetClientMT, TelnetClientUtil telnetClientBK,
            int start, int end, int incNum) {
        try {
            do {
                String cmdRun = "display current-configuration interface Virtual-Ethernet 1/0/0." + start;

                //Check AGG Master
                String content = getValueTelnet(telnetClientMT, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                if (checkVeId(content)) {
                    cmdRun = "display current-configuration interface Virtual-Ethernet 1/0/1." + start;
                    content = getValueTelnet(telnetClientMT, cmdRun, TIME_OUT,
                            null, null, "---- More ----");

                    if (checkVeId(content)) {
                        //Check AGG Backup
                        cmdRun = "display current-configuration interface Virtual-Ethernet 1/0/0." + start;
                        content = getValueTelnet(telnetClientBK, cmdRun, TIME_OUT,
                                null, null, "---- More ----");
                        if (checkVeId(content)) {
                            //Check AGG Backup
                            cmdRun = "display current-configuration interface Virtual-Ethernet 1/0/1." + start;
                            content = getValueTelnet(telnetClientBK, cmdRun, TIME_OUT,
                                    null, null, "---- More ----");

                            if (checkVeId(content)) {
                                return start;
                            }
                        }
                    }
                }

                start += incNum;
            } while (start <= end);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return -1;
    }

    private static String getVsiName(TelnetClientUtil telnetClientMT, TelnetClientUtil telnetClientBK,
            String vsiNameStart) {
        try {
            int start = 0;

            do {
                String vsiName = vsiNameStart + (start <= 0 ? "" : "_" + start);
                String cmdRun = "display vsi name " + vsiName;

                //Check AGG Master
                String content = getValueTelnet(telnetClientMT, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                if (checkVsiName(content)) {
                    //Check AGG Backup

                    content = getValueTelnet(telnetClientBK, cmdRun, TIME_OUT,
                            null, null, "---- More ----");
                    if (checkVsiName(content)) {
                        return vsiName;
                    }
                }

                start++;
            } while (true);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getRdRt(TelnetClientUtil telnetClientCV, int start) {
        try {
            int end = 65565;
            do {
                String cmdRun = "display bgp vpnv4 route-distinguisher 7552:" + start + " routing-table";
                //Check AGG Master
                String content = getValueTelnet(telnetClientCV, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                if (checkRdRt(content)) {
                    return "7552:" + start;
                }

                start++;
            } while (start <= end);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static int getRdRTMax(List<Customer> lstClient) {
        try {
            int maxValue = 0;
            for (Customer client : lstClient) {
                String rdrt = client.getRd();
                if (!isNullOrEmpty(rdrt)) {
                    try {
                        int temp = Integer.parseInt(rdrt.replace("7552:", "").trim());
                        maxValue = (maxValue >= temp ? maxValue : temp);
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                }
            }
            return maxValue;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return 0;
    }

    /**
     * Ham lay tham so cho mang Metro L3
     *
     * @param srt: Site_router khach hang dang dau noi den
     * @param areaCode: Ma khu vuc
     * @param customerNode: Thong tin khach hang
     * @return Map<String, String>
     */
    public static Map<String, String> getParamL3(Node srt, String areaCode, CustomerNode customerNode) {
        Map<String, String> param = new HashMap<>();
        TelnetClientUtil telnetClientSrt = null;
        TelnetClientUtil telnetClientCV = null;

        //Lay thong tin vrf name
        param.put("VRF name", customerNode.getCustomer().getVrfName());

        try {
            String username = getUsername(areaCode);
            String password = getPassword(areaCode);
            telnetClientSrt = login(username, password, srt.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);

            String vlanId = getVlanIdL3(telnetClientSrt, customerNode.getInterfaceId(), srt.getVendor().getVendorName(),
                    srt.getVersion().getVersionName());
            param.put("VLAN ID", vlanId);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientSrt != null) {
                try {
                    telnetClientSrt.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        // Lay thong tin Route distinguisher
        if (isNullOrEmpty(customerNode.getCustomer().getRd())) {
            try {
                telnetClientCV = login(getUsername("KV1"), getPassword("KV1"), "10.249.160.2",
                        23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
                List<Customer> lstClient = new CustomerServiceImpl().findList();
                String rdrt = getRdRt(telnetClientCV, getRdRTMax(lstClient) + 1);
                param.put("Route distinguisher", rdrt);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            } finally {
                if (telnetClientCV != null) {
                    try {
                        telnetClientCV.disConnect();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                }
            }
        } else {
            param.put("Route distinguisher", customerNode.getCustomer().getRd());
        }

        return param;
    }

    /**
     * Ham lay tham so cho mang Metro L3
     *
     * @param aggMaster: AGG master
     * @param aggBackup: AGG backup
     * @param srt: Site_router khach hang dang dau noi den
     * @param areaCode: Ma khu vuc
     * @param typeRing: Loai ring (TYPE_NORMAL: ring thuong, TYPE_CASCADE: ring
     * cascade)
     * @param interfaceDownlinkMaster: Interface downlink AGG Master
     * @param interfaceDownLinkBackup: Interface downlink AGG Backup
     * @param customerNode: Thong tin khach hang
     * @return Map<String, String>
     * @throws java.lang.Exception
     */
    public static Map<String, String> getParamL2(Node aggMaster, Node aggBackup, Node srt, String areaCode,
            int typeRing, String interfaceDownlinkMaster, String interfaceDownLinkBackup, CustomerNode customerNode) throws Exception {
        Map<String, String> param = new HashMap<>();
        TelnetClientUtil telnetClientSrt = null;
        TelnetClientUtil telnetClientAggMT = null;
        TelnetClientUtil telnetClientAggBK = null;
        TelnetClientUtil telnetClientCV = null;

        //Lay thong tin vrf name
        param.put("VRF name", customerNode.getCustomer().getVrfName());

        // Lay thong tin Route distinguisher
        if (isNullOrEmpty(customerNode.getCustomer().getRd())) {
            try {
                telnetClientCV = login(getUsername("KV1"), getPassword("KV1"), "10.249.160.2",
                        23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
                List<Customer> lstClient = new CustomerServiceImpl().findList();
                String rdrt = getRdRt(telnetClientCV, getRdRTMax(lstClient) + 1);
                param.put("Route distinguisher", rdrt);
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            } finally {
                if (telnetClientCV != null) {
                    try {
                        telnetClientCV.disConnect();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                }
            }
        } else {
            param.put("Route distinguisher", customerNode.getCustomer().getRd());
        }

        // Lay thong tin Loopback AGG Master
        param.put("Loopback AGG Master", getIpLoopback(aggMaster.getNodeIp()));
        param.put("Interface downlink AGG Master", interfaceDownlinkMaster);

        // Lay thong tin Loopback AGG Backup
        param.put("Loopback AGG Backup", getIpLoopback(aggBackup.getNodeIp()));
        param.put("Interface downlink AGG Backup", interfaceDownLinkBackup);

        String username = getUsername(areaCode);
        String password = getPassword(areaCode);

        // Lay thong tin Vlan control
        try {
            telnetClientSrt = login(username, password, srt.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
            param.put("Vlan control", getVlanControl(telnetClientSrt, srt.getVendor().getVendorName(),
                    srt.getVersion().getVersionName(), "2"));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientSrt != null) {
                try {
                    telnetClientSrt.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        try {
            telnetClientAggMT = login(username, password, aggMaster.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
            telnetClientAggBK = login(username, password, aggBackup.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);

            // Lay thong tin VRRP ID
            int start = 0, end = 0;
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 79;
                    break;
                case TYPE_CASCADE:
                    start = 179;
                    break;
            }
            param.put("VRRP ID", getVrrpId(telnetClientAggMT, telnetClientAggBK, start, null));

            // Lay thong tin VLAN ID
            param.put("VLAN ID", getVlanIdL2(telnetClientAggMT, telnetClientAggBK, interfaceDownlinkMaster, interfaceDownLinkBackup));

            // Lay thong tin VSI ID
            switch (areaCode.toUpperCase()) {
                case "KV1":
                    start = 1605001;
                    end = 1617200;
                    break;
                case "KV2":
                    start = 1592401;
                    end = 1604600;
                    break;
                case "KV3":
                    start = 1606401;
                    end = 1618600;
                    break;
            }
            int vsiId = getVsiId(telnetClientAggMT, telnetClientAggBK, start, end, 1);
            if (vsiId > 0) {
                param.put("VSI ID", vsiId + "");
            }

            // Lay thong tin VSI NAME
            param.put("VSI Name", getVsiName(telnetClientAggMT, telnetClientAggBK,
                    customerNode.getCustomer().getVrfName() + "_" + srt.getNodeCode()));

            // Lay thong tin VE ID
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 861;
                    end = 1360;
                    break;
                case TYPE_CASCADE:
                    start = 1861;
                    end = 2360;
                    break;
            }
            int veId = getVeId(telnetClientAggMT, telnetClientAggBK, start, end, 1);
            if (veId > 0) {
                param.put("VE ID", veId + "");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientAggMT != null) {
                try {
                    telnetClientAggMT.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            if (telnetClientAggBK != null) {
                try {
                    telnetClientAggBK.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        return param;
    }

    private static String getUsername(String areaCode) throws Exception {
        switch (areaCode.toUpperCase()) {
            case "KV1":
                return PassProtector.decrypt(MessageUtil.getResourceBundleConfig("username_kv1"), "ipchange");
            case "KV2":
                return PassProtector.decrypt(MessageUtil.getResourceBundleConfig("username_kv2"), "ipchange");
            case "KV3":
                return PassProtector.decrypt(MessageUtil.getResourceBundleConfig("username_kv3"), "ipchange");
            default:
                return "";
        }
    }

    private static String getPassword(String areaCode) throws Exception {
        switch (areaCode.toUpperCase()) {
            case "KV1":
                return PassProtector.decrypt(MessageUtil.getResourceBundleConfig("password_kv1"), "ipchange");
            case "KV2":
                return PassProtector.decrypt(MessageUtil.getResourceBundleConfig("password_kv2"), "ipchange");
            case "KV3":
                return PassProtector.decrypt(MessageUtil.getResourceBundleConfig("password_kv3"), "ipchange");
            default:
                return "";
        }
    }

    //---------------------------Cac tham so 4G_Start
    public static List<String> getValueRowColumnIndex(String content, int row, int column,
            int countRow, String rowHeader, String rowHeaderOperator, String rowFooter, String rowFooterOperator,
            String splitColumnChar, String regex) throws Exception {
        BufferedReader bf = null;
        StringReader sr = null;
        try {
            sr = new StringReader(content);
            bf = new BufferedReader(sr);

            String line = "";
            int i = 1;
            int count = 0;
            List<String> lstValue = new ArrayList<>();
            boolean isHeader = false;
            while ((line = bf.readLine()) != null) {
                if (rowHeader != null && !rowHeader.trim().isEmpty()) {
                    switch (rowHeaderOperator) {
                        case "CONTAIN":
                            if (line.trim().contains(rowHeader)) {
                                isHeader = true;
                            }
                            break;
                        case "START WITH":
                            if (line.trim().startsWith(rowHeader)) {
                                isHeader = true;
                            }
                            break;
                        case "END WITH":
                            if (line.trim().endsWith(rowHeader)) {
                                isHeader = true;
                            }
                            break;
                    }
                    if (isHeader) {
                        break;
                    }
                } else {
                    i = 1;
                    break;
                }
            }

            do {
                if (line == null) {
                    break;
                }
                boolean isFooter = false;
                if (!isNullOrEmpty(rowFooter)) {
                    switch (rowFooterOperator) {
                        case "CONTAIN":
                            if (line.trim().contains(rowFooter)) {
                                isFooter = true;
                            }
                            break;
                        case "START WITH":
                            if (line.trim().startsWith(rowFooter)) {
                                isFooter = true;
                            }
                            break;
                        case "END WITH":
                            if (line.trim().endsWith(rowFooter)) {
                                isFooter = true;
                            }
                            break;
                    }
                    if (isFooter) {
                        break;
                    }
                }
                if (row <= i) {
                    if (countRow > 0) {
                        count++;
                        if (count <= countRow) {
                            if (isNullOrEmpty(splitColumnChar)) {
                                String vl = getValueRegex(line.trim(), regex);

                                if (!isNullOrEmpty(vl) && !lstValue.contains(vl)) {
                                    lstValue.add(vl.trim());
                                }
                            } else {
                                String[] arr = line.trim().split(splitColumnChar);
                                if (arr != null && arr.length >= column) {
                                    String vl = getValueRegex(arr[column - 1], regex);

                                    if (!isNullOrEmpty(vl) && !lstValue.contains(vl)) {
                                        lstValue.add(vl.trim());
                                    }
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        if (isNullOrEmpty(splitColumnChar)) {
                            String vl = getValueRegex(line, regex);

                            if (!isNullOrEmpty(vl) && !lstValue.contains(vl)) {
                                lstValue.add(vl.trim());
                            }
                        } else {
                            String[] arr = line.trim().split(splitColumnChar);
                            if (arr != null && arr.length >= column) {
                                String vl = getValueRegex(arr[column - 1], regex);

                                if (!isNullOrEmpty(vl) && !lstValue.contains(vl)) {
                                    lstValue.add(vl.trim());
                                }
                            }
                        }
                    }
                }
                i++;
            } while ((line = bf.readLine()) != null);

            return lstValue;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (sr != null) {
                sr.close();
            }

            if (bf != null) {
                try {
                    bf.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
    }

    private static String getValueRegex(String content, String regex) {
        String value = null;
        try {
            if (isNullOrEmpty(regex) || isNullOrEmpty(content)) {
                return content;
            }
            Pattern patRegex = Pattern.compile(regex, Pattern.MULTILINE | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
            Matcher matcher = patRegex.matcher(content.trim());
            if (matcher.find()) {
                value = matcher.group(1);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return value;
    }

    private static boolean checkInterface4G(String portStatus) {
        if (isNullOrEmpty(portStatus)) {
            return false;
        }

        return "administratively down".equals(portStatus.toLowerCase());
    }

    private static int getRingId4G(TelnetClientUtil telnetClientMT, TelnetClientUtil telnetClientBK,
            int start) {
        try {
            do {
                String vsiNameSer = "4G_" + start + "_Service";
                String cmdRun = "display vsi name " + vsiNameSer;

                //Check AGG Master
                String content = getValueTelnet(telnetClientMT, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                if (checkVsiName(content)) {
                    //Check AGG Backup

                    content = getValueTelnet(telnetClientBK, cmdRun, TIME_OUT,
                            null, null, "---- More ----");
                    if (checkVsiName(content)) {
                        String vsiNameOAM = "4G_" + start + "_OAM";
                        String cmdRunOAM = "display vsi name " + vsiNameOAM;

                        //Check AGG Master
                        String content1 = getValueTelnet(telnetClientMT, cmdRunOAM, TIME_OUT,
                                null, null, "---- More ----");
                        if (checkVsiName(content1)) {
                            content1 = getValueTelnet(telnetClientBK, cmdRunOAM, TIME_OUT,
                                    null, null, "---- More ----");
                            if (checkVsiName(content1)) {
                                return start;
                            }
                        }
                    }
                }

                start++;
            } while (true);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return -1;
    }

    private static String getInterface4G(TelnetClientUtil telnetClientSrt, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {
                try {
                    String[] range = new String[]{"0/0/11", "0/0/12", "0/0/13"};

                    for (String inter : range) {
                        String cmdRun = "display interface GigabitEthernet " + inter;
                        String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                                null, null, "---- More ----");

                        String interfaceStatus = getValue(content, "GigabitEthernet" + inter + " current state :", "\n");

                        if (checkInterface4G(interfaceStatus)) {
                            return "GigabitEthernet" + inter;
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
                return "";
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {
                try {
                    String[] range = new String[]{"gei_0/11", "gei_0/12", "gei_0/13"};

                    for (String inter : range) {
                        String cmdRun = "show interface " + inter;
                        String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                                null, null, "--More--");

                        String interfaceStatus = getValue(content, inter + " is", ",");

                        if (checkInterface4G(interfaceStatus)) {
                            return inter;
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
                return "";
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                try {
                    String[] range = new String[]{"0/4", "0/5", "0/6"};

                    for (String inter : range) {
                        String cmdRun = "show interfaces gigabitEthernet " + inter;
                        String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                                null, null, "--More--");

                        String interfaceStatus = getValue(content, "GigabitEthernet" + inter + " is", ",");

                        if (checkInterface4G(interfaceStatus)) {
                            return "GigabitEthernet" + inter;
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
                return "";
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                try {
                    String[] range = new String[]{"0/2/21", "0/2/22", "0/2/23"};

                    for (String inter : range) {
                        String cmdRun = "display interface GigabitEthernet " + inter;
                        String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                                null, null, "---- More ----");

                        String interfaceStatus = getValue(content, "GigabitEthernet" + inter + " current state :", "\n");

                        if (checkInterface4G(interfaceStatus)) {
                            return "GigabitEthernet" + inter;
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
                return "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static Map<String, String> getVendor4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        Map<String, String> mapValue = new HashMap<>();
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {
                try {
                    String cmdRun = "display transceiver interface GigabitEthernet " + interf;
                    String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                            null, null, "---- More ----");

                    mapValue.put("Vendor PN", getValue(content, "Vendor PN", "\n").replace(":", ""));
                    mapValue.put("Vendor Name", getValue(content, "Vendor Name", "\n").replace(":", ""));
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {
                BufferedReader bf = null;
                StringReader sr = null;
                try {
                    String cmdRun = "show optical-inform interface " + interf;
                    String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                            null, null, "--More--");

                    sr = new StringReader(content);
                    bf = new BufferedReader(sr);
                    String line;
                    while ((line = bf.readLine()) != null) {
                        if (line.contains(interf)) {
                            String[] strs = line.split("\\s+");
                            if (strs.length >= 5) {
                                mapValue.put("Vendor PN", strs[4]);
                                mapValue.put("Vendor Name", strs[3]);
                            }
                            break;
                        }
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                } finally {
                    if (sr != null) {
                        sr.close();
                    }

                    if (bf != null) {
                        try {
                            bf.close();
                        } catch (IOException ex) {
                            logger.error(ex.getMessage(), ex);
                        }
                    }
                }
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                try {
                    String cmdRun = "show controllers GigabitEthernet " + interf + " | include vendor";
                    String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                            null, null, "--More--");

                    mapValue.put("Vendor PN", getValue(content, "vendor_pn", "\n"));
                    mapValue.put("Vendor Name", getValue(content, "vendor_name", "\n"));
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                try {
                    String cmdRun = "display interface GigabitEthernet " + interf + " | include Vendor";
                    String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                            null, null, "---- More ----");

                    mapValue.put("Vendor PN", getValue(content, "The Vendor PN is", "\n"));
                    mapValue.put("Vendor Name", getValue(content, "The Vendor Name is", "\n"));
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return mapValue;
    }

    private static String getMac4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {

                String cmdRun = "display mac-address dynamic GigabitEthernet " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");
                List<String> values = getValueRowColumnIndex(content, 4, 1, 0, "PEVLAN CEVLAN Port", "CONTAIN",
                        null, null, "\\s+", null);

                return values.size() > 0 ? values.get(0) : "";
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {

                String cmdRun = "show mac interface " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");
                List<String> values = getValueRowColumnIndex(content, 2, 1, 0, "-------------", "CONTAIN",
                        null, null, "\\s+", null);

                return values.size() > 0 ? values.get(0) : "";
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {

                String cmdRun = "show mac-address-table interface gigabitEthernet " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");
                List<String> values = getValueRowColumnIndex(content, 3, 2, 0, "Vlan", "CONTAIN",
                        null, null, "\\s+", null);

                return values.size() > 0 ? values.get(0) : "";
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {

                String cmdRun = "display arp vpn-instance VRF_4G_Service | include " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");
                List<String> values = getValueRowColumnIndex(content, 1, 2, 0, "D-0", "CONTAIN",
                        null, null, "\\s+", null);

                return values.size() > 0 ? values.get(0) : "";
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getState4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "GigabitEthernet" + interf + " current state :", "\n");
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {
                String cmdRun = "show interface " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                return getValue(content, interf + " is", ",");
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                String cmdRun = "show interfaces gigabitEthernet " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                return getValue(content, "GigabitEthernet" + interf + " is", ",");
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "GigabitEthernet" + interf + " current state :", "\n");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static Map<String, String> getRxTx4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        Map<String, String> mapValue = new HashMap<>();
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {

                String cmdRun = "display transceiver interface GigabitEthernet " + interf + " verbose";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                mapValue.put(PARAMCODE_ENODEB.RX.value, getValue(content, "RX Power(dBM)", "\n").replace(":", ""));
                mapValue.put(PARAMCODE_ENODEB.TX.value, getValue(content, "TX Power(dBM)", "\n").replace(":", ""));
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {

                String cmdRun = "show optical-inform details rx-power interface " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");
                List<String> values = getValueRowColumnIndex(content, 1, 2, 0, interf, "CONTAIN",
                        null, null, "\\s+", null);

                mapValue.put(PARAMCODE_ENODEB.RX.value, values.size() > 0 ? values.get(0) : "");

                cmdRun = "show optical-inform details tx-power interface " + interf;
                content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");
                values = getValueRowColumnIndex(content, 1, 2, 0, interf, "CONTAIN",
                        null, null, "\\s+", null);

                mapValue.put(PARAMCODE_ENODEB.TX.value, values.size() > 0 ? values.get(0) : "");
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                String cmdRun = "show interfaces GigabitEthernet " + interf + " transceiver";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                List<String> values = getValueRowColumnIndex(content, 1, 1, 0, "Gi" + interf, "CONTAIN",
                        null, null, null, null);

                if (values.size() > 0) {
                    String line = values.get(0);
                    String[] strs = line.split("\\s+");

                    if (strs.length > 5) {
                        mapValue.put(PARAMCODE_ENODEB.TX.value, strs[4]);
                        mapValue.put(PARAMCODE_ENODEB.RX.value, strs[5]);
                    }
                }
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf + " | include dBm";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                mapValue.put(PARAMCODE_ENODEB.RX.value, getValue(content, "Rx Power:", "dBm"));
                mapValue.put(PARAMCODE_ENODEB.TX.value, getValue(content, "Tx Power:", "dBm"));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return mapValue;
    }

    private static String getCrc4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {

                String cmdRun = "display interface GigabitEthernet " + interf + " | include CRC";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "CRC", ",").replace(":", "").trim();
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {

                String cmdRun = "show interface " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                return getValue(content, "CRC-ERROR", "\n").replace(":", "").trim();
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                String cmdRun = "show interfaces GigabitEthernet " + interf + " | include CRC";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                return getValue(content, ",", "CRC");
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf + " | include CRC";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "CRC:", "packets");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getNegotiation4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {

                String cmdRun = "display interface GigabitEthernet " + interf + " | include Negotiation";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "Negotiation:", "\n");
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {

                String cmdRun = "show running-config interface " + interf + " | include negotiation";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                if (content != null && content.toLowerCase().contains("no negotiation auto")) {
                    return "DISABLE";
                } else {
                    return "ENABLE";
                }
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                String cmdRun = "show running-config interface gigabitEthernet " + interf + " | include negotiation";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                if (content != null && content.toLowerCase().contains("no negotiation auto")) {
                    return "DISABLE";
                } else {
                    return "ENABLE";
                }
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf + " | include negotiation";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "negotiation:", ",");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getSpeed4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {

                String cmdRun = "display interface GigabitEthernet " + interf + " | include Speed";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "Speed", ",").replace(":", "").trim();
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {

                String cmdRun = "show interface brief";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                List<String> values = getValueRowColumnIndex(content, 1, 4, 0, interf, "CONTAIN",
                        null, null, "\\s+", null);

                return values.size() > 0 ? values.get(0) : "";
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                String cmdRun = "show interfaces gigabitEthernet " + interf + " status";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                List<String> values = getValueRowColumnIndex(content, 1, 6, 0, "Gi" + interf, "CONTAIN",
                        null, null, "\\s+", null);

                return values.size() > 0 ? values.get(0) : "";
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf + " | include Current BW";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "Current BW:", ",");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getDuplex4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {

                String cmdRun = "display interface GigabitEthernet " + interf + " | include Duplex";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "Duplex", ",").replace(":", "").trim();
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {

                String cmdRun = "show interface " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                return getValue(content, "Duplex", "\n");
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                String cmdRun = "show interfaces gigabitEthernet " + interf + " status";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                List<String> values = getValueRowColumnIndex(content, 1, 5, 0, "Gi" + interf, "CONTAIN",
                        null, null, "\\s+", null);

                return values.size() > 0 ? values.get(0) : "";
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf + " | include duplex";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, ",", "mode");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    private static String getGiant4G(TelnetClientUtil telnetClientSrt, String interf, String vendor, String version) {
        try {
            if ("HUAWEI".equals(vendor.toUpperCase()) && "S5328".equals(version.toUpperCase())) {

                String cmdRun = "display interface GigabitEthernet " + interf + " | include Giants";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "Giants", "\n").replace(":", "").trim();
            } else if ("ZTE".equals(vendor.toUpperCase()) && "5928E".equals(version.toUpperCase())) {

                String cmdRun = "show interface " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                return getValue(content, "Dropped", "Fragments").replace(":", "").trim();
            } else if ("CISCO".equals(vendor.toUpperCase()) && "ASR901-6CZ-FT".equals(version.toUpperCase())) {
                String cmdRun = "show interfaces gigabitEthernet " + interf;
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "--More--");

                return getValue(content, ",", "giants");
            } else if ("HUAWEI".equals(vendor.toUpperCase()) && "ATN910B".equals(version.toUpperCase())) {
                String cmdRun = "display interface GigabitEthernet " + interf + " | include Alignment";
                String content = getValueTelnet(telnetClientSrt, cmdRun, TIME_OUT,
                        null, null, "---- More ----");

                return getValue(content, "Alignment:", "packets");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    /**
     * Ham lay tham so cho mang Metro L3
     *
     * @param srt: Site_router khach hang dang dau noi den
     * @param areaCode: Ma khu vuc
     * @return Map<String, String>
     */
    public static Map<String, String> getParamL3For4G(Node srt, String areaCode, String ipService) {
        Map<String, String> param = new HashMap<>();

        //Lay thong tin VLAN
        param.put("VLAN Service", "2636");
        param.put("VLAN OAM", "2637");

        // IP service
        param.put("IP Service", ipService);

        //IP OAM
        String ipOAM = "";
        try {
            if (isNullOrEmpty(ipService)) {
                ipOAM = "";
            }

            String[] arr = ipService.trim().split("\\.");
            if (arr.length == 4) {
                int octet2 = Integer.parseInt(arr[1]);

                if (octet2 >= 0) {
                    ipOAM = arr[0] + "." + (octet2 + 10) + "." + arr[2] + "." + arr[3];
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        param.put("IP OAM", ipOAM);
        TelnetClientUtil telnetClientSrt = null;
        try {
            String interfaceName = null;
            String username = getUsername(areaCode);
            String password = getPassword(areaCode);
            //20170427_HaNV15_Edit_Start_Fix_ATTT
            String user_lab = MessageUtil.getResourceBundleConfig("user_getParamL3For4G");
            String password_lab = MessageUtil.getResourceBundleConfig("password_getParamL3For4G");
            username = user_lab;
            password = password_lab;
            //20170427_HaNV15_Edit_End_Fix_ATTT
            telnetClientSrt = login(username, password, srt.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
            interfaceName = getInterface4G(telnetClientSrt, srt.getVendor().getVendorName(), srt.getVersion().getVersionName());
            param.put(PARAMCODE_ENODEB.INTERFACE.getValue(), interfaceName);
        } catch (Exception e) {
            param.put(PARAMCODE_ENODEB.INTERFACE.getValue(), "TEST");
            logger.error(e.getMessage(), e);
        } finally {
            if (telnetClientSrt != null) {
                try {
                    telnetClientSrt.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        return param;
    }

    /**
     * Ham lay tham so cho mang Metro L2
     *
     * @param aggMaster: AGG master
     * @param aggBackup: AGG backup
     * @param srt: Site_router khach hang dang dau noi den
     * @param areaCode: Ma khu vuc
     * @param typeRing: Loai ring (TYPE_NORMAL: ring thuong, TYPE_CASCADE: ring
     * cascade)
     * @param interfaceDownlinkMaster: Interface downlink AGG Master
     * @param interfaceDownLinkBackup: Interface downlink AGG Backup
     * @param ipService
     * @return Map<String, String>
     * @throws java.lang.Exception
     */
    public static Map<String, String> getParamL2For4G(Node aggMaster, Node aggBackup, Node srt, String areaCode,
            int typeRing, String interfaceDownlinkMaster, String interfaceDownLinkBackup, String ipService) throws Exception {
        Map<String, String> param = new HashMap<>();
        TelnetClientUtil telnetClientSrt = null;
        TelnetClientUtil telnetClientAggMT = null;
        TelnetClientUtil telnetClientAggBK = null;

        // Lay thong tin Loopback AGG Master
        param.put("Loopback AGG Master", getIpLoopback(aggMaster.getNodeIp()));
        param.put("Interface downlink AGG Master", interfaceDownlinkMaster);

        // Lay thong tin Loopback AGG Backup
        param.put("Loopback AGG Backup", getIpLoopback(aggBackup.getNodeIp()));
        param.put("Interface downlink AGG Backup", interfaceDownLinkBackup);

        // Lay thong tin VLAN ID
        param.put("VLAN Service", "2636");
        param.put("VLAN OAM", "2637");

        //Lay thong tin IP
        String ipOAM = "";
        try {
            if (isNullOrEmpty(ipService)) {
                ipOAM = "";
            }

            String[] arr = ipService.trim().split("\\.");
            if (arr.length == 4) {
                int octet2 = Integer.parseInt(arr[1]);

                if (octet2 >= 0) {
                    ipOAM = arr[0] + "." + (octet2 + 10) + "." + arr[2] + "." + arr[3];
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        param.put("IP OAM", ipOAM);
        param.put("IP Service", ipService);
        param.put("IP Service ENodeB", IpUtil.plusIp(ipService, 1));
        param.put("IP Service Gateway", IpUtil.plusIp(ipService, 6));
        param.put("IP OAM ENodeB", IpUtil.plusIp(ipOAM, 1));
        param.put("IP OAM Gateway", IpUtil.plusIp(ipOAM, 6));

        String username = getUsername(areaCode);
        String password = getPassword(areaCode);
        String interfaceName;

        // Lay thong tin Vlan control
        try {
            telnetClientSrt = login(username, password, srt.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
            param.put("Vlan control", getVlanControl(telnetClientSrt, srt.getVendor().getVendorName(),
                    srt.getVersion().getVersionName(), "1"));

            interfaceName = getInterface4G(telnetClientSrt, srt.getVendor().getVendorName(), srt.getVersion().getVersionName());
            param.put(PARAMCODE_ENODEB.INTERFACE.getValue(), interfaceName);

            //getState(srt, param, telnetClientSrt, interfaceName);
        } catch (Exception ex) {
            param.put(PARAMCODE_ENODEB.INTERFACE.getValue(), "TEST");
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientSrt != null) {
                try {
                    telnetClientSrt.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        try {
            telnetClientAggMT = login(username, password, aggMaster.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
            telnetClientAggBK = login(username, password, aggBackup.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);

            // Lay thong tin VRRP ID
            int start = 0, end = 0;
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 21;
                    end = 26;
                    break;
                case TYPE_CASCADE:
                    start = 121;
                    end = 126;
                    break;
            }
            param.put("VRRP ID", getVrrpId(telnetClientAggMT, telnetClientAggBK, start, end));

            // Lay thong tin VSI ID Service
            switch (areaCode.toUpperCase()) {
                case "KV1":
                    start = 2149832;
                    end = 2149871;
                    break;
                case "KV2":
                    start = 2149030;
                    end = 2149069;
                    break;
                case "KV3":
                    start = 2149532;
                    end = 2149571;
                    break;
            }
            int vsiIdSer = getVsiId(telnetClientAggMT, telnetClientAggBK, start, end, 1);
            if (vsiIdSer > 0) {
                param.put("VSI ID Service", vsiIdSer + "");
            }

            // Lay thong tin VSI ID OAM
            switch (areaCode.toUpperCase()) {
                case "KV1":
                    start = 2149872;
                    end = 2149911;
                    break;
                case "KV2":
                    start = 2149070;
                    end = 2149109;
                    break;
                case "KV3":
                    start = 2149572;
                    end = 2149611;
                    break;
            }
            int vsiIdOam = getVsiId(telnetClientAggMT, telnetClientAggBK, start, end, 1);
            if (vsiIdOam > 0) {
                param.put("VSI ID OAM", vsiIdOam + "");
            }

            // Lay thong tin VSI NAME
            int ringId = getRingId4G(telnetClientAggMT, telnetClientAggBK, 1);
            param.put("VSI Name Service", "4G_" + ringId + "_Service");
            param.put("VSI Name OAM", "4G_" + ringId + "_OAM");

            // Lay thong tin VE ID Service
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 2666;
                    end = 2685;
                    break;
                case TYPE_CASCADE:
                    start = 2706;
                    end = 2725;
                    break;
            }
            int veIdSer = getVeId(telnetClientAggMT, telnetClientAggBK, start, end, 1);
            if (veIdSer > 0) {
                param.put("VE ID Service", veIdSer + "");
            }

            // Lay thong tin VE ID OAM
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 2686;
                    end = 2705;
                    break;
                case TYPE_CASCADE:
                    start = 2726;
                    end = 2745;
                    break;
            }
            int veIdOam = getVeId(telnetClientAggMT, telnetClientAggBK, start, end, 1);
            if (veIdOam > 0) {
                param.put("VE ID OAM", veIdOam + "");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientAggMT != null) {
                try {
                    telnetClientAggMT.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            if (telnetClientAggBK != null) {
                try {
                    telnetClientAggBK.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        return param;
    }

    /**
     * Ham lay tham so cho mang Metro L2 Subring
     *
     * @param aggMaster: AGG master
     * @param aggBackup: AGG backup
     * @param srt: Site_router Enodeb dang dau noi den
     * @param srtL2Neighbor: Site_router L2 neighbor voi site router L3
     * @param areaCode: Ma khu vuc
     * @param typeRing: Loai ring (TYPE_NORMAL: ring thuong, TYPE_CASCADE: ring
     * cascade)
     * @param interfaceDownlinkMaster: Interface downlink AGG Master
     * @param interfaceDownLinkBackup: Interface downlink AGG Backup
     * @param ipService
     * @return Map<String, String>
     * @throws java.lang.Exception
     */
    public static Map<String, String> getParamL2SubringFor4G(Node aggMaster, Node aggBackup, Node srt, Node srtL2Neighbor, String areaCode,
            int typeRing, String interfaceDownlinkMaster, String interfaceDownLinkBackup, String ipService) throws Exception {
        Map<String, String> param = new HashMap<>();
        TelnetClientUtil telnetClientSrt = null;
        TelnetClientUtil telnetClientSrtNeighbor = null;
        TelnetClientUtil telnetClientAggMT = null;
        TelnetClientUtil telnetClientAggBK = null;

        // Lay thong tin Loopback AGG Master
        param.put("Loopback AGG Master", getIpLoopback(aggMaster.getNodeIp()));
        param.put("Interface downlink AGG Master", interfaceDownlinkMaster);

        // Lay thong tin Loopback AGG Backup
        param.put("Loopback AGG Backup", getIpLoopback(aggBackup.getNodeIp()));
        param.put("Interface downlink AGG Backup", interfaceDownLinkBackup);

        // Lay thong tin VLAN ID
        param.put("VLAN Service", "2606");
        param.put("VLAN OAM", "2608");

        param.put("VLAN Service backup", "2607");
        param.put("VLAN OAM backup", "2609");

        //Lay thong tin IP
        String ipOAM = "";
        try {
            if (isNullOrEmpty(ipService)) {
                ipOAM = "";
            }

            String[] arr = ipService.trim().split("\\.");
            if (arr.length == 4) {
                int octet2 = Integer.parseInt(arr[1]);

                if (octet2 >= 0) {
                    ipOAM = arr[0] + "." + (octet2 + 10) + "." + arr[2] + "." + arr[3];
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        param.put("IP OAM", ipOAM);
        param.put("IP Service", ipService);
        param.put("IP Service ENodeB", IpUtil.plusIp(ipService, 1));
        param.put("IP Service Gateway", IpUtil.plusIp(ipService, 6));
        param.put("IP OAM ENodeB", IpUtil.plusIp(ipOAM, 1));
        param.put("IP OAM Gateway", IpUtil.plusIp(ipOAM, 6));

        String username = getUsername(areaCode);
        String password = getPassword(areaCode);
        String interfaceName;

        // Lay thong tin Vlan control
        try {
            telnetClientSrtNeighbor = login(username, password, srt.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
            param.put("Vlan control", getVlanControl(telnetClientSrtNeighbor, srt.getVendor().getVendorName(),
                    srt.getVersion().getVersionName(), "1"));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientSrtNeighbor != null) {
                try {
                    telnetClientSrtNeighbor.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        // Lay thong tin interface, thong tin trang thai
        try {
            telnetClientSrt = login(username, password, srt.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);

            interfaceName = getInterface4G(telnetClientSrt, srt.getVendor().getVendorName(), srt.getVersion().getVersionName());
            param.put("Interface", interfaceName);

            if (!isNullOrEmpty(interfaceName)) {
                param.putAll(getVendor4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin MAC
                param.put("MAC", getMac4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin GIAN
                param.put("GIAN", getGiant4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin State
                param.put("State", getState4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin thu phat
                param.putAll(getRxTx4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin CRC
                param.put("CRC", getCrc4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin Negotiation
                param.put("Negotiation", getNegotiation4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin Speed
                param.put("Speed", getSpeed4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

                //Lay thong tin Duplex
                param.put("Duplex", getDuplex4G(telnetClientSrt, interfaceName,
                        srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientSrt != null) {
                try {
                    telnetClientSrt.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        try {
            telnetClientAggMT = login(username, password, aggMaster.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);
            telnetClientAggBK = login(username, password, aggBackup.getNodeIp(),
                    23, "(>|#)", "\r\n", "exit", (long) TIME_OUT);

            // Lay thong tin VRRP ID
            int start = 0, end = 0;
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 27;
                    end = 33;
                    break;
                case TYPE_CASCADE:
                    start = 127;
                    end = 133;
                    break;
            }
            param.put("VRRP ID", getVrrpId(telnetClientAggMT, telnetClientAggBK, start, end));

            // Lay thong tin VSI ID Service, VSI ID Service backup
            switch (areaCode.toUpperCase()) {
                case "KV1":
                    start = 2149912;
                    end = 2149991;
                    break;
                case "KV2":
                    start = 2149110;
                    end = 2149189;
                    break;
                case "KV3":
                    start = 2149612;
                    end = 2149691;
                    break;
            }
            int vsiIdSer = getVsiId(telnetClientAggMT, telnetClientAggBK, start, end, 2);
            if (vsiIdSer > 0) {
                param.put("VSI ID Service", vsiIdSer + "");

                int vsiIdSerBK = getVsiId(telnetClientAggMT, telnetClientAggBK, vsiIdSer + 1, end, 1);
                if (vsiIdSerBK > 0) {
                    param.put("VSI ID Service backup", vsiIdSerBK + "");
                }
            }

            // Lay thong tin VSI ID OAM
            switch (areaCode.toUpperCase()) {
                case "KV1":
                    start = 2149992;
                    end = 2150071;
                    break;
                case "KV2":
                    start = 2149190;
                    end = 2149269;
                    break;
                case "KV3":
                    start = 2149692;
                    end = 2149771;
                    break;
            }
            int vsiIdOam = getVsiId(telnetClientAggMT, telnetClientAggBK, start, end, 2);
            if (vsiIdOam > 0) {
                param.put("VSI ID OAM", vsiIdOam + "");

                int vsiIdOamBK = getVsiId(telnetClientAggMT, telnetClientAggBK, vsiIdOam + 1, end, 1);
                if (vsiIdOamBK > 0) {
                    param.put("VSI ID OAM backup", vsiIdOamBK + "");
                }
            }

            // Lay thong tin VSI NAME, VSI NAME BK
            int ringId = getRingId4G(telnetClientAggMT, telnetClientAggBK, 1);
            param.put("VSI Name Service", "4G_MPLS_" + ringId + "_Service");
            param.put("VSI Name OAM", "4G_MPLS_" + ringId + "_OAM");
            param.put("VSI Name Backup Service", "4G_MPLS_" + ringId + "_Service_BK");
            param.put("VSI Name Backup OAM", "4G_MPLS_" + ringId + "_OAM_BK");

            // Lay thong tin VE ID Service
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 2746;
                    end = 2785;
                    break;
                case TYPE_CASCADE:
                    start = 2826;
                    end = 2865;
                    break;
            }
            int veIdSer = getVeId(telnetClientAggMT, telnetClientAggBK, start, end, 2);
            if (veIdSer > 0) {
                param.put("VE ID Service", veIdSer + "");

                int veIdSerBk = getVeId(telnetClientAggMT, telnetClientAggBK, veIdSer + 1, end, 1);
                param.put("VE ID Service Backup", veIdSerBk + "");
            }

            // Lay thong tin VE ID OAM
            switch (typeRing) {
                case TYPE_NORMAL:
                    start = 2786;
                    end = 2825;
                    break;
                case TYPE_CASCADE:
                    start = 2866;
                    end = 2905;
                    break;
            }
            int veIdOam = getVeId(telnetClientAggMT, telnetClientAggBK, start, end, 2);
            if (veIdOam > 0) {
                param.put("VE ID OAM", veIdOam + "");

                int veIdOamBk = getVeId(telnetClientAggMT, telnetClientAggBK, veIdOam + 1, end, 1);
                param.put("VE ID OAM Backup", veIdOamBk + "");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (telnetClientAggMT != null) {
                try {
                    telnetClientAggMT.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            if (telnetClientAggBK != null) {
                try {
                    telnetClientAggBK.disConnect();
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        return param;
    }

    private static void getState(Node srt, Map<String, String> param, TelnetClientUtil telnetClientSrt, String interfaceName) {
        if (!isNullOrEmpty(interfaceName)) {
            param.putAll(getVendor4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin MAC
            param.put("MAC", getMac4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin GIAN
            param.put("GIAN", getGiant4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin State
            param.put("State", getState4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin thu phat
            param.putAll(getRxTx4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin CRC
            param.put("CRC", getCrc4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin Negotiation
            param.put("Negotiation", getNegotiation4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin Speed
            param.put("Speed", getSpeed4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));

            //Lay thong tin Duplex
            param.put("Duplex", getDuplex4G(telnetClientSrt, interfaceName,
                    srt.getVendor().getVendorName(), srt.getVersion().getVersionName()));
        }
    }

    //---------------------------Cac tham so 4G_End
    public static void main(String[] args) throws Exception {
//        Node srt = new NodeServiceImpl().findById(100757l);
//        Node agg1 = new NodeServiceImpl().findById(100832l);//0002 GigabitEthernet2/1/1
//        Node agg2 = new NodeServiceImpl().findById(100275l);//0008
//        
//        CustomerNode client = new CustomerNode();
//        Customer customer = new Customer();
//        customer.setVrfName("L3VPN_AGRIBANK_HYN");
//        client.setInterfaceId("0/0/1");
//		client.setCustomer(customer);
//        
//        //System.out.println(getParamL3(srt, "KV1", client));
//        System.out.println(getParamL2(agg1, agg2, srt, "KV1", ParamUtil.TYPE_NORMAL, "GigabitEthernet1/1/0", "GigabitEthernet1/1/6", client));
        String content = "Interface      portattribute  mode  BW(Mbits) Admin Phy   Prot  Description \n"
                + "gei_1/1        optical    Duplex/full  1000   up    up    up    QNH0384AS03_150.\n"
                + "gei_1/2        optical    Duplex/full  1000   up    up    up    QNH0016AS14_150.\n"
                + "gei_1/3        optical    Duplex/full  1000   up    up    up    QNH0016AS13_190.\n"
                + "gei_1/4        optical    Duplex/full  1000   up    up    up    QNH0016AS10(180.\n"
                + "gei_1/5        electric   Duplex/full  100    up    down  down  CAMERA_TRAM_TD\n"
                + "gei_1/6        optical    Duplex/full  1000   up    down  down  OW_BOTC_KBNN_Tie\n"
                + "gei_1/7        optical    Duplex/full  1000   up    up    up    QNH0016AS09(150.\n"
                + "gei_1/8        optical    Duplex/full  1000   up    up    up    QNH0016AS08_180.\n"
                + "gei_1/9        electric   Duplex/full  100    up    up    up    MUL_TEST\n"
                + "gei_1/10       optical    Duplex/full  1000   up    up    up    QNH0016AS07_180.";
        List<String> values = getValueRowColumnIndex(content, 1, 4, 0, "gei_1/1", "CONTAIN",
                null, null, "\\s+", null);

        System.out.println(values.size() > 0 ? values.get(0) : "");
    }
}
