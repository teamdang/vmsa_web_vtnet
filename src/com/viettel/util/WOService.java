/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

//import com.sun.xml.internal.ws.client.BindingProviderProperties;

import com.viettel.gnoc.wfm.service.ResultDTO;
import com.viettel.gnoc.wfm.service.WoServices;
import com.viettel.gnoc.wfm.service.WoServicesImplServiceLocator;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;

/**
 *
 * @author hienhv4
 */
public class WOService {

    private static final Logger logger = Logger.getLogger(WOService.class);
    private static WoServices service;
    private static final int TIME_OUT = 30000;

    static {
        try {
            service = new WoServicesImplServiceLocator().getWoServicesImplPort(new URL(MessageUtil.getResourceBundleConfig("ws_workorder_url")));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public static ResultDTO updateMopInfo(String woNumber, String message, String flowRunId, Long type) {
        try {
            ResultDTO result = service.updateMopInfo(woNumber, message, flowRunId, type);
            return result;
        } catch (RemoteException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
