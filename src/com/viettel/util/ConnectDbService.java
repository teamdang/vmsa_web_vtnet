package com.viettel.util;

import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class ConnectDbService {

	private String passwordEniq;
	private String usernameEniq;
	private String urlEniq;
	private Connection connection = null;
	protected static final Logger logger = LoggerFactory.getLogger(ConnectDbService.class);

	public ConnectDbService(String urlEniq, String usernameEniq, String passwordEniq) {
		super();
		this.passwordEniq = passwordEniq;
		this.usernameEniq = usernameEniq;
		this.urlEniq = urlEniq;
	}

	public void connectionDb() throws Exception {
		try {
			urlEniq = urlEniq.replace("\r", "");
			urlEniq = urlEniq.replace("\n", "");
			if (urlEniq.contains("jdbc:sybase:Tds:"))
				Class.forName("com.sybase.jdbc4.jdbc.SybDataSource");
			else if (urlEniq.contains("jdbc:oracle:thin:"))
				Class.forName("oracle.jdbc.driver.OracleDriver");
			else if (urlEniq.contains("jdbc:jtds:sqlserver"))
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
			else if (urlEniq.contains("jdbc:mysql"))
				Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(urlEniq, usernameEniq, passwordEniq);
		} catch (Exception e) {
			throw (e);
		}
	}
	
	public String getResults(String fileSql, List<List<Object>> data, List<String> columns) throws Exception {

		PreparedStatement preStatement = null;
		ResultSet rs = null;

		StringBuilder result = new StringBuilder();
		try {

			fileSql = fileSql.replaceAll("(?m)^((?:(?!--|').|'(?:''|[^'])*')*)--.*$", "$1");
			String sql = fileSql.trim();
			connection.setAutoCommit(false);
            sql = sql.replaceAll(";$", "");
            sql = sql.trim();
            if (sql.toLowerCase().startsWith("select")){
                sql = "SELECT * \n" +
                        "FROM \n" +
                        "     ("+sql+") \n" +
                        "WHERE ROWNUM <= 1000 \n";
            }
            preStatement = connection.prepareStatement(sql);
            boolean execute = preStatement.execute();
            if(execute){
                rs = preStatement.getResultSet();
            }else{
                result.append(preStatement.getUpdateCount()+" rows updated.\r\n");
            }

            if(rs!=null) {

                ResultSetMetaData rsmd = rs.getMetaData();

//                result += PrintColumnTypes.printColTypes(rsmd);
//                result += "\n";

                int numberOfColumns = rsmd.getColumnCount();

                for (int i = 1; i <= numberOfColumns; i++) {
                    String columnName = rsmd.getColumnName(i);
                    result.append(columnName + "|");
                    columns.add(columnName);
                }
                result.append("\n");
                int line = 0;
                while (rs.next()) {
                    List<Object> objects = new LinkedList<>();
                    for (int i = 1; i <= numberOfColumns; i++) {
                        String columnValue = rs.getString(i);
                        result.append(columnValue + "|");
                        objects.add(rs.getObject(i));
                    }
                    data.add(objects);
                    result.append("\n");
                }
                result.append("\n");
            }
            if (!execute)
			    connection.commit();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
            result.append(Throwables.getStackTraceAsString(e));
			connection.rollback();
		} finally {
			if (rs != null)
				rs.close();
			if (preStatement != null)
				preStatement.close();
		}
		return result.toString();
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

    public static class PrintColumnTypes  {

        public static String printColTypes(ResultSetMetaData rsmd) throws SQLException {
            int columns = rsmd.getColumnCount();
            String result="";
            for (int i = 1; i <= columns; i++) {
                int jdbcType = rsmd.getColumnType(i);
                String name = rsmd.getColumnTypeName(i);
                System.out.println(("Column " + i + " is JDBC type " + jdbcType));
                result = result.concat("Column " + i + " is JDBC type " + jdbcType);
                System.out.println((", which the DBMS calls " + name+"\n"));
                result = result.concat(", which the DBMS calls " + name+"\n");
            }
            return result;
        }
    }


}
