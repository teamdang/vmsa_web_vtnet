package com.viettel.util;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.viettel.model.Action;
import com.viettel.persistence.ActionDetailServiceImpl;
import com.viettel.persistence.ActionOfFlowServiceImpl;
import com.viettel.persistence.NodeTypeServiceImpl;

public class ActionUtil {

	protected static final Logger logger = LoggerFactory.getLogger(ActionUtil.class);
	
	public static boolean checkExistActionDetail(Long vendorId, Long nodeTypeId, 
			Long versionId, Long actionId) {
		boolean check = false;
		try {
			Map<String, Object> filters = new HashMap<String, Object>();
			filters.put("vendor.vendorId", vendorId);
			filters.put("nodeType.typeId", nodeTypeId);
			filters.put("action.actionId", actionId);
			filters.put("version.versionId", versionId);
			
			int count = new ActionDetailServiceImpl().count2(filters);
			if (count > 0) {
				check = true;
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			check = false;
		}
		return check;
	}
	
	public static boolean isActionReferToTempApprove(Action action) {
		boolean check = false;
		if (action != null) {
			try {
				Map<String, Object> paramList = new HashMap<>();
				paramList.put("action_id_approved", action.getActionId());
				String sql = "select count(*) from action_of_flow b where b.flow_templates_id in (select a.flow_templates_id from flow_templates a where a.status = 9) and b.action_id = :action_id_approved";
				int count = new ActionOfFlowServiceImpl().count(null, sql, paramList);
				if (count > 0) {
					check = true;
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}
		return check;
	}
}
