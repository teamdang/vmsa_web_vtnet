/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author hienhv4
 */
public class ZipUtils {
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(ZipUtils.class);
    public static final String X_CHARFORM_NOHORN = "aaaaaaaaaaaaaaaaaeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyydAAAAAAAAAAAAAAAAAEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYD";
    public static final String X_CHARFORM_UNICODE = "áàãảạăắằẵẳặâấầẫẩậéèẽẻẹêếềễểệíìĩỉịóòõỏọôốồỗổộơớờỡởợúùũủụưứừữửựýỳỹỷỵđÁÀÃẢẠĂẮẰẴẲẶÂẤẦẪẨẬÉÈẼẺẸÊẾỀỄỂỆÍÌĨỈỊÓÒÕỎỌÔỐỒỖỔỘƠỚỜỠỞỢÚÙŨỦỤƯỨỪỮỬỰÝỲỸỶỴĐ";

    public static void zipFolder(final File folder, final File zipFile) throws IOException {
        zipFolder(folder, new FileOutputStream(zipFile));
    }

    public static void zipFolder(final File folder, final OutputStream outputStream) throws IOException {
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {
            processFolder(folder, zipOutputStream, folder.getPath().length() + 1);
        }
    }

    private static void processFolder(final File folder, final ZipOutputStream zipOutputStream, final int prefixLength)
            throws IOException {
        for (final File file : folder.listFiles()) {
            if (file.isFile()) {
                final ZipEntry zipEntry = new ZipEntry(file.getPath().substring(prefixLength));
                zipOutputStream.putNextEntry(zipEntry);
                try (FileInputStream inputStream = new FileInputStream(file)) {
                    IOUtils.copy(inputStream, zipOutputStream);
                }
                zipOutputStream.closeEntry();
            } else if (file.isDirectory()) {
                processFolder(file, zipOutputStream, prefixLength);
            }
        }
    }
    
    public static String clearHornUnicode(String strSource) {
        try {
            return convertCharForm(strSource, X_CHARFORM_UNICODE, X_CHARFORM_NOHORN);
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static String getSafeFileName(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != '/' && c != '\\' && c != 0) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private static String convertCharForm(String paramString1, String paramString2, String paramString3) {
        if (paramString1 == null) {
            return null;
        }
        int i = paramString1.length();
        int j;
        StringBuilder localStringBuffer = new StringBuilder();
        for (int k = 0; k < i; ++k) {
            char c = paramString1.charAt(k);
            if ((j = paramString2.indexOf(c)) >= 0) {
                localStringBuffer.append(paramString3.charAt(j));
            } else {
                localStringBuffer.append(c);
            }
        }
        return localStringBuffer.toString().replace("\n", "");
    }
    
    public static File modifyZip(List<File> files, File oldFile, String newFile) {
        ZipOutputStream append = null;
        ZipFile source = null;
        File zipfile = new File(newFile);
        Map<String, Boolean> mapExisted = new HashMap<>();
        FileOutputStream out = null;
        try {
            if (files != null && !files.isEmpty()) {
                for (File file : files) {
                    mapExisted.put(file.getName(), Boolean.TRUE);
                }
            }
            source = new ZipFile(oldFile);

            out = new FileOutputStream(zipfile);
            append = new ZipOutputStream(out);
            Enumeration<? extends ZipEntry> entries = source.entries();
            while (entries.hasMoreElements()) {
                InputStream in = null;
                try {
                    ZipEntry e = entries.nextElement();
                    if (!mapExisted.containsKey(e.getName())) {
                        append.putNextEntry(e);
                        if (!e.isDirectory()) {
                            in = source.getInputStream(e);
                            copy(in, append);
                        }
                        append.closeEntry();
                    }
                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException ex) {
                            LOGGER.error(ex.getMessage(), ex);
                        }
                    }
                }
            }
            
            for (File f : files) {
                FileInputStream in = null;
                try {
                    ZipEntry e = new ZipEntry(f.getName());
                    append.putNextEntry(e);
                    in = new FileInputStream(f);
                    copy(in, append);
                    append.closeEntry();
                } catch (Exception ex) {
                    LOGGER.error(ex.getMessage(), ex);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException ex) {
                            LOGGER.error(ex.getMessage(), ex);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        } finally {
            if (source != null) {
                try {
                    source.close();
                } catch (IOException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
            
            if (append != null) {
                try {
                    append.close();
                } catch (IOException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
            
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    LOGGER.error(ex.getMessage(), ex);
                }
            }
        }
        return zipfile;
    }
    
    public static void copy(InputStream input, OutputStream output) throws IOException {
        byte[] BUFFER = new byte[4096 * 1024];
        int bytesRead;
        while ((bytesRead = input.read(BUFFER)) != -1) {
            output.write(BUFFER, 0, bytesRead);
        }
    }
}
