package com.viettel.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.security.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by hienhv4 on 6/2/2017.
 */
public class EvalUtil {

    private String value;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static class RestrictedAccessControlContext {

        private static final AccessControlContext INSTANCE;

        static {
            INSTANCE = new AccessControlContext(
                    new ProtectionDomain[]{
                        new ProtectionDomain(null, null) // No permissions
                    });
        }
    }

    public String evalScript(final String formula)
            throws ScriptException, PrivilegedActionException {
        ScriptEngineManager factory = new ScriptEngineManager();
        final ScriptEngine engine = factory.getEngineByName("JavaScript");
        // Restrict permission using the two-argument form of doPrivileged()
        try {
            AccessController.doPrivileged(
                    new PrivilegedExceptionAction<Object>() {

                        @Override
                        public Object run() throws ScriptException {
                            value = engine.eval(formula).toString();
                            return value;
                        }
                    },
                    // From nested class
                    RestrictedAccessControlContext.INSTANCE);

        } catch (PrivilegedActionException pae) {
            logger.error(pae.getMessage(), pae);
        }
        return value;
    }
    
    public static void main(String[] args) throws Exception {
        System.out.println(new EvalUtil().evalScript("'\"msg.appId = S6a and msg.dest-realm = epc.mnc018.mcc502.3gppnetwork.org and (msg.user-name =~ /^528111(90001385|70008615)/)\"' != ''"));
    }
}
