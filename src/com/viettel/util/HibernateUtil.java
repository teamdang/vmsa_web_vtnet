package com.viettel.util;



import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import com.viettel.passprotector.PassProtector;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.classloading.internal.ClassLoaderServiceImpl;
import org.hibernate.boot.registry.classloading.spi.ClassLoaderService;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.SessionFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Nguyễn Xuân Huy <huynx6@viettel.com.vn>
 * @sin Jul 29, 2016
 * @version 1.0
 */
public class HibernateUtil {
    private static  byte[] SALT = {
            (byte) 0xba, (byte) 0x96, (byte) 0x67, (byte) 0x84,
            (byte) 0xef, (byte) 0xd5, (byte) 0x25, (byte) 0xa1};
    String s = new String(SALT);
    private static Map<String, SessionFactory> sessionFactorys = new HashMap<String, SessionFactory>();
    protected static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);

    /*private static SessionFactory buildSessionFactory(String resource) {
        try {
            if (sessionFactorys.get(resource) == null) {
                sessionFactorys.put(resource, new Configuration().configure(resource).buildSessionFactory());
            }
            return sessionFactorys.get(resource);
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }*/
    private static synchronized SessionFactory buildSessionFactory(String resource) {
        try {
            if (sessionFactorys.get(resource) == null) {
                ClassLoaderService classLoaderService = new ClassLoaderServiceImpl();
                URL locateResource = classLoaderService.locateResource(resource);
                if (locateResource == null) {
                    locateResource = classLoaderService.locateResource(resource);
                }
                String resourceFolder;
                InputStream inputStream;
                if (locateResource != null) {
                    resourceFolder = new File(locateResource.toURI()).getParent();
                    inputStream = classLoaderService.locateResourceStream(resource);
                } else {
                    resourceFolder = "";
                    logger.info(new File(resource).getAbsolutePath());
                    if (new File(resource).exists()) {
                        inputStream = new FileInputStream(resource);
                    } else {
                        inputStream = null;
                    }
                }
//                logger.info(resourceFolder);
                //Đọc file cấu hình full

                if (inputStream != null) {

                    String contentFile;
                    try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                        contentFile = (IOUtils.toString(reader));
                    }

                    Configuration configure;
                    if (locateResource != null) {
                        configure = new Configuration().configure(resource);
                    } else {
                        configure = new Configuration().configure(new File(resourceFolder + resource).toURI().toURL());
                    }

                    //Lay thong tin da ma hoa
                    String _username = configure.getProperty("hibernate.connection.username");
                    String _password = configure.getProperty("hibernate.connection.password");
                    String _url = configure.getProperty("hibernate.connection.url");

                    if (_username != null && !_username.isEmpty()) {
                        configure.setProperty("hibernate.connection.username", PassProtector.decrypt(_username, new String(SALT)));
                    }
                    if (_password != null && !_password.isEmpty()) {
                        configure.setProperty("hibernate.connection.password", PassProtector.decrypt(_password, new String(SALT)));
                    }

                    if (_url != null && !_url.isEmpty()) {
                        configure.setProperty("hibernate.connection.url", PassProtector.decrypt(_url, new String(SALT)));
                    }

                    //Neu thay doi pass
                    _username = configure.getProperty("hibernate.connection.username.new");
                    _password = configure.getProperty("hibernate.connection.password.new");
                    _url = configure.getProperty("hibernate.connection.url.new");
                    if (_username != null && !_username.isEmpty()) {
                        configure.setProperty("hibernate.connection.username", _username);
                        configure.setProperty("hibernate.connection.username.new", "");
                        contentFile = contentFile.replaceAll("<property name=\"hibernate.connection.username\">(.*)</property>",
                                "<property name=\"hibernate.connection.username\">" + PassProtector.encrypt(_username, new String(SALT)) + "</property>")
                                .replaceAll("<property name=\"hibernate.connection.username.new\">(.*)</property>",
                                        "<property name=\"hibernate.connection.username.new\"></property>");
                    }
                    if (_password != null && !_password.isEmpty()) {
                        configure.setProperty("hibernate.connection.password", _password);
                        configure.setProperty("hibernate.connection.password.new", "");
                        contentFile = contentFile.replaceAll("<property name=\"hibernate.connection.password\">(.*)</property>",
                                "<property name=\"hibernate.connection.password\">" + PassProtector.encrypt(_password, new String(SALT)) + "</property>")
                                .replaceAll("<property name=\"hibernate.connection.password.new\">(.*)</property>",
                                        "<property name=\"hibernate.connection.password.new\"></property>");
                    }
                    if (_url != null && !_url.isEmpty()) {
                        configure.setProperty("hibernate.connection.url", _url);
                        configure.setProperty("hibernate.connection.url.new", "");
                        contentFile = contentFile.replaceAll("<property name=\"hibernate.connection.url\">(.*)</property>",
                                "<property name=\"hibernate.connection.url\">" + PassProtector.encrypt(_url, new String(SALT)) + "</property>")
                                .replaceAll("<property name=\"hibernate.connection.url.new\">(.*)</property>",
                                        "<property name=\"hibernate.connection.url.new\"></property>");
                    }

                    try (PrintWriter printWriter = new PrintWriter(resourceFolder + resource)) {
                        printWriter.println(contentFile);
                    }
                    sessionFactorys.put(resource, configure.buildSessionFactory());

//                    System.err.println(contentFileNoEncrypt.toString());
                    //Mã hóa nội dung và ghi de lai file
                } else {
                    throw new FileNotFoundException(resource + " not found!");
                }

            }
            return sessionFactorys.get(resource);
        } catch (Throwable ex) {
            logger.error("Initial SessionFactory creation failed." + ex);
            logger.error(ex.getMessage(), ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return buildSessionFactory("/hibernate.cfg.xml");
    }

    /**
     * @param resource: "/hibernate.cfg.xml";
     * @return
     * @author huynx6
     *
     */
    public static SessionFactory getSessionFactory(String resource) {
        if (resource == null) {
            return getSessionFactory();
        }
        return buildSessionFactory(resource);
    }

    public static Session openSession() {
        return getSessionFactory().openSession();
    }

    public static Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
    }

    public static void shutdown() {
        getSessionFactory().close();
    }

    public static Properties getProperties() {
        SessionFactoryImpl sessionFactory = (SessionFactoryImpl) getSessionFactory();
        return sessionFactory.getProperties();
    }

    public static void main(String[] args) {

    }
    
    public static Connection connectDbOracle() {
        try {
            Configuration config = new Configuration();
            config.configure("/hibernate.cfg.xml");
            String url = PassProtector.decrypt(config.getProperty("hibernate.connection.url"), new String(SALT));
            String userName = PassProtector.decrypt(config.getProperty("X.connection.username"), new String(SALT));
            String password = PassProtector.decrypt(config.getProperty("hibernate.connection.password"), new String(SALT));
            try {

                Class.forName("oracle.jdbc.driver.OracleDriver");
                Connection conn = DriverManager.getConnection(url, userName, password);
                return conn;

            } catch (ClassNotFoundException | SQLException ex) {
                logger.error(ex.getMessage(),ex);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(),ex);
        }
        return null;
    }
}
