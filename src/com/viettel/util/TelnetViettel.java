/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.util;

import org.apache.log4j.Logger;

/**
 *
 * @author hienhv4
 */
public class TelnetViettel extends TelnetClientUtil {
     private final Logger logger = Logger.getLogger(TelnetViettel.class);

    private final String nodeType;

    public TelnetViettel(String hostName, Integer port, String vendor, String version, String nodeType) {
        super(hostName, port, vendor);
        cmdExit = "quit";
        this.nodeType = nodeType;
    }

    @Override
    public String sendWait(String command, String promt, boolean isRegex, Integer timeOut) throws Exception {
        if ("msc".equalsIgnoreCase(nodeType)) {
            String content = super.sendWait(command, promt, isRegex, timeOut);

            if (promt == null || promt.trim().isEmpty()) {
                try {
                    String value = content.replaceFirst(this.prompt, "");
                    if (!checkRegex(value, this.prompt)) {
                        content += super.readUntilBelongRegex("", this.prompt, 1000);
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            return content;
        } else {
            return super.sendWait(command, promt, isRegex, timeOut);
        }
    }

    @Override
    public String sendWaitHasConfirm(String command, String promt, String confirmPromt, String commandForConfirm, String morePromt, String commandForMore, Integer timeOut) throws Exception {
        if ("msc".equalsIgnoreCase(nodeType)) {
            String content = super.sendWaitHasConfirm(command, promt, confirmPromt, commandForConfirm, morePromt, commandForMore, timeOut);

            if (promt == null || promt.trim().isEmpty()) {
                try {
                    String value = content.replaceFirst(this.prompt, "");
                    if (!checkRegex(value, this.prompt)) {
                        content += super.readUntilBelongRegex("", this.prompt, 1000);
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            return content;
        } else {
            return super.sendWaitHasConfirm(command, promt, confirmPromt, commandForConfirm, morePromt, commandForMore, timeOut);
        }
    }

    @Override
    public String sendWaitHasMore(String command, String promt, String morePromt, String commandForMore, Integer timeOut) throws Exception {
        if ("msc".equalsIgnoreCase(nodeType)) {
            String content = super.sendWaitHasMore(command, promt, morePromt, commandForMore, timeOut);

            if (promt == null || promt.trim().isEmpty()) {
                try {
                    String value = content.replaceFirst(this.prompt, "");
                    if (!checkRegex(value, this.prompt)) {
                        content += super.readUntilBelongRegex("", this.prompt, 1000);
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
            return content;
        } else {
            return super.sendWaitHasMore(command, promt, morePromt, commandForMore, timeOut);
        }
    }

    @Override
    public void write(String value) throws Exception {
        if ("msc".equalsIgnoreCase(nodeType)) {
            super.write(value + "\r\n");
        } else {
            super.write(value);
        }
    }
}
