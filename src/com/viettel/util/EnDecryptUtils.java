package com.viettel.util;

import com.viettel.security.PassTranformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by huybq3 on 1/30/15.
 */
public class EnDecryptUtils {

    protected static final Logger logger = LoggerFactory.getLogger(EnDecryptUtils.class);

    public static void main(String[] args) {
//        System.out.println(encrypt("cisco"));
//        System.out.println(encrypt("viettel@2"));
//        72a3be19e26c50aead5b786d276e8046
//bc42ae7e8506ba8010b01d7a5dd7142e
        String user_lab = MessageUtil.getResourceBundleConfig("user_getParamL3For4G");
        String password_lab = MessageUtil.getResourceBundleConfig("password_getParamL3For4G");
        System.out.println(decrypt(user_lab));
        System.out.println(decrypt(password_lab));
    }

    public synchronized static String encrypt(String input, String salt) {
        PassTranformer.setInputKey(salt);
        return PassTranformer.encrypt(input);
    }

    public synchronized static String encrypt(String input) {
        return encrypt(input, Constants.DEFAULT_SALT);
    }

    public synchronized static String decrypt(String input, String salt) {
        try {
            PassTranformer.setInputKey(salt);
            return PassTranformer.decrypt(input);
        } catch (Exception ex) {
            logger.debug(ex.getMessage(), ex);
            logger.error("Error", "PassTranformer decrypt error : " + input);
            return input;
        }
    }

    public synchronized static String decrypt(String input) {
        return decrypt(input, Constants.DEFAULT_SALT);
    }
}
