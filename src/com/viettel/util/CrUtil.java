package com.viettel.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.faces.model.SelectItem;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.rits.cloning.Cloner;
import com.viettel.gnoc.cr.service.CrImpactInfoForGateProService;
import com.viettel.gnoc.cr.service.CrImpactInfoForGateProServiceImplServiceLocator;
import com.viettel.gnoc.cr.service.GnocInfoForGateProDTO;
import com.viettel.gnoc.cr.service.GnocInfoForGateProOutputDTO;
import com.viettel.vsa.token.UserToken;

public class CrUtil extends SessionWrapper {

    protected static final Logger LOGGER = LoggerFactory.getLogger(CrUtil.class);

    public static List<GnocInfoForGateProDTO> getListGnocInfo(String crNumber, String exchangeIp) {
        Long timeZone = Long.valueOf(ResourceBundle.getBundle("config").getString("VssTimeZone"));
        Date dateTmp = new Date();
        Date date = new Date(dateTmp.getTime() + timeZone * 3600 * 1000);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateStr = formatter.format(date);
        String user = null;
        List<GnocInfoForGateProDTO> lst = new ArrayList<>();
        try {
            try {
                UserToken vsaUserToken = (UserToken) getCurrentSession().getAttribute("vsaUserToken");
                if (vsaUserToken != null) {
                    user = vsaUserToken.getUserName();
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }

            CrImpactInfoForGateProServiceImplServiceLocator crImpactInfoForGateProService = new CrImpactInfoForGateProServiceImplServiceLocator();
            CrImpactInfoForGateProService crService = crImpactInfoForGateProService.getCrImpactInfoForGateProServiceImplPort();
            GnocInfoForGateProOutputDTO infor = crService.getCrInfoByUserForGateProWithIp(null, null, null, null, dateStr, null, user, crNumber, null, null,
                    exchangeIp);

            if (infor.getCrInfoForGateProOutput() != null) {
                lst = Arrays.asList(infor.getCrInfoForGateProOutput());
            }

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }

        return lst;
    }

    public List<GnocInfoForGateProDTO> getListCrForToolSync() {
        return getListGnocInfo(null, null);
    }

    Map<String, List<String>> mapNodeNoCr = new HashMap<String, List<String>>();

    public List<String> getCrWithListNode(List<String> nodeNames) {
        List<String> crs = new ArrayList<String>();
        List<GnocInfoForGateProDTO> gateProDTOs = getListCrForToolSync();
        Multimap<String, String> mapCrWithNode = LinkedListMultimap.create();
        for (GnocInfoForGateProDTO taskNightInfoForGatePro : gateProDTOs) {
            mapCrWithNode.put(taskNightInfoForGatePro.getCrNumber(), taskNightInfoForGatePro.getNodePortName());
        }
        for (Iterator<String> iterator = mapCrWithNode.keySet().iterator(); iterator.hasNext();) {
            String crNumber = (String) iterator.next();
            Collection<String> nodeInCrs = mapCrWithNode.get(crNumber);
            if (nodeInCrs != null && nodeInCrs.containsAll(nodeNames)) {
                crs.add(crNumber);
            } else {
                List<String> clone = new Cloner().deepClone(nodeNames);
                clone.removeAll(nodeInCrs);
                mapNodeNoCr.put(crNumber, clone);
            }

        }
        return crs;
    }

    public static List<SelectItem> getListCrNumber() {
        List<SelectItem> listCr = new ArrayList<>();

        List<GnocInfoForGateProDTO> list = getListGnocInfo(null, null);
        Set<String> crs = new HashSet<>();
        for (GnocInfoForGateProDTO gateProDTO : list) {
            crs.add(gateProDTO.getCrNumber());
        }
        for (String cr : crs) {
            listCr.add(new SelectItem(cr, cr));
        }

        return listCr;
    }

    public static List<String> getListNodeFromCr(String selectedCr) {
        Set<String> crs = new HashSet<>();
        List<GnocInfoForGateProDTO> list = getListGnocInfo(null, null);
        for (GnocInfoForGateProDTO gateProDTO : list) {
            if (gateProDTO.getCrNumber().equalsIgnoreCase(selectedCr)) {
                crs.add(gateProDTO.getNodePortName());
            }
        }
        return new ArrayList<>(crs);
    }

    public static List<String> getListIpFromCr(String selectedCr) {
        Set<String> crs = new HashSet<>();
        List<GnocInfoForGateProDTO> list = getListGnocInfo(null, null);
        for (GnocInfoForGateProDTO taskNightInfoForGatePro : list) {
            if (taskNightInfoForGatePro.getCrNumber().equalsIgnoreCase(selectedCr)) {
                crs.add(taskNightInfoForGatePro.getIp());
            }
        }
        return new ArrayList<>(crs);
    }

    @SuppressWarnings("static-access")
    public static void main(String[] args) {
        System.out.println(new CrUtil().getListGnocInfo(null, null).size());
//		System.err.println(new CrUtil().getCrWithListNode(Arrays.asList(new String[]{"PDL9105BSC22"})) );
        //System.err.println(new CrUtil().getListNodeFromCr("CR_NORMAL_MANGLOI_14188"));
        //System.err.println(new CrUtil().getListIpFromCr("CR_NORMAL_MANGLOI_14188"));
    }

}
