$.fn.clickOff = function(callback, selfDestroy) {
		var clicked = false;
		var parent = this;
		var destroy = selfDestroy || true;

		parent.click(function() {
			clicked = true;
		});

		$(document).click(function(event) {
			if (!clicked) {
				callback(parent, event);
			}
			if (destroy) {
			};
			clicked = false;
		});
	};

/**
 * PrimeFaces Olympos Layout
 */
var Olympos = {

    init: function() {
        this.menuWrapper = $('#layout-menu-cover');
        this.menu = $('#layout-menu');
        this.menulinks = this.menu.find('a.menulink');
        this.menuResizeButton = $('#menu-resize-btn');
        this.menuPopupButton = $('#mobile-menu-btn');
        this.layoutTabmenu = $('#layout-tab-menu');
        this.layoutSubmenus = this.menuWrapper.find('.layout-tab-submenu');
        this.body = $('body');
        this.expandedMenuitems = this.expandedMenuitems||[];

        this.bindEvents();
    },

    bindEvents: function() {
        var $this = this;

        this.equalizeTabMenuHeights();
        $(".nano").nanoScroller({flash: true});

        // open layout tab menu sub menus
        this.layoutTabmenu.find('a').on('mouseover', function () {
            if ("layout-menu" !== $(this).attr('rel')) {
                $this.layoutTabmenu.find('li a').removeClass('active');
                $(this).addClass('active');
                $this.layoutSubmenus.removeClass('active');
                $('#' + String($(this).attr('rel'))).addClass('active');

                $this.equalizeTabMenuHeights();
            } else {
                $this.layoutTabmenu.find('li a').removeClass('active');
                $this.layoutSubmenus.removeClass('active');
            }
        });

        // change menu mode from expanded to slim
        // huynx6
        this.menuResizeButton.on('click',function() {

            if($this.body.hasClass('SlimMenu')){
                $(this).removeClass('active');
                $this.body.removeClass('SlimMenu');
                $('#layout-tab-menu span').css('display','inline-block');

                $('#layout-tab-menu .active span').css('background','#027FAC');
            }
            else{
                $(this).addClass('active');
                $this.body.addClass('SlimMenu');
                $('#layout-tab-menu'+ ' span').css('display','');
                $('#'+tabActive + ' span').css('background','');
            }
        });

        // show and hide popup main menu when click a menu button
        this.menuPopupButton.on('click', function(){
            $this.popupMenuClick = true;

            if($this.menuWrapper.hasClass('active')) {
                $(this).removeClass('active');
                $this.menuWrapper.removeClass('active');
            }
            else {
                $(this).addClass('active');
                $this.menuWrapper.addClass('active');
            }
        });


        this.menulinks.on('click',function(e) {
            var menuitemLink = $(this),
            menuitem = menuitemLink.parent(),
            parentSubmenu = menuitem.parent('ul').parent('li').closest('ul');

            if(parentSubmenu.attr('id') == "layout-menu") {
                $this.resetActiveMenuitems();
            }

            if(menuitem.hasClass('active-menu-parent')) {
                menuitem.removeClass('active-menu-parent');
                menuitemLink.removeClass('active-menu').next('ul').removeClass('active-menu');
                $this.removeMenuitem(menuitem.attr('id'));
            }
            else {
                var activeSibling = menuitem.siblings('.active-menu-parent');
                if(activeSibling.length) {
                    activeSibling.removeClass('active-menu-parent');
                    $this.removeMenuitem(activeSibling.attr('id'));

                    activeSibling.find('ul.active-menu,a.active-menu').removeClass('active-menu');
                    activeSibling.find('li.active-menu-parent').each(function() {
                        var menuitem = $(this);
                        menuitem.removeClass('active-menu-parent');
                        $this.removeMenuitem(menuitem.attr('id'));
                    });
                }

                menuitem.addClass('active-menu-parent');
                menuitemLink.addClass('active-menu').next('ul').addClass('active-menu');
                $this.addMenuitem(menuitem.attr('id'));
            }

            if(menuitemLink.next().is('ul')) {
                e.preventDefault();
            }

            $this.saveMenuState();

            $this.equalizeTabMenuHeights();
        });

        this.menuWrapper.clickOff(function(e) {
            if($this.popupMenuClick) {
                $this.popupMenuClick = false;
            }
            else if($this.body.hasClass('PopupMenu') || document.documentElement.clientWidth <= 960) {
                $this.menuPopupButton.removeClass('active');
                $this.menuWrapper.removeClass('active');
            }
        });
    },

    removeMenuitem: function(id) {
        this.expandedMenuitems = $.grep(this.expandedMenuitems, function(value) {
            return value !== id;
        });
    },

    addMenuitem: function(id) {
        if($.inArray(id, this.expandedMenuitems) === -1) {
            this.expandedMenuitems.push(id);
        }
    },

    saveMenuState: function() {
        $.cookie('olympos_expandeditems', this.expandedMenuitems.join(','), {path:'/'});
    },

    clearMenuState: function() {
        $.removeCookie('olympos_expandeditems', {path:'/'});
    },

    restoreMenuState: function() {
        var menucookie = $.cookie('olympos_expandeditems');
        if(menucookie) {
            this.expandedMenuitems = menucookie.split(',');
            for(var i = 0; i < this.expandedMenuitems.length; i++) {
                var id = this.expandedMenuitems[i];
                if(id) {
                    var menuitem = $("#" + this.expandedMenuitems[i].replace(/:/g,"\\:"));
                    menuitem.addClass('active-menu-parent');
                    menuitem.children('a,ul').addClass('active-menu');
                }
            }
        }
    },

    resetActiveMenuitems: function() {
        for(var i = 0; i < this.expandedMenuitems.length; i++) {
            var id = this.expandedMenuitems[i];
            if(id) {
                var menuitem = $("#" + this.expandedMenuitems[i].replace(/:/g,"\\:"));
                menuitem.removeClass('active-menu-parent');
                menuitem.children('a,ul').removeClass('active-menu');
            }
        }
        this.expandedMenuitems = [];
    },

    equalizeTabMenuHeights: function() {
        var activeTabMenuMaxHeight = 0,
            $this = this;

        this.layoutSubmenus.height('auto');
        this.layoutTabmenu.height('auto');

        this.layoutSubmenus.each(function() {
            var submenu = $(this);
            if(submenu.hasClass('active') && submenu.height() > activeTabMenuMaxHeight){
                activeTabMenuMaxHeight = submenu.height();

                if($this.layoutTabmenu.height() < activeTabMenuMaxHeight){
                    $this.layoutTabmenu.height(activeTabMenuMaxHeight);
                }
            }
        });

        $(".nano").nanoScroller();
    },

    isMobile: function() {
        return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(window.navigator.userAgent));
    },

    changeBodyClass: function (changedClass) {
        var bodyClasses = this.body.attr('class');

        if(changedClass === "") {
            changedClass = bodyClasses.replace("SlimMenu", "").replace("PopupMenu", "");
        }
        else if(changedClass === "SlimMenu") {
            bodyClasses = bodyClasses.replace("PopupMenu", "");
            changedClass = bodyClasses + " " + changedClass;
        }
        else if(changedClass === "PopupMenu") {
            bodyClasses = bodyClasses.replace("SlimMenu", "");
            changedClass = bodyClasses + " " + changedClass;
        }
        else {
            if(bodyClasses.indexOf("SlimMenu") >= 0) {
                changedClass = changedClass + " SlimMenu";
            }
            else if(bodyClasses.indexOf("PopupMenu") >= 0) {
                changedClass = changedClass + " PopupMenu";
            }
        }

        this.body.removeClass().addClass(changedClass);
        this.menuPopupButton.removeClass('active');
        this.menuResizeButton.removeClass('active');
    }
};

$(function() {
   Olympos.init();
});

/* Issue #924 is fixed for 5.3+ and 6.0. (compatibility with 5.3) */
PrimeFaces.widget.Dialog = PrimeFaces.widget.Dialog.extend({
    enableModality: function () {
        this._super();
        $(document.body).children(this.jqId + '_modal').addClass('ui-dialog-mask');
    },
    syncWindowResize: function () {

    }
});
var tabActive;
//huynx6
function ChangeTab(tabId) {
	for (var i = 0; i < 50; i++) {
		var tab = $('#tab-'+i);
		if (tab != null)
			tab.removeClass('active');
	}
	$('#'+tabId).addClass('active');
	tabActive = tabId;
}
PrimeFaces.locales['vi'] = {
	    closeText: 'Đóng',
	    prevText: 'Tháng trước',
	    nextText: 'Tháng sau',
	    monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
	    monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12' ],
	    dayNames: ['Chúa Nhựt', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],
	    dayNamesShort: ['CN', 'Hai', 'Ba', 'Tư', 'Năm', 'Sáu', 'Bảy'],
	    dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
	    weekHeader: 'Tuần',
	    firstDay: 1,
	    isRTL: false,
	    showMonthAfterYear: false,
	    yearSuffix:'',
	    timeOnlyTitle: 'Chọn giờ',
	    timeText: 'Giờ',
	    hourText: 'Giờ',
	    minuteText: 'Phút',
	    secondText: 'Giây',
	    currentText: 'Giờ hiện hành',
	    ampm: false,
	    month: 'Tháng',
	    week: 'Tuần',
	    day: 'Ngày',
	    allDayText: 'Cả ngày',
	    messages: {  //optional for Client Side Validation
	        'javax.faces.component.UIInput.REQUIRED': '{0}: Xác nhận lỗi: Giá trị là bắt buộc.',
	        'javax.faces.converter.IntegerConverter.INTEGER': '{2}: \'{0}\' must be a number consisting of one or more digits.',
	        'javax.faces.converter.IntegerConverter.INTEGER_detail': '{2}: \'{0}\' must be a number between -2147483648 and 2147483647 Example: {1}',
	        'javax.faces.converter.DoubleConverter.DOUBLE': '{2}: \'{0}\' must be a number consisting of one or more digits.',
	        'javax.faces.converter.DoubleConverter.DOUBLE_detail': '{2}: \'{0}\' must be a number between 4.9E-324 and 1.7976931348623157E308  Example: {1}',
	        'javax.faces.converter.BigDecimalConverter.DECIMAL': '{2}: \'{0}\' must be a signed decimal number.',
	        'javax.faces.converter.BigDecimalConverter.DECIMAL_detail': '{2}: \'{0}\' must be a signed decimal number consisting of zero or more digits, that may be followed by a decimal point and fraction.  Example: {1}',
	        'javax.faces.converter.BigIntegerConverter.BIGINTEGER': '{2}: \'{0}\' must be a number consisting of one or more digits.',
	        'javax.faces.converter.BigIntegerConverter.BIGINTEGER_detail': '{2}: \'{0}\' must be a number consisting of one or more digits. Example: {1}',
	        'javax.faces.converter.ByteConverter.BYTE': '{2}: \'{0}\' must be a number between 0 and 255.',
	        'javax.faces.converter.ByteConverter.BYTE_detail': '{2}: \'{0}\' must be a number between 0 and 255.  Example: {1}',
	        'javax.faces.converter.CharacterConverter.CHARACTER': '{1}: \'{0}\' must be a valid character.',
	        'javax.faces.converter.CharacterConverter.CHARACTER_detail': '{1}: \'{0}\' must be a valid ASCII character.',
	        'javax.faces.converter.ShortConverter.SHORT': '{2}: \'{0}\' must be a number consisting of one or more digits.',
	        'javax.faces.converter.ShortConverter.SHORT_detail': '{2}: \'{0}\' must be a number between -32768 and 32767 Example: {1}',
	        'javax.faces.converter.BooleanConverter.BOOLEAN': '{1}: \'{0}\' must be \'true\' or \'false\'',
	        'javax.faces.converter.BooleanConverter.BOOLEAN_detail': '{1}: \'{0}\' must be \'true\' or \'false\'.  Any value other than \'true\' will evaluate to \'false\'.',
	        'javax.faces.validator.LongRangeValidator.MAXIMUM': '{1}: Validation Error: Value is greater than allowable maximum of \'{0}\'',
	        'javax.faces.validator.LongRangeValidator.MINIMUM': '{1}: Validation Error: Value is less than allowable minimum of \'{0}\'',
	        'javax.faces.validator.LongRangeValidator.NOT_IN_RANGE': '{2}: Validation Error: Specified attribute is not between the expected values of {0} and {1}.',
	        'javax.faces.validator.LongRangeValidator.TYPE={0}': 'Validation Error: Value is not of the correct type.',
	        'javax.faces.validator.DoubleRangeValidator.MAXIMUM': '{1}: Validation Error: Value is greater than allowable maximum of \'{0}\'',
	        'javax.faces.validator.DoubleRangeValidator.MINIMUM': '{1}: Validation Error: Value is less than allowable minimum of \'{0}\'',
	        'javax.faces.validator.DoubleRangeValidator.NOT_IN_RANGE': '{2}: Validation Error: Specified attribute is not between the expected values of {0} and {1}',
	        'javax.faces.validator.DoubleRangeValidator.TYPE={0}': 'Validation Error: Value is not of the correct type',
	        'javax.faces.converter.FloatConverter.FLOAT': '{2}: \'{0}\' must be a number consisting of one or more digits.',
	        'javax.faces.converter.FloatConverter.FLOAT_detail': '{2}: \'{0}\' must be a number between 1.4E-45 and 3.4028235E38  Example: {1}',
	        'javax.faces.converter.DateTimeConverter.DATE': '{2}: \'{0}\' could not be understood as a date.',
	        'javax.faces.converter.DateTimeConverter.DATE_detail': '{2}: \'{0}\' could not be understood as a date. Example: {1}',
	        'javax.faces.converter.DateTimeConverter.TIME': '{2}: \'{0}\' could not be understood as a time.',
	        'javax.faces.converter.DateTimeConverter.TIME_detail': '{2}: \'{0}\' could not be understood as a time. Example: {1}',
	        'javax.faces.converter.DateTimeConverter.DATETIME': '{2}: \'{0}\' could not be understood as a date and time.',
	        'javax.faces.converter.DateTimeConverter.DATETIME_detail': '{2}: \'{0}\' could not be understood as a date and time. Example: {1}',
	        'javax.faces.converter.DateTimeConverter.PATTERN_TYPE': '{1}: A \'pattern\' or \'type\' attribute must be specified to convert the value \'{0}\'',
	        'javax.faces.converter.NumberConverter.CURRENCY': '{2}: \'{0}\' could not be understood as a currency value.',
	        'javax.faces.converter.NumberConverter.CURRENCY_detail': '{2}: \'{0}\' could not be understood as a currency value. Example: {1}',
	        'javax.faces.converter.NumberConverter.PERCENT': '{2}: \'{0}\' could not be understood as a percentage.',
	        'javax.faces.converter.NumberConverter.PERCENT_detail': '{2}: \'{0}\' could not be understood as a percentage. Example: {1}',
	        'javax.faces.converter.NumberConverter.NUMBER': '{2}: \'{0}\' could not be understood as a date.',
	        'javax.faces.converter.NumberConverter.NUMBER_detail': '{2}: \'{0}\' is not a number. Example: {1}',
	        'javax.faces.converter.NumberConverter.PATTERN': '{2}: \'{0}\' is not a number pattern.',
	        'javax.faces.converter.NumberConverter.PATTERN_detail': '{2}: \'{0}\' is not a number pattern. Example: {1}',
	        'javax.faces.validator.LengthValidator.MINIMUM': '{1}: Validation Error: Length is less than allowable minimum of \'{0}\'',
	        'javax.faces.validator.LengthValidator.MAXIMUM': '{1}: Validation Error: Length is greater than allowable maximum of \'{0}\'',
	        'javax.faces.validator.RegexValidator.PATTERN_NOT_SET': 'Regex pattern must be set.',
	        'javax.faces.validator.RegexValidator.PATTERN_NOT_SET_detail': 'Regex pattern must be set to non-empty value.',
	        'javax.faces.validator.RegexValidator.NOT_MATCHED': 'Regex Pattern not matched',
	        'javax.faces.validator.RegexValidator.NOT_MATCHED_detail': 'Regex pattern of \'{0}\' not matched',
	        'javax.faces.validator.RegexValidator.MATCH_EXCEPTION': 'Error in regular expression.',
	        'javax.faces.validator.RegexValidator.MATCH_EXCEPTION_detail': 'Error in regular expression, \'{0}\'',
	        //optional for bean validation integration in client side validation
	        'javax.faces.validator.BeanValidator.MESSAGE': '{0}',
	        'javax.validation.constraints.AssertFalse.message': 'must be false',
	        'javax.validation.constraints.AssertTrue.message': 'must be true',
	        'javax.validation.constraints.DecimalMax.message': 'must be less than or equal to {0}',
	        'javax.validation.constraints.DecimalMin.message': 'must be greater than or equal to {0}',
	        'javax.validation.constraints.Digits.message': 'numeric value out of bounds (<{0} digits>.<{1} digits> expected)',
	        'javax.validation.constraints.Future.message': 'must be in the future',
	        'javax.validation.constraints.Max.message': 'must be less than or equal to {0}',
	        'javax.validation.constraints.Min.message': 'must be greater than or equal to {0}',
	        'javax.validation.constraints.NotNull.message': 'may not be null',
	        'javax.validation.constraints.Null.message': 'must be null',
	        'javax.validation.constraints.Past.message': 'must be in the past',
	        'javax.validation.constraints.Pattern.message': 'must match "{0}"',
	        'javax.validation.constraints.Size.message': 'size must be between {0} and {1}'
	    }
	};
