//Fix SelectOneMenu resets and fires change event on ALT press
PrimeFaces.widget.SelectOneMenu = PrimeFaces.widget.DeferredWidget
		.extend({
			init : function(a) {
				
				this._super(a);
				this.panelId = this.jqId + "_panel";
				this.input = $(this.jqId + "_input");
				this.focusInput = $(this.jqId + "_focus");
				this.label = this.jq.find(".ui-selectonemenu-label");
				this.menuIcon = this.jq.children(".ui-selectonemenu-trigger");
				this.panel = this.jq.children(this.panelId);
				this.disabled = this.jq.hasClass("ui-state-disabled");
				this.itemsWrapper = this.panel
						.children(".ui-selectonemenu-items-wrapper");
				this.itemsContainer = this.itemsWrapper
						.children(".ui-selectonemenu-items");
				this.items = this.itemsContainer.find(".ui-selectonemenu-item");
				this.options = this.input.children("option");
				this.cfg.effect = this.cfg.effect || "fade";
				this.cfg.effectSpeed = this.cfg.effectSpeed || "normal";
				this.optGroupsSize = this.itemsContainer
						.children("li.ui-selectonemenu-item-group").length;
				this.cfg.autoWidth = this.cfg.autoWidth === false ? false
						: true;
				var g = this, e = this.options.filter(":selected"), f = this.items
						.eq(e.index());
				this.options.filter(":disabled").each(function() {
					g.items.eq($(this).index()).addClass("ui-state-disabled")
				});
				this.triggers = this.cfg.editable ? this.jq
						.find(".ui-selectonemenu-trigger")
						: this.jq
								.find(".ui-selectonemenu-trigger, .ui-selectonemenu-label");
				if (this.cfg.editable) {
					var c = this.label.val();
					if (c === e.text()) {
						this.highlightItem(f)
					} else {
						this.items.eq(0).addClass("ui-state-highlight");
						this.customInput = true;
						this.customInputVal = c
					}
				} else {
					this.highlightItem(f)
				}
				if (this.cfg.syncTooltip) {
					this.syncTitle(e)
				}
				this.triggers.data("primefaces-overlay-target", true).find("*")
						.data("primefaces-overlay-target", true);
				if (!this.disabled) {
					this.bindEvents();
					this.bindConstantEvents();
					this.appendPanel()
				}
				this.input.data(PrimeFaces.CLIENT_ID_DATA, this.id);
				if (PrimeFaces.env.touch) {
					this.focusInput.attr("readonly", true)
				}
				for (var d = 0; d < this.items.size(); d++) {
					this.items.eq(d).attr("id", this.id + "_" + d)
				}
				var b = f.attr("id");
				this.focusInput.attr("aria-autocomplete", "list").attr(
						"aria-owns", this.itemsContainer.attr("id")).attr(
						"aria-activedescendant", b).attr("aria-describedby", b)
						.attr("aria-disabled", this.disabled);
				this.itemsContainer.attr("aria-activedescendant", b);
				this.renderDeferred()
			},
			_render : function() {
				if (this.cfg.autoWidth) {
					this.jq.css("min-width", this.input.outerWidth())
				}
			},
			refresh : function(a) {
				this.panelWidthAdjusted = false;
				this._super(a)
			},
			appendPanel : function() {
				var a = this.cfg.appendTo ? PrimeFaces.expressions.SearchExpressionFacade
						.resolveComponentsAsSelector(this.cfg.appendTo)
						: $(document.body);
				if (!a.is(this.jq)) {
					a.children(this.panelId).remove();
					this.panel.appendTo(a)
				}
			},
			alignPanelWidth : function() {
				if (!this.panelWidthAdjusted) {
					var a = this.jq.outerWidth();
					if (this.panel.outerWidth() < a) {
						this.panel.width(a)
					}
					this.panelWidthAdjusted = true
				}
			},
			bindEvents : function() {
				var a = this;
				if (PrimeFaces.env.browser.webkit) {
					this.input.on("focus", function() {
						setTimeout(function() {
							a.focusInput.trigger("focus.ui-selectonemenu")
						}, 2)
					})
				}
				this.items.filter(":not(.ui-state-disabled)").on(
						"mouseover.selectonemenu", function() {
							var b = $(this);
							if (!b.hasClass("ui-state-highlight")) {
								$(this).addClass("ui-state-hover")
							}
						}).on("mouseout.selectonemenu", function() {
					$(this).removeClass("ui-state-hover")
				}).on("click.selectonemenu", function() {
					a.selectItem($(this));
					a.changeAriaValue($(this))
				});
				this.triggers.mouseenter(function() {
					if (!a.jq.hasClass("ui-state-focus")) {
						a.jq.addClass("ui-state-hover");
						a.menuIcon.addClass("ui-state-hover")
					}
				}).mouseleave(function() {
					a.jq.removeClass("ui-state-hover");
					a.menuIcon.removeClass("ui-state-hover")
				}).click(function(b) {
					if (a.panel.is(":hidden")) {
						a.show()
					} else {
						a.hide();
						a.revert();
						a.changeAriaValue(a.getActiveItem())
					}
					a.jq.removeClass("ui-state-hover");
					a.menuIcon.removeClass("ui-state-hover");
					a.focusInput.trigger("focus.ui-selectonemenu");
					b.preventDefault()
				});
				this.focusInput.on("focus.ui-selectonemenu", function() {
					a.jq.addClass("ui-state-focus");
					a.menuIcon.addClass("ui-state-focus")
				}).on("blur.ui-selectonemenu", function() {
					a.jq.removeClass("ui-state-focus");
					a.menuIcon.removeClass("ui-state-focus")
				});
				if (this.cfg.editable) {
					this.label.change(function() {
						a.triggerChange(true);
						a.customInput = true;
						a.customInputVal = $(this).val();
						a.items.filter(".ui-state-active").removeClass(
								"ui-state-active");
						a.items.eq(0).addClass("ui-state-active")
					})
				}
				this.bindKeyEvents();
				if (this.cfg.filter) {
					this.cfg.initialHeight = this.itemsWrapper.height();
					this.setupFilterMatcher();
					this.filterInput = this.panel
							.find("> div.ui-selectonemenu-filter-container > input.ui-selectonemenu-filter");
					PrimeFaces.skinInput(this.filterInput);
					this.bindFilterEvents()
				}
			},
			bindConstantEvents : function() {
				var b = this, a = "mousedown." + this.id;
				$(document).off(a).on(
						a,
						function(c) {
							if (b.panel.is(":hidden")) {
								return
							}
							var d = b.panel.offset();
							if (c.target === b.label.get(0)
									|| c.target === b.menuIcon.get(0)
									|| c.target === b.menuIcon.children()
											.get(0)) {
								return
							}
							if (c.pageX < d.left
									|| c.pageX > d.left + b.panel.width()
									|| c.pageY < d.top
									|| c.pageY > d.top + b.panel.height()) {
								b.hide();
								b.revert();
								b.changeAriaValue(b.getActiveItem())
							}
						});
				this.resizeNS = "resize." + this.id;
				this.unbindResize();
				this.bindResize()
			},
			bindResize : function() {
				var a = this;
				$(window).bind(this.resizeNS, function(b) {
					if (a.panel.is(":visible")) {
						a.alignPanel()
					}
				})
			},
			unbindResize : function() {
				$(window).unbind(this.resizeNS)
			},
			unbindEvents : function() {
				this.items.off();
				this.triggers.off();
				this.input.off();
				this.focusInput.off();
				this.label.off()
			},
			revert : function() {
				if (this.cfg.editable && this.customInput) {
					this.setLabel(this.customInputVal);
					this.items.filter(".ui-state-active").removeClass(
							"ui-state-active");
					this.items.eq(0).addClass("ui-state-active")
				} else {
					this
							.highlightItem(this.items.eq(this.preShowValue
									.index()))
				}
			},
			highlightItem : function(a) {
				this.items.filter(".ui-state-highlight").removeClass(
						"ui-state-highlight");
				if (a.length > 0) {
					a.addClass("ui-state-highlight");
					this.setLabel(a.data("label"))
				}
			},
			triggerChange : function(a) {
				this.changed = false;
				this.input.trigger("change");
				if (!a) {
					this.value = this.options.filter(":selected").val()
				}
			},
			triggerItemSelect : function() {
				if (this.cfg.behaviors) {
					var a = this.cfg.behaviors.itemSelect;
					if (a) {
						a.call(this)
					}
				}
			},
			selectItem : function(f, b) {
				var e = this.options.eq(this.resolveItemIndex(f)), d = this.options
						.filter(":selected"), a = e.val() == d.val(), c = null;
				if (this.cfg.editable) {
					c = (!a) || (e.text() != this.label.val())
				} else {
					c = !a
				}
				if (c) {
					this.highlightItem(f);
					this.input.val(e.val());
					this.triggerChange();
					if (this.cfg.editable) {
						this.customInput = false
					}
					if (this.cfg.syncTooltip) {
						this.syncTitle(e)
					}
				}
				if (!b) {
					this.focusInput.focus();
					this.triggerItemSelect()
				}
				if (this.panel.is(":visible")) {
					this.hide()
				}
			},
			syncTitle : function(b) {
				var a = this.items.eq(b.index()).attr("title");
				if (a) {
					this.jq.attr("title", this.items.eq(b.index())
							.attr("title"))
				} else {
					this.jq.removeAttr("title")
				}
			},
			resolveItemIndex : function(a) {
				if (this.optGroupsSize === 0) {
					return a.index()
				} else {
					return a.index()
							- a.prevAll("li.ui-selectonemenu-item-group").length
				}
			},
			bindKeyEvents : function() {
				var a = this;
				this.focusInput.on("keydown.ui-selectonemenu", function(d) {
					var c = $.ui.keyCode, b = d.which;
					switch (b) {
					case c.UP:
					case c.LEFT:
						a.highlightPrev(d);
						break;
					case c.DOWN:
					case c.RIGHT:
						a.highlightNext(d);
						break;
					case c.ENTER:
					case c.NUMPAD_ENTER:
						a.handleEnterKey(d);
						break;
					case c.TAB:
						a.handleTabKey();
						break;
					case c.ESCAPE:
						a.handleEscapeKey(d);
						break;
					case c.SPACE:
						a.handleSpaceKey(d);
						break
					}
				}).on(
						"keyup.ui-selectonemenu",
						function(g) {
							var f = $.ui.keyCode, d = g.which;
							switch (d) {
							case f.UP:
							case f.LEFT:
							case f.DOWN:
							case f.RIGHT:
							case f.ENTER:
							case f.NUMPAD_ENTER:
							case f.TAB:
							case f.ESCAPE:
							case f.SPACE:
							case f.HOME:
							case f.PAGE_DOWN:
							case f.PAGE_UP:
							case f.END:
							case f.DELETE:
							case 16:
							case 17:
							case 18:
							case 91:
							case 92:
							case 93:
							case 20:
							case 44:
								break;
							default:
								if (d >= 112 && d <= 123) {
									break
								}
								var i = $(this).val(), c = null, h = g.metaKey
										|| g.ctrlKey || g.shiftKey;
								if (!h) {
									clearTimeout(a.searchTimer);
									c = a.options.filter(function() {
										return $(this).text().toLowerCase()
												.indexOf(i.toLowerCase()) === 0
									});
									if (c.length) {
										var b = a.items.eq(c.index());
										if (a.panel.is(":hidden")) {
											a.selectItem(b)
										} else {
											a.highlightItem(b);
											PrimeFaces.scrollInView(
													a.itemsWrapper, b)
										}
									}
									a.searchTimer = setTimeout(function() {
										a.focusInput.val("")
									}, 1000)
								}
								break
							}
						})
			},
			bindFilterEvents : function() {
				var a = this;
				this.filterInput.on("keyup.ui-selectonemenu", function(d) {
					var c = $.ui.keyCode, b = d.which;
					
					switch (b) {
					case c.UP:
					case c.LEFT:
					case c.DOWN:
					case c.RIGHT:
					case c.ENTER:
					case c.NUMPAD_ENTER:
					case c.TAB:
					case c.ESCAPE:
					case c.SPACE:
					case c.HOME:
					case c.PAGE_DOWN:
					case c.PAGE_UP:
					case c.END:
					case c.DELETE:
					case 16:
					case 17:
					case 18:
					case 91:
					case 92:
					case 93:
					case 20:
					case 44:
						break;
					default:
						if (b >= 112 && b <= 123) {
							break
						}
						var f = d.metaKey || d.ctrlKey;
						if (!f) {
							a.filter($(this).val())
						}
						break
					}
				}).on("keydown.ui-selectonemenu", function(d) {
					var c = $.ui.keyCode, b = d.which;
					switch (b) {
					case c.UP:
						a.highlightPrev(d);
						break;
					case c.DOWN:
						a.highlightNext(d);
						break;
					case c.ENTER:
					case c.NUMPAD_ENTER:
						a.handleEnterKey(d);
						break;
					case c.TAB:
						a.handleTabKey();
						break;
					case c.ESCAPE:
						a.handleEscapeKey(d);
						break;
					case c.SPACE:
						a.handleSpaceKey(d);
						break;
					default:
						break
					}
				})
			},
			highlightNext : function(b) {
				var c = this.getActiveItem(), a = this.panel.is(":hidden") ? c
						.nextAll(":not(.ui-state-disabled,.ui-selectonemenu-item-group):first")
						: c
								.nextAll(":not(.ui-state-disabled,.ui-selectonemenu-item-group):visible:first");
				if (a.length === 1) {
					if (this.panel.is(":hidden")) {
						if (b.altKey) {
							this.show()
						} else {
							this.selectItem(a)
						}
					} else {
						this.highlightItem(a);
						PrimeFaces.scrollInView(this.itemsWrapper, a)
					}
					this.changeAriaValue(a)
				}
				b.preventDefault()
			},
			highlightPrev : function(b) {
				var c = this.getActiveItem(), a = this.panel.is(":hidden") ? c
						.prevAll(":not(.ui-state-disabled,.ui-selectonemenu-item-group):first")
						: c
								.prevAll(":not(.ui-state-disabled,.ui-selectonemenu-item-group):visible:first");
				if (a.length === 1) {
					if (this.panel.is(":hidden")) {
						this.selectItem(a)
					} else {
						this.highlightItem(a);
						PrimeFaces.scrollInView(this.itemsWrapper, a)
					}
					this.changeAriaValue(a)
				}
				b.preventDefault()
			},
			handleEnterKey : function(a) {
				if (this.panel.is(":visible")) {
					this.selectItem(this.getActiveItem())
				}
				a.preventDefault();
				a.stopPropagation()
			},
			handleSpaceKey : function(a) {
				var b = $(a.target);
				if (b.is("input") && b.hasClass("ui-selectonemenu-filter")) {
					return
				}
				if (this.panel.is(":hidden")) {
					this.show()
				} else {
					this.hide();
					this.revert();
					this.changeAriaValue(this.getActiveItem())
				}
				a.preventDefault()
			},
			handleEscapeKey : function(a) {
				if (this.panel.is(":visible")) {
					this.revert();
					this.hide()
				}
				a.preventDefault()
			},
			handleTabKey : function() {
				if (this.panel.is(":visible")) {
					this.selectItem(this.getActiveItem())
				}
			},
			show : function() {
				var a = this;
				this.alignPanel();
				this.panel.css("z-index", ++PrimeFaces.zindex);
				if ($.browser.msie && /^[6,7]\.[0-9]+/.test($.browser.version)) {
					this.panel.parent().css("z-index", PrimeFaces.zindex - 1)
				}
				if (this.cfg.effect !== "none") {
					this.panel.show(this.cfg.effect, {}, this.cfg.effectSpeed,
							function() {
								PrimeFaces.scrollInView(a.itemsWrapper, a
										.getActiveItem());
								if (a.cfg.filter) {
									a.focusFilter()
								}
							})
				} else {
					this.panel.show();
					PrimeFaces.scrollInView(this.itemsWrapper, this
							.getActiveItem());
					if (a.cfg.filter) {
						this.focusFilter(10)
					}
				}
				this.preShowValue = this.options.filter(":selected");
				this.focusInput.attr("aria-expanded", true)
			},
			hide : function() {
				if ($.browser.msie && /^[6,7]\.[0-9]+/.test($.browser.version)) {
					this.panel.parent().css("z-index", "")
				}
				this.panel.css("z-index", "").hide();
				this.focusInput.attr("aria-expanded", false)
			},
			focus : function() {
				this.focusInput.focus()
			},
			focusFilter : function(a) {
				if (a) {
					var b = this;
					setTimeout(function() {
						b.focusFilter()
					}, a)
				} else {
					this.filterInput.focus()
				}
			},
			blur : function() {
				this.focusInput.blur()
			},
			disable : function() {
				if (!this.disabled) {
					this.disabled = true;
					this.jq.addClass("ui-state-disabled");
					this.input.attr("disabled", "disabled");
					if (this.cfg.editable) {
						this.label.attr("disabled", "disabled")
					}
					this.unbindEvents()
				}
			},
			enable : function() {
				if (this.disabled) {
					this.disabled = false;
					this.jq.removeClass("ui-state-disabled");
					this.input.removeAttr("disabled");
					if (this.cfg.editable) {
						this.label.removeAttr("disabled")
					}
					this.bindEvents()
				}
			},
			alignPanel : function() {
				this.alignPanelWidth();
				if (this.panel.parent().is(this.jq)) {
					this.panel.css({
						left : 0,
						top : this.jq.innerHeight()
					})
				} else {
					this.panel.css({
						left : "",
						top : ""
					}).position({
						my : "left top",
						at : "left bottom",
						of : this.jq,
						collision : "flipfit"
					})
				}
			},
			setLabel : function(b) {
				var a = this.getLabelToDisplay(b);
				if (this.cfg.editable) {
					if (b === "&nbsp;") {
						this.label.val("")
					} else {
						this.label.val(a)
					}
				} else {
					if (b === "&nbsp;") {
						this.label.html("&nbsp;")
					} else {
						this.label.text(a)
					}
				}
			},
			selectValue : function(b) {
				var a = this.options.filter('[value="' + b + '"]');
				this.selectItem(this.items.eq(a.index()), true)
			},
			getActiveItem : function() {
				return this.items.filter(".ui-state-highlight")
			},
			setupFilterMatcher : function() {
				this.cfg.filterMatchMode = this.cfg.filterMatchMode
						|| "startsWith";
				this.filterMatchers = {
					startsWith : this.startsWithFilter,
					contains : this.containsFilter,
					endsWith : this.endsWithFilter,
					custom : this.cfg.filterFunction
				};
				this.filterMatcher = this.filterMatchers[this.cfg.filterMatchMode]
			},
			startsWithFilter : function(b, a) {
				return b.indexOf(a) === 0
			},
			containsFilter : function(b, a) {
				return b.indexOf(a) !== -1
			},
			endsWithFilter : function(b, a) {
				return b.indexOf(a, b.length - a.length) !== -1
			},
			filter : function(j) {
				this.cfg.initialHeight = this.cfg.initialHeight
						|| this.itemsWrapper.height();
				var h = this.cfg.caseSensitive ? $.trim(j) : $.trim(j)
						.toLowerCase();
				if (h === "") {
					this.items.filter(":hidden").show();
					this.itemsContainer
							.children(".ui-selectonemenu-item-group").show()
				} else {
					for (var c = 0; c < this.options.length; c++) {
						var d = this.options.eq(c), b = this.cfg.caseSensitive ? d
								.text()
								: d.text().toLowerCase(), l = this.items.eq(c);
						if (l.hasClass("ui-noselection-option")) {
							l.hide()
						} else {
							if (this.filterMatcher(b, h)) {
								l.show()
							} else {
								l.hide()
							}
						}
					}
					var a = this.itemsContainer
							.children(".ui-selectonemenu-item-group");
					for (var e = 0; e < a.length; e++) {
						var k = a.eq(e);
						if (e === (a.length - 1)) {
							if (k.nextAll().filter(":visible").length === 0) {
								k.hide()
							} else {
								k.show()
							}
						} else {
							if (k.nextUntil(".ui-selectonemenu-item-group")
									.filter(":visible").length === 0) {
								k.hide()
							} else {
								k.show()
							}
						}
					}
				}
				var f = this.items.filter(":visible:first");
				if (f.length) {
					this.highlightItem(f)
				}
				if (this.itemsContainer.height() < this.cfg.initialHeight) {
					this.itemsWrapper.css("height", "auto")
				} else {
					this.itemsWrapper.height(this.cfg.initialHeight)
				}
				this.alignPanel()
			},
			getSelectedValue : function() {
				return this.input.val()
			},
			getSelectedLabel : function() {
				return this.options.filter(":selected").text()
			},
			getLabelToDisplay : function(a) {
				if (this.cfg.labelTemplate && a !== "&nbsp;") {
					return this.cfg.labelTemplate.replace("{0}", a)
				}
				return a
			},
			changeAriaValue : function(a) {
				var b = a.attr("id");
				this.focusInput.attr("aria-activedescendant", b).attr(
						"aria-describedby", b);
				this.itemsContainer.attr("aria-activedescendant", b)
			}
		});